title: Media of the Past - Part 3
date: 2014-04-02 00:00:01
modified: Thu 14 Jan 2021 12:21:01 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/29127.html
originally: Media of the Past - Part 3.html
local: Media of the Past - Part 3_files/
summary: Looking at all this, one may wonder what Chinese bronze "mirrors" have to do with all this technology. It is simple: if before our times and technologies anyone tried to claim there could be information on authentic bronze "mirror" in volumes surpassing all our current media, can you imagine people's reaction to such statement?
status: published

### Translated from:

[https://pro-vladimir.livejournal.com/29127.html](https://pro-vladimir.livejournal.com/29127.html)

**Russian:**

Бронза и DVD, что между ними может быть общего? [http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

Носители информации прошлого. [http://pro-vladimir.livejournal.com/28598.html](http://pro-vladimir.livejournal.com/28598.html)

Носители информации прошлого. Часть 2. [http://pro-vladimir.livejournal.com/28918.html](http://pro-vladimir.livejournal.com/28918.html)

**English:**

Bronze and DVDs, what could the two have in common? [http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

Storage media of the past. Part 1:  [http://pro-vladimir.livejournal.com/28598.html](http://pro-vladimir.livejournal.com/28598.html)

**English:**

Storage media of the past. Part 2. [http://pro-vladimir.livejournal.com/28918.html](http://pro-vladimir.livejournal.com/28918.html)

**Russian:**

Для дальнейшего раскручивания версии, про носители информации прошлого, не плохо бы немного присмотрется к носителям информации текущего настоящего. Честно, ранее особо не вникал с суть того чем мы ныне пользуемся. просто отмечал, что объём носителей непрерывно растет. Да помнил названия некоторых форматов без вникания, а чем собственно они отличаются друг от друга. если коротко.

**English:**

For further explanation of the concept of storage media of the past, it would not be bad to look a little at the storage media of the present. To be honest, I did not go into the essence of what we now use. I simply noted that capacity is constantly growing. To make a long story short, I mentioned the names of some formats without getting into how they differ from each other.

**Russian:**

> Blu-ray основывается на технологии сине-фиолетового лазера. Для хранения на таких дисках подходит абсолютно любая информация. Начиная от текстовых файлов и заканчивая новейшими играми и архивами фильмов в высоком качестве.  В технологии Blu-ray для чтения и записи используется сине-фиолетовый лазер с длиной волны 405 нм. Обычные DVD и CD используют красный и инфракрасный лазеры с длиной волны 650 нм и 780 нм соответственно.  Такое уменьшение позволило сузить дорожку вдвое по сравнению с обычным DVD-диском (до 0,32 мкм) и увеличить плотность записи данных.

Правда всё понятно?

**English:**

> Blu-ray is based on blue-violet laser technology. Any kind of content can be stored on Blu-ray discs. From text files to the latest games to high quality film archives.  Blu-ray technology uses a 405nm blue violet laser to read and write. Conventional DVDs and CDs use red and infrared lasers at 650nm and 780nm, respectively.  This reduction made it possible to halve the size of the track compared to a conventional DVD (reducing track size to 0.32 μm) and therefore to increase the density of the data recorded.

Does it really make sense?

**Russian:**

Прошу не судить строго. Далее просто подборка свойств описания некоторых основных форматов. Да пару слов про еще не ворвавшиеся в нашу жизнь новые форматы. Материала разной степени объёмности в сети более чем. Ну как пример: [http://www.cd-copy.ru/new_articles_CD.php](http://www.cd-copy.ru/new_articles_CD.php) или тут: [http://www.intuit.ru/studies/courses/3460/702/lecture/14152?page=6](http://www.intuit.ru/studies/courses/3460/702/lecture/14152?page=6). или тут: [http://essserbij.ucoz.ru/news/tekhnologii_opticheskikh_diskov_2_chast/2012-12-28-453](http://essserbij.ucoz.ru/news/tekhnologii_opticheskikh_diskov_2_chast/2012-12-28-453)

**English:**

Please do not judge strictly. Below is just a selection of description properties of some basic formats. And a few words about the new formats which have not yet burst into our lives. There is more than enough material of varying degrees of volumetricity on the web. Well, as an example: [http://www.cd-copy.ru/new_articles_CD.php](http://www.cd-copy.ru/new_articles_CD.php) Or here: [http://www.intuit.ru/studies/courses/3460/702/lecture/14152?page=6](http://www.intuit.ru/studies/courses/3460/702/lecture/14152?page=6). Or here: [http://essserbij.ucoz.ru/news/tekhnologii_opticheskikh_diskov_2_chast/2012-12-28-453](http://essserbij.ucoz.ru/news/tekhnologii_opticheskikh_diskov_2_chast/2012-12-28-453)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty39XOx.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image001.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image001.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image001.gif)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty3_004.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image002.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image002.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image002.gif)

**Russian:**

*Рис. 1 - Компакт-диск*

**English:**

*Figure 1 - Compact disc*

**Russian:**

> Дорожка записи с питами находится внутри диска и предохранена от повреждения, неправлена по спирали от центра к краю диска. Внутренний диаметр зоны записи равен 45 мм, наружный - 116мм.

**English:**

> The write track with pits starts at the inside the disc and is protected from damage by being written spirally from the center towards the edge of the disc. The inside diameter of the recording area is 45 mm, the outside diameter is 116 mm.

**Russian:**

> Всего концентрических дорожек с информацией около 20 000, общая длина примерно 5 км. На каждом миллиметре по радиусу умещается около 700 витков спирали - примерно в 70 раз больше, чем на обычной пластинке. Информация с диска считывается бесконтактным способом с помощью лазерного луча. Вопрос о сохранности пластинки, как уберечь её от "запиливания" отпадает сам собой.

**English:**

> In total, there are about 20,000 concentric information tracks, with a total length of about 5 km. Each millimeter in radius contains about 700 spiral turns - about 70 times more than on a regular record. The information on the disk is read without contact by means of a laser beam. There is no question of how to save the record, how to protect it from being "filed".

**Russian:**

> Компакт-диск не боится пыли, отпечатков пальцев (в разумных пределах), мелких царапин. Все дело в том, что если на информационном слое диска луч фокусируется в пятно размером около одного микрона (1 мкм), то на наружной поверхности диска размер его возрастает в тысячу раз - до 1 мм и мелкие дефекты на поверхности диска луч просто не замечает (рис. 2). Информационная емкость КД огромна - 5 миллиардов бит (на обычной пластинке - десятки миллионов бит). Все эти биты разделены на группы по 16 бит, представляющие собой единицу кодирования звука.*

**English:**

> Compact disks are not affected by dust, fingerprints (within reasonable limits) or small scratches. The reason is that when 1 micron diameter laser beam focuses on the sub-surface information-bearing layer of a disk, it first passes through the transparent outer surface of a disk.  At the outer surface its spot size is a thousand times larger - up to 1 mm. Small defects on the disk surface simply do not affect the 1mm diameter beam (fig. 2). The information capacity of a CD is huge: 5 billion bits. A typical music record has tens of millions of bits. All of the CD's bits are divided into groups of 16 bits, which represent the unit of audio coding.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty3_005.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image003.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image003.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image003.gif)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty3_006.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image028.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image028.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image028.gif)

**Russian:**

Можем ли вообще допустить мысль, что в допотопные времена мог быть такой носитель информации? С трудом.

**English:**

Can we even consider the idea that there could have been such a medium in prehistoric times? Hardly.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEQuIsqOAnrW6DLF0rOZ4ax1k92NVX1CiEzNskFo.html)](Media of the Past - Part 3_files/03-31.jpg)
-->

[![Image no longer available](http://www.intuit.ru/EDI/04_03_14_18/1393881485-12448/tutorial/630/objects/3/files/03-31.jpg#clickable)](http://www.intuit.ru/EDI/04_03_14_18/1393881485-12448/tutorial/630/objects/3/files/03-31.jpg)

**Russian:**

Алгоритм работы накопителя CD-ROM

**English:**

CD-ROM drive algorithm

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEQuIsqOAnrW6DLF0rOZ4ax1k92NVX1CiEzN_002.html)](Media of the Past - Part 3_files/03-29.jpg)
-->

[![Image no longer available](http://www.intuit.ru/EDI/04_03_14_18/1393881485-12448/tutorial/630/objects/3/files/03-29.jpg#clickable)](http://www.intuit.ru/EDI/04_03_14_18/1393881485-12448/tutorial/630/objects/3/files/03-29.jpg)

**Russian:**

*Полупроводниковый лазер генерирует маломощныйинфракрасный луч, который попадает на отражающее зеркало.*
 
**English:**

*Semiconductor laser generates a low-power infrared beam that hits a reflecting mirror.*

**Russian:**

> Серводвигатель по командам, поступающим от встроенного микропроцессора, смещает подвижную каретку с отражающим зеркалом к нужной дорожке на компакт-диске. Отраженный от диска луч фокусируется линзой, расположенной под диском, отражается от зеркала и попадает на разделительную призму. Разделительная призма направляет отраженный луч на другую фокусирующую линзу. Эта линза направляет отраженный луч на фотодатчик, который преобразует световую энергию в электрические импульсы. Сигналы с фотодатчика декодируются встроенным микропроцессором и передаются в компьютер в виде данных.*

**English:**

> The servomotor moves the moving carriage with the reflecting mirror to the desired track on the CD according to the commands from the built-in microprocessor. The beam reflected from the disk is focused by a lens placed under the disk, reflected from the mirror and hits the separating prism. The separation prism directs the reflected beam to the other focusing lens. This lens directs the reflected beam to the photosensor, which converts the light energy into electrical pulses. The signals from the photosensor are decoded by the built-in microprocessor and transmitted to the computer as data.

**Russian:**

Сохранение культурного наследия как задача информационной безопасности

[http://rc-it.ru/%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D1%83%D0%BB%D1%8C%D1%82%D1%83%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BD%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D1%8F-%D0%BA%D0%B0%D0%BA/](http://rc-it.ru/%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D1%83%D0%BB%D1%8C%D1%82%D1%83%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BD%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D1%8F-%D0%BA%D0%B0%D0%BA/)

**English:**

Preservation of cultural heritage as a challenge to information security

[http://rc-it.ru/%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D1%83%D0%BB%D1%8C%D1%82%D1%83%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BD%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D1%8F-%D0%BA%D0%B0%D0%BA/](http://rc-it.ru/%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D1%83%D0%BB%D1%8C%D1%82%D1%83%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BD%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D1%8F-%D0%BA%D0%B0%D0%BA/)

**Russian:**

> Цель исследования: Рассмотреть угрозы публичной информации (культурное наследие) и сформировать методы возможной защиты информации в случае локальных и глобальных катастроф повлекших за собой разрушение инфраструктуры.

**English:**

> Purpose of the study: To consider threats to public information (cultural heritage) and develop methods of possible information protection in case of local and global disasters resulting in destruction of infrastructure.

**Russian:**

Хороший обзор на предмет долговечности текущих носителей через призму что стоит о себе оставить, кроме надписи "здесь были люди".

| Тип носителя Срок работы |
|- |- |
| Дискета 5 лет | наименее устойчивый хранитель информации |
| CD/DVD 15-25 лет | молекулярные изменения внешнего слоя разрушат диск существенно раньше этого предела |
| Винчестер 5-10 лет | в узком режиме внешних условий. |
| USB-флеш 5-10 лет | требует регулярной верификации информации и перезаписи |
| Карта памяти 5-10 лет | требует регулярной верификации информации и перезаписи |
| Пленка формата VHS 10-15 лет | требует специальных условий хранения и регулярной перемотки пленки |
| MiniDV/VHS 10-15 лет | требует специальных условий хранения и регулярной перемотки пленки |
| Магнитная лента 20-30 лет | требует специальных условий хранения и регулярной перемотки пленки |
| Диск DVD-RAM 20-30 лет | при идеальных условиях |

**Russian:**

Выводы: Ни один из вышеперечисленных видов информационных носителей не подходит к условиям долговременного хранения данных (более 100 лет) при тяжелых физических условиях. Задача хранения информационного хранилища «Всемирного наследия» не может быть решена с помощью носителей, на которых запись организована магнитным путем или с использованием электронных компонентов, дисков CD и DVD и нанотехнологических элементов.

**English:**

A good review on the longevity of current media through the lens of what's worth leaving behind other than the inscription: "there were people here".

| Media type | Working time |
|- |- |
|5 year old floppy disk | the least stable information storage |
|CD/DVD 15-25 years | molecular changes in the outer layer will destroy the disc significantly sooner than this limit |
|Winchester 5-10 years | in a tight environment mode. |
|USB flash 5-10 years | requires regular verification of information and overwriting |
|Memory card 5-10 years old | requires regular verification of information and overwriting |
|VHS film 10-15 years | requires special storage conditions and regular rewinding of the film |
|MiniDV/VHS 10-15 years | requires special storage conditions and regular rewinding of the film |
|Magnetic tape 20-30 years | requires special storage conditions and regular rewinding of the tape |
|DVD-RAM 20-30 years | under ideal conditions |

Conclusions: None of the above-mentioned types of information carriers is suitable for long-term data storage (more than 100 years) under harsh physical conditions. The task of storing the "World Heritage" information repository cannot be solved by media on which recording is organised magnetically or using electronic components, CDs and DVDs and nanotechnological elements.

**Russian:**

Есть еще одна проблема хранения информации на цифровых носителях. Запись данных на цифровых носителях выполняется кодированием, и как результат необходимость расшифровка данных после прочтения. Перечислим возникающие программно-аппаратные и семантические проблемы длительного хранения данных на цифровых носителях:

1. Совершенствование и изменение аппаратной конфигурации цифровых устройств приводит к резкому моральному старению устройств и их исчезновению. Современные ноутбуки поставляются уже без 3,5-дюймовых дисководов для чтения 3,5 дюймовых дискет. До этого были 5-дюймовые и 7,5-дюймовые дискеты. Попробуйте найти сейчас работоспособный компьютер способный читать 5-дюймовые дискеты, не говоря уже о 7,5-дюймовых дискетах. Единственное место в мире где в работоспособном состоянии хранятся все поколения информационной техники -- Библиотека Конгресса США, но она единственная;

2. Данные на дисках были записаны с помощью кодировочных таблиц и ассоциированных с данными файловых системах. Только за предыдущие 20 лет накоплено более 100 структур файловых систем (HPFS, NTFS, FAT, FAT32, ext2, ext3, ...). Представьте проблемы исследователя будущего через 300 лет взявшегося изучить наследие предыдущего человечества. Расшифровать файл изображения *.jpg располагающийся в каталоге на диске с файловой системой NTFS будет задачей более трудной, чем расшифровка Фестского диска. На Фестском диске мы видим изображение, а в случае с файлом надо еще понять, что тот информационный блок, который мы считали, является изображением (его еще надо уловчиться считать). В этом случае необходимо на отдельном носителе в виде простейших пиктограмм отобразить методы чтения, схему считывающего устройства, объяснить структуру NTFS, объяснить какие типы информации записаны на диск и как их идентифицировать, объяснить структуру каждого типа файлов. Такие кодовые таблицы будут иметь размер и сложность многократно превышающую материал записанный на диске;

3. При потере единственной кодовой таблицы возникает проблема невозможности чтения всего диска. Потеря таблицы с описанием файловой структуры NTFS делает маловероятным распознание информации расположенной на диске.

Таким образом, информационная система долговременного хранения (сотни лет и больше) не может быть записана в цифровом виде. Записывать информацию информационного хранилища «Всемирного наследия» необходимо в аналоговом виде. Наиболее приемлемый аналоговый вид -- изображения размещенные в однозначной последовательности.

**English:**

There is another problem with storing information on digital media. Writing data on digital media is done by coding, and as a result the need to decrypt the data after reading. Let's list arising hardware-software and semantic problems of long-term data storage on digital media:

1. Improvements and changes in the hardware configuration of digital devices lead to a dramatic obsolescence of devices and their disappearance. Today's laptops no longer come with 3.5-inch floppy drives for reading 3.5-inch floppy disks. Before that there were 5 inch and 7.5 inch floppy drives. Try to find a working computer these days that can read a 5 inch floppy diskette, let alone a 7.5 inch floppy diskette. The only place in the world where all generations of information technology are kept in working condition is the Library of Congress, but it is the only place;

2. Data on disks has been written using encoding tables and file systems associated with the data. Over 100 file system structures (HPFS, NTFS, FAT, FAT32, ext2, ext3, etc) have been accumulated for the previous 20 years alone. Imagine the problems of a researcher of the future in 300 years who undertakes to study the legacy of the previous humanity. Decrypting an *.jpg image file stored in a directory on a drive with an NTFS filesystem will be a much harder task than decrypting the Phaestos disk. On a Phaestos disk, we see an image; in the case of a file, we still have to figure out that the information block that we were counting is an image (we still have to figure it out right away). In this case we need a separate medium in the form of simple pictograms to display methods of reading, the scheme of the reader, explain the structure of NTFS, explain what types of information are recorded on the disk and how to identify them, explain the structure of each type of files. Such code tables will have a size and complexity many times greater than the material recorded on the disk;

3. If you lose a single codepage, the whole disk cannot be read. Loss of the table describing NTFS file structure makes it unlikely to recognize information located on the disk.

**English:**

Thus, the information system of long-term storage (hundreds of years and more) cannot be recorded in digital form. It is necessary to record the information of the World Heritage information repository in analogue form. The most acceptable analogue form - images placed in a single-valued sequence.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbERaNHLeNpKW-1WIHqDmgwRtwgIEmQgBUlmyYujH.jpeg#clickable)](Media of the Past - Part 3_files/dvd.jpg)
-->

[![](http://www.ultradisk.ru/info/dvd.jpg#clickable)](http://www.ultradisk.ru/info/dvd.jpg)

**Russian:**

Диск договременного хранения данных «Rosettа»

В США прошла научно-практическая конференция «Библиотека на 10 тысяч лет», устроенная в Стэнфордском университете фондом Long Now Foundation и посвященная обсуждению новых способов хранения информации. Ученых, библиотекарей, технологов и антропологов волновал, главным образом, следующий вопрос: как можно было бы пронести через тысячелетия культурные ценности нашего века, причем сделать это так, чтобы они гарантированно были понятны даже самым отдаленным нашим потомкам. В некотором смысле, обсуждались пути создания своего рода нового «Розеттского камня», когда-то принесшего нам сокровища древнеегипетской культуры, а в новом своем воплощении дававшим бы возможность окунуться в культурное наследие эпохи без привязки к каким-либо конкретным компьютерным технологиям. Наибольший интерес у участников конференции вызвала технология с примечательным названием «Розетта-диск» (HD-Rosetta), предлагаемая американской компанией NORSAM Technologies.

**English:**

Rosetta" data storage drive

In the USA there was a scientific conference "Library for 10 thousand years" organized at Stanford University by the Long Now Foundation and devoted to the discussion of new ways of information storage. Scientists, librarians, technologists and anthropologists were mainly concerned with the following question: how could we carry the cultural values of our century through millennia, and do it in such a way that they are guaranteed to be understood even by our most distant descendants. In a sense, they were discussing the ways to create a kind of a new "Rosetta Stone" that would bring us the treasures of Ancient Egyptian culture, and in its new incarnation would give the opportunity to plunge into the cultural heritage of the era without being linked to any specific computer technology. The technology with a remarkable name "Rosetta" (HD-Rosetta), offered by the American company NORSAM Technologies, was of the greatest interest among the participants of the conference.

[Image no longer available](http://rc-it.ru/wp-content/uploads/2013/12/8.jpg)

**Russian:**

Разработанная учеными Лос-Аламосской национальной лаборатории США, технология хранения «Розетта» строится на базе небольшого, диаметром два дюйма никелевого диска, на который методом микрогравировки с помощью ионного луча наносятся аналоговые тексты и изображения.

**English:**

Developed by scientists at Los Alamos National Laboratory in the United States, Rosetta storage technology is based on a small, two-inch diameter nickel disk onto which analog texts and images are micro-engraved with an ion beam.

[Image no longer available](http://rc-it.ru/wp-content/uploads/2013/12/10.jpg)

**Russian:**

Результат ионной бомбардировки диска (увеличение 1000 крат)

**English:**

Result of disk ion bombardment (1000x magnification)

**Russian:**

На одном диске можно уместить до 350 тысяч страниц текста или до 100 тысяч изображений с разрешающей способностью 4096 x 4096 пиксель. Технология записи допускает нанесение изображений как в оттенках серого, так и в цвете.*

**English:**

A single drive can hold up to 350,000 pages of text or 100,000 images at a resolution of 4096 x 4096 pixels. Recording technology allows for both grayscale and color imaging.

**Russian:**

Конечно можно верить, что, мы первые и единственные прошли путь до этих технологий. И можно абсолютно не верить, что это мог сделать кто-то еще задолго до нас.

**English:**

Of course, we can believe that we were the first and only ones to have made it to this technology. And you can absolutely not believe that someone else could have done it long before us.

**Russian:**

На горизонте и такие вот технологии: [http://www.xakep.ru/post/61438/](http://www.xakep.ru/post/61438/)

**English:**

There's technology like this on the horizon, too: [http://www.xakep.ru/post/61438/](http://www.xakep.ru/post/61438/)

**Russian:**

> Многослойная запись увеличит емкость HDD до 100 ТБ и больше //17.10.2013 Версия для печати Комментарии
>
> В последние пару лет сообщалось о нескольких «прорывных» технологиях, которые обеспечат дальнейший рост емкости жестких дисков. Например, полгода назад компания Seagate продемонстрировала технологию термомагнитной записи HAMR (heat-assisted magnetic recording), которая способна обеспечить плотность информации на поверхности жесткого диска 1 триллион бит на квадратный дюйм. В ближайшие десять лет, заявляет компания, благодаря усовершенствованию этой технологии станет возможным выпуск винчестеров стандартного форм-фактора 3,5 дюйма и емкостью до 60 терабайт. Для сравнения, в современных самых вместительных 3-терабайтных HDD плотность записи не превышает 620 млрд бит на квадратный дюйм и нет особых перспектив для ее увеличения из-за ограничений используемой технологии перпендикулярной магнитной записи (PMR).
>
> Инженеры из Флоридского международного университета разработали новую технологию многослойной 3D-записи, которая превосходит даже HAMR по потенциально возможной плотности информации. Это первая по-настоящему трехмерная технология магнитной записи. Как показано на иллюстрации, запись производится на три магнитных слоя разной толщины, разделенных изолятором.
>
> Благодаря тройному слою каждый магнитный участок способен хранить не 1 бит, а 3 бита информации, принимая одно из восьми возможных состояний.
>
> Прототип устройства ученые испытали в лабораторных условиях и убедились, что могут надежно считывать информацию с трехслойного накопителя.

**English:**

> Multilayer Recording to Extend HDD Capacity to 100TB and Beyond (17.10.2013)
> 
> Several "breakthrough" technologies have been reported over the past couple of years that will further increase hard drive capacity. Six months ago, for instance, Seagate unveiled Heat-Assisted Magnetic Recording (HAMR), a technology capable of delivering an information density of 1 trillion bits per square inch on the surface of a hard drive. Over the next decade, the company said, advances in this technology will enable standard 3.5-inch form factor hard drives with capacities up to 60 terabytes. By comparison, today's highest capacity 3 terabyte HDDs have a recording density of less than 620 billion bits per square inch and have little room for improvement due to the limitations of current Perpendicular Magnetic Recording (PMR) technology.
>
> Engineers at Florida International University have developed a new 3D multilayer recording technology that surpasses even HAMR in terms of potential information density. It is the first truly 3D magnetic recording technology. As shown in the illustration, recording is performed on three magnetic layers of varying thickness, separated by an insulator.
>
> Thanks to the triple layer, each magnetic patch is capable of storing 3 bits of information instead of 1 bit, taking one of eight possible states.
>
> Scientists tested the prototype device in laboratory conditions and verified that they can reliably read information from the three-layer storage device.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbETVF6dmKvSbIKtYDLfitM8lr-31TR6LSRZj_002.html)](Media of the Past - Part 3_files/hdd1.jpg)
-->

[![Image no longer available](http://www.xakep.ru/post/61438/hdd1.jpg#clickable)](http://www.xakep.ru/post/61438/hdd1.jpg)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbETVF6dmKvSbIKtYDLfitM8lr-31TR6LSRZjAtnA.html)](Media of the Past - Part 3_files/hdd2.jpg)
-->

[![Image no longer available](http://www.xakep.ru/post/61438/hdd2.jpg#clickable)](http://www.xakep.ru/post/61438/hdd2.jpg)

**Russian:**

Многослойные оптические диски FMD

**English:**

Multilayer Optical Disks - FMD

**Russian:**

*Настоящим прорывом в технологии записи информации следует считать разработку американской фирмой C3D трехмерного флуоресцентного дискового носителя. При этом запись осуществляется не только по площади диска (двухмерная запись), но и по его глубине на нескольких слоях. Ограничивающим фактором обычной двумерной записи является то, что её невозможно реализовать при наличии более чем двух информационных слоев. Начинают сказываться интерференция, рассеяние, шумы, перекрестные помехи, которые возникают из-за того, что падающий и отраженный пучки имеют одну и ту же длину волны и являются когерентными. При увеличении числа слоев полезный сигнал уже невозможно будет отделить от шумов.

**English:**

*The development of a three-dimensional fluorescent disc carrier by the American company C3D should be regarded as a real breakthrough in the technology of information recording. In this case, the recording is carried out not only on the area of the disk (two-dimensional recording), but also on its depth on several layers. A limiting factor of conventional 2D recording is that it cannot be implemented with more than two layers of information. Interference, scattering, noise, crosstalk, which arise due to the fact that the incident and reflected beams have the same wavelength and are coherent, begin to affect. As the number of layers increases, the useful signal can no longer be separated from the noise.

**Russian:**

Трехмерный диск фирмы C3D использует принципиально иную технологию записи информации, основанную на свойствах излучения флуоресцентных материалов и допускает наличие очень большого числа слоев (рис. 4).

**English:**

The C3D 3D disk uses a fundamentally different technology to record information, based on the emission properties of fluorescent materials and allows the presence of a very large number of layers (Fig. 4).

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty3_002.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image048.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image048.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image048.gif)

**Russian:**

*Рис. 4 - Зависимость ухудшения качества сигнала от количества слоев*

**English:**

*Fig. 4 - Dependence of signal quality degradation on the number of layers*

**Russian:**

FM-диск является абсолютно прозрачным и не имеет отражающего слоя. В основе работы флуоресцентеных дисков лежит явление фотохроматизма, которое заключается в изменении физических свойств (в частности появление флуоресцентного свечения) некоторых химических веществ под воздействием лазера.

В наибольшей степени подобным свойством обладают вещества из группы фульгидов, которые были открыты и изучены советскими учеными ещё много лет назад. Наиболее подходящим представителем данной группы веществ является фотохром.

**English:**

FM disk is completely transparent and has no reflective layer. The fluorescent discs are based on the phenomenon of photochromatism, which consists in a change in the physical properties (in particular the appearance of fluorescent glow) of some chemical substances under the influence of laser.

Fulgide group substances - which were discovered and studied by Soviet scientists many years ago - possess this property to the greatest extent. The most suitable representative of this group of substances is photochromium.

**Russian:**

Сам FM-диск представляет собой слоеный пирог, каждый слой которого является прозрачным и имеет спиральные канавки (по аналогии с обычным компакт-диском их можно назвать питами), заполненные флуоресцентным материалом. При возбуждении такого материала лазерным лучом он начинает излучать как когерентный, так и некогерентный свет. Информация записывается только некогерентным светом. При считывании возбужденный фотохром излучает свет, сдвигая спектр падающего не него излучения в сторону красного цвета на определенную величину (в пределах 30...50 нм), что позволяет легко различить сигнал лазера и свет, излучаемый материалом диска. в результате удается избежать ухудшения характеристик сигнала из-за явлений, связанных с когерентностью, и его качество при увеличении числа слоев снижается незначительно. Разработчики утверждают, что даже при количестве слоев более сотни не будет происходить сильного искажения сигнала.

**English:**

The FM disk itself is a layered pie, each layer of which is transparent and has spiral grooves (by analogy with a conventional CD they can be called pits) filled with fluorescent material. When this material is excited by a laser beam, it starts emitting both coherent and incoherent light. Only incoherent light records information. During reading the excited photochrome emits light, shifting the spectrum of incoming radiation towards red by a certain value (in the range of 30-50 nm) that allows easily distinguish the laser signal from the light emitted by the disk material. The developers claim that even with the number of layers exceeding a hundred there will be no significant distortion of the signal.

**Russian:**

При считывании лазер фокусируется на определенном слое и возбуждает его флуоресцентные элементы (рис. 5), после чего это свечение улавливается фотодетектором. Согласно теоретическим выкладкам, при использовании синего лазера (длина волны 480 нм) становится возможным увеличение плотности записи информации до нескольких Гбайт на один FM-диск.*

**English:**

During reading, the laser focuses on a certain layer and excites its fluorescent elements (Fig. 5), and then this glow is captured by a photodetector. According to theoretical calculations, when using a blue laser (wavelength 480 nm) it becomes possible to increase the density of information recording up to several Gbytes per FM-disk.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbET5m-7wBXOQEk9eUYxTH2FM3OLHIOwcWty3_003.html)](Media of the Past - Part 3_files/new_articles_CD_clip_image049.gif)
-->

[![Image no longer available](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image049.gif#clickable)](http://www.cd-copy.ru/artimages/new_articles_CD_clip_image049.gif)

**Russian:**

*Рис. 5 - Принцип работы FM-диска*

**English:**

*Figure 5 - Operating Principle of an FM disk*

**Russian:**

Важная особенность формата FMD заключается в возможности параллельного считывания сразу с нескольких слоев многослойного диска. При этом, если записывать последовательность бит не вдоль дорожки, а в глубь по слоям, то можно значительно повысить скорость выборки данных. Именно за эту особенность FM-диски и прозвали трехмерными.

**English:**

An important feature of the FMD format is the ability to read from multiple layers of a multilayer disk in parallel. In this case, if the bit sequence is not written along the track, but deep into the layers, it is possible to significantly increase the data sampling rate. It is because of this feature that FM disks are called 3D disks.

**Russian:**

Что касается технологии производства FMD, то здесь очень много сходства с производством обычных компакт-дисков. Из прозрачного поликарбоната отдельно изготовляются информационные слои, которые затем связываются между собой. Единственное что очень важно при производстве FM-диска, так это получение точной формы пита, так как в дальнейшем он заполняется флуоресцентным веществом. Для этих целей используется никелевая матрица (штамп).*

**English:**

As far as the FMD production technology is concerned, there are very many similarities with the production of conventional CDs. Information layers are separately produced from transparent polycarbonate, which are then bonded together. The only thing that is very important in the production of FM-Disc is getting the exact shape of the pita, as it is then filled with a fluorescent substance. A nickel matrix (die) is used for this purpose.

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEWcfL0G77ei9mF6btBR_Dt3B0A3W8wVMGBIOyP_I.gif#clickable)](Media of the Past - Part 3_files/10003_11_fmd.gif)
-->

[![](http://ic1.static.km.ru/sites/default/files/imagecache/590/pc/10003_11_fmd.gif#clickable)](http://ic1.static.km.ru/sites/default/files/imagecache/590/pc/10003_11_fmd.gif)

**Russian:**

Еще одна статья про устройство многослойных дисков: [http://www.ixbt.com/storage/fmd-tech.html](http://www.ixbt.com/storage/fmd-tech.html)

**English:**

Another article about the construction of multilayer disks: [http://www.ixbt.com/storage/fmd-tech.html](http://www.ixbt.com/storage/fmd-tech.html)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEcPSwVCjHj3vu67j4CzqEektW5kFDpOtlOUW_002.gif#clickable)](Media of the Past - Part 3_files/process.gif)
-->

[![](http://www.ixbt.com/storage/fmd-tech/process.gif#clickable)](http://www.ixbt.com/storage/fmd-tech/process.gif)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEcPSwVCjHj3vu67j4CzqEektW5kFDpOtlOUWOVKV.gif#clickable)](Media of the Past - Part 3_files/figfmray.gif)
-->

[![](http://www.ixbt.com/storage/fmd-tech/figfmray.gif#clickable)](http://www.ixbt.com/storage/fmd-tech/figfmray.gif)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEcPSwVCjHj3vu67j4CzqEektW5kFDpOtlOU_002.jpeg#clickable)](Media of the Past - Part 3_files/dvd2.jpg)
-->

[![](http://www.ixbt.com/storage/fmd-tech/dvd2.jpg#clickable)](http://www.ixbt.com/storage/fmd-tech/dvd2.jpg)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEcPSwVCjHj3vu67j4CzqEektW5kFDpOtlOUWOVK.jpeg#clickable)](Media of the Past - Part 3_files/fmdisk.jpg)
-->

[![](http://www.ixbt.com/storage/fmd-tech/fmdisk.jpg#clickable)](http://www.ixbt.com/storage/fmd-tech/fmdisk.jpg)

**Russian:**

Глядя на всё это, можно задаться вопросом, а причём тут все эти технологии и Китайские бронзовые "зеркала". Да всё просто, если бы кто ранее попробовал, до текущих дней и технологий, выдвинуть версию, что на любом подлинном бронзовом "зеркале" может находится информация по объёмам перекрывающая все наши текущие носители. Да еще и во много раз, вы можете себе представить реакцию окружающий на такое заявление? Она и сейчас то, реакция, через большой скептический прищур.

**English:**

Looking at all this, one may wonder what Chinese bronze "mirrors" have to do with all this technology. It is simple: if before our times and technologies anyone tried to claim there could be information on authentic bronze "mirror" in volumes surpassing all our current media, can you imagine people's reaction to such statement? Even now the reaction is delivered through a big, skeptical squint.

**Russian:**

А ведь сами китайцы "свои" зеркала называют прозрачными. А у нас любой дисковый носитель, чтоб считать информацию, его надо бы просветить. В отраженном солнечном свете с этого зеркала видно стартовую заставку. Оную достаточно долго не могли повторить ни как. Сейчас научились делать некое подобие.

**English:**

The Chinese themselves call "their" mirrors transparent. But in our country any disk drive must be illuminated in order to read the information on it. In the reflected sunlight from this mirror we see the start-up desktop, the wallpaper. It was impossible to reproduce it for quite a long time. Now we have learned to make some kind of analogy.

**Russian:**

А если лазером посветить на этот диск? Может там еще много чего обнаружится? что если структура диска располагает сильно сжатой информацией, размерность "пит" которой на порядки меньше, а плотность их гораздо выше?

**English:**

What if we shine a laser light on this disk? What if the structure of the disk has highly compressed information, the "pits" of which are orders of magnitude smaller, and their density is much higher (than ours)?

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEeGDoXSJHJLlywTthla4Y468fBm0lOKnM7R-frs.jpeg#clickable)](Media of the Past - Part 3_files/fxCjR.jpg)
-->

[![](http://s5.uploads.ru/fxCjR.jpg#clickable)](http://s5.uploads.ru/fxCjR.jpg)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbETiFTiALjDFu0iy4eqVLtu1WjtIzdAjMLkYbyvk.jpeg#clickable)](Media of the Past - Part 3_files/KBPYl.jpg)
-->

[![](http://s4.uploads.ru/KBPYl.jpg#clickable)](http://s4.uploads.ru/KBPYl.jpg)

<!-- Local
[![](Media of the Past - Part 3_files/jxpzBIWMCFjfpfjN7mhbEXt217PIwrYL0xEEb47ejhyQVf01vcCrmu8nNBg.jpeg#clickable)](Media of the Past - Part 3_files/pho00447.jpg)
-->

[![](http://www.chiculture.net/20509/picture/pho00447.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00447.jpg)

© All rights reserved. The original author retains ownership and rights.

