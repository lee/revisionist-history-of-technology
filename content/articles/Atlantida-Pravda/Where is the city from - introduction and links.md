title: Where is the city from? Evidence for the Creation of St Petersburg's History 
date: 2013-12
modified: 2022-04-24 11:05:45
category: Atlantida-Pravda
tags: St Petersburg
slug: 
authors: ZigZag
summary: ZigZag's introduction to his series on the anomalies in St Petersburg's history, followed by links to each episode.
status: published
originally: Mysteries of history. Controversial facts and speculations .: Where does the city come from.html

### Translated from: 
[http://atlantida-pravda-i-vimisel.blogspot.com/2013/12/blog-post.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/12/blog-post.html)

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

**In response to the new Google Blogs policy, I hereby declare that all my personal data, photographs, drawings, correspondence, etc., are subject to my copyright (under the Berne Convention). FOR THE COMMERCIAL USE of all the aforementioned objects of copyright in each case, my written permission is required.**

**Google Blogs is now a public company. That is why all users of this social network are encouraged to post such a "privacy notice" on their pages, otherwise (if the notice is not published on the page at least once), you automatically authorize any use of data from your page, your photos and information published in posts on the wall of your page.**

**Anyone who reads this text can copy it to their wall in Google Blogs. After that, you will be protected by copyright laws. This communiqué informs Google Blogs that disclosure, copying, distribution of my personal information FOR COMMERCIAL PURPOSES, or any other illegal activity in relation to my profile on a social network is strictly prohibited. NON-COMMERCIAL use and copying of my photographs, works and other recordings I authorize unconditionally, with the obligatory indication of authorship.**

**What is the PAST?** This is what it really was.

**What is HISTORY?** This is an interpretation of the past by contemporaries. Notice, not by eyewitnesses who lived then, but by people living today.

It is impossible to see the truth while everyone has their own truth, but... you need to strive for it regardless. So it seems to me.

And now, as they say, closer to the body.

We say that there is no time for history, for understanding the essence of things and the meaning of life, for understanding their purpose in this world and on this earth. He, at the same time, is eaten up by work, life, well-being. And if you think about what is behind these words? Without looking back, you don't know where you came from and where to go.

Work is work on oneself, on improving the personality, moral principles and qualities, and not slave labor *for a penny* for the boss, who sometimes himself does not know what the meaning of this very work is. All the same, everyone will be let down under the slogan - there are no irreplaceable! And time is running out.

Life is what surrounds us outside of work and should help a person live, work, and improve himself. To load the mental and physical apparatus, to be needed by the family and people around, and not mindlessly lying on the couch surrounded by newfangled gadgets, and dull contemplation of a zombie box, where every 20 minutes they hint about what you have not yet bought. And time is running out.

Well-being is the presence of shelter and food, responsibility for family and home, and not an all-consuming desire to become richer than a neighbor, thereby wiping his nose. And time is running out.

And what do we have at the end of life??? Each one has its own luggage. But will the descendants remember him, the luggage, and will they remember us?

Truly happy is the one who does not rush about in search of the meaning of life, but lives with this meaning, and enjoys life. The secret has long been known, but not everyone can do it yet:

- patience;
- trust;
- moderation;
- tolerance;
- detachment;
- altruism;
- integrity;
- modesty;
- courage;
- peacefulness;
- beneficence;
- wisdom.

*ZigZag*

[Introductory remarks to the article: "Where is the city from?" History, who are you, a high society lady or a floozy?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html)]

[Chapter 1. Old maps of Peter.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html)

[Chapter 2. Ancient tale in the north of Europe.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/2.html)

[Chapter 3. Unity and monotony of monumental structures scattered throughout the world.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html)

[Chapter 4. Capitol without a column... well, no way, why?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_15.html)

[Chapter 5. One project, one architect or cargo cult?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html)

[Chapter 6. Bronze Horseman, who are you really?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/6.html)

[Chapter 7. Thunder stone or submarine in the steppes of Ukraine?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/7.html)

[Chapter 8. Falsification of most of the monuments of St. Petersburg.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/8.html)

[Chapter 9. Peter the First - an ambiguous personality in the history of the whole Europe.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/9.html)

[Chapter 10. For what to say thank you, Tsar Peter?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/i.html)

[Chapter 10-1. This "happy" tsarist era or the House of Holstein in Russia.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/10/blog-post_1.html)

[Chapter 10-2.  Why was the chain mail and cuirass replaced with stockings and a wig?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/09/blog-post_5.html)

[Chapter 11. Ladoga Canals - witnesses of a grandiose construction.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/11.html)

[Chapter 12. What did you really want to say, Alexander Sergeevich?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/12.html)

[Chapter 13. The Alexander Column - we see only what we see.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/13.html)

[Chapter 14. Alexander 1. The secret of life and the secret of death.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/1.html)

[Chapter 15. Masonic symbolism of St. Petersburg.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/15.html)

[Chapter 16. Antediluvian city, or why the first floors in the earth?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/16.html)

[Chapter 17. Axonometric plan of St. Petersburg - a witness to the great flood.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/16_15.html)

[Chapter 17-1.  Flood witnesses. Antiquity in pictures and drawings.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2012/10/blog-post_3.html)

[Chapter 18. Who are you, ancient builders, or why are there so many inconsistencies among official historians?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/18_14.html)

[Chapter 19. A few words about floods.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/19.html)

[Chapter 19-1. Caspian Sea.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/12/blog-post_1.html)

[Chapter 20. A few more words about sculptures and statues (here the author argues with himself).](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/20.html)

[Chapter 21. Author's speculations.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/21.html)

#### Additions:

[There is a new title - ROTOR (it will be interesting)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%BE%D1%82%D0%BE%D0%BF)

The magnificent work of [Leo the Hud](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://levhudoi.blogspot.com/) on the topic: [Contradictions and absurdities of the official version of the construction of St. Petersburg.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://levhudoi.blogspot.co.il/2013/06/blog-post.html)

And one more work:

[Mysterious square holes in ancient megaliths around the world.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://levhudoi.blogspot.com/2015/08/blog-post_6.html)

I consider Vadim Makhorov's article ["Archaeological Expedition at Cape Okhtinsky, St. Petersburg"](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://eaquilla.livejournal.com/494559.html) to be very informative and rich in photography.

Another film was released on REW TV, with materials and photos from my work on "Where is the city from?" First, you are ridiculed and insulted, and only then, six years later, they make a film about what you told them about. Perhaps this is the fate of any work that does not fit into the generally accepted canons?

[The most shocking hypotheses.](https://www.youtube.com/watch?v=iamz07ArRZM) 

<!--
["Petersburg before our time!"](https://www.youtube.com/watch?v=iamz07ArRZM) 
[(29.01.2018) © REN TV](https://www.youtube.com/watch?v=iamz07ArRZM) 

Youtube link: [https://www.youtube.com/watch?v=iamz07ArRZM](https://www.youtube.com/watch?v=iamz07ArRZM)
-->

[The film 'Impossible Isaac' was released on Kramol.](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://atlantida-pravda-i-vimisel.blogspot.com/2016/03/blog-post.html) Sorry there are no links to sources.



© All rights reserved. The original author retains ownership and rights.

