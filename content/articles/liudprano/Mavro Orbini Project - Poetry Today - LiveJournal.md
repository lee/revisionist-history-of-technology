title: Mavro Orbini Project
date: 2017-01-27
modified: Sun 14 Feb 2021 17:44:56 GMT
category: liudprando
tags: Slavic history; books
slug:
authors: Gregory Shuvalov
from: https://liudprando.livejournal.com/64277.html
originally: Mavro Orbini Project - Poetry Today - LiveJournal.html
local: Mavro Orbini Project - Poetry Today - LiveJournal_files
summary: I plan to write an article about each of the 286 names indexed in Mavro Orbini's *Slavic Kingdom, 1601*. Due to repetitions, there may be around 219 names. It will take a very long time.
status: published

### Translated from:

[https://liudprando.livejournal.com/64277.html](https://liudprando.livejournal.com/64277.html)

**Russian:**

Как и обещал, открываю в своем ЖЖ: проект "Мавро Орбини", посвященный источникам первого историка славянских народов.

**English:**

As promised, I'm opening up my blog: Mavro Orbini project on the sources of the [first historian of the Slavic peoples](http://shop.influx.ru/Mavro-Orbini-Kniga-istoriographija-pochatija-imene-slavy-razshirenija-naroda-slavjanskogo-ih-carej-vladetelej-pod-mnogimi-imenami-so-mnogimi-Carstvijami-Korolevstvami-Provincijami-p-577.html).

<!-- Local
[![Mavro_Orbini_il_regno_de_gli_slavi.jpg](Mavro%20Orbini%20Project%20-%20Poetry%20Today%20-%20LiveJournal_files/0_99b73_9655b248_L.jpg "Mavro_Orbini_il_regno_de_gli_slavi.jpg")
-->

![](https://fotki.yandex.ru/next/users/liudprando/album/137953/view/629619)

**Russian:**

*Титульный лист первого издания книги «Славянское царство» 1601 года*

**English:**

*Title page of the first edition of "Slavic Kingdom", 1601*

**Russian:**

В именном указателе к "Славянскому царству" содержится 286 имен, фактически число авторов меньше, некоторые упоминаются в списке дважды и даже трижды. О каждом из них, кроме неустановленных (пока мне удалось идентифицировать 219 имен), я планирую написать статью, понятно, что это займет очень много времени. Про кого-то будет легко написать, про кого-то - нет, по причине недостатка информации. Сразу же поясню, что означают цвета указателя: [синий] - автор установлен, [зелёный] - неизвестный автор, [красный] - автор есть в указателе, но в тексте не упоминается (возможно, указатель отражает первоначальную редакцию текста, подвергшегося авторскому сокращению или цензуре), [фиолетовый] - повторяющиеся авторы. Впоследствии каждое имя указателя будет открываться как ссылка.

На данный момент работают ссылки № 4, 27 и 171.

**English:**

The index of names in the *Slavic Kingdom* contains 286 names. In reality, the number of authors is less, some are mentioned in the list twice and even three times. I plan to write an article about each of them, except for the unidentified ones (so far I have managed to identify 219 names), it is clear that it will take a very long time. It will be easy to write about some of them and not others due to the lack of information. Let me immediately explain what the colors in the index mean:

- [blue] - an identified author

- [green] - unknown author

- [red] - author is in the index, but not mentioned in the text (perhaps the index reflects the original version of the text, subjected to author reduction or censorship)

- [purple] - repeating authors. Subsequently each name in the index will open as a reference. 

Links #4, 27 and 171 are currently working.

**Russian:**

УКАЗАТЕЛЬ АВТОРОВ, ЦИТИРУЕМЫХ В ЭТОМ ТРУДЕ:

**English:**

AN INDEX OF AUTHORS CITED IN THIS WORK:

[1. Abbate Тritemio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Аббат Тритемий

**English:**

Abbot Trithemius

[2. Abbate Vrspargense] (author identified)

**Russian:**

Аббат Урсбергский

**English:**

The Abbot of Ursberg

[3. Ablabio] (author identified)

**Russian:**

Аблавий

**English:**

Ablavyi

[4. Abraam Ortelio] (author identified)](https://liudprando.livejournal.com/70937.html)

**Russian:**

Абрахам Ортелий

**English:**

Abraham Ortelius

[5. Adameo Sassone] (author identified)

**Russian:**

Адам Бременский

**English:**

Adam of Bremen

[6. М. Adamo] (duplicated authors)

**Russian:**

Адам Бременский (см. 5)

**English:**

Adam of Bremen (see 5).

[7. Agatia Smirneo] (author identified)

**Russian:**

Агафий Миринейский (также Схоластик Миринейский)

**English:**

Agathius of Myrrhynia (also Scholasticus of Myrrhynia)

[8. Agostino Dottore] (author identified)

**Russian:**

Августин [Гиппонский]

**English:**

Augustine [of Hippo].

[9. Agostino Morauo] (author identified)

**Russian:**

Авгyстин Моравий (Августин Оломоуцкий)

**English:**

Augustin Moravian (Augustin Olomouc)

[10. Aimоnе Моnасо] (author identified)

**Russian:**

Аймоин Монах (Аймоин из Флери)

**English:**

Aymoine Monk (Aymoine of Fleury)

[11. Alberto Crantio] (author identified)

**Russian:**

Альберт Кранц

**English:**

Albert Krantz

[12. Alberto Stadense] (author identified)

**Russian:**

Альберт Штаденский

**English:**

Albert Stadensky

[13. Alessandro Guaino] (author identified)

**Russian:**

Александр Гваньини

**English:**

Alexander Gvagnini

[14. Alessandro Sculeto] (author identified)

**Russian:**

Александр Скультетус - Скультетус Абрахам

**English:**

Alexander Skultetus - Skultetus Abraham

[15. Altamero] (author identified)

**Russian:**

Альтхамер - Андреас Альтхамер

**English:**

Althamer - Andreas Althamer

[16. Ammiano Моnасо] (duplicated authors)

**Russian:**

Аймоин Монах (см. 10)

**English:**

Aymoine Monk (see 10)

[17. Andrea Angelo Durazzino] (author not identified)

**Russian:**

Паоло Анджело, [архиепископ] Драчский

**English:**

Paolo Angelo, [Archbishop] of Drach

[18. Andrea Cornelio] (author not identified)

**Russian:**

Андреас Корнелиус (Ставоренский/Stauriese)

**English:**

Andreas Cornelius (Stavorensky/Stauriese)

[19. Andulfo Sagaco] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Ландульф Сагакс

**English:**

Landulf Sagax

[20. Annali di Frisia] (author not identified)

**Russian:**

Фризские летописи

**English:**

Friesian chronicles

[21. Annali di Olanda] (author not identified)

**Russian:**

Голландские летописи («Голландская хроника», цит. Суффридом?)

**English:**

Dutch Chronicle ("Dutch Chronicle," quoted by Suffrid?)

[22. Annali di Rausa] (author not identified)

**Russian:**

Рагyзинские летописи

**English:**

Ragusa chronicles

[23. Annali di Russia] (author not identified)

**Russian:**

Русские летописи (см. 96. Еремей Русский)

**English:**

Russian annals (see 96. Yeremey Russian)

[24. Annali de'Тutchi (Тurchi)] (author not identified)

**Russian:**

Турецкие летописи

**English:**

Turkish annals

[25. Annali di Venetia] (author not identified)

**Russian:**

Венецианские летописи (см. 135. Джулио Фарольди)

**English:**

Venetian Chronicles (see 135. Giulio Faroldi)

[26. Annonio Моnасо] (duplicated authors)

**Russian:**

Аймоин Монах (см. 10)

**English:**

Aymoine Monk (see 10)

[[27. Antonio Bofinio] (author identified)](http://liudprando.livejournal.com/66004.html)

**Russian:**

Антонио Бонфини

**English:**

Antonio Bonfini

[28. Antonio Geufreo] (author not identified)

**Russian:**

Антуан Жоффруа

**English:**

Antoine Joffroy

[29. М. Antonio Sabellico] (author identified)

**Russian:**

Марк Антонио Сабеллико

**English:**

Marc Antonio Sabellico

[30. Antonio Sconcovio] (author not identified)

**Russian:**

Антон ван Схонховен (Schonchovio)

**English:**

Anton van Schonhoven (Schonchovio)

[31. Antonio Viperano] (author in index but not in text; author possibly reduced or censored)

[Джованни] Антонио Виперано

[32. Appiano Alessandrino] (author identified)

**Russian:**

Аппиан Александрийский

**English:**

Appian of Alexandria

[33. Arnoldo Abbate] (author identified)

**Russian:**

Аббат Арнольд [Любекский]

**English:**

Abbot Arnold.

[34. Arpontaco Burdegalense] (author identified)

**Russian:**

Арнольд Понтак Бордоский

**English:**

Arnold Pontac of Bordeaux

[35. Arriano di Nicomedia] (author identified)

**Russian:**

Арриан из Никомидии

**English:**

Arrian of Nicomedia

[36. М. Aurelio Cassiodoro] (author identified)

**Russian:**

Maгн Аврелий Кассиодор

**English:**

Magnus Aurelius Cassiodorus

[37. S. Aurelio Vittore] (author identified)

**Russian:**

Секст Аврелий Виктор

**English:**

Sextus Aurelius Victor

[38. Baltasar Spalatino] (author not identified)

**Russian:**

Бальтасар Сплитский

**English:**

Balthasar of Split

[39. Beato Renano] (author identified)

**Russian:**

Беат Ренан

**English:**

Beate Renan.

[40. Beroso Caldeo] (author identified)

**Russian:**

Бероз Халдей

**English:**

Beroz Halday

[41. Bernardo Giustiniano] (author identified)

**Russian:**

Бернардо Джустиниани

**English:**

Bernardo Giustiniani

[42. Bilibaldo Pirckiameno] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Виллибальд Пиркгеймер

**English:**

Willibald Pirkheimer

[43. Bonifacio Simoneta] (author identified)

**Russian:**

Бонифачо Симонетта

**English:**

Bonifacio Simonetta.

[44. Bulla d'oro] (author identified)

**Russian:**

Золотая булла

**English:**

Golden Bull

[45. Busbequio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Бусбекий

**English:**

Busbeki

[46. Calfurino Sura] (author not identified)

**Russian:**

Кальпурний Сура

**English:**

Calpurnius Sura

[47. Callimaco appresso Plinio] (author identified)

**Russian:**

Каллимах, цитируется по Плинию

**English:**

Callimachus, quoted from Pliny

[48. Carlo Sigonio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Карло Сигонио

**English:**

Carlo Sigonio

[49. Carlo Vagriese] (author not identified)

**Russian:**

Карл Вагрийский

**English:**

Charles of Wagner

[50. Celio Donato] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Элий Донат

**English:**

Elijah Donatus

[51. Cerilliano] (author not identified)

**Russian:**

Цериллиан

**English:**

Cerillian

[52. Cesare Baronio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Цезарь Бароний

**English:**

Caesar Baronius

[53. М. Cicerone] (author identified)

**Russian:**

Марк Цицерон

**English:**

Marc Cicero

[54. Cornelio Тacito] (author identified)

**Russian:**

Корнелий Тацит

**English:**

Cornelius Tacitus

[55. Costantino Porfirogenito] (author identified)

**Russian:**

Константин Порфирородный

**English:**

Constantine Porphyrogenitus

[56. Costantino Spandugino] (author identified)

**Russian:**

Феодор Спандун

**English:**

Theodore Spandoon

[57. Corrado Brugense] (author not identified)

**Russian:**

Конрад из Брюгге

**English:**

Conrad of Bruges

[58. Corrado Peutingero] (author identified)

**Russian:**

Конрад Пейтингер

**English:**

Conrad Paytinger

[59. Crisippo] (author identified)

**Russian:**

Хрисипп

**English:**

Chrysippus

[60. Cronica de' Frati Minoriti] (author identified)

**Russian:**

Францисканская хроника

**English:**

The Franciscan Chronicle

[61. Q. Curtio] (author identified)

**Russian:**

Квинт Курций (Квинт Курций Руф)

**English:**

Quintus Curtius (Quintus Curtius Rufus)

[62. Christofano Varseuiccio] (author identified)

**Russian:**

Кшиштоф Варшевицкий

**English:**

Krzysztof Warszewicki

[63. David Chitreo] (author identified)

**Russian:**

Давид Хитреус, запрещенный автор

**English:**

David Chitraeus, banned author

[64. Descrizione del mondo] (author in index but not in text; author possibly reduced or censored) [Описание мира] (author not identified)

[65. Diodoro Siculo] (author identified)

**Russian:**

Диодор Сицилийский

**English:**

Diodorus of Sicily

[66. Diogene Laertio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Диоген Лаэртский

**English:**

Diogenes of Laertes

[67. Dione Niceo] (author identified)

**Russian:**

Дион [Кассий] Никейский

**English:**

Dion [Cassius] of Nicaea

[68. Dionisio Punico] (duplicated authors)

**Russian:**

Дионисий Пунический, также упоминается, как Дионисий Грек (Dionisio Greco) - Дионисий Галикарнасский?

**English:**

Dionysius of Punic, also referred to as Dionysius the Greek (Dionisio Greco) - Dionysius of Halicarnassus?

[69. Dithmaro Mersapurgese] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Титмар Мерзебургский

**English:**

Titmar of Merseburg

[70. Domenico Mario Nigro] (author identified)

**Russian:**

Доменико Мариа Негри

**English:**

Domenico Maria Negri

[71. Egesippo] (author identified)

**Russian:**

Егесипп

**English:**

Egesipp

[72. Egidio Тschudio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Эгидий Чуди

**English:**

Aegidius Chudy

[73. Еginhаrrо Моnасо] (author identified)

**Russian:**

Эйнгард Монах

**English:**

Eingard the Monk

[74. Elio Cordo] (duplicated authors)

**Russian:**

Элий Корд (см. 137. Юний Корд)

**English:**

Aelius Kord (see 137. Junius Kord)

[75. Elio Spartiano] (author identified)

**Russian:**

Элий Спартиан

**English:**

Elias Spartian

[76. Emanuelo Manasse] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Константин Манассия

**English:**

Konstantin Manasseh

[77. Epitoma di Strabone] (duplicated authors)

**Russian:**

Эпитома Страбона (см. 253. Страбон)

**English:**

Strabo's epitome (see 253. Strabo)

[78. Erasmo Stella] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Эразм Стелла

**English:**

Erasmus Stella

[79. Eudocio Panegirista] (author in index but not in text; author possibly reduced or censored) [Эвдокий Панегирист] (author not identified)

[80. Eugippo Моnасо] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Эвгиппий Монах

**English:**

Eugippius Monk

[81. Eusebio] (author identified)

**Russian:**

Евсевий - Евсевий Кесарийский

**English:**

Eusebius - Eusebius of Caesarea.

[82. Eustachio] (author identified)

**Russian:**

Евстахий - Евстафий Солунский?

**English:**

Eustachius - Eustathius of Thessalonica?

[83. Eutropio] (author identified)

**Russian:**

Евтропий

**English:**

Eutropius

[84. Fabio Celeriano] (duplicated authors)

**Russian:**

Фабий Цериллиан (см. 51. Цериллиан)

**English:**

Fabius Cerillian (see 51. Cerillian)

[85. Farasmanno Creco] (author not identified)

**Russian:**

Фарасман Гpeк

**English:**

Farasman the Greek

[86. Fascicolo de'tempi] (duplicated authors)

**Russian:**

Fasciculus temporum (см. 275. Вернер Ролевинк)

**English:**

Fasciculus temporum (see 275. Werner Rolewink)

[87. Filippo Callimaco] (author identified)

**Russian:**

Филипп Каллимах

**English:**

Philip Callimachus

[88. Filippo Lonicero] (author identified)

**Russian:**

Филипп Лоницер, запрещенный автор

**English:**

Philip Lonitzer, banned author

[89. Flauio Vopisco] (author identified)

**Russian:**

Флавий Вописк

**English:**

Flavius Vopisk.

[90. L. Floro] (author identified)

**Russian:**

Луций Флор

**English:**

Lucius Florus

[91. Francesco Bisio] (author not identified)

**Russian:**

Франческо Бизио (из Бергамо)

**English:**

Francesco Bisio (from Bergamo)

[92. Francesco Baldillo] (author not identified)

**Russian:**

Франческо Бальделли

**English:**

Francesco Baldelli

[93. Francesco Irenico] (author not identified)

**Russian:**

Франциск Иреникус

**English:**

Francis Irenicus

[94. Francesco Serdonati] (author identified)

**Russian:**

Франческо Сердонати

**English:**

Francesco Cerdonati.

[95. Gasparo Hedione] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Каспар Гедио, запрещенный автор

**English:**

Kaspar Gedio, forbidden author.

[96. Gasparo Peucero] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Каспар Пейцер

**English:**

Kaspar Peitzer

[97. Gasparo Tigurino] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Каспар [Мегандер] из Цюриха, запрещенный автор

**English:**

Kaspar [Megander] of Zurich, banned author

[98. Geremia Russo] (author not identified)

**Russian:**

Еремей Русский

**English:**

Yeremey Russian

[99. Gеnаrdо Rudingero] (author not identified)

**Russian:**

Герхард Рудингер

**English:**

Gerhard Rudinger

[100. Gioanni Аubano] (author identified)

**Russian:**

Иоганн Боемус

**English:**

Johann Boehmus

[101. Gioanni Auentino] (author identified)

**Russian:**

Иоганн Авентин, запрещенный автор

**English:**

Johannes Aventine, banned author

[102. Gioanni Battista] (author identified)

**Russian:**

Джованни Баттиста - Иоанн Креститель

**English:**

Giovanni Battista - John the Baptist

[103. Gioanni Botero] (author identified)

**Russian:**

Джованни Ботеро

**English:**

Giovanni Botero

[104. Gioanni Cocleo] (author identified)

**Russian:**

Иоганн Коклеус

**English:**

Johannes Cocleus

[105. Gioanni Curopalato] (author in index but not in text; author possibly reduced or censored) [Иоганн Куропалат] (author not identified)

**Russian:**

Георгий Кодин?

**English:**

Georgy Kodin?

[106. Gioanni Dubrauio] (author identified)

**Russian:**

Ян Дубравий

**English:**

Jan Dubravij

[107. Gioanni di Essendia] (author identified)

**Russian:**

Иоганн Эссенский

**English:**

Johann Essen

[108. Gioanni Herburto] (author identified)

**Russian:**

Ян Хербурт

**English:**

Jan Herburt

[109. Gioanni Laziardo] (author in index but not in text; author possibly reduced or censored) [Иоганн Лациард] (author not identified)

[110. Gioanni Magno Gotho] (author identified)

**Russian:**

Иоанн Магнус  Готландский

**English:**

John Magnus of Gotland

[111. Gioanni Leunclauio] (author identified)

**Russian:**

Иоганн Леунклавий, запрещенный автор

**English:**

Johannes Leunclavius, banned author

[112. Gioanni Nauclero] (author identified)

**Russian:**

Иоганн Науклер

**English:**

Johann Naukler

[113. Gioanni Villano] (author identified)

**Russian:**

Джованни Виллани

**English:**

Giovanni Villani

[114. Gioanni Stadio] (author not identified)

**Russian:**

Иоганн Стадий

**English:**

Johann Stadius

[115. Gioanni Goroppeio] (author not identified)

**Russian:**

Иоганн Горопий [Бекан]

**English:**

Johannes Horopius [Bekan]

[116. Gioanni Gobellino] (author identified)

**Russian:**

Иоганн Гобеллин

**English:**

Johannes Gobelin

[117. Gioanni Моnасо] (author identified)

**Russian:**

Иоанн Монах Иоанн Антиохийский?

**English:**

John the Friar - John of Antioch?

[118. Gioanni di Thvuocz] (author identified)

**Russian:**

Янош Туроци

**English:**

Janos Turoczy

[119. Gioanni Tigurino] (author not identified)

**Russian:**

Иоганн Тигyринус

**English:**

Johann Tigurinus

[120. Gioanni Pineto] (author identified)

**Russian:**

Хуан де Пинеда

**English:**

Juan de Pineda

[121. Giacomo Castaldo] (author identified)

**Russian:**

Джакомо Гастальди

**English:**

Giacomo Gastaldi

[122. Giacomo Meiero] (author not identified)

**Russian:**

Яков Мейер

**English:**

Jacob Meyer

[123. Giacomo Vifelingio] (author identified)

**Russian:**

Яков Вимпфелинг

**English:**

Jacob Wimpfeling

[124. Giacomo Spigelio] (author in index but not in text; author possibly reduced or censored) [Яков Спигелий] (author not identified)

[125. Giacomo Zieglero] (author not identified)

**Russian:**

Яков Циглер, запрещенный автор

**English:**

Jacob Ziegler, banned author

[126. Giorgio Cedreno] (author identified)

**Russian:**

Георгий Кедрин

**English:**

Georgy Kedrin

[127. Giorgio Fabritio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Гeopг Фабриций [из Хемница], запрещенный автор

**English:**

Georg Fabricius [of Chemnitz], banned author

[128. Giorgio Pachimero] (author identified)

**Russian:**

Георгий Пахимер

**English:**

George Pahimer

[129. Giorgio Tirio] (author identified)

**Russian:**

Гийом Тирский (Георгий?)

**English:**

Guillaume of Tyre (George?)

[130. Giorgio Vuerenhero] (author not identified)

**Russian:**

Гeopг Вернер

**English:**

Georg Werner

[131. Giornando Alano] (author identified)

**Russian:**

Иордан Алан

**English:**

Jordan Alan.

[132. Girolamo Dottore] (author identified)

**Russian:**

Иероним [Стридонский], учитель Церкви

**English:**

Jerome [Stridonian], teacher of the Church

[133. Girolamo Bardi] (author identified)

**Russian:**

Джироламо Барди

**English:**

Girolamo Bardi

[134. Girolamo Ruscelli] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Джироламо Рушелли

**English:**

Girolamo Ruscelli

[135. Guilio Faroldo] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Джулио Фарольди

**English:**

Giulio Faroldi

[136. Giustino] (author identified)

**Russian:**

Юстин

**English:**

Justine

[137. Giunio Cordo] (author identified)

**Russian:**

Юний Корд - он же Элий Корд

**English:**

Junius Kord - aka Elijah Kord.

[138. Godifredo Моnасо] (author not identified)

**Russian:**

Готфрид Монах

**English:**

Gottfried Monk

[139. Gothfrido Viterbiense] (author identified)

**Russian:**

Готфрид Витербский

**English:**

Gottfried of Viterbsk

[140. Gregorio Dottore] (author identified)

**Russian:**

Григорий (Назианзин], учитель Церкви (ошибочно) - Григорий Великий I (Двоеслов)

**English:**

Gregory (Nazianzin], teacher of the Church (erroneously) - Gregory the Great I (Dvoeslov)

[141. Gulielmo Cantero] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Вильгельм Кантер

**English:**

Wilhelm Kanter

[142. Gulielmo Frisio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Гемма Фризиус

**English:**

Gemma Friesius

[143. Gunthero Poeta] (author not identified)

**Russian:**

Гюнтер Поэт

**English:**

Gunther Poet

[144. Hartmanno Schedel] (author identified)

**Russian:**

Хартман Шедель

**English:**

Hartman Schedel

[145. Helmoldo Prete] (author identified)

**Russian:**

Гельмольд Пресвитер

**English:**

Helmold Presbyterian.

[146. Henrico di Eruordia] (author identified)

**Russian:**

Генрих Херфордский

**English:**

Henry of Hereford

[147. Неrmаnnо Contratto] (author identified)

**Russian:**

Герман Калека

**English:**

Herman Cripple

[148. Неrmаnnо Hamelmanno] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Герман Хамельманн

**English:**

Hermann Hamelmann

[149. Неrmаnnо Schodel] (duplicated authors)

**Russian:**

Хартман Шедель, см. 144

**English:**

Hartmann Schedel, cf. 144

[150. Herodiano] (author identified)

**Russian:**

Геродиан

**English:**

Herodian

[151. Herodoto Alicemaseo] (author identified)

**Russian:**

Геродот Галикарнасский

**English:**

Herodotus of Halicarnassus

[152. Huldrico Mutio] (author not identified)

**Russian:**

Ульрих Муциус, запрещенный автор

**English:**

Ulrich Mutzius, banned author

[153. Hunibaldo] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Гунибальд

**English:**

Gunibald

[154. Ioachimo Cureo] (author identified)

**Russian:**

Иоахим Куреус

**English:**

Joachim Kureus

[155. Isacio Тzetze] (author identified)

**Russian:**

Исаак Цец - Иоанн Цец

**English:**

Isaac Tsetz - John Tsetz

[156. Isidoro Hispalense] (author identified)

**Russian:**

Исидор Севильский

**English:**

Isidore of Seville

[157. Isigonio appresso Plinio] (author identified)

**Russian:**

Исигон, цитируется по Плинию

**English:**

Isigone, quoted from Pliny.

[158. Кiriaco Spangebergio] (author identified)

**Russian:**

Кириак Шпангенберг,

**English:**

Kiriak Spangenberg,

[159. Lamberto Schaffnaburgense] (author identified)

**Russian:**

Ламберт Герсфельдский (Ашаффенбурrский)

**English:**

Lambert of Hersfeld (Aschaffenburg)

[160. Laonico Calcondila] (author identified)

**Russian:**

Лаоник Халкокондил, запрещенный автор

**English:**

Laonicus Halkocondylus, forbidden author

[161. Laurentio Suro] (author identified)

**Russian:**

Лаврентий Сурий

**English:**

Lavrentiy Surii

[162. Leonardo Aretino] (author identified)

**Russian:**

Леонардо Аретино

**English:**

Leonardo Aretino.

[163. Libro delle cognizioni] (author in index but not in text; author possibly reduced or censored) [Liber cognitorum] (author not identified)

[164. Libro delle parti di Pregadi di Rausa] (author not identified)

**Russian:**

Реестр актов совета прегадов Раryзы

**English:**

Register of Acts of the Council of Pregads of Ragusa

[165. Lodovico Ceruino] (author identified)

**Russian:**

­Лудовик Цриевич-Туберон

**English:**

Ludovic Crijevic Tuberon

[166. Lucano] (author identified)

**Russian:**

Лукан

**English:**

Lukan

[167. Lucio Faunnо] (author not identified)

**Russian:**

Лучо Фауно

**English:**

Lucho Fauno

[168. Lucio Floro] (duplicated authors)

**Russian:**

Луций Флор (см. 90)

**English:**

Lucius Florus (see 90).

[169. Luigi Contarino] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Луиджи Контарини - Амброджо Контарини?

**English:**

Luigi Contarini - Ambrogio Contarini?

[170. Lupoldo Bambergio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Лупольд, епископ Бамберrский

**English:**

Lupold, Bishop of Bamberg

[[171. Luitprando Ticiniense]](http://liudprando.livejournal.com/65028.html)

**Russian:**

Лиутпранд Кремонский (он же Лиутпранд Павийский - Pauese)

**English:**

Leutprand of Cremona (aka Leutprand of Pauese)

[172. Marcellino Conte] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Марцеллин Комит

**English:**

Marcellinus Comitus

[173. Mariano Scoto] (author identified)

**Russian:**

Мариан Скот

**English:**

Marian Scott

[174. Marino Barletio] (author identified)

**Russian:**

Марин Барлеций

**English:**

Maureen Barletius.

[175. Marino Benchemio] (author in index but not in text; author possibly reduced or censored) [Марин Бенхемий] (author not identified)

[176. F. Martino] (author not identified)

**Russian:**

Ф. Мартин

**English:**

Ф. Martin

[177. Martino Abbate] (author in index but not in text; author possibly reduced or censored) [Мартин Аббат] (author not identified)

[178. Martino Cromero] (author identified)

**Russian:**

Марцин Кромер

**English:**

Marcin Kromer

[179. Martino Vescovo Cossentino] (author not identified)

**Russian:**

Мартин, епископ Гнезненский - Мартин Опа вский?

**English:**

Martin, Bishop of Gniezno - Martin of Opava?

[180. Martino Segonio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Мартино Сeгoнo из Новобрдо, епископ Улцинский

**English:**

Martino Segono of Novobrdo, Bishop of Ulcinj

[181. Martino Vagneto] (author not identified)

**Russian:**

Мартин Baгнeт

**English:**

Martin Wagnet.

[182. Marziano Сареllа] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Марциан Капелла

**English:**

Marzian Capella

[183. Matthia Meccouita] (author identified)

**Russian:**

Мацей Меховский

**English:**

Maciej Mechowski

[184. Mazochio] (author identified)

**Russian:**

Джакомо] Мадзоки

**English:**

Giacomo] Mazzocchi

[185. Metello Tigurino] (author in index but not in text; author possibly reduced or censored) [Метелл из Тегернзее] (author not identified)

[186. Metodio Historico] (duplicated authors) [Мефодий Историк] (author not identified)

**Russian:**

, также вне списка упоминаются Мефодий Философ и Мефодий Мученик

**English:**

Also, Methodius the Philosopher and Methodius the Martyr are mentioned out of the list

[187. Michel Riccio] (author identified)

**Russian:**

Микеле Риччо

**English:**

Michele Riccio.

[188. Michel Salonitano] (author not identified)

**Russian:**

Михайло Салонский (Михаил Солунский, ритор?)

**English:**

Michael of Salona (Michael of Salona, the rhetorician?)

[189. Modesto] (author identified)

**Russian:**

Модест

**English:**

Modest

[190. Nazario Mamertino] (author not identified)

**Russian:**

Назарий Мамертин

**English:**

Nazarius Mamertin

[191. Niceforo Gregora] (author identified)

**Russian:**

Никифор Григора

**English:**

Nicephorus Grigora

[192. Niceta Coniato] (author identified)

**Russian:**

Никита Хониат

**English:**

Nikita Honiat

[193. G. Nicolo Doglioni] (author not identified)

**Russian:**

Джованни Николо Дольони

**English:**

Giovanni Nicolo Doloni

[194. Nicolo Marscalco] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Николаус Маршалк

**English:**

Nikolaus Marschalk

[195. Nicolo Stobeo] (author not identified)

**Russian:**

Стобей (Иоанн Стобей?)

**English:**

Stobei (John Stobei?)

[196. Olao Magno] (author identified)

**Russian:**

Олаф Maгнуc

**English:**

Olaf Magnus

[197. Onesimo] (author not identified)

**Russian:**

Онезим

**English:**

Onesimus

[198. Origine de' Gothi] (author identified)

**Russian:**

Origo Gothorum (см. 131. Иордан Алан)

**English:**

Origo Gothorum (see 131. Jordan Alan)

[199. Ottone Frigigense] (author identified)

**Russian:**

Оттон Фрейзингенский

**English:**

Otton of Freisingen

[200. Ouidio Nasone] (author identified)

**Russian:**

Публий Овидий Назон

**English:**

Publius Ovidius Nazon

[201. Paolo Barnefrido] (duplicated authors)

**Russian:**

Павел Варнефрид (см. 202. Павел Диакон)

**English:**

Paulus Barnephrides (see 202. Paulus Deacon)

[202. Paolo Diacono] (author identified)

**Russian:**

Павел Диакон

**English:**

Paul Deacon

[203. Paolo Emilio] (author identified)

**Russian:**

Паоло Эмилио

**English:**

Paolo Emilio

[204. Paolo Niuemontano] (author not identified)

**Russian:**

Пауль Шнееберга

**English:**

Paul Schneeberg

[205. Paolo Giouio] (author identified)

**Russian:**

Павел Иовий

**English:**

Paul Joby

[206. Paolo Langio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Пауль Ланге Цигнеус

**English:**

Paul Lange Zigneus

[207. Paolo Orosio] (author identified)

**Russian:**

Павел Орозий

**English:**

Pavel Orosiy

[208. Paolo Paruta] (author identified)

**Russian:**

Паоло Парута

**English:**

Paolo Paruta.

[209. Paolo Scaligero] (author identified)

**Russian:**

Павел Скалич

**English:**

Pavel Skalic

[210. Petancio] (author identified)

**Russian:**

Петанчич

**English:**

Petancic

[211. Pier Francesco Giambulari] (author identified)

**Russian:**

Пьер Франческо Джамбулари

**English:**

Pierre Francesco Giambulari

[212. Pietro Artорео] (author identified)

**Russian:**

Петер Артопеус, запрещенный автор

**English:**

Peter Artopeus, banned author

[213. Pietro Bellonio] (author identified)

**Russian:**

Пьер Белон

**English:**

Pierre Belon

[214. Pietro Bizaro] (author identified)

**Russian:**

Пьетро Биццари

**English:**

Pietro Bizzari.

[215. Pietro de Castro Pere] (author not identified)

**Russian:**

Петр из Кастро Пере

**English:**

Peter from Castro Pere

[216. Pietro Crusber] (author not identified)

**Russian:**

Петр Крусбер (Голландский)

**English:**

Peter Krueber (Dutch)

[217. Pietro Echilino] (author not identified)

**Russian:**

Петр Эквилийский (Аквилейский)

**English:**

Peter of Equileia (Aquileia)

[218. Pietro Giustiniano] (author identified)

**Russian:**

Пьетро Джустиниани

**English:**

Pietro Giustiniani

[219. Pietro Liuio] (author not identified)

**Russian:**

Пьетро Ливио (Veronese)

**English:**

Pietro Livio (Veronese)

[220. Р. Piteo] (author identified)

**Russian:**

Пьер Питу

**English:**

Pierre Pitou

[221. Pio Secondo] (author identified)

[папа] Пий II (Энеа Сильвио Бартоломео Пикколомини)

[222. Plinio] (author identified)

**Russian:**

Плиний [Старший]

**English:**

Pliny [Elder].

[223. Plutarco] (author identified)

**Russian:**

Плутарх

**English:**

Plutarch

[224. Polibio] (author identified)

**Russian:**

Полибий

**English:**

Polybius

[225. Porfirio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Порфирий

**English:**

Porphyry

[226. Pomponio Leto] (author identified)

**Russian:**

Помпоний Лет

**English:**

Pomponius Leth.

[227. Privilegi di Cataro] (author not identified)

**Russian:**

Привилегии Котора

**English:**

Privileges of Kotor

[228. Procopio di Cesarea] (author identified)

**Russian:**

Прокопий Кесарийский

**English:**

Procopius of Caesarea

[229. Prospero Aquitano] (author identified)

**Russian:**

Проспер Аквитанский

**English:**

Prosper Aquitaine

[230. Rafaelo Volaterano] (author identified)

**Russian:**

Рафаэль из Вольтерры

**English:**

Rafael of Volterra

[231. Reginone Abbate] (author identified)

**Russian:**

Регино Аббат (Регино, аббат Прюмский - Pruniense)

**English:**

Regino Abbot (Regino, Abbot of Pruniense)

[232. Registro delle Cronache] (author identified)

**Russian:**

Registrum cronicarum (Нюрнбергская хроника)

**English:**

Registrum cronicarum (Nuremberg Chronicle)

[233. Reinnero Reinecio] (author identified)

**Russian:**

Рейнерий Рейнекций, запрещенный автор

**English:**

Reinelius Reinetius, forbidden author

[234. Ricardo Bartolino] (author identified)

**Russian:**

Риккардо Бартолини

**English:**

Riccardo Bartolini.

[235 Rinaldo Britanno] (author not identified)

**Russian:**

Ринальд Британский

**English:**

Rinald British.

[236. Roberto Gaguino] (author identified)

**Russian:**

Робер Гаген

**English:**

Robert Gagen

[237. Roberto Valturio] (author identified)

**Russian:**

Роберто Вальтурио

**English:**

Roberto Valturio

[238. Sassone Grammatico] (author identified)

**Russian:**

Саксон Грамматик

**English:**

Saxon Grammar

[239. Sebastian Munstero] (author identified)

**Russian:**

Себастьян Мюнстер, запрещенный автор

**English:**

Sebastian Munster, banned author

[240. Scolastico Smirneo] (author identified)

**Russian:**

Схоластик Миринейский

**English:**

Scholasticus of Myrrhineae

[241. Scipione Ammirato] (author identified)

**Russian:**

Шипионе Аммирато

**English:**

Shipione Ammirato

[242. Seruio] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Сервий

**English:**

Servius

[243. Sidonio Apollinaro] (author in index but not in text; author possibly reduced or censored)

**Russian:**

Сидоний Аполлинарий

**English:**

Sidonius Apollinarium

[244. Sigiberto Gemblacese] (author identified)

**Russian:**

Сигиберт из Жамблу

**English:**

Sigibert of Jamblu.

[245. Sigismondo Herbersteino] (author identified)

**Russian:**

Сиrизмунд Герберштейн

**English:**

Sigismund Gerberstein

[246. Silberto Genebrardo] (author identified)

**Russian:**

Жильбер Женебрар

**English:**

Gilbert Genebrard

[247 Socrate Historico] (author not identified)

**Russian:**

Сократ Историк - Сократ Схоластик???

**English:**

Socrates the Historian - Socrates the Scholastic?

[248. Solino] (author identified)

**Russian:**

Солин

**English:**

Solin

[249. Sozimeno] (author identified)

**Russian:**

Созомен

**English:**

Sozomen

[250. Specchio de' Sassoni] (author identified)

**Russian:**

Саксонское зерцало

**English:**

The Saxon Mirror

[251. С. Statio] (author identified)

**Russian:**

Poeta Цецилий Стаций Поэт

**English:**

Poeta Caecilius Statius the Poet

[252. Stefano Bizantino] (author identified)

**Russian:**

Стефан Византийский

**English:**

Stephen of Byzantium

[253. Strabone] (author identified)

**Russian:**

Страбон

**English:**

Strabo

[254. Suffrido Pietro Misnense] (author identified)

**Russian:**

Суффрид Петри - Петр Суффрид Леовардийский

**English:**

Suffrid Petri - Peter Suffrid of Leovardia

[255. Suida] (author identified)

**Russian:**

Свида

**English:**

Swida

[256. Soplimento di Eutropio] (author identified)

**Russian:**

Добавление к Евтропию (см. 83. Евтропий)

**English:**

Addendum to Eutropius (see 83. Eutropius)

[257. Suetonio Тranquillo] (author identified)

**Russian:**

Светоний Транквилл

**English:**

Suetonius Tranquillus

[258. Suffrido Misnense] (duplicated authors)

**Russian:**

Суффрид Петри (см. 254)

**English:**

Suffrid Petrie (see 254)

[259. Symmaco] (author not identified)

**Russian:**

Симмах (Симмах Грек)

**English:**

Simmachus (Simmachus the Greek)

[260. Teoderico] (author identified)

**Russian:**

Феодорит [епископ Тирский] - на самом деле Кирский

**English:**

Theodorite [Bishop of Tyre] - actually a Cyprian.

[261. Teodoro Spandugino] (author not identified)

**Russian:**

Феодор Спандун

**English:**

Theodore Spandoon

[262. Tеороmро Chio] (author identified)

**Russian:**

Феопомп Хиосский

**English:**

Theopompus of Chios

[263. Teodolo] (author identified)

**Russian:**

Фома Магистр, Феодул (Theodulo)

**English:**

Thomas the Magister, Theodulo.

[264. Tito Liuio] (author identified)

**Russian:**

Тит Ливий

**English:**

Titus Livius

[265. Тolomeo Alessandrino] (author identified)

**Russian:**

Птолемей Александрийский

**English:**

Ptolemy of Alexandria

[266. Тоmа Ebendorfio] (author identified)

**Russian:**

Томас Эбендорфер

**English:**

Thomas Ebendorfer

[267. Trebellio Pollione] (author identified)

**Russian:**

Требеллий Поллион

**English:**

Trebellius Pollion

[268. Trogo Роmрео] (author identified)

**Russian:**

Помпей Трoг

**English:**

Pompey Trog

[269. Tugenone Patauino] (author identified)

**Russian:**

Тагено из Пассау

**English:**

Tageno from Passau

[270. Valerio Massimo] (author identified)

**Russian:**

Валерий Максим

**English:**

Valery Maxim

[271. М. Varone] (author identified)

**Russian:**

Марк Варрон

**English:**

Mark Varron

[272. F. Vegetio] (author identified)

**Russian:**

Флавий Вегеций

**English:**

Flavius Vegetius.

[273. G. Velleio Paterculo] (author identified)

**Russian:**

Гай Веллей Патеркул

**English:**

Gaius Welles Paterculus

[274. Vencesaluo Воеmо] (author not identified)

**Russian:**

Венцеслав Богемский

**English:**

Wenceslav Bohemsky

[275. Vernero Rolenuick] (author identified)

**Russian:**

Вернер Ролевинк (Fasciculus temporum)

**English:**

Werner Rohlevink (Fasciculus temporum)

[276. Vettore Vticense] (author identified)

**Russian:**

Виктор Витенский

**English:**

Victor Vitensky

[277. Vgo Fuluonio] (author not identified)

**Russian:**

Угo Фульвонио

**English:**

Hugo Fulvonio.

[278. Vitichindo Olandese] (author not identified)

**Russian:**

Видукинд Голландский

**English:**

Vidukind Dutch

[279. Vitichindo Sassone] (author identified)

**Russian:**

Видукинд Корвейский - он же Видукинд Монах

**English:**

Vidukind of Corvey - aka Vidukind the Monk

[280. Vitichindo Vagriese] (author not identified)

**Russian:**

Видукинд Вагрийский

**English:**

Viductindus of Wagry

[281. Vnefrido Inglese] (author not identified)

**Russian:**

Винфрид Aнглийский (святой Бонифаций, епископ Майнца)

**English:**

Winfried of England (St Boniface, Bishop of Mainz)

[282. Vuolgfango Lazzio] (author identified)

**Russian:**

Вольфганг Лациус

**English:**

Wolfgang Lacius

[283. Vuolgfango Olandese] (author in index but not in text; author possibly reduced or censored) 

**Russian:**

[Вольфганг Голландский] (author not identified)

**English:**

Wolfgang Dutch (author not identified)

[284. Zacaria Lilio] (author identified)

**Russian:**

Заккариа Лилио

**English:**

Zaccaria Lilio

[285. Zonara] (author identified)

**Russian:**

Иоанн Зонара

**English:**

John Zonara

[286. Zosino] (author identified)

**Russian:**

Зосим

**English:**

Zosimus

© All rights reserved. The original author retains ownership and rights.

