title: Australian Telescope
date: 2019-04-26
modified: Wed 28 Jul 2021 16:23:39 BST
category:
tags: technology; cannons
slug:
authors: Pavel Verkhov (Alexander Alekseev)
from: https://vk.com/@kbogam-teleskop
originally: Australian Telescope.md
local: Australian Telescope_files
summary: In none of the places where the Melbourne Telescope was mentioned, did I find anything about the functionality of this mesh. Such a construction can only make sense if working with wave objects. Light, of course, is also a wave, but telescopes use ONLY the laws of geometric optics, where the wave properties of light are ignored.
status: published

#### Translated from:

[https://vk.com/@kbogam-teleskop](https://vk.com/@kbogam-teleskop)

Since I’ve started on the topic of telescopes, I'll stay on it. This is a fragile, glass telescope. There are, of course, now telescopes without internal lenses, which catch light on a matrix like a video camera or a selfie-phone. But we are considering ancient equipment here and therefore it is made of glass.

![](./images/Australian Telescope_files/iaDk_EvjtUg.jpg#resized)

<!-- ![](https://sun6-21.userapi.com/impf/c853528/v853528884/2d896/iaDk_EvjtUg.jpg) -->
*And this is also a telescope!!!*

So, first there was a refracting telescope. Starting with Galileo in 1605-6-7. The dates are unclear because this device started being invented in different countries at the same time and there was a kerfuffle over patents. We will not dwell on this aspect - there is hardly much truth there anyway.

But from the point of view of refraction of rays from stars, everything is within the framework of modern canons. The lens enlarges and diffuses. One scatters, and the second focuses light on your eye.

![](./images/Australian Telescope_files/thlLltrxBdY.jpg#resized)

<!-- ![](https://sun9-59.userapi.com/impf/c851324/v851324329/100d19/thlLltrxBdY.jpg) -->

There's no difficulty here. The light from the stars, although it is far away, is much larger than a regular JPG image, so it can be driven back and forth. Diffuse (stretch) it and collect (shrink) it and it will not turn into a blur after five attempts to fit it into the corner of the picture like it would if manipulated in Photoshop. All problems of the magnification limit in a refracting telescope from lenses, eyepieces and greasy fingers.

Translator's note: the author is using sarcasm to highlight the lack of logic in the claim that heavy image manipulation did not distort the image obtained.

Where astronomers and engineers obtained high-quality lenses and eyepieces - that is, optics - before the end of the 19th century we will leave to historians. For them, everything materializes in the right place when necessary. If suddenly there is a slight hitch - it doesn't matter. You can always call on another million slaves or serfs to fix things. They certainly won't let you down.

But we are not looking at a refracting telescope here. Our subject belongs to the reflector family of telescopes. They do not use lenses, but curved, concave mirrors.

Mr. Newton, having eaten another apple and holding the philosopher's stone for the hundredth time while stewing it in a pressure cooker, which made it turn into a toad, looked sadly at the stars. He could do this for hours.

Well, more precisely he could, if he was Schwarzenegger. In addition to cloudy lenses, refractors have another weak point: their focal length. If you want to look at a smaller star, increase the focal length. A refracting telescope that provided a magnification of 100 times was already 7 meters long and it was not possible to look into it for a long time alone, even with a tripod.

By the way, historians once again do not give up. According to their stories, the most desperate astronomer - in an attempt to look deeper into the soul of God - made himself a telescope 70 meters long. Say hello to another million slaves who have vanished.

Newton did not like crowds of slaves around him, so he decided to invent a smaller device that would have all the advantages of long refractors, but would be compact and convenient.

Just as he succeeded after just one or two attempts at everything he undertook (except for the Philosopher's Stone) Newton immediately came up with a new type of telescope. The letter "R" tickled him even in the name of the previous models, diction defects affected. Newton's maternal grandmother was a tax inspector and the letter "R" was not given to Isaac since childhood. Therefore, the new telescope became a 'Reflector'.

![](./images/Australian Telescope_files/R0IqPVlxJW4.jpg#resized)

<!-- ![](https://sun9-75.userapi.com/impf/c851324/v851324329/100d29/R0IqPVlxJW4.jpg) -->
*Newton's invention, as always, was ahead of its time.  Humanity now had a telescope based on specular reflection, but unfortunately there were no mirrors yet.  The inventor assembled the prototype in 1705 using an alloy of copper, tin and arsenic instead of mirrors.  What he saw there and who polished it all to a mirror-quality reflectivity for him - well, ask the historians.*

By 1720, the English had somehow learned to make acceptable mirrors (albeit still from polished metal) and the process had begun. Short, blunt reflectors began to conquer the astronomical markets.

![](./images/Australian Telescope_files/s1SZPc_ccNU.jpg#resized)

<!-- ![](https://sun9-59.userapi.com/impf/c851324/v851324096/fe3db/s1SZPc_ccNU.jpg) -->
*Personal Newtonian.*

But then a misfortune happened and another Englishman, pushing himself up on the occasion of a hearty dinner, gave birth to a two-lens refractor. Why he waited for this until 1756, when logically it should have occurred to everyone who had heard about the 70-meter telescope, I do not know. Again, ask the historians.

Everyone, as if on command, immediately forgot about reflectors. All went back to staring into refractors again. Only a hundred years later, in 1856, the notorious Foucault inserted our modern silver-plated mirror into the reflector and the device sparkled with new colors.

But refractors, too, had moved somewhere and also started playing. As a result, the reflectors could not win back even parts of the market and died again without being born. Everyone continued to stare through refractors.

And only by the end of the 19th century, when production mastered normal mirrors with curved and concave sides, did the reflector wake up and run after refractors.

In reality, reflectors began to be built only from the 20th century onwards. In the USSR, the first Soviet reflector appeared only in 1974.

So, if we follow official history, we get that Newton's idea of using mirrors to catch residual light from stars and other nebulae could not work properly until the invention of a conventional mirror with an silver amalgam or, as it is now, aluminium coating. In reality how did they learn to make these mirrors concave or convex (scientifically 'parabolic'), able to collect or scatter rays of light just as lenses do?

In 1835, the German chemist Justus von Liebig revolutionized the production of mirrors by silvering mirrors with silver to obtain a clearer image.

![](./images/Australian Telescope_files/w10x8mZNgpM.jpg#resized)

<!-- ![](https://sun9-18.userapi.com/impf/c850120/v850120798/12b4c2/w10x8mZNgpM.jpg) -->
*Justus*

Other sources claim he did that piece of chemistry in partnership with a Frenchman. And somewhat later.

> The so-called silvering was invented in 1855-1856. Chemists Justus von Liebig and François Ptijan.

In general, we should not thank the Chinese for inventing the technology around the time of the dinosaurs a million years ago.

Now, back at our Australian facility, codenamed "Strainer".

![](./images/Australian Telescope_files/VXBLsBqUO7U.jpg#resized)

<!-- ![](https://sun9-17.userapi.com/impf/c850120/v850120798/12b4cb/VXBLsBqUO7U.jpg) -->
*Australian Telescope, image # 6*

> The Large Melbourne Telescope was one of the largest telescopes in the world during the 19th century and the largest in the Southern Hemisphere.

> Erected in 1869 at the Melbourne Observatory, this "giant of science" was an icon of Melbourne during the boom years that followed the gold rush of the 1850s. The telescope symbolized to the Melbourneers that their city was a city of knowledge and civilization, not just economic wealth.

> Moved to Canberra in 1944, it has been heavily modified for modern astronomy. The 2003 Canberra fires destroyed modern equipment but left the original parts relatively unscathed.

That sounds good. Historically, they succeeded after conventional mirrors appeared. Moreover, it was officially the last reflexive telescope made with metallised reflectors (mirrors).

Another thing is that the production date is not inscribed on it, but this does not matter, of course, if it is a conventional telescope for casual examination of the spiral motions of the Universe.

Translator's note: a plaque apparently noting the telescope's builder (Thomas Grubb) and build date (1868) is now visible in the Melbourne Telescope history page at [https://greatmelbournetelescope.org.au/restoration-project/](https://greatmelbournetelescope.org.au/restoration-project/).

There are a couple of oddities, but in my opinion they are not critical.

To begin with, the official history always portrays this telescope in different ways. Especially interesting is: what is it doing in an open field on the remains of an obviously destroyed structure when it was built specifically for an Australian observatory?

![](./images/Australian Telescope_files/7WO55yPpDMQ.jpg#resized)

<!-- ![](https://sun9-55.userapi.com/impf/c850120/v850120798/12b4ec/7WO55yPpDMQ.jpg) -->
*Australian Telescope, image # 7*

There are some differences in the design of the device itself.

![](./images/Australian Telescope_files/BkToucMh2qY.jpg#resized)

<!-- ![](https://sun9-13.userapi.com/impf/c850120/v850120798/12b4fe/BkToucMh2qY.jpg) -->
*Australian Telescope, image # 8*

But they write that the building seems to have been rebuilt around it later on.

![](./images/Australian Telescope_files/ImEEFxEpQAo.jpg#resized)

<!-- ![](https://sun9-47.userapi.com/impf/c850120/v850120399/12b63c/ImEEFxEpQAo.jpg) -->
*Australian Telescope, image # 9*

Or maybe these strange pictures were taken in Ireland, where they claim the telescope was built. Although its construction in an open field, and indeed destination in a distant land, may also provoke some suspicion, of course.

![](./images/Australian Telescope_files/IHU8y-MEBWw.jpg#resized)

<!-- ![](https://sun9-18.userapi.com/impf/c850120/v850120399/12b68f/IHU8y-MEBWw.jpg) -->
*I don't see Grubb's factory (Translator's note: Pavel Verkhov is here referring to the idea that the 'telescope' would likely have been built under cover). I see an intact mesh structure that is being dismantled in a field.*

Here, by the way, I have more questions for Ireland, where in the 19th century the largest, most technologically complex and high-profile objects are being built (another example is the Titanic) while the Irish are being massacred and sold into slavery along with the blacks!

Now, moving along. About making the telescope itself. It turns out that it is not an Australian miracle at all but an English, or even a common European, one.

Its construction was approved by the British Academy of Sciences, but for some reason the Frenchman Foucault (the one with the pendulum) got upset and actively objected. By the way, his objection was correct. He said only morons would build a telescope with metal mirrors, when there is already a technology for making normal glass mirrors with a silver coating.

Perhaps he just wanted to win the construction contract. The decision of the commission in favor of iron was really strange, especially considering that silver is one of the most reflective metals (93 percent) and therefore mirrors that used silver would be more effective.

It is even more funny that this is not mentioned anywhere in Foucault's proposal, or during the work of the commission for the selection of proposals for the project. One gets the impression that they were not in the know [about silver's reflectivity] yet.

All of the above, of course, is funny, fun and could add to some hypothesis, but in itself it does not speak of any secret.

Now, let's talk about the terrible!

During the entire operation of the telescope, almost no results from its work remain. But the reviews are full of those who tried and reported that nothing could be seen in it. I will attach a document with actual links to the complaints and suggestions book.

The President of the British Royal Astronomical Society, Robert Ellery, could not even see the satellites of Mars through it.

Translator's note: lithographs allegedly created from the telescope's observations are now available on the page at [https://greatmelbournetelescope.org.au/achievements/](https://greatmelbournetelescope.org.au/achievements/). Acknowledgement that its alleged results remain unpublished and a partial denial of its failure are at [https://greatmelbournetelescopeorgau.files.wordpress.com/2015/12/gmt-fact-sheet-08-myth-of-failure.pdf](https://greatmelbournetelescopeorgau.files.wordpress.com/2015/12/gmt-fact-sheet-08-myth-of-failure.pdf). The author's suggestion that the telescope was used to study one Herschel nebula is currently countered by the claim it was used to research 1,700 nebulae discovered by Herschel.

With the wildest difficulty, one hundred and thirty-five times, it was focused on the moon. The results turned out to be worse than when photographing the moon with a conventional camera.

Here you can howl along with the official scientific, and even the historians, who howl that reflex telescopes are not designed for close-range observation of close objects.

And you can parry - and at x .. I then looked? Didn't they know what they had built?

Observations of distant objects also failed. Wherever the barrel of the telescope was pointed, only the Herschel nebulae [already discovered by Herschel at the Cape of Good Hope, South Africa] could usually be seen through the telescope.

![](./images/Australian Telescope_files/UvkLaHdpI6U.jpg#resized)

<!-- ![](https://sun9-54.userapi.com/impf/c850120/v850120683/132ed5/UvkLaHdpI6U.jpg) -->
*Howard Grubb and John Herschel*

Actually, they became Herschel's nebulae after Herschel managed to find a point in the sky, which the telescope enlarged, and did not blur.

To justify the epic fail, they [now write that the Melbourne Telescope was built for the research of the Herschel Nebulae](https://greatmelbournetelescopeorgau.files.wordpress.com/2015/12/gmt-fact-sheet-08-myth-of-failure.pdf). It makes no sense to ask questions of historians. In general, cause-and-effect relationships are not their strong point.

Now about the barrel of the telescope. I have not yet found a justification for this particular construction. This does not mean that there is no jusification. They just haven't figured one out yet.

The point is that reflector telescopes do not require a barrel at all. It only interferes with them by limiting the light collection area.

![](./images/Australian Telescope_files/vYl2A5Fuv68.jpg#resized)

<!-- ![](https://sun9-7.userapi.com/impf/c845521/v845521611/1fb4c8/vYl2A5Fuv68.jpg) -->
*If desired, a reflector telescope can be built without a barrel, and even without a main box.*

The mirror system is mounted in a barrel to protect it from external influences (sun, wind, sand, dust, a drunk laboratory assistant, etc), but only if the telescope is focused on a single point...

And when there is a barrel which does not protect anything; you have to pull a condom on it so that at least it will do something!!!

![](./images/Australian Telescope_files/ffZfUrmR9p0.jpg#resized)

<!-- ![](https://sun9-41.userapi.com/impf/c850220/v850220956/125a5e/ffZfUrmR9p0.jpg) -->
*This is another, similar thing from about the same time.  In America. More about it.*

![](./images/Australian Telescope_files/u-MPTOj-mXk.jpg#resized)

<!-- ![](https://sun9-50.userapi.com/impf/c850120/v850120683/132ff6/u-MPTOj-mXk.jpg) -->

...

As for the mesh section of the Melbourne Telescope's barrel, from the point of view of modern telescopic science, it cannot perform any useful function. It does not protect from external influences, but blocks the road to the rays everywhere.

In none of the places where the Melbourne Telescope was mentioned, did I find anything about the functionality of this mesh.

Such a construction can only make sense if working with wave objects. Light, of course, is also a wave, but telescopes use ONLY the laws of geometric optics, where the wave properties of light are ignored.

![](./images/Australian Telescope_files/CieuZySHESI.jpg#resized)

<!-- ![](https://sun9-54.userapi.com/impf/c850120/v850120683/132f0f/CieuZySHESI.jpg) -->
*Jumping gallop*

Based on all of the above, you can draw your own conclusions. I, of course, cannot accuse those who have already decided that there is nothing unusual in human stupidity. That there is nothing unusual about building an outdated structure without incorporating newer technologies or about commercialism (in which one selects a bad project for the kickbacks), after which one has to diligently remove the evidence and mask it (moving it to Australia, specializing in the study of one nebula), so as not to get caught out.  For on such people - people who lack imagination, who are sober-minded - firmly standing on the side of objective (ie, obvious) reality, the modern world is supported.

For the rest, I propose to dream up and delve into the abyss of conspiracy theory. In our kingdom of crooked mirrors (how consonant that is with a reflexive telescope!), all stupidity vanishes and a completely different picture emerges.

Another incomprehensible device was found on the remains of a destroyed civilization. It seems like other similar ones, in which we now look at the sky and they enlarge the stars for us. But for some reason this one does not focus.

We scratch our turnips... What to do? When you don't understand how the principles of work, you act out a slapstick joke: you turn it on and off, open and close it, and sometimes it helps to slam the door harder.

Here we chose to move the device. It no longer works but it used to work here in this location before. Otherwise, why did the Gods (Atlanteans, giants, Tartars, Hyperboreans) put it here? Like, er, maybe because the Earth was struck it will work now but only if sited on the opposite side? Maybe it will work there now, like it used to work here?

So they called a taxi to rearrange things. Moreover, with instructions that everything should be assembled on the spot exactly as it stood here! Otherwise, I cannot explain for any other reason why it had to be built at the other end of the world, and most importantly, why then take it to the other end of the world TOGETHER WITH ITS STAND? After all, surely they could have built a plinth from the rocks already in Australia!!!

[Video 1 highlights simple plinth parts puzzlingly shipped to Australia](./images/Australian Telescope_files/video.mp4)

After the failure with the Australian "mesh", they realized that it was not a stand, and at the beginning of the 20th century in America they had already dragged the Mount Wilson telescope up Mount Wilson, it was purely blowing with optics.

![](./images/Australian Telescope_files/cL9RaVmfRWc.jpg#resized)

<!-- ![](https://sun9-54.userapi.com/impf/c850220/v850220956/1259bf/cL9RaVmfRWc.jpg) -->
*A high-tech object on a horse-drawn carriage looks funny.*

![](./images/Australian Telescope_files/0rOHlzFAiN4.jpg#resized)

<!-- ![](https://sun9-20.userapi.com/impf/c850220/v850220956/1259d0/0rOHlzFAiN4.jpg) -->
*But it's even funnier when you know that this is an astronomical observatory - where they are taking it!!!*

Then, of course, things became more reasonable. The device began to reveal something. The mechanism was studied. We began to make our own, in the image and likeness.

<!--
![](./images/Australian Telescope_files/bOa3Z-5ChlU.jpg#resized)
-->

<!--
![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:19:31.png#resized)
-->
![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:28:48.png#resized)

<!-- ![](https://sun9-11.userapi.com/impf/c850220/v850220956/125a3d/bOa3Z-5ChlU.jpg) -->
*Some are still standing.*

<!--
![](./images/Australian Telescope_files/EPNeUvWvpjQ.jpg#resized)
-->
<!--
![](./Australian Telescope_files/EPNeUvWvpjQ.png#resized)
-->
![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:17:02.png)

<!-- ![](https://sun9-48.userapi.com/impf/c845419/v845419956/1ed3ec/EPNeUvWvpjQ.jpg) -->
*Australian Telescope, image # 19*

Translator's note: The sky appears to have been re-worked above the observatory. Perhaps delivery by horse-drawn, tiny-wheeled wagon - as pictured earlier - was faked.

<!--
![](./images/Australian Telescope_files/C8-p5pfV6ng.jpg#resized)
-->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:18:19.png#resized)

<!-- ![](https://sun9-72.userapi.com/impf/c845419/v845419956/1ed3f4/C8-p5pfV6ng.jpg) -->
*Australian Telescope, image # 20*

<!--
![](./images/Australian Telescope_files/JkdwWugjmrk.jpg#resized)
-->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:20:14.png#resized)

<!-- ![](https://sun9-76.userapi.com/impf/c845419/v845419956/1ed3fc/JkdwWugjmrk.jpg) -->
*Australian Telescope, image # 21*

<!--
![](./images/Australian Telescope_files/vl4V8JSzzk0.jpg#resized)
-->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:20:46.png#resized)

<!-- ![](https://sun9-38.userapi.com/impf/c845419/v845419956/1ed404/vl4V8JSzzk0.jpg) -->
*Australian Telescope, image # 22*

<!--
![](./images/Australian Telescope_files/h2mWTpuClh8.jpg#resized)
-->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:25:59.png#resized)

<!-- ![](https://sun9-74.userapi.com/impf/c845419/v845419956/1ed40c/h2mWTpuClh8.jpg) -->
*Australian Telescope, image # 23*

<!--
![](./images/Australian Telescope_files/_loAwWmROtU.jpg#resized)
-->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:27:19.png#resized)

<!-- ![](https://sun9-71.userapi.com/impf/c845419/v845419956/1ed415/_loAwWmROtU.jpg) -->
*Australian Telescope, image # 24*

Incidentally, I do not insist on this version at all. It may well be that the American contraption was already one of ours, made after studying those that were found.

![](./images/Australian Telescope_files/McDOdDII2HY.jpg#resized)

<!-- ![](https://sun9-13.userapi.com/impf/c845521/v845521768/1f533d/McDOdDII2HY.jpg) -->
*Australian Telescope, image # 25*

In no way do I want to belittle our ability to copy elementary techniques.

![](./images/Australian Telescope_files/dnOmpX9eQi8.jpg#resized)

<!-- ![](https://sun9-67.userapi.com/impf/c845521/v845521768/1f52d2/dnOmpX9eQi8.jpg) -->
*Australian Telescope, image # 26*

Moreover, we can always adapt everything that we have found in everyday life and use it as we see fit.

![](./images/Australian Telescope_files/XsJmWalcQnw.jpg#resized)

<!-- ![](https://sun9-2.userapi.com/impf/c850220/v850220921/12d7a2/XsJmWalcQnw.jpg) -->
*A whole observatory !!  In the woods!*

Far-fetched you think? Maybe it is.

![](./images/Australian Telescope_files/TTbkhr63gv4.jpg#resized)

<!-- ![](https://sun9-47.userapi.com/impf/c845521/v845521768/1f52c9/TTbkhr63gv4.jpg) -->
*Australian Telescope, image # 28*

In the end, a straight stick with a hollow interior, it is a stick in a telescope, and in a mortar, and in a mortar, and in a cannon. And even in a cocktail tube.

![](./images/Australian Telescope_files/hm6VJcLUXho.jpg#resized)

<!-- ![](https://sun9-28.userapi.com/impf/c845521/v845521768/1f5317/hm6VJcLUXho.jpg) -->
*Australian Telescope, image # 29*

[Video shows howitzer being fired. Author suggests the howitzer was cast from the same mould telescope/maser barrels were cast from](./images/Australian Telescope_files/video2.mp4)

And it is perfectly normal to use one workpiece later for other purposes. And even more so the casting mould (the form).

![](./images/Australian Telescope_files/ltw4h6qkm8c.jpg#resized)

<!-- ![](https://sun9-28.userapi.com/impf/c845521/v845521768/1f52db/ltw4h6qkm8c.jpg) -->
*Australian Telescope, image # 30*

You can at least look at our Soviet factories, which standardized products so that later, as quickly as possible, they could switch from the production lines for tape recorder spare parts to production lines for Kalashnikovs.

![](./images/Australian Telescope_files/l4V1bFGGZd4.jpg#resized)

<!-- ![](https://sun9-70.userapi.com/impf/c845521/v845521768/1f527e/l4V1bFGGZd4.jpg) -->
*A good master has a stick and shoots!*

And, of course, I do not want to slander our civilization and whitewash those from whom such gizmos remained.

![](./images/Australian Telescope_files/BvXYcHUxcYM.jpg#resized)

<!-- ![](https://sun9-59.userapi.com/impf/c845521/v845521768/1f53b0/BvXYcHUxcYM.jpg) -->
*Australian Telescope, image # 32*

If they were white and fluffy, they would not have died out in the scrapes of the cataclysm, which, apparently, they themselves arranged.

![](./images/Australian Telescope_files/3fP1tYYtfx0.jpg#resized)

<!-- ![](https://sun9-16.userapi.com/impf/c845521/v845521768/1f52e4/3fP1tYYtfx0.jpg) -->
*Australian Telescope, image # 33*

And the most important thing. It is not absolutely necessary to see statements here that past civilizations existed at all! I have no answers, I only have questions! Just take a look at the name of the band!

![](./images/Australian Telescope_files/utgLlEs4swU.jpg#resized)

<!-- ![](https://sun9-5.userapi.com/impf/c845521/v845521611/1fb473/utgLlEs4swU.jpg) -->
*True, I wonder why such giant telescopes are depicted in films of the beginning of the century, albeit grotesquely artistic?*

True, I wonder why such giant telescopes are depicted in films of the beginning of the century, albeit grotesquely artistic?

And it would be fine if they were joking, but no!

![](./images/Australian Telescope_files/bfmGbfOeEj0.jpg#resized)

<!-- ![](https://sun9-30.userapi.com/impf/c845521/v845521768/1f5396/bfmGbfOeEj0.jpg) -->
*Australian Telescope, image # 35*

And Uncle Foucault, by the way, who vehemently objected to the Australian telescope, then found money to build his own in Paris.

![](./images/Australian Telescope_files/IFNoJTaWY08.jpg#resized)

<!-- ![](https://sun9-28.userapi.com/impf/c845521/v845521611/1fb5a2/IFNoJTaWY08.jpg) -->
*Australian Telescope, image # 36*

![](./images/Australian Telescope_files/sx_WbS_FKM4.jpg#resized)

<!-- ![](https://sun9-37.userapi.com/impf/c845521/v845521611/1fb5b1/sx_WbS_FKM4.jpg) -->
*Australian Telescope, image # 37*

This miracle of the eye also looks strange, because the fasteners at its section-joints involuntarily hint at famous cannons.

![](./images/Australian Telescope_files/m6KM4gOwZ4k.jpg#resized)

<!-- ![](https://sun9-41.userapi.com/impf/c845521/v845521611/1fb5d5/m6KM4gOwZ4k.jpg) -->
*Australian Telescope, image # 38*

![](./images/Australian Telescope_files/uL-2xfGOSdE.jpg#resized)

<!-- ![](https://sun9-30.userapi.com/impf/c845521/v845521611/1fb5df/uL-2xfGOSdE.jpg) -->
*Australian Telescope, image # 39*

Which in and of themselves, we also understand, are not excellent guns. Because when test-fired, they broke in the most inappropriate places.

![](./images/Australian Telescope_files/TD3dI_Pti6M.jpg#resized)

<!-- ![](https://sun9-17.userapi.com/impf/c845521/v845521611/1fb609/TD3dI_Pti6M.jpg) -->
*Australian Telescope, image # 40*

Worse, it was assumed that they could be made of wood.

![](./images/Australian Telescope_files/MM1ES4_gW80.jpg#resized)

<!-- ![](https://sun9-8.userapi.com/impf/c845521/v845521611/1fb5e7/MM1ES4_gW80.jpg) -->
*And often not even for shooting.*

The funny thing is that we sometimes encounter wooden cannons on the battlefield.

<!-- ![](./images/Australian Telescope_files/gzXUcBP0reQ.jpg#resized) -->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:30:20.png#resized)

<!-- ![](https://sun9-32.userapi.com/impf/c854216/v854216768/2ce88/gzXUcBP0reQ.jpg) -->
*Australian Telescope, image # 42*

<!-- ![](./images/Australian Telescope_files/pXl3yRSrcCI.jpg#resized) -->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:31:19.png#resized)

<!-- ![](https://sun9-57.userapi.com/impf/c845521/v845521768/1f52f6/pXl3yRSrcCI.jpg) -->
*Australian Telescope, image # 43*

<!-- ![](./images/Australian Telescope_files/Vg2vrwdeNQk.jpg#resized) -->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:32:29.png#resized)

<!-- ![](https://sun9-58.userapi.com/impf/c849320/v849320768/17df72/Vg2vrwdeNQk.jpg) -->
*Australian Telescope, image # 44*

<!-- ![](./images/Australian Telescope_files/KzWNSo1Nhno.jpg#resized) -->

![](./images/Australian Telescope_files/Screenshot at 2021-03-27 07:33:05.png#resized)

<!-- ![](https://sun9-52.userapi.com/impf/c850024/v850024768/1794d0/KzWNSo1Nhno.jpg) -->
*Australian Telescope, image # 45*

True, everyone is sure now that these were fake guns in order to deceive the enemy's reconnaissance.

But back to composite products.

Here we see that, if necessary, they could even shoot around corners!!!

![](./images/Australian Telescope_files/-XVll7gM13k.jpg#resized)

<!-- ![](https://sun9-19.userapi.com/impf/c845521/v845521611/1fb611/-XVll7gM13k.jpg) -->
*Australian Telescope, image # 46*

![](./images/Australian Telescope_files/CNQAsLCk-jk.jpg#resized)

<!-- ![](https://sun9-24.userapi.com/impf/c845521/v845521611/1fb619/CNQAsLCk-jk.jpg) -->
*Australian Telescope, image # 47*

What contempt, however, for the laws of physics! Well, that's true if we're talking about cannonballs. And what if we are not?

But that device is a separate conversation. In the meantime, we can choose according to our taste, why there was a barrel with a grating-style mesh on the Australian telescope. For example, we can choose f we attribute to it not only geometric work with light (optics), but also wave work (microwaves).

![](./images/Australian Telescope_files/Ok3Vu4A1lPQ.jpg#resized)

<!-- ![](https://sun9-13.userapi.com/impf/c845521/v845521768/1f52b2/Ok3Vu4A1lPQ.jpg) -->
*Australian Telescope, image # 48*

Image text translated per [Yandex image translate](https://translate.yandex.com/)

![](./images/Australian Telescope_files/Wave Optics Screenshot at 2021-05-24 15:53:49.png#resized)

In terms of viewing celestial bodies, I would vote for diffraction.

![](./images/Australian Telescope_files/aKgQB-Sf4Nc.jpg#resized)

<!-- ![](https://sun9-24.userapi.com/impf/c845521/v845521768/1f52bb/aKgQB-Sf4Nc.jpg) -->

![](./images/Australian Telescope_files/Diffraction Grating Screenshot at 2021-05-24 16:32:55.png#resized) 

*Australian Telescope, image # 49*

![](./images/Australian Telescope_files/HE_HdgppG38.jpg#resized)

<!-- ![](https://sun9-65.userapi.com/impf/c845521/v845521768/1f52c2/HE_HdgppG38.jpg) -->
*Australian Telescope, image # 50*

But not necessarily. Because, it could be like this:

![](./images/Australian Telescope_files/7DNHaGocDKs.jpg#resized)

<!-- ![](https://sun9-28.userapi.com/impf/c845521/v845521768/1f535e/7DNHaGocDKs.jpg) -->
*Image legend says:*
*Red: Current in the inductor*
*Blue: Foucalt current in detail*
*White: Magnetic field*
*Moreover, Foucault has surfaced again!*

Or even like this:

![](./images/Australian Telescope_files/8KleZ-myVwU.jpg#resized)

<!-- ![](https://sun9-42.userapi.com/impf/c845521/v845521768/1f53b9/8KleZ-myVwU.jpg) -->
*Australian Telescope, image # 52*

Because, and so it happened:

![](./images/Australian Telescope_files/4gf2JEEhCIc.jpg#resized)

<!-- ![](https://sun9-25.userapi.com/impf/c845521/v845521768/1f5371/4gf2JEEhCIc.jpg) -->
*Australian Telescope, image # 53*

But most likely it was like this:

![](./images/Australian Telescope_files/XR3RDSbEUJM.jpg#resized)

<!-- ![](https://sun9-23.userapi.com/impf/c845521/v845521768/1f538d/XR3RDSbEUJM.jpg) -->
*Australian Telescope, image # 54*

![](./images/Australian Telescope_files/0Z1lcSc7uuQ.jpg#resized)

<!-- ![](https://sun9-56.userapi.com/impf/c845521/v845521392/1f68cb/0Z1lcSc7uuQ.jpg) -->
*Australian Telescope, image # 55*

Because, lately it seems to me that a long time ago everything was built around magnetic fields. But when I remember that, it seems I need to be baptized, so I never insist.

In short, this is not the end, this is the very beginning! To be continued…. Very soon!!

![](./images/Australian Telescope_files/XSnVF8DovX8.jpg#resized)

<!-- ![](https://sun9-18.userapi.com/impf/c845521/v845521392/1f68d3/XSnVF8DovX8.jpg) -->
*Father Himalaya's Pyrheliophor Assembled for the 1904 Lousiana Puchase Exposition.*

Translator's note: In the light of this interpretation of the mechanics and history of the Melbourne Telescope, the oddities in the history of British work on other telescopes such as May's in [http://www.oasi.org.uk/History/May.php](http://www.oasi.org.uk/History/May.php) might be worth more examination. Examples:

- original steering/pointing mechanism being reinstated after 150 years because it was more precise but used with a builder's tape measure instead of its original 'curious' calibration rods

- a telescope apparently acquired from a different owner - without the required technical knowledge to operate it

- telescopes half built and/or never used

These all suggest experimentation with poorly understood existing engineering than with new optics. And also confirms Pavel's view that the British wanted to remove some technologies from view.

Also, missing from [http://www.royalobservatorygreenwich.org/articles.php?article=3](http://www.royalobservatorygreenwich.org/articles.php?article=3) is William H.C. Bartlett's 1840 report into European observatories. Apparently, the report only exists in manuscript form held in the US Military Academy Library.

© All rights reserved. The original author retains ownership and rights.

