title: Summary table of reconstructions of southern history
date: 2017-10-22
modified: Tue 16 Feb 2021 12:08:01 GMT
category:
tags: forged history; catastrophe
slug:
authors: Andrei Stepanenko
summary: The first stage of the catastrophe took place around 1764, creating the Khvalyn Sea in place of the Caspian Sea and killing a mass of people to the  south and north of the Manych River, through which the snow-soil mixture was carried.
status: published
originally: 2707231.html
from: https://chispa1707.livejournal.com/2707231.html
local: 

#### Translated from:

[https://chispa1707.livejournal.com/2707231.html](https://chispa1707.livejournal.com/2707231.html)

Note: This piece probably requires having read and understood [http://the-small-joys.blogspot.com/2017/03/25032017_25.html](http://the-small-joys.blogspot.com/2017/03/25032017_25.html)

or its longer version (English translation) at:

[Complete history of catastrophic crash (variant No. 3 of 1/29/2020)](./complete-history-of-catastrophic-crash-variant-no-3-of-1292020.html).

**Russian:**

Итак, события в Крыму, Хазарии, Орде, Казани, Астрахани старой, Астрахани новой, Тюмени кавказской, Кабарде, Азове, на Тамани за 1700 лет насильно, но по системе, загнаны в 6-летний шаблон с 1764 по 1769 (1770) год. В результате у исторических событий появилась логика, а ряды событий в разных регионах оказались тесно связанными.

**English:**

So for 1700 years, events in Crimea, Khazaria, Horde, Kazan, old Astrakhan, new Astrakhan, Tyumen Caucasia, Kabarda, Azov, on Taman were systematically driven into a 6-year pattern and forced to fit into 1764 to 1769 (1770). As a result, historical events show a logic, and rows of events in different regions are closely linked.

**Russian:**

ОСНОВНОЕ: Около 1764 года прошел первый этап катастрофы, создавший Хвалынское море на месте Каспийского и убивший массу населения южнее и севернее Маныча, по которому и несло снежно-грунтовую смесь.

Описание этапов катастрофы здесь: http://the-small-joys.blogspot.ru/2017/03/25032017_25.html

**English:**

Stages of the catastrophe are described here: [http://the-small-joys.blogspot.ru/2017/03/25032017_25.html](http://the-small-joys.blogspot.ru/2017/03/25032017_25.html)

The first stage of the catastrophe took place around 1764, creating the Khvalyn Sea in place of the Caspian Sea and killing a mass of people to the south and north of the Manych River, through which a snow-soil mixture was carried.

**Russian:**

Этот этап вызвал Великое переселение народов, разрушил экономическое равновесие и физически уничтожил часть элиты, на которой держалось равновесие политическое. Так и перестало существовать Циркумпонтийское сообщество, наследниками которого мы все являемся.

**English:**

This phase caused the Great Migration of peoples, destroyed the economic equilibrium, and physically annihilated a part of the elite, on which the political equilibrium was based. This is how the Circumpontic (Black Sea-surrounding) community, of which we are all heirs, ceased to exist.

**Russian:**

Затем началось проседание шельфов, и около 1766-1768 годов Маныч стал ненадолго судоходен, что вызвало острый интерес к региону со стороны крупных сил. Результат: в регион вошли Османы и питерская Россия. 1769 году началась трансгрессия хвалынского моря через Маныч, и флот и армия Османов целиком погибли. Событие отнесено в 1569 год. Россия осталась в регионе одна, без крупных конкурентов, что привело к быстрой консолидации местных народов вокруг России. Основная причина консолидации: черноморские порты начали быстро тонуть, и стало ясно, что вывозить товар с Кавказа неоткуда, и осталась только Волга.

**English:**

Then the continental shelves began to subside and around 1766-1768 the Manych River became navigable for a short time - which aroused keen interest in the region on the part of big powers. The result: the Ottomans and St Petersburg's Russia entered the region. In 1769 the transgression of the Khvalyn Sea through the Manych began, and the fleet and army of the Ottomans perished entirely. The event is set back to 1569. Russia was left alone in the region, without major competitors, which led to rapid consolidation of local people around Russia. The main reason for consolidation: the Black Sea ports began to sink, and it became clear that there was nowhere through which to export goods from the Caucasus, leaving only the Volga.

**Russian:**

Второстепенные детали выбросил, оставил только основное. Таблица для ЖЖ велика, поэтому прячу под кат. Но на деле, она вышла очень компактной. Видна глубинная связь ВСЕХ событий как по вертикали, так и по горизонтали, это все - одно историческое целое. ДРУГИХ ПРОЦЕССОВ ЗА 1700 ЛЕТ писаной истории в регионе ПОПРОСТУ НЕТ. Зеленым цветом - начало процесса, желтым - завершение процесса. Незавершенным остался один процесс - в Кабарде. Для тех, кто не любит такую подачу, ниже таблицы дам то же самое, но построчно и поблочно.

**English:**

I threw out the secondary details, leaving only the main ones. The table is too big for a LiveJournal page, so I'm keeping it under the covers. But in fact it is very compact. You can see the deep connection between ALL the events both vertically and horizontally; it is all one historical whole. There are simply no other processes in the region in 1700 years of recorded history. Green is the beginning of the process, yellow is the end of the process. There is one process left unfinished - in Kabarda. For those who do not like this presentation, below the table I give the same, but line by line and block by block.

[![](https://ic.pics.livejournal.com/chispa1707/24769663/1353881/1353881_original.png#clickable)](https://ic.pics.livejournal.com/chispa1707/24769663/1353881/1353881_original.png)

**Russian:**

ТЕПЕРЬ - ТО ЖЕ САМОЕ, НО ПРОСТЫМ ТЕКСТОМ

**English:**

Now the same, but in plain text:

**Russian:**

| | | ХОД КАТАСТРОФЫ |
|:-- | --- |:-- |
| 1764 | | Образование Хвалынского моря, гибель прикаспийских городов и крепостей Кубани |
| 1765 | | Великое Переселение Народов на земли Крыма |
| 1766 | | Вывоз хлеба из Крыма и Черкесии прекращен |
| 1767 | |  |
| 1768 | |  |
| 1769 | | Трансгрессия Каспийского моря, переток по Манычу, удар по Азову |
| 1770 | |  |
| | | |
| | | АЗОВ |
| 1764 | | Азов у русских |
| 1765 | | Война русских и турок за Азов и Лютик. Азов сожжен |
| 1766 | | Русские взяли Азов, борьба за Лютик |
| 1767 | | Борьба с турками за Лютик, Азов пока за Россией |
| 1768 | | Русско-турецкая война, борьба за Лютик |
| 1769 | | Турки взяли Азов, но флот погиб, и Азов ушел России |
| 1770 | | Азов закреплен за Россией |
| | | |
| | | ХАДЖИ-ТАРХАН |
| 1764 | | Разрушение города |
| 1765 | | Раскол Орды, город бесхозен |
| 1766 | | Борьба Москвы, татар и казаков за город |
| 1767 | | Город взяли черкесы |
| 1768 | | Казаки отбили город, а крымцы не сумели |
| 1769 | | Османы вошли в регион |
| 1770 | | Османы отказались от Хаджи-Тархана. |
| | | |
| | | АСТРАХАНЬ |
| 1764 | | Орда и калмыки прибывают, прежний город разрушен |
| 1765 | | Иностранцы оценивают возможности для порта |
| 1766 | | Основание ставки Орды и Астраханского царства |
| 1767 | | Основание города Астрахань |
| 1768 | | Русские строят новый город, разбирая старый |
| 1769 | | Русский порт сильно пострадал от Каспия |
| 1770 | |  |
| | | |
| | | КАБАРДА |
| 1764 | | Крымцы, ногайцы, калмыки, киевляне, монголы вошли в Кабарду |
| 1765 | | Аланы, хазары, арабы, крымцы, калмыки, ногайцы воюют за позиции в Кабарде |
| 1766 | | Крым входит в Кабарду. Аланы воюют с хазарами |
| 1767 | | Крымцы и калмыки в Кабарде. Аланы воюют с хазарами |
| 1768 | | Калмыки, ногайцы и крымцы в Кабарде. Волнения в Чечено-Ингушетии |
| 1769 | | Калмыки, ногайцы, монголы и крымцы в Кабарде. |
| 1770 | | Волнения в Чечено-Ингушетии. Крымцы в Кабарде |
| | | |
| | | РОССИЯ В КАБАРДЕ |
| 1764 | | Часть кабардинцев примыкает к России |
| 1765 | | Раскол в Кабарде. Часть кабардинцев примыкает к России. Посольство к Елизавете |
| 1766 | | Раскол в Кабарде. Часть кабардинцев примыкает к России. Чечня примыкает к Москве |
| 1767 | | Раскол в Кабарде. |
| 1768 | | Русско-турецкая война. Кабарда держит нейтралитет. Персов втягивают в конфликт |
| 1769 | | Ввод русских войск в Кабарду. Персидский поход Петра. Прием кабардинцев на службу |
| 1770 | | Прием кабардинцев на службу |
| | | |
| | | КАЗАНЬ |
| 1764 | | Переворот в Казани |
| 1765 | | Выход Казани из Орды |
| 1766 | | Казань примкнула в Москве |
| 1767 | | Казань отошла от Москвы |
| 1768 | | Борьба Москвы за Казань |
| 1769 | | Москва присоединила Казань |
| 1770 | |  |
| | | |
| | | КРЫМ |
| 1764 | | Крым независим от Орды. Монголы, казаки, русские посягают на Крымские земли |
| 1765 | | Множественные походы соседей на земли крымцев |
| 1766 | | Множественные походы соседей на земли крымцев |
| 1767 | | Литовское присутствие в Крыму |
| 1768 | | Русско-турецкая война за влияние на Крымские земли |
| 1769 | | Вторжение русских и турок в Крым |
| 1770 | | Османы, понеся крупные потери, на время отошли |
| | | |
| | | ГЕНУЯ В КРЫМУ |
| 1764 | | |
| 1765 | | Генуя в Керчи, Судаке, Кафе. Конфликт в Кафе |
| 1766 | | Конфликт с Генуей за Кафу |
| 1767 | | Конфликт с Генуей за Кафу |
| 1768 | | Борьба с Феодоро за Чембало. Судак перешел татарам |
| 1769 | | Генуя платит Хаджи и признает Венецию |
| 1770 | | Генуя передала права на колонии своему банку |
| | | |
| | |  ФЕОДОРО В КРЫМУ |
| 1764 | | Договор Феодоро и Генуи о разделе сфер влияния |
| 1765 | | Феодоро в 1765 году еще цело, и с ним считаются |
| 1766 | | Феодоро роднятся с императорскими семьями. Ротация епархий Феодоро |
| 1767 | | Союз с Византией. Феодориты с их правами на орла получили удел в Москве |
| 1768 | | Обычная жизнь |
| 1769 | | Конфликт в семье. Османы вошли в столицу Феодоро |
| 1770 | | Закат Феодоро |
| | | |
| | | ТАМАНЬ |
| 1764 | | Великое Переселение Народов, захваты разрушенных и брошенных крепостей |
| 1765 | | Захваты разрушенных крепостей |
| 1766 | | Черкесы и русы делят регион |
| 1767 | | Киев и Москва вторгаются в регион |
| 1768 | | Терцы громят крепости, им эти крепости - помеха |
| 1769 | | Москва отжимает регион у черкесов и казаков |
| 1770 | |  |
| | | |
| | | ТЮМЕНЬ |
| 1764 | | Раскол среди местных князей |
| 1765 | | Часть князей Тарковских и Тюмени примыкает к России |
| 1766 | | Число союзников России среди князей растет |
| 1767 | | Число союзников России среди князей растет |
| 1768 | | Борьба России за Тарки. Неудачное вторжение на Сулак |
| 1769 | | Россия присоединила Тарки |
| 1770 | |  |
| | | |
| | | ТЕРКИ И КИЗЛЯР |
| 1764 | | Основание крепости Терки и Кизляра, помогает шотландец |
| 1765 | | Нападения на Терки, образование старшинства казаков |
| 1766 | | Кизляр сгорел, Терки оставлены |
| 1767 | | Терки перенесены на Сунжу, Кизляр восстанавливают |
| 1768 | | Основаны Терки на Копани, Кизляр восстанавливают |
| 1769 | | Перевод Терского гарнизона |
| 1770 | | Пошлины в Кизляре берут со всех |
| | | |
| | | ХАЗАРИЯ |
| 1764 | | Исход хазар в Закавказье, Крым, на Русь и на Дунай |
| 1765 | | Византия в конфликте с хазарами. Переговоры хазар и Византии |
| 1766 | | Хазаро-византийский союз против неких арабов в Закавказье |
| 1767 | | Поход Киева на хазар. Киевская Русь правит в Хазарии |
| 1768 | | Строятся новые крепости хазар и булгар |
| 1769 | | Хазары и адыги наступают на Киев. На деле, это массовое бегство хазар в Крым и на Русь |
| 1770 | |  |
| | | |
| | | ОРДА |
| 1764 | | Орда рушится |
| 1765 | | Распад Орды |
| 1766 | | Передел в Орде и Крыму |
| 1767 | | Передел в Орде и Крыму |
| 1768 | |  |
| 1769 | |  |
| 1770 | |  |
| | | |
| | | СЕМЬЯ ГИЗОЛЬФИ НА ТАМАНИ |
| 1764 | | Выжиди лишь 180 генуэзских семей, Гизольфи обратились за помощью в Геную |
| 1765 | | Гизольфи обратился за помощью к Москве, но безрезультатно |
| 1766 | |  |
| 1767 | |  |
| 1768 | |  |
| 1769 | | Гизольфи пытается бежать из региона, но его насильно вернул князь Молдовы |
| 1770 | | Гизольфи на службе у Менгли-Герая |
| | | |
| | | ДОНСКИЕ КАЗАКИ |
| 1764 | | Неподчинение казаков прибывшим на переговоры питерцам |
| 1765 | | Черкасск примкнул к России |
| 1766 | | Борьба за Черкасск, ставка казаков разрушена |
| 1767 | | Таможенная реформа, льготы казакам, мятеж на Дону, борьба за Черкасск |
| 1768 | | Борьба за Черкасск |
| 1769 | | Монополия питерских на таможню, казаков принуждают к подчинению |
| 1770 | | Начало российского служилого казачества |
| | | |
| | | РЕЛИГИЯ |
| 1764 | | |
| 1765 | | Ислам и иудаизм в Хазарии. Мечети в Крыму. Льготы мусульманам |
| 1766 | |  |
| 1767 | | Мечети в Крыму |
| 1768 | | Мечети в Крыму. Борьба иудаизма с конкурентами по вере |
| 1769 | | Хазары-мусульмане вытесняют хазар-иудеев |
| 1770 | |  |

**English:**

| | | THE CASE OF CATASTROPHE |
|:-- | --- |:-- |
| 1764 | | Formation of the Khvalyn Sea, destruction of Kuban's Caspian towns and fortresses |
| 1765 | | The Great Migration of Peoples to the Crimea |
| 1766 | | Export of bread from the Crimea and Circassia was stopped. |
| 1767 | |  |
| 1768 | |  |
| 1769 | | Transgression of the Caspian Sea, overflow on Manich, strike on Azov |
| 1770 | |  |
| | | |
| | | AZOV |
| 1764 | | The Russians have Azov |
| 1765 | | War of the Russians and Turks for Azov and Buttercup. Azov is burnt down. |
| 1766 | | Russians captured Azov, fighting for Buttercup. |
| 1767 | | Struggle with the Turks for Lyutik, Azov is still Russian. |
| 1768 | | Russian-Turkish War, fighting for Buttercup |
| 1769 | | The Turks took Azov, but the fleet perished, and Azov went to Russia. |
| 1770 | | Azov was ceded to Russia. |
| | | |
| | | KHADZHI TARKHAN |
| 1764 | | Destruction of the city |
| 1765 | | Partition of the Horde, the city is abandoned. |
| 1766 | | Struggle of Moscow, Tatars and Cossacks for the city |
| 1767 | | The city was taken by the Circassians |
| 1768 | | The Cossacks recaptured the city, the Crimeans failed. |
| 1769 | | The Ottomans entered the region |
| 1770 | | The Ottomans gave up Hadji Tarkhan. |
| | | |
| | | ASTRAKHAN |
| 1764 | | Horde and Kalmyks arrive, former city destroyed |
| 1765 | | Foreigners assess opportunities for the port |
| 1766 | | Foundation of the Horde and the Kingdom of Astrakhan |
| 1767 | | Founding of the city of Astrakhan |
| 1768 | | Russians build a new city while demolishing the old one |
| 1769 | | Russian port heavily damaged by the Caspian Sea |
| 1770 | |  |
| | | |
| | | KABARDA |
| 1764 | | Crimeans, Nogais, Kalmyks, Kievs, Mongols entered Kabarda |
| 1765 | | Alans, Khazars, Arabs, Crimeans, Kalmyks, Nogais fight for positions in Kabarda |
| 1766 | | Crimea enters Kabarda. The Alans are at war with the Khazars. |
| 1767 | | Crimeans and Kalmyks in Kabarda. Alans are at war with the Khazars. |
| 1768 | | Kalmyks, Nogais and Crimeans in Kabarda. Unrest in Chechen-Ingushetia |
| 1769 | | Kalmyks, Nogais, Mongols and Crimeans in Kabarda. |
| 1770 | | Unrest in Chechen-Ingushetia. Crimeans in Kabarda |
| | | |
| | | RUSSIA IN KABARDA |
| 1764 | | Some Kabardins joined Russia |
| 1765 | | A schism in Kabarda. Some Kabardins joined Russia. An embassy to Elizabeth. |
| 1766 | | A schism in Kabarda. Some Kabardins joined Russia. Chechnya adjoins Moscow. |
| 1767 | | A schism in Kabarda. |
| 1768 | | The Russo-Turkish War. Kabarda held neutrality. The Persians are drawn into the conflict. |
| 1769 | | Russian troops enter Kabarda. Peter's campaign to Persia. Recruitment of Kabardians |
| 1770 | | The admission of Kabardinians to the service. |
| | | |
| | | KAZAN |
| 1764 | | Coup d'etat in Kazan |
| 1765 | | Kazan left the Horde |
| 1766 | | Kazan joined Moscow |
| 1767 | | Kazan broke away from Moscow |
| 1768 | | Moscow's Struggle for Kazan |
| 1769 | | Moscow annexed Kazan |
| 1770 | |  |
| | | |
| | | CRIMEA |
| 1764 | | The Crimea is independent of the Horde. Mongols, Cossacks and Russians encroach on the Crimean lands. |
| 1765 | | Multiple Attacks by Neighbours on Crimean Lands |
| 1766 | | Multiple forays by neighbours onto Crimean lands |
| 1767 | | Lithuanian presence in the Crimea |
| 1768 | | Russo-Turkish War for control over the Crimean lands |
| 1769 | | Russian and Turkish invasion of Crimea |
| 1770 | | The Ottomans, having suffered heavy losses, withdrew for a time |
| | | |
| | | GENOUSLY (Genghis) IN THE CRIMEA |
1764 |
| 1765 | | Genoa in Kerch, Sudak, Cafe. Conflict in the Cafa |
| 1766 | | Conflict with Genoa over Cafa |
| 1767 | | Conflict with Genoa about Cafa |
| 1768 | | Struggle with Theodoro for Cembalo. Sudak goes to the Tatars. |
| 1769 | | Genoa pays the Haji and recognizes Venice |
| 1770 | | Genoa cedes the rights to the colonies to its bank. |
| | | |
| | | THEODORO IN CRIMEA |
| 1764 | | Treaty of Theodoro and Genoa on the division of spheres of influence |
| 1765 | | Theodoro in 1765 is still intact and is reckoned with. |
| 1766 | | Theodoro is related to the imperial families. Rotation of the dioceses of Theodoro |
| 1767 | | Union with Byzantium. Theodoro with their eagle rights are given an inheritance in Moscow. |
| 1768 | | Routine life |
| 1769 | | Conflict in the family. The Ottomans entered the capital of Theodoro |
| 1770 | | Theodoro's decline |
| | | |
| | | TAMAN |
| 1764 | | Great Migration of Peoples, Seizure of ruined and abandoned forts |
| 1765 | | Seizure of ruined forts |
| 1766 | | Circassians and Russians divide the region |
| 1767 | | Kiev and Moscow invade the region |
| 1768 | | Tertses smashing fortresses, these fortresses are a hindrance for them. |
| 1769 | | Moscow wrests the region from the Circassians and Cossacks. |
| 1770 | |  |
| | | |
| | | TUMEN |
| 1764 | | Schism among local princes |
| 1765 | | Part of the Dukes of Tarkovsk and Tyumen adjoin Russia |
| 1766 | | The number of Russia's allies among the princes increases |
| 1767 | | The number of Russia's allies among the princes grows |
| 1768 | | Russia's fight for the Tarki. Unsuccessful invasion of the Sulak |
| 1769 | | Russia annexes the Tarki |
| 1770 | |  |
| | | |
| | | TARKI AND KIZLAR |
| 1764 | | Founding of the fortress of Terki and Kizlyar, Scotsman helps |
| 1765 | | Attacks on Terki, formation of Cossack eldership |
| 1766 | | Kizlyar burned down, Terki was left |
| 1767 | | Turks were moved to Sunzha, Kizlyar was restored. |
| 1768 | | Turki were founded on Kopan, Kizlyar was restored. |
| 1769 | | Tersky garrison relocated |
| 1770 | | Tolls are taken in Kizlyar |
| | | |
| | | KHAZARIA |
| 1764 | | Exodus of Khazars to Transcaucasia, Crimea, Russia and Danube |
| 1765 | | Byzantium in conflict with the Khazars. Negotiations between the Khazars and Byzantium. |
| 1766 | | Khazar-Byzantine alliance against some Arabs in Transcaucasia |
| 1767 | | Kiev's campaign against the Khazars. Kievan Rus rules over the Khazars. |
| 1768 | | New forts of Khazars and Bulgars are built. |
| 1769 | | Khazars and Adygs attack Kiev. In fact, it is a mass exodus of Khazars to the Crimea and Rus. |
| 1770 | |  |
| | | |
| | | ORDA |
| 1764 | | The Horde collapses. |
| 1765 | | Disintegration of the Horde |
| 1766 | | Redistribution in the Horde and the Crimea |
| 1767 | | Redistribution in the Horde and the Crimea |
| 1768 | |  |
| 1769 | |  |
| 1770 | |  |
| | | |
| | | THE GIZOLFI FAMILY ON TAMAN |
| 1764 | | With only 180 Genoese families surviving, the Ghisolfi turned to Genoa for help. |
| 1765 | | The Ghisolfis appealed to Moscow for help, but to no avail |
| 1766 | |  |
| 1767 | |  |
| 1768 | |  |
| 1769 | | Ghisolfi tries to flee the region, but is forcibly returned by the Prince of Moldavia |
| 1770 | | Gisolfi in the service of Mengli-Herai. |
| | | |
| | | THE DON COSSACKS |
| 1764 | | The insubordination of the Cossacks to the Peterburgers who came to negotiate |
| 1765 | | Cherkassk was annexed to Russia. |
| 1766 | | Struggle for Cherkassk, the Cossack headquarters was destroyed |
| 1767 | | Customs reform, privileges for the Cossacks, mutiny on the Don, fighting for Cherkassk. |
| 1768 | | Struggle for Cherkassk |
| 1769 | | The Piters' monopoly on customs, Cossacks were forced to obey |
| 1770 | | Beginning of the Russian Service Cossacks |
| | | |
| | | RELIGION |
| 1764 | | |
| 1765 | | Islam and Judaism in Khazaria. Mosques in the Crimea. Privileges for Muslims. |
| 1766 | | |
| 1767 | | Mosques in the Crimea. |
| 1768 | | Mosques in the Crimea. Judaism struggles with rival faiths |
| 1769 | | Muslim Khazars ousting Jewish Khazars |
| 1770 | | |


© All rights reserved. The original author retains ownership and rights.
