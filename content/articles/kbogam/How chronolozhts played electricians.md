title: How LiveJournal's Alt Historians Played at Being Electricians
date: 2019-02-26
modified: Mon 24 May 2021 16:18:15 BST
category:
tags: technology
slug:
authors: Pavel Verkhov (Alexander Alekseev)
from: https://vk.com/@kbogam-kak-hronolozhcy-v-elektrikov-igrali
originally: How chronolozhts played electricians.md
local: How chronolozhts played electricians_files
summary: And the substance that was banned is mercury. It was impossible to seize it all en masse, because it was also used in other places, without which even those who started the whole carnival could not get by. But it was surrounded by such vigorous propaganda that no one in their right mind would touch it now.
status: published

#### Translated from:

[https://vk.com/@kbogam-kak-hronolozhcy-v-elektrikov-igrali](https://vk.com/@kbogam-kak-hronolozhcy-v-elektrikov-igrali)

*Translator's note: 'Chronolozhians' and its variants in this piece are taken to mean 'LiveJournal Alternative Historians'*

Quite by chance, it turned out that such, as it seemed to me, an ancient question, studied from all sides, such as the ancient Greek Roman, and others like them, and all subsequent architectural delights, not yet fully understood by all Chronolozhians.

Therefore, I decided to slightly move the second part of the young Indo-Sanskrit-Buddhist culture and urgently go over the versions of construction riddles, questions, assumptions. This easy educational program does not carry any new revelations, I just decided to collect in one place a few not quite crazy options. You could even say plausible.

I am simply obliged to do this so that people who accidentally ran here into the light of the ether would not think that I am an architectural maniac who uploads hundreds of photographs of old, new, built, unfinished and even Piranesi-painted structures. As some kind of archiphile, God forgive me.

But this is the trouble with all architectural theories associated with the use of premises in a different way than the way we are now used to. Everyone pounds their chest that they know how it worked. They draw diagrams, draw parallels, splatter graphs and formulas. The ambush is still a trifle - everyone knows to know, they cannot turn it on.

And nevertheless, I believe that almost all the proposed options have the right to life. Under certain coincidences (which exactly, few know), each ancient Greek-Roman booth by an easy revolution of the planet around someone's axis again becomes a useful technological object.

Let's start with the very foundation, so to speak. What the Earth stands on. And according to historians, it stands on faith in God. Or gods. In their opinion, people in their history devoted all available time to prayers. In the intervals between farming, conception and childbirth, they hung out in temples. Either they knocked their foreheads at the feet of some genius, or they crawled in prayer to the next idol.

Crawling on the damp earth and banging their heads on a tree, it seemed a little to them, and they began to build temples, cathedrals, churches and other places of entertainment. From that moment on, life in the past became even more dense. It was necessary to have time not only to sow wheat and read a prayer to every grain, but also to build a temple the size of a city. At the same time, it is quite natural to live in dugouts ourselves.

Due to the fact that even a clinical idiot (read historian) could not completely believe in such obscurantism of their ancestors, all these stories about piety eventually overflowed the cup of patience of some citizens and they began to think, why then all over the world, in in every city and village, regardless of countries and continents, the ancestors poked their chapels.

It was especially strange that most of the buildings for communication with the gods were very similar to each other and depended little on culture, the age of construction and oh, horror, religion.

<!--
![](https://sun9-17.userapi.com/impf/c851136/v851136859/c6b9f/3o5g6Mk9Whg.jpg)
-->
![](./images/How chronolozhts played electricians_files/3o5g6Mk9Whg.jpg#resized)

*May I not post them here, there are a lot of them on the websites of the Chronolites and in our group.*

The first thing that came to mind was the telegraph. But then, someone remembered that, for example, in ancient Rome there was no telegraph, but there were temples. And logic dictated that the Peter and Paul Fortress, tall and thin, as a means of communication between the swashbuckler on the spire and another equally unfortunate person in the line of sight without fog and other precipitation impairing visibility, it still did not go far. Although, of course, the owl tried to burst several times while it was pulled on such a globe sewn with white threads. But some Isaac's Cathedral or St. Peter's Cathedral, which is wider across itself, from which side you look, as a perch for signalmen, is simply ridiculous to rebuild.

Then they decided to go the scientific way. We walked with difficulty, because most of the chronologers of the humanities. In principle, a thought can arise from them, but she has little chance of reaching the physical expression of the principles of work or a prototype without the participation of God, aliens, pure and impure esoteric force. It's good that we ran into someone with a technical education in time, otherwise the forces of our ancestors and the relics of saints would have drowned in the wells.

Technicians immediately threw aside power, saying that the only thing they can emit is infection. Especially after being kissed by a crowd of idiots.

<!--
![](https://sun9-60.userapi.com/impf/3ZspF5770-drP6kUj5dpnpslU1_nkf2pp789Sw/LucRY1LBiQw.jpg)
-->
![](./How chronolozhts played electricians_files/LucRY1LBiQw.jpg#resized)


*I understand that sometimes a person desperately clings to any straw.  But not for the corpse!!!*

<!--
![](https://sun9-26.userapi.com/impf/c849420/v849420632/13ab5a/0p1H_lK9haI.jpg)
-->
![](./images/How chronolozhts played electricians_files/0p1H_lK9haI.jpg#resized)

*How chronolozhts played electricians, image # 3*

I understand that sometimes a person desperately clings to any straw. But not for the corpse !!!

The power of the ancestors, of course, could help, in principle, but since you did not receive a good inheritance from them, it means that your family did not have such power.

To understand why the ancestors needed such cyclopean structures and why they first built them in a new place, we decided to study what, in principle, is a church building. They took the drawings from the nearest priest and began to peer intently.

<!--
![](https://sun9-46.userapi.com/impf/c850724/v850724555/c8fbb/d0y-ueHIvEo.jpg)
-->
![](./images/How chronolozhts played electricians_files/d0y-ueHIvEo.jpg#resized)

![](./images/How chronolozhts played electricians_files/d0y-ueHIvEo_english.png#resized)

*Many were amazed.  Even the names of parts of the temple complexes resembled something from a physics course (scroll through the photos)*

<!--
![](https://sun9-3.userapi.com/impf/c848532/v848532059/13ce8a/qxb-WdaVTm8.jpg)
-->
![](./images/How chronolozhts played electricians_files/qxb-WdaVTm8.png#resized)

![](./images/How chronolozhts played electricians_files/qxb-WdaVTm8_english.png#resized)

*How chronolozhtsy played electricians, image # 5*

<!--
![](https://sun9-60.userapi.com/impf/c848532/v848532059/13ce93/PsQh-hKUKtQ.jpg)
-->
![](./images/How chronolozhts played electricians_files/PsQh-hKUKtQ.png#resized)

![](./images/How chronolozhts played electricians_files/Vs0W561BwNk_english.png#resized)
<!-- Same image, different names -->

*How chronolozhtsy played electricians, image # 6*

<!--
![](https://sun9-37.userapi.com/impf/c848532/v848532059/13ce9d/ILgVd-7gfOA.jpg)
-->
![](./images/How chronolozhts played electricians_files/ILgVd-7gfOA.png#resized)

![](./images/How chronolozhts played electricians_files/ILgVd-7gfOA_english.png#resized)

*How chronolozhtsy played electricians, image # 7*

Many were amazed. Even the names of parts of the temple complexes resembled something from a physics course (scroll through the photos)

The structure of prayer facilities turned out to be very similar to each other, and conceptually did not change even in different religions.

We started comparing the elements. The dome was the first to give itself out, it was painfully redundant in terms of the complexity of its construction and contained inadequate architectural elements.

<!--
![](https://sun9-29.userapi.com/impf/c850724/v850724428/c769d/8w6IbfIQeBE.jpg)
-->
![](./images/How chronolozhts played electricians_files/8w6IbfIQeBE.jpg#resized)
*The lower dome is needed to reinforce the upper one.*

<!--
![](https://sun9-68.userapi.com/impf/c848532/v848532388/13fdb6/ouepZVcdTic.jpg)
-->
![](./images/How chronolozhts played electricians_files/ouepZVcdTic.jpg#resized)
*How chronolozhtsy played electricians, image # 9*

<!--
![](https://sun9-29.userapi.com/impf/c848532/v848532388/13fdbe/M7SilZcwhB0.jpg)
-->
![](./images/How chronolozhts played electricians_files/M7SilZcwhB0.jpg#resized)
*How chronolozhtsy played electricians, image # 10*

The lower dome is needed to reinforce the upper one.

The word amplification here made some who were fond of radio amateurs remember a detector receiver that worked without batteries, a plug - only from the energy of the radio wave (not the fact that this explanation from the textbook is true, because, despite the "energy of the radio wave", it required an antenna and grounding , i.e., to work, it seems to need the potential difference of the Earth's electric field), but at the same time it allowed it to listen to radio stations.

<!--
![](https://sun9-12.userapi.com/impf/c850724/v850724428/c76b5/vhcsgUo0t4o.jpg)
-->
![](./images/How chronolozhts played electricians_files/vhcsgUo0t4o.jpg#resized)
*The most difficult part was winding the wire around the ferrite core of the coil.*

The most difficult part was winding the wire around the ferrite core of the coil.

<!--
![](https://sun9-56.userapi.com/impf/c851420/v851420428/c9f29/kwmWcjYPyxY.jpg)
-->
![](./images/How chronolozhts played electricians_files/kwmWcjYPyxY.jpg#resized)
*It is not necessary to take off to point G in order to get, as in a plus and minus outlet, the difference in the minus field available on the surface will be enough to get a difference and the possibility of using it to generate electricity.*

It is not necessary to take off to point G in order to get, as in a plus and minus outlet, the difference in the minus field available on the surface will be enough to get a difference and the possibility of using it to generate electricity.

A picture began to form in the minds of the Chronologians. They were helped in this not only by the detector receiver, but also by pictures from the past.

<!--
![](https://sun9-31.userapi.com/impf/c851420/v851420428/c9ef5/F1U90EXCKdM.jpg)
-->
![](./images/How chronolozhts played electricians_files/F1U90EXCKdM.jpg#resized)
*Where do these illumination holidays come from if there are no power plants yet, and you can't light so many candles*

<!--
![](https://sun9-72.userapi.com/impf/c850728/v850728428/ccb2d/-EmU9MDihj0.jpg)
-->
![](./images/How chronolozhts played electricians_files/-EmU9MDihj0.jpg#resized)
*How chronolozhtsy played electricians, image # 14*

<!--
![](https://sun9-33.userapi.com/impf/c850728/v850728428/ccb3d/XIxEzfAWIQs.jpg)
-->
![](./images/How chronolozhts played electricians_files/XIxEzfAWIQs.jpg#resized)
*How chronolozhtsy played electricians, image # 15*

<!--
![](https://sun9-71.userapi.com/impf/c850728/v850728428/ccb36/GT6ABE6BXCk.jpg)
-->
![](./images/How chronolozhts played electricians_files/GT6ABE6BXCk.jpg#resized)
*How chronolozhtsy played electricians, image # 16*

Where do these illumination holidays come from if there are no power plants yet, and you can't light so many candles

Since the mythical esoteric and divine powers were rejected, there was only one source of energy known to mankind - electricity.

We began to look even more closely and found that all churches, regardless of confessions or distance from the center, are directly penetrated with metal ties from head to toe. Or, crossing himself, from the basement to the cross.

<!--
![](https://sun9-16.userapi.com/impf/c851420/v851420428/c9f22/9rI83IXUBDI.jpg)
-->
![](./images/How chronolozhts played electricians_files/9rI83IXUBDI.jpg#resized)
*To see them, it was not even required to take away the temple from the nearest priest and disassemble it brick by brick.  Thank God, there are useful idiots who run through various ruins and worry that the Lord will be very offended if this transformer is not urgently restored in the Orthodox style.*

<!--
![](https://sun9-63.userapi.com/impf/c850728/v850728428/ccb60/v8DLZU8xNa0.jpg)
-->
![](./images/How chronolozhts played electricians_files/v8DLZU8xNa0.jpg#resized)
*How chronolozhts played electricians, image # 18*

<!--
![](https://sun9-8.userapi.com/impf/c850216/v850216388/f3ced/BdKJrZHHk1o.jpg)
-->
![](./images/How chronolozhts played electricians_files/BdKJrZHHk1o.jpg#resized)
*How chronolozhts played electricians, image # 19*

<!--
![](https://sun9-46.userapi.com/impf/c850216/v850216388/f3cf5/gpLUilxnAyI.jpg)
-->
![](./images/How chronolozhts played electricians_files/gpLUilxnAyI.jpg#resized)
*How chronolozhts played electricians, image # 20*

<!--
![](https://sun9-43.userapi.com/impf/c850216/v850216388/f3cfd/QiCECVm7MW8.jpg)
-->
![](./images/How chronolozhts played electricians_files/QiCECVm7MW8.jpg#resized)
*How chronolozhtsy played electricians, image # 21*

To see them, it was not even required to take away the temple from the nearest priest and disassemble it brick by brick. Thank God, there are useful idiots who run through various ruins and worry that the Lord will be very offended if this transformer is not urgently restored in the Orthodox style.

Here already those who did not know about the detector receiver remembered the Leyden jar.

<!--
![](https://sun9-17.userapi.com/impf/c851420/v851420428/c9f8d/lO4cgslOmoI.jpg)
-->
![](./images/How chronolozhts played electricians_files/lO4cgslOmoI.jpg#resized)
*How chronolozhts played electricians, image # 22*

<!--
![](https://sun9-29.userapi.com/impf/c850728/v850728428/ccba4/qSrRT970Zgs.jpg)
-->
![](./images/How chronolozhts played electricians_files/qSrRT970Zgs.jpg#resized)
*How chronolozhtsy played electricians, image # 23*

And in order to certainly confirm the similarity of cathedrals with a condenser, they found such a version of the can.

<!--
![](https://sun9-62.userapi.com/impf/c851420/v851420428/c9f9b/fjPIzzg4_30.jpg)
-->
![](./images/How chronolozhts played electricians_files/fjPIzzg4_30.jpg#resized)
*Condenser made from Leyden jars of Mr. Muschenbruck made in 1745.*

Condenser made from Leyden jars of Mr. Muschenbruck made in 1745.

Now it became clear that the matter is in energy, and church buildings all over the world repeating forms of each other, this is not a road to a mythical god, but to a completely earthly socket.

The similarity with the Leyden bankou was already clearly visible not only in the past, but also in the present.

<!--
![](https://sun9-48.userapi.com/impf/c851136/v851136221/c607a/RvramGKPxs4.jpg)
-->
![](./images/How chronolozhts played electricians_files/RvramGKPxs4.jpg#resized)
*Chandeliers in long tongues?  Could the suspension device be even more complicated?*

<!--
![](https://sun9-44.userapi.com/impf/c846221/v846221476/1b5c1f/kN7UkT-sx8Q.jpg)
-->
![](./images/How chronolozhts played electricians_files/kN7UkT-sx8Q.jpg#resized)
*How chronolozhtsy played electricians, image # 26*

<!--
![](https://sun9-62.userapi.com/impf/c846221/v846221476/1b5c28/pV5ohrdcrUs.jpg)
-->
![](./images/How chronolozhts played electricians_files/pV5ohrdcrUs.jpg#resized)
*How chronolozhts played electricians, image # 27*

<!--
![](https://sun9-54.userapi.com/impf/c846221/v846221476/1b5c30/XuSUiVnESJg.jpg)
-->
![](./images/How chronolozhts played electricians_files/XuSUiVnESJg.jpg#resized)
*How chronolozhts played electricians, image # 28*

<!--
![](https://sun9-39.userapi.com/impf/c846221/v846221476/1b5c39/Xpakb-pPIuM.jpg)
-->
![](./images/How chronolozhts played electricians_files/Xpakb-pPIuM.jpg#resized)
*How chronolozhts played electricians, image # 29*

1 of 5

Chandeliers in long tongues? Could the suspension device be even more complicated?

Immediately, the first detachments of troops of historical infantrymen came running, feeling that they had missed some important precedent. Their arguments were standard and not distinguished by their ingenuity. They talked about age-old traditions that have consolidated the appearance of cathedrals and churches. The fact that the temples of different confessions and religions are sustained almost in the same style was explained by the frequent transfer of buildings from hand to hand after the wars. They especially trumped the mosque of St. Sophia, which, due to historical events, in general, was forced to behave like a prostitute. Change faith depending on the wishes of the owner.

<!--
![](https://sun9-58.userapi.com/impf/c850724/v850724207/c81ad/DjrNF6OkjXs.jpg)
-->
![](./images/How chronolozhts played electricians_files/DjrNF6OkjXs.jpg#resized)
*We have a pool-temple-pool-temple.  There mullah-priest-mullah-pop*

We have a pool-temple-pool-temple. There mullah-priest-mullah-pop

When asked why your age-old traditions look like capacitors, they, of course, could not answer, because they did not know what a capacitor is. But, perhaps, they ran to the Ministry of the Electronic Industry with the plans of the churches and requests on the boards never to repeat this and spread similar elements as far as possible. So that the maximum it could look like is an oil refinery complex. Energy from oil is legal, so there will be no questions to ask.

<!--
![](https://sun9-2.userapi.com/impf/c851420/v851420428/c9fd0/44Vu0WkzR9Q.jpg)
-->
![](./images/How chronolozhts played electricians_files/44Vu0WkzR9Q.jpg#resized)
*By the way, this is an amplifier.*

By the way, this is an amplifier.

The priests, too, strained themselves and urgently came up with explanations for the different number of dome capacitors on their heads.

<!--
![](https://sun9-58.userapi.com/impf/c850724/v850724207/c8234/rsx8IupITbs.jpg)
-->
![](./images/How chronolozhts played electricians_files/rsx8IupITbs.jpg#resized)
Text says:

> THE DIFFERENT NUMBER OF DOMES, OR HEADS, OF A TEMPLE BUILDING IS DETERMINED BY the person to whom they are dedicated.
> SINGLE-DOMED TEMPLE: THE DOME SIGNIFIES the unity of God, the perfection CREATIONS.
> TWO-HEADED TEMPLE: TWO DOMES TWO NATURES OF the God-man Jesus CHRIST, THE TWO REGIONS OF CREATION (ANGELIC and human).
> THREE-DOMED CHURCH: THREE DOMES OF THE MOST HOLY TRINITY.
> four-domed temple: the four domes symbolize the four angels, the four cardinal directions.
> five-domed temple: five domes, one of which rises above the others, JESUS CHRIST AND THE FOUR Evangelists.
> SEVEN-HEADED TEMPLE: The SEVEN DOMES Symbolize the seven sacraments of the church, the seven ECUMENICAL COUNCILS, THE SEVEN VIRTUES.
> NINE-DOMED TEMPLE: NINE DOMES nine ranks of angels.
> THE TEMPLE: THE THIRTEEN DOMES OF JESUS CHRIST AND the twelve apostles.
> THE SHAPE AND COLOR OF THE DOME HAVE a symbolic meaning.
> THE HELMET-SHAPED FORM SYMBOLIZES THE BATTLE/ STRUGGLE/, THE CHURCH leads with the forces of evil.

*I sympathize with that responsible employee who was instructed to come up with a religious justification for the construction of temples*

Chronolozhtsy, meanwhile, put forward one version after another. And about the heads and about all the architectural techniques that are used in the construction of religious buildings. Each considered it his duty to give out the theory.

[Video 1: Illustrates how towers and domes were used as magnetrons](./images/How chronolozhts played electricians_files/video.mp4)

[Video 2: Compares rose windows with magnetrons](./images/How chronolozhts played electricians_files/video2.mp4)

[Video 3: Illustrates horizontal focusing of unspecified waves in church architecture](./images/How chronolozhts played electricians_files/video3.mp4)

Chronolozhtsy rejoiced. In the old pictures there was evidence, in the remains of churches there were metal foundations, it was only necessary to run up to St. Isaac's Cathedral and turn on the unit. But who will give? Priests armed with censers will drive away any chronolozhts from their territory, and with the help of detachments of crazy women who graze around any church, you can win small wars.

Bullshit, chronolozhtsy thought, let's do it ourselves. We know the principle of sockets in stores. Nonsense! You just need to find the scheme. Nobody has Tesla's phone? How did he die? Damn, this is it at the wrong time.

We ran to look for a scheme for collecting an electron-carrying object.

The first step was to find a patent

<!--
![](https://sun9-38.userapi.com/impf/wCbGKb6o4CFhwXp8FZbtLKD0WD_S8U2E2FKLhQ/DVbXXsPp62s.jpg)
-->
![](./images/How chronolozhts played electricians_files/DVbXXsPp62s.jpg#resized)
*the patent looked like a church and promised electricity*

<!--
![](https://sun9-26.userapi.com/impf/c849420/v849420632/13aa9e/s43xriQkJ_M.jpg)
-->
![](./images/How chronolozhts played electricians_files/s43xriQkJ_M.jpg#resized)
*How chronolozhtsy played electricians, image # 34*

Per [Yandex image translate](https://translate.yandex.com/), the text says:

> Dome-shaped triboelements, located vertically and connected to a cruciform antenna, allow for a minimum volume to create a maximum surface for triboelectrization by various atmospheric factors, similar to the electrification of aircraft hulls. As a result, there is a potential difference between the upper electrically charged needle electrode and the lower electrode.

> During snowstorms, rain, and thunderstorms, this process (the accumulation of electric charges) is significantly enhanced by the use of the developed surface of the domes.

> The increase in voltage between the electrodes also depends on the height of the upper electrode (with an antenna and a dome-shaped triboelement), since the E2 vertical component of the Earth's electric field is up to 200 In/m from the Earth's surface, increasing during the period of disturbances (rain, blizzard, thunderstorm). The needle allows you to maximize the concentration of the field strength for the breakdown of the discharge gap.

> The invention makes it possible to increase the accumulation of electric charges from the atmosphere and, consequently, to create a higher voltage between the electrodes, i.e., to increase efficiency.
 
> The dielectric housing is not limited to the functional role of the antenna lift and dome-shaped triboelements, but protects against external electrical interference and increases the safety of operation. Claim for invention

> A DEVICE FOR USING ATMOSPHERIC ELECTRICITY, including a receiving unit with an antenna element connected by a current line to a discharge element, characterized in that the receiving unit contains a system made below the antenna element oriented vertically and communicating with each other conducting dome-shaped triboelements, to the edge of the lower one of which the needle electrode of the discharge element is attached, and its other electrode is made in the form of a grounded metal disk. DRAWINGS

<!--
![](https://sun9-50.userapi.com/impf/c849420/v849420632/13aaa7/ataIY-3h-4A.jpg)
-->
![](./images/How chronolozhts played electricians_files/ataIY-3h-4A.jpg#resized)
*How chronolozhts played electricians, image # 35*

Per [Yandex image translate](https://translate.yandex.com/), the text says:

> Usage: in atmospheric static electricity devices. The device contains a receiving antenna, a system of domed triboelements and a discharge element in the form of a needle connected to the lower triboelement, and a grounded disk. When exposed to atmospheric phenomena ( storms, rain, snow) , an additional charge occurs, which increases the field strength in the discharge element, which increases the efficiency of the device. 1 ill.

> The invention relates to physics, in particular to electrical devices for the use of atmospheric electricity.

> Known device for use of atmospheric electricity that contains the variable capacitor capacity, to one of electrodes which are attached to air group and one of the electrodes of the spark gap, and to the other electrode is attached to the wire coming to the earth, and a second electrode of a spark gap, and between him and grounded plate of the capacitor included coil of self-inductance, which, when you break through the spark gap caused by increase in the voltage across the capacitor under the influence of atmospheric electricity, and an alternating current, inductive in a self-induction coil connected by a magnetic flux to the coil.

> The disadvantage of this device is low efficiency due to the fact that only the potential difference between the location of the antenna of the "panicle" receiving device and the Ground is used, which is insignificant in real designs of the lifts of the antenna device.

> The purpose of the invention is to increase the efficiency due to the additional introduction of a system of dome -shaped triboelements that are electrified when exposed to various meteorological phenomena (rain, snowstorms, dust storms, snowfall, etc.).

> The drawing shows the design of the device.

> The chamber of the capacitor 1 is limited by the housing 2, which is configured in the form of a body of rotation with a conical upper part. The housing is made of dielectric (concrete, limestone). At the top of the housing 2 there is a lower metal dome-shaped triboelement 3, which has a long metal "nose" 4, on which the dome-shaped triboelements connected to each other are rigidly fixed in series (by means of a metal "nose"), the cavities of which and the chambers are connected. A cruciform antenna 6 is mounted on the upper dome-shaped triboelement, from the edge of the lower dome-shaped triboelement, the needle 10 descends vertically. At the base of the chamber 7 is a lower disc-shaped metal electrode 8, which has a ground 9.

> The device works as follows.

*The patent looked like a church and promised electricity*

This picture has been in LiveJournal for everyone who has anything to do with the fake history, the mysteries of the past and the rest of obscurantism. I even visited the Ren-TV channel. However, this popularity did not result in a prototype.

In general, there are a lot of such patents.

<!--
![](https://sun9-47.userapi.com/impf/itmf1fV7Gf_IXyj6E45DwD7FQFrGC4K6kzAAvQ/Xj674-N_V4o.jpg)
-->
![](./images/How chronolozhts played electricians_files/Xj674-N_V4o.jpg#resized)
*Maybe they even work*

<!--
![](https://sun9-52.userapi.com/impf/c849420/v849420632/13aace/QfC48kEfID8.jpg)
-->
![](./images/How chronolozhts played electricians_files/QfC48kEfID8.jpg#resized)
*How chronolozhtsy played electricians, image # 37*

<!--
![](https://sun9-65.userapi.com/impf/c849420/v849420632/13aad5/PdtsWAD7mQo.jpg)
-->
![](./images/How chronolozhts played electricians_files/PdtsWAD7mQo.jpg#resized)
*How chronolozhtsy played electricians, image # 38*

But we have it. Ours, as usual, no one here believes everyone is looking at the beacon on the other side of the globe. And in America, any patent that describes an unknown method of producing energy, or just a mechanism with an efficiency of more than 10% higher than current devices, is immediately classified by a special law of 1953. But it will be necessary to talk about this separately later. that's interesting too.

I don’t know who, using these patents, tried to assemble a small candle factory on free atmospheric electricity, but no one showed it in working form.

Yes, and old churches where they managed to climb, for some reason they did not turn on, no matter how much you pull the tongue of the chandelier or hang on the transverse iron beams.

In time, there were also skeptics. Well, they said, what are you monkeys playing at? Open the physics textbooks; the section of electromagnetic energy... everything is written there! Electricity has been studied up and down, we have been using it for hundreds of years. Do you think no one except you would have guessed about this? Yes, in the United States (meaning the most honest, noble and advanced country in the world) they would have built and found a long time ago, if everything was as you say!

Chronolozhtsy sadly understood that the words of the skeptics really were a healthy grain and slowly got off the church chandeliers.

> And how will you transmit this electricity? Where are the power grids, transmission lines in your pictures, and at least the same sockets in houses? Or, how did the residents connect to the free resource? Did they carry electricity in buckets with their hands from the street?

Chronolozhtsy could not put up anything against dielectrics. Even trams without wires turned out to be simple rail locomotives with steam traction and a firebox for firewood. Where there was no firewood, there was a third rail, where there was no secret cord and it got out. And sometimes not secret, like in San Francisco.

Kiosks, so similar to repeaters of free electricity, suddenly became overgrown with paper products and became ordinary newspaper stalls.

The desire for a miracle and irrepressible imagination played a cruel joke with humanitarian chronologers. But they are not discouraged.

Many abandoned their fantastic ideas and moved on to other more mundane things. Some, however, returned to the otherworldly and are still looking for how to contact God so that he can connect a small temple for them personally. Now, only historians and builders are interested in architecture. Chronolozhtsy, like everyone else, look at the pretentious designs of buildings of the past, as proof of the superiority of their ancestors over today's humanity in taste, a sense of beauty and, most importantly, in the desire to make the world around them better.

Indeed, what hidden technologies can there be in our enlightened time, when the Internet has made available any information anywhere in the world?

In principle, it is useless to prohibit technology, because anyone can invent it at any time. You're not going to run around and eliminate every inventor in the world? So progress can wither.

And this is true, in order to hide something, it is not necessary to forbid inventing something like that, you just need to gloss over the places in history where it was said on what the technology was based and withdraw from circulation the substance that allowed the technology to exist. To instill a god in monkeys and tell that their ancestors for thousands of years knocked their foreheads in this sacred place.

Where there are flaws in history, and you yourself see how the broadcast was made a dirty word undesirable in a society with an education higher than the parish level, I told you.

And the substance that was banned is mercury. It was impossible to seize it all en masse, because it was also used in other places, without which even those who started the whole carnival could not get by. But it was surrounded by such vigorous propaganda that no one in their right mind would touch it now.

Strange right? How did the alchemists spent their lives trying to make gold and the philosopher's stone out of it but did not die along with all their families? Gas masks were invented much later than Isaac Newton.

<!--
![](https://sun9-12.userapi.com/impf/c851136/v851136859/c6b72/v_v-FyJprE4.jpg)
-->
![](./images/How chronolozhts played electricians_files/v_v-FyJprE4.png#resized)
*Sword of the Jedi?*

This is a fluorescent lamp stuck in the ground under a power line. It's lit up. No tricks and no wires. And not even with Nikola Tesla.

And this is its schematic

<!--
![](https://sun9-40.userapi.com/impf/c851136/v851136859/c6b82/3_M26WIua7g.jpg)
-->
![](./images/How chronolozhts played electricians_files/3_M26WIua7g.png#resized)
1. Mercury
2. Stamped glass leg with electric leads
3. Evacuation tube used during manufacture
4. Connection pins
5. End panel
6. Cathode with emitter coating

*Is it clear that the question is not about the lead pins?*

Well, how does mercury work and where, and most importantly, electricity was taken the next time.

© All rights reserved. The original author retains ownership and rights.

