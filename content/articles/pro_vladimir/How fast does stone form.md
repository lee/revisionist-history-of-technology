title: How Fast Does Stone Form?
date: 2015-10-13 08:17
modified: Fri 23 Jul 2021 20:38:48 BST
category:
tags: fossils
slug:
authors: Vladimir Mamzerev
summary: Found in a stream in 1980 near Iraan, Texas, the cowboy boot contains a "petrified" human foot, demonstrating that fossils don't need millions of years to form.
status: published
originally: С какой скоростью образуется камень? - pro_vladimir — LiveJournal.html

### Translated from:

[https://pro-vladimir.livejournal.com/212811.html](https://pro-vladimir.livejournal.com/212811.html)

**Russian:**

Карл Боуг и Дон Паттон утверждают, что ковбойский сапог изготовлен около 1950 и нашли в ручье в 1980 возле Iraan, штат Техас, содержит "окаменевшую" человеческую ногу внутри, демонстрируя, что окаменелости не нужно миллионы лет, чтобы сформироваться.

**English:**

Carl Boog and Don Patton claim the cowboy boot was made circa 1950. Found in a stream in 1980 near Iraan, Texas, it contains "petrified" human leg inside, demonstrating that fossils don't need millions of years to form.

<!-- Local
[![](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRmk5PX7Oa-xt6LYvQG.jpeg#clickable)](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRmk5PX7Oa-xt6LYvQG.jpeg)
-->

[![](http://paleo.cc/paluxy/limestn_cwby453.jpg#clickable)](http://paleo.cc/paluxy/limestn_cwby453.jpg)

<!-- Local
[![](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRkGvBvT-l48zIXUtpS.jpeg#clickable)](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRkGvBvT-l48zIXUtpS.jpeg)
-->

[![](http://paleo.cc/paluxy/boottop.jpg#clickable)](http://paleo.cc/paluxy/boottop.jpg)

<!-- Local
[![](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRlbkBwAFZakVnC0FGa.jpeg#clickable)](%D0%A1%20%D0%BA%D0%B0%D0%BA%D0%BE%D0%B9%20%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C%D1%8E%20%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D1%83%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%B5%D0%BD%D1%8C%3F%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/Emicre24WKctLvjozvnQsMZnCIGEtOAEOICUdajIGRlbkBwAFZakVnC0FGa.jpeg)
-->

[![](http://paleo.cc/paluxy/bootpro1.jpg#clickable)](http://paleo.cc/paluxy/bootpro1.jpg)

[http://paleo.cc/paluxy/boot.htm](http://paleo.cc/paluxy/boot.htm)


( 48 comments)

[**butch_ara**](https://butch-ara.livejournal.com/) [2015-10-13 09:30 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2261323#t2261323)

The "fossil" is carefully quoted because

a) it may not be a "fossil",

b) it is a stream.

[**kirroil**](https://kirroil.livejournal.com/) [2015-10-13 09:56 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2261835#t2261835)


**Russian:**

это может быть не окаменелость, а что? ручей чего? как-то я не много видел окаменевших деревьев в ручьях и/или реках. какие условия были соблюдены и куда делось всё остальное?

**English:**

It might not be a fossil, why? A stream of what? I haven't seen many fossilized trees in streams and/or rivers once. What conditions were met and where did the rest go?

[**butch_ara**](https://butch-ara.livejournal.com/) [2015-10-13 10:12 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2262347#t2262347)

**Russian:**

Это может быть известь. Плоть разложилась и была смыта водой. Остальное покрылось известью.

**English:**

It could be lime. The flesh decomposed and washed away with water. The rest was covered in lime.

[**drevozizni**](https://drevozizni.livejournal.com/)

**Russian:**

Аномалия какая то необьяснимая

**English:**

The anomaly is kind of unexplainable.

[**dmitrij_an**](https://dmitrij-an.livejournal.com/)

**Russian:**

«Озеро превращает животных в статуи»

**English:**

[The lake turns animals into statues.](http://www.gismeteo.ru/news/proisshestviya/ozero-prevraschaet-zhivotnyh-v-statui/)


[**drevozizni**](https://drevozizni.livejournal.com/) [2015-10-13 03:18 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2275915#t2275915)

**Russian:**

тут же речь идет о обычном ручье я так понимаю,поэтому объяснения нет. В ручьях много что лежит, только не окоменевает. Эту ногу в сапоге уже закаменелую закинули, а не ручей ее такой сделал.

**English:**

it's just a regular stream, I take it, so there's no explanation. There's a lot of stuff in the streams that doesn't recommend it. This foot in the boot was already hardened, not the stream.

[**pisar4uk**](https://pisar4uk.livejournal.com/) [2015-10-13 10:41 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2263115#t2263115)

**Russian:**

Хорошие сапоги однако делали в 1950-м.

**English:**

Good boots, however, were made in 1950.

[**kirroil**](https://kirroil.livejournal.com/) [2015-10-13 10:53 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2264139#t2264139)

**Russian:**

тогда их делали, вроде-бы, на заказ, а не на фабриках, и тогда ещё не было принципа запланированного устаревания, так как каждый мастер дорожил своим именем которое давало ему прибыль, сейчас прибыля делают на лохах, и взамен утраченного одного буратины придут два новых(приходили).

**English:**

At that time they were made, as if to order, not in factories, and then there was no principle of planned obsolescence, because each master valued his name, which gave him profit, now profits are made on bastards, and instead of the lost one Pinocchio will come two new (came).

**Russian:**

попробуйте все те деньги за сапоги что вы заносите бренду, занести в ателье по пошиву обуви. для разнообразия. я думаю все останутся довольны.

**English:**

Try all that money for the boots you bring to the brand, bring it to the shoe sewing atelier for a change. I think everyone's gonna be happy.

[**denegnerad**](https://denegnerad.livejournal.com/) [2015-10-13 12:06 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2265675#t2265675)

**Russian:**

Ну вот. Теперь придется пересматривать время вымирания динозавров. Опять скажут, что динозавры жили еще в нашей эре!

**English:**

Well, there you go. Now we have to reconsider the dinosaur extinction time. They're gonna say again that dinosaurs lived back in our era!

[**dmitrij_an**](https://dmitrij-an.livejournal.com/) [2015-10-13 12:25 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2267723#t2267723)

**Russian:**

В некоторых Церквах на фресках есть изображения стегозавров на которых катаются люди. Это было не так давно.

**English:**

Some Churches have images of Stegosaurs on the frescoes where people ride. That wasn't so long ago.

**Russian:**

Про вымирание - это лишь теория, такая же как теория Дарвина или теория электрона, что бежит в проводах.

**English:**

About extinction is just a theory, like Darwin's theory or the theory of the electron that runs in the wires.

[**russ_oleg**](https://russ-oleg.livejournal.com/) [2015-10-13 12:35 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2268235#t2268235)

**Russian:**

А что не так с "теорией Дарвина"?

**English:**

What's wrong with "Darwin's theory"?

**Russian:**

Да и с "теорией электрона, что бежит в проводах"? Не бежит, потому что ножек нет?

**English:**

And the "theory of the electron that runs in the wires"? It doesn't run because it has no legs?

[**russ_oleg**](https://russ-oleg.livejournal.com/) [Darwin's Theory . Theory](https://pro-vladimir.livejournal.com/212811.html?thread=2957643#t2957643)

**Russian:**

### Foot

**English:**

### Foot

[Anonymous] [2015-10-13 12:36 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2268491#t2268491)

**Russian:**

это чей то протез

**English:**

is someone's prosthesis.

[**poseydon2012**](https://poseydon2012.livejournal.com/) [2015-10-15 08:46 pm (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=2326603#t2326603)

**Russian:**

Родственник работал на одном водохранилище и привёз оттуда кусок от окаменевшего ствола дерева. Точнее, он был как бы не до конца окаменевший, и по виду похож на старое дерево. Но тяжелый как камень. Так что я вполне верю, что описанная история правдива.

**English:**

A relative worked at a reservoir and brought a piece of petrified wood from there. More precisely, it was kind of petrified and looked like an old tree. But it was as heavy as a stone. So I quite believe the story described is true.

[**semer777kin**](https://semer777kin.livejournal.com/) [2016-07-15 04:42 am (UTC)](https://pro-vladimir.livejournal.com/212811.html?thread=5157707#t5157707)

**Russian:**

Попробую предположить - ничего удивительного. Шаубергер, живая вода. Жидкость в потоке несет в себе магнитный заряд. магнитный заряд - не только N-S противостояние полюсов, это, прежде всего информация. Этот метод - основа алхимии, трансмутации хим элементов, в том числе по Болотову, хотя у него несколько иначе. Магнитный поток несет информацию о минералах, встречающихся по дну ручья. При завихрении в сапоге, он эту информацию отдает. Отсюда новое золото на полигонах в ручьях, где все вымыли в 40-е годы, оно опять образовалось.Отсюда молотки, свечи зажигания в якобы доисторических минералах. Встречаются даже лягушки, спящие в анабиозе. Также можно отнести статуи, якобы высеченные из мрамора, прежде реальные живые люди. Кто знает, что там еще спряталось?

**English:**

Let me guess, it's no surprise. Schauberger, live water. The liquid in the flow carries a magnetic charge. The magnetic charge is not only N-S confrontation of the poles, it is primarily information. This method is the basis of alchemy, transmutation of chemical elements, including Bolotov, although it is somewhat different. Magnetic flux carries information about minerals occurring at the bottom of the stream. When swirling in a boot, it gives this information. From here, new gold in the streams' landfills, where everything was washed out in the 40s, it was formed again. From here, hammers, spark plugs in supposedly prehistoric minerals. There are even frogs that sleep in anabiosis. There are also statues, allegedly carved out of marble, that were real living people before. Who knows what else is hiding there?
