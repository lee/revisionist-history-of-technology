title: Melted Megaliths and Settlements
date: 2016-10-06
modified: Sun 17 Jan 2021 08:14:00 GMT
category:
tags: catastrophe; thermonuclear war
slug:
authors: digitall-angell
from: https://digitall-angell.livejournal.com/719973.html
originally: Melted Megaliths and Settlements.html
local: Melted Megaliths and Settlements_files/
summary: The earth is full of megalithic structures, many of which can hardly be called natural formations, although the official version says they are. Most are mutilated beyond recognition; they clearly look man-made - though sometimes not so much. Did they witness wars, global cataclysms, merging/separating branches, or are they simply quirks of nature?
status: published

#### Translated from:

[https://digitall-angell.livejournal.com/719973.html](https://digitall-angell.livejournal.com/719973.html)

<!-- Local
![](Melted Megaliths and Settlements_files/2436058_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2436058/2436058_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2444526_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444526/2444526_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2430797_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2430797/2430797_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2444753_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444753/2444753_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/P1220377_1.jpg#resized)
-->

![](http://gorod.tomsk.ru/uploads/46203/1295588620/P1220377_1.jpg#resized)

**Russian:**

Земля полна мегалитов, многие из которых сложно назвать природными образованиями, хотя официальная версия гласит именно так.

**English:**

The earth is full of megalithic structures, many of which can hardly be called natural formations, although the official version says they are.

**Russian:**

Вашему внимаю предлагается подборка таких артефактов со всех сторон (видимого) света, некоторые из которых явно показывают следы термального воздействия, хотя во многих случаях заявлено выветривание. Большинство изуродованы до неузнаваемости, как явно рукотворные, так и не очень. Являются ли они свидетелями войн, глобальных катаклизмов, слияния/ разделения веток или просто причудами природы? Во всех случаях по-разному, да и по времени тоже. Для общей картины и в дополнение к [вчерашнему посту](http://digitall-angell.livejournal.com/719534.html).

**English:**

A selection of such artifacts from all sides of (visible) light is offered for your attention, some of which clearly show traces of thermal influence though in many cases the cause is declared as 'weathering'. Most are mutilated beyond recognition; they clearly look man-made - though sometimes not so much. Did they witness wars, global cataclysms, merging/separating branches, or are they simply quirks of nature?

For the big picture and in addition to [yesterday's post](http://digitall-angell.livejournal.com/719534.html).

**Russian:**

Для начала посмотрим "природные образования", некоторые мы уже видели ранее (см. детали по активным ссылкам):

**English:**

Let's start by looking at "natural formations", some we've seen before (see details on active links):

**Russian:**

[Отсюда](http://udivitelno.com/mesta/item/195-otdykh-v-primorje)

**English:**

[From here](http://udivitelno.com/mesta/item/195-otdykh-v-primorje)

<!-- Local
![](Melted Megaliths and Settlements_files/1495529_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1495529/1495529_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1495617_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1495617/1495617_original.jpg#resized)

**Russian:**

Кто грязными пальцами по <del>пломбиру</del> мягкому граниту ёрзал?

**English:**

Who's been poking their dirty fingers into <del>cold</del> soft granite?

<!-- Local
![](Melted Megaliths and Settlements_files/1495999_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1495999/1495999_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1496974_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1496974/1496974_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1496289_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1496289/1496289_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1496567_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1496567/1496567_original.jpg#resized)

**Russian:**

...или ногами...

**English:**

...or their feet...

<!-- Local
![](Melted Megaliths and Settlements_files/1496632_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1496632/1496632_original.jpg#resized)

**Russian:**

сравним с подобным образованием [[в Южной Африке, даже размеры "ножки" одинаковые:]]

**English:**

Compare it to a similar formation in South Africa, even the size of the "foot" is the same:

**Russian:**

sled

**English:**

sled

<!-- Local
![](Melted Megaliths and Settlements_files/1498399_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1498399/1498399_original.jpg#resized)

**Russian:**

[Магадан](http://oko-planet.su/phenomen/phenomendiscussions/194044-machu-pikchu-i-magadan-goroda-pobratimy.html):

**English:**

[Magadan](http://oko-planet.su/phenomen/phenomendiscussions/194044-machu-pikchu-i-magadan-goroda-pobratimy.html):

<!-- Local
![](Melted Megaliths and Settlements_files/1525281_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1525281/1525281_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1525674_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1525674/1525674_original.jpg#resized)

**Russian:**

Кузбасс: chudesa-kuzbassa1

**English:**

Kuzbass: chudesa-kuzbassa1

<!-- Local
![](Melted Megaliths and Settlements_files/1519118_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1519118/1519118_original.jpg#resized)

**Russian:**

Австралия (Murphys Haystack):

**English:**

Murphys Haystack, Australia:

<!-- Local
![](Melted Megaliths and Settlements_files/2450199_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2450199/2450199_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2450561_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2450561/2450561_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2450769_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2450769/2450769_original.jpg#resized)

**Russian:**

Тут вообще не камень, а застывший пластилин в чистом виде:

**English:**

It's not rock at all, it's pure, frozen plasticine:

**Russian:**

Оригинал взят у [sibved](https://sibved.livejournal.com/) в [Мегалиты Бектау-Ата](http://sibved.livejournal.com/208188.html)

**English:**

Original taken from [sibved](https://sibved.livejournal.com/) in [Bektau-Ata Megaliths](http://sibved.livejournal.com/208188.html)

<!-- Local
![](Melted Megaliths and Settlements_files/userinfo_v8.svg#resized)
-->

![](Melted Megaliths and Settlements_files/userinfo_v8.svg#resized)

**Russian:**

Посреди Казахской степи, примерно в 60-ти километрах от города Балхаш, расположена гористая местность Бектау-Ата:

**English:**

In the middle of the Kazakh steppe, about 60 kilometers from the town of Balkhash, is the mountainous area of Bektau-Ata:

<!-- Local
![](Melted Megaliths and Settlements_files/2184330_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2184330/2184330_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/000001260.jpg#resized)
-->

![](http://www.isostan.com/img/o900/000001260.jpg#resized)

**Russian:**

Эти каменные чаши, говорят геологи, образовались при кипении этого расплавленного гранита (еще раз посмеемся, т.к. после такого гранит бы перестал быть гранитом, стал бы, возможно, уже базальтом или кварцитом).

**English:**

These stone bowls, geologists say, were formed by this molten granite boiling. (Let's laugh again, because granite would cease to be granite, becoming, perhaps, already basalt or quartzite, following this heat treatment).

**Russian:**

![Image no longer available](http://photos.id.kcdn.kz/ce/big_808c4f5167ecbe38818.jpg#resized)

**English:**

![Image no longer available](http://photos.id.kcdn.kz/ce/big_808c4f5167ecbe38818.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/000000418.jpg#resized)
-->

![](http://www.isostan.com/img/o900/000000418.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/114809707.jpg#resized)
-->

![](http://static.panoramio.com/photos/large/114809707.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/27278634.jpg#resized)
-->

![](http://static.panoramio.com/photos/large/27278634.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/c7ddbau-960.jpg#resized)
-->

![](https://c-a.d-cd.net/c7ddbau-960.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/P1220377_1.jpg#resized)
-->

![](http://gorod.tomsk.ru/uploads/46203/1295588620/P1220377_1.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/56094.jpg#resized)
-->

![](http://35photo.ru/photos_series/56/56094.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/bektauata-045.jpg#resized)
-->

![](http://www.kp74.ru/wp-content/gallery/bektauata/bektauata-045.jpg#resized)

**Russian:**

На чью голову похоже?

**English:**

Whose head does this resemble?

<!-- Local
![](Melted Megaliths and Settlements_files/WLYnKaHYShk.jpg#resized)
-->

![](https://pp.vk.me/c617419/v617419020/222ac/WLYnKaHYShk.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/cdf408s-960.jpg#resized)
-->

![Image no longer available](https://g-a.d-cd.net/cdf408s-960.jpg#resized)

**Russian:**

Опять пальцы, прям кружок очумелые ручки

**English:**

Fingers again, just a circle by nimble hands.

**Russian:**

Каменная Тортила

**English:**

Stone Tortilla

![Image no longer available]()

<!-- Local
![](Melted Megaliths and Settlements_files/43453.jpg#resized)
-->

![](http://www.risk.ru/u/img/44/43453.jpg#resized)

**Russian:**

Чебуратор. Пара кирпичей отлично сохранились

**English:**

Cheburator. A couple of bricks are perfectly preserved

**Russian:**

Аналогичные образования есть в Актогайском районе, севернее:

**English:**

There are similar formations in Aktogay district, to the north:

<!-- Local
![](Melted Megaliths and Settlements_files/78129258.jpg#resized)
-->

![](http://static.panoramio.com/photos/large/78129258.jpg#resized)

**Russian:**

[Ссылка](http://maps.google.com/?ll=48.42421,75.40807&z=13&t=h) на карту

**English:**

[Link](http://maps.google.com/?ll=48.42421,75.40807&z=13&t=h) on Google map

**Russian:**

И рядом с Каркаралинском:

**English:**

And near Karkaralinsk:

<!-- Local
![](Melted Megaliths and Settlements_files/114809797.jpg#resized)
-->

![](http://static.panoramio.com/photos/large/114809797.jpg#resized)

**Russian:**

[Оригинал взят у](https://kadykchanskiy.livejournal.com/profile)

**English:**

[Originally taken from](https://kadykchanskiy.livejournal.com/profile)

![](http://static.panoramio.com/photos/large/37990857.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/tigvC2eGL28PfRWjxIBwQ1QojKmvWbKWTO0RTL7HscUO5aCw_N9MGdaJhkMh.svg#resized)
-->

**Russian:**

[kadykchanskiy](https://kadykchanskiy.livejournal.com/)] в [Казахстанская Атлантида. Озеро Боровое.](http://kadykchanskiy.livejournal.com/126718.html)

**English:**

[kadykchanskiy](https://kadykchanskiy.livejournal.com/)] in [Kazakhstan Atlantis. Borovoe Lake](http://kadykchanskiy.livejournal.com/126718.html)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c91a_de3f2a6d_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6430/1118136.36/0_8c91a_de3f2a6d_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c913_547e2a23_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5628/1118136.36/0_8c913_547e2a23_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/37990857.jpg#resized)
-->

![](http://static.panoramio.com/photos/large/37990857.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c916_226a7b65_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5634/1118136.36/0_8c916_226a7b65_XL.jpg#resized)

**Russian:**

Древняя стена с башнями?

**English:**

An ancient wall with towers?

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c918_1130d68d_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6441/1118136.36/0_8c918_1130d68d_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c919_5d44c7c4_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5645/1118136.36/0_8c919_5d44c7c4_XL.jpg#resized)

**Russian:**

Тот, кто бывал в Брестской крепости, не может не заметить аналогий. Только сделать поправку на размеры, и вы увидите остекленевшие потёки расплавленного немецкими огнемётами кирпича.

**English:**

Anyone who has been to the Brest Fortress can't help but notice the analogies. Just make a correction for size, and you'll see the same glazed drips of molten bricks made by German flamethrowers.

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c91d_92507204_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5635/1118136.36/0_8c91d_92507204_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c929_c9b4e35e_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6441/1118136.37/0_8c929_c9b4e35e_L.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c936_fd0234b3_orig)
-->

![](http://img-fotki.yandex.ru/get/6425/1118136.37/0_8c936_fd0234b3_orig)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c92f_dcbed158_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5627/1118136.37/0_8c92f_dcbed158_XL.jpg#resized)

**Russian:**

Разве не очевидно, что это была пирамида? Просто одна её половина рухнула. Рядом ещё пирамиды, поменьше. Всё точно как в Гизе.

**English:**

Wasn't it obvious it was a pyramid? It's just that half of it collapsed. There are other smaller pyramids nearby. It's exactly like Giza.

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c930_33d48ec5_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6429/1118136.37/0_8c930_33d48ec5_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c932_43e62e32_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5631/1118136.37/0_8c932_43e62e32_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c932_43e62e32_orig)
-->

![](http://img-fotki.yandex.ru/get/5631/1118136.37/0_8c932_43e62e32_orig)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c937_30ff8afa_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5629/1118136.37/0_8c937_30ff8afa_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8c937_30ff8afa_orig)
-->

![](http://img-fotki.yandex.ru/get/5629/1118136.37/0_8c937_30ff8afa_orig)

<!-- Local
![](Melted Megaliths and Settlements_files/2438853_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2438853/2438853_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2439072_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2439072/2439072_original.jpg#resized)

**Russian:**

Оригинал взят у [by-enigma](https://by-enigma.livejournal.com/profile) в [Мегалиты Северной Америки](http://ru-hidden.livejournal.com/53790.html)

**English:**

Original taken from [by_enigma](https://by-enigma.livejournal.com/) in [North American Megaliths](http://ru-hidden.livejournal.com/53790.html)

<!-- Local
![](Melted Megaliths and Settlements_files/userinfo_v8.svg#resized)
-->

![](Melted Megaliths and Settlements_files/userinfo_v8.svg#resized)

**Russian:**

Cypress Hills. Провинция Альберта.Канада

**English:**

Cypress Hills. Alberta.Canada.

<!-- Local
![](Melted Megaliths and Settlements_files/0_119313_c69bbe77_orig.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/38765/380168343.a/0_119313_c69bbe77_orig.jpg#resized)

**Russian:**

Национальный парк Джошуа-Три. Калифорния.США

**English:**

Joshua Tree National Park. California.USA.

<!-- Local
![](Melted Megaliths and Settlements_files/0_119330_96170a09_orig.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/57797/380168343.b/0_119330_96170a09_orig.jpg#resized)


**Russian:**

[violet3333](http://violet3333.livejournal.com/) в [Майорка. Cala Portals Vells и другие каменоломни.](http://violet3333.livejournal.com/15713.html)

**English:**

[violet3333](http://violet3333.livejournal.com/) in [Mallorca. Cala Portals Vells and other quarries.](http://violet3333.livejournal.com/15713.html)

<!-- Local
![](Melted Megaliths and Settlements_files/2439191_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2439191/2439191_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2439563_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2439563/2439563_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2439756_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2439756/2439756_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2439972_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2439972/2439972_original.jpg#resized)

**Russian:**

Крым:

**English:**

Crimea:

**Russian:**

Бахчисарайские сфинксы 1

**English:**

Bakhchisarai Sphinxes 1

<!-- Local
![](Melted Megaliths and Settlements_files/1625599_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1625599/1625599_original.jpg#resized)

Bakhchisarai Sphinxes 2
<!-- Local
![](Melted Megaliths and Settlements_files/1625726_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1625726/1625726_original.jpg#resized)

Bakhchisarai Sphinxes 3
<!-- Local
![](Melted Megaliths and Settlements_files/1626035_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1626035/1626035_original.jpg#resized)

Bakhchisarai Sphinxes 4

<!-- Local
![](Melted Megaliths and Settlements_files/1626233_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1626233/1626233_original.jpg#resized)

**Russian:**

Это явно не само из земли так вылупилось:

**English:**

It obviously didn't hatch itself out of the ground like that:

<!-- Local
![](Melted Megaliths and Settlements_files/55310_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/55310/55310_800.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2430113_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2430113/2430113_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2430226_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2430226/2430226_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2430593_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2430593/2430593_original.jpg#resized)

**Russian:**

Можете представить температуру плавления?

**English:**

Can you imagine the melting point?


<!-- Local
![](Melted Megaliths and Settlements_files/2430797_original.jpg#resized)

![](Melted Megaliths and Settlements_files/2430797_original.jpg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/2431156_original.jpg#resized)

![](Melted Megaliths and Settlements_files/2431156_original.jpg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/2431156_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2431156/2431156_original.jpg#resized)

**Russian:**

Бахчисарайские сфинксы 9

**English:**

Bakhchisarai sphinxes 9

<!-- Local
![](Melted Megaliths and Settlements_files/1627299_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1627299/1627299_original.jpg#resized)

**Russian:**

Инкерманский сфинкс 5

**English:**

Inkerman Sphinx 5

<!-- Local
![](Melted Megaliths and Settlements_files/1627416_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1627416/1627416_original.jpg#resized)


**Russian:**

Италия:

domus_de_janas_doma fei

**English:**

Italy:

domus_de_janas_doma fei

<!-- Local
![](Melted Megaliths and Settlements_files/1517727_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1517727/1517727_original.jpg#resized)

**Russian:**

domus_de_janas_doma fei 2

**English:**

domus_de_janas_doma fei 2

<!-- Local
![](Melted Megaliths and Settlements_files/1517998_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1517998/1517998_original.jpg#resized)

**Russian:**

[Мегалиты Горной Шории и не только](http://digitall-angell.livejournal.com/302338.html)

**English:**

[Megaliths of Gornaya Shoria and not only](http://digitall-angell.livejournal.com/302338.html)

**Russian:**

Парк Драконов (Приморье). Фото "Парк Драконов: Дракончик"

**English:**

Dragon Park (Primorye). Dragon Park photo: Dragonfly

<!-- Local
![](Melted Megaliths and Settlements_files/l3EvTTP3Y11ce6UnOv5K9wvIKyTH_OOZPkB3V20_wK3w5CgHSsUyp0u_002.jpeg#resized)
-->

![Image no longer available]()

[http://udivitelno.com/mesta/item/195-otdykh-v-primorje](http://udivitelno.com/mesta/item/195-otdykh-v-primorje)


**Russian:**

Что за идол с хвостом, не дракончик ли, случаем?

**English:**

What's the idol with the tail, a dragon, by any chance?

**Russian:**

Lublyana

**English:**

Lublyana

<!-- Local
![](Melted Megaliths and Settlements_files/1630154_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1630154/1630154_original.jpg#resized)

**Russian:**

Парк Драконов (Приморье): глаз Дракона. Фото

**English:**

Dragon Park (Primorye): Dragon Eye. Photo

<!-- Local
![](Melted Megaliths and Settlements_files/l3EvTTP3Y11ce6UnOv5K9wvIKyTH_OOZPkB3V20_wK3w5CgHSsUyp0u_003.jpeg#resized)
-->

![Image no longer available]()

**Russian:**

Глаз дракона?

**English:**

The eye of the dragon?

**Russian:**

Парк Драконов (Приморье): Лохнесское чудовище. Фото

**English:**

Dragon Park (Primorye): Loch Ness Monster. Photo

<!-- Local
![](Melted Megaliths and Settlements_files/l3EvTTP3Y11ce6UnOv5K9wvIKyTH_OOZPkB3V20_wK3w5CgHSsUyp0u6zV_.jpeg#resized)
-->

![Image no longer available]()

**Russian:**

(Лохнесское чудовище")

**English:**

(Loch Ness Monster")

**Russian:**

Русалка?

**English:**

A mermaid?

**Russian:**

[Мегалиты Карелии. Гора Воттоваара.](http://kadykchanskiy.livejournal.com/63744.html)

**English:**

[Megaliths of Karelia. Vottovaara Mountain](http://kadykchanskiy.livejournal.com/63744.html)

<!-- Local
![](Melted Megaliths and Settlements_files/0_7bd48_3a8ad8aa_L)
-->

![](https://img-fotki.yandex.ru/get/6445/1118136.15/0_7bd48_3a8ad8aa_L)

<!-- Local
![](Melted Megaliths and Settlements_files/0_7bd4a_93d7bc5e_L)
-->

![](https://img-fotki.yandex.ru/get/6429/1118136.15/0_7bd4a_93d7bc5e_L)

**Russian:**

Явно природный шарик, коих [по всему миру тысячи](http://digitall-angell.livejournal.com/614432.html)

**English:**

Clearly a natural ball, of which there are thousands [around the world](http://digitall-angell.livejournal.com/614432.html).

<!-- Local
![](Melted Megaliths and Settlements_files/0_7bd5f_9632f548_XL)
-->

![](https://img-fotki.yandex.ru/get/6427/1118136.16/0_7bd5f_9632f548_XL)

**Russian:**

Stairway to <del>heaven</del> hell

**English:**

Stairway to <del>Heaven</del> Hell

<!-- Local
![](Melted Megaliths and Settlements_files/0_7bd61_8a02de7d_XL)
-->

![](https://img-fotki.yandex.ru/get/6433/1118136.16/0_7bd61_8a02de7d_XL)

**Russian:**

[Мегалиты Урала. Попов остров](http://kadykchanskiy.livejournal.com/39163.html).

**English:**

[Megaliths of the Urals. Popov Island](http://kadykchanskiy.livejournal.com/39163.html)

<!-- Local
![](Melted Megaliths and Settlements_files/0_729b4_b76ad49c_XXL)
-->

![](https://img-fotki.yandex.ru/get/6616/1118136.8/0_729b4_b76ad49c_XXL)

**Russian:**

[Мегалиты Приморья. Гора Пидан.](http://digitall-angell.livejournal.com/160836.html):

**English:**

[Megaliths of Primorye. Mount Pidan](http://digitall-angell.livejournal.com/160836.html):

<!-- Local
![](Melted Megaliths and Settlements_files/0_8d6e8_d4e82977_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6805/1118136.38/0_8d6e8_d4e82977_XL.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/0_8d6f4_f6d81778_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6829/1118136.39/0_8d6f4_f6d81778_XL.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/0_8d702_c9e07717_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6804/1118136.39/0_8d702_c9e07717_XL.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/0_8d728_bf66578e_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6800/1118136.3a/0_8d728_bf66578e_XL.jpg#resized)


**Russian:**

Урал](http://kadykchanskiy.livejournal.com/240191.html):

**English:**

[Urals](http://kadykchanskiy.livejournal.com/240191.html):

<!-- Local
![](Melted Megaliths and Settlements_files/2446235_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2446235/2446235_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2446487_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2446487/2446487_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2446624_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2446624/2446624_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2447103_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2447103/2447103_original.jpg#resized)

**Russian:**

Кутурчинское белогорье:

**English:**

Kuturcha White Ridge:

<!-- Local
![](Melted Megaliths and Settlements_files/2184461_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2184461/2184461_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2184838_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2184838/2184838_original.jpg#resized)

**Russian:**

Остров Пасхи:

**English:**

Easter Island:

<!-- Local
![](Melted Megaliths and Settlements_files/2450153_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2450153/2450153_original.jpg#resized)

**Russian:**

Турция, Каппадокия:

**English:**

Turkey, Cappadocia:

<!-- Local
![](Melted Megaliths and Settlements_files/2421987_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2421987/2421987_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2422223_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2422223/2422223_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2422302_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2422302/2422302_original.jpg#resized)

**Russian:**

Каменный поп-корн:

**English:**

Stone popcorn:

<!-- Local
![](Melted Megaliths and Settlements_files/2423606_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2423606/2423606_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2425030_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2425030/2425030_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2443103_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2443103/2443103_original.jpg#resized)

**Russian:**

УПД из комментариев: Грузия, древний пещерный город Уплисцихе. Как утверждают историки, II-I век до н.э.:

**English:**

Lifted from the comments section: Georgia, the ancient cave city of Uplistsikhe. According to historians, it is from the second or first century BC:

<!-- Local
![](Melted Megaliths and Settlements_files/2454950_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2454950/2454950_original.jpg#resized)

**Russian:**

тоже в Грузии, пещерный храмовый комплекс Вардзия:

**English:**

Also in Georgia, the Vardzia cave temple complex:

<!-- Local
![](Melted Megaliths and Settlements_files/2455206_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2455206/2455206_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2455359_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2455359/2455359_original.jpg#resized)

**Russian:**

Иранский тяп-ляп:

**English:**

An Iranian stunt:

<!-- Local
![](Melted Megaliths and Settlements_files/2431482_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2431482/2431482_original.jpg#resized)

**Russian:**

Немного из другой серии, можно списать на лаву:

**English:**

A bit of a different series, you can write it off as lava:

<!-- Local
![](Melted Megaliths and Settlements_files/2415746_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2415746/2415746_original.jpg#resized)


<!-- Local
![](Melted Megaliths and Settlements_files/2415876_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2415876/2415876_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2418898_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2418898/2418898_original.jpg#resized)

**Russian:**

тут блины пекли

**English:**

Pancakes were baked

<!-- Local
![](Melted Megaliths and Settlements_files/2417558_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2417558/2417558_original.jpg#resized)

**Russian:**

и павидлой намазывали

**English:**

and pavidala was smeared on

<!-- Local
![](Melted Megaliths and Settlements_files/2417750_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2417750/2417750_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2418098_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2418098/2418098_original.jpg#resized)

**Russian:**

Если это лава, то как быстро она должна была течь тут (Австралия, Wave Rock)?

**English:**

If this is lava, how fast must it have been flowing here (Australia, Wave Rock)?

<!-- Local
![](Melted Megaliths and Settlements_files/2419707_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2419707/2419707_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2419900_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2419900/2419900_original.jpg#resized)

**Russian:**

Или тут (Аризона):

**English:**

Or here (Arizona):

<!-- Local
![](Melted Megaliths and Settlements_files/2419982_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2419982/2419982_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2420383_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2420383/2420383_original.jpg#resized)

**Russian:**

Или все-таки примерно вот так это было сделано? ([Глаз Сахары](https://ru.wikipedia.org/wiki/%D0%A0%D0%B8%D1%88%D0%B0%D1%82))

**English:**

Or is this how it was done after all? [Eye of the Sahara - wiki.ru](https://ru.wikipedia.org/wiki/%D0%A0%D0%B8%D1%88%D0%B0%D1%82)

<!-- Local
![](Melted Megaliths and Settlements_files/1163432_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1163432/1163432_original.jpg#resized)

**Russian:**

struktura-rishat-glaz-pustyni-saxara

**English:**

Reichat Structure, glazed salt pastes

<!-- Local
![](Melted Megaliths and Settlements_files/1163770_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1163770/1163770_original.jpg#resized)

**Russian:**

Далее видим несколько примеров принтерной сборки/ печати по скалам, как, например, в Иорданской Петре:

**English:**

Next we see several examples of printmaking/rock-printing, such as in Jordan's Petra:

<!-- Local
![](Melted Megaliths and Settlements_files/2415308_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2415308/2415308_original.jpg#resized)

**Russian:**

Тут либо принтер халтурить начал, либо карго культ заработал:

**English:**

Either the printer's getting sloppy, or the cargo cult's kicked in:

<!-- Local
![](Melted Megaliths and Settlements_files/1518519_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1518519/1518519_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2415498_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2415498/2415498_original.jpg#resized)

**Russian:**

Или это лава сделала ровный проход?

**English:**

Or was it the lava that made the smooth passage?

<!-- Local
![](Melted Megaliths and Settlements_files/1518740_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1518740/1518740_original.jpg#resized)

**Russian:**

...с аркой...

**English:**

...with an arch...

<!-- Local
![](Melted Megaliths and Settlements_files/2443305_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2443305/2443305_original.jpg#resized)

**Russian:**

И кирпичи заодно оплавила:

**English:**

And it melted the bricks as well.

<!-- Local
![](Melted Megaliths and Settlements_files/2443900_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2443900/2443900_original.jpg#resized)

**Russian:**

Или все-таки на принтере лепили и поставщик полимера подкачал?

**English:**

Or did you use a printer or did the polymer supplier get it wrong?

<!-- Local
![](Melted Megaliths and Settlements_files/2444276_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444276/2444276_original.jpg#resized)

**Russian:**

Опаленные солнцем, обдутые ветром...

**English:**

Scorched by the sun, blown by the wind...

<!-- Local
![](Melted Megaliths and Settlements_files/2444526_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444526/2444526_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2444753_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444753/2444753_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2444920_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2444920/2444920_original.jpg#resized)

**Russian:**

Пока взрослые лепили храмы, юнаты лепили гаражи для мафынок

**English:**

While the adults were modeling temples, the young kids were modeling car garages

<!-- Local
![](Melted Megaliths and Settlements_files/2445211_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2445211/2445211_original.jpg#resized)

**Russian:**

..и Барби...

**English:**

..and Barbie...

<!-- Local
![](Melted Megaliths and Settlements_files/2445511_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2445511/2445511_original.jpg#resized)

**Russian:**

[Тут еще Петра до кучи](http://hodor.lol/post/75779/)

**English:**

[There's more Peter to the pile](http://hodor.lol/post/75779/)

**Russian:**

[Пинара, Турция](http://kadykchanskiy.livejournal.com/313833.html). Малобюджетные супергерои из Петры и тут отметились:

**English:**

[Pinara, Turkey](http://kadykchanskiy.livejournal.com/313833.html). The low-budget superheroes from Petra have made their mark here as well:

<!-- Local
![](Melted Megaliths and Settlements_files/0_cdb62_9918beaa_orig.jpeg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/0_cdb63_28540ddd_orig.jpeg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/0_cdb65_961f1b4c_orig.jpeg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/0_cdb66_c5a9af29_orig.jpeg#resized)
-->

<!-- Local
![](Melted Megaliths and Settlements_files/0_cdb67_7544fd52_orig.jpeg#resized)
-->

![Images no longer available]()

**Russian:**

Саудовская Аравия. Одинокие куски храмов посреди пустыни будто вырваны ~~с мясом~~ из другой реальности:

**English:**

Saudi Arabia. The lonely pieces of temples in the middle of the desert are torn <del>from meat</del> from another reality:

<!-- Local
![](Melted Megaliths and Settlements_files/1194312_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1194312/1194312_original.jpg#resized)

madain-saleh-other-name

<!-- Local
![](Melted Megaliths and Settlements_files/1494858_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1494858/1494858_original.jpg#resized)

**Russian:**

Китай:

**English:**

China:

<!-- Local
![](Melted Megaliths and Settlements_files/2012197_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2012197/2012197_original.jpg#resized)

**Russian:**

Бункер <del>фюрера</del> императора

**English:**

The emperor's <del>führer</del> bunker

<!-- Local
![](Melted Megaliths and Settlements_files/1198004_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1198004/1198004_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1198304_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1198304/1198304_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1198532_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1198532/1198532_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1198826_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1198826/1198826_original.jpg#resized)

**Russian:**

Guyaju, Китай:

**English:**

Guyaju, China: Gaochang ancient ruins, Xinjiang

<!-- Local
![](Melted Megaliths and Settlements_files/2435691_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2435691/2435691_original.jpg#resized)

**Russian:**

Страшный сон Кощея

**English:**

Koshchei's bad dream

<!-- Local
![](Melted Megaliths and Settlements_files/2436058_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2436058/2436058_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1523102_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1523102/1523102_original.jpg#resized)

**Russian:**

[Оригинал взят у](https://kadykchanskiy.livejournal.com/profile) [kadykchanskiy](https://kadykchanskiy.livejournal.com/) в [Карпатский брат Мачу Пикчу](http://kadykchanskiy.livejournal.com/460805.html)

**English:**

[Original taken from kadykchanskiy](https://kadykchanskiy.livejournal.com/) in [Carpathian Brother Machu Picchu](http://kadykchanskiy.livejournal.com/460805.html)

<!-- Local
![](Melted Megaliths and Settlements_files/0_f3f22_498061e1_orig)
-->

![](https://img-fotki.yandex.ru/get/67698/1118136.b5/0_f3f22_498061e1_orig)

<!-- Local
![](Melted Megaliths and Settlements_files/0_f3f21_9ecc74a3_orig)
-->

![](https://img-fotki.yandex.ru/get/46165/1118136.b5/0_f3f21_9ecc74a3_orig)

<!-- Local
![](Melted Megaliths and Settlements_files/0_f3f1f_9b8af235_orig)
-->

![](https://img-fotki.yandex.ru/get/65449/1118136.b5/0_f3f1f_9b8af235_orig)

**Russian:**

Горный комплекс в Индии. Жестко оплавлена вся внешняя стена, местами вырваны куски:

**English:**

A mountain complex in India. The entire outer wall is melted, with pieces torn out in places:

<!-- Local
![](Melted Megaliths and Settlements_files/1245664_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1245664/1245664_original.jpg#resized)

**Russian:**

Аджанта2

**English:**

Ajanta 2

<!-- Local
![](Melted Megaliths and Settlements_files/1246371_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1246371/1246371_original.jpg#resized)

**Russian:**

адж4

**English:**

Ajanta 4

<!-- Local
![](Melted Megaliths and Settlements_files/1247589_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1247589/1247589_original.jpg#resized)

**Russian:**

4

**English:**

Ajanta 3

<!-- Local
![](Melted Megaliths and Settlements_files/1248225_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1248225/1248225_original.jpg#resized)

**Russian:**

Читать весь пост: [Храмовые пещеры Индии](http://digitall-angell.livejournal.com/318610.html)

**English:**

Read the whole post: [Temple Caves of India](http://digitall-angell.livejournal.com/318610.html)

**Russian:**

бомбоубежище в <del>окаменевшем ките</del> Индии:

**English:**

Bomb shelter in <del>the rocky whale</del> of India:

<!-- Local
![](Melted Megaliths and Settlements_files/l3EvTTP3Y11ce6UnOv5K9zMkyPAkmypRdHNqz5xKh1kWa90NW5oWs3v9kUF.jpeg#resized)
-->

![Image no longer available]()

**Russian:**

Читать весь пост: Б](http://content.foto.my.mail.ru/mail/kle.alex/_blogs/i-3566.jpg)омбоубежища древней Индии. Пещеры Барабар](http://digitall-angell.livejournal.com/373052.html)

**English:**

Read the whole post: [Bomb shelters of ancient India. Barabar Caves](http://digitall-angell.livejournal.com/373052.html)

![](http://content.foto.my.mail.ru/mail/kle.alex/_blogs/i-3566.jpg#resized)

**Russian:**

[Всего понемножку:]

**English:**

A little bit of everything:

<!-- Local
![](Melted Megaliths and Settlements_files/2416141_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2416141/2416141_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2416534_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2416534/2416534_original.jpg#resized)

**Russian:**

foto-ekskursiaya-kachi-kalon

**English:**

Kachi Kalon photograpy excursing

<!-- Local
![](Melted Megaliths and Settlements_files/1619032_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1619032/1619032_original.jpg#resized)

**Russian:**

Япония:

**English:**

Japan:

<!-- Local
![](Melted Megaliths and Settlements_files/1526460_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1526460/1526460_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1526683_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1526683/1526683_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1527538_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1527538/1527538_original.jpg#resized)

**Russian:**

Стена <del>Плача</del> Ниндзя

**English:**

Wall <del>Wailing</del> Ninja

**Russian:**

япония2Ф

**English:**

Japan 2F

<!-- Local
![](Melted Megaliths and Settlements_files/1527246_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1527246/1527246_original.jpg#resized)

**Russian:**

Norşuntepe (засыпали или наплавили сверху?):

**English:**

Norşuntepe (backfilled or floated on top?):

<!-- Local
![](Melted Megaliths and Settlements_files/1494194_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1494194/1494194_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1494469_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1494469/1494469_original.jpg#resized)

**Russian:**

[Саксайуаман](http://digitall-angell.livejournal.com/551305.html) и другие места южной Америки:

**English:**

[Sacsayhuaman](http://digitall-angell.livejournal.com/551305.html) and other places in South America:

<!-- Local
![](Melted Megaliths and Settlements_files/2008436_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2008436/2008436_original.jpg#resized)

**Russian:**

геймерам и не снилось

**English:**

Gamers wouldn't dream of it

<!-- Local
![](Melted Megaliths and Settlements_files/2009335_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2009335/2009335_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2414779_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2414779/2414779_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1492696_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1492696/1492696_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1493021_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1493021/1493021_original.jpg#resized)

**Russian:**

Кенко:

**English:**

Kenko:

<!-- Local
![](Melted Megaliths and Settlements_files/1528686_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1528686/1528686_original.jpg#resized)

**Russian:**

КЕНКО Б

**English:**

KENKO B

<!-- Local
![](Melted Megaliths and Settlements_files/1528887_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1528887/1528887_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1529283_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1529283/1529283_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1529411_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1529411/1529411_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2419137_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2419137/2419137_original.jpg#resized)

**Russian:**

Четко видны следы мощного удара и плавления. Явно не петардой стреляли:

**English:**

You can clearly see the marks of a powerful impact and melting. Definitely not a firecracker:

<!-- Local
![](Melted Megaliths and Settlements_files/1517446_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1517446/1517446_original.jpg#resized)

**Russian:**

*polygonal stone Peru1233*

**English:**

*polygonal stone Peru1233*

<!-- Local
![](Melted Megaliths and Settlements_files/1520071_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1520071/1520071_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/1520317_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/1520317/1520317_original.jpg#resized)

**Russian:**

Вот эти товарищи сейчас вылезают из под "культурного слоя", матушка Земля их просто выталкивает из себя (кадры [остюда](https://youtu.be/O_kyupQ_2Jc)):**]

**English:**

These are the comrades that are now climbing out from under the "cultural layer", Mother Earth is just pushing them out of themselves (Youtube footage: [ostyud](https://youtu.be/O_kyupQ_2Jc)):

<!-- Local
![](Melted Megaliths and Settlements_files/2452711_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2452711/2452711_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2452760_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2452760/2452760_original.jpg#resized)

**Russian:**

[Здесь](http://kadykchanskiy.livejournal.com/226911.html) вообще ~~охамевшие и никак не вписывающиеся в официальную версию~~ окаменевшие трубы в породе, в т.ч. с металлической прокладкой:

**English:**

[Here](http://kadykchanskiy.livejournal.com/226911.html) in general <del>dumbed down and in no way fitting into the official version</del> fossilized pipes in the rock, including those with a metal lining:

<!-- Local
![](Melted Megaliths and Settlements_files/2447230_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2447230/2447230_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2447608_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2447608/2447608_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2447697_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2447697/2447697_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2448100_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2448100/2448100_original.jpg#resized)

**Russian:**

Это сейчас йоги ходят по тлеющим углям, а вот настоящие древние суперйоги бродили по кипящей магме!

**English:**

These days yogis walk on smoldering embers, but the real ancient super yogis roamed on boiling magma!

<!-- Local
![](Melted Megaliths and Settlements_files/2448144_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2448144/2448144_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2448433_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2448433/2448433_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2448868_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2448868/2448868_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2449147_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2449147/2449147_original.jpg#resized)

**Russian:**

Нижеследующие фоты свидетельствуют о том, что технология работы с расплавленным камнем была утрачена не так давно:

**English:**

The following photos show that the technology for working with molten stone was lost not so long ago:

**Russian:**

Грот Буонталенти. Флоренция:

**English:**

Buontalenti Grotto. Florence:

<!-- Local
![](Melted Megaliths and Settlements_files/s640x480)
-->

![](https://pics.livejournal.com/uchitelj/pic/002325qb/s640x480#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/005cd059)
-->

![](https://pics.livejournal.com/uchitelj/pic/005cd059#resized)

**Russian:**

Из той же серии [Каменный истукан Colosso dell'Appennino](http://digitall-angell.livejournal.com/587624.html):

**English:**

From the same series, [Colosso dell'Appennino stone statue](http://digitall-angell.livejournal.com/587624.html):

<!-- Local
![](Melted Megaliths and Settlements_files/1082118108.jpeg#resized)
-->

![Image no longer available](http://api.ning.com/files/DtcI2O2Ry7AZRZ9NxzwsmfEl*phFuPE*qOCL080qe4XYyP3lF73uMy*444f8xObS8xBnl2vbtu0FpL44l-iUWMQmZ1c4o7m*/1082118108.jpeg#resized)

**Russian:**

**Если камень тут сам не вырос, как [трованты](http://digitall-angell.livejournal.com/598726.html), разумеется:**

**English:**

Unless the rock itself grew here, like [trovantes](http://digitall-angell.livejournal.com/598726.html), of course:

<!-- Local
![](Melted Megaliths and Settlements_files/2451026_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2451026/2451026_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2451308_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2451308/2451308_original.jpg#resized)

**Russian:**

Или не плавили его вовсе, а другими технологиями пользовались? Медным зубилом, например, как [тут](http://digitall-angell.livejournal.com/633912.html):

**English:**

Or they didn't melt it at all, but used other technologies? Like a copper chisel, for example, as [here](http://digitall-angell.livejournal.com/633912.html):

<!-- Local
![](Melted Megaliths and Settlements_files/2449357_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2449357/2449357_original.jpg#resized)

<!-- Local
![](Melted Megaliths and Settlements_files/2449656_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2449656/2449656_original.jpg#resized)

**Russian:**

Так что тут произошло с этими мега товарищами, как думаете? Плавили-облучали-напалмой-выжигали?

Или условия реальности внезапно поменялись, а с ними и качества материи?

Или просто на квантовом уровне вибрационку сместили, где нужно? Ну, чтоб несовместимые элементы друг с другом сшить, синхронизировать атомарные решетки в наложенных пространственно-временных теплицах, а они взяли, да потекли ненароком...

Или всего понемножку?

Вопросы, вопросы...

**English:**

So what do you think happened to these mega comrades here? Melted-irradiated-powdered-burned-out?

Or have the conditions of reality suddenly changed, and with them, the qualities of matter?

Or just on the quantum level the vibraImage was shifted where it is necessary? Well, to sew incompatible elements with each other, to synchronize atomic lattices in superimposed space-time greenhouses, and they took, but flowed inadvertently...

Or just a little bit of everything?

Questions, questions...

**Russian:**

Смотрим по теме (хотя, конечно, тема уж слишком обширная):

Не совсем можно согласиться, но рациональных зерен в рассуждениях масса (по моей информации удар не был ядерным в нашем понимании, использовалось неизвестное нам оружие).

Искажение истории. Ядерный удар. Кунгуров

Упд из комментариев:]

> *По ченнелингу Дж.Робертс "Говорит Сэт"прежняя цивилизация шла по развитию частотно-волновой технологии,в отличие от нашей электро-магнитной.С помощью подбора тональности и наложения волн гранит не плавился,но менял молекулярную структуру и становился мягким,как пластилин.После прекращения воздействия гранит возвращался в обычное состояние.По той-же технологии строились обще-известные пирамиды(левитация) и громадные пещеры(тоннели) с ровными как стекло краями.*

Мы и сами не далеки от таких подходов. Всемирно известный коралловый замок, судя по описаниям работ, был построен именно так.

> Сотрудникам Токийского университета и Технологического института Нагои удалось привести в движение мелкие объекты с помощью сложной системы акустической левитации: звуковые волны перемещали частицы полистирена диаметром от 0,6 до 2 мм в трёхмерном пространстве. Ранее предметы с помощью этой же системы удавалось двигать только в двух измерениях.

> Чтобы двигать по воздуху капли воды, полистиреновые частицы, маленькие кусочки дерева и даже шурупы, понадобились четыре ряда звуковых колонок. Эти объекты перемещали во всех направлениях в пределах, допускаемых условиями эксперимента. Движение в данном случае вызывают стоячие ультразвуковые волны.

Читать весь пост: [Левитация с помощью звука или тайны мегалитов разгаданы?](http://digitall-angell.livejournal.com/647778.html)

**English:**

Looking at the topic (although, of course, the topic is too broad):

One may not entirely agree, but there are many rational grains in the reasoning (according to my information, the strike was not nuclear as we understand it. An unknown weapon was used).

A distortion of history. Nuclear strike. Kungurov

Lifted from the comments section:

> According to J.Roberts' channeling "Seth is speaking", the former civilization went on the development of frequency-wave technology, in contrast to ours electro-magnetic one. By means of selection of tonality and imposition of waves granite did not melt, but changed its molecular structure and became soft as plasticine.After the exposure was stopped, the granite returned to its usual state. The well-known pyramids (levitation) and huge caves (tunnels) with smooth as glass edges were built using the same technology.

We are not far from such approaches ourselves. The world-famous Coral Castle, judging by the descriptions of the works, was built in this way.

> Researchers at the University of Tokyo and the Nagoya Institute of Technology have managed to put small objects in motion using a sophisticated system of acoustic levitation: sound waves moved polystyrene particles ranging in diameter from 0.6 to 2 mm in three dimensions. Previously, objects could only be moved in two dimensions using the same system.

> To move water droplets, polystyrene particles, small pieces of wood, and even screws through the air, four rows of sonic speakers were needed. These objects were moved in all directions within the limits allowed by the conditions of the experiment. The motion in this case is caused by standing ultrasonic waves.

Read the whole post: [Levitation by sound or have the mysteries of megaliths been solved?](http://digitall-angell.livejournal.com/647778.html)

**Russian:**

Чем больше изучаю эту тему, тем больше прихожу к выводу, что именно "опалённые солнцем". Слишком уж большие затраты энрегии, чтобы делать это каким-либо другим способом, кроме использования энрегии ближайшей звезды. Мы ведь наблюдаем непрерывные оплавления на огромных территориях, а не локальные явления.

в том числе и солнцем, но не обязательно. в [Атлантиде, например, использовали кристаллы, пси энергию и энергию планеты](http://digitall-angell.livejournal.com/431861.html).

**English:**

The more I study this topic, the more I come to the conclusion that these rocks were "sunburned". We observe continuous melting over vast territories, not local melting phenomena. The amount of energy required to do that was too big for it to have have been done any other way. Unless it was done using the energy of the nearest star. Including the sun, but not necessarily. In [Atlantis, for example, they used crystals, psi energy and planetary energy](http://digitall-angell.livejournal.com/431861.html).

**Russian:**

есть такая классификация цивилизаций: 1-й уровень: осваивают энергию планеты, начиная с полезных ископаемых и кончая тонкими излучениями 2: осваивают энергию ближайшей звезды 3: осваивают энергию космоса за пределами своей гелиосферы (которая является своего рода защитным коконом их мира от косм. радиации, а точнее границей реальности, выход за пределы которой возможен только при условии наличия высоких технологий или уровня сознания, позволяющих поддерживать привычный квантовый микроклимат или менять несущую частоту тел)

это только то, что касается физики, но воздействие через расширенную реальность (тонкий план) тоже нельзя забывать.

**English:**

There is a classification of civilizations:

- Level 1: master the energy of the planet, from minerals to subtle radiations (coal to nuclear energy)

- Level 2: master the energy of the nearest star

- Level 3: master the energy of space beyond their heliosphere (which is a kind of protective cocoon of their world from cosmic radiation, or rather the boundary of reality, going beyond which is possible only if they have high technology or level of consciousness that allows them to maintain the usual quantum microclimate or change the carrier frequency of bodies).

This is only as far as physics is concerned, but exposure through the extended reality (the subtle plane) should not be forgotten either.

**Russian:**

вход в трансовые состояния известен йогам, казакам, шаманам всего мира. характерники могли входить в состояние боевого спаса и значительно ускоряться по сравнению с противником, наводить морок, черпать не дюжую силу и тд. в наше время остаточным явлением является БЕСконтактный бой, но это фантики в сравнении с другими принципами, да и шарлатанов много.  легенда о мече короля Артура говорит именно о том, как один человек мог своим сознанием перейти на иную частоту и засунуть руку в камень, а остальные не могли, уровень не тот. а если и могли, но теряли в этот момент концентрацию, рука застывала в камне, поэтому не многие и пытались.  сказка о трех богатырях -- это биополе трех прокачанных и стоящих в триангуляции вокруг поселения воинов, разбросанное на десятки километров степи, и позволяющее чувствовать любое колыхание дистанционно, как мы чувствуем на себе взгляд или близко стоящего человека без физического контакта.

**English:**

In our time, there is a residual phenomenon of non-contact fighting, but it is nothing compared to other principles and there are a lot of quacks and charlatans. They were able to enter a battle-winning state, to speed up considerably in comparison to their opponent, to cast spells, to draw strength, and so on.

The legend of the sword of king Arthur tells about how one person could move his mind to a different frequency and insert his hand into a stone, while others could not, because their 'level' was not the same. The tale of the three warriors is the biofield of three triangulated warriors standing in triangulation around a settlement, scattered over dozens of kilometers of steppe, and allowing to feel any rippling remotely, as we feel a gaze or a close standing person on us without physical contact.

**Russian:**

скептики могут утверждать, что все это сказки, но я вывожу людей на тонкий план уже несколько лет и проделанная на нем энергетическая работа отражается на их физическом состоянии, что подтверждает сказанное выше.

**English:**

Skeptics may argue that all this is a fairy tale, but I have been taking people to the subtle plane for several years now and the energy work done on it is reflected in their physical state, which confirms what was said above.

**Russian:**

камни пока двигать не пробовали, всему свое время )

**English:**

We have not tried to move the stones yet. All in good time...

**Russian:**

существо, способное свободно менять свою частотную настройку между тонким планом и физическим, а точнее -- имеющее достаточно расширенное сознание, чтобы находиться "и там и там", становится для непосвященных богом, а его технологии кажутся волшебством чистой воды.

**English:**

A being who is able to freely change his frequency tuning between the subtle plane and the physical plane, or more precisely, who has expanded his consciousness enough to be "in both", becomes a god to the uninitiated, and his technology seems like pure magic.

**Russian:**

если ДНК человека отличается от обезьяны всего на 2%, то кем будет для нас тот, кто тоже отличается от нас на 2% в ту же сторону, что мы от приматов? )

**English:**

If human DNA differs from monkeys by only 2%, how would someone who is another 2% different from us (and different in the same ways as we are from primates) appear to us?

**Russian:**

По теме:  [Отголоски глобальной цивилизации](http://digitall-angell.livejournal.com/634183.html) /  [Египет. Атака техногенных богов](http://digitall-angell.livejournal.com/438220.html) /  [Индустриально развитая цивилизация существует на Земле десятки тысяч лет](http://digitall-angell.livejournal.com/546774.html) /  [Хронология цивилизации или её полное отсутствие](http://digitall-angell.livejournal.com/196066.html)

**English:**

More on this topic: 

- [Echoes of global civilization](http://digitall-angell.livejournal.com/634183.html)

- [Egypt. Attack of technogenic gods](http://digitall-angell.livejournal.com/438220.html) 

- [An industrially advanced civilization has existed on Earth for tens of thousands of years](http://digitall-angell.livejournal.com/546774.html)

- [Chronology of civilization or its complete absence](http://digitall-angell.livejournal.com/196066.html)

© All rights reserved. The original author retains ownership and rights.


