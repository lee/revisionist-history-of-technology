title: Hidden Science and Hidden Political History - Sibved
date: 2021-07-23 00:02
modified: 2022-04-24 09:45:22
category: 
tags: links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md


[Real Physics - What is Ether?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/19127.html)

[Ethero-dynamic/Ephyrodynamic hypotheses](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.atsuk.dart.ru/online/ether_dynamic_hypotheses/index.shtml)

[Man - A Bioengineering Project and Alien Being on Earth](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/219132.html)

[Sobriety Revolts in Russia in 1858-1860](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/18806.html)
