title: Champa Island stone spheres
date: 2014-01-12
modified: Mon 15 Feb 2021 18:06:27 GMT
category: 
tags: quarried Earth; terraformed Earth
slug: 
authors: gadsjl-7
summary: Champa Island is very popular with tourists cruising in the Arctic. And no wonder - it contains objects, the origin of which is still not clear, and no explanation seems likely in the near future.
status: published
from: http://borisoglebsk-online.ru/forum/viewtopic.php?pid=8165#p8165
local: stone_spheres_files

#### Translated from:

[http://borisoglebsk-online.ru/forum/viewtopic.php?pid=8165#p8165](http://borisoglebsk-online.ru/forum/viewtopic.php?pid=8165#p8165)

Possible original source:

[http://gadsjl-7.livejournal.com/2869190.html](http://gadsjl-7.livejournal.com/2869190.html)

**Russian:**

Шары острова Чампа

Остров Чампа очень популярен среди туристов, плывущих в круиз по Арктике. И неудивительно – на нем встречаются объекты, происхождение которых до сих пор не ясно, да и в ближайшем будущем решения этой загадки не предвидится.

Остров Чампа является одним из многочисленных островов арктического архипелага Земля Франца-Иосифа, который относится к наиболее удалённым уголкам России и практически не изучен. Территория этого острова относительно невелика (всего 375 кв.км), и привлекательна не столько живописными, нетронутыми цивилизацией, арктическими пейзажами, сколько загадочными каменными шарами довольно внушительных размеров и идеально круглой формой, которые заставляют теряться в многочисленных догадках относительно их возникновения на этих необитаемых землях.

**English:**

Champa Island is very popular with tourists cruising in the Arctic. And no wonder - it contains objects, the origin of which is still not clear, and no explanation seems likely in the near future.

Champa Island is one of the many islands in the Franz Josef Land Arctic archipelago, which belongs to the most remote corners of Russia and are almost unexplored. The territory of this island is relatively small (only 375 sq. km), and is attractive not so much for its picturesque, untouched-by-civilization Arctic landscapes, as for its mysterious stone balls. Of quite impressive size and perfectly round shape, them make us guess about their origin on this uninhabited land.

![](http://infoglaz.ru/wp-content/uploads/dd110cea00db.jpg#resized)

![](http://infoglaz.ru/wp-content/uploads/2136.jpg#resized)

![](http://infoglaz.ru/wp-content/uploads/arctic17-450x600.jpg#resized)

**Russian:**

Сферолиты острова Чампа представляют собой камни из плотно спрессованного песка. Они явно не вулканического происхождения, а в некоторых из них учёные даже обнаружили зубы древних акул.

Размеры многих шаров достигают нескольких метров (некоторые из них сложно охватить полностью даже троим людям), хотя, встречаются здесь и каменные шарики идеально круглой формы от нескольких сантиметров в диаметре.

**English:**

The spherolites of Champa Island are rocks of densely compacted sand. They are clearly not of volcanic origin and scientists have even found the teeth of ancient sharks in some of them.

Many of the spheres are several meters in diameter. Some of them are difficult to cover completely even with three people, although, there are also perfectly round stone spheres of a few centimeters in diameter.

![](http://infoglaz.ru/wp-content/uploads/837.jpg#resized)

![](http://infoglaz.ru/wp-content/uploads/72263aa83ec9.jpg#resized)

![](http://infoglaz.ru/wp-content/uploads/05-7.jpg#resized)

**Russian:**

По одной из версий, эти шары – результат обмывания водой обычных камней до столь идеально округлой формы.

**English:**

According to one version, these spheres are the result of water washing ordinary stones to this perfectly spherical shape.

![](http://infoglaz.ru/wp-content/uploads/champ.jpg#resized)

![](http://img-fotki.yandex.ru/get/9060/137106206.490/0_f62d5_540d9633_orig.jpg#resized)

![](http://img-fotki.yandex.ru/get/9109/137106206.490/0_f62ce_a4acb0f9_orig.jpg#resized)

![](http://img-fotki.yandex.ru/get/9749/137106206.490/0_f62d9_499c4670_orig.jpg#resized)

[![](https://img.youtube.com/vi/21fOuuKxWc8/0.jpg)](https://youtu.be/21fOuuKxWc8)

© All rights reserved. The original author retains ownership and rights.
