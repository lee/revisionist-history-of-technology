title: Quarried Earth - An industrially developed civilization has existed on Earth for tens of thousands of years
date: 2015-07-26 08:17
modified: Sat 01 May 2021 14:02:55 BST
category:
tags: quarried Earth
slug: 
authors: WakeUpHuman
summary: We have today a huge number of artifacts that can not be repeated today due to the lack of technology, equipment and specialists, and which indicate that 200 years ago there was a global civilization on Earth compared to which we are children in the sandbox. A few examples...
status: published
originally: 1342_wakeuphuman_on_quarried_Earth.html
local: 1342_wakeuphuman_on_quarried_Earth_files/

### Translated from:

[https://wakeuphuman.livejournal.com/1342.html](https://wakeuphuman.livejournal.com/1342.html)

*Translator's note: More translations of quarried Earth articles can be found by clicking on [the quarried Earth tag](./tag/quarried-earth.html). Among the articles listed is Kadykchansky's ['Planet Earth is a giant abandoned mine'](./planet-earth-is-a-giant-abandoned-mine.html) article, which is also worth reading.*

Dear readers, after reading this article, you will most likely have to conduct a complete revision of your knowledge obtained in school and higher educational institutions, at least in such disciplines as history, geography, and geology.

So, let's go. I show you the logical chain of my reasoning and conclusions.

We have today a huge number of artifacts that can not be repeated today due to the lack of technology, equipment and specialists, and which indicate that 200 years ago there was a global civilization on Earth compared to which we are children in the sandbox. A few examples:

Babolovskaya Bath. Granite. It weighs 48 tonnes.

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/47849/47849_900.jpg#resized)

Here is what a turner/fabricator who inspected it writes:

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/82573_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/82573_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/82573/82573_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/82573/82573_original.jpg)

Image text says:

> We, if you'll excuse the expression, are being "screwed" that it was allegedly made by this craftsman: Samson Sukhanov
> He spent 7 years chiseling, polishing like "Papa Carlo" and so on ... Nonsense ...
> Big photo
> With full responsibility as (universal turner - category 5), I declare, this is machine processing... Concave, convex surfaces of the bath, the most precise circumference around the diameter, the most precise spherical surface of the bottom of the bath, inside the bottom the same most precise concavity along the entire diameter... this product is not possible to make by hand, let alone polish... The impression is that it came out from a machine... polishing (too bad in the photo it can not be seen) such as class 4-5 Isaac columns. This is impossible to achieve without high-speed polishing and grinding tools. 

Alexandria column weighing 600 tons, 27 meters high. Granite. The shape is not a cone, but an entasis. Without rotation in the lathe, it is impossible to make such a product. Try to order a smaller copy of such a product with an IDEAL radius from any turner/fabricator made of solid foam or wood with a height of at least 2 meters and a diameter of 30 cm, but requiring only hand tools (planes, chisels, sandpaper), and he will refuse.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/47417_900.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/47417/47417_900.jpg#resized)

Peru, Ollantaytambo. Polygonal joints between blocks weighing 40-120 tons. You can see the quality of the fit for yourself. The blocks are combined in three planes.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/48254_original.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/48254_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/48254/48254_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/48254/48254_original.jpg)

Cappella Sansevero: Il Disinganno. Made from a single piece of marble. Without an advanced CNC machine, it is impossible to make such a thing. Over the past 50 years, nothing remotely similar in complexity has been done by any sculptor. Even with CNC machines.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/48985_original.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/48985/48985_original.jpg#resized)

Marble tombstone in the Monumental cemetery-Museum of Staglieno in Genoa.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/49482_900.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/49482/49482_900.jpg#resized)

Stone bridge in Sevastopol. Each polygonal bridge stone is essentially a separate sculpture. An example of modern stonework is behind the bridge on the left. A wall of rough stone. By current standards, it is considered quite acceptable.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/49845_900.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/49845/49845_900.jpg#resized)

Further, all the cities on the planet were built of stone in the ancient style with pre-designed layouts of streets, avenues, embankments, and so on. All cities had a stone bastion wall, the construction volume of which is often equal to the construction volume of the city itself. More information about this in my article:

[Geography of the global world before nuclear war on the example of ancient architecture and Bastion stars](http://wakeuphuman.livejournal.com/921.html)

Around 1780-1815, there was a thermonuclear war, most likely not for the first time on the planet, which resulted in the nuclear winter of 1816 - a year without summer. The Anglo-Saxons call it "Eighteen Hundred and Freeze to Death". [https://en.wikipedia.org/wiki/Year_Without_a_Summer](https://en.wikipedia.org/wiki/Year_Without_a_Summer)

For more information about the use of thermonuclear weapons 200 years ago, read the links below, if you have not read them before.

[http://wakeuphuman.livejournal.com/1116.html](http://wakeuphuman.livejournal.com/1116.html)

[http://wakeuphuman.livejournal.com/552.html](http://wakeuphuman.livejournal.com/552.html)

I will also give you a few screenshots from Google Earth photos of nuclear craters on the territory of, for example, Belarus. It is easy to find hundreds of such craters in almost all countries. The white marks around the craters are broken limestone, the main building material of the time.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/53518_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/53518_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/53518/53518_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/53518/53518_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/53909_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/53909_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/53909/53909_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/53909/53909_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/54267_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/54267_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/54267/54267_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/54267/54267_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/54469_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/54469_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/54469/54469_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/54469/54469_original.jpg)

In these examples showing craters of Belarus, there is water, because the water table is probably high. But there are a lot of craters on the surface of the planet without water. For example, in Ukraine:

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/53429_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/53429_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/53429/53429_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/53429/53429_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/52623_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/52623_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/52623/52623_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/52623/52623_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/52751_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/52751_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/52751/52751_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/52751/52751_original.jpg)

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/53171_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/53171_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/53171/53171_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/53171/53171_original.jpg)

As a result of the nuclear winter, almost all plants were frozen out and polar caps were formed. This confirms the almost complete absence of trees older than 200 years in the Northern hemisphere. Some of them were burned out in the war, some of them were frozen out. To visually assess this, Google 'Roger Fenton Crimea' or 'James Robertson Crimea' and click 'show images'. You will see photos by these two military photographers, the first sent to the Crimea in 1853 (approximately 40 years after the nuclear war) to photograph the siege of Sevastopol. Compare the vegetation then and now.

Example of one of Fenton's photographs from near Sevastopol:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/50287_900.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/50287/50287_900.jpg#resized)

Type 'Siberia photo of the 19th century' in Google. You will see many photos from the end of the 19th century, where trees have just started to grow. For example, the Sverdlovsk region:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/49929_900.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/49929/49929_900.jpg#resized)

After this war, our level of development fell back to that of a feudal society. Because they suffered least, the Anglo-Saxons began to profit. They crushed the remnants of the world for 150 years under themselves, re-invented the steam engine to run on coal and off they went. Now in the era of oil and gas, as well as nuclear energy, our industrial complex uses the entire periodic table, which was allegedly invented in a dream. In fact, it was just thrown through it.

Let's get to the most interesting part. I say that the present civilization is only a shadow of a bygone one. We are children compared to them. It is impossible to prove this through the industrial equipment of the previous civilization, so it was simply disposed of - melted down. For example, after the collapse of the USSR, drunks dug trenches and cables and water pipes from the ground were taken to hand over to the metal receiving point. How can I prove it? It's easy. If the past civilization was much more developed than our own, then the entire periodic table was also needed for the functioning of its industrial-industrial and metallurgical complex. And all the isotopes of the elements. And almost all the elements of the periodic table are found in rock and earth. So, I need to show you large-scale traces of rock removal from the slopes of mountains, from the surface of the Earth and from the ground. As well as traces of processed waste rock after its enrichment at mining and processing plants of the past. That's what we'll do. I will use the method of analogy, so it is very clear.

Until the 18th century, residential buildings were built almost exclusively of limestone.

For cutting, advanced machines were used, which produced perfect parallel edges. You will not be able to slip a blade into the seam of the masonry built of such limestone blocks. Here is a photo of a house in Crimea, the first floor of which is covered with clay for three or four meters, as in all cities in the former USSR. In Sevastopol, Simferopol, Feodosia, Kerch, all houses that are sunk into the ground for 3-4 meters have masonry of this quality.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/54744_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/54744/54744_800.jpg#resized)

It takes 200 years, and in Soviet times, this kind of limestone masonry was considered very good:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/54904_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/54904/54904_800.jpg#resized)

Masonry of the quality seen in the first photo is no longer used anywhere. This is called regression.

Now let's see how much limestone, the main building material, was mined on this planet and for how long. Using the example of the Crimea, since I am from there and the local landscapes and catacombs pushed me to the right path.

This Is Esky-Kerman. Illiterate guides will tell you that this is one of the cave cities of the Crimea, where people lived.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/55310_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/55310/55310_800.jpg#resized)

When I asked about this track, I was told that this track was made by the wheels of the carts of the local nobility.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/55728_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/55728/55728_800.jpg#resized)

Here is another 'cave city' of Crimea: Chufut-Kale.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/56263_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/56263/56263_800.jpg#resized)

And this is a modern Crimean limestone quarry. With a sawn-out quarry room. Apparently, it is convenient to store the tools in there. Mentally send this quarry 10,000-2,0000 years into the future; apply the effects of wind and water erosion to it, and what will you have at the end? That's right, another 'cave city' of Crimea. The track on the top photo, as you understand, was left by a trolley that was used to remove the sawn stone. Although, in the post-nuclear era, a quarry is a good place for survivalists. Apparently, it was used as a protected town.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/56453_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/56453/56453_800.jpg#resized)

Continue... In Crimea, there are thousands of kilometers of catacombs from which limestone was cut. The volume mined out is simply exorbitant. Moreover, it is officially stated that the stone has been mined since the 'ancient Greeks', before our era. Sawn with handsaws and extracted with chisels and spades. I went on a tour of Adzhimushkayskie quarry. Unfortunately, I didn't take a photo. The ceiling clearly shows traces of disc saws. The thickness of the disc is 4 mm and the diameter of the disk is about 2 meters - this is clearly visible on the walls, when the block was broken off after cutting, the diameter is clearly visible in the place where the disk stopped. If you are in the catacombs, pay attention.

In this photo, taken before the revolution of 1917, you can see that a segment of the limestone slope has been carefully cut out, at the bottom of which a railway passes and houses are built.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/56824_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/56824/56824_800.jpg#resized)

Now, a very important picture of the Inkerman quarry (the name of champagne) taken in 1890. On it we see a passage sawn through a hill. It is 100 meters wide and 80 meters high. In the walls of the huge sawn-out niches are one-storey houses. Under the vertical wall on the right, we see small sub-standard pieces of limestone piled up to form a slope, and limestone crumbs that fell from under the saws. Some of these niches are the beginning of catacombs that extend hundreds of kilometers deep. Large-scale underground limestone mining was carried out. During the Second World War, these catacombs housed a headquarters, a hospital, a clothing shop, and warehouses. Trucks moved freely inside. During the retreat, the entrances were blown up. By the way, there are ancient catacombs under any city on the planet. Google it. The length of the catacombs under Odessa is 2,500 km.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/57214_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/57214/57214_800.jpg#resized)

And now we open the manipulation. What you are served under the guise of rocks, canyons and gorges is nothing more than quarryies. Both very ancient quarries and relatively recent quarries.

So, Crimea... Belogorsk. White Rock. This is a limestone quarry. The wall was formed as a result of cutting off the slope of the hill. At the foot of the wall is a characteristic mound of limestone crumbs and rubble.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/57400_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/57400/57400_800.jpg#resized)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/57826_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/57826/57826_800.jpg#resized)

Furthermore, do you see this passage from which a mass of limestone was removed in the Bakhchisaray district? It is passed off as a valley. The slopes of limestone crumbs under the walls are already covered with oak forests:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/57988_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/57988/57988_800.jpg#resized)

Compare the upper photo of the 'gorge' with the photo of Inkerman quarry from the 19th century. The mounds at the vertical limestone walls were not yet wooded:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/117111_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/117111/117111_800.jpg#resized)

And a picture from 1855 of this place with an aqueduct on the other side. In the background, you can also see the giant doorway limestone quarries. Clickable:

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/156201_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/156201_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/156201/156201_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/156201/156201_original.jpg)

Same. Bakhchisaraysky district:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/58128_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/58128/58128_800.jpg#resized)

This image shows a locality. It is located at the bottom of an ancient quarry. But it is called the valley that was washed by the river. This is nonsense. On the contrary, after this mountain was reduced, water flowed from the broken aquifer along the bottom of the quarry. Or perhaps a stream that previously flowed along a different route turned here. This is the norm of any working day. The river can't wash away the mountain range that stands in its way. The range will be a dam in the river's path. Many of you who are older have seen streams in your childhood that flow from a vertical limestone wall. Here, for 30-40 years, this stream has increased the diameter of the hole from which it flows? So-and-so

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/58623_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/58623/58623_800.jpg#resized)

Well, does the scale of stone mining in little Crimea impress you? Looking ahead, I will say that this is still a small thing. On this planet, there is not a single piece of rock, probably 100 meters deep across the entire area, that was not at one time extracted, ground, chewed and thrown away. This is not a planet, this is a giant quarry where the entire periodic table is extracted in the most barbaric way.

And now look at the photo and pay attention to the tiered structure of quarries and mines. Mining of iron ore at the Lebedinsky deposit by open explosive method.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/58786_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/58786/58786_800.jpg#resized)

Magnetic Mountain, Urals

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/58975_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/58975/58975_800.jpg#resized)

Cheremshansky nickel mines

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/59215_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/59215/59215_800.jpg#resized)

Copper mines, Kennecott, Utah, USA

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/61143_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/61143/61143_800.jpg#resized)

East Ore Quarry

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/59854_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/59854/59854_800.jpg#resized)

Bingham Canyon copper quarry in Utah, USA

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/60543_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/60543/60543_800.jpg#resized)

Magnesium crankcase (scrape? sump?) in Navarre (Translator's note: possibly Canteras Aldoyar, 42.88352, -2.20457)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/60886_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/60886/60886_800.jpg#resized)

A rotary excavator. Power consumption in the region of 4-5 megawatts. But we will go into detail later. Just remember how it shows its pedigree. How it forms, in fact, a canyon lined with big terraces.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/61259_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/61259/61259_800.jpg#resized)

A rotary excavator cuts through the mountain range in tiers. It forms a structure exhibiting right angles when viewed from above.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/61920_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/61920/61920_800.jpg#resized)

Another excavator took the rock in a semicircle in front of it

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/63364_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/63364/63364_800.jpg#resized)

And now I will show you mountains, mountain ranges, gorges, canyons in almost uninhabited places with different romantic names. They are often named after a 'discoverer'. Do academics and professors of geology and geography not see this?

'Mountain' on the Kola Peninsula. I don't know the name.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/61499_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/61499/61499_800.jpg#resized)

'Mountains' in Antarctica. A semi-circular rock carved out by a rotary excavator in Antarctica that was only discovered in 1820!

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/62205_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/62205/62205_800.jpg#resized)

Antarctica. There are even traces of heavy machinery tracks.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/62355_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/62355/62355_800.jpg#resized)

Greenland. Mountains Of Votkinsk. How do you like the scale of production?

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/62517_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/62517/62517_800.jpg#resized)

Greenland. Flight From Frankfurt To Los Angeles.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/86468_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/86468/86468_800.jpg#resized)

Gunnbjorn. The highest mountain in Greenland. 3700 meters. No problem. Gutted almost completely.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/63636_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/63636/63636_800.jpg#resized)

Svalbard, Norway. Aurora Borealis on the background of a quarry

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/63786_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/63786/63786_800.jpg#resized)

Antarctica. Transantarctic Mountains. There are still traces of machinery at the bottom

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/64168_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/64168/64168_800.jpg#resized)

Antarctica. Transantarctic Mountains. The quarry system. Pay attention to the background.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/64317_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/64317/64317_800.jpg#resized)

The Mount Kailash. Tibet. With a height of 6638 meters! Have you ever seen heavy mining equipment raised to such a height in our time?

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/64824_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/64824/64824_800.jpg#resized)

The Mount Kailash. Tibet.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/65100_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/65100/65100_800.jpg#resized)

Valley Of The Goblins State Park, Utah, Usa

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/65389_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/65389/65389_800.jpg#resized)

Gloss Mountains State Park, Oklahoma, Usa. The height of cynicism is to call spent careers national parks.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/65571_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/65571/65571_800.jpg#resized)

Now take a deep breath and look wide-eyed. Grand canyon, Arizona, USA. It's just a giant quarry. Gutted territory. Millions of tourists think that this is almost a miracle of the world, because they were told so.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/65912_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/65912/65912_800.jpg#resized)

Quarry of the Grand canyon, Arizona, USA. There are no signs of water erosion anywhere. Only shock and explosion impact on the rock.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/67078_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/67078/67078_800.jpg#resized)

Quarry-Rocks of the Svalbard archipelago

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/66358_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/66358/66358_800.jpg#resized)

Quarry of the Grand canyon. Cut the stone with a circular saw.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/68038_original.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/68038/68038_original.jpg#resized)

Quarry in Australia. Blue Mountains

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/66650_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/66650/66650_800.jpg#resized)

Blue Mountains from a different angle

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/67068_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/67068/67068_800.jpg#resized)

Blue Mountains. Vertical wall. Compare it with the wall of a marble quarry in the Alps, which has not yet been subjected to water and air erosion:

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/116541_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/116541_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/116541/116541_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/116541/116541_original.jpg)

Marble mining in the Alps

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/116854_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/116854_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/116854/116854_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/116854/116854_original.jpg)

A giant quarry. I don't know where. The photo is offered as Wallpaper on the desktop throughout the Internet.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/67510_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/67510/67510_800.jpg#resized)

Caprock Canyons State Park Texas. Again a national Park created from a spent quarry in the United States

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/68538_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/68538/68538_800.jpg#resized)

In the spent quarries where there is a lot of moisture, people are engaged in farming - Banaue Rice terraces

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/68771_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/68771/68771_800.jpg#resized)

Рисовые террасы Банауэ
The rice terraces of Banaue (Borneo?)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/69104_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/69104/69104_800.jpg#resized)

And here is the Canyon De Chelly National Monument. USA. National monument. Here saws, apparently, were used for mining.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/69204_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/69204/69204_800.jpg#resized)

Painted hills - painted hills in Oregon. Officially:

> This place attracts thousands of tourists every year, especially those interested in Geology and paleontology. Of course, a considerable number of photographers also come here in search of magical landscape shots.

Painted hills is an area protected by the US government and all 1,267 hectares of land represent the historical heritage of modern Americans.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/81110_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/81110/81110_800.jpg#resized)

Mountains Ravine. Impressive volumes.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/69467_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/69467/69467_800.jpg#resized)

South Africa. Orange River and mountains.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/69764_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/69764/69764_800.jpg#resized)

Timna national park in Israel. Timna quarry in Israel

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/70332_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/70332/70332_800.jpg#resized)

Green canyon quarry in China

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/70725_original.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/70725/70725_original.jpg#resized)

A flooded quarry is the Charvak reservoir in Uzbekistan.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/83984_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/83984/83984_800.jpg#resized)

Flooded quarry Charvak Reservoir in Uzbekistan. Different perspective

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/84469_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/84469/84469_800.jpg#resized)

I'll tell you more. There don't seem to be any natural mountains or gorges on this planet. See the photo? This is a giant quarry. Although there are no obvious tiers, but it is clear that this is a quarry. I trust my gut.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/71023_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/71023/71023_800.jpg#resized)

Now we move on to the worst part. Now I will show you how to create deserts on Earth. Note how the rotary excavator removes layer after layer of rock from large areas.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/71543_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/71543/71543_800.jpg#resized)

Another photo. There are two of them here. They immediately remove two layers from the same area. In the lower left corner, a large bulldozer is driving. Estimate the scale.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/71875_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/71875/71875_800.jpg#resized)

This photo is clickable. The excavator removes a layer with a height of 30-40 meters. The bottom of the quarry is a huge area and it is absolutely flat as a table. Convenient for moving the excavator.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/72064_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/72064_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/72064/72064_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/72064/72064_original.jpg)

A couple more photos:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/72273_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/72273/72273_800.jpg#resized)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/72675_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/72675/72675_800.jpg#resized)

It turns out that on our planet there are quarries the size of several countries or the size of an entire desert. For example, on the territory of Uzbekistan, Turkmenistan, Tajikistan, Afghanistan, Kazakhstan, Iran, there are no fertile soils in most of them, because almost the entire area of these countries has been stripped of a layer of rock 100 meters thick, along with the soil and all living things. This is hard to believe, but you will have to believe your eyes. It seems that the Aral sea and the Caspian sea are giant flooded quarries. Yes, all the areas on the planet painted in Google maps in yellow - this is the bottom of quarries.

The Bosjira tract is located in the Western part of the Ustyurt plateau. Kazakhstan. Do you see that the high ground behind the Volkswagen is a wall formed by a rotary excavator?

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/72863_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/72863/72863_800.jpg#resized)

The Ustyurt Plateau. Clickable. In the middle of the image is a group of cars. As far as the eye can see, the top layer of soil with a thickness of 100 meters has been removed. If you splash water here with a layer of 15 meters, you will get an analog of the sea of Azov.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/73071_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/73071_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/73071/73071_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/73071/73071_original.jpg)

Azov sea. A flooded old quarry. The bottom is as smooth as a table on which the rotary excavators rolled. The maximum depth is 15 meters. Probably mined thorium. Type in Google-radioactive Sands of Azov.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/81872_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/81872/81872_800.jpg#resized)

The edge of the Karakum desert. The area is 350,000 km2. Clickable. The impression that some kind of planetary Ripper was working on the planet.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/73336_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/73336_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/73336/73336_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/73336/73336_original.jpg)

In reality, a quarry. For the population - Yangikala Canyons. Turkmenistan.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/73949_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/73949/73949_800.jpg#resized)

In reality, a quarry. For the population - tuzbair Plateau. Kazakhstan

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/74041_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/74041/74041_800.jpg#resized)

USA, Monument Valley. Clickable. Previously, the area of this territory was as high as the top of the stub directly ahead. A layer several hundred meters high is hidden.

<!-- Local
[![](1342_wakeuphuman_on_quarried_Earth_files/74245_800.jpg#clickable)](1342_wakeuphuman_on_quarried_Earth_files/74245_800.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/74245/74245_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/74245/74245_800.jpg)

USA, Monument Valley. It's the same here

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/74616_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/74616/74616_800.jpg#resized)

Namibia. The desert is the bottom of a quarry

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/74797_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/74797/74797_800.jpg#resized)

Egypt. Remove the top layer along with the soil. They also burned the house.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/75142_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/75142/75142_800.jpg#resized)

Most of Australia has been wiped clean. There is no soil, all red desert.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/75518_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/75518/75518_800.jpg#resized)

Australia

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/75567_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/75567/75567_800.jpg#resized)

Nigeria. Desert.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/75858_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/75858/75858_800.jpg#resized)

The conclusion is this for deserts: they are completely man-made. They appeared as a result of long-term metallurgical activity. And more than that. You can safely replace the words canyon, gorge, rock, ravine, plateau, mountain lake, or just lake in your vocabulary with the words quarry, mine, and flooded quarry, flooded mine.

On old foreign maps of the 16th and 17th centuries, where the territory of Ukraine, Russia and other former republics is often marked as Tartary, the rivers flow more or less straight, turning smoothly. Modern rivers in this area strongly loop, sometimes turning 180 degrees. Here, for example, is a screenshot of the Tobol river in Siberia:

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/82751_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/82751/82751_800.jpg#resized)

One bank of such rivers is often higher than the other and this is explained by the strength of Coriolis. I suggest not to touch Coriolis and look at the following short video of the work of the ERSHRD 5000 rotary excavator and 2 photos of the river in Russia. Sorry for the checkmate in the video, but it is very visual.

<!-- Local
[https://youtu.be/mhHv_dXcmAU](https://youtu.be/mhHv_dXcmAU)
-->

<!-- Local
[Роторный экскаватор ЭРШРД-5000-mhHv_dXcmAU.mp4](1342_wakeuphuman_on_quarried_Earth_files/Роторный экскаватор ЭРШРД-5000-mhHv_dXcmAU.mp4)
-->

<!-- [https://www.youtube.com/watch?v=mhHv_dXcmAU](Роторный экскаватор ЭРШРД-5000-mhHv_dXcmAU.mp4) -->

[Роторный экскаватор ЭРШРД-5000-mhHv_dXcmAU.mp4 (https://www.youtube.com/watch?v=mhHv_dXcmAU)](https://www.youtube.com/watch?v=mhHv_dXcmAU)

And now a photo of the river in the Voronezh region. The place is called "Crimobile". The river had never flowed through this loop before. It flowed here when the height of the landscape changed after the removal of soil by a rotary excavator.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/77214_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/77214/77214_800.jpg#resized)

Crimobile from a different angle. On an island in the middle, overgrown with bushes, stood a rotary excavator.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/77733_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/77733/77733_800.jpg#resized)

What does official science tell us about this career? I quote:

> The Krivoborye cliff is a huge ravine, a geological section that is valuable for studying the geological past of the Voronezh region. According to the features of the soil structure and organic remains, geologists restore natural events that occurred here several millennia ago.

The name "Krivoborye" was given in honor of the shape of the forest, which is located on one of its cliffs. This is a pine forest, which is home to many large wild animals. Currently, "Crimobile" is considered a national monument of nature. Any activity other than sightseeing and educational activities is prohibited on its territory. This place is often visited by students of geological and geographical faculties to do research.

While all the geologists of the planet are unsuccessfully reconstructing what natural events occurred in Krivoborye a few millennia ago, I will do it for them - a rotary excavator 200-300 years ago, judging by the undisturbed slope. And this situation is typical for the entire planet. Often in conversation you can hear the accusations of Conspirology. They say that it is impossible to hide anything from society. And there is some truth in this. Why hide it when everything is in plain sight and no one sees?

Or here's another khutzpa. It is obvious that a rotary excavator was working on limestone in Switzerland. But people are presented with:

> Creux-du-Van (Creux-du-Van), a giant rocky depression in the shape of a horseshoe with a width of 1400 meters and a height of about 200 meters, a natural amphitheater formed as a result of rock erosion on the territory of the Jura mountain range in the Canton of Neuchatel.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/81519_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/81519/81519_800.jpg#resized)

Siberia. The Anabar Plateau. River Jogja

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/87038_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/87038/87038_800.jpg#resized)

Go on.

We put ourselves in the place of the one who is gutting the planet and move on to the next metallurgical stage. The rock with a certain content of the desired element was extracted. What should I do with it? Before it is sent to the smelting or extraction of the desired element in any other way, the ore needs to be enriched to increase the percentage of content. To do this, it is sent to GOKi - mining and processing plants. There, the concentrate is separated, and the empty waste rock is taken to the dump or to the landfill. You will logically ask me, where are the deposits of waste rock with such huge volumes of ore extraction? And I must show you. Replace the words hill, mound, volcano, hillock in your vocabulary with the words dump and waste and everything will fall into place in your head. But it's better to see it once:)

This is a waste heap with the spent rock of the Donbass. Their height sometimes reaches 300 meters. There are chemical reactions inside them, they burn and sometimes explode when excess pressure builds up inside.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/77911_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/77911/77911_800.jpg#resized)

And more

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/78162_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/78162/78162_800.jpg#resized)

And this is just the Vesuvius Terrikon in Italy with a height of 1281 meters. But it was called a volcano, because it burns and once exploded. And it was also named so that you wouldn't guess:)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/78348_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/78348/78348_800.jpg#resized)

Shall we look into the Caldera? If this is a volcano, then the Caldera walls must be melted by liquid lava. And if it is a waste ground, the walls will be layered and will consist of loose rock that can be dug with a shovel. Look carefully. And what do we see? Slagheap.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/78651_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/78651/78651_800.jpg#resized)

And this is the Klyuchevsky hill. Lit. 4850 meters.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/79026_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/79026/79026_800.jpg#resized)

Waste heap Taranaki in New Zealand. Where are the frozen crystallized lava flows? The slopes are entirely composed of loose rock.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/86707_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/86707/86707_800.jpg#resized)

And this is the Santa Anna landfill in El Salvador

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/79153_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/79153/79153_800.jpg#resized)

Blasted the top of the heap Popocatepetl in Mexico. The height is 5426 meters.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/79516_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/79516/79516_800.jpg#resized)

The heap is the Maly Semyachik volcano, Kamchatka Krai
From Wikipedia:

> It is a short ridge about 3 km long at the top, consisting of three merged cones — the Northern ancient one, which is the highest (1560 m)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/79756_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/79756/79756_800.jpg#resized)

Tolbachik
From Wikipedia:

> A volcanic massif in Kamchatka, in the South-Western part of the Klyuchevskaya group of volcanoes. The height is 3682 meters, it consists of a Sharp Tolbachik (3682 m) and a flat Tolbachik merged with it (active, height – 3140 m). There are more than 120 cinder cones on the slopes of the Flat Tolbachik and in the adjacent Tolbachinsky valley.
Slag, Carl!

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/79986_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/79986/79986_800.jpg#resized)

Have you set aside 4 paychecks yet to visit the slopes of the Fujiyama landfill in Japan? Hurry up, it's worth it:)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/80225_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/80225/80225_800.jpg#resized)

We've sorted out the trash cans. Now we go to the dumps, which do not have a pronounced conical shape. Here the rule is, if loose, layered and you can dig with a shovel, then most likely the dump of waste rock that our ancestors piled up in a hurry to live.

For example, the gorgeous geological Park in China Zhangye Danxia. Colored mountains, beauty. Under state protection, of course. Tourists are driven exclusively along paved paths, so that God forbid tourists do not fall into this toxic waste henna.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/80463_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/80463/80463_800.jpg#resized)

Blade-mount Smittia, Norilsk

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/83397_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/83397/83397_800.jpg#resized)

Or for example, you are walking along the valley of the Sugran river, in the Pamirs. Around piled up some earth in heaps, nothing grows. These are dumps.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/80675_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/80675/80675_800.jpg#resized)

Pyatigorsk mountains are very similar to the waste areas

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/81263_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/81263/81263_800.jpg#resized)

The Philippines is one of the most desirable places to visit on the planet, and if you don't know much about the Philippines, then you should definitely hear about the world-famous island of Bohol. It became famous thanks to the "chocolate hills", which stretch over an area of about 50 square kilometers, in the number of 1268 hills of regular conical shape with a height of up to 100 meters.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/82138_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/82138/82138_800.jpg#resized)

In General, you understand the principle. We saw a hill near the house - look closely, think. Most likely it will be man-made.
And there are no natural caves on Earth. Reviewed a lot of videos, all the caves are different degrees of antiquity mountain underground passages, often multi-tiered. Yes, many of them collapsed and started to look chaotic, but this did not stop them from being artificial.

Important addition about mining waste from the blogger [mylnikovdm](https://mylnikovdm.livejournal.com/)

> By the way, one of the readers in my blog gave an interesting tip.

> Many people ask about where the dumps from the processing plant are located, which should be commensurate with the shown scale of production.

> At the same time, we have a huge amount of sand in the deserts, the origin of which no one has yet been able to explain properly, especially when the deserts are found inside the continents, ie inland. It is likely that the sand is the waste of the enrichment process. If we produce chemical enrichment, then for better contact of the chemical with the rock, it must be crushed to increase the surface area. That is, sand is best suited for these purposes. In this case, after processing, there remains only empty rock. That is, silicon or quartz, and everything else, including metals and their compounds, goes into solution. Then we throw out the empty rock.

> In favor of this version is the fact that there are plenty of sand placers on all continents, even in the center of Siberia. At the same time, many of them are located relatively close to mining sites, such as the "Grand canyon" and the Nevada desert in the United States. In Central Asia, the middle East, and Egypt, deserts are also adjacent to mountains that have traces of mining.

> A lot of sand along the river valleys, which also fits into this version. Sand was poured into the river, and the stream carried it along the riverbed.

> Another argument in favor of this version is that in most cases, river sand consists of "waste rock", that is, silicon or quartz, and not of those minerals that are found along the riverbed.

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/84682_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/84682/84682_800.jpg#resized)

The conclusions of this story can be drawn as follows:

1. The volume of production prohibitive. It is obvious that on Earth is consumed well if 5% of the extracted. The earth looks like someone's giant quarry. Perhaps this quarry just serves humanity.

2. People come and go, States appear and collapse, Nations form and collide and disappear. One thing doesn't change: 

[God's millstones grind slowly but surely](https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D0%BB%D1%8C%D0%BD%D0%B8%D1%86%D1%8B_%D0%93%D0%BE%D1%81%D0%BF%D0%BE%D0%B4%D0%BD%D0%B8)

A poem taken from the link:

> God's Mill

> The Windmill Of The God

> Is very good.

> It grinds slowly.

> Slowly but surely

> Walking wheel.

> Absolutely everything

> Will be relocated.

The final destination of our route is probably the same as in the picture below. Even so, God's millstones will probably not stop, so we humans will kindly design and build self-replicating robots by the time they take our place. They will not depend on the composition of the atmosphere, and we will go down in history. By the way, now you know what 'volcanoes' are on Mars:)

<!-- Local
![](1342_wakeuphuman_on_quarried_Earth_files/83145_800.jpg#resized)
-->

![](http://ic.pics.livejournal.com/wakeuphuman/74019716/83145/83145_800.jpg#resized)

But the logic of the process suggests that the one who has a profit from this, our departure from the scene will not be very upset. Apparently, he is not here, he does not live here. I would like to know who this person is, of course. As we all know, between the Lord (master), whose name can not be called and must be written through the dash as G-d and we have intermediaries - God-chosen. You should ask them. Ordinary Jews hardly knew even what I have shown in this post. But high-ranking people know for sure. Start asking. We need a dialogue about this. In General, Judaism and its derivative religions, in the light of the revealed facts, are represented as a system of managing a planet - a quarry for a percentage. Periodically, when employees get into the subject and start to rebel, we have to arrange a restart of the system by organizing wars and generation gaps. And since we have figured out what's what, it may soon begin :) But what will be, will be. The power is in the truth. And the truth is that a society that lives in a closed system, constantly multiplies and is set to consume more tomorrow than yesterday - is doomed as soon as it reaches the ceiling on the amount of available energy or territory. You can only develop and multiply indefinitely in an infinite Universe. If we don't get out Of the earth quarry, we're doomed.

But on the other hand, if they wanted to hide it, Sergey Brin would never have made the public service Google Maps, Google Images, or just Google. And no one would ever collect material on this topic in a bunch in one place. So, it's not that simple.

And for a snack, I want to show a couple of videos on the topic. The last 40 seconds of the video are especially relevant:

John Carter: [https://youtu.be/k_UqyNxs_Ak](https://youtu.be/k_UqyNxs_Ak)

Elysium - Heaven is Not On Earth - second Trailer: [https://youtu.be/fFiXjBwIvIU](https://youtu.be/fFiXjBwIvIU)

Well, bye! Look for those who know the answers to your questions and don't hesitate to ask.

For example, the first half-joking question: how many rockets fly into orbit per year and what do they carry besides satellites?:) For example, a gram of radium costs $230. A gram of Osmium-187 is $200,000, and a gram of California-252 is $6,500,000. With the cost of putting 1 kg into orbit at $3000, it is quite cost-effective to put rare elements and isotopes up there. The dirt remains here, a clean product to the owner :)

© All rights reserved. The original author retains ownership and rights.
