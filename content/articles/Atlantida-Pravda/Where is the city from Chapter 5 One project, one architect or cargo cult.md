title: Where is the city from? Chapter 5. One project, one architect or cargo cult?
date: 2013-11-04
modified: 2022-04-24 10:08:21
category: 
tags: St Petersburg
slug: 
authors: ZigZag
summary: I can show a possible theory about the unity of all the world's (so-called) religious buildings. Let's take a look at St. Isaac's Cathedral
status: published
originally: Mysteries of history. Controversial facts and speculations .: Where does the city come from? Chapter 5. One project, one architect or cargo cult?.html
from: http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html
local: Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files

### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available     from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-   links.html). The translation below is kept purely for archival reasons.*

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

I can show a possibile theory about the unity of all the world's (so-called) religious buildings.

Let's take a look at St. Isaac's Cathedral

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0def6a9d5f00.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0def6a9d5f00.jpg)
-->

[![](http://2.bp.blogspot.com/-tH554IRUBLQ/UVwCx4I1vTI/AAAAAAAACZE/GyCYiWb7Bnc/s1600/0def6a9d5f00.jpg#clickable)](http://2.bp.blogspot.com/-tH554IRUBLQ/UVwCx4I1vTI/AAAAAAAACZE/GyCYiWb7Bnc/s1600/0def6a9d5f00.jpg)

I deliberately took a view from above, and now we will remove the antique colonnade from it, from all four sides, and ... let's compare with this

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_005.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_005.jpg)
-->

[![](http://4.bp.blogspot.com/-gQ0EeJB8iBc/UVwDYPfHf4I/AAAAAAAACZM/W1g08JIUacQ/s1600/%D0%A3%D1%81%D1%8B%D0%BF%D0%B0%D0%BB%D1%8C%D0%BD%D0%B8%D1%86%D0%B0+%D0%93%D0%BE%D0%BB+%D0%93%D1%83%D0%BC%D0%B1%D0%B0%D0%B7.jpg#clickable)](http://4.bp.blogspot.com/-gQ0EeJB8iBc/UVwDYPfHf4I/AAAAAAAACZM/W1g08JIUacQ/s1600/%D0%A3%D1%81%D1%8B%D0%BF%D0%B0%D0%BB%D1%8C%D0%BD%D0%B8%D1%86%D0%B0+%D0%93%D0%BE%D0%BB+%D0%93%D1%83%D0%BC%D0%B1%D0%B0%D0%B7.jpg)

Tomb of Gol Gumbaz

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_008.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_008.jpg)
-->

[![](http://1.bp.blogspot.com/-stq3irYfj8Q/UVwDk5C-77I/AAAAAAAACZU/GHv9AUFx2_E/s1600/%D0%9C%D0%B0%D0%B2%D0%B7%D0%BE%D0%BB%D0%B5%D0%B9+%D0%A5%D1%83%D0%BC%D0%B0%D1%8E%D0%BD%D0%B0.jpg#clickable)](http://1.bp.blogspot.com/-stq3irYfj8Q/UVwDk5C-77I/AAAAAAAACZU/GHv9AUFx2_E/s1600/%D0%9C%D0%B0%D0%B2%D0%B7%D0%BE%D0%BB%D0%B5%D0%B9+%D0%A5%D1%83%D0%BC%D0%B0%D1%8E%D0%BD%D0%B0.jpg)

Humayun's mausoleum

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/II.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/II.jpg)
-->

[![](http://4.bp.blogspot.com/-Vhfsjhzh31s/UVwDwzIfBMI/AAAAAAAACZc/8Wb8tLN3SwE/s1600/%D0%9D%D0%B5%D0%B4%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9+%D0%BC%D0%B0%D0%B2%D0%B7%D0%BE%D0%BB%D0%B5%D0%B9+%D0%90%D0%BB%D0%B8+%D0%90%D0%B4%D0%B8%D0%BB+%D0%A8%D0%B0%D1%85%D0%B0+II+%D0%91%D0%B8%D0%B4%D0%B6%D0%B0%D0%BF%D1%83%D1%80%D0%B0.jpg#clickable)](http://4.bp.blogspot.com/-Vhfsjhzh31s/UVwDwzIfBMI/AAAAAAAACZc/8Wb8tLN3SwE/s1600/%D0%9D%D0%B5%D0%B4%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9+%D0%BC%D0%B0%D0%B2%D0%B7%D0%BE%D0%BB%D0%B5%D0%B9+%D0%90%D0%BB%D0%B8+%D0%90%D0%B4%D0%B8%D0%BB+%D0%A8%D0%B0%D1%85%D0%B0+II+%D0%91%D0%B8%D0%B4%D0%B6%D0%B0%D0%BF%D1%83%D1%80%D0%B0.jpg)

The unfinished mausoleum of Ali Adil Shah II of Bijapur

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_007.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_007.jpg)
-->

[![](http://1.bp.blogspot.com/-UXk5E7FCVgk/UVwD_eP_5VI/AAAAAAAACZk/Plh8t6pUSlk/s1600/%D0%A1%D0%B0%D1%81%D0%B0%D1%80%D0%B0%D0%BC.jpg#clickable)](http://1.bp.blogspot.com/-UXk5E7FCVgk/UVwD_eP_5VI/AAAAAAAACZk/Plh8t6pUSlk/s1600/%D0%A1%D0%B0%D1%81%D0%B0%D1%80%D0%B0%D0%BC.jpg)

Sasaram

I'm not even talking about orientation, these architectural compositions do not seem similar to you: four columns on the sides, and a dome in the center, what kind of world architecture is this for a blueprint.

And finally the most famous...

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/-.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/-.jpg)
-->

[![](http://4.bp.blogspot.com/-6lCpzFAS-d0/UVwFK_eEVLI/AAAAAAAACZw/fA6FbtR5h5c/s1600/%D0%90%D0%B9%D1%8F-%D0%A1%D0%BE%D1%84%D0%B8%D1%8F.jpg#clickable)](http://4.bp.blogspot.com/-6lCpzFAS-d0/UVwFK_eEVLI/AAAAAAAACZw/fA6FbtR5h5c/s1600/%D0%90%D0%B9%D1%8F-%D0%A1%D0%BE%D1%84%D0%B8%D1%8F.jpg)

Hagia Sophia

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/37389.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/37389.jpg)
-->

[![](http://4.bp.blogspot.com/-yFYZHHClnoc/UVwFgXlG_LI/AAAAAAAACZ4/J-6hfGjWCII/s1600/37389.jpg#clickable)](http://4.bp.blogspot.com/-yFYZHHClnoc/UVwFgXlG_LI/AAAAAAAACZ4/J-6hfGjWCII/s1600/37389.jpg)

And the Taj Mahal itself

**Russian:**

А теперь включим воображение, откинем мечети и приставим к нему античную колоннаду Исаакиевского собора ... Что получим в результате, а вот это и получим.

**English:**

And now let's turn on the imagination, throw away the mosques and attach to it the ancient colonnade of St. Isaac's Cathedral... What we get as a result, and this is what we get.


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/f_15762564.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/f_15762564.jpg)
-->

[![](http://3.bp.blogspot.com/-QiKqRozBPWM/UVwGg-XTFbI/AAAAAAAACaE/Wj10fYqdnXs/s1600/f_15762564.jpg#clickable)](http://3.bp.blogspot.com/-QiKqRozBPWM/UVwGg-XTFbI/AAAAAAAACaE/Wj10fYqdnXs/s1600/f_15762564.jpg)


**Russian:**

Да и топография местности, и положение у реки, как всё похоже

**English:**

And the topography of the area, and the position by the river, as it were.


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/68338134_a21.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/68338134_a21.jpg)
-->

[![](http://4.bp.blogspot.com/-2ehJ6LHvWew/UV6iJ13eDqI/AAAAAAAACac/5GE_4ihw7gY/s1600/68338134_a21.jpg#clickable)](http://4.bp.blogspot.com/-2ehJ6LHvWew/UV6iJ13eDqI/AAAAAAAACac/5GE_4ihw7gY/s1600/68338134_a21.jpg)


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_003.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_003.jpg)
-->

[![](http://4.bp.blogspot.com/-M8FTn8TBIWA/UV6izZnnvYI/AAAAAAAACak/lSP4nKwooys/s1600/%D0%A2%D0%9C.jpg#clickable)](http://4.bp.blogspot.com/-M8FTn8TBIWA/UV6izZnnvYI/AAAAAAAACak/lSP4nKwooys/s1600/%D0%A2%D0%9C.jpg)


**Russian:**

И внутренний двор строго на противоположной стороне от реки.

**English:**

And the courtyard is strictly on the opposite side of the river.


**Russian:**

А вот Вам ещё одно чудо света, так похожее на предыдущие и видом, и расположением - храм Христа Спасителя.

**English:**

And here is another miracle of light, so similar to the previous ones, both in view and location - the temple of Christ the Savior.


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/948.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/948.jpg)
-->

[![](http://3.bp.blogspot.com/-8usN2cO6YsA/UV6rSSLyfvI/AAAAAAAACa8/y0WqMKOR6EY/s1600/948.jpg#clickable)](http://3.bp.blogspot.com/-8usN2cO6YsA/UV6rSSLyfvI/AAAAAAAACa8/y0WqMKOR6EY/s1600/948.jpg)


**Russian:**

Хотя согласно первоначального проекта, храм должен был выглядеть вот так

**English:**

Although according to the original design, the temple was supposed to look like this...


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/800px-Vitberg_Cathedral.gif#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/800px-Vitberg_Cathedral.gif)
-->

[![](http://1.bp.blogspot.com/-Yrc-yMB_EY4/UZ8U7AYj7_I/AAAAAAAADOA/rSYEL1isbcw/s1600/800px-Vitberg_Cathedral.gif#clickable)](http://1.bp.blogspot.com/-Yrc-yMB_EY4/UZ8U7AYj7_I/AAAAAAAADOA/rSYEL1isbcw/s1600/800px-Vitberg_Cathedral.gif)


**Russian:**

Я долго думал, на что похожи культовые постройки, разбросанные по всему миру, мавзолеи, храмы..., а потом, случайно, картинки сложились... Что это, на что похоже?

**English:**

I thought for a long time about what cult buildings scattered around the world, mausoleums, temples... and then, by chance, the pictures came together... What is it, what does it look like?


**Russian:**

Ватикан

**English:**

Vatican


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/11.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/11.jpg)
-->

[![](http://1.bp.blogspot.com/-kv7zJ3VTzZ4/UWJ-VShYhXI/AAAAAAAACcE/_8gjYXDLGc8/s1600/11.jpg#clickable)](http://1.bp.blogspot.com/-kv7zJ3VTzZ4/UWJ-VShYhXI/AAAAAAAACcE/_8gjYXDLGc8/s1600/11.jpg)


**Russian:**

Капитолий

**English:**

Capitol


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/541.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/541.jpg)
-->

[![](http://2.bp.blogspot.com/-7OFpZSeB62Y/UWKdVQGbiiI/AAAAAAAACgA/ifn0We7-lh4/s1600/541.jpg#clickable)](http://2.bp.blogspot.com/-7OFpZSeB62Y/UWKdVQGbiiI/AAAAAAAACgA/ifn0We7-lh4/s1600/541.jpg)


**Russian:**

Петерскирхе (Вена)

**English:**

Peterskeirche (Vienna)


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_004.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_004.jpg)
-->

[![](http://1.bp.blogspot.com/-W9m0DKUHn5k/UWUNH81LLxI/AAAAAAAACiY/DdBIRpTTOYw/s400/%D0%9F%D0%BA.jpg#clickable)](http://1.bp.blogspot.com/-W9m0DKUHn5k/UWUNH81LLxI/AAAAAAAACiY/DdBIRpTTOYw/s400/%D0%9F%D0%BA.jpg)


**Russian:**

Исаакиевский собор

**English:**

St. Isaac's Cathedral


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/31.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/31.jpg)
-->

[![](http://3.bp.blogspot.com/-3pMXHsFww7o/UWKBBGGUDHI/AAAAAAAACcc/FzHTrujhIzY/s1600/31.jpg#clickable)](http://3.bp.blogspot.com/-3pMXHsFww7o/UWKBBGGUDHI/AAAAAAAACcc/FzHTrujhIzY/s1600/31.jpg)


**Russian:**

Храм Христа Спасителя

**English:**

Temple of Christ the Savior


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/41.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/41.jpg)
-->

[![](http://4.bp.blogspot.com/-1vO3klyqAXQ/UWKCrRpgb9I/AAAAAAAACc0/RyvkhcH63KU/s1600/41.jpg#clickable)](http://4.bp.blogspot.com/-1vO3klyqAXQ/UWKCrRpgb9I/AAAAAAAACc0/RyvkhcH63KU/s1600/41.jpg)


**Russian:**

Тадж Махал

**English:**

Taj Mahal


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/32.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/32.jpg)
-->

[![](http://2.bp.blogspot.com/-ZtlrBcR-OxQ/UWKB62kfrfI/AAAAAAAACco/XSwmuDREJ-8/s1600/32.jpg#clickable)](http://2.bp.blogspot.com/-ZtlrBcR-OxQ/UWKB62kfrfI/AAAAAAAACco/XSwmuDREJ-8/s1600/32.jpg)


**Russian:**

Шатл Атлантис

**English:**

Shuttle Atlantis


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/21.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/21.jpg)
-->

[![](http://4.bp.blogspot.com/-nubP3njYYYM/UWJ-6tC7N_I/AAAAAAAACcM/0S5wENmq1Kc/s1600/21.jpg#clickable)](http://4.bp.blogspot.com/-nubP3njYYYM/UWJ-6tC7N_I/AAAAAAAACcM/0S5wENmq1Kc/s1600/21.jpg)


**Russian:**

Сравните контуры, есть совпадения? Или так...

**English:**

Compare the contours, any matches? Or so...


**Russian:**

Современный ракетоноситель Ares IV

**English:**

Modern rocket launcher Ares IV


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_006.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_006.jpg)
-->

[![](http://1.bp.blogspot.com/-wiTuxm6FCBg/UV63NhSsKhI/AAAAAAAACbk/-kf67AoI49o/s1600/%D0%A1%D0%A1.jpg#clickable)](http://1.bp.blogspot.com/-wiTuxm6FCBg/UV63NhSsKhI/AAAAAAAACbk/-kf67AoI49o/s1600/%D0%A1%D0%A1.jpg)


**Russian:**

Чуть подожмём картинку, посмотрите...

**English:**

Let's take a little look at the picture...


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_002.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a_002.jpg)
-->

[![](http://2.bp.blogspot.com/-WX_OAEV6mSE/UV6763VxAeI/AAAAAAAACb0/gJSl4O3P6Co/s1600/%D1%81%D1%81%D1%81.jpg#clickable)](http://2.bp.blogspot.com/-WX_OAEV6mSE/UV6763VxAeI/AAAAAAAACb0/gJSl4O3P6Co/s1600/%D1%81%D1%81%D1%81.jpg)


**Russian:**

А теперь на что похоже?

**English:**

Now what does it look like?


**Russian:**

Хочу сказать ещё одно, соблюдены все технологические детали, вот ракета "Союз" на старте

**English:**

I just want to say one more thing, all the technological details are in place, here's the Soyuz rocket on launch.


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_43ec4_ad724119_XL.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_43ec4_ad724119_XL.jpg)
-->

[![](http://2.bp.blogspot.com/-EmIZVr8_zr8/UWKDoPYgcFI/AAAAAAAACdA/zANgafJadd0/s1600/0_43ec4_ad724119_XL.jpg#clickable)](http://2.bp.blogspot.com/-EmIZVr8_zr8/UWKDoPYgcFI/AAAAAAAACdA/zANgafJadd0/s1600/0_43ec4_ad724119_XL.jpg)


**Russian:**

Сравним детали

**English:**

Compare details.


<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/51.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/51.jpg)
-->

[![](http://2.bp.blogspot.com/-jaMJfmhaAiE/UWKFkONBrhI/AAAAAAAACdQ/WTqw5u_Ns20/s1600/51.jpg#clickable)](http://2.bp.blogspot.com/-jaMJfmhaAiE/UWKFkONBrhI/AAAAAAAACdQ/WTqw5u_Ns20/s1600/51.jpg)

**Russian:**

1. Стабилизатор вертикальной тяги.

2. Отсек для космонавтов.

3. Разделитель между маршевым двигателем (вверху) и топливными баками (внизу).

4. Силовой блок.

**English:**

1. Stabilizer for vertical traction.

2. The astronaut compartment.

3. Separator between the marshal engine (top) and fuel tanks (bottom).

4. The power unit.

**Russian:**

... по бокам разгонные блоки, их четыре, точно так-же, как и куполов.

**English:**

... on the sides of the acceleration blocks, four of them, just like the domes.

**Russian:**

Старт ракетоносителя "Энергия" с "Бураном" на борту.

**English:**

The launch of the rocket carrier Energia with Buran on board.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1266241110_energia_buran10.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1266241110_energia_buran10.jpg)
-->

[![](http://2.bp.blogspot.com/-mSOPFhCF5h0/UWKIX6NdfLI/AAAAAAAACdg/sGM6ygVTeBY/s1600/1266241110_energia_buran10.jpg#clickable)](http://2.bp.blogspot.com/-mSOPFhCF5h0/UWKIX6NdfLI/AAAAAAAACdg/sGM6ygVTeBY/s1600/1266241110_energia_buran10.jpg)

**Russian:**

Вот вся линейка ракетостроения СССР

**English:**

Here's the whole line of rocket science in the USSR.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/47ea8f4937701.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/47ea8f4937701.jpg)
-->

[![](http://3.bp.blogspot.com/-l7f2omj247Q/UWUTc9LnQcI/AAAAAAAACjY/GdOw52_ZqD0/s1600/47ea8f493770+(1).jpg#clickable)](http://3.bp.blogspot.com/-l7f2omj247Q/UWUTc9LnQcI/AAAAAAAACjY/GdOw52_ZqD0/s1600/47ea8f493770+(1).jpg)

**Russian:**

Кстати эта картинка ещё один плюс в пользу того, [что американцы никогда не летали на Луну](http://atlantida-pravda-i-vimisel.       blogspot.com/2011/04/blog-post_2992.html) у нас и техника помощнее, и ракет побольше, а что могут предложить нам янки?

**English:**

By the way, this picture is another clue [supporting the fact that Americans have never flown to the moon.](http://atlantida-pravda-i-vimisel.blogspot.com/2011/04/blog-post_2992.html). We've got better equipment and bigger missiles, what can the Yankees have to offer us?

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/000_main_comparison_full.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/000_main_comparison_full.jpg)
-->

[![](http://2.bp.blogspot.com/-8YVYNAg8Nc4/UWURIYFe2_I/AAAAAAAACjA/s6pWYk6h8fc/s1600/000_main_comparison_full.box.jpg#clickable)](http://2.bp.blogspot.com/-8YVYNAg8Nc4/UWURIYFe2_I/AAAAAAAACjA/s6pWYk6h8fc/s1600/000_main_comparison_full.box.jpg)

**Russian:**

Вот ещё парочка уж очень "откровенных " культовых построек.

**English:**

Here's a couple more very "straightforward" cult buildings.

**Russian:**

Грундтвингская церковь в Копенгагене, Дания.

**English:**

Grundtwing Church in Copenhagen, Denmark.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_d1909ac8.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_d1909ac8.jpg)
-->

[![](http://1.bp.blogspot.com/-zc4swkEj5mQ/UWvdmD4wsPI/AAAAAAAACps/GZ47SffKJKM/s1600/y_d1909ac8.jpg#clickable)](http://1.bp.blogspot.com/-zc4swkEj5mQ/UWvdmD4wsPI/AAAAAAAACps/GZ47SffKJKM/s1600/y_d1909ac8.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_bfc7cb83.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_bfc7cb83.jpg)
-->

[![](http://2.bp.blogspot.com/-WTekNgvo4-g/UWvdtGJXh-I/AAAAAAAACp0/zpxC5hLocDw/s1600/x_bfc7cb83.jpg#clickable)](http://2.bp.blogspot.com/-WTekNgvo4-g/UWvdtGJXh-I/AAAAAAAACp0/zpxC5hLocDw/s1600/x_bfc7cb83.jpg)

**Russian:**

Церковь Хатльгримскиркья в Рейкьявике, Исландия.

**English:**

Hatlgrimskyrkja Church in Reykjavik, Iceland.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_35194bd1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_35194bd1.jpg)
-->

[![](http://4.bp.blogspot.com/-qujrHDAXBu0/UWvd3w6ZmoI/AAAAAAAACp8/2Q_J0UQsfb8/s1600/y_35194bd1.jpg#clickable)](http://4.bp.blogspot.com/-qujrHDAXBu0/UWvd3w6ZmoI/AAAAAAAACp8/2Q_J0UQsfb8/s1600/y_35194bd1.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_b976f84c.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_b976f84c.jpg)
-->

[![](http://2.bp.blogspot.com/-5dtkMRj3KiU/UWvfuHulaOI/AAAAAAAACqg/MX0DLS9qFtY/s640/y_b976f84c.jpg#clickable)](http://2.bp.blogspot.com/-5dtkMRj3KiU/UWvfuHulaOI/AAAAAAAACqg/MX0DLS9qFtY/s1600/y_b976f84c.jpg)

<!--
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_5bfab610.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_5bfab610.jpg)
-->

[![](http://3.bp.blogspot.com/-Yms2QALnEWE/UWvfucqDLiI/AAAAAAAACqk/9zM1EFpok34/s640/x_5bfab610.jpg#clickable)](http://3.bp.blogspot.com/-Yms2QALnEWE/UWvfucqDLiI/AAAAAAAACqk/9zM1EFpok34/s1600/x_5bfab610.jpg)

**Russian:**

Искупительный храм Святого Семейства в Барселоне, Испания.

**English:**

The Redeeming Sacred Family Church in Barcelona, Spain.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_c6e80656.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_c6e80656.jpg)
-->

[![](http://1.bp.blogspot.com/-q-8-ECUf2wk/UWvd4K5rYCI/AAAAAAAACqA/Hs9ul5-C2tM/s1600/y_c6e80656.jpg) ](http://1.bp.blogspot.com/-q-8-ECUf2wk/UWvd4K5rYCI/AAAAAAAACqA/Hs9ul5-C2tM/s1600/y_c6e80656.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_00df350c.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/y_00df350c.jpg)
-->

[![](http://2.bp.blogspot.com/-1uKWIo3G-Ww/UWvd8MltEMI/AAAAAAAACqM/H06YH0MIFfI/s1600/y_00df350c.jpg#clickable)](http://2.bp.blogspot.com/-1uKWIo3G-Ww/UWvd8MltEMI/AAAAAAAACqM/H06YH0MIFfI/s1600/y_00df350c.jpg)

**Russian:**

Капелла Святого Генриха на острове Хирвенсало, Финляндия.

**English:**

St. Henry's Chapel on Hirvensalo Island, Finland.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_7113627a.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/x_7113627a.jpg)
-->

[![](http://4.bp.blogspot.com/-eI5ca6AYzBk/UWvgiLno5nI/AAAAAAAACq4/cFeWEnE5Zec/s1600/x_7113627a.jpg#clickable)](http://4.bp.blogspot.com/-eI5ca6AYzBk/UWvgiLno5nI/AAAAAAAACq4/cFeWEnE5Zec/s1600/x_7113627a.jpg)

**Russian:**

Церковь Ла Перегрина, построенная в 18 веке и посвящена Деве Перегрине, святой покровительнице провинции Понтеведра и паломнического пути в Сантьяго.

**English:**

The Church of La Peregrina, built in the 18th century, is dedicated to the Virgin Peregrina, the patron saint of the province of Pontevedra and the pilgrimage route to Santiago.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/D186D0B5D180D0BAD0BED0B2D18CD09BD0B0D09FD0B5D180D0B5D0B3D180.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/D186D0B5D180D0BAD0BED0B2D18CD09BD0B0D09FD0B5D180D0B5D0B3D180.jpg)
-->

[![](http://1.bp.blogspot.com/-uRvoKj8YIkY/UoNLsvJ8U-I/AAAAAAAAFSc/ZCqm_fqlEco/s1600/%25D1%2586%25D0%25B5%25D1%2580%25D0%25BA%25D0%25BE%25D0%25B2%25D1%258C+%25D0%259B%25D0%25B0+%25D0%259F%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B3%25D1%2580%25D0%25B8%25D0%25BD%25D0%25B0.jpg#clickable)](http://1.bp.blogspot.com/-uRvoKj8YIkY/UoNLsvJ8U-I/AAAAAAAAFSc/ZCqm_fqlEco/s1600/%25D1%2586%25D0%25B5%25D1%2580%25D0%25BA%25D0%25BE%25D0%25B2%25D1%258C+%25D0%259B%25D0%25B0+%25D0%259F%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B3%25D1%2580%25D0%25B8%25D0%25BD%25D0%25B0.jpg)

**Russian:**

Вормский собор. Германия

**English:**

The cathedral of Worms. Germany

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/5vormskisobor2.gif#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/5vormskisobor2.gif)
-->

[![](http://4.bp.blogspot.com/-QQ2Ln3CLt74/UWzednjvjDI/AAAAAAAACrg/8SKQPHgR0gE/s1600/5+vormski+sobor+2.gif#clickable)](http://4.bp.blogspot.com/-QQ2Ln3CLt74/UWzednjvjDI/AAAAAAAACrg/8SKQPHgR0gE/s1600/5+vormski+sobor+2.gif)

**Russian:**

Для сравнения

**English:**

For comparison.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/561.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/561.jpg)
-->

[![](http://4.bp.blogspot.com/-WwymS9Nj_Uw/UWzhEHk0ADI/AAAAAAAACrs/zaEn6H5ALCU/s1600/561.jpg#clickable)](http://4.bp.blogspot.com/-WwymS9Nj_Uw/UWzhEHk0ADI/AAAAAAAACrs/zaEn6H5ALCU/s1600/561.jpg)

**Russian:**

Ничего не напоминает?

**English:**

Does that ring a bell?

**Russian:**

Возвращаясь к "нашим баранам"...

**English:**

Going back to "our rams"...

**Russian:**

В этом плане интересны и старые крепости

**English:**

The old fortresses are also interesting in this respect.

Bodiam Castle, East Sussex, United Kingdom

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1245505983_70.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1245505983_70.jpg)
-->

[![](http://4.bp.blogspot.com/-bJkCXpfu5xg/UWKOzqyGXQI/AAAAAAAACdw/DOyvuOoGMMQ/s1600/1245505983_70.jpg#clickable)](http://4.bp.blogspot.com/-bJkCXpfu5xg/UWKOzqyGXQI/AAAAAAAACdw/DOyvuOoGMMQ/s1600/1245505983_70.jpg)

Bodiam Castle, East Sussex, United Kingdom

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/05902e290d43.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/05902e290d43.jpg)
-->

[![](http://2.bp.blogspot.com/-5-JhEFCuYwQ/UWKOzte4seI/AAAAAAAACd0/xuI4J4XWgk8/s1600/05902e290d43.jpg#clickable)](http://2.bp.blogspot.com/-5-JhEFCuYwQ/UWKOzte4seI/AAAAAAAACd0/xuI4J4XWgk8/s1600/05902e290d43.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/i.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/i.jpg)
-->

[![](http://1.bp.blogspot.com/-e_AkWpIyVCg/UWKO0jJSZjI/AAAAAAAACeA/GVlzQnow5Cg/s1600/i.jpg#clickable)](http://1.bp.blogspot.com/-e_AkWpIyVCg/UWKO0jJSZjI/AAAAAAAACeA/GVlzQnow5Cg/s1600/i.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1294861884_4843e7b211a1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1294861884_4843e7b211a1.jpg)
-->

[![](http://3.bp.blogspot.com/-cn462z2F0-c/UWKO0vn_tFI/AAAAAAAACeE/iGfD7Uofqqc/s1600/1294861884_4843e7b211a1.jpg#clickable)](http://3.bp.blogspot.com/-cn462z2F0-c/UWKO0vn_tFI/AAAAAAAACeE/iGfD7Uofqqc/s1600/1294861884_4843e7b211a1.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1348006524-1565031-0219728_www.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1348006524-1565031-0219728_www.jpg)
-->

[![](http://4.bp.blogspot.com/-4i7WcteFae4/UWKPAE_ZMxI/AAAAAAAACeQ/-ZTIbuI3894/s1600/1348006524-1565031-0219728_www.nevseoboi.com.ua.jpg#clickable)](http://4.bp.blogspot.com/-4i7WcteFae4/UWKPAE_ZMxI/AAAAAAAACeQ/-ZTIbuI3894/s1600/1348006524-1565031-0219728_www.nevseoboi.com.ua.jpg)

**Russian:**

Тут всё понятно, башни функциональны, на них легко разместить лучников, пушки, и т.д.

**English:**

Everything is clear here, the towers are functional, it's easy to place archers, cannons, etc. on them.

**Russian:**

А вот в какой момент начали строить такие башни, и зачем? Они малофунциональны при осаде, как смотровые - тоже не очень.

**English:**

But at what point did they start building these kinds of towers, and why? They are not very functional in a siege, and are more like observation towers.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/d3c14f.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/d3c14f.jpg)
-->

[![](http://1.bp.blogspot.com/-fYg5KQ9zg5Q/UWKPrGzH9XI/AAAAAAAACeY/06PJiK1z3Ag/s1600/d3c14f.jpg#clickable)](http://1.bp.blogspot.com/-fYg5KQ9zg5Q/UWKPrGzH9XI/AAAAAAAACeY/06PJiK1z3Ag/s1600/d3c14f.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/image011.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/image011.jpg)
-->

[![](http://3.bp.blogspot.com/-c1XQwhuDIVc/UWKPrQxqEsI/AAAAAAAACeg/u-kErUOhxZ0/s1600/image011.jpg#clickable)](http://3.bp.blogspot.com/-c1XQwhuDIVc/UWKPrQxqEsI/AAAAAAAACeg/u-kErUOhxZ0/s1600/image011.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_b853_41c8aaeb_L.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_b853_41c8aaeb_L.jpg)
-->

[![](http://4.bp.blogspot.com/-tUoL6H3ZjeI/UWKPrkYFntI/AAAAAAAACek/yjnjJNacIr4/s1600/0_b853_41c8aaeb_L.jpg#clickable)](http://4.bp.blogspot.com/-tUoL6H3ZjeI/UWKPrkYFntI/AAAAAAAACek/yjnjJNacIr4/s1600/0_b853_41c8aaeb_L.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/49891435_Kelnskogo_sobor.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/49891435_Kelnskogo_sobor.jpg)
-->

[![](http://4.bp.blogspot.com/-uph83y-pXM8/UWT6NtrdcGI/AAAAAAAACiI/wb7yLDgd-nE/s1600/49891435_Kelnskogo_sobor.jpg#clickable)](http://4.bp.blogspot.com/-uph83y-pXM8/UWT6NtrdcGI/AAAAAAAACiI/wb7yLDgd-nE/s1600/49891435_Kelnskogo_sobor.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/yar_1b.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/yar_1b.jpg)
-->

[![](http://2.bp.blogspot.com/-r-HmpJOZ_08/UWT6BDDpwGI/AAAAAAAACiA/twehi-FK53w/s1600/yar_1b.jpg#clickable)](http://2.bp.blogspot.com/-r-HmpJOZ_08/UWT6BDDpwGI/AAAAAAAACiA/twehi-FK53w/s1600/yar_1b.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/450.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/450.jpg)
-->

[![](http://3.bp.blogspot.com/-7RD96ud5q3g/UWKY8IiTnNI/AAAAAAAACfo/chN_SXoc0-4/s1600/450.jpg#clickable)](http://3.bp.blogspot.com/-7RD96ud5q3g/UWKY8IiTnNI/AAAAAAAACfo/chN_SXoc0-4/s1600/450.jpg)

**Russian:**

Может с целью подражания чему-то, для устрашения ....

**English:**

Maybe as a role model for something to scare...

**Russian:**

Сравним их вот с этим

**English:**

Let's compare them to this.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/33.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/33.jpg)
-->

[![](http://2.bp.blogspot.com/-phTO2jj3R9A/UWO9pQT5_dI/AAAAAAAAChw/cv5P_e3u0Wg/s1600/33.jpg#clickable)](http://2.bp.blogspot.com/-phTO2jj3R9A/UWO9pQT5_dI/AAAAAAAAChw/cv5P_e3u0Wg/s1600/33.jpg)

**Russian:**

Мой личный опыт подсказывает, что человек не может придумать того, чего не существует, не существовало, или не будет существовать... даже самые смелые фантастические предположения базируются на чём-то: жизненном опыте, генетической памяти, снах, понимании природы процессов, даже на интуиции. Нельзя просто так взять и выдумать что-то неприемлемое для воображения, только потому, что воображение уже само по себе рисует приемлемые картины.

**English:**

My personal experience tells me that a person cannot come up with something that does not exist, did not exist, or will not exist... even the boldest fantastic assumptions are based on something: life experience, genetic memory, dreams, understanding the nature of processes, even intuition. You can't just make up something that is unacceptable to the imagination, just because the imagination itself draws acceptable pictures.

**Russian:**

Точно так-же и с сооружениями... Мы всегда опираемся на опыт, свой-чужой, какая разница, и хоть он, опыт - "сын ошибок трудных", мы всегда, почти всегда, можем логично объяснить, то или иное устройство, приспособление, конструкцию или механизм.  Даже в самых смелых фантастических рассказах нет ничего не реального, они приемлемы и понимаемы, тут всё логично.

**English:**

It's the same with the structures... We always rely on experience, someone else's experience, what difference does it make, and though he or she is the "son of difficult errors", we can always, almost always, logically explain this or that device, device, construction or mechanism.  Even the most daring fantastic stories have nothing real, they are acceptable and understandable, everything is logical here.

**Russian:**

Так почему так нелогична история, в чём подвох?

**English:**

So why is the story so illogical, what's the catch?

**Russian:**

Н-1 на старте

**English:**

N1 on start

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/n1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/n1.jpg)
-->

[![](http://2.bp.blogspot.com/-7te6tqWgLkA/UWKQl8Wa9SI/AAAAAAAACew/TLDkbn23t1M/s1600/n1.jpg#clickable)](http://2.bp.blogspot.com/-7te6tqWgLkA/UWKQl8Wa9SI/AAAAAAAACew/TLDkbn23t1M/s1600/n1.jpg)

**Russian:**

Н-1 в МИКе

**English:**

N1 at MIK

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/pic_30.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/pic_30.jpg)
-->

[![](http:// 3.bp.blogspot.com/-A97r12wqGj0/UWKQmMG9nFI/AAAAAAAACe0/vTDCBEbOaLw/s1600/pic_30.jpg#clickable)](http://3.bp.blogspot.com/-A97r12wqGj0/UWKQmMG9nFI/AAAAAAAACe0/vTDCBEbOaLw/s1600/pic_30.jpg)

**Russian:**

Американский "Сатурн" и Н-1 (СССР)

**English:**

American Saturn and N1 (USSR)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/sat5-n1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/sat5-n1.jpg)
-->

[![](http://3.bp.blogspot.com/-QCGvLcte9kY/UWKQu-yd4PI/AAAAAAAACfI/EP1xb3-9zmY/s1600/sat5-n1.jpg#clickable)](http://3.bp.blogspot.com/-QCGvLcte9kY/UWKQu-yd4PI/AAAAAAAACfI/EP1xb3-9zmY/s1600/sat5-n1.jpg)

**Russian:**

Есть совпадения  с архитектурными сооружениями по внешнему виду?

**English:**

Any matches with the architectural structures in terms of appearance?

**Russian:**

Можно по новому взглянуть и на здания церквей, присмотритесь

**English:**

You can take a fresh look at the church buildings. Have a closer look.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/post65569_img1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/post65569_img1.jpg)
-->

[![](http://2.bp.blogspot.com/-tWTEW0YiPf8/UWOzvdOzUFI/AAAAAAAACgk/3_nbjH7J82k/s1600/post65569_img1.jpg#clickable)](http://2.bp.blogspot.com/-tWTEW0YiPf8/UWOzvdOzUFI/AAAAAAAACgk/3_nbjH7J82k/s1600/post65569_img1.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_33d79_354cacbb_XL.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_33d79_354cacbb_XL.jpg)
-->

[![](http://4.bp.blogspot.com/-UlLJvG3IJ80/UWOzvbw81qI/AAAAAAAACgo/_c2oZjmd0LY/s1600/0_33d79_354cacbb_XL.jpg#clickable)](http://4.bp.blogspot.com/-UlLJvG3IJ80/UWOzvbw81qI/AAAAAAAACgo/_c2oZjmd0LY/s1600/0_33d79_354cacbb_XL.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/troica-v-nikitnikah.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/troica-v-nikitnikah.jpg)
-->

[![](http://1.bp.blogspot.com/-nrNOHiLOC74/UWOzvgruswI/AAAAAAAACgw/wsqfQ_JwKMs/s1600/troica-v-nikitnikah.jpg#clickable)](http://1.bp.blogspot.com/-nrNOHiLOC74/UWOzvgruswI/AAAAAAAACgw/wsqfQ_JwKMs/s1600/troica-v-nikitnikah.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/70e4223e24d45cdf88872bae10e33dcc_1024_0.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/70e4223e24d45cdf88872bae10e33dcc_1024_0.jpg)
-->

[![](http://4.bp.blogspot.com/-0FXNHtvGr0g/UWO1r3yi9gI/AAAAAAAAChI/dGhLgHGaQaI/s1600/70e4223e24d45cdf88872bae10e33dcc_1024_0.jpg#clickable)](http://4.bp.blogspot.com/-0FXNHtvGr0g/UWO1r3yi9gI/AAAAAAAAChI/dGhLgHGaQaI/s1600/70e4223e24d45cdf88872bae10e33dcc_1024_0.jpg)

**Russian:**

Или вот так

**English:**

Or like this.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/2.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/2.jpg)
-->

[![](http://4.bp.blogspot.com/-SIo2RsAHTII/UWO4QljJGfI/AAAAAAAAChY/6-hS0BTf2mw/s1600/2.jpg#clickable)](http://4.bp.blogspot.com/-SIo2RsAHTII/UWO4QljJGfI/AAAAAAAAChY/6-hS0BTf2mw/s1600/2.jpg)

**Russian:**

**Рассмотрим мечети подробнее, ночное освещение тут играет выгодную роль**

**English:**

Let's take a closer look at the mosques, the night-time lighting plays an advantageous role here

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_1d359_4f475f38_XL.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_1d359_4f475f38_XL.jpg)
-->

[![](http://1.bp.blogspot.com/-6n0WL4TG-HM/VESsXgXsX7I/AAAAAAAAHjI/34Nhs8VxRew/s1600/0_1d359_4f475f38_XL.jpg#clickable)](http://1.bp.blogspot.com/-6n0WL4TG-HM/VESsXgXsX7I/AAAAAAAAHjI/34Nhs8VxRew/s1600/0_1d359_4f475f38_XL.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/21901ad272248d74a1fb19711fcd5ffe.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/21901ad272248d74a1fb19711fcd5ffe.jpg)
-->

[![](http://3.bp.blogspot.com/-ISsqd8ZknwU/VESsXaXuWrI/AAAAAAAAHjo/N6UCK6LoWvM/s1600/21901ad272248d74a1fb19711fcd5ffe.jpg#clickable)](http://3.bp.blogspot.com/-ISsqd8ZknwU/VESsXaXuWrI/AAAAAAAAHjo/N6UCK6LoWvM/s1600/21901ad272248d74a1fb19711fcd5ffe.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/636123403_7.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/636123403_7.jpg)
-->

[![](http://4.bp.blogspot.com/-Aum3SEeidx8/VESsXYa-JHI/AAAAAAAAHjE/NcGy-s_sD38/s1600/636123403_7.jpg#clickable)](http://4.bp.blogspot.com/-Aum3SEeidx8/VESsXYa-JHI/AAAAAAAAHjE/NcGy-s_sD38/s1600/636123403_7.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/chechnya-grozny-photo-2010-foto-chechni-24.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/chechnya-grozny-photo-2010-foto-chechni-24.jpg)
-->

[![](http://2.bp.blogspot.com/-44Jiey3Jh9A/VESsYrm8_YI/AAAAAAAAHjU/MFsmRzQwJYw/s1600/chechnya-grozny-photo-2010-foto-chechni-24.jpg#clickable)](http://2.bp.blogspot.com/-44Jiey3Jh9A/VESsYrm8_YI/AAAAAAAAHjU/MFsmRzQwJYw/s1600/chechnya-grozny-photo-2010-foto-chechni-24.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/mosque0A.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/mosque0A.jpg)
-->

[![](http://2.bp.blogspot.com/-AspI6k8xS1M/VESsZlfo3_I/AAAAAAAAHjk/Ja381xndtCg/s1600/mosque0A.jpg#clickable)](http://2.bp.blogspot.com/-AspI6k8xS1M/VESsZlfo3_I/AAAAAAAAHjk/Ja381xndtCg/s1600/mosque0A.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/muslim-u-ka-8.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/muslim-u-ka-8.jpg)
-->

[![](http://1.bp.blogspot.com/-cyV-9I-iSDs/VESsa3cU0xI/AAAAAAAAHj0/zoKkkEwDsqY/s1600/muslim-u-ka-8.jpg)](http://1.bp.blogspot.com/-cyV-9I-iSDs/VESsa3cU0xI/AAAAAAAAHj0/zoKkkEwDsqY/s1600/muslim-u-ka-8.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1274624_740x550.JPG#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1274624_740x550.JPG)
-->

[![](http://3.bp.blogspot.com/-usoGUl6UbBM/VEYHWYxeQII/AAAAAAAAHkw/z_1LZtFMp2Q/s1600/1274624_740x550.JPG#clickable)](http://3.bp.blogspot.com/-usoGUl6UbBM/VEYHWYxeQII/AAAAAAAAHkw/z_1LZtFMp2Q/s1600/1274624_740x550.JPG)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1096027_800x600.JPG#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1096027_800x600.JPG)
-->

[![](http://3.bp.blogspot.com/-tvqZllfDlYU/VEYHWKfWoQI/AAAAAAAAHks/44t2aF6ehVI/s1600/1096027_800x600.JPG#clickable)](http://3.bp.blogspot.com/-tvqZllfDlYU/VEYHWKfWoQI/AAAAAAAAHks/44t2aF6ehVI/s1600/1096027_800x600.JPG)

**Russian:**

Сравним вот с этим

**English:**

Let's compare it to this.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/66.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/66.jpg)
-->

[![](http://1.bp.blogspot.com/-AIz97cJVuI4/VESsuR_1UAI/AAAAAAAAHkE/8VZqDjO5dTU/s1600/66.jpg#clickable)](http://1.bp.blogspot.com/-AIz97cJVuI4/VESsuR_1UAI/AAAAAAAAHkE/8VZqDjO5dTU/s1600/66.jpg)

**Russian:**

Байконур. Ночной старт. 

**English:**

Baikonur. Night start.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Bankoboev.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Bankoboev.jpg)
-->

[![](http://3.bp.blogspot.com/-u8r46jR0luE/VESsw96sJqI/AAAAAAAAHkc/DXjqT4KyGMk/s1600/Bankoboev.Ru_start_rakety_v_nochnuyu_poru.jpg#clickable)](http://3.bp.blogspot.com/-u8r46jR0luE/VESsw96sJqI/AAAAAAAAHkc/DXjqT4KyGMk/s1600/Bankoboev.Ru_start_rakety_v_nochnuyu_poru.jpg)

**Russian:**

Выводы оставляю за вами.

**English:**

I leave the conclusions to you.

**Russian:**

**Сюда легко вписываются и азиатские пагоды.**

**English:**

Asian pagodas fit in easily here too.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_6b977_fa3814b7_XL.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/0_6b977_fa3814b7_XL.jpg)
-->

[![](http://4.bp.blogspot.com/-dqrcP0XlAeY/UW5cy1kTnTI/AAAAAAAACsQ/N0zmpajBMGQ/s1600/0_6b977_fa3814b7_XL.jpg#clickable)](http://4.bp.blogspot.com/-dqrcP0XlAeY/UW5cy1kTnTI/AAAAAAAACsQ/N0zmpajBMGQ/s1600/0_6b977_fa3814b7_XL.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1906-1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/1906-1.jpg)
-->

[![](http://2.bp.blogspot.com/-WEyYG2-tYV0/UW5cvflFf4I/AAAAAAAACsA/Fu73XFJF9-A/s1600/1906-1.jpg#clickable)](http://2.bp.blogspot.com/-WEyYG2-tYV0/UW5cvflFf4I/AAAAAAAACsA/Fu73XFJF9-A/s1600/1906-1.jpg)

**Russian:**

Или так

**English:**

Or like so.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/GPN-2002-000184.png#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/GPN-2002-000184.png)
-->

[![](http://3.bp.blogspot.com/-v1acCVYVa0M/UWURvGFPfII/AAAAAAAACjM/wWwAQSoVABg/s1600/GPN-2002-000184.png#clickable)](http://3.bp.blogspot.com/-v1acCVYVa0M/UWURvGFPfII/AAAAAAAACjM/wWwAQSoVABg/s1600/GPN-2002-000184.png) 

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/161.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/161.jpg)
-->

[![](http://3.bp.blogspot.com/-OycAKUOqMUY/UW5cw5tRgPI/AAAAAAAACsI/GNmeb22mU30/s1600/161.jpg#clickable)](http://3.bp.blogspot.com/-OycAKUOqMUY/UW5cw5tRgPI/AAAAAAAACsI/GNmeb22mU30/s1600/161.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4014048_703e425b45ab1.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4014048_703e425b45ab1.jpg)
-->

[![](http://3.bp.blogspot.com/-wD8IM0-qA-c/UW5c0QahkQI/AAAAAAAACsg/EVhzz5eY22g/s1600/4014048_703e425b45ab1.jpg#clickable)](http://3.bp.blogspot.com/-wD8IM0-qA-c/UW5c0QahkQI/AAAAAAAACsg/EVhzz5eY22g/s1600/4014048_703e425b45ab1.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4148315n1c.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4148315n1c.jpg)
-->

[![](http://3.bp.blogspot.com/-Cq0uc8z1NeA/UW5czPm0ltI/AAAAAAAACsU/W-jgVLDFqX0/s1600/4148315n1c.jpg#clickable)](http://3.bp.blogspot.com/-Cq0uc8z1NeA/UW5czPm0ltI/AAAAAAAACsU/W-jgVLDFqX0/s1600/4148315n1c.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Kew_Gardens_Pagoda.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Kew_Gardens_Pagoda.jpg)
-->

[![](http://2.bp.blogspot.com/-9AxbagMSVo4/UW5dGYOf6HI/AAAAAAAACtI/evPS2ZpZm4I/s1600/Kew_Gardens_Pagoda.jpg#clickable)](http://2.bp.blogspot.com/-9AxbagMSVo4/UW5dGYOf6HI/AAAAAAAACtI/evPS2ZpZm4I/s1600/Kew_Gardens_Pagoda.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Nankin.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Nankin.jpg)
-->

[![](http://1.bp.blogspot.com/-oRcZldpZ6e0/UW5c37UYvKI/AAAAAAAACso/Gmg6xKISUm0/s1600/Nankin.jpg#clickable)](http://1.bp.blogspot.com/-oRcZldpZ6e0/UW5c37UYvKI/AAAAAAAACso/Gmg6xKISUm0/s1600/Nankin.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Shwedagonpagodacomplex.JPG#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Shwedagonpagodacomplex.JPG)
-->

[![](http://2.bp.blogspot.com/-DrGRstMrLOQ/UW5c3120seI/AAAAAAAACss/wzx4dooJOS8/s1600/Shwedagon+pagoda+complex.2.JPG#clickable)](http://2.bp.blogspot.com/-DrGRstMrLOQ/UW5c3120seI/AAAAAAAACss/wzx4dooJOS8/s1600/Shwedagon+pagoda+complex.2.JPG)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a30a2f0d573eb5ce78db218d9ad14ba7.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a30a2f0d573eb5ce78db218d9ad14ba7.jpg)
-->

[![](http://1.bp.blogspot.com/-8OCrApy0LCQ/UW5c6IoV0UI/AAAAAAAACtA/Pf-M07-p0b4/s1600/a30a2f0d573eb5ce78db218d9ad14ba7.jpg#clickable)](http://1.bp.blogspot.com/-8OCrApy0LCQ/UW5c6IoV0UI/AAAAAAAACtA/Pf-M07-p0b4/s1600/a30a2f0d573eb5ce78db218d9ad14ba7.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/suzhou-ruiguang-ta-pagoda-0073.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/suzhou-ruiguang-ta-pagoda-0073.jpg)
-->

[![](http://3.bp.blogspot.com/-WQy35u4cm3A/UW5c501QHrI/AAAAAAAACs4/M-ngCi0aV44/s1600/suzhou-ruiguang-ta-pagoda-0073.jpg#clickable)](http://3.bp.blogspot.com/-WQy35u4cm3A/UW5c501QHrI/AAAAAAAACs4/M-ngCi0aV44/s1600/suzhou-ruiguang-ta-pagoda-0073.jpg)

**Russian:**

Внешние соответствия мы рассмотрели, теперь можно заглянуть внутрь. Присмотритесь, вам это ничего не напоминает.

**English:**

We've looked at the external matches, now we can look inside. Look, it doesn't look like anything to you.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/endeavour-flight-deck-168.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/endeavour-flight-deck-168.jpg)
-->

[![](http://4.bp.blogspot.com/-pHXDqymrDks/UmENadupDGI/AAAAAAAAE7g/Grtsl2gEGmA/s1600/endeavour-flight-deck-168.jpg#clickable)](http://4.bp.blogspot.com/-pHXDqymrDks/UmENadupDGI/AAAAAAAAE7g/Grtsl2gEGmA/s1600/endeavour-flight-deck-168.jpg)

**Russian:**

Рубка управления Шаттлом 

**English:**

Shuttle Control Room

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/31670272.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/31670272.jpg)
-->

[![](http://1.bp.blogspot.com/-CQWgMoqrhQM/UmENsWNg5XI/AAAAAAAAE7o/xjDGobMBIz4/s1600/31670272.jpg#clickable)](http://1.bp.blogspot.com/-CQWgMoqrhQM/UmENsWNg5XI/AAAAAAAAE7o/xjDGobMBIz4/s1600/31670272.jpg)

**Russian:**

Церковный иконостас

**English:**

Church iconostasis

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/nebula10.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/nebula10.jpg)
-->

[![](http://3.bp.blogspot.com/-IXS2SXfJkyA/UmEOLFz7onI/AAAAAAAAE74/d7-ku9AMZ-U/s1600/nebula10.jpg#clickable)](http://3.bp.blogspot.com/-IXS2SXfJkyA/UmEOLFz7onI/AAAAAAAAE74/d7-ku9AMZ-U/s1600/nebula10.jpg)

**Russian:**

Рубка управления космическим кораблём (из фантастических романов)

**English:**

Spaceship Control (from science fiction novels)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/ikonostas_hrama_v_belom_kolodeze_700x504.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/ikonostas_hrama_v_belom_kolodeze_700x504.jpg)
-->

[![](http://2.bp.blogspot.com/-TWyx_oKLUU0/UmEOc1QLxSI/AAAAAAAAE8A/IcZ5e3iKCRk/s1600/ikonostas_hrama_v_belom_kolodeze_700x504.jpg#clickable)](http://2.bp.blogspot.com/-TWyx_oKLUU0/UmEOc1QLxSI/AAAAAAAAE8A/IcZ5e3iKCRk/s1600/ikonostas_hrama_v_belom_kolodeze_700x504.jpg)

**Russian:**

Всё тот же иконостас.

**English:**

It's still the same iconostasis.

**Russian:**

Вы случайно никаких совпадений не находите? Нет? Ну и ладно, проехали.

**English:**

You wouldn't happen to find any coincidences? No? All right, let's forget it.

**Russian:**

Меня давно мучают вопросы: как бы отреагировали наши предки, начинающие познавать что такое бронза, на посадку космического корабля? Что бы они запомнили попав внутрь? За кого бы приняли астронавтов? Как бы сохранили память о них? Какие бы культовые сооружения строили в память о них? Как передавали знания? И кто тихонько подменил одни знания на другие?  

**English:**

I have long been tormented by questions: how would our ancestors, who were just beginning to know about bronze, react to a spaceship landing? What would they have remembered when they got inside? Who would they take astronauts for? How would they remember them? What kind of places of worship would they build in memory of the astronauts? How would they pass on knowledge? And who quietly swapped in different knowledge for some of it?  

**Russian:**

Но это только фантазии автора. Правда ["культ карго"](http://ru.wikipedia.org/wiki/%CA%F3%EB%FC%F2_%EA%E0%F0%E3%EE) не я придумал.

**English:**

But this is only the author's fantasy. But I did not come up with "[cargo cult (Wiki.ru)](http://ru.wikipedia.org/wiki/%CA%F3%EB%FC%F2_%EA%E0%F0%E3%EE)".

**Russian:**

Вот ещё одна тема для размышления

**English:**

Here's another topic to think about.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/magnetosphere_rendition.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/magnetosphere_rendition.jpg)
-->

[![](http://3.bp.blogspot.com/-BIN8vlFFBic/UndfAa7xM0I/AAAAAAAAFJA/F-3zaSqz7Kw/s1600/magnetosphere_rendition.jpg#clickable)](http://3.bp.blogspot.com/-BIN8vlFFBic/UndfAa7xM0I/AAAAAAAAFJA/F-3zaSqz7Kw/s1600/magnetosphere_rendition.jpg)

**Russian:**

[Магнитное поле Земли](http://pro-2012.info/magnetic_field/)

**English:**

[Earth's Magnetic Field](http://pro-2012.info/magnetic_field/)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Angelchr2.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/Angelchr2.jpg)
-->

[![](http://3.bp.blogspot.com/-CkmrjFCvQUY/UndfMeuehFI/AAAAAAAAFJI/CAyn03NZhRM/s1600/Angel+chr2.jpg#clickable)](http://3.bp.blogspot.com/-CkmrjFCvQUY/UndfMeuehFI/AAAAAAAAFJI/CAyn03NZhRM/s1600/Angel+chr2.jpg)

**Russian:**

[Ангел-хранитель](http://www.ipckatakomb.ru/pages/809/)

**English:**

[Guardian Angel](http://www.ipckatakomb.ru/pages/809/)

**Russian:**

Присмотритесь к рисункам, вы не находите, что они похожи, и миссия (функция) у них одна и та же. Но узнать об этом можно только покинув пределы Земли.

**English:**

Look at the drawings, do you not find that they are similar, and their mission (function) is the same. But you only discover this when you leave Earth.

**Russian:**

Вот ещё простые примеры культа карго.

**English:**

Here are some more simple examples of the cargo cult.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a.jpg)
-->

[![](http://3.bp.blogspot.com/-EF4HIO3ollk/UbQ8h5PygoI/AAAAAAAADWc/Mi8ZVx9f6ZI/s1600/%D0%A8%D0%B0%D1%80%D0%BB%D0%B5%D1%80%D1%83%D0%B0+%D0%91%D0%B5%D0%BB%D1%8C%D0%B3%D0%B8%D1%8F.jpg#clickable)](http://3.bp.blogspot.com/-EF4HIO3ollk/UbQ8h5PygoI/AAAAAAAADWc/Mi8ZVx9f6ZI/s1600/%D0%A8%D0%B0%D1%80%D0%BB%D0%B5%D1%80%D1%83%D0%B0+%D0%91%D0%B5%D0%BB%D1%8C%D0%B3%D0%B8%D1%8F.jpg)

**Russian:**

Колонна  из Кастелло Родриго.

**English:**

Column from Castello Rodrigo.

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/2012-12-07_10-07-45_524877.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/2012-12-07_10-07-45_524877.jpg)
-->

[![](http://4.bp.blogspot.com/-w-3EdlHpU1E/UbQ9a5dHreI/AAAAAAAADWw/JHkcEQS9pAk/s1600/2012-12-07_10-07-45_524877.jpg#clickable)](http://4.bp.blogspot.com/-w-3EdlHpU1E/UbQ9a5dHreI/AAAAAAAADWw/JHkcEQS9pAk/s1600/2012-12-07_10-07-45_524877.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/bashnya-s-peredatchikom-na-fone-golubogo-neba-0001112496-pre.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/bashnya-s-peredatchikom-na-fone-golubogo-neba-0001112496-pre.jpg)
-->

[![](http://4.bp.blogspot.com/-xEhRaW1lFI4/UbQ9bLfDZMI/AAAAAAAADW0/vFSnmEa0rhY/s1600/bashnya-s-peredatchikom-na-fone-golubogo-neba-0001112496-preview.jpg#clickable)](http://4.bp.blogspot.com/-xEhRaW1lFI4/UbQ9bLfDZMI/AAAAAAAADW0/vFSnmEa0rhY/s1600/bashnya-s-peredatchikom-na-fone-golubogo-neba-0001112496-preview.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/n1G3UL34sJM.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/n1G3UL34sJM.jpg)
-->

[![](http://2.bp.blogspot.com/-jkM1o39olYA/UbQ8oIIvpaI/AAAAAAAADWk/mFYJ8y8yTfw/s1600/n1G3UL34sJM.jpg#clickable)](http://2.bp.blogspot.com/-jkM1o39olYA/UbQ8oIIvpaI/AAAAAAAADWk/mFYJ8y8yTfw/s1600/n1G3UL34sJM.jpg)

**Russian:**

Современный ретранслятор

**English:**

Modern transponder [Cargo cult (imitation) (Wiki.ru)](http://ru.wikipedia.org/wiki/%CA%F3%EB%FC%F2_%EA%E0%F0%E3%EE)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4469b444c62d84314dad3b62878.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/4469b444c62d84314dad3b62878.jpg)
-->

[![](http://4.bp.blogspot.com/-MOpKeLzw9EE/UWZo41UccmI/AAAAAAAACjs/dhzdOybS_eM/s1600/4469b444c62d84314dad3b62878.jpg#clickable)](http://4.bp.blogspot.com/-MOpKeLzw9EE/UWZo41UccmI/AAAAAAAACjs/dhzdOybS_eM/s1600/4469b444c62d84314dad3b62878.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/af93d71.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/af93d71.jpg)
-->

[![](http://4.bp.blogspot.com/-i4A5bTybYzs/UWZpEITcwNI/AAAAAAAACj0/5LRi5vW3jrk/s1600/af93d71.jpg#clickable)](http://4.bp.blogspot.com/-i4A5bTybYzs/UWZpEITcwNI/AAAAAAAACj0/5LRi5vW3jrk/s1600/af93d71.jpg)

<!-- Local
[![](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a0697b65b55a353997c5f769b6f6ef08.jpg#clickable)](Where%20is%20the%20city%20from%3F%20Chapter%205.%20One%20project,%20one%20architect%20or%20cargo%20cult%3F_files/a0697b65b55a353997c5f769b6f6ef08.jpg)
-->

[![](http://2.bp.blogspot.com/-EK2iw89Nz8s/UWZpEjeSHgI/AAAAAAAACj4/6ep1HWY1aJc/s1600/a0697b65b55a353997c5f769b6f6ef08.jpg#clickable)](http://2.bp.blogspot.com/-EK2iw89Nz8s/UWZpEjeSHgI/AAAAAAAACj4/6ep1HWY1aJc/s1600/a0697b65b55a353997c5f769b6f6ef08.jpg)

**Russian:**

Копируя статью, не забывайте указывать авторство. 

**English:**

When copying the article, do not forget to credit the author.

**Russian:**

Адрес полной версии тут: ["Откуда город?"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

**English:**

The full version address is here: ["Where's the city from?"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

Author: ZigZag

© All rights reserved. The original author retains ownership and rights.
