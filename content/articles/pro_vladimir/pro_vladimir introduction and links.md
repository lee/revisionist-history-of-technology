title:  Pro_Vladimir's Introduction and Links
date: 2015-07-20
modified: Thu 14 Jan 2021 07:22:11 GMT
category: pro_vladimir
tags: links
slug: 
authors: Vladimir Mamzerev
summary: Links. A huge number of uncomfortable questions are asked there. Also present are a huge number of directions where you can search for answers to inconvenient questions.
status: published
originally: 183496.html


### Translated from: 

[https://pro-vladimir.livejournal.com/183496.html](https://pro-vladimir.livejournal.com/183496.html)

In light of the interest shown in the current posts, some introduction is overdue in order to explain what is happening within this magazine. Everything is simple, here is another attempt to shake the generally accepted foundations. It's all alternative.

We take this thread and pull it apart into components, and then put it together as it will.

We get acquainted with the traditional foundations (those that are shaky) in the process of analyzing elements. We can say that we start from them, and look further and wider on the time scale.

We are cramped in quite a few areas. Attempts to remind about this framework, with an official code, if you think that it is your duty to slip this or that dogma - hardened specialist, the collective farm is a voluntary matter. You like that everything has already been stolen before us, and we just have to agree, so be it.

There are only versions, how and who will dispose of them, everyone decides for himself. At least no one is chained to the monitor, and the page is easy to close and forget.

Virtual excavations are based on the texts of [dmitrijan](https://dmitrijan.livejournal.com/). In other words, everything stolen in this magazine has its roots in the numerous texts from the author [dmitrijan](https://dmitrijan.livejournal.com/).

The subject matter is very different. The scope of interests is wide. A huge number of uncomfortable questions are asked there. A huge number of directions where you can search for answers to inconveniently asked questions are also present.

Texts are multi-dimensional and multi-level. In the sense that each paragraph has semantic modular bindings to other elements of this complex of information. Any paragraph has its own weight, but when combined with the complex, it is more clearly positioned in the semantic picture of the submitted text.

There are many letters. Some simple words are put together in such a way that many who try to read it will freeze. In the truest sense of the word. From the outside, it looks like a "sleeping computer". And then, in fact, there will be deactivation and "erasing" of what you read, so that you can continue to live like everyone else in happy ignorance. Home, work, born like everyone else, lived like everyone else, died like everyone else. A headstone: Date. Dash. Date. Everything is like everyone else, in the warehouse of spent bodies. Treasure-bish.

In the same magazine, the light version is based on the texts of [dmitrijan](https://dmitrijan.livejournal.com/). With pictures. A tightened visual range. Like there's a script, there's a TV series based on it.

From the very beginning to the present day.

### Real History of Russia
- [Немного про то как откапывали Русь.](http://pro-vladimir.livejournal.com/116073.html) English title: [a Little about how Russia was dug up.](http://pro-vladimir.livejournal.com/116073.html)
- [По мотивам «И была Страна до России»](http://pro-vladimir.livejournal.com/994.html) English title: [based on And there was a Country before Russia»](http://pro-vladimir.livejournal.com/994.html)
- [По мотивам «И была Страна до России». Часть 2.](http://pro-vladimir.livejournal.com/1134.html) English title: [based on And there was a Country before Russia. Part 2.](http://pro-vladimir.livejournal.com/1134.html)
- [По мотивам «И была Страна до России». Часть 3.](http://pro-vladimir.livejournal.com/1376.html) English title: [based on And there was a Country before Russia. Part 3.](http://pro-vladimir.livejournal.com/1376.html)
- [По мотивам «И была Страна до России». Часть 4.](http://pro-vladimir.livejournal.com/1710.html) English title: [based on And there was a Country before Russia. Part 4.](http://pro-vladimir.livejournal.com/1710.html)
- [По мотивам «И была Страна до России». Часть 5.](http://pro-vladimir.livejournal.com/1813.html) English title: [based on And there was a Country before Russia. Part 5.](http://pro-vladimir.livejournal.com/1813.html)
- [По мотивам «И была Страна до России» Часть 6.](http://pro-vladimir.livejournal.com/2301.html) English title: [based on And there was a Country before Russia Part 6.](http://pro-vladimir.livejournal.com/2301.html)
- [По мотивам «И была Страна до России». Часть 7.](http://pro-vladimir.livejournal.com/2330.html) English title: [based on And there was a Country before Russia. Part 7.](http://pro-vladimir.livejournal.com/2330.html)
- [По мотивам «И была Страна до России». Часть 8.](http://pro-vladimir.livejournal.com/3857.html) English title: [based on And there was a Country before Russia. Part 8.](http://pro-vladimir.livejournal.com/3857.html)
- [По мотивам «И была Страна до России». Часть 9.](http://pro-vladimir.livejournal.com/8535.html) English title: [based on And there was a Country before Russia. Part 9.](http://pro-vladimir.livejournal.com/8535.html)
- [По мотивам И была Страна до России. Часть 10.](http://pro-vladimir.livejournal.com/8801.html) English title: [based on And there was a Country before Russia. Part 10.](http://pro-vladimir.livejournal.com/8801.html)
- [По мотивам И была Страна до России. Часть 11.1](http://pro-vladimir.livejournal.com/9239.html) English title: [based on And there was a Country before Russia. Part 11.1](http://pro-vladimir.livejournal.com/9239.html)
- [По мотивам И была Страна до России. Часть 11.2](http://pro-vladimir.livejournal.com/9704.html) English title: [based on And there was a Country before Russia. Part 11.2](http://pro-vladimir.livejournal.com/9704.html)
- [По мотивам «И была Страна до России. Часть 13»](http://pro-vladimir.livejournal.com/68718.html) English title: [based on And there was a Country before Russia. Part 13](http://pro-vladimir.livejournal.com/68718.html)
- [По мотивам «И была Страна до России». Часть 14](http://pro-vladimir.livejournal.com/70313.html) English title: [based on And there was a Country before Russia. Part 14](http://pro-vladimir.livejournal.com/70313.html)
- [По мотивам «И была Страна до России». Часть 15](http://pro-vladimir.livejournal.com/72882.html) English title: [based on And there was a Country before Russia. Part 15](http://pro-vladimir.livejournal.com/72882.html)
- [По мотивам «И была Страна до России». Часть 16.](http://pro-vladimir.livejournal.com/73396.html) English title: [based on And there was a Country before Russia. Part 16.](http://pro-vladimir.livejournal.com/73396.html)
- [По мотивам «И была Страна до России». Часть 17.](http://pro-vladimir.livejournal.com/74999.html) English title: [based on And there was a Country before Russia. Part 17.](http://pro-vladimir.livejournal.com/74999.html)
- [По мотивам «И была Страна до России». Часть 18.](http://pro-vladimir.livejournal.com/78168.html) English title: [based on And there was a Country before Russia. Part 18.](http://pro-vladimir.livejournal.com/78168.html)
- [По мотивам Старинные русские карты 17  начала 18 века](http://pro-vladimir.livejournal.com/137458.html) English title: [based on old Russian maps of the 17th and early 18th Century](http://pro-vladimir.livejournal.com/137458.html)
- [По мотивам Тайны прошлых цивилизаций 3](http://pro-vladimir.livejournal.com/144978.html) English title: [based on Secrets of past civilizations 3](http://pro-vladimir.livejournal.com/144978.html)
- [По мотивам текста про Планету.](http://pro-vladimir.livejournal.com/35497.html) English title: [based on the text about the Planet.](http://pro-vladimir.livejournal.com/35497.html)
- [По мотивам-Пирамиды божки и боги](http://pro-vladimir.livejournal.com/129606.html) English title: [Based on: Pyramids, deities and gods](http://pro-vladimir.livejournal.com/129606.html)

### Towns and Cities
- [Кто построил Петербург?](http://pro-vladimir.livejournal.com/8369.html) English title: [Who built St. Petersburg?](http://pro-vladimir.livejournal.com/8369.html)
- [Кто построил Петербург? 2.](http://pro-vladimir.livejournal.com/87729.html) English title: [Who built St. Petersburg? 2.](http://pro-vladimir.livejournal.com/87729.html)
- [Аксонометрический план Санкт - Петербурга](http://pro-vladimir.livejournal.com/165362.html) English title: [the Axonometric plan of Saint - Petersburg](http://pro-vladimir.livejournal.com/165362.html)
- [Волновое воздействие.](http://pro-vladimir.livejournal.com/101232.html) English title: [Wave effect.](http://pro-vladimir.livejournal.com/101232.html)
- [Волновое воздействаие. Часть 4. Бронзовые стволы.](http://pro-vladimir.livejournal.com/104489.html) English title: [Wave action. Part 4. Bronze cannons.](http://pro-vladimir.livejournal.com/104489.html)
- [Волновое воздействие и немного про кремль.](http://pro-vladimir.livejournal.com/104159.html) English title: [Wave impact and a bit about the Kremlin.](http://pro-vladimir.livejournal.com/104159.html)
- [Немного про Тобольск.](http://pro-vladimir.livejournal.com/137718.html) English title: [A little about Tobolsk.](http://pro-vladimir.livejournal.com/137718.html)
- [По мотивам Немного про Тобольск](http://pro-vladimir.livejournal.com/138004.html) English title: [Based on A little about Tobolsk](http://pro-vladimir.livejournal.com/138004.html)

- [Немного про Тобольск.](http://pro-vladimir.livejournal.com/137718.html) English title: [A little about Tobolsk.](http://pro-vladimir.livejournal.com/137718.html)
- [Еще немного про кремль.](http://pro-vladimir.livejournal.com/114163.html) English title: [a little more about the Kremlin.](http://pro-vladimir.livejournal.com/114163.html)


### Philosophy of the Situation
- [Готовы ли мы принимать правду?](http://pro-vladimir.livejournal.com/79798.html) English title: [are We ready to accept the truth?](http://pro-vladimir.livejournal.com/79798.html)
- [Зачем?](http://pro-vladimir.livejournal.com/152911.html) English title: [Why?](http://pro-vladimir.livejournal.com/152911.html)


### Technology

- [Бронза и DVD что между ними может быть общего?](http://pro-vladimir.livejournal.com/24561.html) English title: [Bronze and a DVD - what can they have in common?](http://pro-vladimir.livejournal.com/24561.html)
- [3D Печать до изобретения 3D печати.](http://pro-vladimir.livejournal.com/154764.html) English title: [3D Printing – before the invention of 3D printing.](http://pro-vladimir.livejournal.com/154764.html)
- [Немного про технологии прошлого.](http://pro-vladimir.livejournal.com/124969.html) English title: [A little bit about the technologies of the past.](http://pro-vladimir.livejournal.com/124969.html)
- [Грузозахватное приспособление из прошлого.](http://pro-vladimir.livejournal.com/102680.html) English title: [a load-grabbing device from the past.](http://pro-vladimir.livejournal.com/102680.html)
- [Средневековая гигавидеокамера?](http://pro-vladimir.livejournal.com/149733.html) English title: [a medieval Giga-video camera?](http://pro-vladimir.livejournal.com/149733.html)
- [Куда ушла дискета.](http://pro-vladimir.livejournal.com/120756.html) English title: [Where the floppy disk went.](http://pro-vladimir.livejournal.com/120756.html)
- [Таким образом можно «накапать» весьма большой заряд.](http://pro-vladimir.livejournal.com/139252.html) English title: [in this way you can accumulate a very large charge.](http://pro-vladimir.livejournal.com/139252.html)

- [Иконы говорите?](http://pro-vladimir.livejournal.com/87427.html) English title: [Icons you say?](http://pro-vladimir.livejournal.com/87427.html)
- [Ипсиссимус](http://pro-vladimir.livejournal.com/146399.html) English title: [Ipsissimus](http://pro-vladimir.livejournal.com/146399.html)
- [Украшения или запчасти?](http://pro-vladimir.livejournal.com/5472.html) English title: [Jewelry or spare parts?](http://pro-vladimir.livejournal.com/5472.html)
- [Просто колонны проcто древнего Египта.](http://pro-vladimir.livejournal.com/164242.html) English title: [just columns just ancient Egypt.](http://pro-vladimir.livejournal.com/164242.html)
- [«Магнитооптическая голография уже применяется для трехмерного хранения информации»](http://pro-vladimir.livejournal.com/84296.html) English title: [magneto-Optical holography is already being used for three-dimensional information storage](http://pro-vladimir.livejournal.com/84296.html)
- [Малахитовая тайнопись Уральских Мастеров](http://pro-vladimir.livejournal.com/145717.html) English title: [malachite cryptography of the Ural Masters](http://pro-vladimir.livejournal.com/145717.html)
- [Астрономические и геодезические инструменты](http://pro-vladimir.livejournal.com/82696.html) English title: [Astronomical and geodetic instruments](http://pro-vladimir.livejournal.com/82696.html)

- [Загадочные золотые спирали бронзового века](http://pro-vladimir.livejournal.com/182745.html) English title: [Mysterious golden spirals of the bronze age](http://pro-vladimir.livejournal.com/182745.html)

- [Измерение параметров мира. Магнитный планетарий.](http://pro-vladimir.livejournal.com/96138.html) English title: [the Measurement of the world. Magnetic planetarium.](http://pro-vladimir.livejournal.com/96138.html)

- [Машина времени изобретена и скрывается Ватиканом - Хроновизор итальянского монаха](http://pro-vladimir.livejournal.com/95296.html) English title: [time Machine invented and hidden by the Vatican - Italian monk's Chronovisor](http://pro-vladimir.livejournal.com/95296.html)


### Electricity
- [Электрическое оборудование прошлого.](http://pro-vladimir.livejournal.com/3405.html) English title: [Electrical equipment of the past.](http://pro-vladimir.livejournal.com/3405.html)
- [Кто есть Вольт? или Электрическое оборудование прошлого. Часть 2.](http://pro-vladimir.livejournal.com/4416.html) English title: [who is Volt? or Electrical equipment of the past. Part 2.](http://pro-vladimir.livejournal.com/4416.html)
- [Электрическое оборудование прошлого. Часть 3.](http://pro-vladimir.livejournal.com/40121.html) English title: [Electrical equipment of the past. Part 3.](http://pro-vladimir.livejournal.com/40121.html)
- [Электрическое оборудование прошлого. Часть 4.](http://pro-vladimir.livejournal.com/54023.html) English title: [Electrical equipment of the past. Part 4.](http://pro-vladimir.livejournal.com/54023.html)
- [Электрическое оборудование прошлого. Часть 5.](http://pro-vladimir.livejournal.com/54721.html) English title: [Electrical equipment of the past. Part 5.](http://pro-vladimir.livejournal.com/54721.html)
- [Электрическое оборудование прошлого. Часть 6.](http://pro-vladimir.livejournal.com/56021.html) English title: [Electrical equipment of the past. Part 6.](http://pro-vladimir.livejournal.com/56021.html)
- [Электрическое оборудование прошлого. Часть 7.](http://pro-vladimir.livejournal.com/97018.html) English title: [Electrical equipment of the past. Part 7.](http://pro-vladimir.livejournal.com/97018.html)
- [Электрическое оборудование прошлого. Часть 8.](http://pro-vladimir.livejournal.com/99272.html) English title: [Electrical equipment of the past. Part 8.](http://pro-vladimir.livejournal.com/99272.html)
- [Электрическое оборудование прошлого. Часть 9](http://pro-vladimir.livejournal.com/105902.html) English title: [Electrical equipment of the past. Part 9](http://pro-vladimir.livejournal.com/105902.html)
- [Электрическое оборудование прошлого. Часть 10.](http://pro-vladimir.livejournal.com/106967.html) English title: [Electrical equipment of the past. Part 10.](http://pro-vladimir.livejournal.com/106967.html)
- [Электрическое оборудование прошлого. Часть 11.](http://pro-vladimir.livejournal.com/129033.html) English title: [Electrical equipment of the past. Part 11.](http://pro-vladimir.livejournal.com/129033.html)

- [Физика водоворотов. Видео.](http://pro-vladimir.livejournal.com/117837.html) English title: [physics of whirlpools. Video.](http://pro-vladimir.livejournal.com/117837.html)

- [Электричество такое электрическое](http://pro-vladimir.livejournal.com/179038.html) English title: [Electricity is electric](http://pro-vladimir.livejournal.com/179038.html)

- [Электростатический генератор в изображениях прошлого.](http://pro-vladimir.livejournal.com/144480.html) English title: [Electrostatic generator in images of the past.](http://pro-vladimir.livejournal.com/144480.html)

- [Не слишком ли много ионов в прошлом?](http://pro-vladimir.livejournal.com/163371.html) English title: [Were there too many ions in the past?](http://pro-vladimir.livejournal.com/163371.html)

- [В проводе тока нет. Как нет?](http://pro-vladimir.livejournal.com/179351.html) English title: [There is no current in the wire. Why not?](http://pro-vladimir.livejournal.com/179351.html)

- [Пагода есть электрический сепаратор?](http://pro-vladimir.livejournal.com/133223.html) English title: [Does the pagoda have an electric separator?](http://pro-vladimir.livejournal.com/133223.html)

- [АРХИ-ВОЛЬТ](http://pro-vladimir.livejournal.com/5781.html) English title: [ARCH-VOLT](http://pro-vladimir.livejournal.com/5781.html)

- [Использование атмосферного электричества в прошлом+комментарии](http://pro-vladimir.livejournal.com/110349.html) English title: [use of atmospheric electricity in the past+comments](http://pro-vladimir.livejournal.com/110349.html)

- [Использование силовых полей в прошлом.](http://pro-vladimir.livejournal.com/173377.html) English title: [use of force fields in the past.](http://pro-vladimir.livejournal.com/173377.html)

- [На что способна микроволновка! Высоковольтная](http://pro-vladimir.livejournal.com/51165.html) English title: [What a microwave can do! High-voltage](http://pro-vladimir.livejournal.com/51165.html)

- [Электронные библиотеки.](http://pro-vladimir.livejournal.com/113318.html) English title: [Electronic libraries.](http://pro-vladimir.livejournal.com/113318.html)
- [Электронные ресурсы](http://pro-vladimir.livejournal.com/161754.html) English title: [Electronic resources](http://pro-vladimir.livejournal.com/161754.html)

- [Электрические фонтаны Гастона Планте](http://pro-vladimir.livejournal.com/96384.html) English title: [Gaston Plante electric fountains](http://pro-vladimir.livejournal.com/96384.html)

- [Как из колокольни сделать магнетрон. Пособие для чайников.](http://pro-vladimir.livejournal.com/85261.html) English title: [How to make a magnetron out of a bell tower. Manual for dummies.](http://pro-vladimir.livejournal.com/85261.html)

- [Принцип колоколен](http://pro-vladimir.livejournal.com/179838.html) English title: [bell tower principle](http://pro-vladimir.livejournal.com/179838.html)

### Giants
- [Далеко ли спрятались Великаны?](http://pro-vladimir.livejournal.com/151102.html) English title: [How far did the giants hide?](http://pro-vladimir.livejournal.com/151102.html)
- [Гиганты с книжками](http://pro-vladimir.livejournal.com/107740.html) English title: [Giants with books](http://pro-vladimir.livejournal.com/107740.html)
- [Книги гигантов](http://pro-vladimir.livejournal.com/107878.html) English title: [The book of giants](http://pro-vladimir.livejournal.com/107878.html)


### Summary of Cannons
- [Итог по стволам. Часть 1.](http://pro-vladimir.livejournal.com/141009.html) English title: [Summary of the cannons. Part 1.](http://pro-vladimir.livejournal.com/141009.html)
- [Итог по стволам. Часть 2.](http://pro-vladimir.livejournal.com/141150.html) English title: [Summary of the cannons. Part 2.](http://pro-vladimir.livejournal.com/141150.html)
- [Итог по стволам. Часть 3.](http://pro-vladimir.livejournal.com/141381.html) English title: [Summary of the cannons. Part 3.](http://pro-vladimir.livejournal.com/141381.html)
- [Итог по стволам. Часть 4.](http://pro-vladimir.livejournal.com/141703.html) English title: [Summary of the cannons. Part 4.](http://pro-vladimir.livejournal.com/141703.html)
- [Итог по стволам. Часть 5.](http://pro-vladimir.livejournal.com/141931.html) English title: [Summary of the cannons. Part 5.](http://pro-vladimir.livejournal.com/141931.html)
- [Итог по стволам. Часть 6.](http://pro-vladimir.livejournal.com/142299.html) English title: [Summary of the cannons. Part 6.](http://pro-vladimir.livejournal.com/142299.html)
- [Итог по стволам. Часть 7.](http://pro-vladimir.livejournal.com/142494.html) English title: [Summary of the cannons. Part 7.](http://pro-vladimir.livejournal.com/142494.html)
- [Итог по стволам. Часть 8.](http://pro-vladimir.livejournal.com/142826.html) English title: [Summary of the cannons. Part 8.](http://pro-vladimir.livejournal.com/142826.html)
- [Итог по стволам. Часть 9.](http://pro-vladimir.livejournal.com/142872.html) English title: [Summary of the cannons. Part 9.](http://pro-vladimir.livejournal.com/142872.html)
- [Итог по стволам. Часть 10.](http://pro-vladimir.livejournal.com/143229.html) English title: [Summary of the cannons. Part 10.](http://pro-vladimir.livejournal.com/143229.html)
- [Итог по стволам. Часть 11.](http://pro-vladimir.livejournal.com/143397.html) English title: [Summary of the cannons. Part 11.](http://pro-vladimir.livejournal.com/143397.html)
- [Итог по стволам. Часть 12.](http://pro-vladimir.livejournal.com/143682.html) English title: [Summary of the cannons. Part 12.](http://pro-vladimir.livejournal.com/143682.html)


### Armament and War
- [Кратер?](http://pro-vladimir.livejournal.com/134955.html) English title: [A crater?](http://pro-vladimir.livejournal.com/134955.html)
- [ПВО =Великая Китайская Стена](http://pro-vladimir.livejournal.com/11651.html) English title: [air defense = Great wall of China](http://pro-vladimir.livejournal.com/11651.html)
- [Осмотр пушечных стволов.](http://pro-vladimir.livejournal.com/78712.html) English title: [inspection of the cannon barrels.](http://pro-vladimir.livejournal.com/78712.html)
- [Осмотр пушечных стволов. Часть 2.](http://pro-vladimir.livejournal.com/85825.html) English title: [inspection of the cannon barrels. Part 2.](http://pro-vladimir.livejournal.com/85825.html)
- [Все ли трубы могут метать ядра порохом?](http://pro-vladimir.livejournal.com/87019.html) English title: [can all pipes throw cannon balls with gunpowder?](http://pro-vladimir.livejournal.com/87019.html)
- [А если попытать Царь-пушку?](http://pro-vladimir.livejournal.com/146658.html) English title: [And if you try the Tsar cannon?](http://pro-vladimir.livejournal.com/146658.html)
- [Подвиды артиллерии.](http://pro-vladimir.livejournal.com/153137.html) English title: [the Subtypes of artillery.](http://pro-vladimir.livejournal.com/153137.html)
- [Деталь архитектурного элемента или пушка?](http://pro-vladimir.livejournal.com/144318.html) English title: [Part of an architectural element or a cannon?](http://pro-vladimir.livejournal.com/144318.html)

#### Indict (Translator's note: query meaning)
- [Еще немного про индикт.](http://pro-vladimir.livejournal.com/122852.html) English title: [a little more about the indict.](http://pro-vladimir.livejournal.com/122852.html)
- [Индикты в отметках на картах. Часть 2.](http://pro-vladimir.livejournal.com/116921.html) English title: [the Indiction in the marks on the cards. Part 2.](http://pro-vladimir.livejournal.com/116921.html)
- [Индикты в отметках на картах.](http://pro-vladimir.livejournal.com/116421.html) English title: [the Indiction in the marks on the cards.](http://pro-vladimir.livejournal.com/116421.html)
- [Индикты в отметках на приборах. Обсуждение.](http://pro-vladimir.livejournal.com/120947.html) English title: [the Indiction in the marks on the instruments. Discussion.](http://pro-vladimir.livejournal.com/120947.html)
- [Индикты в отметках на приборах.](http://pro-vladimir.livejournal.com/120066.html) English title: [the Indiction in the marks on the instruments.](http://pro-vladimir.livejournal.com/120066.html)


- [Кремль индикты и стволы.](http://pro-vladimir.livejournal.com/109820.html) English title: [The Kremlin, the Indiction and cannons.](http://pro-vladimir.livejournal.com/109820.html)
- [Кремль индикты и стволы. Часть 2.](http://pro-vladimir.livejournal.com/113415.html) English title: [The Kremlin, the Indiction and cannons. Part 2.](http://pro-vladimir.livejournal.com/113415.html)


### Robots and Automata
- [Автоматон](http://pro-vladimir.livejournal.com/139551.html) English title: [an Automaton](http://pro-vladimir.livejournal.com/139551.html)
- [АВМ (аналоговая вычислительная машина)](http://pro-vladimir.livejournal.com/148390.html) English title: [AVM (analog computing machine](http://pro-vladimir.livejournal.com/148390.html)
- [Программируемые машины прошлого.](http://pro-vladimir.livejournal.com/100791.html) English title: [Programmable machines of the past.](http://pro-vladimir.livejournal.com/100791.html)
- [Программируемые машины прошлого. Часть 2. Волновое воздействие.](http://pro-vladimir.livejournal.com/102358.html) English title: [Programmable machines of the past. Part 2. Wave action.](http://pro-vladimir.livejournal.com/102358.html)
- [Программируемые машины прошлого. Часть 3. Волновое воздействие.](http://pro-vladimir.livejournal.com/102461.html) English title: [Programmable machines of the past. Part 3. Wave action.](http://pro-vladimir.livejournal.com/102461.html)

### Transport Systems of the Past

- [Транспортные системы прошлого.](http://pro-vladimir.livejournal.com/6435.html) English title: [Transport systems of the past.](http://pro-vladimir.livejournal.com/6435.html)
- [Транспортные системы прошлого. Часть 2.](http://pro-vladimir.livejournal.com/6728.html) English title: [Transport systems of the past. Part 2.](http://pro-vladimir.livejournal.com/6728.html)
- [Транспортные системы прошлого. Часть 3.](http://pro-vladimir.livejournal.com/11316.html) English title: [Transport systems of the past. Part 3.](http://pro-vladimir.livejournal.com/11316.html)
- [Транспортные системы прошлого. Часть 4.](http://pro-vladimir.livejournal.com/15315.html) English title: [Transport systems of the past. Part 4.](http://pro-vladimir.livejournal.com/15315.html)
- [Транспортные системы прошлого. Часть 5.](http://pro-vladimir.livejournal.com/16890.html) English title: [Transport systems of the past. Part 5.](http://pro-vladimir.livejournal.com/16890.html)
- [Транспортные системы прошлого. Часть 6.](http://pro-vladimir.livejournal.com/20182.html) English title: [Transport systems of the past. Part 6.](http://pro-vladimir.livejournal.com/20182.html)
- [Транспортные системы прошлого. Часть 7.](http://pro-vladimir.livejournal.com/27608.html) English title: [Transport systems of the past. Part 7.](http://pro-vladimir.livejournal.com/27608.html)
- [Транспортные системы прошлого. Часть 8. ](http://pro-vladimir.livejournal.com/53401.html) English title: [Transport systems of the past. Part 8.](http://pro-vladimir.livejournal.com/53401.html)
- [Транспортные системы прошлого. Часть 9.](http://pro-vladimir.livejournal.com/53778.html) English title: [Transport systems of the past. Part 9.](http://pro-vladimir.livejournal.com/53778.html)
- [Транспортные системы прошлого. Часть 10.](http://pro-vladimir.livejournal.com/69373.html) English title: [Transport systems of the past. Part 10.](http://pro-vladimir.livejournal.com/69373.html)
- [Транспортные системы будущего.](http://pro-vladimir.livejournal.com/87086.html) English title: [Transport systems of the future.](http://pro-vladimir.livejournal.com/87086.html)
- [Транспортные системы прошлого. Часть 12.](http://pro-vladimir.livejournal.com/112919.html) English title: [Transport systems of the past. Part 12.](http://pro-vladimir.livejournal.com/112919.html)
- [Транспортные системы прошлого. Часть 13.](http://pro-vladimir.livejournal.com/119733.html) English title: [Transport systems of the past. Part 13.](http://pro-vladimir.livejournal.com/119733.html)
- [Транспортные системы прошлого. Часть 14.](http://pro-vladimir.livejournal.com/123333.html) English title: [Transport systems of the past. Part 14.](http://pro-vladimir.livejournal.com/123333.html)
- [Транспортные системы прошлого. Часть 15.](http://pro-vladimir.livejournal.com/125334.html) English title: [Transport systems of the past. Part 15.](http://pro-vladimir.livejournal.com/125334.html)
- [Транспортные системы прошлого. Часть 16.](http://pro-vladimir.livejournal.com/130473.html) English title: [Transport systems of the past. Part 16.](http://pro-vladimir.livejournal.com/130473.html)
- [Транспортные системы прошлого. Часть 17.](http://pro-vladimir.livejournal.com/130688.html) English title: [Transport systems of the past. Part 17.](http://pro-vladimir.livejournal.com/130688.html)
- [Транспортные системы прошлого. Часть 18.](http://pro-vladimir.livejournal.com/130939.html) English title: [Transport systems of the past. Part 18.](http://pro-vladimir.livejournal.com/130939.html)
- [Транспортные системы прошлого. Часть 19.](http://pro-vladimir.livejournal.com/147883.html) English title: [Transport systems of the past. Part 19.](http://pro-vladimir.livejournal.com/147883.html)
- [Разрушенные «звезды». Транспортные системы прошлого. Часть 20.](http://pro-vladimir.livejournal.com/154114.html) English title: [Destroyed the stars. Transport systems of the past. Part 20.](http://pro-vladimir.livejournal.com/154114.html)
- [Транспортные системы прошлого. Часть 21.](http://pro-vladimir.livejournal.com/160753.html) English title: [Transport systems of the past. Part 21.](http://pro-vladimir.livejournal.com/160753.html)
- [Транспортные системы прошлого. Часть 22.](http://pro-vladimir.livejournal.com/162950.html) English title: [Transport systems of the past. Part 22.](http://pro-vladimir.livejournal.com/162950.html)
- [Транспортные системы прошлого. Часть 23.](http://pro-vladimir.livejournal.com/166079.html) English title: [Transport systems of the past. Part 23.](http://pro-vladimir.livejournal.com/166079.html)
- [Транспортные системы прошлого. Часть 24-1.](http://pro-vladimir.livejournal.com/166966.html) English title: [Transport systems of the past. Part 24-1.](http://pro-vladimir.livejournal.com/166966.html)
- [Транспортные системы прошлого. Часть 24-2. ](http://pro-vladimir.livejournal.com/167410.html) English title: [Transport systems of the past. Part 24-2.](http://pro-vladimir.livejournal.com/167410.html)
- [Транспортные системы прошлого. Часть 24-3. ](http://pro-vladimir.livejournal.com/167487.html) English title: [Transport systems of the past. Part 24-3.](http://pro-vladimir.livejournal.com/167487.html)
- [Транспортные системы прошлого. Часть 25.](http://pro-vladimir.livejournal.com/169144.html) English title: [Transport systems of the past. Part 25.](http://pro-vladimir.livejournal.com/169144.html)
- [Транспортные системы прошлого. Часть 26. Многомерность где же ты?](http://pro-vladimir.livejournal.com/170335.html) English title: [Transport systems of the past. Part 26. Multidimensionality where are you?](http://pro-vladimir.livejournal.com/170335.html)
- [Транспортные системы прошлого. Часть 27. Многомерность где же ты?](http://pro-vladimir.livejournal.com/173831.html) English title: [Transport systems of the past. Part 27. Multidimensionality where are you?](http://pro-vladimir.livejournal.com/173831.html)
- [Транспортные системы прошлого. Часть 28.](http://pro-vladimir.livejournal.com/176141.html) English title: [Transport systems of the past. Part 28.](http://pro-vladimir.livejournal.com/176141.html)
- [Как подступится к Транспортным системам прошлого.](http://pro-vladimir.livejournal.com/177222.html) English title: [how to approach the Transport systems of the past.](http://pro-vladimir.livejournal.com/177222.html)
- [Как подступится к Транспортным системам прошлого. Часть 2.](http://pro-vladimir.livejournal.com/177542.html) English title: [how to approach the Transport systems of the past. Part 2.](http://pro-vladimir.livejournal.com/177542.html)
- [Транспортные системы прошлого. Часть 29](http://pro-vladimir.livejournal.com/178758.html) English title: [Transport systems of the past. Part 29](http://pro-vladimir.livejournal.com/178758.html)
- [Транспортные системы прошлого. Часть 30](http://pro-vladimir.livejournal.com/181574.html) English title: [Transport systems of the past. Part 30](http://pro-vladimir.livejournal.com/181574.html)


### Architecture
- [Что не так с навершиями?](http://pro-vladimir.livejournal.com/158441.html) English title: [what's wrong with the finials?](http://pro-vladimir.livejournal.com/158441.html)
- [Почему колонку назвали колонкой?](http://pro-vladimir.livejournal.com/147406.html) English title: [Why was the column called a column?](http://pro-vladimir.livejournal.com/147406.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра](http://pro-vladimir.livejournal.com/7567.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA](http://pro-vladimir.livejournal.com/7567.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 2.](http://pro-vladimir.livejournal.com/19699.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 2.](http://pro-vladimir.livejournal.com/19699.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 3.](http://pro-vladimir.livejournal.com/26142.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 3.](http://pro-vladimir.livejournal.com/26142.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 4.](http://pro-vladimir.livejournal.com/31497.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 4.](http://pro-vladimir.livejournal.com/31497.html)
- [По мотивам И была Страна до России. Часть 12. или Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 5.](http://pro-vladimir.livejournal.com/33597.html) English title: [based on And there was a Country before Russia. Part 12. or Architecture= AZ-RH-Te-K-To-U-RA. Part 5.](http://pro-vladimir.livejournal.com/33597.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 6.](http://pro-vladimir.livejournal.com/66325.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 6.](http://pro-vladimir.livejournal.com/66325.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 7.](http://pro-vladimir.livejournal.com/93652.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 7.](http://pro-vladimir.livejournal.com/93652.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 8.](http://pro-vladimir.livejournal.com/97821.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 8.](http://pro-vladimir.livejournal.com/97821.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 9.](http://pro-vladimir.livejournal.com/98345.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 9.](http://pro-vladimir.livejournal.com/98345.html)
- [Архитектура= Азъ-РХ-Те-К-То-У-Ра. Часть 10.](http://pro-vladimir.livejournal.com/98972.html) English title: [Architecture= AZ-RH-Te-K-To-U-RA. Part 10.](http://pro-vladimir.livejournal.com/98972.html)
- [Странности каменной кладки.](http://pro-vladimir.livejournal.com/152376.html) English title: [the strangeness of masonry.](http://pro-vladimir.livejournal.com/152376.html)


### Steel Ties and Temples
- [Копилка стальных связей.](http://pro-vladimir.livejournal.com/164533.html) English title: [Piggy Bank of steel ties.](http://pro-vladimir.livejournal.com/164533.html)
- [Стальные связи и решетки храмов.](http://pro-vladimir.livejournal.com/88394.html) English title: [Steel ties and lattices of temples.](http://pro-vladimir.livejournal.com/88394.html)
- [Стальные связи и решетки храмов. Часть 2.](http://pro-vladimir.livejournal.com/88587.html) English title: [Steel ties and lattices of temples. Part 2.](http://pro-vladimir.livejournal.com/88587.html)
- [Стальные связи и решетки храмов. Часть 2. +комментарии](http://pro-vladimir.livejournal.com/89622.html) English title: [Steel ties and lattices of temples. Part 2. +comments](http://pro-vladimir.livejournal.com/89622.html)
- [Стальные связи и решетки храмов. Часть 3.](http://pro-vladimir.livejournal.com/89982.html) English title: [Steel ties and lattices of temples. Part 3.](http://pro-vladimir.livejournal.com/89982.html)
- [Стальные связи и решетки храмов. Часть 4.](http://pro-vladimir.livejournal.com/91750.html) English title: [Steel ties and lattices of temples. Part 4.](http://pro-vladimir.livejournal.com/91750.html)
- [Стальные связи и решетки храмов. Часть 5.](http://pro-vladimir.livejournal.com/92442.html) English title: [Steel ties and lattices of temples. Part 5.](http://pro-vladimir.livejournal.com/92442.html)
- [Стальные связи и решетки храмов. Часть 6.](http://pro-vladimir.livejournal.com/99908.html) English title: [Steel ties and lattices of temples. Part 6.](http://pro-vladimir.livejournal.com/99908.html)
- [Стальные связи и решетки храмов. Часть 7.](http://pro-vladimir.livejournal.com/123642.html) English title: [Steel ties and lattices of temples. Part 7.](http://pro-vladimir.livejournal.com/123642.html)


### Geophysics
- [Солнце](http://pro-vladimir.livejournal.com/103253.html) English title: [the Sun](http://pro-vladimir.livejournal.com/103253.html)
- [Солнце](http://pro-vladimir.livejournal.com/51221.html) English title: [the Sun](http://pro-vladimir.livejournal.com/51221.html)
- [Все ли мы знаем про солнце?](http://pro-vladimir.livejournal.com/38761.html) English title: [do we know everything about the sun?](http://pro-vladimir.livejournal.com/38761.html)

- [Мысли. А как давно в последний раз менялись параметры мира?](http://pro-vladimir.livejournal.com/161850.html) English title: [Thoughts. How long ago was the last time the world's parameters changed?](http://pro-vladimir.livejournal.com/161850.html)


### Chronology
- [Индикт или система записи дат прошлого. Часть 2.](http://pro-vladimir.livejournal.com/94813.html) English title: [an Indication or system for recording past dates. Part 2.](http://pro-vladimir.livejournal.com/94813.html)
- [Индикт или система записи дат прошлого. Часть 3.](http://pro-vladimir.livejournal.com/95866.html) English title: [an Indication or system for recording past dates. Part 3.](http://pro-vladimir.livejournal.com/95866.html)
- [Индикт или система записи дат прошлого.](http://pro-vladimir.livejournal.com/92685.html) English title: [an Indication or system for recording past dates.](http://pro-vladimir.livejournal.com/92685.html)
- [Куда ведут календари.](http://pro-vladimir.livejournal.com/127037.html) English title: [where calendars lead.](http://pro-vladimir.livejournal.com/127037.html)

### Forgeries
- [Ловля фальсификаторов.](http://pro-vladimir.livejournal.com/170152.html) English title: [catching forgers.](http://pro-vladimir.livejournal.com/170152.html)
- [Ловля фальсификаторов. Часть 2.](http://pro-vladimir.livejournal.com/171092.html) English title: [catching forgers. Part 2.](http://pro-vladimir.livejournal.com/171092.html)
- [Ловля фальсификаторов. Часть 3.](http://pro-vladimir.livejournal.com/174713.html) English title: [catching forgers. Part 3.](http://pro-vladimir.livejournal.com/174713.html)

### Maps

- [Листая старые карты](http://pro-vladimir.livejournal.com/52718.html) English title: [flipping Through old maps](http://pro-vladimir.livejournal.com/52718.html)
- [Листая старые карты.2.](http://pro-vladimir.livejournal.com/107457.html) English title: [flipping Through old maps.2.](http://pro-vladimir.livejournal.com/107457.html)
- [Листая старые карты.3.](http://pro-vladimir.livejournal.com/108105.html) English title: [flipping Through old maps.3.](http://pro-vladimir.livejournal.com/108105.html)

- [Старинные русские карты 17 начала 18 века .](http://pro-vladimir.livejournal.com/136826.html) English title: [old Russian maps of the 17th and early 18th century .](http://pro-vladimir.livejournal.com/136826.html)


### Books

- [Листая старые трактаты.](http://pro-vladimir.livejournal.com/131447.html) English title: [flipping Through old tracts.](http://pro-vladimir.livejournal.com/131447.html)
- [Листая старые трактаты.](http://pro-vladimir.livejournal.com/152818.html) English title: [flipping Through old tracts.](http://pro-vladimir.livejournal.com/152818.html)
- [Листая старые трактаты. Часть 1.](http://pro-vladimir.livejournal.com/62595.html) English title: [flipping Through old tracts. Part 1.](http://pro-vladimir.livejournal.com/62595.html)
- [Листая старые трактаты. Часть 2.](http://pro-vladimir.livejournal.com/62933.html) English title: [flipping Through old tracts. Part 2.](http://pro-vladimir.livejournal.com/62933.html)
- [Листая старые трактаты. Часть 3.](http://pro-vladimir.livejournal.com/63661.html) English title: [flipping Through old tracts. Part 3.](http://pro-vladimir.livejournal.com/63661.html)
- [Листая старые трактаты. Часть 4.](http://pro-vladimir.livejournal.com/64305.html) English title: [flipping Through old tracts. Part 4.](http://pro-vladimir.livejournal.com/64305.html)
- [Листая старые трактаты. Часть 5.](http://pro-vladimir.livejournal.com/64593.html) English title: [flipping Through old tracts. Part 5.](http://pro-vladimir.livejournal.com/64593.html)
- [Листая старые трактаты. Часть 6.](http://pro-vladimir.livejournal.com/65237.html) English title: [flipping Through old tracts. Part 6.](http://pro-vladimir.livejournal.com/65237.html)
- [Листая старые трактаты. Часть 7.](http://pro-vladimir.livejournal.com/66591.html) English title: [flipping Through old tracts. Part 7.](http://pro-vladimir.livejournal.com/66591.html)
- [Листая старые трактаты. Часть 8.](http://pro-vladimir.livejournal.com/67047.html) English title: [flipping Through old tracts. Part 8.](http://pro-vladimir.livejournal.com/67047.html)
- [Листая старые трактаты. Часть 9.](http://pro-vladimir.livejournal.com/70477.html) English title: [flipping Through old tracts. Part 9.](http://pro-vladimir.livejournal.com/70477.html)

- [Листая старые трактаты. Часть 10.](http://pro-vladimir.livejournal.com/71020.html) English title: [flipping Through old tracts. Part 10.](http://pro-vladimir.livejournal.com/71020.html)
- [Листая старые трактаты. Часть 11.](http://pro-vladimir.livejournal.com/75407.html) English title: [flipping Through old tracts. Part 11.](http://pro-vladimir.livejournal.com/75407.html)
- [Листая старые трактаты. Часть 12.](http://pro-vladimir.livejournal.com/75663.html) English title: [flipping Through old tracts. Part 12.](http://pro-vladimir.livejournal.com/75663.html)
- [Листая старые трактаты. Часть 13. или Транспортные системы прошлого. Часть 11.](http://pro-vladimir.livejournal.com/76791.html) English title: [flipping Through old tracts. Part 13. or Transport systems of the past. Part 11.](http://pro-vladimir.livejournal.com/76791.html)
- [Листая старые трактаты. Часть 13.](http://pro-vladimir.livejournal.com/75919.html) English title: [flipping Through old tracts. Part 13.](http://pro-vladimir.livejournal.com/75919.html)
- [Листая старые трактаты. Часть 14.](http://pro-vladimir.livejournal.com/79143.html) English title: [flipping Through old tracts. Part 14.](http://pro-vladimir.livejournal.com/79143.html)
- [Листая старые трактаты. Часть 15.](http://pro-vladimir.livejournal.com/80291.html) English title: [flipping Through old tracts. Part 15.](http://pro-vladimir.livejournal.com/80291.html)
- [Листая старые трактаты. Часть 16.](http://pro-vladimir.livejournal.com/82646.html) English title: [flipping Through old tracts. Part 16.](http://pro-vladimir.livejournal.com/82646.html)

- [Старопечатные книги.](http://pro-vladimir.livejournal.com/105511.html) English title: [old Printed books.](http://pro-vladimir.livejournal.com/105511.html)

- [Первоисточники. Карты Книги .…](http://pro-vladimir.livejournal.com/126700.html) English title: [Primary sources. Maps books .…](http://pro-vladimir.livejournal.com/126700.html)

- [Первоисточники.](http://pro-vladimir.livejournal.com/115789.html) English title: [Primary sources.](http://pro-vladimir.livejournal.com/115789.html)

- [Первоисточники. Читать не перечитать.](http://pro-vladimir.livejournal.com/172001.html) English title: [Primary sources. Read not reread.](http://pro-vladimir.livejournal.com/172001.html)


### Libraries
- [Вы не подскажите как пройти в библиотеку Ивана Грозного?](http://pro-vladimir.livejournal.com/133428.html) English title: [Can you tell me how to get to the library of Ivan the terrible?](http://pro-vladimir.livejournal.com/133428.html)
- [Библиотека Ватикана](http://pro-vladimir.livejournal.com/174135.html) English title: [the Vatican Library](http://pro-vladimir.livejournal.com/174135.html)
- [Александрийская библиотека.](http://pro-vladimir.livejournal.com/80391.html) English title: [the library of Alexandria.](http://pro-vladimir.livejournal.com/80391.html)
- [Александрийская библиотека. часть 2.](http://pro-vladimir.livejournal.com/82962.html) English title: [the library of Alexandria. part 2.](http://pro-vladimir.livejournal.com/82962.html)
- [Александрийская библиотека. Часть 3.](http://pro-vladimir.livejournal.com/84535.html) English title: [library of Alexandria. Part 3.](http://pro-vladimir.livejournal.com/84535.html)


### The Media of the Past
- [Носители информации прошлого.](http://pro-vladimir.livejournal.com/28598.html) English title: [the Media of the past.](http://pro-vladimir.livejournal.com/28598.html)
- [Носители информации прошлого. Часть 2.](http://pro-vladimir.livejournal.com/28918.html) English title: [the Media of the past. Part 2.](http://pro-vladimir.livejournal.com/28918.html)
- [Носители информации прошлого. Часть 3.](http://pro-vladimir.livejournal.com/29127.html) English title: [the Media of the past. Part 3.](http://pro-vladimir.livejournal.com/29127.html)
- [Носители информации прошлого. Часть 4.](http://pro-vladimir.livejournal.com/29387.html) English title: [the Media of the past. Part 4.](http://pro-vladimir.livejournal.com/29387.html)
- [Носители информации прошлого. Часть 5.](http://pro-vladimir.livejournal.com/29661.html) English title: [the Media of the past. Part 5.](http://pro-vladimir.livejournal.com/29661.html)

### Gold and Money

- [Про золото.](http://pro-vladimir.livejournal.com/27053.html) English title: [About the gold.](http://pro-vladimir.livejournal.com/27053.html)
- [ПРО... ДЕНЬГИ](http://pro-vladimir.livejournal.com/50137.html) English title: [about... MONEY](http://pro-vladimir.livejournal.com/50137.html)


### Uncategorised

- [Щель в зазеркалье.](http://pro-vladimir.livejournal.com/87962.html) English title: [a crack in the looking Glass.](http://pro-vladimir.livejournal.com/87962.html)

- [Дефект который ждал десятилетия + комментарии.](http://pro-vladimir.livejournal.com/168547.html) English title: [a defect that has been waiting for decades + comments.](http://pro-vladimir.livejournal.com/168547.html)

- [Дверь в двери.](http://pro-vladimir.livejournal.com/164891.html) English title: [a door to door service.](http://pro-vladimir.livejournal.com/164891.html)

- [Смесь бульдога с носорогом.](http://pro-vladimir.livejournal.com/81340.html) English title: [a mixture of a bulldog and a rhinoceros.](http://pro-vladimir.livejournal.com/81340.html)

- [И один в поле воин. Или…+комментарии](http://pro-vladimir.livejournal.com/172454.html) English title: [And one in the field warrior. Or...+comments](http://pro-vladimir.livejournal.com/172454.html)

- [А кто этот маленький?](http://pro-vladimir.livejournal.com/182139.html) English title: [And who is this little one?](http://pro-vladimir.livejournal.com/182139.html)

- [Спрашивали-отвечаем.](http://pro-vladimir.livejournal.com/147501.html) English title: [Asked-answered.](http://pro-vladimir.livejournal.com/147501.html)

- [Пробой в измерениях. А как по вашему должен выглядеть портал?](http://pro-vladimir.livejournal.com/158810.html) English title: [breakdown in dimensions. What do you think the portal should look like?](http://pro-vladimir.livejournal.com/158810.html)


- [Нарубил и замесил.](http://pro-vladimir.livejournal.com/124566.html) English title: [chopped And kneaded.](http://pro-vladimir.livejournal.com/124566.html)

- [Яснослышание и не только.](http://pro-vladimir.livejournal.com/67444.html) English title: [Clairaudience and not only.](http://pro-vladimir.livejournal.com/67444.html)

- [Codex SERAPHINIANUS - одна страница буквально.](http://pro-vladimir.livejournal.com/157002.html) English title: [Codex SERAPHINIANUS - one page literally.](http://pro-vladimir.livejournal.com/157002.html)

- [Комментарив к Левитация и храмы](http://pro-vladimir.livejournal.com/153981.html) English title: [Commentaries on levitation and temples](http://pro-vladimir.livejournal.com/153981.html)

- [Разговор. А и О.](http://pro-vladimir.livejournal.com/84130.html) English title: [Conversation. A and O.](http://pro-vladimir.livejournal.com/84130.html)

- [Хрустальные кресты иссопы и прочий церковный хрусталь.](http://pro-vladimir.livejournal.com/149831.html) English title: [Crystal crosses hyssop and other Church crystal.](http://pro-vladimir.livejournal.com/149831.html)

- [Глубже и шире.](http://pro-vladimir.livejournal.com/128296.html) English title: [deeper and Wider.](http://pro-vladimir.livejournal.com/128296.html)

- [Всё спрятано](http://pro-vladimir.livejournal.com/180094.html) English title: [Everything is hidden](http://pro-vladimir.livejournal.com/180094.html)

- [Измышления по поводу взора.](http://pro-vladimir.livejournal.com/2998.html) English title: [Fabrications about the gaze.](http://pro-vladimir.livejournal.com/2998.html)

- [Перво-Боги. Статуи изображения.](http://pro-vladimir.livejournal.com/129831.html) English title: [First - the Gods. Statues images.](http://pro-vladimir.livejournal.com/129831.html)


### Forgotten Temples of the Land of Tula
- [Забытые храмы земли Тульской (Часть IV - вдоль Старого Каширского тракта)+комментарии.](http://pro-vladimir.livejournal.com/158612.html) English title: [forgotten temples of the land of Tula (part IV - along The old Kashirsky tract)+comments.](http://pro-vladimir.livejournal.com/158612.html)

- [Из Богдановского в Висляево или забытые храмы Ферзиковского района +комментарии.](http://pro-vladimir.livejournal.com/174987.html) English title: [from Bogdanovsky to Vislyaevo or forgotten temples of the Ferzikovsky district +comments.](http://pro-vladimir.livejournal.com/174987.html)

- [Из найденного вчера.](http://pro-vladimir.livejournal.com/169413.html) English title: [From what I found yesterday.](http://pro-vladimir.livejournal.com/169413.html)


- [Очки.](http://pro-vladimir.livejournal.com/58553.html) English title: [Glasses.](http://pro-vladimir.livejournal.com/58553.html)
- [Гирокары + комментарий](http://pro-vladimir.livejournal.com/153772.html) English title: [Grocery + review](http://pro-vladimir.livejournal.com/153772.html)


- [Еще немного еще чуть чуть.](http://pro-vladimir.livejournal.com/12001.html) English title: [just a little more just a little more.](http://pro-vladimir.livejournal.com/12001.html)

- [Московское барокко в Калужской глуши или Казанская церковь в Богородском+комментарии](http://pro-vladimir.livejournal.com/173673.html) English title: [Moscow Baroque in the Kaluga wilderness or Kazan Church in Bogorodskoye+comments](http://pro-vladimir.livejournal.com/173673.html)

- [Многомерность. Из обсуждений.](http://pro-vladimir.livejournal.com/170836.html) English title: [multi-Dimensionality. From the discussion.](http://pro-vladimir.livejournal.com/170836.html)

- [Возвращается муж с командировки. Открывает шкаф а там Нарния.(с)](http://pro-vladimir.livejournal.com/146938.html) English title: [My husband returns from a business trip. Opens the closet and there's Narnia.(C)](http://pro-vladimir.livejournal.com/146938.html)

- [Н. Витсен Северная и Восточная Тартария](http://pro-vladimir.livejournal.com/105407.html) English title: [N. Witsen Northern and Eastern Tartary](http://pro-vladimir.livejournal.com/105407.html)

- [Живопись.](http://pro-vladimir.livejournal.com/182883.html) English title: [Painting.](http://pro-vladimir.livejournal.com/182883.html)

- [Путь.](http://pro-vladimir.livejournal.com/102106.html) English title: [Path.](http://pro-vladimir.livejournal.com/102106.html)

- [Заложили...лишние отверстия от сквозняков.](http://pro-vladimir.livejournal.com/115420.html) English title: [Pawned...extra holes from drafts.](http://pro-vladimir.livejournal.com/115420.html)

- [Люди кремниевых тел](http://pro-vladimir.livejournal.com/152035.html) English title: [people of silicon bodies](http://pro-vladimir.livejournal.com/152035.html)



- [Люди такие люди.](http://pro-vladimir.livejournal.com/3728.html) English title: [People such people.](http://pro-vladimir.livejournal.com/3728.html)

- [Бот Петра.](http://pro-vladimir.livejournal.com/162297.html) English title: [Peter's Bot.](http://pro-vladimir.livejournal.com/162297.html)

- [Планета.](http://pro-vladimir.livejournal.com/163318.html) English title: [planet.](http://pro-vladimir.livejournal.com/163318.html)

- [post](http://pro-vladimir.livejournal.com/109391.html) English title: [post](http://pro-vladimir.livejournal.com/109391.html)

- [Публичная казнь белокровой красотки или Вот ты какая голубая кровь!+комментарии](http://pro-vladimir.livejournal.com/110852.html) English title: [public execution of a fair-haired beauty or that's what you are blue blood!+comments](http://pro-vladimir.livejournal.com/110852.html)

- [regiomontanus calendarium](http://pro-vladimir.livejournal.com/110762.html) English title: [regiomontanus calendarium](http://pro-vladimir.livejournal.com/110762.html)

- [Загадки истории](http://pro-vladimir.livejournal.com/130063.html) English title: [Riddles of history](http://pro-vladimir.livejournal.com/130063.html)

- [Руины древнего города Мерв + комментарии.](http://pro-vladimir.livejournal.com/168365.html) English title: [ruins of the ancient city of Merv + comments.](http://pro-vladimir.livejournal.com/168365.html)

- [Посох: этимологический ракурс.+комментарии](http://pro-vladimir.livejournal.com/112677.html) English title: [Staff: an etymological perspective.+comments](http://pro-vladimir.livejournal.com/112677.html)


- [Всевидящие око говорите?](http://pro-vladimir.livejournal.com/97024.html) English title: [the all-seeing eye say?](http://pro-vladimir.livejournal.com/97024.html)

- [Город Петра.](http://pro-vladimir.livejournal.com/43333.html) English title: [the City of Petra.](http://pro-vladimir.livejournal.com/43333.html)
- [Разгерметизация.](http://pro-vladimir.livejournal.com/172919.html) English title: [the Depressurization.](http://pro-vladimir.livejournal.com/172919.html)
- [Горят купола. Бобры построили плотину.](http://pro-vladimir.livejournal.com/132473.html) English title: [the domes are burning. The beavers built a dam.](http://pro-vladimir.livejournal.com/132473.html)
- [Горят купола. Бобры построили плотину. Часть 2.](http://pro-vladimir.livejournal.com/132608.html) English title: [the domes are burning. The beavers built a dam. Part 2.](http://pro-vladimir.livejournal.com/132608.html)
- [Поле чудес. Крутите барабан.](http://pro-vladimir.livejournal.com/134374.html) English title: [the Field of miracles. Spin the drum.](http://pro-vladimir.livejournal.com/134374.html)
- [Поле чудес. Крутите барабан. Часть 2.](http://pro-vladimir.livejournal.com/135302.html) English title: [the Field of miracles. Spin the drum. Part 2.](http://pro-vladimir.livejournal.com/135302.html)
- [Эффект Джанибекова](http://pro-vladimir.livejournal.com/16086.html) English title: [the Janibekov Effect](http://pro-vladimir.livejournal.com/16086.html)
- [Ключи от портала?](http://pro-vladimir.livejournal.com/83435.html) English title: [the keys to the portal?](http://pro-vladimir.livejournal.com/83435.html)
- [Самое большое казино планеты- ООО МММ Америка. Аминь.](http://pro-vladimir.livejournal.com/178465.html) English title: [the largest casino on the planet is MMM America LLC. Amen.](http://pro-vladimir.livejournal.com/178465.html)
- [Значение символов. часть 1.](http://pro-vladimir.livejournal.com/2602.html) English title: [the meaning of the symbols. part 1.](http://pro-vladimir.livejournal.com/2602.html)


- [Папский крест? Или то вовсе и не крест?](http://pro-vladimir.livejournal.com/53690.html) English title: [the Papal cross? Or is it not a cross at all?](http://pro-vladimir.livejournal.com/53690.html)




- [Две крайности.](http://pro-vladimir.livejournal.com/7994.html) English title: [Two extremes.](http://pro-vladimir.livejournal.com/7994.html)

- [«Вверх» («Up»)](http://pro-vladimir.livejournal.com/171466.html) English title: [Up (Up»)](http://pro-vladimir.livejournal.com/171466.html)

- [Букварь Войнича- первые шаги дешифровки.](http://pro-vladimir.livejournal.com/151301.html) English title: [Voynich's Primer - the first steps of decryption.](http://pro-vladimir.livejournal.com/151301.html)

- [Рассматриваем основание Замка святого ангела.](http://pro-vladimir.livejournal.com/145337.html) English title: [We consider the foundation of the Castle of St. Angelo.](http://pro-vladimir.livejournal.com/145337.html)

- [А каких волков кормите вы?](http://pro-vladimir.livejournal.com/166718.html) English title: [What kind of wolves do you feed?](http://pro-vladimir.livejournal.com/166718.html)

© All rights reserved. The original author retains ownership and rights.





<!-- Original list reserved below in case the above misses any

#### Original site links

Kept for reference to check completeness of translated versions above

- [http://pro-vladimir.livejournal.com/994.html based on "And there was a Country before Russia»](http://pro-vladimir.livejournal.com/994.html)
- [http://pro-vladimir.livejournal.com/1134.html based on "And there was a Country before Russia". Part 2.](http://pro-vladimir.livejournal.com/1134.html)
- [http://pro-vladimir.livejournal.com/1376.html based on "And there was a Country before Russia". Part 3.](http://pro-vladimir.livejournal.com/1376.html)
- [http://pro-vladimir.livejournal.com/1710.html based on "And there was a Country before Russia". Part 4.](http://pro-vladimir.livejournal.com/1710.html)
- [http://pro-vladimir.livejournal.com/1813.html based on "And there was a Country before Russia". Part 5.](http://pro-vladimir.livejournal.com/1813.html)
- [http://pro-vladimir.livejournal.com/2301.html based on "And there was a Country before Russia" Part 6.](http://pro-vladimir.livejournal.com/2301.html)
- [http://pro-vladimir.livejournal.com/2330.html based on "And there was a Country before Russia". Part 7.](http://pro-vladimir.livejournal.com/2330.html)
- [http://pro-vladimir.livejournal.com/2602.html the meaning of the symbols. part 1.](http://pro-vladimir.livejournal.com/2602.html)
- [http://pro-vladimir.livejournal.com/2998.html Fabrications about the gaze.](http://pro-vladimir.livejournal.com/2998.html)
- [http://pro-vladimir.livejournal.com/3405.html Electrical equipment of the past.](http://pro-vladimir.livejournal.com/3405.html)
[http://pro-vladimir.livejournal.com/3728.html People, such people.](http://pro-vladimir.livejournal.com/3728.html)
[http://pro-vladimir.livejournal.com/3857.html based on "And there was a Country before Russia". Part 8.](http://pro-vladimir.livejournal.com/3857.html)
[http://pro-vladimir.livejournal.com/4416.html who is Volt? or Electrical equipment of the past. Part 2.](http://pro-vladimir.livejournal.com/4416.html)
[http://pro-vladimir.livejournal.com/5472.html Jewelry or spare parts?](http://pro-vladimir.livejournal.com/5472.html)

# Unformatted below
http://pro-vladimir.livejournal.com/5781.html ARCH-VOLT
http://pro-vladimir.livejournal.com/6435.html Transport systems of the past.
http://pro-vladimir.livejournal.com/6728.html Transport systems of the past. Part 2.
http://pro-vladimir.livejournal.com/7567.html Architecture= AZ-RH-Te-K-To-U-RA
http://pro-vladimir.livejournal.com/7994.html Two extremes.
http://pro-vladimir.livejournal.com/8369.html Who built St. Petersburg?
http://pro-vladimir.livejournal.com/8535.html based on "And there was a Country before Russia". Part 9.
http://pro-vladimir.livejournal.com/8801.html based on "And there was a Country before Russia". Part 10.
http://pro-vladimir.livejournal.com/9239.html based on "And there was a Country before Russia". Part 11.1
http://pro-vladimir.livejournal.com/9704.html based on "And there was a Country before Russia". Part 11.2
http://pro-vladimir.livejournal.com/11316.html Transport systems of the past. Part 3.
http://pro-vladimir.livejournal.com/11651.html air defense =Great wall of China
http://pro-vladimir.livejournal.com/12001.html just a little more, just a little more.
http://pro-vladimir.livejournal.com/15315.html Transport systems of the past. Part 4.
http://pro-vladimir.livejournal.com/16086.html the Janibekov Effect
http://pro-vladimir.livejournal.com/16890.html Transport systems of the past. Part 5.
http://pro-vladimir.livejournal.com/19699.html Architecture= AZ-RH-Te-K-To-U-RA. Part 2.
http://pro-vladimir.livejournal.com/20182.html Transport systems of the past. Part 6.
http://pro-vladimir.livejournal.com/24561.html Bronze and DVD, what can they have in common?
http://pro-vladimir.livejournal.com/26142.html Architecture= AZ-RH-Te-K-To-U-RA. Part 3.
http://pro-vladimir.livejournal.com/27053.html About the gold.
http://pro-vladimir.livejournal.com/27608.html Transport systems of the past. Part 7.
http://pro-vladimir.livejournal.com/28598.html the Media of the past.
http://pro-vladimir.livejournal.com/28918.html the Media of the past. Part 2.
http://pro-vladimir.livejournal.com/29127.html the Media of the past. Part 3.
http://pro-vladimir.livejournal.com/29387.html the Media of the past. Part 4.
http://pro-vladimir.livejournal.com/29661.html the Media of the past. Part 5.
http://pro-vladimir.livejournal.com/31497.html Architecture= AZ-RH-Te-K-To-U-RA. Part 4.
http://pro-vladimir.livejournal.com/33597.html based on "And there was a Country before Russia". Part 12. or Architecture= AZ-RH-Te-K-To-U-RA. Part 5.
http://pro-vladimir.livejournal.com/35497.html based on the text "about the Planet".
http://pro-vladimir.livejournal.com/38761.html do we know everything about the sun?
http://pro-vladimir.livejournal.com/40121.html Electrical equipment of the past. Part 3.
http://pro-vladimir.livejournal.com/43333.html the City of Petra.
http://pro-vladimir.livejournal.com/50137.html about... MONEY
http://pro-vladimir.livejournal.com/51165.html What a microwave can do! High-voltage
http://pro-vladimir.livejournal.com/51221.html the Sun
http://pro-vladimir.livejournal.com/52718.html flipping Through old maps
http://pro-vladimir.livejournal.com/53401.html Transport systems of the past. Part 8.
http://pro-vladimir.livejournal.com/53690.html the Papal cross? Or is it not a cross at all?
http://pro-vladimir.livejournal.com/53778.html Transport systems of the past. Part 9.
http://pro-vladimir.livejournal.com/54023.html Electrical equipment of the past. Part 4.
http://pro-vladimir.livejournal.com/54721.html Electrical equipment of the past. Part 5.
http://pro-vladimir.livejournal.com/56021.html Electrical equipment of the past. Part 6.
http://pro-vladimir.livejournal.com/58553.html Glasses.
http://pro-vladimir.livejournal.com/62595.html flipping Through old tracts. Part 1.
http://pro-vladimir.livejournal.com/62933.html flipping Through old tracts. Part 2.
http://pro-vladimir.livejournal.com/63661.html flipping Through old tracts. Part 3.
http://pro-vladimir.livejournal.com/64305.html flipping Through old tracts. Part 4.
http://pro-vladimir.livejournal.com/64593.html flipping Through old tracts. Part 5.
http://pro-vladimir.livejournal.com/65237.html flipping Through old tracts. Part 6.
http://pro-vladimir.livejournal.com/66325.html Architecture= AZ-RH-Te-K-To-U-RA. Part 6.
http://pro-vladimir.livejournal.com/66591.html flipping Through old tracts. Part 7.
http://pro-vladimir.livejournal.com/67047.html flipping Through old tracts. Part 8.
http://pro-vladimir.livejournal.com/67444.html Clairaudience and not only.
http://pro-vladimir.livejournal.com/68718.html based on "And there was a Country before Russia. Part 13"
http://pro-vladimir.livejournal.com/69373.html Transport systems of the past. Part 10.
http://pro-vladimir.livejournal.com/70313.html based on "And there was a Country before Russia". Part 14
http://pro-vladimir.livejournal.com/70477.html flipping Through old tracts. Part 9.
http://pro-vladimir.livejournal.com/71020.html flipping Through old tracts. Part 10.
http://pro-vladimir.livejournal.com/72882.html based on "And there was a Country before Russia". Part 15
http://pro-vladimir.livejournal.com/73396.html based on "And there was a Country before Russia". Part 16.
http://pro-vladimir.livejournal.com/74999.html based on "And there was a Country before Russia". Part 17.
http://pro-vladimir.livejournal.com/75407.html flipping Through old tracts. Part 11.
http://pro-vladimir.livejournal.com/75663.html flipping Through old tracts. Part 12.
http://pro-vladimir.livejournal.com/75919.html flipping Through old tracts. Part 13.
http://pro-vladimir.livejournal.com/76791.html flipping Through old tracts. Part 13. or Transport systems of the past. Part 11.
http://pro-vladimir.livejournal.com/78168.html based on "And there was a Country before Russia". Part 18.
http://pro-vladimir.livejournal.com/78712.html inspection of the "cannon" barrels.
http://pro-vladimir.livejournal.com/79143.html flipping Through old tracts. Part 14.
http://pro-vladimir.livejournal.com/79798.html are We ready to accept the truth?
http://pro-vladimir.livejournal.com/80291.html flipping Through old tracts. Part 15.
http://pro-vladimir.livejournal.com/80391.html the library of Alexandria.
http://pro-vladimir.livejournal.com/81340.html a mixture of a bulldog and a rhinoceros.
http://pro-vladimir.livejournal.com/82646.html flipping Through old tracts. Part 16.
http://pro-vladimir.livejournal.com/82696.html Astronomical and geodetic instruments
http://pro-vladimir.livejournal.com/82962.html the library of Alexandria. part 2.
http://pro-vladimir.livejournal.com/83435.html the keys to the portal?
http://pro-vladimir.livejournal.com/84130.html Conversation. A and O.
http://pro-vladimir.livejournal.com/84296.html "magneto-Optical holography is already being used for three-dimensional information storage"
http://pro-vladimir.livejournal.com/84535.html library of Alexandria. Part 3.
http://pro-vladimir.livejournal.com/85261.html How to make a magnetron out of a bell tower. Manual for dummies.
http://pro-vladimir.livejournal.com/85825.html inspection of the "cannon" barrels. Part 2.
http://pro-vladimir.livejournal.com/87019.html can all pipes throw cannon balls with gunpowder?
http://pro-vladimir.livejournal.com/87086.html Transport systems of the future.
http://pro-vladimir.livejournal.com/87427.html Icons, you say?
http://pro-vladimir.livejournal.com/87729.html Who built St. Petersburg? 2.
http://pro-vladimir.livejournal.com/87962.html a crack in the looking Glass.
http://pro-vladimir.livejournal.com/88394.html Steel ties and lattices of temples.
http://pro-vladimir.livejournal.com/88587.html Steel ties and lattices of temples. Part 2.
http://pro-vladimir.livejournal.com/89622.html Steel ties and lattices of temples. Part 2. +comments
http://pro-vladimir.livejournal.com/89982.html Steel ties and lattices of temples. Part 3.
http://pro-vladimir.livejournal.com/91750.html Steel ties and lattices of temples. Part 4.
http://pro-vladimir.livejournal.com/92442.html Steel ties and lattices of temples. Part 5.
http://pro-vladimir.livejournal.com/92685.html an Indication or system for recording past dates.
http://pro-vladimir.livejournal.com/93652.html Architecture= AZ-RH-Te-K-To-U-RA. Part 7.
http://pro-vladimir.livejournal.com/94813.html an Indication or system for recording past dates. Part 2.
http://pro-vladimir.livejournal.com/95296.html time Machine invented and hidden by the Vatican - Italian monk's Chronovisor
http://pro-vladimir.livejournal.com/95866.html an Indication or system for recording past dates. Part 3.
http://pro-vladimir.livejournal.com/96138.html the Measurement of the world. Magnetic planetarium.
http://pro-vladimir.livejournal.com/96384.html Gaston Plante electric fountains
http://pro-vladimir.livejournal.com/97018.html Electrical equipment of the past. Part 7.
http://pro-vladimir.livejournal.com/97024.html the all-seeing eye say?
http://pro-vladimir.livejournal.com/97024.html the all-seeing eye say?
http://pro-vladimir.livejournal.com/97821.html Architecture= AZ-RH-Te-K-To-U-RA. Part 8.
http://pro-vladimir.livejournal.com/98345.html Architecture= AZ-RH-Te-K-To-U-RA. Part 9.
http://pro-vladimir.livejournal.com/98972.html Architecture= AZ-RH-Te-K-To-U-RA. Part 10.
http://pro-vladimir.livejournal.com/99272.html Electrical equipment of the past. Part 8.
http://pro-vladimir.livejournal.com/99908.html Steel ties and lattices of temples. Part 6.
http://pro-vladimir.livejournal.com/100791.html Programmable machines of the past.
http://pro-vladimir.livejournal.com/101232.html Wave effect.
http://pro-vladimir.livejournal.com/102106.html Path.
http://pro-vladimir.livejournal.com/102358.html Programmable machines of the past. Part 2. Wave action.
http://pro-vladimir.livejournal.com/102461.html Programmable machines of the past. Part 3. Wave action.
http://pro-vladimir.livejournal.com/102680.html a load-grabbing device from the past.
http://pro-vladimir.livejournal.com/103253.html the Sun
http://pro-vladimir.livejournal.com/104159.html Wave impact and a bit about the Kremlin.
http://pro-vladimir.livejournal.com/104489.html Wave action. Part 4. Bronze cannons.
http://pro-vladimir.livejournal.com/105407.html N. Witsen Northern and Eastern Tartary
http://pro-vladimir.livejournal.com/105511.html old Printed books.
http://pro-vladimir.livejournal.com/105902.html Electrical equipment of the past. Part 9

http://pro-vladimir.livejournal.com/106967.html Electrical equipment of the past. Part 10.
http://pro-vladimir.livejournal.com/107457.html flipping Through old maps.2.
http://pro-vladimir.livejournal.com/107740.html Giants with books
http://pro-vladimir.livejournal.com/107878.html the Book of giants
http://pro-vladimir.livejournal.com/108105.html flipping Through old maps.3.
http://pro-vladimir.livejournal.com/109391.html post
http://pro-vladimir.livejournal.com/109820.html the Kremlin, the Indiction and cannons.
http://pro-vladimir.livejournal.com/110349.html use of atmospheric electricity in the past+comments
http://pro-vladimir.livejournal.com/110762.html regiomontanus calendarium
http://pro-vladimir.livejournal.com/110852.html public execution of a fair-haired beauty, or that's what you are, blue blood!+comments
http://pro-vladimir.livejournal.com/112677.html Staff: an etymological perspective.+comments
http://pro-vladimir.livejournal.com/112919.html Transport systems of the past. Part 12.
http://pro-vladimir.livejournal.com/113318.html Electronic libraries.
http://pro-vladimir.livejournal.com/113415.html the Kremlin, the Indiction and cannons. Part 2.
http://pro-vladimir.livejournal.com/114163.html a little more about the Kremlin.
http://pro-vladimir.livejournal.com/115420.html "Pawned"...extra holes from drafts.
http://pro-vladimir.livejournal.com/115789.html Primary sources.
http://pro-vladimir.livejournal.com/116073.html a Little about how Russia was dug up.
http://pro-vladimir.livejournal.com/116421.html the Indiction in the marks on the cards.
http://pro-vladimir.livejournal.com/116921.html the Indiction in the marks on the cards. Part 2.
http://pro-vladimir.livejournal.com/117837.html physics of whirlpools. Video.
http://pro-vladimir.livejournal.com/119733.html Transport systems of the past. Part 13.
http://pro-vladimir.livejournal.com/120066.html the Indiction in the marks on the instruments.
http://pro-vladimir.livejournal.com/120756.html Where the floppy disk went.
http://pro-vladimir.livejournal.com/120947.html the Indiction in the marks on the instruments. Discussion.
http://pro-vladimir.livejournal.com/122852.html a little more about the indict.
http://pro-vladimir.livejournal.com/123333.html Transport systems of the past. Part 14.
http://pro-vladimir.livejournal.com/123642.html Steel ties and lattices of temples. Part 7.
http://pro-vladimir.livejournal.com/124566.html chopped And kneaded.
http://pro-vladimir.livejournal.com/124969.html A little bit about the technologies of the past.
http://pro-vladimir.livejournal.com/125334.html Transport systems of the past. Part 15.
http://pro-vladimir.livejournal.com/126700.html Primary sources. Maps, books, ....
http://pro-vladimir.livejournal.com/127037.html where calendars lead.
http://pro-vladimir.livejournal.com/128296.html deeper and Wider.
http://pro-vladimir.livejournal.com/129033.html Electrical equipment of the past. Part 11.
http://pro-vladimir.livejournal.com/129606.html based on-Pyramids, deities and gods
http://pro-vladimir.livejournal.com/129831.html First-the Gods. Statues, images.
http://pro-vladimir.livejournal.com/130063.html Riddles of history
http://pro-vladimir.livejournal.com/130473.html Transport systems of the past. Part 16.
http://pro-vladimir.livejournal.com/130688.html Transport systems of the past. Part 17.
http://pro-vladimir.livejournal.com/130939.html Transport systems of the past. Part 18.
http://pro-vladimir.livejournal.com/131447.html flipping Through old tracts.
http://pro-vladimir.livejournal.com/132473.html the domes are burning. The beavers built a dam.
http://pro-vladimir.livejournal.com/132608.html the domes are burning. The beavers built a dam. Part 2.
http://pro-vladimir.livejournal.com/133223.html does the Pagoda have an electric separator?
http://pro-vladimir.livejournal.com/133428.html Can you tell me how to get to the library of Ivan the terrible?
http://pro-vladimir.livejournal.com/134374.html the Field of miracles. Spin the drum.
http://pro-vladimir.livejournal.com/134955.html A crater?
http://pro-vladimir.livejournal.com/135302.html the Field of miracles. Spin the drum. Part 2.
http://pro-vladimir.livejournal.com/136826.html old Russian maps of the 17th and early 18th century .
http://pro-vladimir.livejournal.com/137458.html based on "old Russian maps of the 17th and early 18th Century"
http://pro-vladimir.livejournal.com/139252.html in this way, you can "accumulate" a very large charge.
http://pro-vladimir.livejournal.com/139551.html an Automaton
http://pro-vladimir.livejournal.com/141009.html Summary of the cannons. Part 1.
http://pro-vladimir.livejournal.com/141150.html Summary of the cannons. Part 2.
http://pro-vladimir.livejournal.com/141381.html Summary of the cannons. Part 3.
http://pro-vladimir.livejournal.com/141703.html Summary of the cannons. Part 4.
http://pro-vladimir.livejournal.com/141931.html Summary of the cannons. Part 5.
http://pro-vladimir.livejournal.com/142299.html Summary of the cannons. Part 6.
http://pro-vladimir.livejournal.com/142494.html Summary of the cannons. Part 7.
http://pro-vladimir.livejournal.com/142826.html Summary of the cannons. Part 8.
http://pro-vladimir.livejournal.com/142872.html Summary of the cannons. Part 9.
http://pro-vladimir.livejournal.com/143229.html Summary of the cannons. Part 10.
http://pro-vladimir.livejournal.com/143397.html Summary of the cannons. Part 11.
http://pro-vladimir.livejournal.com/143682.html Summary of the cannons. Part 12.
http://pro-vladimir.livejournal.com/144318.html Part of an "architectural" element or a cannon?
http://pro-vladimir.livejournal.com/144480.html Electrostatic generator in images of the past.
http://pro-vladimir.livejournal.com/144978.html based on "Secrets of past civilizations 3"
http://pro-vladimir.livejournal.com/145337.html we Consider the Foundation of the "Castle of St. Angelo".
http://pro-vladimir.livejournal.com/145717.html malachite cryptography of the Ural Masters
http://pro-vladimir.livejournal.com/146399.html Ipsissimus
http://pro-vladimir.livejournal.com/146658.html And if you "try" the Tsar cannon?
http://pro-vladimir.livejournal.com/146938.html My husband returns from a business trip. Opens the closet, and there's Narnia.(C)
http://pro-vladimir.livejournal.com/147501.html Asked-answer.
http://pro-vladimir.livejournal.com/147406.html Why was the column called a column?
http://pro-vladimir.livejournal.com/147883.html Transport systems of the past. Part 19.
http://pro-vladimir.livejournal.com/148390.html AVM (analog computing machine)
http://pro-vladimir.livejournal.com/149733.html a medieval Giga-video camera?
http://pro-vladimir.livejournal.com/149831.html Crystal crosses, hyssop and other Church crystal.
http://pro-vladimir.livejournal.com/151102.html how Far did the Giants hide?
http://pro-vladimir.livejournal.com/151301.html "Voynich's Primer" - the first steps of decryption.
http://pro-vladimir.livejournal.com/152035.html people of silicon bodies
http://pro-vladimir.livejournal.com/152376.html the strangeness of masonry.
http://pro-vladimir.livejournal.com/152818.html flipping Through old tracts.
http://pro-vladimir.livejournal.com/152911.html Why?
http://pro-vladimir.livejournal.com/153137.html the Subtypes of "artillery".
http://pro-vladimir.livejournal.com/153772.html Grocery + review
http://pro-vladimir.livejournal.com/153981.html commentaries on "Levitation and temples"
http://pro-vladimir.livejournal.com/154114.html Destroyed the "stars". Transport systems of the past. Part 20.
http://pro-vladimir.livejournal.com/154764.html 3D Printing, before the invention of 3D printing.
http://pro-vladimir.livejournal.com/157002.html Codex SERAPHINIANUS - one page literally.
http://pro-vladimir.livejournal.com/158441.html what's wrong with the finials?
http://pro-vladimir.livejournal.com/158612.html forgotten temples of the land of Tula (part IV - along The old Kashirsky tract)+comments.
http://pro-vladimir.livejournal.com/158810.html breakdown in dimensions. What do you think the portal should look like?
http://pro-vladimir.livejournal.com/160753.html Transport systems of the past. Part 21.
http://pro-vladimir.livejournal.com/161754.html Electronic resources
http://pro-vladimir.livejournal.com/161850.html Thoughts. How long ago was the last time the world's parameters changed?
http://pro-vladimir.livejournal.com/162297.html Peter's Bot.
http://pro-vladimir.livejournal.com/162950.html Transport systems of the past. Part 22.
http://pro-vladimir.livejournal.com/163318.html planet.
http://pro-vladimir.livejournal.com/163371.html are There too many ions in the past?
http://pro-vladimir.livejournal.com/163371.html are There too many ions in the past?
http://pro-vladimir.livejournal.com/164242.html just columns, just ancient Egypt.
http://pro-vladimir.livejournal.com/164533.html Piggy Bank of steel ties.
http://pro-vladimir.livejournal.com/164891.html a door to Door service.
http://pro-vladimir.livejournal.com/165362.html the Axonometric plan of Saint - Petersburg
http://pro-vladimir.livejournal.com/166079.html Transport systems of the past. Part 23.
http://pro-vladimir.livejournal.com/166718.html What kind of wolves do you feed?
http://pro-vladimir.livejournal.com/166966.html Transport systems of the past. Part 24-1.
http://pro-vladimir.livejournal.com/167410.html Transport systems of the past. Part 24-2.
http://pro-vladimir.livejournal.com/167487.html Transport systems of the past. Part 24-3.
http://pro-vladimir.livejournal.com/168365.html ruins of the ancient city of Merv + comments.
http://pro-vladimir.livejournal.com/168547.html a defect that has been waiting for decades + comments.
http://pro-vladimir.livejournal.com/169144.html Transport systems of the past. Part 25.
http://pro-vladimir.livejournal.com/169413.html From what I found yesterday.
http://pro-vladimir.livejournal.com/170152.html catching forgers.
http://pro-vladimir.livejournal.com/170335.html Transport systems of the past. Part 26. Multidimensionality, where are you?
http://pro-vladimir.livejournal.com/170836.html multi-Dimensionality. From the discussion.
http://pro-vladimir.livejournal.com/171092.html catching forgers. Part 2.
http://pro-vladimir.livejournal.com/171466.html "Up" ("Up»)
http://pro-vladimir.livejournal.com/172001.html Primary sources. Read not reread.
http://pro-vladimir.livejournal.com/172454.html And one in the field warrior. Or...+comments
http://pro-vladimir.livejournal.com/172919.html the Depressurization.
http://pro-vladimir.livejournal.com/173377.html use of force fields in the past.
http://pro-vladimir.livejournal.com/173673.html Moscow Baroque in the Kaluga wilderness or Kazan Church in Bogorodskoye+comments
http://pro-vladimir.livejournal.com/173831.html Transport systems of the past. Part 27. Multidimensionality, where are you?
http://pro-vladimir.livejournal.com/174135.html the Vatican Library
http://pro-vladimir.livejournal.com/174713.html catching forgers. Part 3.
http://pro-vladimir.livejournal.com/174987.html from Bogdanovsky to Vislyaevo or forgotten temples of the Ferzikovsky district +comments.
http://pro-vladimir.livejournal.com/176141.html Transport systems of the past. Part 28.
http://pro-vladimir.livejournal.com/177222.html how to approach the "Transport systems of the past".
http://pro-vladimir.livejournal.com/177542.html how to approach the "Transport systems of the past". Part 2.
http://pro-vladimir.livejournal.com/178465.html the largest casino on the planet is MMM America LLC. Amen.
http://pro-vladimir.livejournal.com/178758.html Transport systems of the past. Part 29
http://pro-vladimir.livejournal.com/179038.html Electricity is electric
http://pro-vladimir.livejournal.com/179351.html There is no current in the wire. How not?
http://pro-vladimir.livejournal.com/179838.html bell tower principle
http://pro-vladimir.livejournal.com/180094.html Everything is hidden
http://pro-vladimir.livejournal.com/181574.html Transport systems of the past. Part 30
http://pro-vladimir.livejournal.com/182139.html And who is this "little one"?
http://pro-vladimir.livejournal.com/182745.html mysterious Golden spirals of the bronze age
http://pro-vladimir.livejournal.com/182883.html Painting.
-->
