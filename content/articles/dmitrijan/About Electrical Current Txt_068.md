title: About Electrical Current
date: 2013-10-22
modified: Thu 14 Jan 2021 07:23:05 GMT
category: 
tags: technology
slug: 
authors: Dmitry Anatolyevich
summary: But what if the adults aren't as correct as they seemed? Can a possible misconception from more than 100 years ago still be the basis of our knowledge?
status: published
originally: Txt_068.txt and https://dmitrijan.livejournal.com/90034.html

### Translated from:

Txt_068.txt and [https://dmitrijan.livejournal.com/90034.html](https://dmitrijan.livejournal.com/90034.html)

Translated text has not been reviewed.

[https://youtu.be/72LWr7BU8Ao](https://youtu.be/72LWr7BU8Ao)

*Fun with Vortex Rings in the Pool*

There are interesting phenomenon like current and charges.

Our life is surrounded by such phenomena as current and charges. We are so used to them that we don't notice them. Even more - many believe that this is the fate of individual subjects from science, and ordinary people do not need it, but, despite this belief, nothing prevents them from using all this in everyday life, clearly and not explicitly meeting with this phenomenon constantly.

Since the time of Edison, Faraday, Volt, Maxwell, Tesla, etc, this phenomenon has remained studied at the level of those times. Postulates were laid down according to the concepts of that time and were a consequence of the development of mechanics. All subsequent generations believed that the postulates put forward then - and beautifully confirmed by experiments at the same time - are indisputable facts and evidence of the core dogmas of today's electrical and electronics engineering. The experiments are not complicated, they are studied at school and therefore most do not cause doubts. At that golden age, most of what adults say seems to be true. And in the future, it has already become the basis of personal knowledge, which many people do not want to review.

But what if the adults aren't as correct as they seemed? What if they themselves also once considered what their elders told them to be true? Is it possible that a misconception from more than 100 years ago might still be the basis of our knowledge? It is no secret that some inquisitive minds still found some nuances in school experiments, but were strictly suppressed by the pressure of authorities. It is even known that Tesla 100 years ago tried to show in experiments that the postulates are not as firm as it seems, but it is easier and calmer for society to believe in what the founders said than it is to think. And so it is calmer. Yes, and Einstein arrived with a theory in time. So many people couldn't be wrong, could they?

But this is a lyrical digression.

And so, what do we have? Let's look at the facts.

The realm of electricity, as you know, is divided, by analogy with mechanics, into three areas: 

- static, charges and static electricity; dynamics of uniform motion

- constant current; dynamics of motion with acceleration

- alternating current.

### Statics.

It is considered to be its base charges and here a bright representative - the electron. They are material, but we were not able to physically catch them and this is explained by their very small size. So small that they can't be seen even with an electron microscope. And this whole section is based on this postulate.

Basic effects. The root cause of charge separation can be produced by friction. Charges accumulate on the surface of dielectrics. Charges of the same type (polarity) repel each other. Charges flow from dielectrics along conductors. Accumulating a lot of charge can produce a spark (discharge). Measurement is performed by using the charge interaction method.

The application of statics is huge, as in the future:

["About Statistics"](http://dmitrijan.livejournal.com/97134.html)

["about Statolets"](http://dmitrijan.livejournal.com/104006.html)

and in the past:

["Ivan and Mary"](http://dmitrijan.livejournal.com/91508.html)

["use of atmospheric electricity in the past"](http://sibved.livejournal.com/158087.html)

["Electrical equipment of the past"](http://pro-vladimir.livejournal.com/3405.html)

Well, also without a visual theory (clickable):

<!-- Local
[![](Txt_068_23112006_Про ток - Едины во всём - Едины всегда. — LiveJournal_files/27345_600.jpg#clickable)](Txt_068_23112006_Про ток - Едины во всём - Едины всегда. — LiveJournal_files/27345_original.jpg)
-->

[![](https://ic.pics.livejournal.com/dmitrijan/42576892/27345/27345_600.jpg#clickable)](http://ic.pics.livejournal.com/dmitrijan/42576892/27345/27345_original.jpg)

And where without a description, were many beeches ["eh Troya"](http://dmitrijan.livejournal.com/113831.html)

### Current.

It is generally assumed, by analogy with mechanics, that its carrier is an electron, a small material object, very nimble and so small that it is not even visible in an electron microscope, but only as a cloud of movement. Let's think about that - in an electron microscope! That is, a system that is built on a postulate about the electron, cannot see a part of the subject of research, but only a cloud! How can this be?

How many times have you tried to find this elusive electron inside the cloud and everything is useless! We didn't find it, but searched, oh how we searched! And the assumption once made long ago has turned out to be so tenacious in the minds that until now, with all our modern achievements, it seems to many to be real. Moreover, all new phenomena in this area attract to this postulate! Thus appeared dualism in the behavior of the electron, and so appeared quanta. And, despite all these patches in theory, its basis is considered correct! Moreover, the case with the measured speed of the electron (slow) and the speed of current (fast) was explained simply by the transfer of interaction by electrons pushing each other in a conductor! And even that mishap, that current and did not think to go in the material of the conductor, but only on its surface did not confuse anybody. It is simply accepted, as being true.

The current is constant. The root cause is a potential difference. Potential, ie charge separation, occurs through a chemical reaction producing electromagnetic force (EMF). The current "moves" through the conductor. The current does not pass through the dielectric. Charges accumulate on the dielectric (capacitor). A good potential will create a spark (discharge). The current heats up the conductor. When it passes through, it creates a field, which is commonly referred to as magnetic, for magnets react to it. Measurement of current is performed by measuring its interaction with a magnetic field.

Variable current. Source - EMF or periodic fluctuating process generator, such as an LC oscillating circuit. Current "moves" through the conductor. It does not pass through the dielectric, despite the fact that it passes through the capacitor. It can heat up a conductor. Can create spark (discharge) potential. Has a phase bites eerily, though no one has caught it. Measurements are made by its interaction on the magnetic field just as direct current is also measured. It has a characteristic frequency of change (oscillations). At their limits, oscillations are similar to direct current.

In short, it seems to be everything.

Will we think about what we see? Do they really look so similar? And really, where is the difference? Actually, what **is** the difference? All phenomena require some kind of action to manifest. They use conductors for spatial interaction, but they cannot use dielectrics. They always sit on the surface. They heat up conductors. They create a spark. They are measured by magnetic or static methods. In fact, all the effects we can derive about them are pure speculation. Can we be confused by some difference in measurement and representation method? So it's not a problem - let's remember that we can accumulate a static charge in a Leiden jar (capacitor) and then nothing prevents us from connecting a circuit to it and a miracle! We can see all the signs of current! We can even test in the usual way. And the qualitative difference between DC and AC current? It's only in frequency, as far as we know. I.e. direct current is either a very fast alternating current or alternating current is a very slow direct current. Or direct current is a special case of an alternating current at a certain stage of phase change. One does not interfere with the other at all. Only confirming that they are different facets of one phenomenon.

And what do we see? For so many years science has been creating different formulas, calculation methods, methods, substantiations, approaches to the same phenomenon in different species purely on the basis of a division of related phenomena suggested more than 100 years ago?! How can it be so? Is an elephant a snake trunk or four legs of a table? Would it be more logical to consider it as different effects on the same environment? We do not see or feel it? After all, the air was once considered too loose to lift planes! Until there were those who refuted it, but even now we can find subjects who do not believe that an airplane can fly and not flap its wings.

Human nature, however, is convinced that there are only things that can be touched. This is the essence of this mishap! And a century ago, Tesla was showing experiments with electricity, explaining them by the presence of air (aether?) around us, an ephemeral world! He was convincingly proving, with examples, on models. And people did what? And people believed the authorities of the time, not the young man who was showing strange experiences. And only the problems with DC transmission over long distances forced them to use alternating current, contrary to their faith! But even today, it contradicts the basic concepts of current. But they put up with it. Because it exists and is used, they got used to it.

So it turns out that for some unknown reason, having a lot of facts, conducting experiments, building new things, civilization - in some not so trivial moments as electricity - is built upon an initially incorrect foundation. And everything created is not the essence of knowledge of the phenomenon, but only a description of the consequences obtained experimentally. Approximately, it's like determining the operation of a car engine not by examining the engine but by examining the exhaust left behind after the car has passed. Strange, but that is how it is.

For clarity, let's draw a picture of the electric world using a simple and slightly exaggerated example. Imagine an infinitely large surface, say something like a pier, under which there is water. But the surface is not just monolithic, it has some holes through to the water. Let's say at distances of about 10 meters apart. But this is not enough; this "pier" surface is covered with a flexible material (say, a tarpaulin) and therefore we do not see water. We don't even know that it exists, because we see a solid covering of tarpaulin.

However, at some point in time, moving on the surface of such a space, the experimenter accidentally discovers that if a weight is placed on the surface, then under certain conditions, a miracle occurs in some places - the weight sinks! But this is not enough: the dip does not sink very far. He puts more load on it - and it sinks lower, but at the same time the first load rises. We conclude that a lot of weight in one place causes the surface to rise in another place. What's next? Further, this effect is written as a postulate that this surface is nothing more than a charge, which can have the sign "-" (electron) or "+" (hole, positron). How to measure it? The height of the column is the number of charges. So the doctrine of static electricity was born.

But time did not stand still, and his attentive gaze noticed that there are areas where there is not just a rise in the surface, but a certain path appears! Oh, says the researcher, yes, it's a current! And it consists of many small solid electrons that move and can't be seen because they are so fast. And we'll call the path a guide - it's their conductor. And the rest of the space will be a dielectric – after all, there is no current passing through it. This is how the doctrine of direct current appeared.

For practical use, he "invents" a ruler that can be placed next to one of the little hillocks and measures the voltage by its height. And how does he measure the current? To do that, he puts a load on the conductor. The flatter the slope, the less the current. Immediately this produces the fundamental law for direct current (Ohm's law) - the dependence of current, voltage and resistance (weight of the load). He develops a method for transmitting signals over a distance: place one weight, a dot... place two weights, a dash. Thus the telegraph (Morse code).

As time passed, the experimenter discovered that if you do not just put stones, but periodically press the load on this sink-place, or jump on it yourself - then the same thing happens in another place! And if you jump with a certain frequency, you can jump higher than the source! Oh! says the experimenter - resonance! And if you jump at the beginning of the conductor track, then a wave will run along the track! And what is immediately noticed is that reaching the end of the track, this wave bounces back to the jumper. A reversing wave! And this phenomenon is called alternating current. The flow of the wave back and forth from obstacle to obstacle with the increase of the wave is called resonance, and the system is called Inductance-Capacitance (LC) contour.

But this was not enough. The wave does not just hit the end of the track, but sometimes continues to move in another track, which is separated from the first by space (dielectric). Oh, says the researcher, alternating current can pass through dielectrics.

Also, during their experiments, researchers began to discover a strange phenomenon: that there are places where the wave does not just go in a straight line, but diverges in circles from the source! So radio communication was born.

What next? Then there was a lot of research. The electron continued to be considered solid, because it was more familiar, and new knowledge about waves was imputed to existing qualities as dualism - duality of properties. After all why abandon the postulates and rewrite everything previously written?

Later, some researchers wanted to see inside the particles that they could not see through an electron microscope, so they began to build big mass accelerators and smashers to penetrate the secrets of matter. And we found an interesting thing: if you first make a bulge (create a charge), and then hit it hard, then this bulge is not so monolithic, but is a lot of bulges. Though it is not long before they subside again. Oh! say the researchers - these bulges prove the birth of new types of elementary particles that are smaller than an electron. Look at the variety they have! But there is no limit to perfection, you want to know more, so you need to hit harder. And so that researchers can better understand the structure of matter, they need a more powerful device or a smasher. Perhaps they'll find out more!

What's all this about? Yes, actually it's that strange issue of investigating the world, where our civilization explores not the environment which creates such a phenomenon as electrical current, considering it the basis, but instead investigates the phenomena and effects of this environment separately. And, despite all the achievements, they are achieved only on the basis of purely experimental data in their database, without understanding the essence. How much time could have been saved during these 100-150 years if humanity had not been so materialistic and the authorities so stubborn in their desire to understand the world.

And besides Tesla, Maxwell went down this path and we have what we have. But such an understanding of the structure of the world would give awareness, moreover, confidence and even confirmation of the fact that communication and "transmission" of information in our world might be as far as we can go, and that energy might be obtained without spending material energy. Moreover, the formulas and calculations used today can be applied to such concepts, though you need to be able to understand the essence of the description of phenomena by formulas, and not just memorize them.

He who can think, let him understand.
[Txt_068_23112006_About current, Dmitrij]

Comments:

1.  Scope of laws
[ermolaevai](https://ermolaevai.livejournal.com/) 2013-10-23 08:50 am (UTC)
Thus, it turns out that all laws, rules, concepts, etc. have their own scope of applicability, which is limited to time, space, sphere and other metrics. This is true even if the laws and rules are based on erroneous postulates. The main problem arises when the postulates of laws begin to apply outside their field of operation.

Replies: 
    
    Because ZaCons act as a con game, so different. We do not play by laws of a chess in cards or on the contrary though we can.

    Rules and concepts are under construction as paradigms on the same laws.


2. [zanyks](https://zanyks.livejournal.com/) 2013-10-25 01:11 pm (UTC) 

:) Mda.

The electron is of course good, but it really has a mass of course, and now it is material. As you know, the valence state of matter depends on the orbit of the electron cloud. Substances with variable valence have several orbits of the flight of the electron, energy levels if shorter. In general, the state of transition from one orbit to another does not have electrons. It is either there or there. Or free. There is no average. Despite the speed of a wave it does not move. Simply either there or there. I do not know how scientists have defined it, but this conclusion is amazing. That is, even taking the corpuscular-wave dualism of the electron we cannot substantiate this phenomenon. Or maybe one disappears and another appears?

The movement of charged particles is considered a current, yes. But the movement of particles occurs along the entire length of the conductor. The electromagnetic field is responsible for this. The fields seem to be spreading at the speed of light. These things seem not to have been well studied. We don't feel matter, we feel any action through fields. In other words, the mass of matter is its gravitational field. The charge is its electromagnetic field. Electromagnetic wave is a change in the electromagnetic field of the receiving photocells (sensitive retinas, sensitive elements of devices like the Geiger counter, etc.). Moreover, the discovery of the finite mass of a quantum suggests that all this is somehow tricky.

All this is very interesting. However, it is only a bunch of truth manifestations, and there is no actual confirmation of anything yet. There may be a bunch of theories, hypotheses... and a lot of fantasies of people far from science. Such as I am, for example, in general, by education, in heat engineering. :)

It seems to me that when we understand what fields are, not through their manifestations, but in general, then science will move on. But in the meantime, so far, nothing prevents us from using them in everyday life.

This is an interesting article. Thank you.

Replies: 

    About valance [arendator1778](https://arendator1778.livejournal.com/) 2018-12-27 09:09 am (UTC) 

    Valence is complete nonsense... For example, take oxygen. What is its valence? And why is it in the 6th row of the valence group in the Mendeleev's table, etc?

    [dmitrijan](https://dmitrijan.livejournal.com/) 2013-10-28 11:25 am (UTC)
    the problem is that the mass of the electron is calculated.
    
    Here's the problem - the mass of the electron is calculated, as well as the Sun and other objects that are not available to us. Therefore, this is only a calculated mass, which can not talk about any materiality. We might as well calculate the mass of Mickey Mouse or Shrek.

And if we are torn off by a part of our body, through what fields can we feel this interaction with matter?

Will we understand everything or is there something specific? And if someone understands, then of course will everyone else move on at once?


3. [taylash](https://taylash.livejournal.com/) 2014-11-03 07:14 pm (UTC)

Tesla has seen this Wednesday, you, Dmitry, judging by the texts you see. I can see it, but the detail is low, maybe just the speed of the "processor" is not enough, to process in the right detail - I am working on it. Maybe you can throw in a couple of pictures, like your sketches?

The fact that you can use the properties of the environment to extract and transmit energy without wires, I saw for myself when my friends made the sketch. Obviously the environment has base frequencies and certain properties and you can use them, here's an example (you mentioned the link somewhere): [http://www.qrz.ru/schemes/contribute/antenns/eh/](http://www.qrz.ru/schemes/contribute/antenns/eh/). The characteristics there are interesting. Radio waves - it is clear that the ether waves, electric current - the movement of the wave. Here, with charges (pressure - discharge of the air?), semiconductors and dielectrics is already more complicated. Somehow, I haven't thought about this topic much yet, I thought to accelerate first, then to study in detail and to comprehend. In due time I entered the University of Control Systems and Radioelectronics for "microelectronics". I thought it would help me to better understand the world around me. When I heard the theory of relativity that the earth falls to the stone, only different reference systems, I thought it was nonsense. With the electrons in physics, chemistry, and electrical engineering, I had a habit of rolling (like with Christianity at first), but I heard about the "holes" - I already understood something not clean. Somehow, a "hole" from a bagel. I saw a lot of inconsistencies, like electrons running, the mass does not change.... It is not for nothing that I threw, without plunging deep into such a peculiar (_!_) dogma. I do not think that the bulk of the masses are ready for that knowledge, give the dill, for example, these technologies, the consequences will be much more deplorable. Therefore, by the way, I don't agree with one of your comments that Tesla tried to earn first, if Edison moved, he was quite successful. I'm not looking for ready-made answers in your texts, I'll be ready for them myself, but I thank you for this work.

Replies:

    [dmitrijan](https://dmitrijan.livejournal.com/) 2014-11-05 08:06 am (UTC)
    
    @Maybe you could throw in a couple of pictures, like your sketches?

    Easy (clickable): [![THROYA](https://ic.pics.livejournal.com/dmitrijan/42576892/27345/27345_600.jpg#clickable)](http://ic.pics.livejournal.com/dmitrijan/42576892/27345/27345_original.jpg)
    

4. Electricity [nikenait](https://nikenait.livejournal.com/) 2015-02-06 11:00 am (UTC)

Electricity. El(L)-ec(x,exe)-three(trinity)-quality. LX3, or LXXX

exe - self-extracting archive, XYZ (coordinate axes)

Triunity in one. Three x, ex-three (breakdown), three crosses.

El, Eloah, Ilu - creator of the world, father of many gods and all living things, master of time. Neither Ilu nor his wife rule the world, standing high above it. It was believed that the world is ruled by lower deities, who argue and fight, which were the temples.
Al's children: Baal Adad, Yam and Mott, who in Greek mythology are identified with Zeus, Poseidon and Hades respectively.

Electrounion is a rooster among the ancient Greeks. El-ek(X)-tron. L (energy, current) goes along the X axis to the throne (?).

Ice. El-Yod. LYO-d. On the XYZ coordinate system. If energy (L) passes through the (Y) axis to the point (0), it freezes space, crystallizes, " orders. U-col, U-jalit (Alexandria pole).

Alexander. Al-ex-andr. Android of LX series.

Kaba - kol (Dal's dictionary). De - kaba (disassembly of the pole). December. Decembrists. Perhaps in December, the poles in star forts dismantled for the winter.

XYZ. 0 - intersection point of coordinate axes. XOZ-farm, Chaos. Y - power vertical, control, perpendicular to XOZ plane through zero point.

Three crosses. Rembrandt etching 1653.

![](https://upload.wikimedia.org/wikipedia/commons/8/81/Rembrandt_The_Three_Crosses_1653.jpg#resized)

Olympus - reflection through point 0. Limb.

Limbus is a metal ring with evenly spaced strokes (divisions), an important part of angle tools.

Limb is the visible edge of the disk of the Moon, Sun or planet in projection on the celestial sphere.

The corneal limb (Blind Zone) is the place where the cornea meets the sclera, an area about 1 mm wide, rich in vessels feeding the cornea.

4. Limbus (lat. limbus - border, edge) - in Catholicism, the place of stay of souls who did not get to the Paradise of Souls, which does not coincide with hell or purgatory.


Yryryryry [necrosfodel](https://necrosfodel.livejournal.com/) 2015-02-27 02:57 pm (UTC)

The current starts to run on the surface of the conductor only at high frequencies of this very current. This is a known phenomenon, but it cannot be said that the current flows only through the surface under any conditions.

Re: Yryryryry [dmitrij an](https://dmitrij-an.livejournal.com/) 2015-04-01 05:53 pm (UTC)

Tested easily, but lazy people are stressed by that.


[antoxa 80](https://antoxa-80.livejournal.com/) 2016-04-04 07:53 pm (UTC)

I looked at Rybnikov Yuri Stepanovich and learned that there are no electrons. And there is ALL and Periodic system.

[karyoo](https://karyoo.livejournal.com/) 2018-08-16 12:14 am (UTC)

So what changes from what to call an alternating current - reflected DC? What is the incorrect division of currents? Both have their own applications: welding and transmission lines, for example.

[marta_inj](https://marta-inj.livejournal.com/) 2018-08-16 07:02 am (UTC) 

A new view of the theory makes it possible to create new devices with a different operating principle.

[karyoo](https://karyoo.livejournal.com/) 2018-08-16 12:15 am (UTC)

If to reason so, then in mechanics for 500 years, nothing new, F=ma, as it was and remains. However, everyone probably drives a car.

© All rights reserved. The original author retains ownership and rights.
