title: Inspired by "And There Was a Country Before Russia" - Part 12 onwards
date: 2014-06-30 00:00:02
modified: 2021-09-03 09:01:20
category: 
tags: cannons; technology; links
slug:
authors: Vladimir Mamzerev
summary: Inspired by "And there was a country before Russia" - Parts 12 to 18. Links to Google-translated English pages
status: published
local: 
originally: Inspired\ by\ And\ There\ Was\ a\ Country\ Before\ Russia\ -\ Part\ 12\ onwards.md


### Translated from:

[https://pro-vladimir.livejournal.com/106270.html](https://pro-vladimir.livejournal.com/106270.html)


[http://pro-vladimir.livejournal.com/33597.html Part 12 Architecture](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/33597.html)

[https://pro-vladimir.livejournal.com/68718.html Part 13 Bells and cannons](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/68718.html)

[https://pro-vladimir.livejournal.com/70313.html Part 14](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/70313.html)

[https://pro-vladimir.livejournal.com/72449.html Part 15](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/72449.html)

[https://pro-vladimir.livejournal.com/72882.html Part 15 Cannons edited](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/72882.html)

[https://pro-vladimir.livejournal.com/73396.html Part 16 (Leprosy is radiation sickness)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/73396.html)

[https://pro-vladimir.livejournal.com/74999.html Part 17 Cannons' real uses](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/74999.html)

[http://pro-vladimir.livejournal.com/78168.html Part 18](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/78168.html)

© All rights reserved. The original author retains ownership and rights.

