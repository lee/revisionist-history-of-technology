1
00:00:21,920 --> 00:00:23,400
Hello, fellows.

2
00:00:23,600 --> 00:00:25,960
I recently introduced you

3
00:00:26,160 --> 00:00:33,800
with traces of technology and a new order of us
that we've been observing in belbeck.

4
00:00:34,000 --> 00:00:39,680
after that, I got a call from alexei sergeyevich
Smith and reminded that on his page

5
00:00:39,880 --> 00:00:43,880
on facebook is very
nice collection of photos

6
00:00:44,080 --> 00:00:48,360
on which it is clearly possible
consider artificial

7
00:00:48,560 --> 00:00:54,200
the shafts are made exactly by the method
but just the lightweight method.

8
00:00:54,400 --> 00:00:57,240
and let's talk about it in this form.

9
00:00:57,440 --> 00:00:59,240
but before we get into the narrative with

10
00:00:59,440 --> 00:01:02,960
of new information, I traditionally thank
of all our associates

11
00:01:03,160 --> 00:01:08,320
that support us in our
in our endeavors, for your help to us

12
00:01:08,520 --> 00:01:10,080
is very important.

13
00:01:10,280 --> 00:01:14,040
as a result, we are conducting expeditions and

14
00:01:14,240 --> 00:01:18,280
sharing with you those observations
that we have.

15
00:01:19,360 --> 00:01:25,200
Alexei Kuznetsov's observation refers to
to a very interesting stretch of road.

16
00:01:25,400 --> 00:01:29,400
it's you coming up and practically

17
00:01:29,600 --> 00:01:31,800
you're coming to Ufa.

18
00:01:32,000 --> 00:01:35,200
the river's gonna be a big one.

19
00:01:35,400 --> 00:01:38,000
to the first concession

20
00:01:38,200 --> 00:01:42,360
at this location is
the hill begins the second ledge.

21
00:01:42,560 --> 00:01:47,200
There's also a display case
power plants is a village

22
00:01:47,400 --> 00:01:49,600
and you're off to Ufa.

23
00:01:49,800 --> 00:01:56,920
but what I'm most interested in
that these concessions have strictly

24
00:01:57,120 --> 00:01:58,920
horizontal

25
00:01:59,120 --> 00:02:05,040
the layers they're cut off and very
interested in this holding company.

26
00:02:06,480 --> 00:02:11,760
so you understand that this
the holding is stuffed with clay.

27
00:02:12,280 --> 00:02:13,800
and the most important thing is that when you do.

28
00:02:14,000 --> 00:02:20,000
looking at this hill, these are the footprints
they're on the fly this way.

29
00:02:26,160 --> 00:02:31,960
about the same thing I showed you
when he was showing the Millennium Falcon

30
00:02:32,160 --> 00:02:36,840
so I moved on to the next one.
the hill and all these stripes.

31
00:02:37,040 --> 00:02:39,520
just the same.

32
00:03:08,160 --> 00:03:10,280
in order to understand how these strips

33
00:03:10,480 --> 00:03:15,040
are made quite simply
to remember our life experiences.

34
00:03:15,240 --> 00:03:17,480
imagine there's some kind of long

35
00:03:17,680 --> 00:03:20,280
walkway
the tech, and then bam.

36
00:03:20,480 --> 00:03:23,000
the tech stops walking.

37
00:03:23,200 --> 00:03:26,120
so the denser clay sits at the bottom

38
00:03:26,320 --> 00:03:30,000
and more liquid higher
the clay stays on top.

39
00:03:30,200 --> 00:03:34,120
and then she starts flipping

40
00:03:34,320 --> 00:03:40,760
to distinguish clay that it's notional
liquid method into this place.

41
00:03:40,960 --> 00:03:43,000
and the photos that Alexei took

42
00:03:43,200 --> 00:03:49,000
that's exactly what they're showing
that we have so many layers of clay

43
00:03:49,200 --> 00:03:52,640
that were poured into this place
exactly in the liquid state.

44
00:03:52,840 --> 00:03:59,320
they froze, and after that.
we observe what we observe.

45
00:03:59,520 --> 00:04:01,800
but you have to understand that after

46
00:04:02,000 --> 00:04:06,720
after these layers are cured.
these layers right here.

47
00:04:12,320 --> 00:04:16,000
someone from that.

48
00:04:18,440 --> 00:04:21,360
a section of rock selected and made the topography

49
00:04:21,560 --> 00:04:28,680
that you are observing, for the hill itself
It's not shaping up that way for itself.

50
00:04:28,880 --> 00:04:31,000
in doing so, you must understand and address

51
00:04:31,200 --> 00:04:35,360
note that in this instance
we have clay material.

52
00:04:35,560 --> 00:04:38,640
the cases when I showed you in Belek and

53
00:04:38,840 --> 00:04:44,160
the white rock there's snow material but it has
the same structure the same

54
00:04:44,360 --> 00:04:50,240
the principles of education when
I'm showing you the Millennium Falcon.

55
00:04:50,440 --> 00:04:52,760
There's a different material.

56
00:04:53,000 --> 00:04:59,080
some kind of weird stone
but it's just as layered with colors

57
00:04:59,280 --> 00:05:00,200
by birth and so on.

58
00:05:00,400 --> 00:05:03,520
which means he intended
in exactly the same way.

59
00:05:03,960 --> 00:05:09,840
and it turns out that as a result
alexei sergeyevich's separate observation

60
00:05:10,040 --> 00:05:15,280
of the Smithsonian, we've basically put the puzzle together
that we haven't been able to do for a long time.

61
00:05:15,480 --> 00:05:18,080
so, to summarize, our

62
00:05:18,280 --> 00:05:24,240
of the research very briefly, the first point we
have clearly identified the technologies that

63
00:05:24,440 --> 00:05:29,360
you could call a conventionally plywood cake
Napoleon and so on and so forth.

64
00:05:29,560 --> 00:05:32,200
this technology is found in both Crimea

65
00:05:32,400 --> 00:05:37,440
It's the same in Ufa and in Khakassia everywhere
the same technology is used.

66
00:05:37,640 --> 00:05:40,280
that is, it's happening at

67
00:05:40,480 --> 00:05:43,240
of some material, he's able to keep up

68
00:05:43,440 --> 00:05:48,000
the time of the show to tell you to relax and
to approve the approval after that is carried out on

69
00:05:48,200 --> 00:05:53,040
the face of the next batch of material
material and so on and so forth.

70
00:05:53,240 --> 00:05:54,800
which means that as a result of what we're seeing

71
00:05:55,000 --> 00:05:59,200
certain tectonic
ongoing processes

72
00:05:59,400 --> 00:06:01,120
over time

73
00:06:01,640 --> 00:06:07,080
in some way, it's a process
has the same number of servings and

74
00:06:07,280 --> 00:06:11,960
of the substance that's being poured into
in some case, these portions

75
00:06:12,160 --> 00:06:17,160
are different and, as a result.
the thickness of the layers is different, but

76
00:06:17,400 --> 00:06:22,600
clearly we can say that it's not
Continuous and very long accumulation

77
00:06:22,800 --> 00:06:26,720
of materials, which is what.
then the cyclicality and the process by

78
00:06:27,760 --> 00:06:32,360
of shaping the topography exactly with
with what's called a break.

79
00:06:32,600 --> 00:06:37,360
I mean, once again, I just want to remind you that
first the horizon was open

80
00:06:37,560 --> 00:06:41,800
a lot of layers and then
selected areas and selected

81
00:06:42,840 --> 00:06:46,240
the area where people live is planned.

82
00:06:46,440 --> 00:06:51,240
just so you understand it's a completely different
level of technology and most importantly

83
00:06:51,440 --> 00:06:55,960
an entirely different level of challenge than
what we can imagine.

84
00:06:56,160 --> 00:07:01,280
the link to the materials I used
will be in the description below this video.

85
00:07:01,480 --> 00:07:05,640
which one of you will be driving on the highway

86
00:07:06,040 --> 00:07:09,080
especially in the summer

87
00:07:09,280 --> 00:07:12,360
in the springtime in the fall before
the moment the snow falls.

88
00:07:12,560 --> 00:07:16,080
pay attention to this situation.

89
00:07:16,280 --> 00:07:23,000
it's very, very interesting to look at
the world around us through different eyes.

90
00:07:23,200 --> 00:07:28,360
I hope that Alexei Sergeyevich's observations
the Smithsonian has been of interest to you

91
00:07:28,560 --> 00:07:34,360
my explanation on the matter
was clear and convincing to you

92
00:07:34,560 --> 00:07:38,360
I thank you, my companions.
for watching this video.

93
00:07:38,560 --> 00:07:43,840
but the fact that you support us in our
in your endeavors, join the recovery.

94
00:07:44,040 --> 00:07:45,360
thank you for your attention.

95
00:07:45,560 --> 00:07:47,080
goodbye.

96
00:08:00,560 --> 00:08:06,120
apparently this is where they picked out the soil
where we drive doesn't look like a funnel.

97
00:08:45,520 --> 00:08:51,120
well, that's how I've been chained up in layers.

98
00:08:57,240 --> 00:09:02,040
because it's bigger, right?
and contacted the Turkish one.

99
00:09:03,800 --> 00:09:04,960
guy

100
00:09:05,160 --> 00:09:06,760
I guess.

101
00:09:09,000 --> 00:09:10,480
more.

102
00:09:12,520 --> 00:09:14,600
level with

103
00:09:14,800 --> 00:09:20,000
and no complications
not until the end of Belgrade.

104
00:09:27,160 --> 00:09:29,640
there's one right there.

105
00:09:30,160 --> 00:09:31,640
situation.

106
00:09:32,280 --> 00:09:34,040
our Vitya

107
00:09:36,280 --> 00:09:37,160
waved his hand.

