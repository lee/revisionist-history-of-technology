title: Everybody Dances - Clues to the Real History of Airships
date: 2020-10-29
modified: 2022-04-24 09:57:35
category: 
tags: technology
slug: 
authors: tech_dancer
summary: Well, in Eurasia, these forces turned the Russian-German Russian Empire into a desert, perhaps sparing its western part. Those events are sparingly mentioned in history as the defense of Sevastopol, although fighting elsewhere on the coast also occurred. And there is complete silence about the bombing of cities inland.
status: published
originally: Everybody Dances.html
from: https://www.tart-aria.info/tancujut-vse/
local: Everybody Dances_files

### Translated from: 

[https://www.tart-aria.info/tancujut-vse/](https://www.tart-aria.info/tancujut-vse/)

**Russian:**

*Советский анекдот (вместо эпиграфа) Мужик трудоустраивается на секретный военный завод,он же «почтовый ящик». На собеседовании его сразу жепросканировал*

**English:**

*A Soviet anecdote (instead of an epigraph)... A man goes for a job at a secret military factory, aka a "mailbox". He was immediately screened in a job interview by a member of the 'First Department':*

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/0.jpg)

**Russian:**

> Фамилия?

> Сахаров.

> Точнее?

> Сахарович.

> Ещё точнее?

> Цуккерман*.

**English:**

> Q: Last name?

> A: Sakarov.

> Q: Specifically?

> A: Sakharovich.

> Q: More specifically?

> A: Zuckerman.

**Russian:**

Забавно, не правда ли?

**English:**

Funny, isn't it?

**Russian:**

Почему-то при попадании в командировки куда-нибудь за Урал делал для себя открытие, что людей с такими интересными фамилиями вокруг тебя становится всё больше. Но побили все рекорды города Республики Коми и ЯНАО. Там совершенно легко можно встретить фамилию практически любого представителя народов СССР, ну и может быть чуть посложнее фамилии народов бывшего европейского соцлагеря. Но чаще абсолютно всех фамилий советских народов там можно встретить немецкие фамилии. На вопрос «как вы тут оказались?» можно услышать примерно одно и то же -- издержки социалистического строя. Предки тех людей очутились в этом краю далеко не по своей воле, и с годами утратили связь со своей малой Родиной. Так и жили, забыв родные язык и культуру, и со временем привыкнув к неблагоприятности климата этих мест. Человек так устроен, что со временем может приспособиться ко многому. Такова его природа.

**English:**

For some reason, when you go on a business trip somewhere beyond the Urals, you discover that there are more and more people with interesting surnames around you. But the cities of the Komi Republic and the Yamalo-Nenets Autonomous District break all records. There, you can easily come across a surname of almost any representative of the peoples of the USSR and, well, perhaps the slightly more complicated surnames of the peoples of the former European socialist camp. But more often, the surnames of the Soviet peoples there are German surnames. To the question: "how did you come to be here?" you usually hear approximately the same answer: "the costs of the socialist system". The ancestors of these people found themselves in this region not by choice and over the years they lost touch with their small motherland. So they lived, forgetting their native language and culture, and over time became accustomed to the unfavorable climate of these places. Man is so built that over time he can adapt to many things. That is his nature.

**Russian:**

Почему эти люди не переехали в ту же Москву или хотя бы близлежащие окрестности? Наверное, страх от тех лет ещё жив в поколениях тех людей по сей день, несмотря на то, что поколения уже сменились несколько раз. Не хотят они переезжать в Москву, хоть столица давно уже более чем гостеприимна ко всем народам, какой она собственно была почти всегда, исключая некоторый период в 20 веке. Вспоминается один великий танцевальный хит от 20 века, исполненный немецкой поп-группой [Gengis Khan](https://ru.wikipedia.org/wiki/Dschinghis_Khan).

**English:**

Why didn't these people move to Moscow, or at least Moscow's surrounding area? Probably, the fear from those years is still alive in today's generations of those people - despite the fact that there have been multiple generations since those times. They do not want to move to Moscow, even though the capital has long been more than hospitable to all peoples - just as it almost always was but for a certain period in the 20th century. I remember one great dance hit from the 20th century, performed by German pop group [Gengis Khan](https://ru.wikipedia.org/wiki/Dschinghis_Khan):

**Russian:**

| German lyrics | Russian translation |
|:-- |:-- |
| Moskau - fremd und geheimnisvoll | Москва - незнакомая и таинственная, |
| Türme aus rotem Gold | С башнями из червонного золота |
| Kalt wie das Eis | И холодная как лёд. |
| Moskau - doch wer dich wirklich kennt | Москва - но те, кто знаком с тобою ближе |
| Der weiß ein Feuer brennt | Знают, что в тебе горит |
| In dir so heiß | Обжигающий огонь. |
| Kosaken - he-he-he - hebt die Gläser | Казаки - хей, хей, хей - поднимайте стаканы! |
| Natascha - ha-ha-ha - du bist schön | Наташа - ха, ха, ха - ты так хороша! |
| Tovarisch - he-he-he - auf das Leben | Товарищ - хей, хей, хей - за жизнь! |
| Auf dein Wohl Bruder he - Bruder ho | За ваше здоровье, братья, хей, братья, хей! |

**English:**

| German lyrics | English translation |
|:-- |:-- |
| Moskau - fremd und geheimnisvoll | Moscow - unknown and mysterious, |.
| Türme aus rotem Gold | With towers of red gold |.
| Kalt wie das Eis | And cold as ice. |
| Moskau - doch wer dich wirklich kennt | Moscow - but those who know you better |.
| Der weiß ein Feuer brennt | Know what's burning in you |
| In dir so heiß | Searing fire. |
| Kosaken - he-he-he - hebt die Gläser | Cossacks - hey, hey, hey, hey - raise your glasses! |
| Natascha - ha-ha-ha - du bist schön | Natasha - ha, ha, ha - you are so beautiful! |
| Tovarisch - he-he-he - auf das Leben | Comrade - hey, hey, hey - to life! |.
| Auf dein Wohl Bruder he - Bruder ho | Cheers, brothers, hey, brothers, hey! |

**Russian:**

Хоть эта песня во многом была пародией на западноевропейское представление о Москве, она пользовалась огромной популярностью на территории всего бывшего СССР. Поскольку в то время все переписывали песни друг у друга, у многих пользователей качество записи было таким, что слов в песне было уже не разобрать, да в общем этого и не требовалось. Главным была необычность исполнения песни и её тотальные гонения со всех как сейчас говорят информационных ресурсов. В 80-х годах 20 века, несмотря на запрет, эту песню можно было услышать практически из каждого окна, где имелись магнитофоны. Ходили слухи, что запретили эту песню из-за политического подтекста. В песне якобы слышалось желание врага захватить Москву, что не получилось сделать несколько десятилетий тому назад. Всё это, конечно, сейчас рассматривается как чушь, но тогда к этому относились более чем серьёзно. При появлении этой песни в школах, на танцевальных вечерах, комсомольскому активу начинали промывать мозги буквально на следующий день. Несмотря ни на что, история с исполнением трудов коллектива «Чингиз-Хан» на музыкальных вечерах продолжалась. Танцевала вся молодёжь, несмотря на принадлежность к ВЛКСМ, и даже некоторые учителя.

**English:**

Although this song was in many ways a parody of the Western European idea of Moscow, it was very popular throughout the former Soviet Union. Since everybody was copying each other's copies of songs at that time, many owners' recordings were of such poor quality that the lyrics were incomprehensible. But in general, comprehensive wasn't required. The main thing was the unusual singing of the song and its total censorship by everyone, as information resources say now. In the 20th century's 1980s, this song could be heard practically from every window where there were tape recorders, despite the ban. There were rumors that this song was banned because of political overtones. In the song was heard - ostensibly - the enemy's desire to capture Moscow, having failed to capture it a few decades earlier. All this, of course, is now considered as nonsense, but back then it was taken more than seriously. When this song appeared in schools, on dance evenings, the Komsomol assets were brainwashed the very next day. In spite of everything, the story continued: the works of the "Genghis Khan" collective continued to be performed at musical evenings. All the young people danced despite their belonging to Komsomol and so, even, did some teachers.

**Russian:**

Зачем в этой песне её авторы намекали (если вообще намекали) на взятие Москвы, интересно узнать? И почему все работники идеологического сектора Комсомола, запрещавшие данный коллектив, в один голос говорили, что это специально сделанный диверсионный проект? Его финансирование, с их слов, осуществляется мировой буржуазией, а цель -- разрушение основ русской культуры. Серьёзно, в своё время слышал сам на собраниях. Ни дать, ни взять.

**English:**

It is interesting to know, why the authors of this song hinted (if they really were hinting) at the capture of Moscow? And why did all the workers in the Komsomol ideological sector who prohibited this collective, unanimously say that it was a specially created subversive project? According to them, it was financed by the world's bourgeoisie, and its goal was to destroy the foundations of Russian culture? Seriously, I've heard this at meetings myself. That's straight up true!

**Russian:**

Ну и на этой лирической ноте заканчиваем вступление и переходим к главному.

**English:**

And on that lyrical note, we finish the intro and move on to the main piece.

<!-- Local
![](Everybody Dances_files/3-250-.gif "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/3-250-.gif)

**Russian:**

А главным (но не единственным) сегодня будет воздухоплавание. Да-да, то самое, которое было до массового внедрения самолётов. Или воздухоплавание на [дирижаблях](https://ru.wikipedia.org/wiki/%D0%94%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8C).

**English:**

And the main (but not the only) subject today will be aeronautics. Yes, yes, the aeronatuics that occurred before the mass introduction of planes. Or ballooning in [dirigibles (Wiki.ru)](https://ru.wikipedia.org/wiki/%D0%94%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8C).

<!-- Local
![](Everybody Dances_files/1-1.jpg)
-->

![](https://m1.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/1-1.jpg)

**Russian:**

Несмотря на то, что тема подобного воздухоплавания является уже достаточно избитой, попробуем найти в ней нечто новое. И по возможности не будем повторяться с материалами многочисленных исследователей, отдавших этой теме много и не очень много времени. Итак, дирижабли.

**English:**

Despite the fact that the topic of such aeronautics has already been quite well beaten, let's try to find something new. And if possible, we will not repeat the material of numerous researchers who have given so much, and not quite so much, time to this topic. So, airships...

**Russian:**

> Дирижа́бль (от фр. dirigeable - управляемый), или управляемый аэростат - вид воздушных судов: аэростат, снабжённый силовой установкой и способный передвигаться в заданном направлении со значительной скоростью в большом диапазоне высот. Корпус дирижабля представляет собой тело удобно обтекаемой формы, объёмом от 2000 до 200000 м3, снабжённое стабилизаторами, вертикальными и горизонтальными рулями, в составе системы управления ориентацией, обеспечивающей возможность передвигаться в любом направлении независимо от направления воздушных потоков. На дирижаблях устанавливаются мощные моторы (обычно поршневые), приводящие во вращение воздушные винты, создающие тягу. По конструкции корпуса дирижабли делятся на три типа: нежёсткие (мягкие), жёсткие и полужёсткие.

**English:**

> Blimp (from Fr. dirigeable - controlled), or controlled balloon, a type of aircraft: a balloon equipped with a propulsion system and capable of moving in a given direction at high speed in a wide range of heights. The blimp body is a conveniently streamlined body with a volume from 2,000 to 200,000 m3, equipped with stabilizers, vertical and horizontal rudders, as part of the orientation control system, providing the ability to move in any direction regardless of wind direction. The airships are equipped with powerful motors (usually piston engines), which rotate the propellers, creating thrust. The blimps are divided into three types: non-rigid (soft), rigid and semi-rigid.

**Russian:**

Полагаю, все читатели достаточно хорошо представляют себе те самые дирижабли. Расхожим стереотипом является то, что как правило дирижабль был огромным воздушным шаром, под которым находилась маленькая кабина для пассажиров. Настолько маленькая, что на некоторых фото её можно было и не заметить. Почему возникали подобные диспропорции?

**English:**

I suppose all readers have a pretty good idea of those blimps. A common stereotype is that a blimp was usually a huge balloon, under which there was a small cabin for passengers. It was so small that in some photos you would not even notice it. Why did such disproportion arise?

<!-- Local
![](Everybody Dances_files/2-1.jpg "Танцуют все! tech_dancer")i
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/2-1.jpg)

**Russian:**

В конструкции дирижабля всегда предусмотрена оболочка для размещения газа легче воздуха под давлением. Давление газа внутри оболочки обеспечивает противодействие её смятию давлением внешней атмосферы. На ранних дирижаблях весь газ помещали в оболочке с единым объёмом и простой стенкой из промасленной или лакированной ткани. Впоследствии оболочки стали делать из прорезиненной ткани или других (синтетических) материалов однослойными или многослойными для предотвращения утечек газа и увеличения их срока службы, а объём газа внутри оболочки стали разделять на отсеки - баллоны.

**English:**

The design of the blimp always includes a shell to accommodate lighter-than-air pressurized gas. The gas pressure inside the casing ensures that the outer atmosphere pressure counteracts its buckling. In early airships, all gas was placed in a shell with a single volume and a simple wall of oiled or varnished fabric. Subsequently the shells began to be made of rubberized fabric or other (synthetic) materials as single or multilayer walls to prevent gas leaks and increase their service life, and the volume of gas inside the shell was divided into compartments: cylinders.

**Russian:**

Действительно, а сколько нужно лёгкого газа, чтобы накачать аэростат подобных размеров, создать внутри него соответствующее упоминаемое давление и ещё поддерживать это давление в течение времени полёта? Как известно, дирижабли в 20 веке летали без промежуточных посадок на достаточно дальние расстояния. Известны случаи, когда дирижабли осуществляли кругосветный перелёт за два дня всего с тремя посадками, и то, не известно, чем они были вызваны -- необходимостью выхода пассажиров или нуждами эксплуатации. Не каждый современный самолёт, кстати, может похвастаться такими возможностями беспосадочного перелёта.

**English:**

Indeed, how much light gas does it take to fill a balloon of this size, to create the appropriate pressure inside it and still maintain this pressure during the flight? As you know, airships in the 20th century were flying for quite long distances without intermediate landings. There are known cases when airships flew around the world in two days with only three landings, and it is not known whether their landings were caused by the need to exit passengers or by operational needs. Not every modern aircraft, by the way, can boast of such non-stop flight possibilities.

<!-- Local
![](Everybody Dances_files/3.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/3.jpeg)

**Russian:**

Неужели такую бандуру накачивали газом легче воздуха при таких её объёмах? Но факт имеет место быть. Такие дирижабли делали трансатлантические перелёты, что было в общем вполне заурядным явлением в начале 20 века. В различных источниках информации имеется ряд упоминаний о том, что в США научились добывать в промышленных масштабах гелий. Гелий легче воздуха, и его использовали для наполнения дирижаблей. Но при этом гелий совершенно безопасен, в отличие от водорода, использовавшегося в тех же целях в Европе, не имеющей месторождений гелия. Из-за этого гелий в США сделали стратегическим сырьём и запретили его продажу в другие страны.

**English:**

Did they really inflate junk like this with a gas lighter than air at such a volume? But these existed. Such airships made transatlantic flights, which was generally quite ordinary in the early 20th century. There are a number of references in various sources to the fact that the United States learned how to extract helium on an industrial scale. Helium is lighter than air, and it was used to fill airships. But helium is completely safe, unlike hydrogen, which was used for the same purposes in Europe, where there are no helium deposits. Because of this, the US categorised helium as a strategic raw material and prevented it from being sold to other countries.

**Russian:**

Но вопросы, тем не менее, остаются. Каким образом производители дирижаблей обеспечивали герметичность подобной оболочки при таких её размерах? Это достаточно сложно, даже если поделить внутренний объём на отсеки. При малейших заводских браках оболочка перестала бы держать драгоценный газ, и могли быть аварии. И всё же, дирижабли считались намного надёжнее появившихся аэропланов, и статистика это подтверждала.

**English:**

But the questions still remain. How did airship manufacturers ensure that a shell of this size was airtight? It is quite difficult, even if you divide the inner volume into compartments. With the slightest factory rejects, the casing would stop holding the precious gas and there could be accidents. Still, airships were considered much more reliable than airplanes, and statistics confirmed it.

<!-- Local
![](Everybody Dances_files/4.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/4.jpeg)

**Russian:**

А в данном случае вообще скрестили самолёт с дирижаблем. А что? Вполне разумное техническое решение. Чем ниже пассажирская гондола находится от аэростата, тем устойчивее считался дирижабль. Только вот разве что если какой-нибудь любитель из хулиганских побуждений решил бы по такому дирижаблю пострелять, то все его достоинства сходили на нет. Всё это очень диссонирует с тем, что дирижабли широко применялись в боевых действиях, например в ту же первую мировую. С них осуществляли бомбардировку, и даже создавая при этом огромное преимущество нападавшей стороне. Как же так? При такой уязвимости дирижабля подобные факты кажутся невероятными.

**English:**

And in this case, they even crossed a plane with a blimp. Why? It's a reasonable technical solution. The lower the passenger gondola is below the aerostat, the more stable the blimp was considered to be. Except that if a hooligan decided to shoot at such a blimp, all its merits were lost. All this is very dissonant with the fact that the blimps were widely used in combat operations, such as the First World War. They were used to bombard, and even created a huge advantage for the attacking side. How so? With a blimp so vulnerable, such facts seem incredible.

**Russian:**

И всё же, почему в конструкциях дирижаблей была неестественная диспропорция между размерами шара и грузопассажирскими помещениями? Это очень неудобно, непрактично, затратно и как угодно ещё неэффективно. По законам физики, газовый резервуар дирижабля может быть и маленьких размеров. Всё зависит от разности удельного веса между наполняющим газом и окружающим воздухом. Чем легче наполняющий газ, тем меньше может быть оболочка дирижабля в размерах, ровно в соответствии с законом Архимеда. Возможно, некое увеличение длины оболочки было бы нужно для лучшей управляемости, но не до такой степени, как например здесь.

**English:**

Still, why was there an unnatural disproportion between the size of the blimp and the passenger and cargo areas in the blimp structures? It's very uncomfortable, impractical, costly, and inefficient as anything else. According to the laws of physics, the gas reservoir of a blimp can be small. It all depends on the difference in specific gravity between the filling gas and the surrounding air. The lighter the filling gas, the smaller the blimp's shell can be, according to Archimedes' principle. Perhaps some increase in shell length would be necessary for better controllability, but not to the same extent as we see here.

<!-- Local
![](Everybody Dances_files/5-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/5-1.jpg)

**Russian:**

Такой большой размер оболочки косвенно указывает на то, что применяемый газ не очень сильно отличался по своему удельному весу от аналогичного показателя обычного воздуха, и его для обеспечения полётных характеристик требовалось много. Что же там было? Гелий низкого качества или разбавленный чем-то негорючим водород? Не будем делать поверхностные выводы, и посмотрим на рисунки предков тех самых «современных» (если можно так сказать) дирижаблей.

**English:**

Such a large shell size indirectly hints that the gas did not differ very much in its specific weight from that of ordinary air, and it required a lot to ensure flight characteristics. So what was it? Was the helium of poor quality or was it hydrogen diluted with something non-combustible? Let's not make superficial conclusions and let's look at the drawings of the ancestors of those very "modern" (if I may say so) airships.

<!-- Local
![](Everybody Dances_files/6.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/6.jpeg)

**Russian:**

По аннотации это летательный аппарат первой половины 19 века, что немного странно (об этом чуть позже). Судя по рисунку, принцип действия этого аппарата аналогичен дирижаблям. Внутри аппарата находятся балонеты, с помощью которых осуществлялось управление полётом. По свидетельствам различных источников, в дирижаблях было два варианта управления -- в балонеты накачивался газ весом гораздо легче воздуха при нахождении вне балонет обычного воздуха с обычным давлением, и инверсный вариант, когда в оболочке был газ легче воздуха, а в балонеты накачивался обычный воздух. К какому варианту здесь склоняться больше, сказать сложно. Но судя по тому, что на рисунке балонеты явно тянутся вниз подобно мужскому достоинству, их вес явно больше находящегося в оболочке газа. Так что же, всё таки в большие по размерам оболочки дирижаблей накачивали водород, и летели на нём, крестясь, вокруг света за два дня? Или мы чего-то про дирижабли всё же не знаем?

**English:**

According to the annotation this aircraft is from the first half of the 19th century, which is a bit strange (more about that a little later). Judging by the drawing, the principle of operation of this aircraft is similar to that of airships. Inside the spacecraft there are balonets by means of which the flight was controlled. According to various sources, there were two variants of control in airships - gas weighing much lighter than air was pumped into the cylinders when ordinary air with normal pressure was outside the cylinders, and the inverse variant, when the shell contained gas lighter than air, and ordinary air was pumped into the cylinders. To which variant one is more inclined here, it is difficult to say. But judging by the fact that in the picture the ballonets are obviously pulled down like a man's dignity, their weight is clearly greater than the gas in the shell. So, did they pump hydrogen into large airship shells and fly around the world in two days? Or is there something about airships we don't know?

**Russian:**

А странность, о которой сказано выше, заключается в том, что Википедия очень необычно описывает нижний хронологический период возникновения дирижаблей. Как будто их в том виде, в которым мы и наблюдаем на многочисленных фото, изобрели только во второй половине 19 века некоторые альтруисты-энтузиасты.

**English:**

The strange thing about the Wikipedia entry above is that it describes the earlier? chronological period of blimps in a very unusual way. It is as if they were invented in the form in which we see them in numerous photographs only in the second half of the 19th century by some altruistic enthusiasts.

**Russian:**

> Дирижабль с паровым двигателем конструкции Анри Жиффара, который позаимствовал эти идеи у Мёнье более чем полвека спустя, совершил первый полёт только 24 сентября 1852. Такая разница между датой изобретения аэростата (1783 г.) и первым полётом дирижабля объясняется отсутствием в то время двигателей для аэростатического летательного аппарата. Следующий технологический прорыв был совершён в 1884 году, когда был осуществлён первый полностью управляемый свободный полёт на французском военном дирижабле с электрическим двигателем La France Шарлем Ренаром и Артуром Кребсом. Длина дирижабля составила 52 м, объём - 1900 м³, за 23 минуты было покрыто расстояние в 8 км при помощи двигателя мощностью 8,5 л. с. Тем не менее, эти аппараты были недолговечны и чрезвычайно непрочны. Регулярные управляемые полёты не совершались до появления двигателя внутреннего сгорания.

**English:**

> The steam-engined blimp by Henri Giffard, who borrowed these ideas from Meunier more than half a century later, did not make its first flight until September 24, 1852. This difference between the date of invention of the aerostat (1783) and the first flight of the blimp is due to the lack of engines for the aerostatic aircraft at the time. The next technological breakthrough was made in 1884, when the first fully steerable free flight on a French military blimp with an electric engine La France Charles Renard and Arthur Krebs was performed. The blimp was 52 m long, with a volume of 1,900 m³, and in 23 minutes it was covered a distance of 8 km with an 8.5 HP engine. There were no regular controlled flights until the internal combustion engine appeared.

**Russian:**

О чём вообще идёт речь? На аэростаты установили паровой и электрический двигатели, и они как будто смогли пролететь, и даже неплохо. Ни слова о том, какой использовался газ и как поддерживалось его давление внутри оболочки. И вдруг с появлением двигателя внутреннего сгорания все проблемы недолговечности и непрочности тех аппаратов стали решены, и дирижабли стремительно начали свой хоть и недолгий, но яркий век.

**English:**

What are we talking about, anyway? Steam and electric engines were installed in the balloons, and it was as if they could fly, and not even badly. Not a word about what kind of gas was used and how its pressure was maintained inside the shell. And suddenly, with the advent of an internal combustion engine, all the problems of frailty and durability of those devices were solved, and the airships began their short, bright century.

**Russian:**

А может, всё, что мы знаем про дирижабли тех лет, является сказкой? Ну разве что кроме фотографий этих красавцев в небе.

**English:**

Or maybe everything we know about blimps in those years is a fairy tale? Well, except for the pictures of these beauties in the sky.

<!-- Local
![](Everybody Dances_files/7.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/7.jpg)

**Russian:**

И исчезли эти дирижабли совсем не оттого, что некто признал их опасными после [катастрофы дирижабля «Гинденбург»](https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D1%82%D0%B0%D1%81%D1%82%D1%80%D0%BE%D1%84%D0%B0_%D0%B4%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8F_%C2%AB%D0%93%D0%B8%D0%BD%D0%B4%D0%B5%D0%BD%D0%B1%D1%83%D1%80%D0%B3%C2%BB) в 1937 году. Кто не в курсе, это событие стало переломным в истории эксплуатации дирижаблей, и с него начался закат эры дирижаблестроения по всему миру. С подробностями этой истории можно ознакомиться по ссылке. В то же время, в сети гуляет некая конспирологическая информация, что перед данным рейсом в компанию-владельца дирижабля поступил анонимный звонок о готовящемся теракте. По одной из версий, в дирижабль должны были выстрелить в пункте прибытия, по другой, в багаже было заложено взрывное устройство, которое должны были привести в действие также по прибытию. Несмотря ни на что, рейс состоялся, с тем страшным финалом. Описание пожара на дирижабле выглядит необычно. Водород, как мы знаем, сгорает в открытом воздухе достаточно быстро, как любой горючий газ. Процесс его горения займёт не более минуты. В смеси с воздухом он взрывается, и процесс происходит ещё быстрее и разрушительнее. Прежде чем взорваться, водороду надо как-то смешаться с воздухом, на что нужно достаточно продолжительное время, гораздо больше, чем время горения. Но то, что приводится в случае с тем дирижаблем, упорно описывает взрыв и горение постороннего вещества, оказавшегося неким образом внутри летательного аппарата. И налицо все признаки теракта. Что там было в реалии, уже не установить. Кстати, в Википедии совершенно не стесняясь приводят следующую историю, даже с именами действующих лиц:

**English:**

And these blimps disappeared not because someone recognized them as dangerous after [the Hindenburg blimp disaster](https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D1%82%D0%B0%D1%81%D1%82%D1%80%D0%BE%D1%84%D0%B0_%D0%B4%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8F_%C2%AB%D0%93%D0%B8%D0%BD%D0%B4%D0%B5%D0%BD%D0%B1%D1%83%D1%80%D0%B3%C2%BB) in 1937. Who does not know this event was a turning point in the history of the exploitation of blimps; that it started the sunset of the blimp era around the world? The details of this history can be found here. At the same time, there is some conspiracy information on the Internet. It says the airship owner received an anonymous call before this flight about an impending terrorist attack. According to one version, the blimp should have been fired on at the point of arrival, while another version says an explosive device was hidden in its luggage, and should have been detonated upon arrival. In spite of everything, the flight took place, with that terrible finale. The description of the blimp fire looks unusual. Hydrogen, as we know, burns outdoors as fast as any flammable gas. It doesn't take more than a minute to burn. It explodes when mixed with air, and then the burn process is even faster and more destructive. Before it explodes, though, hydrogen has to be mixed with air somehow, which takes a long time, much longer than the combustion time itself. But what is described in the case of that blimp, persistently suggests the explosion and burning of foreign matter that is somehow inside an aircraft. And there are all the signs of a terrorist attack. In reality, what was there can no longer be determined. By the way, Wikipedia does not hesitate to cite the following story, even with the names of the actors:

**Russian:**

> Согласно версии, впервые выдвинутой американским историком-любителем Адольфом Хёлингом (нем. Adolf Höling), «Гинденбург» был уничтожен взрывом мины с часовым механизмом. Эта мина была установлена техником Эрихом Шпелем (нем. Erich Spel) на дне баллона № 4. Взрыв должен был произойти после причаливания, когда все пассажиры и экипаж должны были покинуть дирижабль. Но так как «Гинденбург» сделал «лишний» круг, часовой механизм сработал до их высадки. Сам же Шпель выпрыгнул из горящего дирижабля, но вскоре скончался в госпитале от полученных ожогов. Этой же версии придерживался бывший начальник IV управления РСХА (гестапо) Генрих Мюллер.

**English:**

> According to the version first put forward by American amateur historian Adolf Höling, the Hindenburg was destroyed by a clockwork mine. This mine was set by technician Erich Spelle at the bottom of cylinder number 4. The explosion was to occur after docking, when all passengers and crew had to leave the blimp. But since the Hindenburg made an "extra" lap, the clockwork went off before they disembarked. Spelle himself jumped out of a burning blimp, but soon died in hospital from the burns he received. The former head of the IV department of RSHA (Gestapo) Heinrich Muller believed that version of the event.

**Russian:**

Ну если так считал Мюллер, то можно не сомневаться в правдивости этой истории. Зачем и кому был нужен теракт?

**English:**

Well, if that's what Mueller thought, there's no question about the truth of the story. Who would want a terrorist attack and why?

<!-- Local
![](Everybody Dances_files/8.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/8.jpeg)

**Russian:**

Очевидно, стояла задача уничтожить дирижабли как класс. Кому-то они очень сильно мешали, или служили свидетелями неких неизвестных страниц в истории, которых обывателям было знать совсем не обязательно. В любом случае, участь дирижаблей была предрешена, и они стали практически синхронно гореть по всему миру. Их массовое исчезновение стало вопросом короткого времени.

**English:**

Obviously, the task was to destroy the blimps as class. They were very much in the way, or they served as witnesses to some unknown pages in history, which the common people did not need to know at all. In any case, the fate of the blimps was predetermined, and they began to burn almost synchronously throughout the world. Their mass extinction was a matter of short time.

<!-- Local
![](Everybody Dances_files/9-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/9-1.jpg)

**Russian:**

Таким образом дирижабли высаживали пассажиров. Для безопасности высадки строились специальные башни, с круговыми площадками вверху. Дирижабль стыковался к башне через специальное устройство, с помощью тросов, и свободно вращался в азимутальной плоскости на случай ветра. Очевидно, у дирижабля не было возможности опускаться непосредственно на грунт. Что-то там мешало.

**English:**

This is how airships landed their passengers. For the safety of the disembarkation, special towers were built with circular platforms at the top. The airship was connected to the tower by a special device, using cables, and rotated freely in the azimuth plane in case of wind. Obviously, the blimp did not and could not descend directly to the ground. Something was in the way.

**Russian:**

И сразу вспомнилось фото из [прошлой статьи](https://www.tart-aria.info/jenergetika-proshlogo-krymskaja-vojna/).

**English:**

Immediately I remembered a photo from [the last article](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/jenergetika-proshlogo-krymskaja-vojna/).

<!-- Local
![](Everybody Dances_files/10-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/10-1.jpg)

**Russian:**

Оно, не? Так с чего там разбомбили Севастополь? Начинаем думать.

**English:**

It is, isn't it? So why did they bomb Sevastopol? We're starting to think.

**Russian:**

А впрочем, очень удобно. Имея с десяток дирижаблей, при отсутствии их у обороняющихся можно в достаточно короткий срок превратить любую территорию в руины. И при этом совсем не важно, какие упражнения делает армия врага на земле - её участь в любом случае решена. Если дирижабль за два дня облетал вокруг света, то превратить в пустыню такую территорию, как бывшая Великая Тартария, было можно сказать вопросом техники и не очень долгого времени. Лишь бы хватило бомб. А почему отсутствовали дирижабли у обороняющихся?

**English:**

It's very convenient, though. A dozen blimps, in the absence of defendants, can turn any territory into ruins in a fairly short time. And it does not matter what exercises the enemy army does on the ground, its fate is certain in any case. If a dirigible flew around the world in two days, it was possible to turn such a territory as the former Great Tartaria into a desert, and not require a long time. There would only be enough bombs. And why were there no airships for the defendants?

**Russian:**

В качестве отправной точки возьмём достаточно хорошо известную историю о строительстве дирижаблей в Германии, тех самых, фирменных.

**English:**

As a starting point we will take the well-known story about the construction of airships in Germany, the very same branded ones.

Luftschiff, Flugaufnahme, gelaufen 9.6.1935:

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/11-1.jpg)

**Russian:**

Их строили на заводах [Фердинанда фон Цеппелина](https://ru.wikipedia.org/wiki/%D0%A6%D0%B5%D0%BF%D0%BF%D0%B5%D0%BB%D0%B8%D0%BD,_%D0%A4%D0%B5%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D0%BD%D0%B4_%D1%84%D0%BE%D0%BD), которые находились на берегу Боденского озера. Несмотря на достаточно подробную страницу в Википедии, почему-то про местонахождение этих заводов не сказано совсем ничего. Зато про самого г-на Цеппелина имеется очень много занимательных и интересных фактов.

**English:**

They were built at [Ferdinand von Zeppelin's](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.wikipedia.org/wiki/%D0%A6%D0%B5%D0%BF%D0%BF%D0%B5%D0%BB%D0%B8%D0%BD,_%D0%A4%D0%B5%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D0%BD%D0%B4_%D1%84%D0%BE%D0%BD) plants, which were located on the shore of Lake Constance. Despite the rather detailed page in Wikipedia, for some reason nothing is said about the location of these plants. But there are a lot of interesting and interesting facts about Mr. Zeppelin himself.

**Russian:**

> В 1863 году Цеппелин приезжает в США в качестве военного наблюдателя (в это время шла гражданская война между Севером и Югом). Здесь ему впервые довелось подняться на воздушном шаре. Это событие повлияло на всю его дальнейшую жизнь: именно после этого он решил заняться проблемами воздухоплавания. Вернувшись на родину, он продолжил военную службу; воевал в рядах прусской армии во время прусско-австрийской и франко-прусской войн. В 1870---1871 гг., во время франко-прусской войны, служа кавалерийским офицером, Цеппелин прославился как хороший разведчик. Для своей деятельности он использовал воздушные шары, с их помощью наблюдая за позициями и действиями противника.

**English:**

> In 1863 Zeppelin came to the United States as a military observer (at this time there was a civil war between North and South). Here, for the first time, he had the chance to climb a balloon. This event had an impact on his entire life: it was afterwards that he decided to tackle the problems of ballooning. Upon returning home, he continued his military service; he fought in the Prussian army during the Prussian-Austrian and Franco-Prussian wars. In 1870--1871, during the Franco-Prussian War, serving as a cavalry officer, Zeppelin became famous as a good scout. He used balloons for his activities, using them to observe enemy positions and actions.

**Russian:**

Странно, не правда ли? Человек едет на войну, идущую на другом континенте, и можно сказать рискует жизнью, выступая за одну из сторон. А может, та одна из сторон имела отношение к родине г-на Цеппелина, и располагалась на двух континентах? Впрочем, гражданская война в США есть одна из величайших тайн в истории. Делать выводы на основании официальных данных нет смысла. Тем не менее, возвратившись, г-н Цеппелин принимает участие в другой, не менее загадочной войне, и в том же качестве, с использованием воздухоплавательных аппаратов. Кстати, от той войны до нас из правдивого дошли только фото руин Страсбурга и Парижа. При желании их можно легко найти, они выложены на многочисленных ресурсах. Оба города, но более всего Страсбург, после бомбардировок напоминали знаменитую Хиросиму образца 1945 года. И нигде нет фото сражений с той войны. Чем же там сражались? Но вернёмся к г-ну Цеппелину.

**English:**

Weird, isn't it? Man goes to war on another continent, and you could say he risks his life fighting for one side. Or maybe one of the parties had something to do with Mr. Zeppelin's homeland and was located on two continents? However, the US civil war is one of the greatest mysteries in history. It makes no sense to draw conclusions from official data. Nevertheless, on his return, Mr. Zeppelin took part in another, no less mysterious war, and in the same capacity, using aeronautical devices. By the way, only pictures of the ruins of Strasbourg and Paris have reached us from that war. If you want, you can easily find them - they are laid out on numerous resources. Both cities, and most of all Strasbourg, resembled the famous Hiroshima model of 1945 after the bombing. And nowhere are pictures of the battles from that war. What were the battles? But let's go back to Mr. Zeppelin.

**Russian:**

> Был женат на уроженке Российской империи баронессе Изабелле фон Вольф (1846---1922), происходившей из дворян юго-восточной Ливонии. В браке родилась единственная дочь - Хелена (Хелла) фон Цеппелин (1879---1967), которая вышла замуж в 1909 году за Александра фон Брандштейна (1881---1949). Двоюродная сестра Изабеллы, София фон Вольф (1840---1919), вышла замуж за младшего брата Цеппелина, Эберхарда (нем.) русск. (1842---1906), историка, банкира и хозяина гостиничного бизнеса, супруги имели четырёх сыновей. Потомки графа Цеппелина по женской и мужской линиям здравствуют по сей день.

**English:**

> He was married to Baroness Isabella von Wolf (1846-1922), a native of the Russian Empire, who came from the nobility of south-eastern Livonia. His only daughter - Helena (Hella) von Zeppelin (1879-1967) - married Alexander von Brandstein (1881-1949) in 1909. Isabella's cousin Sophia von Wolf (1840-1919) married Zeppelin's younger brother Eberhardt (1842-1906), a historian, banker and owner of hotel businesses. The spouses had four sons. Female and male descendants of Earl Zeppelin are still around today.

**Russian:**

Как видим, обычные российские имена и фамилии того времени. Ни дать, ни взять. Что всё это значит? И вдовесок посмотрите на сам дирижабль производства заводов г-на Цеппелина. Там чётко и ясно прописано на борту перед его фамилией титул «graf». Не барон, не герцог, не фрайхерр, а именно граф. Что, Россия когда-то доходила до Боденского озера?

**English:**

As we can see, they have the usual Russian names of that time... Seriously! What does all this mean? And look at Mr. Zeppelin's blimp factory itself. It's got the graf title on board before his name. Not Baron, not Duke, not Freiherr, but Count. What? Did Russia ever get to Lake Constance?

**Russian:**

Не менее загадочна история того самого города [Фридрихсхафена](https://ru.wikipedia.org/wiki/%D0%A4%D1%80%D0%B8%D0%B4%D1%80%D0%B8%D1%85%D1%81%D1%85%D0%B0%D1%84%D0%B5%D0%BD), где находились заводы по изготовлению дирижаблей.

**English:**

No less mysterious is the history of that very city [Friedrichshafen](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.wikipedia.org/wiki/%D0%A4%D1%80%D0%B8%D0%B4%D1%80%D0%B8%D1%85%D1%81%D1%85%D0%B0%D1%84%D0%B5%D0%BD), where the airship factories were located:

**Russian:**

> В XVI веке начался упадок городской жизни, обусловленный как религиозными столкновениями, так и многочисленными войнами. Чрезвычайно выгодно расположенный, Буххорн был ареной Крестьянской войны 1525 года и Тридцатилетней войны, причём в последней, будучи занятый шведами в 1634 году, он был переименован в Густавсбург (Gustavsburg). В ходе Коалиционных войн в конце XVIII века Буххорн был в очередной раз разрушен, и в 1802 году согласно постановлениям Люневильского мира был передан Баварии, и затем в 1810 году по Парижскому договору отошёл Вюртембергу. В XIX веке город пережил свой расцвет: в целях торговли со Швейцарией и Австрией была значительно расширена гавань, а сам Фридрихсхафен, как теперь назывался Буххорн, стал летней резиденцией вюртембергских королей и превратился в курорт.

**English:**

> In the 16th century, urban life began to decline due to both religious clashes and numerous wars. Buchhorn, in an extremely advantageous location, was the scene of the Peasants' War of 1525 and the Thirty Years' War, in the latter, being occupied by the Swedes in 1634, it was renamed Gustavsburg (Gustavsburg). During the Coalition Wars at the end of the 18th century, Buchhorn was once again destroyed, and in 1802, according to the regulations of the Luneville Peace, it was handed over to Bavaria, and then in 1810, according to the Treaty of Paris, went to Wurttemberg. In the 19th century, the city experienced its heyday: in order to trade with Switzerland and Austria, the harbour was greatly expanded, and Friedrichshafen itself, as Buchhorn is now called, became the summer residence of the kings of Württemberg and became a resort.

**Russian:**

Оказывается, в 19 веке город был создан из ничего. Ранее на этом месте стоял город Буххорн, и он был разрушен неизвестной войной в начале 19 века. И кстати, случай далеко не единичен. История многих европейских городов берёт своё реальное начало вместе с началом 19 века. В то время произошло некое переформатирование мира, о чём есть масса свидетельств. О масштабе того события мы можем только догадываться.

**English:**

It turns out that the city was created out of nothing in the 19th century. Earlier on this site stood the town of Buchhorn, and it was destroyed by an unknown war in the early 19th century. And by the way, the case is not an isolated one. The history of many European cities has their real founding at the start of the 19th century. At that time there was some kind of reformatting of the world. For this there is plenty of evidence. We can only guess at the scale of the event.

**Russian:**

А что же с нашими заводами г-на Цеппелина?

**English:**

What about our Mr Zeppelin's factories?

<!-- Local
![](Everybody Dances_files/12-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/12-1.jpg)

<!-- Local
![](Everybody Dances_files/12-3.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/12-3.jpg)

**Russian:**

Они действительно в этом городе были, в различных архивах есть тому масса доказательств. При этом есть информация, что самый первый завод графа был построен в виде плавучей платформы -- у него не было средств на приобретение земли. Уже в начале 20 века заводы пришли к тому виду, в котором мы можем наблюдать их на фото. Эти заводы простояли до середины 20 века, а потом внезапно исчезли вместе с дирижаблями.

**English:**

They really were in this town, there's a lot of evidence in various archives. However, there is information that the Count's very first factory was built as a floating platform - he did not have the funds to purchase land. Already by the beginning of the 20th century, the factories had developed to the point where we can see them in the photo. These factories stood idle until the middle of the 20th century and then suddenly disappeared along with the dirigibles.

<!-- Local
![](Everybody Dances_files/13-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/13-1.jpg)

**Russian:**

Как вы наверное уже поняли, сейчас на месте тех заводов не осталось абсолютно ничего, что могло о них напоминать. Где-то на побережье Боденского озера есть музей воздухоплавания, и не более того. Всё это выглядит очень странно, зная любовь европейцев к архитектурному наследию. Но... не будем расстраиваться по этому поводу. Благо, с появлением цифровых архивов появилось много пищи для размышлений.

**English:**

As you have probably already understood, now there is absolutely nothing left on the site of those plants; nothing that could remind anyone of them. Somewhere on the shore of Lake Constance there's a museum of aeronautics, and nothing more. All this looks very strange, knowing the Europeans' love for architectural heritage. But... let's not get upset about it. With the advent of digital archives, there's a lot of food for thought.

**Russian:**

Для начала проведём архивный поиск по тэгу «Friedrichshafen». Неожиданно компьютер выдаёт странный результат.

**English:**

Let's start with an archival search for the "Friedrichshafen" tag. Suddenly the computer produces a strange result...

<!-- Local
![](Everybody Dances_files/14.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/14.jpg)

**Russian:**

Оказывается, ранее уже существовала некая звезда-крепость Friedrichshafn. При этом данный объект совершенно не коррелирует с современным Фридрихсхафеном на берегу Боденского озера. Совсем, следов подобной звезды-крепости не видно как на его спутниковой съёмке, так и на старых планах. Собственно, ничего удивительного, на том же плане звезды-крепости Friedrichshafn есть пометка, что этот объект находится в составе города Kopenhafn, он же современная столица Дании Копенгаген, или как в старину по русски, Купи-гавань. Очевидно, там был крупный торговый центр, стоящий на перекрёстке водных путей.

**English:**

It turns out that there used to be a kind of Friedrichshafn star-fortress. This object does not correlate at all with the modern Friedrichshafen on the shore of Lake Constance. No trace of such a star-fortress can be seen on Lake Constance's Friedrichshaven's satellite imagery at all, nor on old plans. In fact, it's no surprise that on the same Friedrichshafn star plan there is a note that this object is part of the city of Kopenhafn, the modern capital of Denmark. Copenhagen, or as it was known in old Russian, Buying Harbor (or 'Market Harbour'). Obviously, there was a large shopping mall standing at its intersection of waterways.

<!-- Local
![](Everybody Dances_files/15-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/15-1.jpg)

**Russian:**

На современной спутниковой карте Копенгагена действительно имеется схожий объект, только называется он Крепость Кастеллет. Ни в одном источнике нет упоминаний о том, что ранее эту крепость называли Friedrichshafn (могу ошибаться, знатоки Копенгагена, вам слово). Откуда же взялась наша звезда-крепость Фридрихсхафен?

**English:**

There is indeed a similar object on a modern satellite map of Copenhagen, only it is called Castellet Fortress. There is no mention in any source that this fortress was previously called Friedrichshafn (I may be mistaken, connoisseurs of Copenhagen, your comments please). Where did our Friedrichshafn star-fortress come from?

<!-- Local
![](Everybody Dances_files/16.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/16.jpg)

**Russian:**

Здесь приведена старинная карта Копенгагена, и там тоже нет объекта с именем Friedrichshafn. Что тут можно сказать? Карта вполне реалистична, спутниковой съёмке соответствует, хотя и не ориентирована на север. Пятиугольная крепость присутствует, и даже плану отдельно взятой звезды-крепости Friedrichshafn она вроде бы соответствует.

**English:**

Here is an ancient map of Copenhagen, and there is no object named Friedrichshafn there either. What is there to say? The map is quite realistic, it is consistent with the satellite imagery, although it is not oriented to the north. The pentagonal fortress is present, and there is even the plan of a single star-fortress. It seems to correspond with Friedrichshafn.

<!-- Local
![](Everybody Dances_files/17.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/17.jpg)

**Russian:**

Если встать на дорожку между разными уровнями звезды-крепости Кастеллет в Копенгагене и посмотреть вперёд, то можно увидеть пейзаж, как на этой панораме Гугл. И есть одно интересное свойство, на которое сейчас обращает внимание далеко не каждый посетитель той достопримечательности. Уровень воды в каналах по разные стороны водораздела в настоящее время одинаков. Это не удивительно, никто сейчас не будет восстанавливать на подобных сооружениях ни источник воды в центре, ни систему шлюзов, как было в старые добрые времена. Сохранилась крепость, и очень хорошо, выровняли её ярусы и запустили туристов. Но в настоящих старинных крепостях такого не бывало никогда, все внутренние каналы на разных ярусах имели разную высотную отметку. Так что же, наш Friedrichshafn на старинном плане и Копенгаген -- разные географические места? Впрочем, всем известно, что тот район постепенно уходит под воду, и начался этот процесс очень давно. Оставим этот вопрос без ответа.

**English:**

If you stand on the path between the different levels of the Castellet star-fortress in Copenhagen and look ahead, you see a landscape like this Google panorama. And there is one interesting feature that not every visitor to that landmark pays attention to now. The water levels in the canals on different sides of the watershed are now the same. This is not surprising, no one would restore on such structures either the source of water in the center, nor the system of locks. Not now, not as they were in the good old days. The fortress has been preserved, and very well; its tiers have been levelled and set up for tourists. But this never happened in real ancient fortresses, all the internal canals on different tiers had different heights. So are the Friedrichshafn we see on the ancient plan and the Copenhagen we see in Google and old maps different geographical places? Let's leave this question unanswered but note that we all know that the area is gradually sinking under water and that this process began a very long time ago.

**Russian:**

Но, цепляясь за логику поиска в цифровых архивах, делаем новый поиск, уже по тэгу «Kopenhafn». И снова находим странности.

**English:**

Nevertheless, clinging to the logic of search in digital archives, we do a new search, this time on the tag "Kopenhafn". And once again, we find strange things.

<!-- Local
![](Everybody Dances_files/18.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/18.jpg)

**Russian:**

Что это? Согласно аннотации это тот же Копенгаген, но только вот он как-то совершенно не соответствует современному виду датской столицы. Хотя крепость Friedrichshafn в данном случае почти идеально соответствует плану. Даже четырёхугольный замок в форме колодца, находящийся внутри крепости, прорисован почти идеально. Что за чудеса?

**English:**

What's that? According to the annotation it is the same Copenhagen, but it somehow does not correspond to the modern look of the Danish capital. Although the fortress of Friedrichshafn in this case almost perfectly matches the plan. Even the four-sided well-shaped castle inside the fortress is almost perfectly painted. What are the wonders?

<!-- Local
![](Everybody Dances_files/19.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/19.jpg)

**Russian:**

Это, очевидно, один и тот же объект. Но совсем не Копенгаген, который сейчас в Дании. Сколько же было на этом свете Копенгагенов? Как вы думаете, существуют на этом свете города-клоны? Впрочем, существуют. Есть например известный город Каменец-Подольский. Сейчас уже мало кто знает, что у него есть город-близнец Каменец-Немецкий, он же современный Chemnitz в Германии, он же чуть ранее Karl-Marx-Stadt. Время распорядилось так, что эти города оказались в разных странах, хотя ранее они явно были в одной. Как же так? Может, и у Копенгагена был брат-близнец в какой-нибудь Кемской волости, которую, как известно, кому-то там отдали, но потом вернули?

**English:**

It's obviously the same object. But not at all Copenhagen, which is now in Denmark. How many Copenhagens were there in this world? Do you think there are clonal cities? There are though. There is, for example, the famous city of Kamyanets-Podilsky. Now, few people know that it has a twin city - Kamenets - modern Chemnitz in Germany, and called a little earlier 'Karl-Marx-Stadt'. Time dictated that these cities were in different countries, although previously they had clearly been in one. How so? Maybe Copenhagen also had a twin brother in some parish in Kem, which was known to have been given to someone there but then returned?

<!-- Local
![](Everybody Dances_files/20-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/20-1.jpg)

**Russian:**

Кстати, мало кто знает, что город Кемь получил своё название с лёгкой руки Екатерины Великой. На прошениях от своих соратников с просьбой не казнить провинившихся она, если сжаливалась, писала «сослать к е.м.», или к такой-то матери. Несмотря на то, что Екатерина была немкой, русскую культуру она впитала очень хорошо. Странная в то время была страна. Вот именно, странная. А Кемь, скорее всего, был собирательный образ мест, очень далёких и малопригодных для проживания. Возможно, там и построили впоследствии одноимённый город.

**English:**

By the way, few people know that the city of Kem got its name from the easy hand of Catherine the Great. On petitions from her comrades-in-arms not to execute the guilty she - feeling compassionate - wrote "exile to e.m.". Despite the fact that Catherine was German, she absorbed Russian culture very well. It was a strange country at the time. That's right, strange. And Kem, most likely, was a collective image of places very distant and uninhabitable. Perhaps that's where the city of the same name was built.

**Russian:**

Так где же был наш дубль-Копенгаген? Может, как раз на Боденском озере?

**English:**

So where was our twin Copenhagen? Maybe just on Lake Constance?

**Russian:**

Но как не примерять наши старинные карты на существующие города восточного побережья озера, они даже отдалённо не напоминают ничего похожего. Зато там есть другие интересные населённые пункты, такие, как Вассербург (Wasserburg). По легенде, наш граф Цеппелин имел здесь тоже некое предприятие, связанное с дирижаблестроением.

**English:**

But don't try matching our old maps of the existing cities on the east shore of the lake - they don't even remotely resemble anything similar. But there are other interesting settlements, such as Wasserburg. Legend has it that our Count Zeppelin also had some sort of airship-building enterprise here.

<!-- Local
![](Everybody Dances_files/21-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/21-1.jpg)

**Russian:**

Может, это и есть остатки того загадочного Копенгагена, планы которого мы случайно нашли? Но нет, этот город также никак не относится к старинным планам Копенгагена, хотя, судя по названию (Водный замок), он имел все шансы им стать. Но... снова мы имеем некие странности, если произвести поиски в архивах, но уже по тэгу «Wasserburg»

**English:**

Maybe these are the remains of that mysterious Copenhagen we accidentally found? But no, Wasserburg has nothing to do with the ancient plans of Copenhagen either, although judging by the name - Water Castle - it has every chance of becoming one. But... again, we find some strange things going on when we search the archives for things tagged 'Wasserburg'...

<!-- Local
![](Everybody Dances_files/22.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/22.jpg)

**Russian:**

Оказывается, таких «водных замков» было много. Этот, с гравюры, был где-то в Германии, и даже художник отразил нападение на этот город французов и шведов. Данный город находился не на берегу Боденского озера. Где же его искать? И какое отношение здесь имеет ко всему граф Цеппелин? Обратите внимание на обведённую гору, она нам понадобится чуть позже.

**English:**

Turns out there were a lot of "water castles" like that. This one, with an engraving, was somewhere in Germany, and even the artist repelled the attack on this city by the French and Swedes. This city was not on the shore of Lake Constance. Where to look for it? And what does Count Zeppelin have to do with anything here? Pay attention to the circled mountain, we'll return to it later.

**Russian:**

Вопросы так бы и остались без ответа, если летом не пришлось побывать в командировке. Нет, не на Боденском озере (не дорос ещё), а гораздо прозаичнее. На Чебоксарском водохранилище, что в общем почти как Кемская волость. И привлёк там внимание один странный креативный элемент местной туриндустрии.

**English:**

These questions would have remained unanswered if we hadn't had to go on a business trip this summer. No, not on Lake Constance (not yet), but much more prosaic. On the Cheboksary reservoir, which is almost like the Kemskaya parish. And one strange creative element of the local tourism industry attracted attention there.

<!-- Local
![](Everybody Dances_files/23-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/23-1.jpg)

**Russian:**

Это всего лишь камень, на котором некто высек карту древних марийских городищ, находившихся ранее в районе впадения реки Суры в Волгу. И среди иноязычных местных названий как-то странно диссонирует как по звучанию, так и по сочетанию букв городище Цеппель. Что это? Сейчас на этом месте стоит обычный рабочий посёлок Васильсурск, даже не райцентр, а так, просто большая деревня. Ранее Васильсурск был достаточно известным уездным городом. И ещё знаменит он тем, что стал прообразом города будущего Нью-Васюки из известного произведения Михаила Булгакова (Ильфа и Петрова). А где же Цеппель? Может, он как раз и был в этом месте самым главным городом прошлого?

**English:**

It's just a stone on which someone made a map of the ancient Mari settlements that used to be situated at the confluence of the Sura river and the Volga. And among the foreign language local names is "Zeppel Hill Fort". It's somehow strangely dissonant both in sound and in combination of letters. What is it? In this place today there stands an ordinary working settlement: Vasilsursk: it's not even a district center - just a big village. Vasilsursk used to be a quite famous county town. And it is also famous for being a prototype of the city of the future: New Vasyuki from the famous work of Mikhail Bulgakov (Ilf and Petrov). And where is Zeppel? Perhaps the most important city of the past was in this very place?

<!-- Local
![](Everybody Dances_files/24-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/24-1.jpg)

**Russian:**

Таким Васильсурск стал после подъёма воды в Волге, когда ввели в строй Чебоксарскую ГЭС и наполнили то самое водохранилище. Вода поднялась на высоту около 10 м и залила все нижние улицы посёлка. Исчезла знаменитая лестница, по которой Великий комбинатор убегал со своего шахматного турнира. Жители были вынуждены переселяться вверх, для чего разбирали дома и перевозили их на вершину стоявшего холма, где по преданиям ранее стояла крепость Цеппель. А каким посёлок был ранее, лет сто назад?

**English:**

This is how Vasilsursk looked after the Cheboksary hydropower plant was put into operation and the Volga river's level had risen when the reservoir was filled. The water rose to a height of about 10m and flooded all the lower streets of the settlement. The famous staircase where the Great Combinator ran away from his chess tournament disappeared. The inhabitants were forced to move to higher ground, for which they dismantled the houses and transported them to the top of the hill. There, according to legends, there used to be the Zeppel fortress. And what was the village like a hundred years ago?

<!-- Local
![](Everybody Dances_files/25-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/25-1.jpg)

**Russian:**

Да это же та самая гора, что обведена на гравюре обороны Вассербурга. А если применить тот принцип, который ранее изложен в эпиграфе?

**English:**

It's the same mountain that's circled on the engraving of Wasserburg's defense. And if you apply the principle previously set out in the epigraph at the start of this piece?

**Russian:**

> Город, как тебя зовут?

> [Васильсурск](https://ru.wikipedia.org/wiki/%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D1%81%D1%83%D1%80%D1%81%D0%BA).

> Точнее?

> Wessel-град (кстати, веселящий газ в «сосуде» есть гелий, любители воздушных шаров поймут, о чём речь).

> Ещё точнее?

> Wasserburg (ну и крепость Цеппель, стоявшая внутри него на той самой горе).

**English:**

> Q: City, what's your name?

> A: [Vasilsursk](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.wikipedia.org/wiki/%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D1%81%D1%83%D1%80%D1%81%D0%BA).

> Q: Specifically?

> A: Wessel-grad (by the way, there is "helium" in the word "vessel", balloon lovers will understand what we are talking about).

> Q: More specifically?

> A: Wasserburg (and Zeppel fortress that stood inside on the same mountain).

**Russian:**

М-да... Судя по тому, что в конце современного названия находится корень «сурск», на слияниях других рек с Волгой тоже стояли вессель-грады. А что, вполне технологично. Если сделать такую же башню для дирижаблей, как в Севастополе на фото (см.выше), можно очень удобно добираться до дальних загородных дач на берегах рек. Собственно, в 10 км от Васильсурска на берегу Волги стоит замок тех самых Шереметьевых. Совершенно официально на экскурсиях там рассказывают, что из этого замка Шереметьевы ездили в Швейцарию, как будто их в замке постоянно ждал "Экспресс". Так куда там ходил Иван Грозный и на чём?

**English:**

Yeah... Judging by the fact that at the end of the modern name is the root "sursk", at the confluence of other rivers with the Volga there were also vessel-grads. Why, it is quite technological. If they made a similar tower for airships, as the one in Sevastopol in the photo above, it would be very convenient to get to distant suburban dachas on the banks of rivers. Actually 10km from Vassilsursk stands the castle of the same Sheremetevs on the bank of the Volga. Even on its official tours they say that the Sheremetevs went to Switzerland from this castle, as if the "Express" was constantly waiting for them in the castle. So where did Ivan the Terrible go and in what?

**Russian:**

Может, мы всё же ошибаемся? Давайте смотреть карту.

**English:**

Maybe we're wrong after all. Let's see the map.

<!-- Local
![](Everybody Dances_files/26-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/26-1.jpg)

**Russian:**

Всё очень похоже, вот только большую часть древнего города затопило Чебоксарским водохранилищем, и река Сура сменила русло при впадении в Волгу. А какой у этого города герб?

**English:**

Everything is very similar, but most of the ancient city was flooded by the Cheboksary reservoir, and the Sura River changed its course at the confluence of the Volga. And what is the coat of arms of this city?

<!-- Local
![](Everybody Dances_files/27-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/27-1.jpg)

**Russian:**

Посмотрите внимательно на обведённую часть. Интересный герб, не правда ли? А что за странный аппарат изображён в гербе? В 20 веке от него политкорректно оставили только скелет и придумали легенду, что в лесах вокруг этого места росли самые лучшие корабельные сосны. Кстати, есть легенда, что «Пётр» привёз в эти края голландцев, очевидно, строить Нью-Копенгаген. Так что это за аппарат в гербе? Который строили в Цеппеле? И где должен быть всемирный город будущего Нью-Васюки, который удачно троллил Булгаков в своём произведении, видимо, зная настоящую историю этих мест?

**English:**

Look closely at the circled part. It's an interesting coat of arms, isn't it? And what kind of strange device is depicted in the coat of arms? In the 20th century it was politically correct to leave only the skeleton and someone invented a legend that the best ship pines grew in the forests around this place. By the way, there is a legend that "Peter" brought the Dutch to these parts, apparently, to build New Copenhagen. So what's that machine in the coat of arms? The one that was built in Zeppel? And where should be the world city of the future New Vasuka, which successfully trolls Bulgakov in his work, apparently knowing the real history of these places?

**Russian:**

> Вдруг, как в сказке, скрипнула дверь.

**English:**

> Suddenly, like in a fairy tale, the door squeaks.

**Russian:**

> И всё мне ясно, стало теперь.

**English:**

> And now it's clear to me.

<!-- Local
![](Everybody Dances_files/29-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/29-1.jpg)

**Russian:**

Нет этого города, и уже давно. Остаётся только танцевать, глядя как Иван Грозный брал Казань, по легенде ехав на лошадке через Васильсурск. А был тот город, скорее всего, примерно таким. И так футуристично показывали Нью-Васюки в экранизации того самого произведения Булгакова.

**English:**

There is no such city, and has not been for a long time. We can only dance, looking at how Ivan the Terrible took Kazan, according to legend, by riding a horse through Vasilsursk. And that city was probably something like this. And that's how futuristic New Vasyuki was portrayed in the film adaptation of Bulgakov's work.

<!-- Local
![](Everybody Dances_files/28-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/28-1.jpg)

**Russian:**

Но увы... его разбомбили, и вдребезги. Примерно в то время, когда разбомбили Севастополь. Семейный бизнес графов Цепéльских по строительству летательных аппаратов переехал на побережье Боденского озера, бросив свой фамильный город, ну или то, что от него осталось. И все с тех мест бежали на запад от той неизвестной нам войны. Болгары (волгари) -- в Болгарию. Джунгары (хунну) -- в Венгрию. Кто ещё?

**English:**

But alas... ...it was bombed and shattered. Around the time Sevastopol was bombed. The counts of Cepelsky's family business building aircraft moved to the coast of Lake Constance, leaving his family town, or what was left of it. And everyone from those places fled west from that unknown war. The Bulgarians (Volgari) - to Bulgaria. The Jungars (Hunnu) to Hungary. Who else?

**Russian:**

Один раз, в советское время, был слегка удивлён рассказу одного знакомого. Он считал себя чистокровным мордвином, и знал в совершенстве мордовский язык. И когда его призвали на воинскую службу в часть на территории Венгрии (тогда ещё), он начал там общаться с местными жителями на мордовском языке и они неплохо понимали друг друга. При этом венгры совершенно не знали, откуда им достался такой язык. Внешне они выглядели как обычные европейцы. Впрочем, мордва от венгров в этом плане тоже не сильно отличалась.

**English:**

Once, in Soviet times, I was a little surprised by the story of an acquaintance. He considered himself a pure-blooded Mordvinist, and knew the Mordovian language perfectly well. And when he was called up for military service in the territory of Hungary (at that time), he began to communicate with the locals there in the Mordovian language, and they understood each other well. The Hungarians didn't know where they got their language from at all. Outwardly, they looked like ordinary Europeans. However, the Mordovians did not differ much from the Hungarians in this respect either.

**Russian:**

А брал ли Иван Грозный Казань? Может, наоборот отступал туда во время неизвестной войны, а затем в Астрахань? А может, это всё также было большой полностью придуманной сказкой для обывателей?

**English:**

And did Ivan the Terrible take Kazan? Maybe, on the contrary, he retreated there during the unknown war, and then to Astrakhan? Or maybe this was also a big, fully invented fairy tale for the inhabitants of Astrakhan?

<!-- Local
![](Everybody Dances_files/30-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/30-1.jpg)

**Russian:**

Впрочем, по порядку. Начнём с Васильсурска. Всё это замечательно, но слишком смело. Давайте разбирать, для чего приведём цитаты из местных краеведческих ресурсов.

**English:**

However, in order. Let's start with Vasilsursk. This is all great, but too bold. Let's take it apart, for which we will quote from local history resources.

**Russian:**

Есть примечательная информация [здесь](http://francomania.ru/obzory/183-vasilsursk-gorod-pod-oblakami.html):

**English:**

There is some noteworthy information [here](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://francomania.ru/obzory/183-vasilsursk-gorod-pod-oblakami.html):

**Russian:**

> Весьма интересное и загадочное место находится, приблизительно, в 9 километрах от Васильсурска на кряжистом возвышении гор, меж двух оврагов в густом лесу: Чёртово Городище. Городище -- круглая площадка около 60 метров в длину и около 50 в ширину с остатками крепостного вала и небольшого рва. На площадке сохранились и следы внутренней постройки. Об этом месте ходят несколько легенд. В первой говориться, что жил в Чёртовом городище очень жестокий пан. Вторая утверждает, что здесь было пристанище Стеньки Разина и это исторически возможно: к тому же при раскопках были найдены боевые топоры. И третья, тоже вполне правдоподобная, говорит о том, что Чёртово городище -- притон страшного атамана Галани который никого не отпускал живым. Говорят, что Галаня спрятал здесь множество кладов, но все они прокляты, так как из-за них пролилось много человеческой крови, но это не останавливает множество искателей кладов, которые успешно находят старинные монеты и даже сундуки с сокровищами. Ходят так же рассказы, что по ночам здесь слышны стоны душ разбойников, не нашедших себе покоя. И по преданиям марийского народа на Чёртовом городище (Орла нер -- по-марийски «чёртов нос») зарыт «ботничек», то есть лодка, полная золота, охраняемая чёртом и обычному человеку, не знающему ворожбы, не найти его. Ходят слухи и об аномальности данного места: у людей долго пробывших на Чёртовом городище отстают часы, так как время там тянется медленнее.

**English:**

> A very interesting and mysterious place is located approximately 9 kilometers from Vasilsursk on a steep hill between two ravines in a dense forest: the Chyortovo Gorodishche. Devil's Hill Fort - a rounded area about 60 meters long and about 50 wide with the remains of a rampart and a small ditch. Some traces of an inner building have been preserved. There are several legends about this place. The first one says that a very cruel lord used to live in the Devil's Mound. The second one says that Stenka Razin's home was located here, and it is historically possible: moreover, military axes were found during the excavations. And the third, also quite plausible, says that the Devil's Fortress is a hideout of terrible ataman Galani, who never let anyone live. Galani is said to have hidden many treasures here, but they are all cursed, because so much human blood has been spilled because of them. But this has not stopped many treasure hunters, who successfully find ancient coins and even treasure chests. There are also stories that at night you can hear the groans of the souls of thieves who have not found rest. And according to the legend of the Mari people, a boat full of gold is buried  on the Devil's Mound ("Orla ner" in Mari: "Devil's Nose"). It is guarded by the devil, and no ordinary person can find it unless they not know witchcraft. There are rumors about the abnormality of this place: people who have been on the Devil's Nose for a long time have lagging hours, because time stretches slower there.

**Russian:**

Если выжать всю воду и отбросить сказки, то можно понять, что существует некое место с сохранившимися стенами от того самого города Вассербурга. Собственно, именно там и находится тот камень, на котором набиты старинные города. Не без труда удалось найти, что данное «чёртово городище» находится неподалёку от современной деревни Барковка.

**English:**

If you squeeze out all the water and discard the fairy tales, you can understand that there is a certain place with preserved walls from that very city of Wasserburg. As a matter of fact, it is there that the stone on which the ancient cities are inscribed is found. Not without difficulty I managed to find out that this "Devil's Hill Fort" is located near the modern village of Barkovka.

<!-- Local
![](Everybody Dances_files/31-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/31-1.jpg)

**Russian:**

Очень даже может быть, что мы на правильном пути. Во время бомбардировки этого места тот дальний угол мог уцелеть, и его просто засыпало грунтом, которым засыпало в то время всё вокруг. Возможно, в том месте грунтовые массы смыло водой, или даже там расчистили местные жители для хозяйственных нужд, придумав кучу красивых легенд. Смотрим дальше, в [следующий ресурс](https://nn.mk.ru/articles/2013/07/10/881829-opolzen-v-vasilsurske-v-mgnovene-unichtozhil-krepost-hramyi-i-doma.html).

**English:**

We may very well be on the right track. During the bombing of this place, that far corner could have survived, and it just fell asleep with the soil that was all around it at the time. Perhaps the ground washed away with water in that place, or even the locals cleared it up for household use, inventing a bunch of beautiful legends. Let's move on, to [the next resource](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://nn.mk.ru/articles/2013/07/10/881829-opolzen-v-vasilsurske-v-mgnovene-unichtozhil-krepost-hramyi-i-doma.html).


**Russian:**

> В самом начале XVI столетия Волга протекала не там, где теперь. Сура впадала в нее у современного устья Хмелевки. Васильсурск находился как бы на полуострове, отрезанном от Большой Земли с востока устьем Суры, на юге -- притоком Суры, рекой Чугункой, а на северо-западе, кроме мелких речушек вроде Калиновца, никаких водных препятствий для того, чтобы пешим путем попасть в Фокино или Сомовку, по сути дела, не было. Волга была в нескольких верстах от Васильсурска. Но спустя полвека, к 1556 году, Волга подошла к городу вплотную, сделав гигантский «прыжок» с северо-запада на юг. Она полностью вобрала в себя низовья Суры, которая, в свою очередь, сместилась далеко на запад. В результате таких природных пертурбаций Сура была укорочена едва ли не на десяток километров. Коренным образом изменилось направление течения рек, и, как свидетельствует летописец, «оползла гора великая, на которой кремль стоял, да и в реку ушла, и покрыли воды речные все постройки каменные».

**English:**

> At the very beginning of the 16th century, the Volga flowed not where it is now. The Sura flowed into it at the modern mouth of Khmelevka. Vasilsursk was located on a peninsula, cut off from Bolshaya Zemlya to the east by the mouth of Sura, to the south by a tributary of Sura, Chugunka river, and in the northwest, except for small rivulets like Kalinovets, there were no water obstacles to get to Fokino or Somovka on foot. The Volga was a few versts from Vasilsursk. But half a century later, by 1556, the Volga came close to the city, making a giant "jump" from northwest to south. It completely absorbed the lower reaches of Sura, which, in turn, shifted far to the west. As a result of such natural perturbations Sura has been shortened by nearly ten kilometers. The direction of the rivers flow radically changed, and, as the chronicler testifies, "the great mountain, on which the Kremlin stood, slid down and went into the river, and the river waters covered all the stone buildings".

**Russian:**

Похоже, всё становится ясным -- после бомбардировки и выпадания грунта исчез тот самый ров, который шёл вдоль юго-восточной стены Вассербурга и который являлся старым руслом Суры. Сура в месте впадения в Волгу потекла на запад, пробив новое русло, так она течёт и сейчас, а Волга наоборот сместилась на восток, и с тех пор вплоть до наполнения Чебоксарского водохранилища реки свой фарватер не меняли.

**English:**

It seems that everything becomes clear - after the bombing and the fallout, the ditch that ran along the southeast wall of Wasserburg, which was the old riverbed of Sura, disappeared. The Sura flowed westward at its confluence with the Volga, and so it still flows, while the Volga, on the contrary, shifted to the east, and since then, until the Cheboksary reservoir was filled up, the rivers did not change their courses.

<!-- Local
![](Everybody Dances_files/32-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/32-1.jpg)

**Russian:**

Обратите внимание на выкопировку из старинной карты Васильсурска, в левом верхнем углу. Очевидно, границы Вассербурга составителям карты были известны, и они нанесли их тёмно-красной линией. Т.о., действительно часть территории города ушла под воды Волги, даже ещё задолго до наполнения Чебоксарского водохранилища. И как ни странно, современная административная граница между Нижегородской областью и республикой Марий Эл идёт строго по этой линии, порою не совсем логично пересекая острова и водные преграды. Составители современных границ российских регионов даже не пытались скрыть подобные ляпы. Глядишь, побольше вникая в карту современных областей и республик, найдёшь по кусочкам всю настоящую Российскую Империю, погибшую в той неизвестной нам войне.

**English:**

Note the excerpt from the old map of Wasserburg, in the upper left corner. Apparently, the mapmakers knew the borders of Wasserburg, and they plotted them with a dark red line. Thus, indeed, part of the territory of the city went under the waters of the Volga, even long before the filling of the Cheboksary reservoir. And strangely enough, the modern administrative border between Nizhny Novgorod Oblast and the Republic of Mari El runs strictly along this line, sometimes not entirely logically crossing islands and water obstacles. The drafters of modern borders of Russian regions did not even try to hide such blunders. You see, by delving more into the map of modern regions and republics, you will find the pieces of the whole real Russian Empire, which perished in that unknown war.

**Russian:**

Что там ещё может быть интересного?

**English:**

What else could be interesting there?

**Russian:**

В различных источниках упоминается о «Супротивном ключе» - роднике, который течёт в священной Цеппельской роще, месте поклонений марийцев. Эта роща находится на восточном краю бывшей крепости Цеппель. Ещё Васильсурск очень любили художники за прекрасные высокогорные пейзажи. Какое-то время в городе жили и работали Шишкин и Левитан. Последний, по легенде, находился в Васильсурске в глубокой депрессии, оставался один (как пелось в какой-то песне) и «пил горькую». Личность творческая, можно понять. А то место, где Шишкин стоял на обрывистом берегу и писал картины, так и назвали Шишкин мыс. Это есть одна из достопримечательностей Васильсурска по сей день.

**English:**

Various sources mention the "Suprachnyi Klyuch," a spring that flows in the sacred Zeppel Grove, a place of Mari worship. This grove is located on the eastern edge of the former Zeppel fortress. Even Vasilsursk was loved by artists for its beautiful mountain scenery. For a while Shishkin and Levitan lived and worked in the city. The latter, according to legend, was in Vasilsursk in a deep depression, stayed alone (as sung in a song) and "drinking bitter". Creative personality, you understand. And the place where Shishkin stood on the steep shore and painted, now called Shishkin cape. It is one of the attractions of Vasilsursk to this day.

**Russian:**

А что за «замок принца» указан на плане «Копенгагена» (для наглядности план перевёрнут)? Его можно как-нибудь идентифицировать на современной местности?

**English:**

What is the "prince's castle" on the "Copenhagen" plan (the plan is reversed for clarity)? Is there any way to identify it on the present-day terrain?

<!-- Local
![](Everybody Dances_files/33.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/33.jpg)

**Russian:**

Неожиданно выдался сенсационный результат.

**English:**

Suddenly there was a sensational result.

<!-- Local
![](Everybody Dances_files/34-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/34-1.jpg)

**Russian:**

М-да.. А что там вообще за [многофункциональный радиокомплекс](https://ru.wikipedia.org/wiki/%D0%A1%D1%83%D1%80%D0%B0_%28%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%29)?

**English:**

Uh, yeah... What's with the [multifunction radio complex](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.wikipedia.org/wiki/%D0%A1%D1%83%D1%80%D0%B0_%28%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%29)?

**Russian:**

> Принцип действия «Суры» - излучённый установкой узкий пучок радиоволн мощностью приблизительно до 200 МВт поглощается в ионосфере Земли, вызывает её нагрев и другие локальные возмущения. Изменения в поведении частиц, в полях и волнах регистрируются наземными и космическими датчиками. Нагревательный стенд «Сура» - единственный расположенный в средних широтах среди подобных установок по всему миру, это позволяет проводить эксперименты на относительно спокойных областях ионосферы.

**English:**

> The Sura's principle of operation is an approximately 200MW plant that emits a narrow beam of radio waves that are absorbed by the Earth's ionosphere, causing its heating and other local disturbances. Changes in particle behaviour, fields and waves are recorded by ground and space sensors. The Sura heating bench was the only one located at mid-latitudes among similar facilities around the world, allowing experiments to be conducted on relatively calm areas of the ionosphere.

**Russian:**

Поскольку сам радист, то могу смело утверждать -- это чушь (особенно про излучение узкого пучка волны антенными полями). Нагрев воздуха, может, и не чушь, но с ионосферой что-то перебрали. Интересно, как они там фиксировали температуру нагрева? Наверное, термометром. Ну точно чушь. А в свете того, что на месте этого комплекса ранее стоял некий «замок принца» - это чушь в квадрате. Гарантию даю, там есть подземный ход как минимум до Нижнего Новгорода. И по нему идёт какой-нибудь секретный кабель. И по кабелю идёт сигнал, который как раз транслируется в атмосферу на низких частотах антенными полями. Это связь сильных мира сего с большим радиусом действия, и интуиция подсказывает, что когда-нибудь в будущем этот кабель, как нить следственного клубка, выдаст всех, кто им пользовался. И новое поколение вытрясет за балонеты из тех людей всю правду о том, кто всё таки затеял эту неизвестную войну и почти 200 лет её скрывал. Впрочем, мы отвлеклись.

**English:**

As a radio operator myself, I can safely say that this is nonsense (especially about the radiation of a narrow wavelength beam by antenna fields). Air heating may not be nonsense, but there is something wrong with the 'heating the ionosphere' part. I wonder how they recorded the heating temperature there? Probably with a thermometer. Well, that's bullshit for sure. And in light of the fact that there previously stood a kind of "prince's castle" at the site of this complex - it's nonsense squared I guarantee, there is an underground passage at least to Nizhny Novgorod. And there's some kind of secret cable running through it. And the cable carries a signal, which is just broadcast into the atmosphere at low frequencies by antenna fields. It's a long-range communication of the powerful, and my intuition tells me that sometime in the future this cable, like a thread in an investigative tangle, will give away someone who has used it. And the new generation will shake all the truth from those people for baubles about who started this unknown war and concealed it for almost 200 years. However, we have digressed.

**Russian:**

У кого-нибудь ещё есть сомнения, что наш Васильсурск имеет все шансы стать Нью-Васюками, как того хотел Великий комбинатор? Давайте подведём итог.

**English:**

Does anyone else have any doubts that our Vasilsursk has every chance of becoming New Vasyuki, as the Great Combinator wanted? Let's sum it up.

<!-- Local
![](Everybody Dances_files/35-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/35-1.jpg)

**Russian:**

Позиция 1 -- знаменитый Покровский собор в Васильсурске, ныне не существующий.

**English:**

Position 1 is the famous Intercession Cathedral in Vasilsursk, now defunct.

<!-- Local
![](Everybody Dances_files/36-1.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/36-1.jpg)

**Russian:**

Город был сильно подвержен оползням, они проходили там за всю известную историю очень часто. Последний сильный оползень там был зафиксирован в 1979 году, с наполнением Чебоксарского водохранилища. Уровень воды в Волге тогда был поднят на значительную величину, примерно как на фото.

**English:**

The city was very prone to landslides; they have occurred frequently there through its entire known history. The last major landslide there was recorded in 1979, with the filling of the Cheboksary reservoir. The water level in the Volga was then raised by a significant amount, approximately as in the photo.

<!-- Local
![](Everybody Dances_files/37-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/37-1.jpg)

**Russian:**

В тот год под воду обвалилась вся старинная улица Покровская, на которой, помимо прочего, стоял тот самый храм, богатые купеческие дома и здание шахматного клуба, где "несло" Остапа. Современная дорога, ведущая вниз, не повторяет ту исчезнувшую улицу. Нет точных данных о том, обрушился ли тот храм сам или его снесли из соображений безопасности. Строители в любом случае возводить такое здание на непрочных грунтах не стали бы, выбирая места, где в грунте уже были сооружения пятилучевой крепости Цеппель. Впрочем, какая разница, все эти храм и купеческие здания были построены уже после уничтожения Цеппеля. И скорее всего из тех материалов. Была в те времена технология переработки.

**English:**

That year, the entire old Pokrovskaya Street, on which, among other things, stood the very temple, rich merchant houses and the chess club building, where Ostap was "carried", collapsed under the water. The modern road that leads downhill does not follow that vanished street. There is no exact data as to whether that temple collapsed on its own or was demolished for safety reasons. In any case the builders would not have erected such a building on fragile ground, choosing places where there were already structures of the five-bay Zeppelin fortress in the ground. However, who cares; all these temple and merchant buildings were built after the destruction of Zeppelin. And most likely made of those materials. There was recycling technology in those days.

**Russian:**

Позиция 2 -- по официальной истории, на вершине бывшей крепости Цеппель был выстроен некий тюремный замок. И это было единственным капитальным зданием, которое было выстроено на высокой части разрушенной крепости в 19 веке.

**English:**

According to the official history, position 2 is where a certain prison castle was built on the top of the former castle of Zeppel. It was the only capital building that was built in the 19th century on a high part of the ruined castle.

<!-- Local
![](Everybody Dances_files/38-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/38-1.jpg)

**Russian:**

Здание частично сохранилось и сейчас, причём его ось неким странным образом совпадает с осью одного из лучей звезды. Народ на высоте не селился -- вся жизнь была у воды. Переселяться наверх он начал только в 20 веке в СССР, в связи с участившимися оползнями и поднятием уровня воды в Волге. Так вот, ни одна из улиц Васильсурска не совпадает с осью здания тюремного замка. Не исключено, что тюремный замок был выстроен с использованием фундамента от разрушенного старинного здания в самой крепости Цеппель.

**English:**

The building is still partially preserved, and its axis coincides in a strange way with the axis of one of the rays of the star. People did not settle at this altitude - all life was by the water. Only in the 20th century, in the USSR, due to the landslides and the rising water level of the Volga, did people start to move uphill. So, none of the streets of Vasilsursk coincide with the axis of the prison castle building. It is possible that this prison castle was built using the foundations of the ruined ancient building in the Zeppelin fortress itself.

**Russian:**

Позиции 3, 4 -- смотровые площадки, за которыми находится крутой обрывистый берег. Одна из этих площадок и есть упоминаемый выше Шишкин мыс. Площадки находятся в местах ещё сохранившихся крепостных стен, о которых упоминают на краеведческих ресурсах. Те самые перепады высот на разных ярусах звёзд-крепостей, о которых говорилось ранее, в реалии составляли не менее 10 метров.

**English:**

Positions 3, and 4 are observation sites, behind which there is a steep, precipitous shore. One of these sites is the mentioned above Shishkin Cape. The platforms are in the places of still preserved walls, which are mentioned in the local history resources. The same height differences on the different tiers of star-fortresses, which were mentioned earlier, in reality were not less than 10 meters.

**Russian:**

Позиция 5 -- тот самый Супротивный ключ в священной Цеппельской роще. Странным образом его место совпадает с расположением четырёхугольного замка, который нанесён на план Фридрихсхафена. По всем приметам это здание являлось четырёхбашенным причалом для дирижаблей. По некоторым данным, при СССР здесь какое-то время находилась запретная зона за ограждением, и просто так к роднику было не пройти. Что здесь могло быть спрятано? Впрочем, если недалеко был «замок принца», в земле там до сих пор находится очень много интересного. Только все ходы взорваны и просто так туда не попасть. Слухи о несметных богатствах, скрытых под землёй, явно родились не на пустом месте. Кстати, в многочисленных воспоминаниях старожилов упоминается о васильсурском водопроводе, когда вода с вершины горы, из руин крепости Цеппель, по подземным каналам приходила в богатые дома (которых уже нет). По свидетельствам, воды хватало на всех, напор был хороший, качество воды отменное.

**English:**

Position 5 is the same Suprot key in the sacred Zeppelin Grove. Strangely enough, its location coincides with the location of the quadrangle castle, which is plotted on the plan of Friedrichshafen. By all indications this building was a four-tower dock for airships. According to some reports during the Soviet Union there was a restricted area behind the fence, and just to get to the spring was not passable. What could be hidden here? However, if there was a "castle of the prince" nearby, there is still a lot of interesting things hidden in the ground there. Only all the passages were blown up and there is no easy way to get there. Rumors of untold riches hidden under the ground, obviously, were not born out of nothing. By the way, numerous recollections of old residents mention the water pipe of Vasilsur, when water from the top of the mountain, from the ruins of Zeppel fortress, came through underground canals to rich houses (which no longer exist). According to the testimonies, there was enough water for everyone, the pressure was good and the quality of water was excellent.

<!-- Local
![](Everybody Dances_files/39-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-1.jpg)

<!-- Local
![](Everybody Dances_files/39-2.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-2.jpg)

<!-- Local
![](Everybody Dances_files/39-3.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-3.jpg)

<!-- Local
![](Everybody Dances_files/39-4.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-4.jpg)

<!-- Local
![](Everybody Dances_files/39-5.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-5.jpg)

<!-- Local
![](Everybody Dances_files/39-6.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/39-6.jpg)

**Russian:**

Что всё это значит?

**English:**

What is all this about?

**Russian:**

Ну наверное, как минимум то, что на данном месте ранее находился город, по масштабам и значимости соизмеримый с Москвой. И в какую-то неизвестную войну Москву пощадили, а этому городу не повезло. Его разбомбили полностью, не оставив камня на камне, вместе с не успевшим эвакуироваться населением. И после разрушения на это место через некоторое время пришли жить совершенно посторонние люди, никогда до этого здесь не бывавшие. Крепость Цеппель осталась только в памяти, а название остального города -- Вассербург переиначили на русский манер, подобрав более созвучное слово Василь. Бывшие хозяева этого города ушли далеко на запад и возвращаться не планировали. Территория по планам бомбивших её должна была вымереть, почему этого не произошло? Может, это заслуга выживших людей, а может, упущение уничтожавших эту территорию. Сейчас уже не узнать.

**English:**

Well, probably, at least the fact that in this place previously was a city, on the scale and importance commensurate with Moscow. And in some unknown war, Moscow was spared, but this city was unlucky. It was bombed completely, leaving no stone unturned, along with the population, which had no time to evacuate. And some time after the destruction, this place was inhabited by complete strangers who had never been here before. The Zeppelin fortress was all that survived, and the name of the rest of the city, Wasserburg, was given a Russian name, which was more in tune with the word Vasil. The former masters of the city went far to the west and had no plans to return. The territory, according to the plans of those who bombed it, should have been wiped out. Why did that not happen? Maybe it was the merit of the survivors, or maybe it was the omission of those who destroyed the territory. It is impossible to know now.

**Russian:**

В какой-то момент люди, осевшие в этом месте, отошли от законов человеческого стада и начали задумываться о своём прошлом. И новая власть привезла им уже готовую историю, в которой абсолютно всё, что предшествовало периоду пробуждения их сознания, было сказкой. Абсолютно всё, включая походы кого-то там из Москвы через Васильсурск на Казань. Желающие могут найти эти сказки без особого труда. Что тут можно сказать? Писали их мастера своего дела, достаточно подробно и красочно. Но что было на самом деле в этих краях до их уничтожения, не было ни в одной сказке даже в виде полуправды. Собственно, этого и не требовалось. Задачи вернуть тот город обратно, в память людей или (упаси б-же) в реалию, как впрочем и других подобных сметённых с лица планеты городов на тех землях, категорически не стояло.

**English:**

At some point, the people who settled in this place moved away from the laws of the human herd and began to think about their past. And the new government brought them a ready-made story, in which absolutely everything that preceded the period of awakening of their consciousness was a fairy tale. Absolutely everything, including the campaigns of someone there from Moscow through Vasilsursk to Kazan. Those who wish can find these tales without much difficulty. What can I say? They were written by masters of their craft, quite detailed and colorful. But the tales do not include what was actually in these parts before their destruction - not even in the form of half-truths. As a matter of fact, it was not required. The task of returning that city back to human memory or (God forbid) to reality, as well as other similar cities swept away from the face of the planet on those lands, was categorically not worth it.

<!-- Local
![](Everybody Dances_files/40-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/40-1.jpg)

**Russian:**

Вассербургу суждено было погибнуть. Мало того, что это был крупный транспортный терминал по приёму воздушного, водного транспорта и, возможно, очень длинного метрополитена. В окрестностях аэровокзала - крепости Цеппель, на территории города, защищённого со всех сторон рвами с водой, происходило производство дирижаблей. Это было главное семейное дело графов Цепелиных. Дирижабли в то время были самым важным оружием. Они давали подавляющее преимущество в любых сражениях. Кто владел дирижаблями, тот правил миром. Естественно, не теми дирижаблями, которые были легко уязвимы даже для выстрелов с земли. Здесь всё далеко не так однозначно.

**English:**

Wasserburg was destined to die. It wasn't only a major transport terminal for receiving air, water transport, and possibly a very long subway. Airship production took place in the vicinity of the air terminal, the Zeppelin fortress, in the city, protected on all sides by ditches with water. It was the main family business of the Count Zeppelins. Airships at that time were the most important weapons. They gave an overwhelming advantage in any battle. Whoever owned airships ruled the world. Except, naturally, those airships that were easily vulnerable even to shots from the ground. Here everything is far from clear-cut.

<!-- Local
![](Everybody Dances_files/41.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/41.jpg)

**Russian:**

В архивах имеется достаточно много фотографий как изготовления дирижаблей на предприятиях возле Боденского озера, так и последствий их непонятных аварий по всему миру. На многих фотографиях фигурируют металлические баллоны. В данном случае рассмотрим фото странной аварии дирижабля Цеппелин в Салониках (Греция), в 1915 году. Несмотря на то, что история гибели этого дирижабля достаточно тёмная и в сети практически не освещена, фотографий последствий аварии опубликовано достаточно много. Есть даже видеоролик, смонтированный на основе фотографий с этого события.

**English:**

The archives contain quite a few pictures of both the production of airships at facilities near Lake Constance and the consequences of their incomprehensible accidents around the world. Many photos show metal cylinders. In this case, let's consider a photo of a strange Zeppelin dirigible accident in Thessaloniki (Greece) in 1915. In spite of the fact that the history of the blimp's death is rather dark and practically unavailable on the Internet, there are quite a few photos of the accident's consequences. There is even a video based on photos from this event.

<!-- Local
![](Everybody Dances_files/42.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/42.jpg)

**Russian:**

Официально, именно в эти баллоны закачивался лёгкий газ, гелий или водород. По мере полёта газом из этих баллонов наполняли балонеты, обеспечивая нужные полётные характеристики аппарата. Так поясняется практически во всех информационных источниках по истории дирижаблей. Не будем вникать в нелогичности и подвергать это сомнению, допустим, так оно и было. Вместе с тем, имеются другие фотографии, вызывающие некоторые вопросы.

**English:**

Officially, it was light gas, helium or hydrogen, that was pumped into these cylinders. As the airship flew, the balloons were filled with gas from these cylinders, providing the required flight characteristics of the airship. This is how it is explained in almost all information sources on the history of airships. Let's not get into illogic and question it, and let's assume that it was so. At the same time, there are other pictures that raise some questions.

<!-- Local
![](Everybody Dances_files/43.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/43.jpg)

**Russian:**

Что делают эти так сказать спасатели? Несут неразорвавшуюся в пожаре бомбу? Данное фото трудно заподозрить в постановке сюжета или фотомонтаже. Сапёры, если вы есть среди читателей, дайте комментарий.

**English:**

What are these so-called rescuers doing? Carrying an unexploded bomb in a fire? This photo is hard to suspect of staging or photomontage.  Bomb squad, if you are among the readers, comment.

<!-- Local
![](Everybody Dances_files/44.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/44.jpg)

**Russian:**

Как там, солдат спит, а служба идёт. Но всё же, терзают смутные сомнения, что спит (или даже притворяется спящим) он совсем не на бомбе. Тем более что авиабомбы в 20 веке уже имели ту форму, которая всем хорошо известна по второй мировой. И почему-то эти бомбы спасали на месте крушения дирижабля очень бережно и первым делом.

**English:**

As it happens, the soldier is sleeping while on duty. But still, I have vague doubts that he is sleeping (or even pretending to sleep) at all on a bomb. Especially since the bombs in the 20th century already had the shape that everyone is well aware of from World War II. And for some reason these bombs were rescued at the crash site of the airship first thing and very carefully.

<!-- Local
![](Everybody Dances_files/45.jpeg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/45.jpg)

**Russian:**

Какой ценностью обладали эти так сказать ядра? Впрочем, понятно. Аналогичные ядра использовались и [на кораблях](https://www.tart-aria.info/navalnyj-jard/). Только там они были очень ценными узлами в устройствах получения энергии из воздуха. Похоже, использование на дирижаблях двигателей внутреннего сгорания является очередной сказкой. И действительно, как такие летательные аппараты могли облетать треть земного шара без дозаправки? Даже современные самолёты на такое способны далеко не все. Но сейчас нам упорно преподносят назначение этих «ядер» как авиабомб.

**English:**

What value did these, so to speak, cores have? However, it is clear. Similar cores were also used [on ships](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/navalnyj-jard/). Only there they were very valuable units in the devices for obtaining energy from the air. It seems that the use of internal combustion engines on airships is just another fairy tale. And indeed, how could such aircraft fly around one third of the globe without refueling?   Even modern airplanes are not capable of this. But now we are persistently presented with the purpose of these "nukes" as aerial bombs.

<!-- Local
![](Everybody Dances_files/46.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/46.jpg)

**Russian:**

Существует множество фото с первой мировой, как например эта. По сюжету некто готовится к монтажу в «бомбы» детонаторов. Даже если принять во внимание, что это фото является не постановочным (хотя масса примет указывает на то, что это позируют артисты), бомбы в этом случае просто изготавливали путём приспособления ядер. Но прямое назначение этих ядер изначально было совсем другим. Не надо забывать, что в условиях войны очень часто приходится надеяться только на себя и приспосабливать под оружие и боеприпасы то, что попалось под руку.

**English:**

There are many photos from World War I like this one. The subject is someone preparing detonators to fit into "bombs". Even if we take into account that this photo is not staged (although a lot of signs indicate that the artists are posing), the bombs in this case were simply made by adapting the cores. But the direct purpose of these cores was originally quite different. It should not be forgotten that in war conditions one often has to rely only on oneself and adapt to whatever comes to hand into weapons and munitions.

**Russian:**

Но и это ещё не всё.

**English:**

But there's more to it than that.

**Russian:**

Почему-то очень скупо в Википедии описан один из подвидов дирижаблей -- [вакуумные дирижабли](https://ru.wikipedia.org/wiki/%D0%92%D0%B0%D0%BA%D1%83%D1%83%D0%BC%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8C).

**English:**

For some reason, Wikipedia has described one of the subspecies of blimps very sparingly - [vacuum blimps](https://ru.wikipedia.org/wiki/%D0%92%D0%B0%D0%BA%D1%83%D1%83%D0%BC%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B8%D1%80%D0%B8%D0%B6%D0%B0%D0%B1%D0%BB%D1%8C).

**Russian:**

> Вакуумный дирижабль - дирижабль жёсткой конструкции, внутри оболочки которого создаётся и поддерживается технический вакуум заданной глубины (вакуумирование), вследствие чего в соответствии с законом Архимеда возникнет аэростатическая подъёмная сила как разность между силой Архимеда и силой веса аппарата в целом.В 1670 году иезуит Франческо Терци де Лана издал книгу "Prodromo, ouero faggio di alcune inuentioni nuoue premeffo all'arte maestra" («Предварение, сиречь Описание некоторых новых изобретений, предзнаменующее Великое Искусство»), в 6-й главе которой он описал судно с мачтой и парусом на ней. Это судно, по утверждению Ланы, могло бы летать, поддерживаемое четырьмя медными предварительно вакуумированными сферами диаметром порядка 7,5 метров каждая и при толщине их медной стенки около 4 мм. Франческо Лана полагал, что такое воздушное судно может быть легче воздуха. В переиздании своего труда в 1686 году Лана указал, что вес медной пустой сферы станет сопоставим с весом вытесненного воздуха при её диаметре 130 футов (порядка 40 м) и толщине стенки около 1,5 мм, что конечно было технологически невозможно в его время. Им были так же рассчитаны сферы (способные поднимать груз до нескольких килограмм): стеклянная (диаметром около 1,2 м с толщиной стенки около 0,15 мм) и деревянная (диаметром около 3 м с толщиной стенки около 1 мм).

**English:**

> A vacuum airship is an airship of rigid construction, inside the shell of which a technical vacuum of a given depth (vacuumization) is created and maintained, due to which, in accordance with Archimedes' law, an aerostatic lifting force as the difference between Archimedes' force and the weight force of the apparatus as a whole will arise. In 1670 the Jesuit Francesco Terzi de Lana published Prodromo, ouero faggio di alcune inuentioni nuoue premeffo all'arte maestra (Foretaste, siree Description of some new inventions, heralding the Great Art), in chapter 6 of which he described a vessel with a mast and a sail on it. This vessel, according to Lana, could fly supported by four pre-vacuumed copper spheres of about 7.5 meters diameter each and with a copper wall thickness of about 4 mm. Francesco Lana believed that such an aircraft could be lighter than air. In the republishing of his work in 1686, Lana indicated that the weight of the empty copper sphere would be comparable to the weight of displaced air when it is 130 feet in diameter (about 40 m) and the wall thickness is about 1.5 mm, which of course was technologically impossible at his time. He also calculated spheres (capable of lifting loads up to several kilograms): a glass one (about 1.2 m in diameter with a wall thickness of about 0.15 mm) and a wooden one (about 3 m in diameter with a wall thickness of about 1 mm).
f about 1 mm).

**Russian:**

Думаете, эти идеи так и остались только в мечтах изобретателей-энтузиастов?

**English:**

You still think these ideas are only the dreams of enthusiastic inventors?

<!-- Local
![](Everybody Dances_files/Flying_boat1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/Flying_boat1.jpg)

**Russian:**

А если, при наличии на дирижабле источника свободной энергии, на него поставить компрессор и металлические баллоны, из которых периодически откачивать воздух для управления полётом или просто поддержания нужной разреженности? Конечно же, идеальный вакуум создать трудно, и нужное значение подъёмной силы добирали количеством баллонов. Пусть для этого и приходилось делать большие оболочки дирижаблей, чтобы спрятать в них нужное число этих самых баллонов. Полагаю, многие теперь начинают понимать, почему дирижабли были обречены. И все истории с добычей гелия как стратегического материала были или сказкой, или гелий использовали как резервный способ поддержания подъёмной силы дирижаблей, но в очень небольшом объёме.

**English:**

What if, if the airship has a source of free energy, it is equipped with a compressor and metal cylinders, from which air is periodically pumped out for flight control or simply to maintain the necessary rarefaction? Of course, it is difficult to create a perfect vacuum, and the required value of the lifting force was achieved by the number of cylinders. Even if for this purpose we had to make large airship shells in order to hide in them the required number of cylinders. I suppose many people now begin to understand why airships were doomed. And all the stories about extracting helium as a strategic material were either a fairy tale or helium was used as a reserve way to maintain the lifting power of airships, but in very small quantities.

**Russian:**

И правда, если такие дирижабли действительно существовали, то при установки на них обычных электрических пушек, которые ставили также и на кораблях, всё становится на свои места. Дирижабли могли совершенно автономно подвергать ковровым бомбардировкам эфирными тороидами абсолютно любые площади. А эфирные тороиды можно просто получать из воздуха, как мыльные пузыри. Для этого были нужны только набор тех самых ядер с неким веществом, ну и компрессор, баллоны и летательный аппарат, что в общем уже вопрос техники. И можно держать в страхе весь мир.

**English:**

Indeed, if such airships really existed, then when they were equipped with conventional electric cannons, which were also placed on ships, everything falls into place. Airships could absolutely autonomously subject to carpet bombardment by ether toroids absolutely any area. And etheric toroids could simply be obtained from the air, like soap bubbles. This required only a set of those very nuclei with some substance, and a compressor, cylinders, and a flying machine, which in general is already a matter of technology. And one could keep the whole world in fear.

**Russian:**

Почему-то ранее всегда возникал вопрос о происхождении слова «кавалерия» как рода войск. Почему-то всегда думалось, что это слово происходит от итальянского «cavallo» - лошадь. Собственно, так происхождение кавалерии поясняют официальные источники. Всё бы ничего, но по латински слово «лошадь» является совсем другим -- equus. В биологии от этого слова произошло очень много названий видов лошадей -- гиппарион, эогиппус, меригиппус и т.д. Где тут кавалерия? Оказалось, всё просто. Кавалерия -- это дословно «езда вверху», или верхом. К лошадям происхождение этого термина имеет очень посредственное отношение. После того, как прошла настоящая война «красных и белых», или та самая неизвестная нам война, в качестве кавалерии всему миру вместе с псевдоисторией незаметно подсунули всадников на лошадях. Глупо было бы так не сделать. И пришедшие псевдо-«красные» вовсю ратовали за конницу Будённого и Ворошилова. А настоящая кавалерия -- это дирижабли. Но про них было низя. Хотя, не исключено, что и про 20 век мы многое не знаем. Когда кто-то там понял, что дальнейшее существование дирижаблей ломает главную доктрину, их и уничтожили как класс. Пусть для этого пришлось инсценировать несчастный случай. В мировой истории, кстати, случаи проведения терактов с маскировкой под несчастные случаи совсем не единичны.

**English:**

For some reason the question of the origin of the word "cavalry" as a kind of troops has arisen before. For some reason it was always thought that this word comes from the Italian "cavallo" - horse. In fact, this is how the origin of cavalry is explained by official sources. That would be all right, but the Latin word for "horse" is something else: equus. In biology, many names of horse species are derived from this word -- hipparion, eohippus, merigippus, etc. Where's the cavalry here?

Turns out it's simple. Cavalry is literally "riding overhead," or on horseback. The origin of the term has very little to do with horses. After the real war of "red and white", or that unknown-to-us war, took place, the whole world was inconspicuously presented with horseback riders as cavalry along with pseudo-history. It would have been foolish not to. And the pseudo-"Reds" who came to the world were all for the cavalry of Budyonny and Voroshilov. And the real cavalry was airships. But they were not allowed. Though it is possible that we don't know much about the 20th century either. When somebody there understood that the further existence of airships breaks the main doctrine, they were destroyed as a class. Even if it meant staging an accident. In world history, by the way, the cases of terrorist attacks disguised as accidents are not at all isolated.

<!-- Local
![](Everybody Dances_files/48-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/48-1.jpg)

**Russian:**

По переезду на берега Боденского озера граф Цеппелин восстановил своё семейное предприятие. Вполне возможно, Фридрихсхафен им был выбран за своё сходство с тем Цеппелем, который уже к тому времени ушёл навсегда. Предприятие графа ещё несколько десятилетий вполне успешно работало, и даже выполняло военные заказы, но... всё имеет свойство заканчиваться.

**English:**

On moving to the shores of Lake Constance, Count Zeppelin rebuilt his family business. It is possible that he chose Friedrichshafen because of its resemblance to Zeppel, which by that time was gone forever. The Count's enterprise had been working quite successfully for several decades, and even fulfilled military orders, but... everything comes to an end.

**Russian:**

А как же могли воевать те самые уязвимые дирижабли, например, в первую мировую? Если теперь посмотреть на них под другим углом, с учётом вышесказанного, то можно сделать вывод, что как оружие они были очень даже неплохи. Если дирижабль действительно имел жёсткий корпус, за которым были защищены баллоны с вакуумом, то сбить его из огнестрельного оружия было бы очень непросто. А если при этом дирижабль имел люки с орудиями на всех сторонах корпуса, как на гербе Васильсурска, то он запросто мог не подпустить к себе никого как минимум на длину выстрела эфирного тороида из пушек.

**English:**

But how could those very vulnerable airships have fared in World War I, for example? If we now look at them from a different angle, taking into account the above-mentioned, we can conclude that as a weapon they were very good. If the airship really had a rigid hull, behind which the vacuum cylinders were protected, it would be very difficult to shoot it down with firearms. And if the airship had hatches with cannons on all sides of the hull, as on the emblem of Vasilsursk, then it could not let anyone near it at least the length of the ether toroid shot from the cannons.

**Russian:**

Недавно в новостях передавалась информация, что Турция намерена разместить в Украине производство своих боевых беспилотников «Байрактар». Ой, не те аппараты надо размещать на производство. Если кто-нибудь догадается собрать хоть один дирижабль в том виде, каким он был сто лет назад, все беспилотники сразу же морально устареют. Один выстрел из такого дирижабля, и дроны попадают, примерно как комары, если в их стаю направить струю огня (помню, баловались в детстве).

**English:**

The news recently reported that Turkey intends to place production of its Bayraktar combat drones in Ukraine. Oh, the wrong machines should be placed in production. If someone guesses to assemble even one airship as it was a hundred years ago, all the drones will immediately become morally obsolete. One shot from such an airship, and the drones hit like mosquitoes if you send a jet of fire at their flock (I remember dabbling in my childhood).

**Russian:**

Ну и наверное, всех читателей интересует, что же это была за неизвестная война, на которую по тексту идёт очень много ссылок. Давайте попробуем объяснить. И начнём с конца.

**English:**

Well, probably all readers are wondering what this unknown war was, to which there are so many references throughout the text. Let's try to explain. And let's start at the end.

<!-- Local
![Конец, конец\... Концы в воду!](Everybody Dances_files/49.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/49.jpg)

**Russian:**

Высокотехнологичное производство дирижаблей в начале 20 века граф Цеппелин начал явно не с нуля, хотя официальная история уверяет нас в обратном. Ему неким образом удалось эвакуировать все технологии, которые применялись ранее на его семейном предприятии в Поволжье. Эти технологии во многом опережают даже технологии 21 века, которые весь мир применяет в настоящее время.

**English:**

Count Zeppelin clearly did not start high-tech airship production from scratch at the beginning of the 20th century, although the official history assures us of the opposite.    He somehow managed to evacuate all the technology previously used at his family enterprise in the Volga region. These technologies are in many ways ahead of even the 21st century technologies that the whole world currently uses.

<!-- Local
![FRIEDRICHSHAFEN, ZEPPELINDORF, J-RAD MIT MIT RUDOLF JARAY](Everybody Dances_files/50-FRIEDRICHSHAFEN-ZEPPELINDORF-J-RAD-MIT-MIT-RUDOLF-JARAY.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/50-FRIEDRICHSHAFEN-ZEPPELINDORF-J-RAD-MIT-MIT-RUDOLF-JARAY.jpg)

**Russian:**

О чём там можно говорить, если инженера с заводов Цеппелина ездили на подобных велосипедах. Даже китайские производители до таких ещё не додумались в настоящее время.

**English:**

What is there to talk about if the engineers from the Zeppelin factories rode bicycles like this. Even Chinese manufacturers have not yet come up with such bikes.

**Russian:**

Вопрос -- если дирижабли были такие высокотехнологичные и даже воевали в первую мировую (есть масса неопровержимых доказательств), то почему в первую мировую не было разрушений, какие были зафиксированы, например, после Франко-Прусской войны? Возможно, такие разрушения были и ближе по хронологии к первой мировой, и были более «свежие» войны, но в истории это скрыто. Вариантов ответа, в общем, немного -- или мощное оружие в первую мировую было уже запрещено, или воюющие стороны были не заинтересованы его применять с воздуха, или (внимание), в той войне над воюющими сторонами была третья сторона, которая всячески управляла той войной и не давала ни одной из сторон осуществить победу путём использования стратегического оружия с воздуха. Или, выражаясь проще, заставляла вести войну ради войны средствами малой механизации (примерно как сейчас в Донбассе), чтобы стороны побольше поубивали друг друга и бесконечно расходовали на эту войну ресурсы.

**English:**

Question... if airships were so high-tech and even fought in World War I (there is plenty of hard evidence), then why was there no destruction in World War I such as was recorded, for example, after the Franco-Prussian War? Perhaps such destruction was closer in chronology to WWI, and there were more "fresh" wars, but it is hidden in history. There are, in general, few variants of the answer -- either powerful weapons in WWI were already banned, or the warring parties were not interested in using them from the air, or - pay attention - in that war there was a third party above the warring parties, which managed that war in every way and did not let either party achieve victory through the use of strategic weapons from the air. Or, to put it more simply, forced a war for the sake of war by means of small-scale mechanization (like now in the Donbass), so that the sides would kill each other more and spend endless resources on this war.

**Russian:**

По свидетельствам историков, первая мировая как раз была непонятной затяжной войной, без стратегического планирования, с переменным успехом каждой из сторон. Потом появился Ленин, превратил эту войну в гражданскую, и случилось то, что все знают. Какую силу он представлял и почему он так странно один вид войны превратил в другой?

**English:**

According to historians, World War I was just an incomprehensible protracted war, without strategic planning, with varying success on each side. Then Lenin came along, turned this war into a civil war, and everybody knows what happened next. What power did he represent, and why did he so strangely turn one kind of war into another?

**Russian:**

Спустимся по хронологической шкале ранее где-то на 50 лет, и посмотрим, какие события происходили в это время в российской истории. Естественно, не той истории, про которую пишут в учебниках.

**English:**

Let's go down the chronological scale by about 50 years, and see what events were taking place at this time in Russian history. Naturally, not the history they write about in textbooks.

**Russian:**

А из событий, собственно, там не происходило ничего. Как мы уже выяснили, Российская Империя была подвергнута тотальной бомбардировке и лежала в руинах. Центральная, восточная, южная части огромного государства были превращены буквально в безжизненное пространство. Кто жил на этой территории ранее? И в частности, почему в Поволжье стояли немецкие предприятия?

**English:**

And of the events, in fact, nothing happened there. As we have already learned, the Russian Empire was totally bombed and lay in ruins. The central, eastern, and southern parts of the vast country had been turned into a lifeless space. Who lived in this territory before? And in particular, why were there German factories in the Volga region?

**Russian:**

История с заселением российского Поволжья немцами уходит корнями в глубокое прошлое. Официально, немцев в Россию пригласила сама Екатерина Великая. И за достаточно короткое время немцы заселили значительные территории в Поволжье.

**English:**

The history of the settlement of the Russian Volga region by Germans goes deep into the past. Officially, the Germans were invited to Russia by Catherine the Great herself.  And in a fairly short time, the Germans settled large areas in the Volga region.

<!-- Local
![](Everybody Dances_files/51-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/51-1.jpg)

**Russian:**

Это карта от 20 века, когда уже прошла та самая война. Немецкие поселения локализованы только на средней Волге, от Самары до Царицына. Но мы то уже знаем, что ранее немцы жили и выше по Волге. И жили они вплоть до столицы, Санкт-Петербурга. Что это вообще была за странная страна?

**English:**

This is a map from the 20th century, when that very first world war had already taken place. German settlements are localized only in the middle Volga, from Samara to Tsaritsyn. But we already know that earlier the Germans lived up the Volga. And they lived as far as the capital, St. Petersburg. What kind of strange country was this?

<!-- Local
![](Everybody Dances_files/52-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/52-1.jpg)

**Russian:**

Ничего странного. Это и была та самая Российская Империя, состоявшая из гармоничного компактного проживания русских, немцев и прочих народов. На восток эта страна шла до американского континента, на юг до Индии включительно. Екатерина не приглашала немецкие народы, она с ними пришла и устроила "золотой век", но это уже отдельная история. И затем случилась та самая неизвестная война, в которой немцы выжили только на том самом, среднем Поволжье. Неведомая сила прошлась по всему миру, завоевала северную Америку, Индию, частично Африку. Среднеазиатская и частично восточноазиатская часть Евразии была превращена этой силой в руины. Воины этой силы были одеты в мундиры красного цвета. Назовём их условно «британские» (без какой-либо привязки к современной Великобритании).

**English:**

Nothing strange. This was the Russian Empire, consisting of a harmonious compact of Russians, Germans, and other peoples. To the east this country went as far east as the American continent, to the south as far south as India. Catherine did not invite the German peoples, she came with them and arranged a "golden age", but that's a separate story. And then came the unknown war, in which the Germans survived only in the middle of the Volga. An unknown force walked all over the world, conquered North America, India, partly Africa. This force turned the Central Asian and part of East Asian Eurasia into ruins. The warriors of this force wore red uniforms. Let us call them conventionally "British" (without any reference to modern Britain).

**Russian:**

Каким образом «британским» удалось покорить весь мир? Как бы не банально звучало, но помогли им в этом армия и флот. Но только с флотом есть маленькая оговорка -- помимо морского, был ещё воздушный флот. Те самые дирижабли, работавшие на небесном ресурсе, или кавалерия. Наличие воздушного флота при отсутствии его у противников делало эти силы непобедимыми. А как «британские» победили воздушный флот противника, сложно сказать. Возможно, помогло некое природное событие. Возможно,его величество случай. Возможно, как в истории с «Непобедимой армадой», их воздушный флот оказался более технически совершенным. Но факт свершился -- они стали эксклюзивными обладателями боевых воздушных кораблей. Далее им оставалось неспеша облетать весь мир, и превращать его нужные места в руины, совершенно не опасаясь за безопасность в небе. И потом выдавать эти бомбардировки за пожары, землетрясения, и даже некие локальные войны.

**English:**

How did "the British" manage to conquer the world? As clichéd as it may sound, the army and navy helped them do it. But there was a small caveat with the navy... in addition to the navy, there was also the air fleet. Those very airships that worked on the sky resource, or the cavalry. The presence of an air fleet in the absence of an enemy made these forces invincible. And how the "British" defeated the enemy's air fleet is hard to say. Perhaps some natural event helped. Perhaps it was the majesty of chance. Perhaps, as in the story of the "Invincible Armada", their air fleet was more technically advanced. But the fact was accomplished - they became the exclusive owners of warships. From then on, they had no choice but to fly around the world, turning the right places into ruins without any fear for safety in the sky. And then to pass off these bombings as fires, earthquakes, and even some local wars.

**Russian:**

Ну а на территории Евразии эти силы превратили русско-немецкую Российскую Империю в пустыню, возможно, пощадив её западную часть. Те события в истории скупо упоминаются как оборона Севастополя, хотя боестолкновения в других местах на побережье тоже присутствовали. А про бомбардировки городов внутри материка - полнейшее молчание.

**English:**

Well, in Eurasia, these forces turned the Russian-German Russian Empire into a desert, perhaps sparing its western part. Those events are sparingly mentioned in history as the defense of Sevastopol, although fighting elsewhere on the coast also occurred. And there is complete silence about the bombing of cities inland.

**Russian:**

Итогом той войны стало расчленение Российской Империи.

**English:**

The outcome of that war was the dissection of the Russian Empire.

<!-- Local
![](Everybody Dances_files/53-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/53-1.jpg)

**Russian:**

Так появились на свет Германия, Австро-Венгрия (как потомки народов, эвакуировавшихся с территории бомбардировок на запад), и Россия. Вот только в России, как наверное многие уже догадались, был посажен «не настоящий» пробританский царь.

**English:**

This is how Germany, Austria-Hungary (as descendants of the peoples who evacuated the bombed-out territory to the west), and Russia were born. Except that in Russia, as many have probably already guessed, a "fake" pro-British tsar was put in.

<!-- Local
![](Everybody Dances_files/54-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/54-1.jpg)

**Russian:**

И вполне логичным, предсказуемым и гениальным ходом «британских» было искусственным образом создать затяжную войну между бывшими «братскими» народами, что и было сделано в виде первой мировой. Естественно, войны ради войны, без использования «стратегических бомбардировщиков», чтобы навеки посеять неприязнь между народами. Паззлы удивительно встали на свои места. В то время даже все немецкие названия городов централизованно привели в русский вид, от Петрограда до Баронска. Ну а царя потом благополучно эвакуировали в Сидней, разыграв спектакль в Екатеринбурге.

**English:**

And it was quite logical, predictable and ingenious for the "British" to artificially create a protracted war between the former "brotherly" nations, which was done in the form of World War I. Naturally, war for war's sake, without the use of "strategic bombers" to sow animosity between the peoples forever. The puzzles surprisingly fell into place. At that time, even all German city names were centralized into Russian form, from Petrograd to Baronsk. Well, the tsar was then safely evacuated to Sydney, playing out the show in Yekaterinburg.

**Russian:**

Но и это ещё не всё. Как только что-то пошло не так и чуть позже Сталин и Гитлер стали пытаться образовать подобие альянса, были предприняты беспрецедентные меры. Между народами снова был надолго забит клин. Кстати, по непроверенным данным, Гитлер спокойно дожил до 1965 года в южной Америке. И тоже в апреле 1945 года возле рейхс-канцелярии в Берлине был разыгран спектакль. Самые лучшие актёры, как мы знаем, никогда не работают в театре.

**English:**

But that was not all. As soon as something went wrong and a little later Stalin and Hitler began trying to form a semblance of an alliance, unprecedented measures were taken. Once again a wedge was driven between the nations for a long time. By the way, according to unverified reports, Hitler lived quietly until 1965 in South America. And also in April 1945, near the Reich Chancellery in Berlin, a play was staged. The best actors, as we know, don't work in the theater.

**Russian:**

Беспрецедентные меры по недопущению сближения России и Германии вводят и сейчас.

**English:**

Unprecedented measures to prevent rapprochement between Russia and Germany are still being introduced even now.

**Russian:**

И что интересно, больше всего «британские» боятся повторения именно альянса России и Германии. И тех, и тех в настоящее время они «нагнули», каждого своим способом. Отдельно взятые славяне (от слова slave) их совсем не пугают, но при малейших попытках воссоздать тот вид Российской Империи, уничтоженной около 200 лет назад, может последовать всё что угодно из набора мер воздействия. Тот славянский народ застрял своей памятью в 20 веке, и для него война -- это когда идут танки и летят самолёты. А когда его медленно варят, как лягушку в котле, это нормальная повседневность. Нужна будет война? Проведут, и как по классику, с переходом из очередной Отечественной в гражданскую. Как там, нужны рога -- наставим, не нужны -- отшибём. Лягушка почти уже сварена, больших усилий совсем не надо.

**English:**

And what is interesting is that it is the alliance between Russia and Germany that the "British" fear most of all. They are currently "bent over" both of them, each in their own way. Separated Slavs (from the word 'slave') do not scare them at all, but at the slightest attempt to recreate that kind of Russian Empire, destroyed about 200 years ago, any option from the set of measures can follow. That Slavic nation is stuck in its memory in the 20th century, and for it war is when tanks and planes move. And when they are slowly boiled, like a frog in a cauldron, it is a normal everyday occurrence. Will there be a war? They will, and like the classics, with the transition from another Patriotic War to the Civil War. If we need horns, we'll put them on; if we don't need them, we'll blast them off. The frog is almost cooked, it does not take much effort at all.

**Russian:**

Когда-нибудь этот народ вылечат. Если он сам не вымрет от этого многолетнего эксперимента. Всех вылечат.

**English:**

Some day these people will be cured. If they don't go extinct themselves from this years-long experiment. Everyone will be cured.

<!-- Local
![](Everybody Dances_files/55-1.jpg "Танцуют все! tech_dancer")
-->

![](https://m3.tart-aria.info/wp-content/uploads/2020/10/tech_dancer/55-1.jpg)

**Russian:**

Один шутник недавно сказал, что в России тихо и бескровно удалось ввести налог на воздух. Подышать в маске стоит 20 рублей, подышать без маски стоит 5000 рублей. Кого-нибудь что-нибудь удивляет?

**English:**

One joker recently said that Russia quietly and bloodlessly managed to introduce an air tax. It costs 20 rubles to breathe in a mask and 5,000 rubles to breathe without a mask. Does anything surprise anyone?

**Russian:**

И зачем надо было подразумевать политический подтекст в песнях «Чингиз-Хан»? Кому та Москва сейчас нужна? Впрочем, тот музыкальный проект не разрушал русскую культуру, а наоборот, способствовал популяризации идей объединения русской и немецкой культур. Что вовсю раздражало "британских". Так что улыбаемся и танцуем.

**English:**

And why was it necessary to imply political overtones in the "Genghis Khan" songs? Who needs that Moscow nowadays? However, that musical project did not destroy Russian culture; on the contrary, it promoted the idea of uniting Russian and German cultures. Which annoyed the "British" no end. So let's smile and dance.

**Russian:**

Автор: tech_dancer, источник: [tart-aria.info](https://www.tart-aria.info/tancujut-vse/)

**English:**

Author: tech_dancer, source: [tart-aria.info](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/tancujut-vse/) |.

**Russian:**

При использовании материалов статьи активная ссылка на tart-aria.info с указанием автора обязательна.

**English:**

At use of materials of article the active reference to tart-aria.info with instructions of the author is obligatory.

**English:**

© All rights reserved. The original author retains all rights.
