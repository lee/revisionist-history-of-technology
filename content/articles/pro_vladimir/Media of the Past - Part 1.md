title: Media of the Past - Part 1
date: 2014-03-31
modified: Sun 31 Jan 2021 11:50:18 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/28598.html
originally: Media of the Past - pro_vladimir — LiveJournal.html
local: Media of the Past_files/
summary: People do not want to accept the fact that there were civilizations with quite advanced technologies here before them. Most accept the idea that they were, but the fact that there is much left of them, almost no one considers it.
status: published

### Translated from:

[https://pro-vladimir.livejournal.com/28598.html](https://pro-vladimir.livejournal.com/28598.html)

Translator's note: Many of the images originally linked to in this article are no longer available. However, an English-language translation of this article with *some* of the now-lost original images is available at: [https://bashny.net/t/en/346220](https://bashny.net/t/en/346220).

**Russian:**

Бронза и DVD, что между ними может быть общего? [http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

**English:**

Bronze and DVD, what could the two have in common? [http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

**Russian:**

Люди не хотят принимать тот факт, что были до них тут цивилизации с вполне себе продвинутыми технологиями. Большинство допускают мысль, что оные были, но то, что от них осталось много чего, этого почти никто не рассматривает.

**English:**

People do not want to accept the fact that there were civilizations with quite advanced technologies here before them. Most accept the idea that they were, but the fact that there is much left of them, almost no one considers it.

**Russian:**

На одном уважаемым мной форуме, как-то была поднята тема, смысл оной сводился к тому, что пытались обсуждать вопрос хранения информации. В том плане, чтоб она (информация) хранилась долго и не портилась. Ведь бумага тлеет, зубилом точать каменные страницы долго и слишком большие трудозатраты для того требуется. Электронные носители, то же рано или поздно превратятся в труху.

**English:**

On one of my esteemed forums, a topic was discussed that was reduced to trying to discuss storage of information. In the sense that it (information) was stored for a long time and did not spoil. In fact paper smolders and it takes too much time and labour to grind stone pages with a chisel. Electronic carriers, too, sooner or later will turn to dust.

**Russian:**

Но раз мы слышали про другие развитые цивилизации, то почему никто не допускает мысли, что они на тот момент этот вопрос успешно решили. И что достаточно захотеть только поискать эти решения. Поискать те флешки и диски, что в изобилии разбросаны и там и тут. Попробовать прочитать уже их.

**English:**

But since we have heard about other developed civilizations, why does no one allow the idea that they have successfully solved this question at that time? And that one only has to be willing to look for those solutions? Look for those flash drives and disks that are scattered here and there in abundance. Try to read them.

**Russian:**

Вон хоть те же бронзовые китайские зеркала. А там этого «винила» превеликое множество. А дело дальше пускания зайчиков не пошло.

**English:**

There are Chinese bronze mirrors, for example. There is plenty of this "vinyl" out there. And the matter did not go further than bunny rabbits.

**Russian:**

Или вот такой носитель информации, отсюда:

**English:**

Or a medium like this, from here:

[http://oko-planet.su/phenomen/phenomennews/236886-neobyasnimoe-drevniy-geneticheskiy-disk-v-kartinkah-obyasnyaet-fazy-zarozhdeniya-zhizni.html](http://oko-planet.su/phenomen/phenomennews/236886-neobyasnimoe-drevniy-geneticheskiy-disk-v-kartinkah-obyasnyaet-fazy-zarozhdeniya-zhizni.html)

**Russian:**

Необъяснимое: Древний "генетический диск" в картинках объясняет фазы зарождения жизни

**English:**

Unexplained: An ancient "genetic disc" in pictures explains the phases of the origin of life

**Russian:**

Перед вами самый важный и самый невероятный артефакт, который был найден в Колумбии. Это - так называемый "Генетический диск". Он выполнен из Лидита очень крепкого камня. По прочности он не уступает граниту, но структура камня слоистая, поэтому из такого материала невозможно сделать такой диск в наши дни.Диаметр - 27см.

**English:**

Before you is the most important and most incredible artifact that was found in Colombia. This is the so-called "Genetic Disc". It's made of Lydite, a very hard stone. It is as strong as granite, but the structure of the stone is layered, so it is impossible to make such a disk from such material in our days.Diameter - 27 cm.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_003.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-36-40.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-36-40.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-36-40.jpg)

Missing image replaced with:

[![](http://4.bp.blogspot.com/_HpZpN1sdyyc/TLojhjMfSWI/AAAAAAAAACs/l6HMpBX_dvM/s400/life48_54.jpg#clickable)](http://4.bp.blogspot.com/_HpZpN1sdyyc/TLojhjMfSWI/AAAAAAAAACs/l6HMpBX_dvM/s1600/life48_54.jpg)

**Russian:**

На этом диске несколько изображений тех процессов которые в обычной жизни можно рассмотреть только под микроскопом.В левой части диска на 11 часов, можно рассмотреть изображение мужского яичка без сперматозоида и со сперматозоидом, видимо здесь показан процесс зарождения сперматозоидов.

**English:**

On this disk, several images of the processes that in normal life can be viewed only under a microscope. On the left side of the disk at 11 o'clock, you can see the image of a male testicle without sperm and with sperm, apparently it shows the process of sperm birth.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_005.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-37-51.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-37-51.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-37-51.jpg)

Missing image replaced with:

[![](http://3.bp.blogspot.com/_HpZpN1sdyyc/TLojnpjpx2I/AAAAAAAAAC0/MlDRHP7f8_k/s400/life48_56.jpg#clickable)](http://3.bp.blogspot.com/_HpZpN1sdyyc/TLojnpjpx2I/AAAAAAAAAC0/MlDRHP7f8_k/s1600/life48_56.jpg)

**Russian:**

Слева примерно в направлении часа можно увидеть несколько уже зародившихся сперматозоидов. Дольше изображение непонятное пока для нас .Нужно более детальное исследование специалистов биологов.

**English:**

On the left about the direction of the hour you can see a few sperm already nucleated. A longer image is not understandable for us yet. A more detailed study by specialist biologists is needed.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQpLb1.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-17.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-17.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-17.jpg)

**Russian:**

На этом фрагменте "Генетического диска" изображения выглядят так как в жизни для сравнения представлен снимок сделанный исследователями.

**English:**

On this fragment of the "Genetic disk" images look as in a life for comparison the image made by researchers is presented.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_008.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-41.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-41.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-38-41.jpg)

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_009.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-13.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-13.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-13.jpg)

**Russian:**

На обратной стороне диска вверху изображен зародыш в нескольких стадиях развития. и заканчивая тем как выглядит новорожденный ребенок.

**English:**

The back of the disc at the top depicts a fetus in several stages of development. and ending with what a newborn baby looks like.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_006.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-32.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-32.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-39-32.jpg)

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_007.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-08.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-08.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-08.jpg)

**Russian:**

Мы видим так же на диске в районе шести часов изображение мужчины и женщины.

**English:**

We also see the image of a man and a woman on the disk around six o'clock.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_002.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-38.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-38.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-40-38.jpg)

**Russian:**

В районе трёх часов на диске видны изображения мужчины, женщины и ребёнка, странность тут в том как изображена голова человека.Если это не стилистическое изображение , то к какому виду относятся эти люди.

**English:**

At about three o'clock on the disc we can see images of a man, a woman and a child, the strange thing is in the way the human head is depicted, if it is not a stylistic image, what species these people belong to.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDXByl7TwF-10A4WWcE4FOySIiJG6ZIk_vAQ_004.html)](Media of the Past - pro_vladimir — LiveJournal_files/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-41-05.jpg)
-->

[![Image no longer available](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-41-05.jpg#clickable)](http://say746.ru/wp-content/uploads/2012/03/Avalon-Klaus-Dona-Skryitaya-istoriya-CHelovecheskoy-Rasyi17-41-05.jpg)

**Russian:**


(По материалам интервью с Клаусом Доной, организовавшим проект "Авалон".

Людит - этот камень так же твёрд, как гранит, но структура его такова, что он очень хрупок.На "генетическом диске показаны фазы зарождения и развития фетуса, включая графическое изображение сперматозоидов и оплодотворённой яйцеклетки. Как известно, впервые 25 лет назад в Швеции удалось сделать фото, как выглядят такие клетки внутри женщины, используя высокотехнологичные приспособления и микроскоп. Думаю, всего несколько тысяч лет назад такие знания были недоступны. В этой же коллекции - искусно выполненные приборы, предположительно медицинского назначения. В Вене мы провели анализ материала, из которого изготовлены эти артефакты. Этот материал, чёрного цвета, напоминающий по виду металл, - несомненно, людит. Самый опытный в Вене специалист по работе с драгоценными камнями, после многочасовой экспертизы, сказал нам, что у него нет никаких предположений, как эти предметы были изготовлены, кем и когда. В наши дни, из тех же самых материалов, невозможно выполнить подобную работу.

**English:**

(Based on an interview with Klaus Dona, who organized the Avalon project.)

Ludite - this stone is as hard as granite, but its structure is such that it is very fragile.The "genetic disk shows the phases of conception and development of fetus, including a graphic image of sperm and fertilized egg. As you know, for the first time 25 years ago, Sweden managed to make a photo of what such cells look like inside a woman using high-tech gadgets and a microscope. I think, just a few thousand years ago, such knowledge was unavailable. In the same collection - elaborately made devices, presumably for medical purposes. In Vienna, we analysed the material used to make these artefacts. This material, black in colour and resembling metal, is undoubtedly human. The most experienced gemstone specialist in Vienna, after hours of examination, told us that he had no idea how these objects were made, by whom and when. Nowadays, with the same materials, it is impossible to perform such a work.

**Russian:**

В Колумбии, комитет по археологии отказался признать эти предметы как представляющие историческую ценность.

**English:**

In Colombia, a committee on archaeology refused to recognize the objects as of historical value.

**Russian:**

Клаусу Донаудалось в силу специфики его деятельности собрать воедино более 400 предметов, которые никак не возможно объяснить исходя из современного Социального Договора.

**English:**

Due to the specific nature of his work, Klaus Donau was able to put together more than 400 items, which can in no way be explained in terms of the modern Social Treaty.

**Russian:**

Будучи куратором Арт Экспозиции Дома Габсбургов, Австрия, он изначально скептически относящийся к "артефактам", столкнувшись воочию с сотнями реальных предметов, что не укладывающихся в временную историю человечества пошел дальше.

**English:**

As curator of the Art Exposition of the Habsburg House, Austria, he was initially sceptical about "artefacts", confronted first-hand with hundreds of real objects that do not fit into the temporal history of mankind went further.

**Russian:**

И убедился в том, что в действительности современная наука, вся система общества устроены так, чтобы скрывать информацию, факты, объекты - которые никак не возможно ни понять, ни сделать... даже в нашем "технически-развитом" социуме.

**English:**

And made sure that in reality modern science, the whole system of society is arranged in such a way as to conceal information, facts, objects - which are in no way possible to understand or make... even in our "technologically advanced" society.

**Russian:**

Редкий пример, когда человек самостоятельно начинает исследовать, а не отвергать и меняет свою систему убеждений в процессе самостоятельного поиска и осмысления).

**English:**

It is a rare example of an individual starting to explore rather than reject and change their belief system in a process of self-discovery and reflection).


**Russian:**

есть еще и такие вот артефакты:

**English:**

There are also artifacts like this:


[http://en.wikipedia.org/wiki/Bi_%28jade%29](http://en.wikipedia.org/wiki/Bi_%28jade%29)


<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/lossy-page1-589px-Jade_Bi_Ornament_Dragon_designs_China_-_Wa.jpg#clickable)](Media of the Past - pro_vladimir — LiveJournal_files/lossy-page1-589px-Jade_Bi_Ornament%2C_Dragon_designs%2C_China_-_Warring_States_period%2C_Western_Han_dynasty%2C_4th-2nd_century_BC.tiff.jpg)
-->

[![](http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Jade_Bi_Ornament%2C_Dragon_designs%2C_China_-_Warring_States_period%2C_Western_Han_dynasty%2C_4th-2nd_century_BC.tiff/lossy-page1-589px-Jade_Bi_Ornament%2C_Dragon_designs%2C_China_-_Warring_States_period%2C_Western_Han_dynasty%2C_4th-2nd_century_BC.tiff.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Jade_Bi_Ornament%2C_Dragon_designs%2C_China_-_Warring_States_period%2C_Western_Han_dynasty%2C_4th-2nd_century_BC.tiff/lossy-page1-589px-Jade_Bi_Ornament%2C_Dragon_designs%2C_China_-_Warring_States_period%2C_Western_Han_dynasty%2C_4th-2nd_century_BC.tiff.jpg)


A Western Han dynasty Bi, with dragon designs.


**Russian:**

Точное предназначение би дисков неизвестно.

**English:**

The exact purpose of the Bi-discs is unknown.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/B_bi4__disc.jpg#clickable)](Media of the Past - pro_vladimir — LiveJournal_files/B%C3%AC_%28bi4%29_%E7%92%A7_disc.jpg)
-->

[![](http://upload.wikimedia.org/wikipedia/commons/8/8f/B%C3%AC_%28bi4%29_%E7%92%A7_disc.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/8/8f/B%C3%AC_%28bi4%29_%E7%92%A7_disc.jpg)

**Russian:**

A Han Dynasty bi, 16 cm in diameter.

**English:**

A Han Dynasty bi, 16 cm in diameter.

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/bi.JPG#clickable)](Media of the Past - pro_vladimir — LiveJournal_files/bi.JPG)
-->

[![](http://www.ljplus.ru/img4/u/t/utnapishti/bi.JPG#clickable)](http://www.ljplus.ru/img4/u/t/utnapishti/bi.JPG)

<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/NzXaf7xwtprAiXaDDIkIDZZvqomOtthuAhJArYcI7SmF7YbyppVMrzrsBXJh.gif#clickable)](Media of the Past - pro_vladimir — LiveJournal_files/586432cegbcdaea7797a1&690)
-->

[![Image no longer available](http://s2.sinaimg.cn/middle/586432cegbcdaea7797a1&690)](http://s2.sinaimg.cn/middle/586432cegbcdaea7797a1&690)


<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/1061.jpg#clickable)](%D0%9D%D0%BE%D1%81%D0%B8%D1%82%D0%B5%D0%BB%D0%B8%20%D0%B8%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D0%B8%20%D0%BF%D1%80%D0%BE%D1%88%D0%BB%D0%BE%D0%B3%D0%BE.%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/1061.jpg)
-->

[![Image no longer available](http://auctionhongkong.com/images/A2/1061.jpg#clickable)](http://auctionhongkong.com/images/A2/1061.jpg)


<!-- Local
[![](Media of the Past - pro_vladimir — LiveJournal_files/2011-02/1297858035_bezimeni-93.gif#clickable)](Media of the Past - pro_vladimir — LiveJournal_files/2011-02/1297858035_bezimeni-93.gif)
-->

[![Image no longer available](http://extrasenstop.ru/uploads/posts/2011-02/1297858035_bezimeni-93.gif#clickable)](http://extrasenstop.ru/uploads/posts/2011-02/1297858035_bezimeni-93.gif)


© All rights reserved. The original author retains ownership and rights.
