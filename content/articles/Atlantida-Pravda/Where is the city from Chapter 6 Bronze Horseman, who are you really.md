title: Where is the city from? Chapter 6. Copper Rider, who are you really?
date: 2013-11-06
modified: 2022-04-24 10:08:34
category: 
tags: St Petersburg
slug: 
authors: ZigZag
summary: In the poem "The Bronze Horseman", well-known author Alexander Pushkin created several misconceptions.
status: published
originally: Where is the city from? Chapter 6. Bronze Horseman, who are you really?.html
from: http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/6.html
local: Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files

### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/6.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/6.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available     from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-   links.html). The translation below is kept purely for archival reasons.*

The well-known Alexander Pushkin in the poem "The Bronze Horseman" became the author of several misconceptions.

Why copper? It is bronze, but as they say, "believe what is written, for you cannot cut it down with an axe.

In the author's note to his line "to cut a window to Europe", he directly refers to the original source - the French words of Francesco Algarotti:

> St. Petersburg is the window through which Russia looks on to Europe

But sources of mass knowledge such as school textbooks and the notorious Wikipedia - supported by official historians of all stripes and ranks - persistently broadcast:

> To cut a window to Europe" — a catch phrase from the poem of A. S. Pushkin's "The Bronze Horseman", describing the foundation of the city of St. Petersburg by Peter I - the first seaport of the Moscow state

although there was no sea port in the city in the time of Peter I. The only real sea port then - and now - is Kronstadt on the island of Kotlin. Due to a section of shallow water 27 nautical miles (47 km) long, St. Petersburg was denied the right to be called a "door" (port - gate, door). At that time it remained only a "window to Europe".

Another misconception:

In the fifth note to the poem "The Bronze Horseman" Pushkin refers to a poem by Mickiewicz. And the lines from the poem "Monument of Peter the Great" in literal translation sound like this:

> To the first of the kings who created these miracles, 
>
> Another Queen erected a monument.
>
> Already the king, cast in the image of a giant,
>
> Sat on the bronze ridge (spine? back?) of Bucephalus
>
> I was looking for places to ride into.
>
> But Peter cannot stand on his own land...

For some reason, Mickiewicz mentions the name of Alexander the Great's favorite horse, although it was known that Peter's favorite horse was Lizeta, which was later made into an effigy.

The censor of the poem "The Bronze Horseman" was Tsar Nicholas I. For some reason, he forbade the use of the word "idol" in relation to Peter I.

Perhaps the Tsar knew that the horseman on a horse (though not Peter) was really once a national idol?

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1967ecc2d0b8.jpg#resized)
-->

![](https://4.bp.blogspot.com/-pdNxMmcBH2Q/UXeYMIYO13I/AAAAAAAACtY/TUb7Mws5kxU/s400/1967ecc2d0b8.jpg#resized)

** Russian: **
Вот ещё одно совпадение. 
Пётр I держит руку так, что в неё легко вложить копьё, оно вполне гармонично там бы смотрелось.

** English: **
Here's another coincidence. Peter I holds his hand so that it is easy to put a spear in it; a spear would look quite harmonious there.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/6fe2f29094d3.jpg#resized)
-->

![](https://3.bp.blogspot.com/-WYJM5vz4TC0/UXeY1hBqb2I/AAAAAAAACtg/gItDnHG23MY/s400/6fe2f29094d3.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/6fe2f29094d13.jpg#resized)
-->

![](https://1.bp.blogspot.com/-vevAdRFBWQQ/UXeaVXAYrYI/AAAAAAAACt0/1-WZAD4mzS0/s400/6fe2f29094d13.jpg#resized)

The horse stepped on the snake with its right hind foot, just like a book. And the position of the hand and head is not so difficult to edit. Not all monuments have a cloak (cape) from the time of Alexander the Great. And this is a completely different hero:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/21433b51314b.jpg#resized)
-->

![](https://1.bp.blogspot.com/-gogOws63yjs/UXeaxjAKO4I/AAAAAAAACt8/PkQeNKu_ZFQ/s320/21433b51314b.jpg#resized)

St. George The Victorious.

...And here's the "Petrovsky" Altyn (a three-kopeck piece).

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_8fa45_540ea9e0_XL.jpg#resized)
-->

![](https://4.bp.blogspot.com/-_rLKZ8dIv-o/UaxcXsk95hI/AAAAAAAADRs/oVDTnkwrXho/s400/0_8fa45_540ea9e0_XL.jpg#resized)

And this is a penny of Ivan V Vasilyevich the terrible.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1338026594_1kop1550.jpg#resized)
-->

![](https://2.bp.blogspot.com/--dUSVskISBw/Ufj6g00V-1I/AAAAAAAAD8Y/4-b93dzG1Ws/s320/1338026594_1kop1550.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/35735_1.jpg#resized)
-->

![](https://4.bp.blogspot.com/-jts7x4B2u5k/Ufj6qHvSjZI/AAAAAAAAD8g/NwynNG8qRAA/s320/35735_1.jpg#resized)

And here on Wikipedia is a well-known seal - the seal of Ivan III, from 1497:
<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/Seal_of_Ivan_3.png#resized)
-->

![](https://2.bp.blogspot.com/-mMysqra8ocg/UgHuixvgJZI/AAAAAAAAEBM/PkWKCFWSetM/s400/Seal_of_Ivan_3.png#resized)

Another interesting fact: historians attribute the creation of the equestrian statue of Peter to the sculptor Etienne Falcone in 1768-1770, but the head of Peter itself was sculpted by a student of this sculptor, Marie-Anne Collot... Why would that be?

The legend made up by guides - about lightning hitting a stone - is also confusing. The very name Thunder-stone appeared, allegedly, because of a lightning strike. More precisely, lightning explains the front granite prefix to the pedestal, which seems to form a very intricate crack.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1090b0cdcf769.jpg#resized)
-->

![](https://4.bp.blogspot.com/-SdFGydKILoI/UZDa0d88sLI/AAAAAAAAC4Q/FeJtmwehQuI/s400/1090b0cdcf769.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/315e9db07571.jpg#resized)
-->

![](https://2.bp.blogspot.com/-PLeczvImgJ0/UZDa1EVwiFI/AAAAAAAAC4U/IAOgUR5qo5A/s400/315e9db07571.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/f914d3a23e21.jpg#resized)
-->

![](https://2.bp.blogspot.com/-BqIRnHTLH2I/UZDa3w0zURI/AAAAAAAAC4g/5c5r24t17qA/s400/f914d3a23e21.jpg#resized)

Surprisingly, the crack runs exactly along the border of various colored (chemical and crystal) structures of granite, and the band of enlarged inclusions also abruptly and unnaturally ends at this border.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1.jpg#resized)
-->

![](https://3.bp.blogspot.com/-jmrKKdkP9vc/UZDPcTojc1I/AAAAAAAAC3I/asgMbNMDjJc/s640/1.jpg#resized)

And the most important thing... The monument has not only one granite insert, but two of them, front and back.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/img_6154_1.jpg#resized)
-->

![](https://1.bp.blogspot.com/-LDIzcXCcmvo/UZtqNZcaImI/AAAAAAAADFY/pUYYMY0ixgk/s640/img_6154_1.jpg#resized)

Look here. The historical version reads: 

> A rock was sitting there, then lightning struck it, and - as if in a fairy tale - the resulting crack that ran through it changed the color, structure, orientation of the crystals, even the size of the grain...

Do you believe this? If so, then for you the whole fictional story of the city's construction is also true. The added fragment looks more like the result of restoration after the destruction of the front and back parts of the monument's pedestal. The whole appearance of the pedestal, its treatment and the wavy slabs laid around it indicate that it once represented not just a wild rock, but the crest of a wave - and that the original was destroyed.

Perhaps it originally looked something like this:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/d5bd5817061e.jpg#resized)
-->

![](https://3.bp.blogspot.com/-cXOQDYPPHJc/UZDRY_LhHcI/AAAAAAAAC3c/sl775y8F56o/s400/d5bd5817061e.jpg#resized)

The sharp chipping of the stone in front looks very unnatural next to the smooth features of the base, they look more like a sea wave without a crest.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/7ca17c5bf0d0.jpg#resized)
-->

![](https://4.bp.blogspot.com/-xEM0FmpJSng/UoXZ0Pmk-UI/AAAAAAAAFZs/I_HrV2C3Bj4/s400/7ca17c5bf0d0.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/46b485a4a780.jpg#resized)
-->

![](https://3.bp.blogspot.com/--8mlhVZIrW8/UoXZ0EsBFdI/AAAAAAAAFZo/Eg9ew96MgmY/s400/46b485a4a780.jpg#resized)

In addition, the snake under the hoof looks more comical than symbolic.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/048906d3ac4e.jpg#resized)
-->

![](https://2.bp.blogspot.com/-0VhubEL3QGs/UoXalN1s_uI/AAAAAAAAFaA/P6ZOgCW3XAo/s400/048906d3ac4e.jpg#resized)

Its large scales are closer to dragons' scales.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/b04a90e4387c.jpg#resized)
-->

![](https://1.bp.blogspot.com/-Z_Btvk4LdMA/UoXalA09U-I/AAAAAAAAFZ8/y5GJleoVowk/s400/b04a90e4387c.jpg#resized)

And the head without scales looks completely unnatural.


<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/snakes4.jpg#resized)
-->

![](http://1.bp.blogspot.com/-ZeO1pE_mWbY/UoXbXXzePgI/AAAAAAAAFaU/6xbgd4WIrc0/s1600/snakes4.jpg#resized)


<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/snakes2.jpg#resized)
-->

![](https://3.bp.blogspot.com/-9IomDwhFWj8/UoXbAXrn_4I/AAAAAAAAFaM/zUZqvh9b_Fw/s200/821085542.jpg#resized)


The details of the horse and rider were cast very delicately, but the snake is hack. Perhaps the snake is all that Falcone had the strength to cast? 

Although, history says he did not even cast the snake. It was made by Fyodor Gordeev. From official sources:

> The model of the equestrian statue of Peter was made by the sculptor Etienne Falcone in 1768-1770. Peter's head was sculpted by his pupil, Marie-Anne Collot. The snake was designed by Falcone and sculpted by Fyodor Gordeev. The casting of the statue was carried out under the supervision of master Yemelyan Khaylov and was completed in 1778. Architectural and planning solutions and general guidance was provided by Y. M. Felten.

Until 1844, no one knew that this monument was given to Peter I by Catherine, and there is no sign of any inscription on the painting by N. M. Vorobyov.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1246097203_4.jpg#resized)
-->

![](https://1.bp.blogspot.com/-oqFFv5hbi2A/Uleov6i7XbI/AAAAAAAAEzw/Uz7qOKk8uyI/s640/1246097203_4.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/surikov.jpg#resized)
-->

![](https://2.bp.blogspot.com/-2GETNwk-Dfo/Uleqlc7HYKI/AAAAAAAAEz8/gbD4C5p35Xo/s640/surikov.jpg#resized)

Another nuance is surprising. Peter on this monument, however, as on the other, which we will consider below, is sitting without trousers, in a Roman toga, and such clothes were never worn by Russian nobles or shipwrights. The position of the "Copper Rider's" hand also seems familiar.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_8fa6e_9268d486_XL.jpg#resized)
-->

![](https://4.bp.blogspot.com/-Yny_40GFLPY/UaxbAyE40NI/AAAAAAAADRc/iLPwHTfqqaA/s400/0_8fa6e_9268d486_XL.jpg#resized)

Except that this is Marcus Aurelius in Rome.

Why does the Sovereign-Emperor need such an outfit? It is not good for a Russian autocrat to flaunt himeslf without trousers! Moreover, Peter is sitting on a horse without stirrups. And what does history say: the stirrup was invented in the fourth century. From this we can draw a clear conclusion that this rider lived no later than the fourth century, and the statue must also have been cast much earlier than in the 18th century.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/P10002311.jpg#resized)
-->

![](https://3.bp.blogspot.com/-imQY0O2LCBQ/UZsYCPqPszI/AAAAAAAADDs/g2hADMRrzEc/s640/P10002311.jpg#resized)

And when did the sovereign indulge in such weapons?

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/kortik.jpg#resized)
-->

![](https://4.bp.blogspot.com/-o0C7flxLA4I/UZsZJ2esISI/AAAAAAAADD8/rjOQMozMB5I/s320/kortik.jpg#resized)

In the time of Peter I, the army was not armed with swords, but with sabers.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/sablia9_min.jpg#resized)
-->

![](https://2.bp.blogspot.com/-Z5kxq3QVu-4/UZsSNj1mXHI/AAAAAAAADCk/PE8Sq9tiRIU/s400/sablia9_min.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_69124_31f1e136_XL.jpg#resized)
-->

![](https://1.bp.blogspot.com/-yBEZXeFJopM/UZsST7M4hII/AAAAAAAADCs/gmY_FaCy0Ag/s640/0_69124_31f1e136_XL.jpg#resized)

Hence the question: who armed the bronze horseman with a sword?

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/1289841782_75.jpeg#resized)
-->

![](https://1.bp.blogspot.com/-ZNdt02fVIso/UZsUJYsjJcI/AAAAAAAADDM/Y5FtNVaeyvo/s640/1289841782_75.jpeg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/neaSCavZ2G.jpg#resized)
-->

![](https://1.bp.blogspot.com/-9m9UvK_4uTo/UZsUD37GnQI/AAAAAAAADDE/sOBMy8rWRWA/s400/neaSCavZ2G.jpg#resized)

Does Bucephalus' stance remind you of anything?

This is how A. Makedonsky (Alexander the Great) was always depicted on a horse.

And here is a monument to Alexander the Great in Skopje:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/alexander-makedonski-monument-skopje.jpg#resized)
-->

![](https://2.bp.blogspot.com/-ailRW187cMw/UZR8Msn3MxI/AAAAAAAAC-I/vIiJNyeXBzE/s400/alexander-makedonski-monument-skopje.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/6f5e497d2cd8.jpg#resized)
-->

![](https://3.bp.blogspot.com/-_VP7nS3i2_U/UZR8tRGEwaI/AAAAAAAAC-Y/UwrikVI_Ik4/s400/6f5e497d2cd8.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/y_749a889f.jpg))
-->

![](https://3.bp.blogspot.com/-NNr5eQtqJBM/UZR8Tqqee_I/AAAAAAAAC-Q/irqs4ZBJUPw/s400/y_749a889f.jpg#resized)

The sword, the horse, the cloak, the harness on the horse, and the rider's clothing - do they not remind you of anything? But the real Peter I...

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_45fc9_66ddf9c8_XL.jpg#resized)
-->

![](https://2.bp.blogspot.com/-_2xoU43cPLQ/UoCfJoX7zQI/AAAAAAAAFNw/54S0Lxnd4F8/s640/0_45fc9_66ddf9c8_XL.jpg#resized)

This was how he was supposed to sit on his favorite mare, Lisette.

A poem by this author *"The Bronze Horseman" from a different angle.* (it's not exactly Pushkin)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/%D1%86%D1%86%D1%86.jpg))
-->

![](https://1.bp.blogspot.com/-chs8dhHmOUE/UayMNhIA6tI/AAAAAAAADR8/1ief_QvV-iM/s320/%D1%86%D1%86%D1%86.jpg#resized)

> Resplendent with bronze over the Neva,

> And clouds loins teasing,

> He is full of rainwater,

> This is a strange land for him.

> Granite shackles itch,

> Away from the enemy columns...

> And Macedonian Sasha again

> Going to ancient Babylon.

According to the records of bakmeister Ivan Grigoryevich, the bibliographer of Catherine the Great:

> She already had the image of PETER the Great sculpted", which is still preserved, however, it did not satisfy the desired intention. An ordinary pedestal, on which most of these statues are built, does not mean anything and is not able to excite a new reverent thought in the soul of the viewer. The monument, erected by Catherine, should CORRESPOND to the dignity of the noblest and most majestic way. The chosen pedestal to the sculptured image of the Russian iroi (king?) should be a wild and inconvenient stone, on which he is represented riding on a horse with his right hand outstretched. A new, bold, and much-expressed thought! The stone itself as an ornament should remind you of the state of the State at that time and of the difficulties that the creator of it had to overcome when manifesting his intentions. How perfectly the chosen allegory resembles its subject is proven by the fact that PETER the Great had a seal on which he was depicted as a stonecutter carving a statue of a female person, that is, Russia. 

> The calm position of the rider depicts the undaunted courage and spirit of iroi, who feels his majesty and is not afraid of any danger. The speed of a furious horse reaching the top of a stone mountain shows the speed of its affairs and the successful changes made by its power in its indefatigable work. The outstretched right hand is the sign of the commander, who blesses his loyal subjects and cares for the welfare of his possessions as the Father of the Fatherland.

This is a quote from: "*Historical news about the sculptured equestrian image of Peter the Great Composed by the collegiate assessor and librarian of the Imperial Academy of Sciences, Ivan Burmeistera. Translated by Mykola Karandasheva. — SPb.: Type. Snore, 1786*". The original text was in German.

What this text says, is that the monument apparently tilted (or even fell). It was damaged, so they say, which is why it was sent for restoration. As a result of which, it was subject to a small alteration, namely, the head and right hand were sawn off and completely new parts of a different shape were soldered to it.

Here is a version made up for posterity, to successfully fit into the academic work. Excerpt from Falcone's letter to Catherine II:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/729cbb6c6f48.jpg#resized)
-->

![](https://1.bp.blogspot.com/-7KC175V_FX0/UhW0TtoHwUI/AAAAAAAAEOE/dvi_-zQAer4/s640/729cbb6c6f48.jpg#resized)

Author Kaganovich A. *The Copper Horseman. History of the monument. - 2nd Ed., additional - L.: Art, 1982. p. 150*. It is quite a "suitable document" for later generations who may have all sorts of questions about the presence of a seam in the area of the head and shoulder on the solid casting of the monument...

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_a6329_82101830_orig.jpg#resized)
-->

![](https://3.bp.blogspot.com/--79BDbN6AkE/U3Mts_pu0bI/AAAAAAAAGy8/Lec50KmpYrg/s1600/0_a6329_82101830_orig.jpg#resized)

The text below this drawing also speaks for itself. (Translator's note: text from image needs English translation...)

The pedestal also needed to be restored. It was necessary to update the fallen-off parts, a large piece in front and a smaller one in the back.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/-pr2KqPFntc.jpg#resized)
-->

[![](https://1.bp.blogspot.com/-11wv8dsBZKY/UhWzaz1Xq2I/AAAAAAAAEN4/BXAE33Js0Tg/s640/-pr2KqPFntc.jpg#clickable)](https://1.bp.blogspot.com/-11wv8dsBZKY/UhWzaz1Xq2I/AAAAAAAAEN4/BXAE33Js0Tg/s640/-pr2KqPFntc.jpg)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/tKPBu-xiW4c.jpg#resized)
-->

[![](https://2.bp.blogspot.com/-BtBhlZCVtSs/UhWzNg87GbI/AAAAAAAAEN0/RkX4cq20H3s/s640/tKPBu-xiW4c.jpg#clickable)](https://2.bp.blogspot.com/-BtBhlZCVtSs/UhWzNg87GbI/AAAAAAAAEN0/RkX4cq20H3s/s640/tKPBu-xiW4c.jpg)

I was very puzzled by another incident. See for yourself:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/%D0%9C%D0%92.jpg#resized)
-->

![](https://2.bp.blogspot.com/-1mXrMTKXzrg/UoCd5kXYlsI/AAAAAAAAFNc/dtvAKhYmo_8/s640/%D0%9C%D0%92.jpg#resized)

The Emperor of Russia would have been more suited to his famous three-cornered hat. He not only did not wear laurel wreaths, he also did not allow paintings of his image in this form during his lifetime.

So is it Peter on the horse or is it not Peter?

Who around the world still liked to portray themselves in this way?

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/GfP-IYlle3k.jpg#resized)
-->

![](https://3.bp.blogspot.com/-NLYgI6zEJ-w/UoCer-XfaUI/AAAAAAAAFNo/85nFP75M6L4/s320/GfP-IYlle3k.jpg#resized)

A shot from a well-known movie.

I can't pass up another drawing:

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/MV1.jpg#resized)
-->

![](https://3.bp.blogspot.com/-bw9QzosJ3WE/U3GwqEQJDqI/AAAAAAAAGys/-_xWQ06xofQ/s1600/MV1.jpg#resized)

The so-called Saint George.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/s640/00259-752x1024.jpg#resized)
-->

![](https://2.bp.blogspot.com/-yzbLbd7-Kf8/V3OL-GSCVnI/AAAAAAAAH3k/R9uEp5JAMc0fYOsMAgsA2JjSZN6XbaguACLcB/s640/00259-752x1024.jpg#resized)

George The Victorious. Sculpted by N. S. Pimenov. 1856. The Hermitage.

Does this sculpture's pedestal remind you of anything?

[Illustrated catalog Of the art Department of the all-Russian exhibition in Moscow, 1882 part 1.](http://humus.livejournal.com/3805541.html)

As a schoolboy, I came across a history book, or rather a historical novel, where the author tried to find Alexander the Great's trail - taking as a basis the legend of the missing Golden Monument to Alexander the Great. (The monument was not all gold; only the armor, helmet and spear, the rest was of bronze). It ended with the fact that the monument was transported from Babylon, drowned somewhere in the area of Malta. Or maybe he swam? Why was he taken there? Who was directing it? The book said that the carriers were knights of the order of St. John of Jerusalem (Maltese Cross). It happened exactly at the end of the 17th century.

Translator's Note: this author memory is reminiscent of the English tale - allegedly historical fact - of the [1216 loss of king John's crown jewels in The Wash' near Kings Lynn](https://www.edp24.co.uk/news/weird-norfolk-kings-lynn-king-john-treasure-mystery-6402776). This area is close to:

- *England's city of Peterborough on the edge of the England's fenlands (parallels with Petersburg and Finland may be noted)*
- *Lincolnshire's ['Stragglethorpe Rider God' finds](https://romanlincolnshire.wordpress.com/2016/08/29/rider-gods-of-roman-lincolnshire/), and*
- *Lincolnshire and Belgium's tales of the horse-riding dragon-slayer [Byard](https://www.lincolnshirelife.co.uk/posts/view/the-legend-of-byards-leap)/[Bayard](https://www.discoverbenelux.com/dendermonde-meet-the-legendary-bayard-steed/).* 

*King John traditionally represents England's monarchy robbing the poor in the Robin Hood's tale, and signed the (probably fake) Magna Carta. Similarly, although this author - ZigZag - does not explicitly say it, Pushkin's poem of the horse trampling a serpent/dragon/griffin is [sometimes treated as a metaphor for the rise of the state based on the labour of its people, but which tramples them too](http://www.tyutchev.org.uk/Download/Bronze%20Horseman.pdf).*

Let's recall the story: in 1798, when Napoleon I captured Malta during an expedition to Egypt, the knights of the order appealed to the Russian Emperor Paul I to assume the rank of Grand Master of the order of St. Nicholas, John of Jerusalem, to which Paul agreed. At the very end of 1798, the Russian Emperor Paul I was proclaimed Grand Master of the order of Malta. So what I'm getting at is: in the late 17th century the monument of Alexander of Macedon was lost, and in the mid-18th century there appeared an updated monument to Peter I. Maybe before the update it looked exactly as in the picture above? 

Another nuance, this warrior in Roman armor does not kill a snake, as we are used to expecting, but a Griffin - a symbol of the great Tartary.

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/102502832_large_imperialtarty.jpg))
-->

![](https://1.bp.blogspot.com/-zx2zb1FgAaY/U3Mvp0yF9yI/AAAAAAAAGzM/a_vzjb5HaGs/s1600/102502832_large_imperialtarty.jpg#resized)

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/102502833_large_tartarychina.jpg#resized)
-->

![](https://4.bp.blogspot.com/-uxgKwRGDkEk/U3Mvp-xJ4TI/AAAAAAAAGzI/YN_VYmO51zE/s1600/102502833_large_tartarychina.jpg#resized)


Why would that be?

<!-- Local
![](Where is the city from? Chapter 6. Bronze Horseman, who are you really?_files/0_92781_eaf3d646_XXXL.jpg#resized)
-->

![](https://4.bp.blogspot.com/-dIL-O7_4DTw/UjKllqGP45I/AAAAAAAAEjM/6_rHNciev4c/s640/0_92781_eaf3d646_XXXL.jpg#resized)

[Drawing from an 1833 album](http://humus.livejournal.com/3069766.html)

The remains of building materials that were used for repairs have not yet been removed.

History does not hide this:

> Peter's head was sculpted by a student of the sculptor E. Falcone, Marie Anne Collot. The snake was designed by Falcone and sculpted by Fyodor Gordeev. The casting of the statue fragments was carried out under the supervision of master Yemelyan Khaylov and was completed in 1778. Architectural and planning decisions and General management were carried out by Yu. M. Felten... and below the signature: the author of the monument is Etienne Falnone. 

Interesting, right?

> Falcone, who had never before had to perform such work himself, refused to finish the monument on his own and waited for the arrival of the French master B. Ersman. The caster, accompanied by three apprentices, arrived on 11 May 1772, with everything necessary to guarantee success: "earth, sand, clay...". However, the long-awaited master could not fulfill the requirements of the sculptor and soon, at Felten's insistence, was dismissed. Ersman simply refused to engage in the task assigned to him. From that moment on, all the preparatory work for the casting was carried out by Falcone himself. To assess the tension of the situation and the relations of the actors, it is necessary to cite the sculptor's letter of November 3, 1774 to Catherine II, appealing to her patronage: 

> Your most gracious Majesty, at the beginning of last month, Mr. Betskoy ordered me to write through Felten my demands for the completion of the casting (here we should read "alterations") of the statue, although this formality seemed unnecessary to me, nevertheless I immediately sent a letter, from which I attach a copy, since then I have not received an answer. Without your August patronage, I am at the mercy of a man who hates me more every day, and if Your Majesty does not wish to see me more, I should have to live here worse than any stranger who finds a patron at the end...

Here is what Falcone himself wrote about the monument:

> My monument will be simple… I will confine myself to the statue of this hero, whom I do not treat as either a great general or a conqueror, although he was, of course, both. The personality of the creator-legislator is much higher...

Here Falcone clearly let slip about "the great commander and conqueror". For the authenticity of the idea, the sculptor engraved the inscription "sculpted and cast by Etienne Falcone Parisian of 1778" on one of the folds of the cloak of the "Bronze Horseman".

Such were the passions that raged at that time, but the attempt to falsify the origin of the monument, thanks to Pushkin's poem of the same name, was one hundred percent successful.

When copying an article, do not forget to indicate the authorship.

The full version address is here: [Where is the city from? (by ZigZag)](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/6.html)

© All rights reserved. The original author retains ownership and rights.
