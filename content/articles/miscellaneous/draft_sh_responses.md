title: draft SH responses
date: 2022-01-23 08:39
modified: 2022-01-23 12:05:30
category: 
tags: 
slug: 
authors: RoHT
summary:
status: draft


#### [Meta-historical conspiracies: Part 1](https://stolenhistory.net/threads/meta-historical-conspiracies-part-1.5797/post-108099). Originally [here](https://blog.plan99.net/meta-historical-conspiracies-part-1)

![[](../../images/sh_responses/modern_vellum_production_in_England.jpeg)]

> There’s another strange problem to do with the cost of books. All these manuscripts were written on parchment, a writing material made of animal skins. The production of parchment was extremely expensive. It required animals to be raised, fed, killed and skinned. Then the skin had to be washed and scrubbed with lime, left to soak for days, pinned to a frame and left to dry under tension for yet more days, then treated and have hair removed again, before being scrubbed once more. One animal might yield only 3–4 pieces of writing material.

i> So as you can see, the production of a book was an extraordinarily expensive endeavour. There’s even a word for a parchment that’s been scraped clean of text and reused — a palimpset. This leads to two questions:

> - Why were these books produced at such great expense, dumped in the wildest reaches of the Empire, then abandoned unread?
> - Given the expense of writing material it was common in early texts for authors to save space via extreme measures, like skipping vowels. How did Roman authors manage to develop such a fluent and flowery writing style that they easily filled dozens of books in a single project? They didn’t know about paper yet sound as if they’ve been writing their entire lives.

See [https://stolenhistory.net/threads/anthropodermic-bibliopegy.3834/](https://stolenhistory.net/threads/anthropodermic-bibliopegy.3834/) and obsidian post: 'Britain's Real History - Post 13'.

Only expensive and troublesome until you modify one of your primates for a hide that is:
- hairless
- white

To make this easier, let's lose any denial that leather is made from human skin:

<!-->
- Pergamena [https://www.pergamena.net/store](https://www.pergamena.net/store)
- Pergamena Uterine Calf parchment https://www.pergamena.net/products/binding-uterine-calf-parchment
-->
- Human Leather [http://www.humanleather.com/](http://www.humanleather.com/)
- Fashion that gets under the skin: Alexander McQueen Leather [https://www.nytimes.com/2016/07/19/fashion/leather-dna-alexander-mcqueen.html](https://www.nytimes.com/2016/07/19/fashion/leather-dna-alexander-mcqueen.html)

Parchment and vellums for books are best made from the smooth skin. Smooth, new skin that:

> has not been exposed to any outside elements

Such as 'uterine calf' parchment. Source: https://www.pergamena.net/products/binding-uterine-calf-parchment

StolenHistory.net: [https://stolenhistory.net/threads/anthropodermic-bibliopegy.3834/](https://stolenhistory.net/threads/anthropodermic-bibliopegy.3834/). included the below?
Links to websites run by people who identify, catalogue, collect or own books made from human skin. They tend to focus on covers  of human skin rather than the pages (vellum/parchment). More details on anthropodermic bibliopegy (books made from human skin):

Most of the below links are from the transcript of the Ologies Podcast, dated 2020-10-20, which can be downloaded from: [https://static1.squarespace.com/static/5998d8226f4ca3396027aae2/t/5ff4ee1e77db40531a08848a/1609887264230/Ologies+-+Anthropodermic+Biocodicology.pdf](https://static1.squarespace.com/static/5998d8226f4ca3396027aae2/t/5ff4ee1e77db40531a08848a/1609887264230/Ologies+-+Anthropodermic+Biocodicology.pdf)

![[Ologies+-+Anthropodermic+Biocodicology.pdf]]

This discussion mentioned preservation of human skin in urns of urine. We mentioned the etymological link between 'urn' as in 'large storage jar' and 'urine' in section 18 of the first Cannibalism/IHASFEMR SH post at https://stolenhistory.net/threads/cannibalism-in-humans-great-apes-prion-diseases-and-mrna-therapy.4161/post-42529

[*Dark Archives: A Librarian's Investigation Into the Science and History of Books Bound in Human Skin*](https://us.macmillan.com/books/9780374134709)

[*Dark Archives* author Megan Rosenbloom's website:](https://meganrosenbloom.com/)

[*Dark Archives* author Megan Rosenbloom's Death Salon enterprise:](https://deathsalon.org/)

Identification of Mammalian Sources of Cultural Materials: Dr Daniel Kirby [https://projects.iq.harvard.edu/pmfcm/people/daniel-kirby-phd](https://projects.iq.harvard.edu/pmfcm/people/daniel-kirby-phd)

Harvard confirms book is bound in human skin: [https://www.thetimes.co.uk/article/harvard-confirms-antique-book-is-bound-in-human-skin-kcfhlxfx0d5](https://www.thetimes.co.uk/article/harvard-confirms-antique-book-is-bound-in-human-skin-kcfhlxfx0d5)

The first confirmed human skin book: *Des destinees de l'ame* [http://www.thehistoryblog.com/archives/30815](http://www.thehistoryblog.com/archives/30815)

The Muttermuseum [http://muttermuseum.org/](http://muttermuseum.org/)

Racism and anatomy cadavers [https://www.michiganradio.org/post/recovering-brain-surgery-detroit-artist-explores-black-men-rest-new-exhibit](https://www.michiganradio.org/post/recovering-brain-surgery-detroit-artist-explores-black-men-rest-new-exhibit)
The poor, the Black, and the marginalized as the source of cadavers in the United States anatomical education [https://pubmed.ncbi.nlm.nih.gov/17226823/](https://pubmed.ncbi.nlm.nih.gov/17226823/)

So that's why you would develop a white skinned dexterous primate. Now, why would you keep them underground? Why would the London Olympics 2012 opening ceremony show them emerging from underground?

--- 

That piece had two follow ups:

- Review [Meta-historical conspiracies: Part 2](https://stolenhistory.net/threads/meta-historical-conspiracies-part-2.5798/) Originally [Meta-historical conspiracies: Part 2](https://blog.plan99.net/meta-historical-conspiracies-part-2-197a70c13fa9)

- Review [Meta-historical conspiracies: Part 3](https://stolenhistory.net/threads/meta-historical-conspiracies-part-3.5799/) Originally [here](https://blog.plan99.net/meta-historical-conspiracies-part-3-808e7d9e385d)

They follow the theme "I don't necessarily believe this. I'm just sayin...". There are two three-part series like this, the other series being the ones from unz.com. Also re-posted on SH by Dreamtime...

1. [How Fake is Roman Antiquity?](https://stolenhistory.net/threads/how-fake-is-roman-antiquity-part-1-3-unz-review.5573/) from [here on unz](https://www.unz.com/article/how-fake-is-roman-antiquity/)

2. [How Fake is Church History](https://stolenhistory.net/threads/how-fake-is-church-history-part-2-3-unz-review.5574/) from [here on unz](https://www.unz.com/article/how-fake-is-church-history/)

3. [How Long was the First Millennium?](https://stolenhistory.net/threads/how-long-was-the-first-millennium-part-3-3-unz-review.5580/) from [here on unx](https://www.unz.com/article/how-long-was-the-first-millenium/)


#### [Iseidon on rivers](https://stolenhistory.net/threads/my-way-here-my-vision-for-future-research-directions.5813/post-108380)

> Second. Create a database of rivers. Rivers, in my opinion, are the clearest example of the man-made component of the world. If in the random construction of a single architecture, but by different architects and schools, you can still somehow believe (for the layman, of course). But it is difficult, if not impossible, to believe in accidental activities in relation to rivers. Considering that the population of the world is mainly distributed along the rivers, the rivers, in my opinion, cannot be bypassed.

Tip: use [Yandex maps](https://yandex.ru/map-constructor)

And from [later post](https://stolenhistory.net/threads/my-way-here-my-vision-for-future-research-directions.5813/post-108392)

> Frankly, I "wonder" why there are no serious works on this subject. In fact, I am interested in this topic (rivers) also because I think that I assume the existence of underground sub-mountain cities and bunkers in the areas of river sources. Plus there is water creation there, which makes them an ideal military target. So it's convenient to combine important "natural" sites (river headwaters) with military sites to reduce the cost of defending them.

Response:

Sibved illustrates this emerging river thing at: [Karst Sinkholes - Giant Water Sources of the Past](https://sibved-livejournal-com.translate.goog/317056.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
and [Water outlets from mountains and hills part 1](https://sibved-livejournal-com.translate.goog/309956.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB) 
and [https://sibved-livejournal-com.translate.goog/311552.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB](https://sibved-livejournal-com.translate.goog/311552.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
and [Plasticine Mountain of El Fuerte in Bolivia](https://translate.google.com/website?sl=ru&tl=en&u=https://zen.yandex.ru/media/sibved24/plastilinovaia-gora-elfuerte-v-bolivii-5d8cbb8f74f1bc00adad5e1c)
and [Water out of Stone in Australia](https://sibved-livejournal-com.translate.goog/311552.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
and [Lakes Formed by the Release of Salt Water from Deep Inside Earth](https://sibved-livejournal-com.translate.goog/314630.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

