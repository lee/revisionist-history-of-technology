title: English History, Chronology and Technology Sites
date: 2020-12-08
modified: 2022-08-25 09:13:34
category: 
tags: links
slug: 
authors: Various
summary: Links to new chronology and alt history sites' English-language pages
local: N/A
status: published
originally: N/A

#### History

[digitall-angell English pages](https://d-a-english.livejournal.com/)

[https://geolines.ru/eng/](https://geolines.ru/eng/) AKA Objective History

[Tart-Aria.info](https://www.tart-aria.info/en/)

[Nikolay Levashov - The Untold History of Russia](http://www.nikolay-levashov.ru/English/Articles/History-1-eng.html)

[Chronologia](http://chronologia.org/en/index.html)

[CronoLogo](http://www.ilya.it/chrono/en/index.html)

[Andrei Stepanenko](https://chispa1707.blogspot.com/)

[Tim Cullen's MalagaBay](https://malagabay.wordpress.com/) [RSS](https://malagabay.wordpress.com/feed)

[Jef Demolder's Abyss of Time](https://jefdemolder.blogspot.com/)

[WildHeretic](http://www.wildheretic.com/forum/)

[WeeWarrior's Wondrous Wonders Weblog](https://weewarrior.wordpress.com/)

[StolenHistory.org](https://www.stolenhistory.org/)

[StolenHistory.net](https://www.stolenhistory.net/)

[Reddit: /r/CulturalLayer](https://www.reddit.com/r/CulturalLayer/)

[Reddit: /r/CulturalLayer links](https://www.reddit.com/r/CulturalLayer/comments/asfijs/best_of_culturallayer_and_resource_guide_amended/)

[https://stephanhuller.blogspot.com/](https://stephanhuller.blogspot.com/)

[https://varchive.org/](https://varchive.org/)

Alexander Koltypin's [English Earth Before the Flood](http://earthbeforeflood.com). Many pages are in Russian so use the root Russian site with a translator per [Non-English sites](./links-to-non-english-sites.html)

[GodLikeLunatics64](https://godlikelunatics64.createaforum.com/general-discussion/)

[GodLikeProductions](https://godlike.com/newthreads.php)


#### Technology

[GS Journal](https://www.gsjournal.net/)

### Foreign-language sites with a built-in translate option

[https://atlantida-pravda-i-vimisel.blogspot.com](https://atlantida-pravda-i-vimisel.blogspot.com)

[Falsehood.md](http://falsehood.md)

[De Grazia static electricity](http://bearfabrique.org/History/degrazia.html)

