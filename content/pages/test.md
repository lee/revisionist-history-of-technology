title: Test
date: 2021-01-05 11:44
modified: 2022-04-09 15:16:53
category: 
tags: 
slug: 
authors: RoHT
summary: Scratchpad for testing RoHT site enhancements at: https://178.62.117.238/drafts/pages/test.html
status: draft

Test mp4 serving from RoHT mp4 server

<!--
Below are proven working or working-problematic with explanations:

Two '../' instead of one because this page is published in 'drafts' folder
[Link to mp4 video](../../videos/test.mp4)

And again with a front image:

Method 1:

[![Alternate Text](../../images/test_video.png)](../../videos/test.mp4 "Link Title")
Allegedly adds player controls. Image created to 'front' this video.

Method 2:

[![](../../images/test_video.png)](../../videos/test.mp4)
Image created to 'front' this video.

Method 3: Embed but video starts on page load and video cannot be resized:

<embed src="../../videos/test.mp4" width="400px" height="200px">

But starts on load.

Method 4: HTML5 The best. Video does not start on page load like Method 3 and you can expand video size

<video controls="controls" width="400px" height="200px">
    <source src="../../videos/test.mp4" type="video/mp4">
    Your browser does not support the HTML5 Video element.
</video>

-->

Method 5: HTML5 As above but with width set by bootstrap's css (hopefully)

<video controls="controls">
    <source src="../../videos/test.mp4" type="video/mp4">
    Your browser does not support the HTML5 Video element.
</video>


##### Embedding tests

From: [http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/](http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/)

- [Artificial dumps in Bashkiria. Evidence of Terraformation.](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/iskusstvennye-otvaly-bashkirii-dokazatelstva-terraformacii/)
    
    - [![](https://img.youtube.com/vi/6AI-iPkIDKs/0.jpg)](https://youtu.be/6AI-iPkIDKs)

    - [English subtitle .srt file:](../../files/Искусственные отвалы Башкирии.  Доказательства Терраформации.-6AI-iPkIDKs.srt)

##### ToDo

Test table striping:

| Protocol       | Contact Information
|:-------------- |:-----------------------------------------------------|
| jabber/xmpp    | [winlu@jabber.at](winlu@jabber.at)                   |
| email          | [derwinlu@gmail.com](mailto:derwinlu@gmail.com)      |
| IRC - freenode | winlu                                                |

Should look great and has row striping.

Now a quote test:

Yes. From [*Forty years' researches in British and Saxon burial mounds of East Yorkshire*, John Robert Mortimer, 1905](https://archive.org/details/fortyyearsresear00mort_0), Introduction, page xxiv:

> The fact that broken and dismembered human bones - often associated with animal bones - mixed in the grave and in the mound, have been found in so many of the barrows I have opened, would seem to indicate that they had often served the same purpose as did the animal bones. These scattered human bones, like the associated animal bones, are generally in far better preservation than those of the internment below. They have also a different appearance, as if they had been somewhat changed, probably by some process of cooking. Repulsive as cannibalism is, the evidence of its existence in the barrows of this neighbourhood seems to me so strong as to place its former practice beyond doubt. 

In [http://hiddenea.com/quest3.htm](http://hiddenea.com/quest3.htm), Mike Burgess continues sorting through various versions of the story of Norfolk giant Tom Hickathrift and his battle with another giant (or, depending on the version, with villagers). Next to Hickathrift's alleged grave - seemingly a barrow - ([Google Maps](https://www.google.com/maps/search/?api=1&amp;query=52.663658,0.251153)), ([OpenStreetMap](https://www.openstreetmap.org/?mlat=52.663658&mlon=0.251153&zoom=13#map=13/52.663658/0.251153)) - were two depressions. One called "Hickathrift's Hand Basin" ([Google Maps](https://www.google.com/maps/search/?api=1&amp;query=52.663847,0.250673)), ([OpenStreetMap](https://www.openstreetmap.org/?mlat=52.663847&mlon=0.250673&zoom=13#map=13/52.663847/0.250673)), the other called "Hickathrift's Feeding Bowl" ([Google Maps](https://www.google.com/maps/search/?api=1&amp;query=52.663658,0.251153)), ([OpenStreetMap](https://www.openstreetmap.org/?mlat=52.663658&mlon=0.251153&zoom=13#map=13/52.663658/0.251153)).

Sounds like Tom was a big eater.

Locations:

1. Tom Hickathrift's Grave: (possibly at 52.663658, 0.251153) 
2. Tom Hickathrift's Hand Basin: (52.663847, 0.250673)
3. Tom Hickathrift's Feeding Bowl: (52.663658, 0.251153).

