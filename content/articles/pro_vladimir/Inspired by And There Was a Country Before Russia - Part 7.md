title: Inspired by "And there was a Country before Russia". Part 7
date: 2013-10-26
modified: Fri 02 Apr 2021 20:06:42 BST
category: pro_vladimir
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: In the process of searching, the eye often came across a description of a certain "Greek fire" that the Byzantine Empire possessed.
status: published
originally: По мотивам «И была Страна до России». Часть 7.html

### Translated from:
[https://pro-vladimir.livejournal.com/2330.html](https://pro-vladimir.livejournal.com/2330.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html) and [Ivan and Mary](http://dmitrijan.livejournal.com/91508.html)

<!-- Local
[![](По мотивам «И была Страна до России». Часть 7._files/23853_original.jpg#clickable)](По мотивам «И была Страна до России». Часть 7._files/23853_original.jpg)
-->

[![](https://ic.pics.livejournal.com/dmitrijan/42576892/23853/23853_original.jpg#clickable)](https://ic.pics.livejournal.com/dmitrijan/42576892/23853/23853_original.jpg)

In the process of searching, the eye often came across a description of a certain "Greek fire" that the Byzantine Empire possessed. This miracle weapon was attributed to them as exclusive owners. The secret of this weapon was jealously guarded.

[http://lib.rus.ec/b/309371/read](http://lib.rus.ec/b/309371/read)

*Flamethrower-incendiary weapons*, Alexey Ardashev

Real Greek fire appears in the early middle Ages. Recipes are contradictory…

To begin with, a few quotes.

> "A mixture of lighted pitch, sulphur, tow, incense, and sawdust of resinous wood is used to burn enemy ships."

*Aeneas Tactician ("On the art of the commander", 350 BC)*

> "Greek fire is" kerosene "(petroleum), sulfur, tar and tar".

*Arabic manuscript (Saladin, 1193)*

> "To get Greek fire, you need to take an equal amount of molten sulfur, tar, one-fourth of opopanax (vegetable juice) and pigeon droppings; all this, well dried, dissolve in turpentine or sulfuric acid, then put in a strong closed glass vessel and heat for fifteen days in the oven. After that, the contents of the vessel should be distilled like wine alcohol and stored in a ready-made form..."

*Viicentius (alchemist XIII e.)*

> "prepare Greek fire in this way: take pure sulfur, earth oil (oil), boil it all, put oakum and set it on fire."

*Mark the Greek (author of a thirteenth-century treatise)*

> "the Composition of Greek fire and gunpowder must be almost identical."

*Ludoviklallai (1847, Paris)*

> "Contrary to evidence, many authors identify Greek fire with gunpowder, and at the same time, without taking into account the specifics of the way it was used, they deceive themselves".

*J. Partington (1961, Cambridge)*

Greek fire is known, or at least heard, by everyone who is at least a little familiar with history. But no conscientious historian or chemist will take the liberty to claim that he knows the composition of this powerful fighting tool of antiquity. History has left us with the most detailed descriptions of battles and sea battles where Greek fire was used. The name of its inventor, methods of use on land and at sea, and even ancient methods of protection against it are known. Everything except its composition and method of preparation (all the above mixtures certainly burn well, but which of them is Greek fire is unknown). Thousands of researchers, from medieval alchemists to the greatest scientists of our time, have tried to penetrate the mystery of Greek fire. Unsuccessfully.

That is, there were weapons, but not everyone could use them?

The terrible weapon of Byzantium

Most historical sources attribute the invention of Greek fire to the mechanic and engineer Kallinikos of Heliopolis, a Syrian scientist and engineer, and a refugee from Maalbek. The historian Theophanes in the Chronograph reports that in 673 AD, during the siege of Constantinople by the Arabs, Kallinikos gave the Byzantine Emperor a recipe for an incendiary compound, later called Greek fire.

When Constantine IV learned of the approach of the Arabs, he prepared huge double-decked ships equipped with Greek fire and siphon-carrying ships… The Arabs were shocked… They fled in great fear."

Despite the use of Greek fire by the Byzantines, in 911 Slavic squads led by Prince Oleg defeated the Byzantine fleet and Oleg nailed his princely shield on the gates of Constantinople (aka Constantinople, aka Istanbul). But this experience was not taken into account by another Russian prince – Igor. In 941, during Igor's campaign to Tsargrad, the Greek Emperor Constantine used Greek fire against the Russian squadron, destroying 30% of the ships. According to the chronicler, Greek fire, "like a bolt of lightning fell on the Russian ships and burned them." But three years later, having provided protection from Greek fire, Prince Igor defeated the Byzantines.

I was attracted to the phrase "like lightning".

They threw it at the enemy from copper pipes (the first prototypes of formidable guns), or in pots using catapults installed on the deck of warships.

So the emitters were found.

Of course, in those barbaric times, people were prone to exaggeration, but the following eyewitness account can be considered reliable. The famous Crusader knight Joinville described the Greek fire as follows:

> "it flew through the air like a winged dragon the size of a barrel, thundering like thunder, with the speed of lightning scattering the darkness of the night with its terrible brilliance."

According to some accounts, Tamerlane (1333-1405) was also armed with Greek fire. Among the troops of Tamerlane were special units of the fire throwers.

After the secret of the Greek fire became the property of many peoples, it lost its significance, and the chronicles that tell about the sea and land battles of the XII century and the first half of the XIV century, barely mention it. The last record of it was made by the historian Francis, describing the siege of Constantinople by Mahomet II in 1453. During the siege, Greek fire was used by both sides; both the Byzantines and the Turks.

Tamerlane? That is, the same antennas on the cockade and the top of the domes?

on the diagram of the bell tower, at the beginning, there is a description of how it worked then. What's more, the last mention of the use of Greek fire falls on the siege of Constantinople in 1453. Where we see collapsible emitters in the panorama.

Siphons, as is commonly believed, were made of bronze, but exactly how they threw the fuel composition is not known for sure (although assumptions, of course, exist). But in any case, the range of the Greek fire was more than moderate - a maximum of 25m.

Once, the mere mention of Greek fire filled people with horror and confusion. It had a particularly disastrous effect on ships during naval battles. Greek fire gave absolute superiority in war at sea, since it was the crowded fleets of wooden ships that made an excellent target for incendiary mixture. Both Greek and Arab sources state with one voice that the combat effect of the Byzantine fire was simply stunning.

That is, the average person will say that it was just simple flamethrowers and that the range was not great, because simply the incendiary mixture burns until it reaches. But what if it was a plasma beam? After all, at high charges, under certain conditions, the formation of this aggregate state of matter is observed. But while the charge is flying, it melts.

With a good charge, water decomposes into oxygen and hydrogen, and this reaction is spontaneously maintained as long as there is an enclosed space and water in it.

A rattlesnake mixture or rattlesnake gas is usually a mixture of hydrogen and oxygen, in which are mixed two volumes of hydrogen per one volume of oxygen. This mixture is very explosive when ignited. The following reaction occurs with the release of a large amount of energy:

2H2 + O2 → 2H2O + 484 kJ (136.74 kcal)

The temperature of this reaction reaches 2,800°C, which is why the explosive gas is used in metallurgy and for melting quartz.

> An explosive mixture can be formed, for example, when water decomposes into hydrogen and oxygen under the influence of high temperatures. This is why it is strictly forbidden to extinguish materials that have a high burning temperature (for example, metals) with water.

*Gorenje*

Note: Although there is a different opinion on this matter, see Vladimir's comment  [http://www.mumbo-jumbo.ru/?p=64](http://www.mumbo-jumbo.ru/?p=64).

> Accumulations of hydrogen are dangerous, and it is important to avoid accumulating hydrogen in electroplating workshops, where hydrogen is released in large volumes when applying protective coatings to metals. A hydrogen explosion threatens to destroy the building.

[http://files.school-collection.edu.ru/dlrstore/36fbfc5d-cb25-31b9-808b-36c1e8086a0a/index.htm](http://files.school-collection.edu.ru/dlrstore/36fbfc5d-cb25-31b9-808b-36c1e8086a0a/index.htm)

> A high or ultra-high voltage is applied to the electrode. Then water is applied to the electrode. Water in the form of drops falls down. Under the influence of an electromagnetic field, water droplets are attracted to the electrode. Getting into the area of high electric field strength, a micro-arc appears in the water droplets. Under the influence of a micro-arc, water decomposes into hydrogen and oxygen. During decomposition, gases (hydrogen and oxygen) are ionized. Hydrogen with a negative charge, oxygen with a positive charge. The gas mixture rises in the transformer oil. Their separation is carried out in a known way, as occurs in the electrolysis method. Hydrogen is released at the connected anode and oxygen is released at the cathode.

[http://fund.patent.kg/catalog/1/94/488.html](http://fund.patent.kg/catalog/1/94/488.html)

> Using a quasi-equilibrium plasma, such important scientific and practical tasks as obtaining acetylene from methane, coal conversion, synthesis of refractory compounds, and others are successfully solved. However, the uniformity of the energy contribution to all degrees of freedom inherent in a quasi-equilibrium plasma does not allow obtaining selective chemical reactions with high energy efficiency on this basis.

[http://relec.ru/highfrequency-energy/560-ispolzovanie-svch-plazmy-v-vodorodnoy-energetike.html](http://relec.ru/highfrequency-energy/560-ispolzovanie-svch-plazmy-v-vodorodnoy-energetike.html)

Water can be decomposed into hydrogen and oxygen by electrolysis of water with a large expenditure of energy. If you do not take measures to separate these gases, you can get a rattling mixture.

That is, the rattlesnake dissolved on the way dissolving in the air. More precisely, the charge was melting from the moisture in the air.

Q: What was the use of remembering sulfur and other chemicals at that time? Really a cocktail of ground? [http://habrahabr.ru/post/187084/](http://habrahabr.ru/post/187084/)

A: Sulfur could serve as a fuse or as salt for electrolysis, which was also discovered when studying information about Greek fire.

### "Answer that the fire was opened by an Angel…"

The Byzantine emperors immediately appreciated the strategic importance of the new weapon. The composition of Greek fire was considered a state secret of extreme importance, and for about four centuries Muslims tried in vain to find out. Leo the Philosopher ordered Greek fire to be prepared only in secret laboratories, and Constantine VII the Porphyrionous declared the recipe for its production a state secret. To preserve it, he used the entire Arsenal of means of intimidation and secrecy at his disposal. For the edification of his son, the future heir to the throne, he wrote in "Discourses on public administration":

> "You must take the greatest care of the Greek fire... and if anyone dares to ask you for it, as we have often asked ourselves, reject these requests and answer that the fire was opened by an Angel to Constantine, the first Emperor of the Christians. The great Emperor, as a warning to his heirs, ordered a curse to be carved in the temple on the throne against anyone who dared to pass this discovery on to strangers…"

This warning could not fail to play a part in preserving the secret of Greek fire for many centuries…

So we don't know the truth about the Greek fire, that it was taken care of in vain? What lies strong and loud in the very sight of our noses from those systems? Do the images of the lighthouse of Alexandria remind you of the bell tower of Ivan the Great?

<!-- Local
[![](По мотивам «И была Страна до России». Часть 7._files/e-11.jpg#clickable)](По мотивам «И была Страна до России». Часть 7._files/e-11.jpg)
-->

[![](http://alexandria-history.limarevvn.ru/e-11.jpg#clickable)](http://alexandria-history.limarevvn.ru/e-11.jpg)

Ie with a kind of pronounced light drum. And there are more than enough light drums of this type. And not only on this reconstruction of the lighthouse.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 7._files/d9d4fe09be79.jpg#clickable)](По мотивам «И была Страна до России». Часть 7._files/d9d4fe09be79.jpg)
-->

[![](https://i052.radikal.ru/1109/57/d9d4fe09be79.jpg#clickable)](https://i052.radikal.ru/1109/57/d9d4fe09be79.jpg)

Some legends say that the mirror on the Pharos lighthouse could also be used as a weapon. Allegedly, it was able to focus the sun's rays so that it burned enemy ships as soon as they appeared in sight. Other legends say that it was possible to see Constantinople on the other side of the sea, using this mirror as a magnifying glass. Both stories seem too far-fetched.

From here: [http://www.liveinternet.ru/users/3596969/post143778266/](http://www.liveinternet.ru/users/3596969/post143778266/)

How did the world's first lighthouse end up at the bottom of the Mediterranean sea? Most sources say that the lighthouse, like other ancient structures, fell victim to earthquakes. The Pharos lighthouse stood for 1,500 years, but aftershocks in 365, 956, and 1303 AD severely damaged it. And the earthquake of 1326 (according to other sources, it was in 1323) completed the destruction.

The story of how a large part of the lighthouse was turned into ruins in 850 by the intrigues of the Emperor of Constantinople seems completely unreliable.

The Emperor fought to keep the secret of using Greek fire?

What else. Defense of Syracuse using machines built by Archimedes. Is it a link in the same chain?

<!-- Local
![](По мотивам «И была Страна до России». Часть 7._files/EngravingMirrorBig1.jpg#resized)
-->

[![](http://tainy.net/wp-content/uploads/2012/03/EngravingMirrorBig1.jpg#clickable)](http://tainy.net/wp-content/uploads/2012/03/EngravingMirrorBig1.jpg)

Heat and beam weapons. We know of at least one famous precedent for the use of peculiar "death rays". We are talking about the mirrors (or mirror) of Archimedes.

The bonzes of ancient historiography - Polybius, Livy, and Plutarch - in their description of the Roman siege of Syracuse allied to the Carthaginians (211 BC) do not report the use of heat weapons. But the Greek writer Lucian (II century AD) provides interesting information, which was later happily seized by scientists, philosophers, and artists of the Renaissance.

Archimedes built a hexagonal mirror made up of small quadrangular mirrors. Each of these mirrors was fixed on hinges and set in motion by a chain drive. Due to this, the angles of rotation of the mirrors could be selected so that the reflected sun rays were focused at a point located at the distance of an arrow flight from the mirror. With this system of mirrors, Archimedes set fire to the Roman ships. This story delighted the titans of the Renaissance and continues to stir the minds of modern historians of material culture. And the artist Giulio Parigi (1566-1633) drew a charming, fantastic picture that you can see at the top of the page.

What personally confuses me about this story?

First, some general physical considerations, which I will not give, so as not to bore the reader with boring details.

Second, the conspiratorial silence of the classic Punic war historian, namely Polybius. Mirrors are only mentioned by the late Lucian (II century AD), and he was a famous storyteller.

Third, the lack of replicas. If Archimedes really succeeded in such a technical adventure, why didn't the hand-wielding Romans capture Syracuse in defiance of all the engineering wonders of the defenders and copy the battle mirrors? They also borrowed the quinqueremes from the Carthaginians, and the Scorpions from the Greeks.

But anything is possible in this best of all possible worlds. At the very least, magic is possible. From here: [http://xlegio.ru/throwing-machines/antiquity/greek-fire-archimedes-mirrors/](http://xlegio.ru/throwing-machines/antiquity/greek-fire-archimedes-mirrors/)

One can only agree with the author's doubts that mirrors in their pure form are of little use as weapons. And as a reflector of microwave radiation is very suitable. However, mirrors with different reflectivity were probably used for different tasks. Because they adjusted to the wavelength of the emitter. And you can also show a hypothesis for why the Romans did not copy the installation of Archimedes ' mirrors. Not because they did not have enough knowledge to repeat it in vain. But because they simply did not understand the principle of operation of the installation. Although they most likely tried.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 7._files/004qbcs5.jpeg#clickable)](По мотивам «И была Страна до России». Часть 7._files/004qbcs5.jpeg)
-->

[![](https://pics.livejournal.com/aldanov/pic/004qbcs5)](http://pics.livejournal.com/aldanov/pic/004qbcs5)

They also used bronze but silver-plated mirrors.

Looking through all the available information that is fanned out, here and there. I decided to take a closer look at what happened to the lighthouse of Alexandria. After all, it was broken this way and that. The Emperor of Tsar-grad put his hand to it. And the earthquakes worked on it. In the end, it was used to build a fort, which was also rebuilt many times.

Photo from Wikipedia.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 7._files/Qaitbay_0005.jpg#clickable)](По мотивам «И была Страна до России». Часть 7._files/Qaitbay_0005.jpg)
-->

[![](https://upload.wikimedia.org/wikipedia/commons/f/fe/Qaitbay_0005.JPG#clickable)](http://upload.wikimedia.org/wikipedia/commons/f/fe/Qaitbay_0005.JPG)

Here is the fort that stands on the bones of the lighthouse. What's on the right side of the picture? The boiler? Some kind of bell? And the pipe stub to the right? Isn't it a piece of the radiator? Unfortunately, I didn't find any other photos of these objects. On the current ones, first the "trunk" disappears, and then the "bell-cauldron". They didn't seem to fit into the exhibition. Or maybe they just moved it somewhere. I didn't find where.

And actually, why the Teutonic knights? Yes, just a hiss went through the ice of Lake Ladoga, with a sort of typical hiss that molten metal makes when in contact with ice and water.

Vladimir Mamzerim. 26.10.2013

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-8.html)

© All rights reserved. The original author has asserted their ownership and rights.

Comments

pavell743
2013-10-27 10: 55 am (UTC)
Step voltage at the beginning of the Millennium)))))))
A comic drawing with villains is interesting.

pro_vladimir
2013-10-28 06:36 am (UTC)
Yes, the author that sketched the scheme, not without a sense of humor.)) very "serious" this manner of image circuitry will scare away. and who will laugh, and find how it really was there))

About "guns" with two screwed-together halves.
and_re_j
2013-10-27 12:22 pm (UTC)
Your guess about the design of the vertical "cannon" plus the bell on top is absolutely correct. Obviously, this construction is called the "Jericho trumpet" in the Bible. Its vibrations caused the fortress walls to collapse. The vibration frequency was adjusted by smoothly screwing the upper half of the pipe into the lower half.

Re: About "guns" of two screwed halves.
pro_vladimir
2013-10-28 06:47 am (UTC)
Not exactly. This is not my guess at this arrangement of installations of those times. I just tried to look for evidence-refutations in the text that is here http://dmitrijan.livejournal.com/71535.html
and all " my "research" is based on this text. Of course, it's a little flattering that this is "my" guess, but it's not.

And regarding the Jericho pipes, there is an opinion that everything was somewhat simpler there. For six days the people marched in circles distracting the defenders of the city. While simple tunnels were dug under the walls and laid with charges for detonation. Although everyone remembers only "pipes". The external factor overlapped the background. You can put it in the piggy bank of versions before "finding out"))

bloodmeri
2020-08-03 05: 07 pm (UTC)
Jericho did not fall from the sound of a trumpet. How can you read like that? Jericho fell to the screams of hundreds of thousands of people around it, who had been silent for three days. The sound of the trumpet was only a signal, the weapon was their voice.
