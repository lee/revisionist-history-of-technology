title: Another history of the Earth Part 1g
date: 2017-04-01
modified: 2022-12-14 16:05:08
category:
tags: catastrophe
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/225365.html
originally: Another history of the Earth. Part 1g. - Dmitry Mylnikov - LiveJournal.html
local: Another history of the Earth Part 1g_files/
summary: When, at the moment of impact and breakdown of the Earth's crust, the outer hard shell of the Earth slips and slows down relative to the molten core, the water of the world ocean continues to move as it moved before the catastrophe, forming a so-called "inertial wave",
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/225365.html](https://mylnikovdm.livejournal.com/225365.html)

[Start](http://mylnikovdm.livejournal.com/219459.html)

For a long time I have been receiving letters and comments. In these they send me an image of the formations between North and South America, as well as between South America and Antarctica, which I talked about in the previous part. But at the same time, a variety of explanations are given for the reasons for their formation. Of these, two are the most popular. The first is that these are traces of the impact of large meteorites, some even argue that these are the consequences of the fall of two earlier Earth satellites, called Fata and Lelya. Allegedly, this is reported by the "ancient Slavic Vedas". The second version is that these are very ancient tectonic formations that formed a very long time ago, when the solid crust was formed as a whole. And so that no one doubts this version,

<!-- Local
![1e - Lithospheric plates.jpg](./Another%20history%20of%20the%20Earth.%20Part%201g.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/167166_900.jpg "1e - Lithospheric plates.jpg")
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167166/167166_900.jpg#resizable)

On this schematic map, these small slabs are labelled the Caribbean Plate and the Scotia Plate. To understand that neither the first version nor the second is consistent, let's once again take a closer look at the formation between South America and Antarctica, but not on a map, where the shapes of objects are distorted due to projection onto a plane, but in the Google Earth program.

<!-- Local
[![](Another history of the Earth Part 1g_files/170520_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170520/170520_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/170520/170520_original.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170520/170520_original.jpg)

It turns out that if we remove the distortions introduced during projection, then it is very clearly seen that this formation is not direct, but has the shape of an arc. Moreover, this arc is very well consistent with the daily rotation of the Earth.

Now answer the question yourself: can a meteorite, when falling, leave a trail in the form of a similar arc? The flight path of a meteorite in relation to the Earth's surface will always be almost a straight line. The daily rotation of the Earth around its axis does not affect its trajectory in any way. Moreover, even if a large meteorite falls into the ocean, then the shock wave that will diverge from the place of impact of the meteorite will also go from the place of impact in a straight line, ignoring the daily rotation of the Earth.

Or maybe the formation between the Americas is a trace of the meteorite falling? Let's take a closer look at it, too, through Google Earth.

<!-- Local
[![](Another history of the Earth Part 1g_files/170929_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170929/170929_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/170929/170929_original.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170929/170929_original.jpg)

Here, too, the trail is not completely straight, as it should be in the case of a meteorite. Moreover, the existing bend is consistent with the shape of the continents and the general relief. In other words, if an inertial wave made a gap for itself between the continents, then it should have moved in exactly this way.

In addition, the likelihood that a meteorite could accidentally fall exactly in such a way as to get exactly between the continents, in the very direction where the inertial wave will move, and even leave a trail almost the same size as the formation between South America and Antarctica, practically zero.

Thus, the version with a track from a meteorite fall can be discarded as contradicting the observed facts or requiring the coincidence of too many random factors to fit the observed facts.

I personally believe that such an arc formation, as we observe between South America and Antarctica, could have formed only as a result of an inertial wave (if someone thinks differently and can substantiate their version, I will gladly discuss this topic with him). When, at the moment of impact and breakdown of the Earth's crust, the outer hard shell of the Earth slips and slows down relative to the molten core, the water of the world ocean continues to move as it moved before the catastrophe, forming a so-called "inertial wave", which is actually more correctly called inertial flow. Reading the comments and letters of readers, I see that many do not understand the fundamental difference between these phenomena and their consequences, so we will dwell on them in more detail.

In the case of a large object falling into the ocean, even as large as during the described catastrophe, a shock wave is formed, which is precisely a wave, since the bulk of the water in the ocean does not move. Due to the fact that the water practically does not compress, the fallen body will displace the water at the place of fall, but not to the sides, but mainly upward, since it will be much easier to squeeze out excess water there than to move the entire water column of the world's oceans. And then this squeezed out excess water will begin to flow over the upper layer, forming a wave. At the same time, this wave will gradually decrease in height, as it moves away from the impact site, since its diameter will grow, which means that the squeezed out water will be distributed over an ever larger area. That is, with a shock wave, our water movement occurs mainly in the surface layer,

When we have a displacement of the earth's crust relative to the inner core and the outer hydrosphere, another process takes place. The entire volume of water in the world's oceans will tend to continue to move relative to the decelerated solid surface of the Earth. That is, it will be precisely the inertial flow throughout the entire thickness, and not the movement of the wave in the surface layer. Therefore, the energy in such a flow will be much more than in the shock wave, and the consequences of meeting obstacles in its path are much stronger.

But most importantly, the shock wave from the impact site will propagate in straight lines along the radii of the circles from the impact site. Therefore, she will not be able to leave the gully in an arc. And in the case of an inertial flow, the water of the world ocean will continue to move in the same way as it moved before the catastrophe, that is, it will rotate about the old axis of rotation of the Earth. Therefore, the traces that it will form near the pole of rotation will have the shape of an arc.

By the way, this fact allows us, after analyzing the tracks, to determine the location of the rotation pole before the disaster. To do this, you need to build tangents to the arc that the trace forms, and then draw perpendiculars to them at the points of tangency. As a result, we will get the diagram that you see below.

<!-- Local
![](Another history of the Earth Part 1g_files/171153_900.png#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/171153/171153_900.png#resizable)

What can we say based on the facts that we got by building this scheme?

First, at the moment of impact, the Earth's rotation pole was in a slightly different place. That is, the displacement of the earth's crust did not occur strictly along the equator against the rotation of the Earth, but at a certain angle, which was to be expected, since it was directed at a certain angle to the equator line.

Secondly, we can say that after this catastrophe there were no other displacements of the rotation pole, especially 180-degree flips. Otherwise, the resulting inertial flow of the world ocean should not only wash away these traces, but also form new ones, comparable or even more significant than these. But we do not observe such large-scale traces either on the continents or at the bottom of the oceans.

By the size of the formation between the Americas, which is located almost near the equator and is about 2,600 km, we can determine the angle at which the Earth's solid crust turned at the time of the catastrophe. The length of the Earth's diameter is 40,000 km, respectively, a fragment of the arc of 2,600 km is 1 / 15.385 of the diameter. Dividing 360 degrees by 15.385 gives an angle of 23.4 degrees. Why is this value interesting? And the fact that the angle of inclination of the axis of rotation of the Earth to the plane of the ecliptic is 23.44 degrees. To be honest, when I decided to calculate this value, I did not even imagine that there could be any connection between it and the angle of inclination of the Earth's axis of rotation. But I fully admit that there is a connection between the described catastrophe and the fact that the angle of inclination of the Earth's axis of rotation to the plane of the ecliptic has changed precisely by this value, and we will return to this topic a little later. Now we need this value of 23.4 degrees for something completely different.

If, with a displacement of the earth's crust by only 23.4 degrees, we observe such large-scale and well-readable consequences on satellite images, then what should be the consequences if the solid shell of the Earth, as supporters of the theory of revolution due to the Dzhanibekov effect, allegedly turns over almost by 180 degrees ?! Therefore, I believe that all talk about coups due to the "Dzhanibekov effect", of which there are a great many on the Internet today, can be closed at this point. At the beginning, show traces that should be much stronger than those left from the described disaster, and then we'll talk.

As for the second version, that these formations are lithospheric plates, there are also many questions. As far as I understand, the boundaries of these plates are determined by the so-called "faults" in the earth's crust, which are determined by the same methods of seismic exploration, and which I have already described earlier. In other words, in this place, the devices record some anomaly in the reflection of signals. But if we had an inertial flow, then in these places it had to wash a kind of trench in the original soil, and then washed away sedimentary rocks brought by the flow from other places had to settle into this trench. At the same time, these settled rocks will differ both in composition and in their structure.

Also, in the above map-scheme of lithospheric plates, the so-called "Scotia plate" is depicted practically without bending, although we have already found out that this is a distortion of projection and in reality this formation is curved in an arc around the previous pole of rotation. How did it happen that the faults in the earth's crust, which form the Scotia Plate, pass along an arc that coincides with the trajectory of rotation of points on the Earth's surface in a given place? It turns out that here the plates split taking into account the daily rotation of the Earth? Then why don't we see such a match anywhere else?

[*Continuation*](http://mylnikovdm.livejournal.com/225623.html)


© All rights reserved. The original author retains ownership and rights.

