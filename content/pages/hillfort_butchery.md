title: Hillfort Butchery
date: 2021-01-05 11:44
modified: 2022-01-01 12:26:55
category: 
tags: 
slug: 
authors: RoHT
summary: Container for SH hillfort butchery images. To help display them large in initial post
status: draft

![](../../images/hillfort_butchery_files/Butchery sites map.png#resized)
![](../../images/hillfort_butchery_files/Hambleden baby remaining skeleton.png#resized)
![](../../images/hillfort_butchery_files/Hambledon 38 cut marks-002.jpg#resized)
![](../../images/hillfort_butchery_files/clue_Hambleden baby remaining skeleton.png#resized)
![](../../images/hillfort_butchery_files/Canid gnawed parietal -034.jpg#resized)
![](../../images/hillfort_butchery_files/Gnawed human bones -035.jpg#resized)
![](../../images/hillfort_butchery_files/ten year old in pit -012.jpg#resized)
![](../../images/hillfort_butchery_files/pit 4332 intact foot bones -013.jpg#resized)
![](../../images/hillfort_butchery_files/Dual inhumation pit 4223.png#resized)
![](../../images/hillfort_butchery_files/halved baby parts missing -024.jpg#resized)
![](../../images/hillfort_butchery_files/spearhead or rotisserie head -022.jpg#resized)
![](../../images/hillfort_butchery_files/Paco-Rabanne-Unwearable-dress.png#resized)

