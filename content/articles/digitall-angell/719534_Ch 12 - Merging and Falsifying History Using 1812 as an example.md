title: Chapter 12. Merging Branches and Falsifying History Using 1812 as an Example
date: 2016-10-05
modified: Thu 11 Mar 2021 15:47:09 GMT
category:
tags: catastrophe; forged history
slug:
authors: digitall-angell
from: https://digitall-angell.livejournal.com/719534.html
originally: 719534.html
local: 719534_Ch 12 - Using 1812 as an example_files
summary: There was a dramatic drop in the population. It's like everyone got up and went but no one went anywhere. Just disappeared. Everybody got off the ground and just disappeared. Cataclysm. Planned floods. We had to act tough. Wars were initiated by local structures, not from above. And floods were initiated for the purpose of civilizational replacement. Global sweeps of failed experiments.
status: published

#### Translated from:

[https://digitall-angell.livejournal.com/719534.html](https://digitall-angell.livejournal.com/719534.html)

<!-- Local
![](719534_files/2391550_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2391550/2391550_original.jpg#resized)

**Russian:**

Как известно, историю пишут победители. На тему несоответствий официальной версии тут уже говорилось неоднократно. Рассмотрим то, что удалось выяснить через сеансы.

**English:**

As you know, history is written by the victors. The subject of inconsistencies in the official version has already been mentioned here more than once. Let's consider what we were able to find out through the sessions.

**Russian:**

> В: Правильно ли говорить, что последнее слияние веток реальности произошло лет 200-300 назад?

> О: Да, что-то было сшито, а что-то было выкинуто при сшивании веток. Вижу Сибирь и Дальний Восток. Этот кусок как будто стерли, опустошили весь.

**English:**

> Q: Is it correct to say that the last merging of the branches of reality happened about 200-300 years ago?

> A: Yes, something was stitched and something was thrown away when the branches were stitched together. I see Siberia and the Far East. It's as if this piece has been erased, emptied all over.

**Russian:**

> В: Была война? Как это стерли?

> О: Резкое сокращение численности населения. Как будто все поднялись и пошли, и никто никуда не пришел. Просто исчезли. Все снялись с мест и просто исчезли. Катаклизм. Плановые потопы. «Приходилось действовать жестко». Войны инициировали местные структуры, не сверху. А потопы инициировались с целью цивилизационной замены. Глобальные зачистки при неудачных экспериментах.

**English:**

> Q: There was a war? How was it erased?

> A: There was a dramatic drop in the population. It's like everyone got up and went but no one went anywhere. Just disappeared. Everybody got   off the ground and just disappeared. Cataclysm. Planned floods. We had to act tough. Wars were initiated by local structures, not from above. And    floods were initiated for the purpose of civilizational replacement. Global sweeps of failed experiments.

**Russian:**

> В: Что можно сказать про 1812 год, что тут произошло?

*В Сибири, как и по всему миру, хватает странных артефактов, уж слишком похожих на останки древней культуры. Например, Горная Шория:*

**English:**

> Q: What can you say about 1812, what happened here?

*In Siberia, as all over the world, there are enough strange artifacts that look too much like the remains of an ancient culture. For example, the Shoria Mountain:*

<!-- Local
![](719534_files/0_9ccc9_f60b0a7c_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9167/1118136.54/0_9ccc9_f60b0a7c_XL.jpg#resized)

<!-- Local
![](719534_files/0_9cccf_b2210841_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9167/1118136.54/0_9cccf_b2210841_XL.jpg#resized)


<!-- Local
![](719534_files/0_9ccbd_a0f5c379_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9092/1118136.54/0_9ccbd_a0f5c379_XL.jpg#resized)

**Russian:**

По теме: [Плато Путорана - гигантский карьер?](http://digitall-angell.livejournal.com/553820.html) /  [Война и слияние реальностей](http://digitall-angell.livejournal.com/411406.html) / [Природные мегалиты или останки древних цивилизаций?](http://digitall-angell.livejournal.com/640043.html) /  [Мегалиты Горной Шории и не только](http://digitall-angell.livejournal.com/302338.html) /  [Следы древних войн и цивилизаций](http://digitall-angell.livejournal.com/230709.html) / [Таймыр. Апокалиптические пейзажи](http://digitall-angell.livejournal.com/593325.html) / [Плато Путорана - гигантский карьер?](http://digitall-angell.livejournal.com/553820.html) /

**English:**

More on this topic: [Putorana Plateau - a giant quarry?](http://digitall-angell.livejournal.com/553820.html) / [War and fusion of realities](http://digitall-angell.livejournal.com/411406.html) / [Natural megaliths or remains of ancient civilizations?](http://digitall-angell.livejournal.com/640043.html) / [Megaliths of Mountainous Shoria and not only](http://digitall-angell.livejournal.com/302338.html) / [Traces of ancient wars and civilizations](http://digitall-angell.livejournal.com/230709.html) / [Taimyr. Apocalyptic landscapes](http://digitall-angell.livejournal.com/593325.html)

**Russian:**

> О: «Мифологизация пространства» идет. Как театр и разыгранная история. Исторические фигуры были, перемещения войск были. Но ощущение спектакля. Вся история искажена. Москву несколько раз пытались загасить. По идее Москву должны были еще во вторую мировую войну уничтожить, но почему-то этого не получилось.

**English:**

> A: The "mythologization of space" is underway. Like theater and played out history. There were historical figures, there were troop movements. But it feels like a play. The whole history is distorted. Moscow has been tried to be put out of action a few times. In theory, Moscow should have been destroyed back in World War II, but for some reason it didn't work out.

**Russian:**

> В: Как создавалась мифологизация?

О:  Многое вручную - артефакты, летописи, картины. А проекты типа Петры - как на матричном принтере. Уже на нашей жизни мы увидим примеры такого создания реальности

**English:**

> Q: How was mythologizing created?

> A: A lot of things are handmade - artifacts, annals, paintings. And Petra-type projects are like on a matrix printer. Already in our lives we will see examples of this kind of reality creation

**Russian:**

> В: Миры драконов, эльфов когда отделились?

> О: Дальше, чем в момент слияния веток реальности. Раньше они спокойно мигрировали туда-сюда и могли контактировать с нами. А потом наложили печать, препоны и изолировали их от нашей ветки

**English:**

> Q: Dragon worlds, elven worlds, when did they separate?

> A: Further than when the branches of reality merged. They used to migrate back and forth and be able to contact us. And then they were sealed, obstructed, and isolated from our branch

**Russian:**

Петра:

**English:**

Petra:

<!-- Local
![](719534_files/2391805_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2391805/2391805_original.jpg#resized)

**Russian:**

[Петра, Иордания](http://1chudo.ru/goroda/48-drevnij-gorod-petra-v-iordanii.html)

**English:**

[Petra, Jordan](http://1chudo.ru/goroda/48-drevnij-gorod-petra-v-iordanii.html)

<!-- Local
![](719534_files/2539939_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2539939/2539939_original.jpg#resized)

<!-- Local
![](719534_files/2540190_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/2540190/2540190_original.jpg#resized)

**Russian:**

Детали тут: [Оплавленные мегалиты и городища # 2. Как течет камень](http://digitall-angell.livejournal.com/721838.html)

**English:**

Details here: [Melted Megaliths and Settlements # 2. How the Stone Flows](http://digitall-angell.livejournal.com/721838.html)

**Russian:**

[По теме:] [Закат Тартарии](http://digitall-angell.livejournal.com/210385.html)

**English:**

More on this topic: [Sunset Tartaria](http://digitall-angell.livejournal.com/210385.html)

**Russian:**

Из другого сеанса:

**English:**

From another session:

**Russian:**

> В: Что конкретно здесь произошло в районе 1812 года? Правильно ли понимать, что была война, и если да, то с кем?

О:  Интересная информация идет. Показывается,  что это попытка совмещения двух веток и двух миров.  Показывают гроздь винограда, один мир рушится,  одну виноградинку сливают и, в попытке сохранить ее мир, берут ее и другой мир, и объединяют в единое целое для того, чтобы сохранить остатки того мира, который сливают.

**English:**

> Q: What exactly happened here around 1812? Is it correct to understand that there was a war, and if so, with whom?

> A: Interesting information is coming. It is shown that it is an attempt to combine two branches and two worlds.  A bunch of grapes is shown, one world collapses, one grape is merged and, in an attempt to preserve its world, they take it and the other world and combine it into a whole in order to preserve the remnants of the world that is merged.

**Russian:**

> В: Почему  это делается?

> О: Потому что каждый мир ценен.  До последнего идут попытки сохранения всего того, что там есть. Чтобы не было нарушения равновесия. Потому что если погибнет один мир, может пойти цепная реакция, т.е. будет нарушено равновесие. И за счет слияния миров  поддерживается равновесие, потому что когда две виноградинки сливаются вместе, из двух маленьких они становятся одной большой.  Но это несет за собой некие последствия, которые влияют и на  жителей и на атмосферу обоих миров. Но здесь преследуются более глобальные цели, учитывается соотношение вреда и пользы.

**English:**

> Q: Why is this being done?

> A: Because every world is valuable.  To the last, there is an attempt to preserve everything that is there. So there's no imbalance. Because if one world dies, there might be a chain reaction, i.e. the balance will be broken. And due to the merging of the worlds the balance is kept, because when two little grapes merge together, from two small ones they become one big one.  But it has some consequences, which affect the inhabitants and the atmosphere of both worlds. But here we are pursuing more global goals, the balance of harm and benefit is taken into account.

**Russian:**

> В: То, что мы знаем как войну 1812 года, это произошло до или после слияния веток реальностей?

> О: Это процесс. При слиянии это процесс. Мир, который рушился, это был техногенный  саморазрушающийся  мир. Соответственно, при слиянии двух веток произошло слияние двух миров.  И те остатки цивилизации, которые там были, пришли и получили доступ сюда, к этому миру.  И получившаяся виноградинка сейчас намного крупнее, чем все остальные виноградинки. Реально она очень большая. Но здесь происходят некие процессы. Техногенная цивилизация, которая сама себя довела до разрушения, начала паразитическую деятельность здесь, т.е. фактически захватила тот мир, который был. А тот мир, который был соединен, он на момент соединения сам по себе был, скажем, светлый-пушистый. Он не знал лжи, они были очень чистые.  Люди, которые здесь жили, были очень доверчивые, не знали лжи, они доверяли. Соответственно, при слиянии, паразиты очень быстро смогли войти в доверие и начать захватывать все, что здесь было.

**English:**

> Q: What we know as the War of 1812, did that happen before or after the merging of the branches of reality?

> A: It's a process. With the merger, it's a process. The world that was collapsing was a technogenic self-destructive world. Accordingly, at the merger of the two branches, there was a merger of the two worlds.  And those remnants of the civilization that were there, came and got access here, to this world.  And the resulting grape now is much larger than all the other grapes. Really, it is very big. But there are some processes going on here. The technogenic civilization, which brought itself to destruction, has started a parasitic activity here, i.e. it has actually taken over the world that was. And the world that was connected, it was, let's say, light and fluffy at the time of the connection. It knew no lies, they were very pure.  The people who lived here were very trusting, they knew no lies, they trusted. Consequently, with the merger, the parasites were very quickly able to enter into trust and begin to take over everything that was here.

**Russian:**

> В: Что это за техногенная цивилизация? Мы с ней сталкивались раньше?

О:  Техногенная цивилизация - это не те серые, которых мы видим. Изначально на планете было несколько правящих цивилизаций.

**English:**

> Q: What is this man-made civilization? Have we encountered it before?

> A: The technogenic civilization is not the greys we see. There were originally several ruling civilizations on the planet.

**Russian:**

Официальная историческая наука до сих пор так и не выяснила, кто поджёг Москву.

**English:**

The official historical 'science' still has not found out who set Moscow on fire.

**Russian:**

Французы считали, что это сделали сами москвичи. И даже расстреляли четыреста «поджигателей» (рис. 43). Дабы другим неповадно было.

**English:**

The French believed that the Muscovites themselves had done it. And even shot four hundred "arsonists" (Fig. 43). In order that others would not be punished.

<!-- Local
![](719534_files/0_92cdc_d5216ead_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9327/1118136.48/0_92cdc_d5216ead_L.jpg#resized)

**Russian:**

*Рис. 43. Расстрел московских «поджигателей»(худ.В.Верищагин)*

**English:**

*Fig. 43. Shooting of Moscow "arsonists" (artist V.Verishchagin).*

**Russian:**

Русские полагали, что во всём виновато корсиканское чудовище. Мстительное и злобное. Из природной кровожадности погубившее огромный город и десятки тысяч людей, включая тридцать тысяч собственных солдат и офицеров.

**English:**

The Russians believed it was all the fault of the Corsican monster (Napoleon). Vengeful and vicious. Out of natural bloodthirstiness it had destroyed a huge city and tens of thousands of people, including thirty thousand of its own soldiers and officers.

**Russian:**

Но так ли это? Французам поджигать Москву было незачем. Впереди - зима. А от Москвы до Парижа - шестьсот шестьдесят шесть лье. То бишь, очень далеко! Помимо прочего, Москва была нужна Наполеону в качестве разменной монеты на предстоящих переговорах о мире.

**English:**

But was it so? There was no need for the French to set Moscow on fire. Winter was ahead. And from Moscow to Paris it's six hundred and sixty-six leagues. That is very far! Among other things, Napoleon needed Moscow as a bargaining chip in the upcoming peace talks.

**Russian:**

Москвичам самосжигаться тоже было ни к чему. Впереди - зима. И надо как-то выживать. Не взирая на оккупацию. Кроме того, в Москве было оставлено тридцать тысяч раненых. Которые практически все сгинули в огне пожара. Вместе с двадцатью тысячами горожан, не успевших покинуть обречённый город.

**English:**

Muscovites did not need to burn themselves out either. Winter was coming. And we had to survive somehow. Notwithstanding the occupation. In addition, thirty thousand wounded were left in Moscow. ...who practically all perished in the fire... Along with 20,000 citizens who did not have time to leave the doomed city.

**Russian:**

Бригадный генерал граф Филипп де Сегюр в своих воспоминаниях писал:

> «Два офицера расположились в одном из кремлёвских зданий, откуда им открывался вид на северную и восточную части города. Около полуночи их разбудил необычайный свет, и они увидали, что пламя охватило дворцы: сначала оно осветило изящные и благородные очертания их архитектуры, а потом всё это обрушилось... Сведения приносимые съезжавшимися со всех сторон офицерами, совпадали между собой. В первую же ночь, 14-го на 15-е огненный шар спустился над дворцом князя Трубецкого и поджёг это строение».

Очень странный пожар. Мягко говоря.

**English:**

Brigadier General Count Philippe de Ségur wrote in his memoirs:

> Two officers were stationed in one of the Kremlin buildings, from where they had a view of the northern and eastern parts of the city. Around midnight they were awakened by an extraordinary light, and they saw that the flames engulfed the palaces: first it illuminated the elegant and noble outlines of their architecture, and then it all came crashing down ... The reports brought in by the officers who came from all sides were consistent with one another. During the first night, on the 14th-15th, a fireball came down over the palace of Prince Trubetskoy and set fire to this building.

A very strange fire. To put it mildly.

**Russian:**

Московский житель рассказывает:

> «Казармы были завалены больными солдатами, лишёнными всякого присмотра, а госпитали ранеными, умиравшими сотнями от недостатка в лекарствах и даже в пище... улицы и площади были завалены мёртвыми окровавленными телами человеческими и лошадьми... Стенали борящиеся со смертью раненые, коих иные проходящие мимо солдаты, из сострадания прикалывали с таким точно хладнокровием, с каким мы в летнее время умерщвляем муху... Целый город превращён был в кладбище».

**English:**

A Moscow resident relates:

> The barracks were littered with sick soldiers, deprived of all care, and the hospitals with the wounded, dying by the hundreds from lack of medicine and even food... the streets and squares were littered with dead bloody human and horse bodies... Wounded men were moaning, fighting with death and some passing by soldiers were brutally beating them up in the same cold blood as we kill flies in summer time... The whole town was turned into a cemetery.

**Russian:**

Всего погибло более восьмидесяти тысяч человек (для справки: во время атомного взрыва в Хиросиме погибло семьдесят тысяч человек, в Нагасаки - шестьдесят). Из девяти тысяч ста пятидесяти восьми строений было уничтожено шесть тысяч пятьсот тридцать два.

**English:**

All in all more than eighty thousand people died (for reference: during the atomic explosion in Hiroshima seventy thousand people died, in Nagasaki sixty thousand). Of Moscow's nine thousand one hundred and fifty-eight buildings, six thousand five hundred and thirty-two were destroyed.

**Russian:**

Читать весь пост: [Гибель Тартарии](http://digitall-angell.livejournal.com/197364.html)

**English:**

Read the whole post: [The demise of Tartaria](http://digitall-angell.livejournal.com/197364.html)

**Russian:**

Из раннего по теме:

**English:**

From earlier on this topic:

**Russian:**

> В: Вы сказали, что цивилизации устраняются обычно в тех случаях, когда они начинают быть способными разрушить Матрицу. Тартария была разрушена. Каким образом она могла разрушить Матрицу? Какое оружие у нее было?

> О: Нет, Тартария, это другое. Тартария, это то, что происходило локально. Это была игра в войну. Это была проба сил молодых, но уже достаточно могучих цивилизаций. И, способности были достаточно высокими.

**English:**

> Q: You said that civilizations are usually eliminated when they begin to be able to destroy the Matrix. Tartary was destroyed. How was she able to destroy the Matrix? What weapons did it have?

> A: No, Tartary is different. Tartary, that's what was happening locally. It was a game of war. It was a tryout for the powers of young but already powerful enough civilizations. And, the abilities were quite high.

**Russian:**

> В: "Тогда", это когда? 200 лет назад?

> О: 300, и немного дальше. Люди, которые жили в то время на планете, все еще имели возможность общаться с создателями, но они были, уже вполне себе люди, как и те, кто живет сейчас. И те, с кем они общались, не управляли ими, а были скорее, советниками. И, если ангелы даже строили здесь города, то со стороны противоборствующих сил, на тот момент, было только советничество. В принципе, Тартария была достаточно мощным государством и без всяких советников. Это было государство людей, которые пользовались старыми ресурсами и, скажем так, добрыми советами. Надо так же иметь в виду, что Тартария, численно была больше, чем любая из стран, и армия Тартарии была в десятки раз больше, чем любая другая армия.

**English:**

> Q: "Back then," that's when? 200 years ago?

> A: 300, and a little further. The people who lived on the planet at that time were still able to communicate with the creators, but they were, already quite human, just like those who live now. And those with whom they communicated were not controlling them, but rather, advisors. And while the angels even built cities here, there was only counselling on the part of the opposing forces at the time. In principle, Tartary was a powerful enough state without any advisors. It was a state of people who used the old resources and, let us say, good advice. We must also keep in mind that Tartary, numerically, was larger than any of the countries, and the army of Tartary was dozens of times larger than any other army.

**Russian:**

> В: Правильно ли утверждать, что там был рабовладельческий строй?

> О: Да, был. Но, собственно, в истории с Тартарией, все дело в случившейся подвижке планеты, после которой произошел локальный "[всемирный потоп](http://digitall-angell.livejournal.com/408972.html)". Локальный, потому что, был когда-то всемирный потоп во времена Атлантиды. Вот он был глобальным. Случившийся с Тартарией потоп, можно назвать малым. Таких потопов было много, точно так же, как было несколько малых ледниковых периодов. По сути, Тартарию добила не победа чужой армии, они бы оправились от этого, а сдвижка планеты на несколько градусов, когда все затопило, и последовавшее за этим похолодание, когда люди, уже лишенные инфраструктуры, просто помирали от холода.

**English:**

> Q: Is it correct to say that there was a slave system there?

> A: Yes, it was. But actually, in the Tartarya story, it's all about the planetary shift that happened and then there was a local "[World Flood](http://digitall-angell.livejournal.com/408972.html)". Local, because once upon a time there was a Flood at the time of Atlantis. That was a global one. The flood which happened to Tartary, it is possible to name small. There had been a lot of floods, just as there had been several small ice ages. In fact, Tartary suffered not a victory of alien armies, they would recover from that, but the shift of the planet by several degrees when everything was flooded, and the resulting cooling, when people, already lacking infrastructure, simply died of cold.

**Russian:**

Фрагмент. Помимо динозавра, виден затонувший город в устье.

**English:**

Fragment. In addition to the dinosaur, the sunken city in the estuary can be seen.

**Russian:**

1572 фр 5

**English:**

1572 fr 5

<!-- Local
![](719534_files/527018_original.png#resized)
-->

![](https://ic.pics.livejournal.com/chispa1707/24769663/527018/527018_original.png)

**Russian:**

И василиск, пожирающий свинью. Василиск - символ и герб Тартарии. Московия на карте, ещё только часть её.

**English:**

And a basilisk devouring a pig. The basilisk is the symbol and emblem of Tartaria. Muscovy on the map, still only a part of it.

**Russian:**

Вот замерзшее южное побережье Балтики и торчащие изо льда башни городов. Их тут - тьма.

**English:**

Here's the frozen southern coast of the Baltic Sea and towers of cities sticking out of the ice. There's a lot of them here.

**Russian:**

1572 фр 9

**English:**

1572 fr 9

<!-- Local
![](719534_files/528100_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/chispa1707/24769663/528100/528100_original.jpg#resized)

**Russian:**

Море (хотя лед и изображен коричневым цветом) замерзло до самой Дании. И там (эта карта слишком нехороша) есть серия затонувших городов, башни которых торчат прямо посреди морского льда. Вдоль всего южного побережья.

**English:**

The sea (though the ice is depicted in brown) is frozen all the way to Denmark. And there (this map is too uncool) are a series of sunken cities, with towers sticking out right in the middle of the sea ice. All along the southern coast.

**Russian:**

1550 номер 3

**English:**

1550 number 3

<!-- Local
![](719534_files/525537_original.png#resized)
-->

![](https://ic.pics.livejournal.com/chispa1707/24769663/525537/525537_original.png)

**Russian:**

Детали тут: [Листая старые карты](http://digitall-angell.livejournal.com/717915.html)

**English:**

Details here: [Casting Old Maps](http://digitall-angell.livejournal.com/717915.html)

**Russian:**

> В: Кому выгодно сейчас появление Тартарии? Ведь, ее скрывали сначала. Кому было выгодно, изначально скрыть, а теперь выводить наружу эту информацию?

> О: Это, как в карты играть. Ты не показываешь все карты сразу, но, каждая новая карта привлекает внимание. И, нужно это только для того, чтобы отвлечь.

**English:**

> Q: Who benefits from the emergence of Tartary now? After all, it was hidden at first. Who benefited from hiding it initially, and now to bring it out?

> A: It's like playing cards. You don't show all the cards at once, but, each new card draws attention. And, it's just a distraction.

**Russian:**

> В: То есть, это совершенно не важно, что там произошло?

> О: Это просто одна из войн. Даже на памяти еще живущих людей, войн было достаточно много. И, даже информация о последней войне, уже на памяти совсем молодых людей, изменилась до неузнаваемости, за последние 20 лет.

**English:**

> Q: So it doesn't matter at all what happened there?

> A: It's just one of the wars. Even in the memory of people still living, there have been quite a few wars. And, even the information about the last war, already in the memory of very young people, has changed beyond recognition, over the last 20 years.

**Russian:**

Из раннего по теме: Оригинал взят у [kadykchanskiy](http://kadykchanskiy.livejournal.com/) в [Мина и пожар. Снова "рука инопланетных агрессоров".](http://kadykchanskiy.livejournal.com/222096.html)

**English:**

From earlier on this topic: Original taken from [kadykchanskiy](http://kadykchanskiy.livejournal.com/) in [Mine and fire. The "hand of alien aggressors" again.](http://kadykchanskiy.livejournal.com/222096.html)

<!-- Local
[![](719534_files/userinfo.gif?v=17080?v=141.7#resized)](719534_files/profile)
-->

**Russian:**

Вызывает огромное недоумение тот факт, что закончив победой, тяжелейшую в истории русского народа войну (на тот момент), все скульпторы и архитекторы почему то словно взбесились, массово помешались, и начали по всей России устанавливать памятники, в честь событий другого эпохального  года. Тоже двенадцатого, но не восемьсот, а шестьсот. Диво дивное! Представьте такую ситуацию: Ещё не стихло эхо салюта Победы 9 мая 1945г., а скульпторы дружно кинулись ваять памятники героям русско-турецкой войны, к примеру. Это нормально? Нет. Тогда почему по окончании войны  1812 года никто не думал об увековечении памяти об этой войне, а все разом озаботились событиями двухсотлетней давности!???

**English:**

What causes great bewilderment is the fact that all sculptors and architects, having finished with a victory the hardest war in the history of the Russian people (at that moment), for some reason became insane and started erecting monuments all over Russia in honor of the events of another epoch-making year. Also twelfth, but not eight hundred, but six hundred. Wonderful! Imagine the situation: May 9, 1945 Victory Salute echoed, and sculptors at once started to build monuments to heroes of Russian-Turkish war, for example. Is it normal? No. Then why at the end of the War of 1812 no one thought about perpetuating the memory of this war, and all at once concerned about the events of two hundred years ago!?

**Russian:**

И это не всё! Как раз в середине 19 века, и во второй его половине, по России прокатился бум увековечивания памяти героев войны 1812! Ну чем провинились герои войны 1853-1856? Но нет! Кругом ставят и ставят памятники, триумфальные арки, храмы имени Архангела Михаила, и всё это в честь тех, давних событий, когда дрались не с третьим Наполеоном, а с его дядей - Первым боа-на-партом.

**English:**

And that's not all! Just in the middle of the 19th century and in its second half, there was a boom of immortalization of memory of the heroes of the War of 1812 in Russia! Well, what did the heroes of the War of 1853-1856 do wrong? But no! Monuments, triumphal arches, temples named after Archangel Michael are being erected all over the place and all of this in honor of those, long ago events, when they fought not with the third Napoleon, but with his uncle - the first boa-na-partite.

**Russian:**

Вывод напрашивается сам собой. В 1812 году на самом деле что-то произошло глобальное, и в честь этого события устанавливались памятники. Но потом политика изменилась, и эти памятники переименовали, в честь событий 1612 года, о которых в народе никто и не помнил ничего уже давно. Именно творения мастеров тех лет, скорее всего не имеют отношения к тем лицам, которым приписано авторство.

**English:**

The conclusion suggests itself. In 1812 something global has actually happened, and monuments were established in honor of this event. But then the policy has changed, and these monuments were renamed, in honor of events of 1612 about which people did not remember anything for a long time. It is creations of masters of those years, most likely, have no relation to those persons to whom authorship is attributed.

**Russian:**

И тут начинается самое интересное. Для начала посмотрите внимательно на храм, построенный в память о воинах, погибших при взятии Казани в 1552г.

**English:**

And here begins the most interesting part. First, look closely at the temple, built in memory of the soldiers who died in the capture of Kazan in 1552.

<!-- Local
[![](719534_files/0_ad6c5_bacb2473_XL.jpg#resized)](719534_files/0_ad6c5_bacb2473_orig)
-->

[![](https://img-fotki.yandex.ru/get/9250/1118136.5f/0_ad6c5_bacb2473_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/9250/1118136.5f/0_ad6c5_bacb2473_orig)

**Russian:**

Угадайте, в каком году построено? Умрёте со смеху. В 1813!!! Т.е. на дворе война, весь народ напрягся во имя победы над оккупантами, бегают по лесам с вилами и граблями в поисках, потерявшихся мусье - шевалье, а в это же самое время Амвросий Сретенский строит такую вот пирамиду, украшенную масонской символикой. да ещё в честь событий, отгремевших за двести пятьдесят лет до него. Ну бред же ведь очевидный!

**English:**

Guess what year it was built? You're going to die laughing. В 1813!!! I.e. there is a war out there, all the people are trying to win the occupants, they are running through forests with pitchforks and rakes looking for the lost chevaliers and at the same time Sretensky Ambrose builds such a pyramid decorated with Masonic symbols, and in honor of events that took place more than two hundred and fifty years before him. Well it is an obvious nonsense, isn't it?

**Russian:**

Построено это чудовище в полном соответствии с уровнем технологий второй половины 19 века. И суда по оформлению, в память о том же событии, которому посвящены и все остальные храмы, на которых стали повсеместно изображать "всевидящее око". События 1552г. - жалкая отмазка. Однако очевидно, что не просто так поставили этот вертеп в Казани! Значит Казань имела отношение к тем, скрываемым событиям, которые замаскированы под Отечественную войну 1812г.

**English:**

This monstrosity was built in full accordance with the level of technology of the second half of the 19th century. It commemorates the same event to which all the other churches, on which the "all-seeing eye" is universally depicted, are dedicated. The events of 1552. - is a pathetic excuse. However, it is obvious that not for nothing they put the cave in Kazan! So Kazan was related to those, hidden events, which are masked in the Patriotic War of 1812.

**Russian:**

Если всё так, тогда становится, наконец понятным присутствие в соседнем городе на Волге вот этого шедевра:

**English:**

If everything is so, then the presence of this masterpiece finally becomes clear in the nearby town on the Volga:

<!-- Local
[![](719534_files/0_ad6b0_841d00bb_XL.jpg#resized)](719534_files/0_ad6b0_841d00bb_XL.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/5001/1118136.5f/0_ad6b0_841d00bb_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/5001/1118136.5f/0_ad6b0_841d00bb_XL.jpg#resized)

**Russian:**

Обратите внимание на барельеф. Его автор вездесущий И.П.Мартос.

**English:**

Pay attention to the bas-relief. Its author is the ubiquitous I.P. Martos.

<!-- Local
![](719534_files/0_ad6b3_5bfe93af_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9826/1118136.5f/0_ad6b3_5bfe93af_L.jpg#resized)

**Russian:**

Иван Петрович Мартос (1754-1835) И что же хотел он нам сказать, указывая на Афину Палладу?

**English:**

Ivan Petrovich Martos (1754-1835) And what did he want to tell us, pointing to Athena Pallada?

**Russian:**

О нём отдельно нужно говорить, потому, что всё что в России устанавливалось, непременно каким то боком имеет отношение к этому персонажу с простой русской фамилией.

**English:**

We should speak about him separately, because everything that was established in Russia, is somehow related to this person with a simple Russian surname.

**Russian:**

Это невероятно, но Мартосу приписывается авторство тысяч произведений, разбросанных по территории всей ~~Священной римской~~ Российской империи. Вот список самых известных его творений:

**English:**

It's incredible, but Martos is credited with the authorship of thousands of works scattered throughout the <del>Sacred Roman</del> Russian Empire. Here is a list of his most famous works:

**Russian:**

- бронзовая статуя Иоанна Крестителя, украшающая собой портик Казанского собора в Петербурге.;

- барельеф «Моисей источает воду из камня», над одним из проездов в колоннаде этого храма;

- памятник великой княгине Александре Павловне, в дворцовом парке г. Павловска;

- скульптура в павильоне «Любезным родителям» Павловского парка;

- памятник Минину и Пожарскому (1804 - 1818)[1];

- мраморная статуя Екатерины II, в зале Московского дворянского собрания;

- бюст императора Александра I, изваянный для петербургского биржевого зала;

- памятник Александра I в Таганроге;

- памятник герцогу де Ришельё (1823-1828) в Одессе;

- памятник князя Потемкина в Херсоне;

- памятник Ломоносова в Холмогорах;

- надгробие Прасковьи Брюс;

- надгробный памятник Турчанинова;

- памятник кн. Гагариной, в Александро-Невской лавре;

- памятник тайной советнице Карнеевой (Лашкаревой) Елене Сергеевне, в Александро-Невской лавре;

- «Актеон»;

- памятник Ломоносову в Архангельске перед зданием АГТУ;

- надгробие С.С. Волконской (1782)

- надгробие М.П. Собакиной (1782)

- надгробие Е.С. Куракиной (1792)

- надгробие К. Г. Разумовского в Воскресенской церкви Батурина

**English:**

- The bronze statue of John the Baptist, which adorns the portico of the Kazan Cathedral in St Petersburg;

- Bas-relief "Moses drains water from the stone", above one of the passages in the colonnade of this temple;

- The monument to Grand Duchess Alexandra Pavlovna, in Pavlovsk Palace Park;

- A sculpture in the Pavlovsky Park pavilion "To Lovely Parents;

- Monument to Minin and Pozharsky (1804 - 1818)[1];

- A marble statue of Catherine II in the hall of the Moscow Nobility Assembly;

- A bust of Emperor Alexander I sculpted for the St. Petersburg Stock Exchange Hall;

- Alexander I monument in Taganrog;

- Monument to the Duke de Richelieu (1823-1828) in Odessa;

- Monument of Prince Potemkin in Kherson;

- The Lomonosov monument in Kholmogory;

- Praskovia Bruce's tombstone;

- Turchaninov's tombstone;

- Monument to pr. Gagarina, in the Alexander Nevsky Lavra;

- Monument to secret counselor Elena Sergeyevna Karneeva (Lashkareva) in the Alexander Nevsky Lavra;

- "Acteon."

- The Lomonosov monument in Arkhangelsk in front of the Arkhangelsk State Technical University;

- Tombstone of S.S. Volkonskaya (1782)

- Tombstone of M.P. Sobakina (1782)

- Tombstone of E.S. Kurakina (1792)

- Tombstone of K.G. Razumovsky in Voskresenskaya Church of Baturin

**Russian:**

Впечатлились? Я тоже. Очень похоже на то, что если авторство было неизвестно, или его необходимо было скрыть, то это приписывалось Мартосу. Как в той поговорке: - "Кто знает? Дык Пушкин всё знает!" Вот и Мартос был чем то вроде Пушкина, только в скульптуре.

**English:**

Impressed? So was I. It's very likely that if the authorship was unknown, or had to be concealed, it was attributed to Martos. It's like the proverb: "Who knows? Pushkin knows everything!" So Martos was something like Pushkin, only in sculpture.

**Russian:**

Первой строчкой в списке творений - статуя Иоанна Крестителя.  Посмотрим на неё:

**English:**

First on the list of creations is the statue of John the Baptist.  Let's take a look at it:

<!-- Local
![](719534_files/0_b62e2_6d7cb09b_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9757/1118136.63/0_b62e2_6d7cb09b_L.jpg#resized)

**Russian:**

Как видим, нормальный римско-имперский стиль. А теперь сравним с другими статуями, которые украшают Казанский собор:

**English:**

As we can see, the usual Roman-imperial style. And now let's compare with other statues that decorate the Kazan Cathedral:

<!-- Local
![](719534_files/0_ad6b4_15ff24c7_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9306/1118136.5f/0_ad6b4_15ff24c7_L.jpg#resized)

**Russian:**

Андрей Первозванный. Автор - В.И. Демут-Малиновский.

**English:**

Andrew the First Called. Author - V.I. Demut-Malinovsky.

<!-- Local
![](719534_files/0_ad6b5_f1b33889_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9752/1118136.5f/0_ad6b5_f1b33889_L.jpg#resized)

**Russian:**

Александр Невский. С.С. Пименов.

**English:**

Alexander Nevsky. S.S. Pimenov.

<!-- Local
![](719534_files/0_ad6b6_eaec2277_L.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9113/1118136.5f/0_ad6b6_eaec2277_L.jpg#resized)

**Russian:**

Князь Владимир. С.С.Пименов.

**English:**

Prince Vladimir. S.S. Pimenov.

**Russian:**

Ваяли целой бригадой, но без подписи не различишь, чьё именно творение перед вашими глазами. Единство стиля и качество исполнения потрясающие. Это настоящие шедевры. Посмотрим ещё:

**English:**

The whole crew sculpted it, but without a signature you can't tell whose creation is in front of your eyes. The unity of style and quality of workmanship is amazing. These are true masterpieces. We'll see more:

<!-- Local
![](719534_files/0_ad6b7_95f857e2_XXL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/5001/1118136.5f/0_ad6b7_95f857e2_XXL.jpg#resized)

**Russian:**

Дюк в Одессе. Да... Непревзойдённое мастерство, и если не знать о том, что раньше эта статуя венчала Александрийскую колонну на Дворцовой площади в Петербурге, можно уверовать, что и её тоже сотворил великий русский скульптор Мартос.

**English:**

Duke is in Odessa. Yes... Unsurpassed skill, and if you don't know that this statue used to crown the Alexander Column on Palace Square in St. Petersburg, you can believe that it, too, was created by the great Russian sculptor Martos.

<!-- Local
![](719534_files/0_ad6b8_a51b176f_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9165/1118136.5f/0_ad6b8_a51b176f_XL.jpg#resized)

**Russian:**

Памятник М.В.Ломоносову в Архангельске.

**English:**

Monument to Mikhail Lomonosov in Arkhangelsk.

**Russian:**

Стоп,  стоп... И что... Это вот убожество тоже создал великий Мартос? На каком основании мы верим этому? Только лишь потому, что мусчина с гитарой одет в простыню? Помилуйте! С чего такая разница то!

**English:**

Whoa, whoa... So... This is also the wretchedness created by the great Martos? On what grounds do we believe it? Just because the man with the guitar is wearing a sheet? Come on! Why is there such a difference!

<!-- Local
![](719534_files/0_ad6ba_7ee10e1a_XXL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9762/1118136.5f/0_ad6ba_7ee10e1a_XXL.jpg#resized)

**Russian:**

И Князь Потёмкин в Херсоне, тоже дело рук Мартоса? Не складывается что-то...

**English:**

And Prince Potemkin in Kherson, also the work of Martos? Something doesn't add up...

<!-- Local
![](719534_files/0_ad6b9_609c1cd0_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6731/1118136.5f/0_ad6b9_609c1cd0_XL.jpg#resized)

**Russian:**

Это невероятно, но Мартос ещё и камень резал по выходным, вот такого Моисея сотворил. Однако, универсал!!!

**English:**

It's unbelievable, but Martos was also cutting stone on weekends, that's the kind of Moses he created. However, versatile!!!

<!-- Local
![](719534_files/0_ad6c0_5a0375c2_XXL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/9113/1118136.5f/0_ad6c0_5a0375c2_XXL.jpg#resized)

**Russian:**

А эту надпись, почему то делал не Мартос. Ну или Мартос, но в состоянии страшного бодуна, иначе никак не объяснишь это уродство. На такой великолепном памятнике, такой примитив!" Не вяжется никак с уровнем исполнения самого памятника. Кстати посмотрим на него.

**English:**

And this inscription, for some reason Martos did not do. Well or Martos, but in a state of a terrible hangover, otherwise there is no other way to explain this ugliness. On such a magnificent monument, such a primitive!" Does not tie in any way with the level of execution of the monument. By the way, let's have a look at it.

<!-- Local
[![](719534_files/0_ad6c2_3ebfc417_XL.jpg#resized)](719534_files/0_ad6c2_3ebfc417_orig)
-->

[![](https://img-fotki.yandex.ru/get/9306/1118136.5f/0_ad6c2_3ebfc417_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/9306/1118136.5f/0_ad6c2_3ebfc417_orig)

**Russian:**

Что? Кто то не понял, что перед глазами? Поясняю, что таких памятников два. Один в Москве, на Красной площади, второй в Нижнем Новгороде на Торгу. Этот - Нижегородский. Смотрите при максимальном увеличении. Особое внимание на постамент. Он сложен из блоков, скорее всего карельского, гранита. И тут..... тра-та-та-та-та!!! (звучит барабанная дробь)...

**English:**

What? Somebody don't get what's in front of them? Let me explain, there are two such monuments. One in Moscow, on Red Square, the other in Nizhny Novgorod on Torg. This one is Nizhny Novgorod. Look at the maximum magnification. Special attention on the pedestal. It is made of granite blocks, most likely from Karelia. And here..... tra-ta-ta-ta-ta-ta!!! (drum roll sounds)...

<!-- Local
[![](719534_files/0_ad6c1_2d53c12e_XL.jpg#resized)](719534_files/0_ad6c1_2d53c12e_orig)
-->

[![](https://img-fotki.yandex.ru/get/9749/1118136.5f/0_ad6c1_2d53c12e_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/9749/1118136.5f/0_ad6c1_2d53c12e_orig)

**Russian:**

Але.... ОП!!! Кликаем на картинку для увеличения, и убеждаемся, что постамент из трёх частей: крышка, днище из двух блоков, а межу ними.... МОНОЛИТ!

**English:**

Ale.... OP!!! Click on the picture to enlarge, and make sure that the pedestal is made of three parts: the cover, the bottom of the two blocks, and between them.... MONOLITH!

**Russian:**

И самое главное...

**English:**

And most importantly...

<!-- Local
[![](719534_files/0_ad6be_aabe940e_XL.jpg#resized)](719534_files/0_ad6be_aabe940e_orig)
-->

[![](https://img-fotki.yandex.ru/get/9814/1118136.5f/0_ad6be_aabe940e_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/9814/1118136.5f/0_ad6be_aabe940e_orig)

**Russian:**

Кликаем, увеличиваем, убеждаемся, что в центре Москвы стоит уникальный артефакт, повторить который, современная промышленность не в силах! Это то самое, чем так восхищаемся мы, любители древних булыжников - 3D резка гранита. В монолите, неизвестным науке способом, изготовлена ниша, в которую помещён барельеф,  выемкой материала в трёх плоскостях.

**English:**

Click, increase, make sure that in the center of Moscow is a unique artifact, which can not be repeated by modern industry! This is the thing we admire so much, lovers of ancient cobblestones - 3D granite cutting. In the monolith, unknown to science method, made a niche, which is placed in a bas-relief, recess material in three planes.

**Russian:**

Читать весь пост: [Немного о войне 1812 года и несоответствиях официальной истории](http://digitall-angell.livejournal.com/658995.html)

**English:**

Read the whole post: [A little bit about the War of 1812 and the inconsistencies of official history](http://digitall-angell.livejournal.com/658995.html)

**Russian:**

ТАКЖЕ НЕ ЗАБЫВАЕМ ПРО ЕКАТЕРИНИНСКИЙ СДВИГ:

**English:**

ALSO DON'T FORGET THE CATHERINE SHIFT:

**Russian:**

1472. Иван III женится на Софье, дочери Палеолога

307. Константин женится на Фаусте, дочери Максимиана

Разница 1165 лет

**English:**

1472. Ivan III marries Sophia, daughter of Palaeologus

307. Constantine marries Fausta, daughter of Maximian

The difference is 1165 years.

**Russian:**

1491. Иван III схватил своего брата Андрея Большого

324. Константин схватил и умертвил зятя-соправителя Лициния

Разница 1167 лет

**English:**

1491. Ivan III captured his brother Andrew the Great

324. Constantine seized and put to death his co-ruler son-in-law Licinius

The difference is 1167 years.

**Russian:**

1503. Раздел земель Руси между Иваном III и великим князем Литовским

337. Раздел земель империи между Западной и Восточной

Разница 1166 лет

**English:**

1503. Partition of the lands of Russia between Ivan III and the Grand Duke of Lithuania

337. Partition of the lands of the empire between the Western and Eastern

The difference is 1166 years.

**Russian:**

1561. Иван IV стал Московским Царем

395. Аркадий стал Императором Византии

Разница 1166 лет

**English:**

1561. Ivan IV became Tsar of Moscow

395. Arcadius became Emperor of Byzantium

The difference is 1166 years.

**Russian:**

1575. Новый великий князь Всея Руси - Симеон Беркулатович

408. Новый император Византии - Феодосий II.

Разница 1167 лет

**English:**

1575. The new Grand Duke of All Russia - Simeon Berkulatovich

408. The new emperor of Byzantium - Theodosius II.

The difference is 1167 years.

**Russian:**

1591. Малолетний царевич Дмитрий убит. Престол узурпировал Борис Годунов

423. Малолетний император Валентиниан III свергнут. Престол узурпировал Иоанн

Разница 1168 лет

**English:**

1591. Juvenile Tsarevich Dmitry is killed. Boris Godunov usurps the throne

423. The minor emperor Valentinian III is deposed. The throne was usurped by John

The difference is 1168 years.

**Russian:**

1645: 16-летний царь Алексей Михайлович под «присмотром» Бориса Морозова

476: 16-летний император Ромул отдает власть командующему гвардией Одоакру

Разница 1169 лет

**English:**

1645: The 16-year-old Tsar Alexei Mikhailovich under the "supervision" of Boris Morozov

476: The 16-year-old emperor Romulus gives power to Odoacer, commander of the guard

The difference is 1169 years.

**Russian:**

1689: Соправителем Ивана V стал Петр I 

527: Соправителем Юстина I стал Юстиниан I

Разница 1168 лет

**English:**

1689: Ivan V was succeeded by Peter I 

527: Justinian I was succeeded by Justinian I

The difference is 1168 years.

**Russian:**

1696: Смерть Ивана V, соправителя Петра I

527: Смерть Юстина I, соправителя Юстиниана I

Разница 1169 лет

**English:**

1696: Death of Ivan V, co-creator of Peter I

527: Death of Justinian I, co-ruler of Justinian I

The difference is 1169 years.

**Russian:**

Далее тут: [Тартария, Рим, Екатерининский сдвиг и дальнейшее разделение](http://digitall-angell.livejournal.com/413802.html)

Дополнение к посту: [Оплавленные мегалиты и городища](http://digitall-angell.livejournal.com/719973.html)

По теме: [Новые маски на старых лицах войны 1812 года](http://digitall-angell.livejournal.com/299594.html) /  [Генетические эксперименты, слияние реальностей и роль рептилий](http://digitall-angell.livejournal.com/411969.html) /  [Иллюзия Петра](http://digitall-angell.livejournal.com/191181.html) /  [Индустриально развитая цивилизация существует на Земле десятки тысяч лет](http://digitall-angell.livejournal.com/546774.html)

**English:**

More here: [Tartary, Rome, the Catherine Shift and Further Separation](http://digitall-angell.livejournal.com/413802.html)

Addendum to the post: [Melted megaliths and settlement sites (Russian)](http://digitall-angell.livejournal.com/719973.html)

Addendum to the post: [Melted megaliths and settlement sites (English)](./melted-megaliths-and-settlements.html)

More on this topic: [New masks on the old faces of the War of 1812](http://digitall-angell.livejournal.com/299594.html) / [Genetic experiments, reality fusion and the role of reptiles](http://digitall-angell.livejournal.com/411969.html) / [Peter's illusion](http://digitall-angell.livejournal.com/191181.html) / [An industrially advanced civilization has been on Earth for tens of thousands of years](http://digitall-angell.livejournal.com/546774.html)

© All rights reserved. The original author retains ownership and rights.


