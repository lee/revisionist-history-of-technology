title: Geography of the global world before nuclear war based on example of ancient architecture and starforts
date: 2015-04-27 08:17
modified: Fri 23 Jul 2021 07:36:36 BST
category: 
tags: thermonuclear war; catastrophe
slug: 
authors: WakeUpHuman
summary: Gradually, the timeframe of the nuclear war of the past began to emerge. Its peak period was 1780-1816. By 1816, the nuclear winter had already begun.
status: published
originally: 921_Geography of the global world before nuclear war.html
local: 921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/

### Translated from:

[https://wakeuphuman.livejournal.com/921.html](https://wakeuphuman.livejournal.com/921.html)

Gradually, the timeframe of the nuclear war of the past began to emerge. Its peak period was 1780-1816. By 1816, the nuclear winter had already begun.

[A year without summer (wiki.ru)](https://ru.wikipedia.org/wiki/%D0%93%D0%BE%D0%B4_%D0%B1%D0%B5%D0%B7_%D0%BB%D0%B5%D1%82%D0%B0), [(Google English translation)](https://translate.google.com/translate?sl=ru&tl=en&u=https://ru.wikipedia.org/wiki/%D0%93%D0%BE%D0%B4_%D0%B1%D0%B5%D0%B7_%D0%BB%D0%B5%D1%82%D0%B0)

For three years in the Northern hemisphere, even in summer, there was frost.

I want to show in the article that the world was already global before this war, using the example of the school of engineering and architecture, which was united across the entire planet.

At this point, you can absolutely take into account two facts:

#### Fact 1:

Before the war of 1780-1816, most cities on the planet were built in the same antique style. I mean residential real estate. Buildings that are now classified as temple buildings and buildings of unknown purpose, such as the Giza Pyramids, Mayan Pyramids, and so on, differ. Ancient architecture is best preserved in western Europe. In the rest of the world, most of the cities were completely destroyed. Some were partially damaged, so the ancient buildings have survived to the present day and are presented under the guise of "colonial" architecture. This is nonsense, of course. Those who reformatted the world did not have time to build beautiful buildings during regular uprisings and fighting.

#### Fact 2:

All ancient cities on the planet were surrounded by gigantic, cyclopean structures in the form of star-shaped bastion walls, now called star-forts. The construction volume of one such star around a major city is often equal to the construction volume of the city itself. Millions of cubic meters of earthworks and millions of cubic meters of building stone. Moreover, the stone is filigree processed by machine industrial method. The role of star-forts can be questioned because there are many indicators that suggest they were not fortifications. But more on that later.

Using [google maps](https://maps.google.com) and [google images](https://google.com/), you can verify the truth of the above two facts, and also learn by that implementing the principle 'Divide and Rule', the current helmsmen, who won this war, have been [diligently wiping out ancient-style cities](https://stolenhistory.net/threads/abandoned-ancient-city-found-in-brazil.1617/post-3636), and especially the starforts, for two hundred years. This is done in order to break the unified architectural field of the planet, so that the modern population does not guess that the world was already global in the past.

#### Examining fact number one...

Here is the "construction" of a colonial-style Palace in China by Anglo-French troops. From [http://en.wikipedia.org/wiki/Old_Summer_Palace](http://en.wikipedia.org/wiki/Old_Summer_Palace):

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/23600_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/23600/23600_800.jpg#resized)

The caption under the photo at the [link](http://en.wikipedia.org/wiki/Old_Summer_Palace) says:

> Looting of the Old Summer Palace by Anglo-French forces in 1860 during the Second Opium War.

Here was a palace in China: Yuanmingyuan.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/24783_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/24783/24783_800.jpg#resized)

After being visited by Anglo-French troops, it looked like this.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/25032_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/25032/25032_800.jpg#resized)

In the middle and late 19th century, England conducted more than 200 wars. If she did not participate directly in the war, her interest was always present indirectly. And everywhere, England won and became an empire over which the Sun never set. 

All these wars are more like a punitive cleanup of the remnants of the armed forces in the territories destroyed by nuclear war and the creation of compensation administrations there. It is obvious that without total military and technical superiority, it would be impossible to implement such a large-scale re-arranging of the world.

Tokyo, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/9517_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/9517/9517_800.jpg#resized)

Tokyo, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/9754_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/9754/9754_800.jpg#resized)

Tokyo, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/176530_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/176530/176530_800.jpg#resized)

Yokohama, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/8980_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/8980/8980_800.jpg#resized)

Yokohama, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/9264_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/9264/9264_800.jpg#resized)

Yokohama, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/167937_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/167937/167937_800.jpg#resized)

Yokohama, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/168257_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/168257/168257_800.jpg#resized)

Osaka, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/168607_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/168607/168607_800.jpg#resized)

Osaka, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/168732_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/168732/168732_800.jpg#resized)

Osaka, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/168732_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/168732/168732_800.jpg#resized)

Nagasaki, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/176055_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/176055/176055_800.jpg#resized)

Kobe, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/176283_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/176283/176283_800.jpg#resized)

Arita, Japan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/17466_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/17466/17466_800.jpg#resized)

Buenos Aires, Argentina

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/166724_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/166724/166724_800.jpg#resized)

Buenos Aires, Argentina

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/167003_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/167003/167003_800.jpg#resized)

Buenos Aires, Argentina

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/21550_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/21550/21550_800.jpg#resized)

Buenos Aires, Argentina

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/167222_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/167222/167222_800.jpg#resized)

Havana, Cuba

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/164056_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/164056/164056_800.jpg#resized)

Havana, Cuba

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/164112_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/164112/164112_800.jpg#resized)

Santiago, Chile

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/10508_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/10508/10508_800.jpg#resized)

Santiago, Chile

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/167670_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/167670/167670_800.jpg#resized)

Asuncion, Paraguay

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/169284_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/169284/169284_800.jpg#resized)

Asuncion, Paraguay

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/169621_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/169621/169621_800.jpg#resized)

Asuncion, Paraguay

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/169919_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/169919/169919_800.jpg#resized)

Asuncion, Paraguay

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/170207_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/170207/170207_800.jpg#resized)

Chicago 19th century. Can you believe that such a complex could have been designed and sculpted in marble by the descendants of the conquistadors, who suffered from scurvy and sailed on wooden vessels for 6 months to America?

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/13258_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/13258/13258_800.jpg#resized)

Chicago, USA

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/11886_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/11886/11886_800.jpg#resized)

I highly recommend this article, where the author analyzes the details of facades of buildings in the antique style: [http://mishawalk.blogspot.com/2014/12/2.html (Russian)](http://mishawalk.blogspot.com/2014/12/2.html), [(Google English translation)](https://translate.google.com/translate?sl=ru&tl=en&u=http://mishawalk.blogspot.com/2014/12/2.html).

Seattle, USA

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/26644_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/26644/26644_800.jpg#resized)

Dallas, USA

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/177503_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/177503/177503_800.jpg#resized)

Sevastopol before 1853

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/12090_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/12090/12090_800.jpg#resized)

Sevastopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/170253_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/170253/170253_800.jpg#resized)

Sevastopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/162978_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/162978/162978_800.jpg#resized)

Sevastopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/170511_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/170511/170511_800.jpg#resized)

Sevastopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/170932_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/170932/170932_800.jpg#resized)

Moscow, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/13446_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/13446/13446_800.jpg#resized)

Moscow, Russia, Pashkov House

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/162005_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/162005/162005_800.jpg#resized)

Moscow, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/172050_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/172050/172050_800.jpg#resized)

The ruins of the castle Meshchersky, Moscow region, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/151364_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/151364/151364_800.jpg#resized)

Saint Petersburg, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/162237_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/162237/162237_800.jpg#resized)

Saint Petersburg, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/187086_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/187086/187086_800.jpg#resized)

Omsk, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/21919_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/21919/21919_800.jpg#resized)

Perm, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/22109_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/22109/22109_800.jpg#resized)

Kerch, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/26496_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/26496/26496_800.jpg#resized)

Vladivostok, Russia. American troops in Vladivostok in 1922

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/22291_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/22291/22291_800.jpg#resized)

Vladivostok, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/173656_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/173656/173656_800.jpg#resized)

Vladivostok, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/174033_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/174033/174033_800.jpg#resized)

Simferopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/174385_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/174385/174385_800.jpg#resized)

Simferopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/163164_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/163164/163164_800.jpg#resized)

Simferopol, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/27781_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/27781/27781_800.jpg#resized)

Saratov, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/21411_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/21411/21411_800.jpg#resized)

Saratov, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/171203_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/171203/171203_800.jpg#resized)

Taganrog, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/29323_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/29323/29323_800.jpg#resized)

Simbirsk, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/171437_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/171437/171437_800.jpg#resized)

Kazan, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/163462_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/163462/163462_800.jpg#resized)

Kazan, Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/163744_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/163744/163744_800.jpg#resized)

The Church in Iste, Ryazanskaya obl (oblast?), Russia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/151607_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/151607/151607_800.jpg#resized)

Kiev, Ukraine

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/28055_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/28055/28055_800.jpg#resized)

Kiev, Ukraine

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/28205_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/28205/28205_800.jpg#resized)

Kiev, Ukraine

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/20308_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/20308/20308_800.jpg#resized)

Odessa, Ukraine

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/174093_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/174093/174093_800.jpg#resized)

Chernovtsy, Ukraine

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/162508_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/162508/162508_800.jpg#resized)

Gomel, Belarus

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/171639_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/171639/171639_800.jpg#resized)

Gomel, Belarus

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/171872_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/171872/171872_800.jpg#resized)

Tehran, Iran

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/13791_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/13791/13791_800.jpg#resized)

Tehran, Iran

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/175830_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/175830/175830_800.jpg#resized)

Hanoi, Vietnam

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/14287_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/14287/14287_800.jpg#resized)

Hanoi, Vietnam

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/158355_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/158355/158355_800.jpg#resized)

Haiphong, Vietnam

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/158523_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/158523/158523_800.jpg#resized)

Saigon, Vietnam

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/14743_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/14743/14743_800.jpg#resized)

Padang, Indonesia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/15591_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/15591/15591_800.jpg#resized)

Bogota, Columbia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/17692_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/17692/17692_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/177783_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/177783/177783_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/178062_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/178062/178062_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/178323_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/178323/178323_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/178441_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/178441/178441_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/178844_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/178844/178844_800.jpg#resized)

Rio de Janeiro, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/179011_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/179011/179011_800.jpg#resized)

Sao Paulo, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/179281_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/179281/179281_800.jpg#resized)

Sao Paulo, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/179624_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/179624/179624_800.jpg#resized)

Sao Paulo, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/179779_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/179779/179779_800.jpg#resized)

Sao Paulo, Brazil

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/180067_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/180067/180067_800.jpg#resized)

Malta

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/162791_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/162791/162791_800.jpg#resized)

Manila, Philippines

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/17937_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/17937/17937_800.jpg#resized)

Manila, Philippines

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/158933_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/158933/158933_800.jpg#resized)

Manila, Phillipines

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/159115_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/159115/159115_800.jpg#resized)

Karachi, Pakistan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/18375_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/18375/18375_800.jpg#resized)

Karachi, Pakistan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/19804_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/19804/19804_800.jpg#resized)

Shanghai, China

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/18444_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/18444/18444_800.jpg#resized)

Shanghai, China

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/20169_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/20169/20169_800.jpg#resized)

Taipei, Taiwan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/180240_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/180240/180240_800.jpg#resized)

Taipei, Taiwan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/180674_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/180674/180674_800.jpg#resized)

Taipei, Taiwan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/180970_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/180970/180970_800.jpg#resized)

Taipei, Taiwan

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/181142_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/181142/181142_800.jpg#resized)

Singapore, Republic Of Singapore

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/159347_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/159347/159347_800.jpg#resized)

Singapore, Republic Of Singapore

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/159578_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/159578/159578_800.jpg#resized)

Singapore, Republic Of Singapore

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/159928_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/159928/159928_800.jpg#resized)

Singapore, Republic Of Singapore

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/160110_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/160110/160110_800.jpg#resized)

Singapore, Republic Of Singapore

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/160499_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/160499/160499_800.jpg#resized)

Managua, Nicaragua

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/19097_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/19097/19097_800.jpg#resized)

Managua, Nicaragua

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/160736_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/160736/160736_800.jpg#resized)

Calcutta, India. The Prince of Wales entered with an army. The Palace in the "colonial" style is already standing.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/21051_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/21051/21051_800.jpg#resized)

Calcutta, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/175302_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/175302/175302_800.jpg#resized)

Calcutta, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/175585_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/175585/175585_800.jpg#resized)

Calcutta, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/177036_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/177036/177036_800.jpg#resized)

Pondicherry, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/158067_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/158067/158067_800.jpg#resized)
Chennai, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/157287_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/157287/157287_800.jpg#resized)

Chennai, India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/157804_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/157804/157804_800.jpg#resized)

Bombay (Mombay), India

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/174722_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/174722/174722_800.jpg#resized)

Cape Town, South Africa

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/22569_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/22569/22569_800.jpg#resized)

Cape Town, South Africa

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/40470_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/40470/40470_800.jpg#resized)

Pretoria, South Africa

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/177385_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/177385/177385_800.jpg#resized)

Seol, Korea

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/19250_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/19250/19250_800.jpg#resized)

Seol, Korea

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/23240_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/23240/23240_800.jpg#resized)

Melbourne, Australia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/23419_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/23419/23419_800.jpg#resized)

Brisbane, Australia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/25147_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/25147/25147_800.jpg#resized)

Oaxaca, Mexico

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/25597_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/25597/25597_800.jpg#resized)

Mexico City, Mexico

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/25723_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/25723/25723_800.jpg#resized)

Mexico City, Mexico

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/167852_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/167852/167852_800.jpg#resized)

Caracas, Venezuela

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/166242_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/166242/166242_800.jpg#resized)

Caracas, Venezuela

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/166619_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/166619/166619_800.jpg#resized)

Toronto, Canada

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/25873_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/25873/25873_800.jpg#resized)

Toronto, Canada

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/27035_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/27035/27035_800.jpg#resized)

Montreal, Canada

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/28674_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/28674/28674_800.jpg#resized)

Penang Island, George Town, Malaysia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/100160_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/100160/100160_800.jpg#resized)

Penang Island, George Town, Malaysia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/100495_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/100495/100495_800.jpg#resized)

Penang Island, George Town, Malaysia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/100678_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/100678/100678_800.jpg#resized)

Dakka, Bangladesh

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/149573_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/149573/149573_800.jpg#resized)

Phuket, Thailand

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/28519_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/28519/28519_800.jpg#resized)

Bangkok, Thailand

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/161016_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/161016/161016_800.jpg#resized)

Bangkok, Thailand

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/161122_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/161122/161122_800.jpg#resized)

Bangkok, Thailand

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/161513_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/161513/161513_800.jpg#resized)

Bangkok, Thailand

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/161770_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/161770/161770_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/164511_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/164511/164511_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/164854_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/164854/164854_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/165030_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/165030/165030_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/165173_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/165173/165173_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/165593_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/165593/165593_800.jpg#resized)

Istanbul (Constantinople), Turkey

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/165711_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/165711/165711_800.jpg#resized)

Colombo, Sri-Lanka

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/172402_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/172402/172402_800.jpg#resized)

Colombo, Sri-Lanka

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/172606_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/172606/172606_800.jpg#resized)

Colombo, Sri-Lanka

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/172837_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/172837/172837_800.jpg#resized)

Colombo, Sri-Lanka

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/173184_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/173184/173184_800.jpg#resized)

Colombo, Sri-Lanka

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/173529_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/173529/173529_800.jpg#resized)

In this list, you also need to add all the destroyed cities that the manipulator assigned the status of ancient Greek and Roman. This is all nonsense. They were destroyed 200-300 years ago. Simply because of the desertification of the territory, life mostly did not resume in the ruins of such cities.

Timgad, Algeria, Africa

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/89713_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/89713/89713_800.jpg#resized)

Palmyra, Syria

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/40846_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/40846/40846_800.jpg#resized)

Palmyra, Syria (translator's note: presumably - the original is unlabelled)

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/39650_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/39650/39650_800.jpg#resized)

Ephesus in Turkey and any similar ancient ruins

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/166090_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/166090/166090_800.jpg#resized)

Compare buildings. Their design and original functional purpose are identical:

Lebanon, Baalbek

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/40097_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/40097/40097_800.jpg#resized)

Orthodox Cathedral of St. Peter and Paul, Sevastopol.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/40294_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/40294/40294_800.jpg#resized)

The old museum in Kerch on Mount Mithridates, Crimea.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/89877_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/89877/89877_800.jpg#resized)

Church Of The Holy Archangel Michael, Alupka, Crimea.

It was destroyed by a strong landslide. And in 1908, the building - which had fallen into disrepair - was dismantled.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/187261_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/187261/187261_800.jpg#resized)

Valhalla in Regensburg, Germany

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/90343_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/90343/90343_800.jpg#resized)

Temple Of Poseidon, Italy

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/90581_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/90581/90581_800.jpg#resized)

The Parthenon, Nashville, USA

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/90768_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/90768/90768_800.jpg#resized)

Temple of Apollo in Delphi

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/91107_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/91107/91107_800.jpg#resized)

Temple of Theseus in Vienna, Austria

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/91283_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/91283/91283_800.jpg#resized)

Temple of Hephaestus in Athens

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/91477_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/91477/91477_800.jpg#resized)

Paris. The Church Of The Madeleine. 1860

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/114336_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/114336/114336_800.jpg#resized)

Peterhof. The Belvedere Palace.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/186674_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/186674/186674_800.jpg#resized)

Garni temple in Armenia

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/91804_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/91804/91804_800.jpg#resized)

You can continue indefinitely. The reader can see this for himself, it is enough to Google the name of any more or less large city in English plus the keywords 'old buildings' or <name of city> + 'old photos' or <name of city> + '19 century photos' and click 'Show Images'. Residential real estate will be very similar: identical arches, pilasters, turrets, columns, balustrades.

For example, look at the images for the following keywords

- 'sidney old buildings' [Google](https://www.google.com/search?&q=sidney+old+buildings) [Bing](https://www.bing.com/search?&q=sidney+old+buildings) [Yandex](https://www.google.com/search?&q=sidney+old+buildings)

- 'calcutta old buildings' [Google](https://www.google.com/search?&q=calcutta+old+buildings) [Bing](https://www.bing.com/search?&q=calcutta+old+buildings) [Yandex](https://www.yandex.com/search?&q=calcutta+old+buildings)

- 'boston old buildings' [Google](https://www.google.com/search?&q=boston+old+buildings) [Bing](https://www.bing.com/search?&q=boston+old+buildings) [Yandex](https://www.yandex.com/search?&q=boston+old+buildings)

- 'rangoon old buildings' [Google](https://www.google.com/search?&q=rangoon+old+buildings) [Bing](https://www.bing.com/search?&q=rangoon+old+buildings) [Yandex](https://www.yandex.com/search?&q=rangoon+old+buildings)

- 'manila old buildings' [Google](https://www.google.com/search?&q=manila+old+buildings) [Bing](https://www.bing.com/search?&q=manila+old+buildings) [Yandex](https://www.yandex.com/search?&q=manila+old+buildings)

- 'melbourne old photos' [Google](https://www.google.com/search?&q=melbourne+old+buildings) [Bing](https://www.bing.com/search?&q=melbourne+old+buildings)  [Yandex](https://www.yandex.com/search?&q=melbourne+old+buildings)

What you should pay attention to?

The official history is that all these buildings were built in the mid-late 19th century. At this time, use of cameras was already in full force. But you will not find any photos of the construction of a more or less serious object, although they were built massively at this time. There was a real construction boom. The whole world in the 19th century was at war ([list of wars of the 19th century](https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%B2%D0%BE%D0%B9%D0%BD_XIX_%D0%B2%D0%B5%D0%BA%D0%B0)) and at the same time the whole world was built up with ancient buildings, many of which can not be built now. Theaters and opera theaters are not built in times of war and unrest.

Almost all photos of the 19th century show bearded people in worn-out clothes, in shapeless old boots, mostly working with earth, carrying earth in wheelbarrows, using rather primitive lifting cranes made of logs, sometimes using steam engines for earthwork. But there are no photos that clearly show a half-built building like the Vienna Opera, the Budapest Parliament, or the Odessa Opera House.

The most you will find is a photo showing finished buildings in scaffolding, which shows that restoration work is underway; the domes and roof are being repaired.

Feed these to search engines and look at photos and images:

- 19 century construction [Google](https://www.google.com/search?&q=19+century+construction) [Bing](https://www.bing.com/search?&q=19+century+construction) [Yandex](https://www.google.com/search?&q=19+century+construction)

- 19 century town building [Google](https://www.google.com/search?&q=19+century+town+building) [Bing](https://www.bing.com/search?&q=19+century+town+building) [Yandex](https://www.google.com/search?&q=19+century+town+building)

- 19 century opera building [Google](https://www.google.com/search?&q=19+century+opera+building) [Bing](https://www.bing.com/search?&q=19+century+opera+building) [Yandex](https://www.google.com/search?&q=19+century+opera+building)

- 19 century museum building [Google](https://www.google.com/search?&q=19+century+museum+building) [Bing](https://www.bing.com/search?&q=19+century+museum+building) [Yandex](https://www.google.com/search?&q=19+century+museum+building)

and you will see that these ancient buildings were not built in the 19th century.

#### Let's go to fact number two - the star city.

They are found on all continents except Australia. In Australia, they are completely destroyed. Surprisingly, few people know about them. To date, about one thousand have been found. [Here in this Vkontakte group](http://vk.com/albums-55242135) (Russian), you can look at satellite images of several hundred of these objects, as well as city plans made in the 17th and 18th centuries.

[http://vk.com/albums-55242135](http://vk.com/albums-55242135)

It is necessary to view satellite images and old city plans in order to understand the scale of stone construction in the past and to understand the unity of standards in the engineering and architectural industry of that time. I can't upload 500 photos here, it will overload the article too much.

You can (Translator note: now 'could') download the kmz file with star tags for Google Earth using this link
[http://peshkints.livejournal.com/7944.html](http://peshkints.livejournal.com/7944.html)

So, the most famous star-shaped object in Russia is the Petropalov fortress:

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/15759_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/15759/15759_800.jpg#resized)

It is the only one that is perfectly preserved. Hundreds, maybe thousands, have been destroyed over the past 200 years.

Very often, the outer slope of such a star is lined with high-quality masonry. The weight of blocks can reach a ton or more. Sometimes the slope is earthen. Entrance gates to star FORTS are often made of stone with very high-quality milling processing. Almost all the triumphal arches in the world are entrance gates through the wall to former stars. Inside and outside there is a stone infrastructure similar to the Peter and Paul fortress.

In Europe, small star cities are preserved perfectly, they look like this:

Neuf-Brisach fortress, France

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/16412_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/16412/16412_800.jpg#resized)

Palmanova fortress

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/16690_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/16690/16690_800.jpg#resized)

And on the territory of the former USSR, like this:

Brest Fortress

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/16957_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/16957/16957_800.jpg#resized)

The diameter of this star is 2 km. Inside it was built up with the same houses as in the two pictures above, but now everything is wiped out in the dust. And this is a common trend for almost all cities on the planet. The stars, along with the residential infrastructure, are best preserved in Western Europe and to a lesser extent in Eastern Europe. Viewing modern cities through Google Earth, the pathological desire of the manipulator to destroy star systems is evident. In Europe, it was dismantled carefully, preserving the surrounding infrastructure. For example, I will show you three European cities of Turin, Strasbourg and Antwerp. But this rule works for ANY city.

Plan of Turin in 1799

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/29543_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/29543/29543_800.jpg#resized)

Turin in 1682

The image is clickable

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/29708_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/29708_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/29708/29708_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/29708/29708_original.jpg)

Turin now

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30010_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30010_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/30010/30010_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/30010/30010_original.jpg)

You will not find any traces of this gigantic star-shaped wall or canal. A great deal of work has been done to dismantle such structures in order to hide their very nature.

Plan Of Strasbourg

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30428_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30428_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/30428/30428_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/30428/30428_original.jpg)

Strasbourg in 1644

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30476_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30476_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/30476/30476_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/30476/30476_original.jpg)

Strasbourg now. The Basilica with the spire is in place, the canals are in place, the walls with bastions are not.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/30735_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/30735/30735_800.jpg#resized)

Strasbourg from ae satellite. The bastion wall has been carefully removed.

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/31167_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/31167_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/31167/31167_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/31167/31167_original.jpg)

Antwerp.

The plan is from 1642, it is tilted 90 degrees. The length of the horizontal segment from the left edge of this star wall to the right is 5.7 km. The object is huge!

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/31386_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/31386_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/31386/31386_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/31386/31386_original.jpg)

Second plan of Antwerp

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/32322_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/32322/32322_original.jpg#resized)

See below and to the right are eight forts marked with diamonds? Now we will look at the satellite photo. On it, we will see that only the channel remains of the wall. Of the forts, too, all that remains are earthen mounds, eight of them. I circled them in red. The width of each fort is 600 meters.

The image is clickable

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/32757_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/32757_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/32757/32757_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/32757/32757_original.jpg)

How are things in Russia, Ukraine, Belarus, Kazakhstan, and so on? Worse. Far worse. Everything was razed to the ground. There are almost no authentic antique buildings left. All these stalinki, khrushchevki - these are quickly erected barracks. In fact, quickly built according to standard designs, so that the surviving population could move and demographically support the number of workers.

Moscow, 17th century. I think inside the ring at that time, the architecture was much larger than Turin and Paris.

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/32803_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/32803_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/32803/32803_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/32803/32803_original.jpg)

Kiev

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33106_800.png#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33106_original.png)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/33106/33106_800.png#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/33106/33106_original.png)

Kiev, what remained was a fragment. The houses are already a poor new style of building.

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33968_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/33968/33968_800.jpg#resized)

Orenburg

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33394_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33394_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/33394/33394_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/33394/33394_original.jpg)

The plan of Orenburg overlaid on a satellite image

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33642_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/33642_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/33642/33642_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/33642/33642_original.jpg)

Ochakov. Remember Griboyedov's "Times of Ochakov and the Conquest of the Crimea"?

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/34145_800.gif#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/34145_original.gif)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/34145/34145_800.gif#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/34145/34145_original.gif)

Ochakov. The plan superimposed on the image

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/34527_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/34527_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/34527/34527_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/34527/34527_original.jpg)

Ishmael

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35214_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35214_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/35214/35214_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/35214/35214_original.jpg)

Ishmael from Sputnik

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35368_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35368_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/35368/35368_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/35368/35368_original.jpg)

Ishmael. The plan superimposed on the image

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35061_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35061_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/35061/35061_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/35061/35061_original.jpg)

Kherson

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35696_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35696_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/35696/35696_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/35696/35696_original.jpg)

Kherson, Sputnik. The manipulator likes to build stadiums on the sites of star citadels. In Odessa as well.

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35869_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/35869_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/35869/35869_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/35869/35869_original.jpg)

Kherson. The plan superimposed on the image

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/36117_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/36117_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/36117/36117_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/36117/36117_original.jpg)

Plan Of Slutsk, Belarus

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/86259_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/86259/86259_original.jpg#resized)

Slutsk, satellite photo

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/85031_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/85031/85031_800.jpg#resized)

Slutsk. The plan superimposed on the image

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/85298_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/85298/85298_800.jpg#resized)

Ukraine. On the site of an ancient city with a star, cottages are located. Coast of the sea of Azov, Peter's fortress near Berdyansk.

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/36738_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/36738_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/36738/36738_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/36738/36738_original.jpg)

The plan of Peter's fortress

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/85548_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/85548/85548_800.jpg#resized)

Peter's fortress. The plan superimposed on the image

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/85894_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/85894/85894_800.jpg#resized)

Russia. Near Rostov

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/37325_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/37325_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/37325/37325_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/37325/37325_original.jpg)

Under Rostov city with a star on 6 bastions was. Here, for comparison, is the same in Croatia, on 6 bastions. The city is called Karlovac.

Plan

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/38058_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/38058_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/38058/38058_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/38058/38058_original.jpg)

Karlovac in our time.

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/38355_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/38355_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/38355/38355_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/38355/38355_original.jpg)

Compared? Now appreciate the cynicism of the Western media reproaching the Slavs and other peoples that you do not know how to build anything, you live behind the times. And this is their handiwork. But more on that later.

And here is a star that speaks for itself in Northern Kazakhstan, with the surrounding infrastructure demolished by three nuclear explosions. Later, the village of Stanovoe appeared on the ashes. Click to enlarge

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/39274_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/39274_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/39274/39274_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/39274/39274_original.jpg)

And the second star in the Omsk region. Next to it is a crater more than 1 kilometer in diameter. The starfort is circled in red. The image is clickable:

<!-- Local
[![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/47151_800.jpg#clickable)](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/47151_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/47151/47151_800.jpg#clickable)](https://ic.pics.livejournal.com/wakeuphuman/74019716/47151/47151_original.jpg)

Crater on the site of the former city, with the corresponding legend:

<!-- Local
![](921_Geography of the global world before nuclear war based on example of ancient architecture and starforts_files/149384_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/149384/149384_800.jpg#resized)

The search pattern for old city plans is simple. In different languages, feed the phrase to a search engine

- 'old plan city' [Google](https://google.com/search?&q=old+plan+city) [Bing](https://bing.com/search?&q=old+plan+city) [Yandex](https://yandex.com/search?&q=old+plan+city)
- 'old map city' [Google](https://google.com/search?&q=old+map+city) [Bing](https://bing.com/search?&q=old+map+city) [Yandex](https://yandex.com/search?&q=old+map+city)
- 'ancient map of the city. [Google](https://google.com/search?&q=ancient+map+of+the+city) [Bing](https://bing.com/search?&q=ancient+map+of+the+city) [Yandex](https://yandex.com/search?&q=ancient+map+of+the+city)

Try it in Latin, French, Spanish, German. And click 'Show Images'. If the plan was drawn up before the end of the 18th century, the city will be framed by a star wall.

© All rights reserved. The original author retains ownership and rights.
