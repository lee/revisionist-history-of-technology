title: Prior Technologies as Religious Artifacts and Cargo Cults - by Sibved
date: 2021-07-23 00:02
modified: 2022-04-24 09:42:14
category: 
tags: technology; links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md


[Ancient Technology of Background Vibrations](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/212566.html)

[Technical Artifacts that have Become Religious Cult Objects](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/228897.html)

[Aboriginal Helmets in Indonesia and Other Strange Cults](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/238330.html)

[Striking Analogies - Part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/208671.html)

[Striking Analogies - Part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/208991.html)

[Striking Analogies - Part 3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/265855.html)

[The Secrets of St Basil's Cathedral](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/213063.html)

[Inlay in St Isaac's Cathedral - How](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/194867.html)
