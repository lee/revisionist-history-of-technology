title: Where is the city from? Chapter 1. Old maps
date: 2013-11-01
modified: 2022-04-24 10:07:22
category: 
tags: St Petersburg
slug: 
authors: ZigZag
summary: It is hard to believe that the planning, breakdown and referencing of the buildings of St. Petersburg under Peter I was carried out without surveyors... the scope and accuracy, volume and territory are striking. Or maybe the city stood long before the current historical version of its appearance?
status: published
originally: Where is the city from? Chapter 1. Old maps.html
local: Where is the city from Chapter 1 Old maps_files
from: http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html

### Translated from:
[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-links.html). The translation below is kept purely for archival reasons.*

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

Now the younger generation reads much less, give them films and programs. Write a maximum of SMS-length text, read at least the same SMS, and, at the most, a magazine. So far, my students have only had enough for pictures, the essence for them is shaky and vague, and what difference does it make what was there, it is important what is and what will be. But my life experience tells me: without looking back, you will never know where to go, because you don't know where you came from.

The older generation firmly believes in what they were told, in what they were forced to learn from memory, socialism, communism, atheism. And that those who lead know exactly which path, course, direction to choose. Although many are already looking at them in confusion, not realizing that the course is not straight, but curved, and their life has passed like a non-stop running in a circle.

My history teacher recently told me:

> Do not take away those crumbs that are left to us, faith in what we were taught. I'm tired of believing in the party, Lenin and Stalin, but you swung at Peter I himself, at the splendor of Russian history. Don't trample my last fairy tale, or people like me will trample you.

It's hard for them to understand me, I'm just trying to figure out:

What for?

For what?

Who benefits from it?

> A person's consciousness is blinkered by the information that has been instilled in him since childhood. But it is worth pointing out to him the inconsistency of the imposed versions, as he immediately declares: I knew this the day before yesterday. So, most of our history was created precisely in order to confuse, divert from logical conclusions, distract from the perception of the whole picture, breaking this picture into puzzles and focusing on the most colorful, but explaining nothing fragments. Understanding comes only to a few. I want the ranks of thinkers to number in the thousands, tens of thousands, millions. Perhaps loud words, but I try, I do at least something. (ZigZag).

So now I sit and look at the old map of St. Petersburg, and I am amazed:

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/700px-Map_of_Saint-Petersburg_in_1720_Homann.jpg#resized)
-->

[![](http://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg/700px-Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg/700px-Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg)

*Plan of St. Petersburg I. Homann. Etching, cutter, watercolor on paper. 50.5х59.5 cm.1720th (before 1725)*

**Russian:**

А история говорит, что Са́нкт-Петербу́рг основан 16 мая 1703 (Государь положил первый камень постройке 16 мая 1703 г., в день Святой Троицы. Вот предание об основании города), и что, это всё за 10-15 лет, при зимах -35-40, мошкаре, сырости, отсутствии дорог и заводов, я уже о строительной технике не говорю. Достаточно бросить взгляд на Васильевский остров, ещё ничего нет, но есть разметка и планировка, а масштабы? Никто в Европе ещё и не помышлял о такой планировке, а тут?

**English:**

And history says that St. Petersburg was founded on May 16, 1703 (the Emperor laid the first stone of the building on may 16, 1703, on the day of the Holy Trinity. Here is the legend about the Foundation of the city), and what, it's all in 10-15 years, with winters-35-40, midges, dampness, lack of roads and factories, I'm not talking about construction equipment. Just take a look at Vasilievsky island, there is nothing yet, but there is a layout and layout, and the scale? No one in Europe has ever thought of such a layout, but here?

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/734px-Summer_Garden_Zubov.jpg#resized)
-->

![](http://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Summer_Garden_%28Zubov%29.jpg/734px-Summer_Garden_%28Zubov%29.jpg#resized)

[Summer garden in 1716, by Alexey Zubov](http://ru.wikipedia.org/wiki/%C7%F3%E1%EE%E2,_%C0%EB%E5%EA%F1%E5%E9_%D4%B8%E4%EE%F0%EE%E2%E8%F7).

**Russian:**

Во скорость постройки, "не то, что нынешнее племя", или где-то кроется подвох, может врут историки? Некоторые из зданий, изображенных на этой
гравюре, согласно официальной истории, должны появиться намного позже, уже после смерти автора, но А.Зубов точно зал что и где рисовать. Слева и справа вдалеке виднеются шпили, слева Михайловский замок справа Спас на Крови, так вот: 17 апреля 1819 года был заложен фундамент Михайловского дворца.

**English:**

During the construction, "not like the current tribe", or somewhere there is a catch, maybe historians are lying? Some of the buildings depicted in this engraving, according to the official history, should appear much later, after the author's death, but A. Zubov knew exactly what and where to draw. To the left and right, spires can be seen in the distance. To the left, [Mikhailovsky Castle](http://ru.wikipedia.org/wiki/%CC%E8%F5%E0%E9%EB%EE%E2%F1%EA%E8%E9_%E7%E0%EC%EE%EA), on the right, Saved on Blood, so: on April 17, 1819, the foundation of [the Mikhailovsky Palace](http://ru.wikipedia.org/wiki/%CD%EE%E2%EE-%CC%E8%F5%E0%E9%EB%EE%E2%F1%EA%E8%E9_%E4%E2%EE%F0%E5%F6) was laid. This day was the day of the Foundation of one of the largest museums in the world - the State Russian Museum. Church of the Savior on Spilled Blood.

The [Church of the Savior on blood](http://ru.wikipedia.org/wiki/%D1%EF%E0%F1_%ED%E0_%CA%F0%EE%E2%E8) was erected in 1883-1907, on the site where the Tsar-Liberator Alexander II was mortally wounded on March 1, 1881. But more on that below. Punctures of Zubov Montferan, Falconet, Schubert, Karamzin and the artistic gift of the well-known A.S. We will consider Pushkin in detail below.

An amendment came from [violet3333](http://violet3333.livejournal.com/) (a [wonderful magazine](http://violet3333.livejournal.com/), I'll tell
you):

> In the article "Where is the City", I found an inaccuracy in the description of Zubov's engraving - in fact, the Mikhailovsky Castle is in the center, on the left is the Church of Saints and Righteous Simeon the God-Receiver and Anna the Prophetess, on the right I do not know what it is but it is not the Savior on Spilled Blood.

But even with these amendments, Zubov still painted what - according to historical documents - should appear much later.

It is hard to believe that the planning, breakdown and referencing of the buildings of St. Petersburg under Peter I was carried out without surveyors... the scope and accuracy, volume and territory are striking. Or maybe the city stood long before the current historical version of its appearance?

1753:
<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/5669010.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/5669010.jpg)
-->

[![](http://3.bp.blogspot.com/-HzaYns5w92A/UBYlvfat2DI/AAAAAAAACB4/jGYgqe3GNmc/s1600/5669010.jpg#clickable)](http://3.bp.blogspot.com/-HzaYns5w92A/UBYlvfat2DI/AAAAAAAACB4/jGYgqe3GNmc/s1600/5669010.jpg)


19th century:

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/pitermap_m.jpg#resized)
-->

![](http://www.arhivtime.ru/karta/karta/map_drugie/pitermap_m.jpg#resized)

Another observation is very interesting in this regard... When all of Europe lived in cities where sewage systems hardly appeared under the streets, the width of which barely allowed the carriages to leave, and the buildings expanded from the center (map of Paris, late 17th century, at that time the only standard city ​​building)

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/562246f7a7bb700183010dda00d334d0.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/562246f7a7bb700183010dda00d334d0.jpg)
-->

[![](http://1.bp.blogspot.com/-U6QurP0MtUY/Udagg3Wi6QI/AAAAAAAADhg/5lP6SpFR8aE/s640/562246f7a7bb700183010dda00d334d0.jpg#clickable)](http://1.bp.blogspot.com/-U6QurP0MtUY/Udagg3Wi6QI/AAAAAAAADhg/5lP6SpFR8aE/s1600/562246f7a7bb700183010dda00d334d0.jpg)

The same Amsterdam that taught Peter everything:

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/616px-Cornelis_anthonisz_vogelvluchtkaart_amsterdam.JPG#resized)
-->

![](http://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Cornelis_anthonisz_vogelvluchtkaart_amsterdam.JPG/616px-Cornelis_anthonisz_vogelvluchtkaart_amsterdam.JPG#resized)

(London (below), the year is indicated on the map .... Capital as capital, not a single straight line)

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/Rocques_Map_of_London_1741-5.jpg#resized)
-->

![](http://upload.wikimedia.org/wikipedia/commons/3/3a/Rocque%27s_Map_of_London_1741-5.jpg#resized)

Moscow could not get rid of the chaotic development.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/c5d4b275e546.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/c5d4b275e546.jpg)
-->

[![](http://4.bp.blogspot.com/-ZZ2xjWmNBmA/UdafQXnbpfI/AAAAAAAADhI/B0cGXuqcDy0/s1600/c5d4b275e546.jpg#clickable)](http://4.bp.blogspot.com/-ZZ2xjWmNBmA/UdafQXnbpfI/AAAAAAAADhI/B0cGXuqcDy0/s1600/c5d4b275e546.jpg)

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/0116a0f3e01ct.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/0116a0f3e01ct.jpg)
-->

[![](http://3.bp.blogspot.com/-VDa23C1WZlw/UdafV9OhmhI/AAAAAAAADhQ/Y7XqLig_1_E/s600/0116a0f3e01ct.jpg#clickable)](http://3.bp.blogspot.com/-VDa23C1WZlw/UdafV9OhmhI/AAAAAAAADhQ/Y7XqLig_1_E/s600/0116a0f3e01ct.jpg)

And here is a map of Kiev - the mother of Russian cities:

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/kiev-map-historical-1700.jpg#resized)
-->

![](http://maps.vlasenko.net/historical/kiev/kiev-map-historical-1700.jpg#resized)

**Russian:**

Карта 1717 года, и это только проект застройки Питера, заказанный но не запущенный

**English:**

Map of 1717, and this is only a development project of St. Petersburg, ordered but not launched:

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/ris1.jpg#resized)
-->

![](http://www.gumer.info/bibliotek_Buks/History/Article/ris1.jpg#resized)

**Russian:**

А вот карта 1720, как говорится "по факту"

**English:**

And here is a map of 1720, as they say "in fact"

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/700px-Map_of_Saint-Petersburg_in_1720_Homann.jpg#resized)
-->

![](http://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg/700px-Map_of_Saint-Petersburg_in_1720_%28Homann%29.jpg#resized)

[Here are some more drawings, all authentic and kept in the Museum. Just click on the link.](http://artcyclopedia.ru/zubov_aleksej_fedorovich.htm)

**Russian:**

Так разбить Васильевский остров без геодезистов... ну никак, так кому верить?

**English:**

So, to break Vasilievsky island without surveyors... well, no way, so who to believe?

**Russian:**

А вот город в 1716 году, ещё до генерального проекта, это данные с гравюры, или опять врут?

**English:**

But the city in 1716, even before the General project... is this data from the engraving, or are they lying again?

**Russian:**

По этой ссылке обалденная подборка карт городов. Выводы делайте сами, а автору от меня лично ОГРОМНАЯ благодарность за его труд: Тут ещё две очень занимательные карты, уже датипрованные 1703 годом, даю ссылки 

[Записки скучного человека - Планы столичных городов Европы и некоторых примечательных городов в Азии, Африке и Америке. 1771](http://humus.livejournal.com/3422588.html)

[http://www.swaen.com/zoom.php?id=4399&referer=item.php](http://www.swaen.com/zoom.php?id=4399&referer=item.php)

[http://www.swaen.com/zoom.php?id=3734&referer=item.php](http://www.swaen.com/zoom.php?id=3734&referer=item.php)

**English:**

This link is a great selection of city maps. Draw your own conclusions, and I personally thank the author for his work: Here are two more very interesting maps, already dated 1703, I give links:

[Notes of a boring person - Plans of the capital cities of Europe and some notable cities in Asia, Africa and America. 1771](http://humus.livejournal.com/3422588.html)

[http://www.swaen.com/zoom.php?id=4399&referer=item.php](http://www.swaen.com/zoom.php?id=4399&referer=item.php)

[http://www.swaen.com/zoom.php?id=3734&referer=item.php](http://www.swaen.com/zoom.php?id=3734&referer=item.php)

**Russian:**

Они больше похожи не на проекты застройки, а на проекты реконструкции.

**English:**

They are more like reconstruction projects than building projects.

**Russian:**

Необходимо оговориться, что сохранившиеся исторические сведения об основании Петербурга не отличаются безусловной достоверностью. В Преображенском походном журнале говорится, что 11 мая Петр поехал сухим путем в Шлиссельбург, 14 мая был на сясском устье, 16 мая проехал еще далее, и 17 мая прибыл на Лодейную пристань. Таким образом, если верить этому дневнику, 16 мая Петра не было в Петербурге. Поэтому многие принимают за день основания новой столицы 29 июня 1703 года, когда совершена была закладка храма святых Апостолов Петра и Павла. Достойно внимания также, что ни в каких современных документах имя Санкт-Петербурга не упоминается ни в мае, ни в июне того года; местность эта сохраняла имя Шлотбурга. Но на картах начала 18 века Петропавловская крепость уже стоит, не островок, а именно крепость, с чётко обрисованными границами.Вот такая она сегодня, только снял с гуглмапс, те же шесть лучей, а сколько её строили по истории? И ещё... И. Э. Клейненберг обнаружил известие о Васильевском острове, лежащем в устье Невы, в ливонском документе 1426 года, странно, не правда ли?

**English:**

It should be noted that the preserved historical information about the Foundation of St. Petersburg is not absolutely reliable. The Preobrazhensky marching journal says that on may 11, Peter went overland to Shlisselburg, on may 14 was at the Syassky estuary, on may 16 went even further, and on may 17 arrived at Lodeynaya pier. Thus, according to this diary, on may 16, Peter was not in St. Petersburg. Therefore, many take the day of the Foundation of the new capital on June 29, 1703, when the Church of the Holy Apostles Peter and Paul was laid. It is also noteworthy that no contemporary documents mention the name of St. Petersburg either in may or June of that year; the area retained the name of Schlotburg. But on the maps of the early 18th century, the Peter and Paul fortress is already standing, not an island, but a fortress, with clearly outlined borders.Here it is today, only removed from GoogleMaps, the same six rays, and how much it was built on the history? And yet... I. E. Kleinenberg found the news about Vasilievsky island, lying in the mouth of the Neva river, in a Livonian document of 1426, strange, isn't it?

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/9.jpg#resized)
-->

![](http://2.bp.blogspot.com/-AvHOUghwIqk/UA6phsIuWbI/AAAAAAAAB7E/LdoUqUhwFUM/s640/9.jpg#resized)

**Russian:**

Написано, что строительство закончено в 1780 году, а в 1785 часть стен облицевали гранитом, во как, а на картах 1720 года все стены есть.

**English:**

It is written that the construction was completed in 1780, and in 1785 part of the walls were faced with granite, in France, and on the maps of 1720 all the walls are there.

**Russian:**

План Петропавловской крепости

**English:**

Plan of the Peter and Paul fortress

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/070b0c9d1e81.jpg#resized)
-->

![](http://i044.radikal.ru/0802/55/070b0c9d1e81.jpg#resized)

**Russian:**

Отчётливо повторяет собой все остальные крепости, как будто они выполнены по одному сценарию. За образец города-крепости была взята итальянская крепость эпохи Ренессанса в форме звезды 1500-х годов. ***[крепости в форме звезды](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_3848.html)*** [Ниенша́нц](http://ru.wikipedia.org/wiki/%CD%E8%E5%ED%F8%E0%ED%F6)

**English:**

It clearly repeats all the other fortresses, as if they were built according to the same scenario. An Italian Renaissance fortress in the shape of a star in the 1500s was taken as a model of the fortress city. [Star-shaped fortresses](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_3848.html) of [Nyenskans/the Nienschanz](http://ru.wikipedia.org/wiki/%CD%E8%E5%ED%F8%E0%ED%F6) 

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/original.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/original.jpg)
-->

[![](http://1.bp.blogspot.com/-2BIiGN40r6M/UBEF1ifZeMI/AAAAAAAAB_A/lzXlSvMG1GM/s1600/original.jpg#clickable)](http://1.bp.blogspot.com/-2BIiGN40r6M/UBEF1ifZeMI/AAAAAAAAB_A/lzXlSvMG1GM/s1600/original.jpg)

Nienschanz - Russified from 'Nyenskans' (the Swedish 'Nyenskans', Finnish 'Nevanlinna', Russian 'Kantsy') - Swedish fortress. It was the main fortification of the city of Nien (Sweden. Nyen) on the Okhta promontory on the Bank of the Neva river, at the mouth of the Okhta river on its left Bank, near the modern Krasnogvardeyskaya square in St. Petersburg. The fortress was founded in 1611 on the land that was taken from Russia, on the site of the Russian commercial settlement of Nevsky Gorodok (Neva Estuary) to control the Izhora land, called Ingermanland by the Swedes, and to control the waterway up the Neva river. Literally translated as Nevsky (Nyen) trench (skans).

Here is a detailed map of the star-shaped forts scattered across Europe:

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/europe_17401.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/europe_17401.jpg)
-->

[![](http://4.bp.blogspot.com/-s0qxntYDKfk/UYoBvxXR2eI/AAAAAAAAC0Q/v3RvJtpI-ww/s1600/europe_1740+(1).jpg#clickable)](http://4.bp.blogspot.com/-s0qxntYDKfk/UYoBvxXR2eI/AAAAAAAAC0Q/v3RvJtpI-ww/s1600/europe_1740+(1).jpg)

All these fortresses are the remains of former FORTS and fortifications built according to the same plan, and in ancient times.

And even in the depths of the earth, right under the Foundation of decayed churches and temples, you can find the following:

*[content now unavailable]*

<!-- Local
![](Where is the city from Chapter 1 Old maps_files/4n14-032.html)
-->

![](http://livehistory.ru/media/kunena/attachments/327/4n14-034.jpg#resized)

How the text reminds of the times of Alexander Nevsky, Ivan the Terrible...

[Read more here, link: THE ROMANOVS, WHAT WAS BUILT BEFORE THEM, EITHER DESTROYED AND COMPARED TO THE GROUND, OR APPROPRIATED.](http://ladaria.livejournal.com/988019.html)

Examples of falsification of history are waiting for us at every step. For example, an artist of the 19th century draws the history of St. Petersburg...

Year 1799:

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1799.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1799.jpg)
-->

[![](http://2.bp.blogspot.com/-p-P8qidRtsM/UZnjaoyF4sI/AAAAAAAADAM/B9yC8Eiuwxg/s1600/1799.jpg#clickable)](http://2.bp.blogspot.com/-p-P8qidRtsM/UZnjaoyF4sI/AAAAAAAADAM/B9yC8Eiuwxg/s1600/1799.jpg)


Year 1756:
<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1756.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1756.jpg)
-->

[![](http://2.bp.blogspot.com/-2Z8q8CTJvkw/UZnjvo4m60I/AAAAAAAADAU/H2mg_jVXLH8/s1600/1756.jpg#clickable)](http://2.bp.blogspot.com/-2Z8q8CTJvkw/UZnjvo4m60I/AAAAAAAADAU/H2mg_jVXLH8/s1600/1756.jpg)

Year 1738:
<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1738.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1738.jpg)
-->

[![](http://4.bp.blogspot.com/-s3Fpy9QqI9o/UZnkAL6Z1_I/AAAAAAAADAc/R74e_8_9rII/s1600/1738.jpg#clickable)](http://4.bp.blogspot.com/-s3Fpy9QqI9o/UZnkAL6Z1_I/AAAAAAAADAc/R74e_8_9rII/s1600/1738.jpg)

Year 1725:

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1725.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1725.jpg)
-->

[![](http://4.bp.blogspot.com/-WVQe5Zm-yV0/UZnkWf3wQdI/AAAAAAAADAk/30Y1Rep1tU4/s1600/1725.jpg#clickable)](http://4.bp.blogspot.com/-WVQe5Zm-yV0/UZnkWf3wQdI/AAAAAAAADAk/30Y1Rep1tU4/s1600/1725.jpg)

Year 1705:

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1705.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1705.jpg)
-->

[![](http://1.bp.blogspot.com/-7Uy-L61FkB8/UZnke5STF5I/AAAAAAAADAs/7jG5V7-zNeg/s1600/1705.jpg#clickable)](http://1.bp.blogspot.com/-7Uy-L61FkB8/UZnke5STF5I/AAAAAAAADAs/7jG5V7-zNeg/s1600/1705.jpg)

The fictional story is ready, now all this is turned over and the countdown begins with the smallest date. It is said that it was so, so it was! 

Here is another map for you... note the date: 1698.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/535554606.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/535554606.jpg)
-->

[![](http://1.bp.blogspot.com/-Xl-vvWeVACY/UgCxCacYn2I/AAAAAAAAEAc/saOY6Kq2_SA/s1600/535554606.jpg#clickable)](http://1.bp.blogspot.com/-Xl-vvWeVACY/UgCxCacYn2I/AAAAAAAAEAc/saOY6Kq2_SA/s1600/535554606.jpg)

This is an official history commissioned for textbooks, but these maps contradict other maps, such as Erik Nilsson Aspegreen's 1643 map:

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/125bcd6873ef.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/125bcd6873ef.jpg)
-->

[![](http://4.bp.blogspot.com/-bl2Ft9BjoL8/UZnqmn-TquI/AAAAAAAADA8/qYNR8kSwLSs/s1600/125bcd6873ef.jpg#clickable)](http://4.bp.blogspot.com/-bl2Ft9BjoL8/UZnqmn-TquI/AAAAAAAADA8/qYNR8kSwLSs/s1600/125bcd6873ef.jpg)

In ancient Russian and Scandinavian written sources up to the Peace of Orekhovets in 1323 in the Neva region, on the Baltic coast and in the Ladoga region, 42 settlements are noted. Of these, 32 are Novgorod settlements (in size and social scale from the capital city to the monastery village), 6 cities "in Chudi", 1 city in Latgallia, 1 city in the Livonian land, 1 German city. According to the Treaty of Orekhovets, the state border between the Novgorod Republic and Sweden moved to the River Sister.

Based on historical data, we can say now that on the territory of the future Greater St. Petersburg during the 15th - end of the 17th centuries. 900 - 1000 settlements were stable, united by hundreds of kilometers of roads. Many of these settlements became "buds" of the creation of St. Petersburg settlements, ensembles and building blocks. Even under Peter I, the borders of St. Petersburg included the territory of at least 55 villages of the pre-Petrine period, and the suburban area united more than a hundred more previously existing villages, manors, villages and hamlets. Modern St. Petersburg and the territories under its administration cover more than 200 ancient settlements. Read more here: [Nests of Peter the Great.](http://www.spbvedomosti.ru/article.htm?id=10299224@SV_Articles)

This area has always been quite densely populated, and the notes of the cartographer passing by should not be neglected, as well as this map of the early 17th century.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/002_f4282ca08f85.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/002_f4282ca08f85.jpg)
-->

[![](http://4.bp.blogspot.com/-VRY2D92_W5I/UZnruASVR2I/AAAAAAAADBM/Fe4KKefgp7w/s1600/002_f4282ca08f85.jpg#clickable)](http://4.bp.blogspot.com/-VRY2D92_W5I/UZnruASVR2I/AAAAAAAADBM/Fe4KKefgp7w/s1600/002_f4282ca08f85.jpg)

Here is another plan of the city with the Nieschanz fortress, dated 1643.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/7ff5d5f097a9.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/7ff5d5f097a9.jpg)
-->

[![](http://2.bp.blogspot.com/-eINyD6NGo7c/UZntZSWJ5TI/AAAAAAAADBc/upCSmg--3GI/s1600/7ff5d5f097a9.jpg#clickable)](http://2.bp.blogspot.com/-eINyD6NGo7c/UZntZSWJ5TI/AAAAAAAADBc/upCSmg--3GI/s1600/7ff5d5f097a9.jpg)

And so, the Nieshants fortress, founded in 1611.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/b805e2d6a496.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/b805e2d6a496.jpg)
-->

[![](http://4.bp.blogspot.com/-wEuucq4BAfo/UZnt_cF9eDI/AAAAAAAADBk/ZtdBMwRo96M/s1600/b805e2d6a496.jpg#clickable)](http://4.bp.blogspot.com/-wEuucq4BAfo/UZnt_cF9eDI/AAAAAAAADBk/ZtdBMwRo96M/s1600/b805e2d6a496.jpg)

Mouth of the Neva River, city of Nyen with its surroundings, late 17th century.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/1698.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/1698.jpg)
-->

[![](http://3.bp.blogspot.com/-JEw0wwX62bs/UZn0AKSoI9I/AAAAAAAADB0/2AOVzpXZ8CA/s1600/1698.jpg#clickable)](http://3.bp.blogspot.com/-JEw0wwX62bs/UZn0AKSoI9I/AAAAAAAADB0/2AOVzpXZ8CA/s1600/1698.jpg)

According to Swedish historians, in 1691 there was a catastrophic flood on the Neva. The water rose to Nyen seven and a half meters above the ordinary. It was the highest of the recorded water heights for the entire existence of Nyen, a lot of structures on the coastal part went under the water, and were subsequently abandoned.

Wonderful maps of the early 18th century, they were sent to me by a history lover under the pseudonym Otets Sergiy.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/a_002.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/a_002.jpg)
-->

[![](http://2.bp.blogspot.com/-pvnjUY5zkZM/Uk6xZg3tuOI/AAAAAAAAEps/oJcn8_koyfY/s1600/%D0%9A%D0%B0%D1%80%D1%82%D0%B0+%D0%A1%D0%9F%D0%91+%D1%81+%D1%82%D0%B0%D1%80%D1%82%D0%B0%D1%80%D1%81%D0%BA%D0%BE%D0%B9+%D1%81%D0%BB%D0%BE%D0%B1%D0%BE%D0%B4%D0%BE%D0%B9.jpg#clickable)](http://2.bp.blogspot.com/-pvnjUY5zkZM/Uk6xZg3tuOI/AAAAAAAAEps/oJcn8_koyfY/s1600/%D0%9A%D0%B0%D1%80%D1%82%D0%B0+%D0%A1%D0%9F%D0%91+%D1%81+%D1%82%D0%B0%D1%80%D1%82%D0%B0%D1%80%D1%81%D0%BA%D0%BE%D0%B9+%D1%81%D0%BB%D0%BE%D0%B1%D0%BE%D0%B4%D0%BE%D0%B9.jpg)

Pay attention to how the map is signed.

This is still the first map of Peter, of those that came across to me, with the TARTAR settlement.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/a.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/a.jpg)
-->

[![](http://2.bp.blogspot.com/-AEh_5F0bwmI/Uk6xaxuUgfI/AAAAAAAAEp0/M0cCE2_Xdsw/s1600/%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D0%B8%D1%81.jpg#clickable)](http://2.bp.blogspot.com/-AEh_5F0bwmI/Uk6xaxuUgfI/AAAAAAAAEp0/M0cCE2_Xdsw/s1600/%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D0%B8%D1%81.jpg)

And here is the 1703 Petropolis. Interesting, right? They only just got together to build it, and it has already been built.

<!-- Local
[![](Where is the city from Chapter 1 Old maps_files/PiterMapNew21.jpg#clickable)](Where is the city from Chapter 1 Old maps_files/PiterMapNew21.jpg)
-->

[![](http://4.bp.blogspot.com/-_NRo_c6E_L8/Uk6ztUtl1nI/AAAAAAAAEqA/nGUM3Gocf0o/s1600/PiterMapNew21.jpg#clickable)](http://4.bp.blogspot.com/-_NRo_c6E_L8/Uk6ztUtl1nI/AAAAAAAAEqA/nGUM3Gocf0o/s1600/PiterMapNew21.jpg)


[Petropolis in 1744](http://www.spetersburg.net/2011/08/blog-post_944.html), what is the scale, what is the speed of construction, how many neighborhoods, canals and communications?

Interesting articles on the topic: [Stupid Petersburg](http://igor-grek.ucoz.ru/news/piter_bad/2013-05-12-350) and [Irreplaceable Petersburg](http://igor-grek.ucoz.ru/news/piter_good/2013-05-15-352).

An entertaining point of view on cartography of that time: [Are all old maps of St. Petersburg fake?](http://igor-grek.ucoz.ru/news/fals_map_piter/2013-07-26-374)

And one more thing: [The biggest scam of the 19th century.](http://igor-grek.ucoz.ru/index/1812/0-14)

Almost from the very moment the city was founded, a legend began to take shape about St. Petersburg as a ghost town, about its "unreality" and lack of connection with the history of the country. In 1845, in the article "Petersburg and Moscow" V.G. Belinsky wrote:

> "People used to think of St. Petersburg as a city built not even on a swamp, but almost in the air."

The story of the removal of the capital of the Russian state practically outside the borders of the state itself seems very strange for that time. Even at the beginning of the 19th century, I'm not even talking about the 18th century, Petersburg was categorically isolated from Muscovy, there was not a single normal direct waterway (only the unsuccessfully made Vyshnevolotsk system, somehow working to descend to St. Petersburg). In those days, of course, there were no planes, no railways, no highways, only waterways along rivers, and short land sections - "drags" between river ways. And if there are no normal communication routes along which goods, troops, etc. can move, then there is no transport connectivity, without which there can be no statehood. Couriers with decrees can get there, but without the economic and power components these decrees are worthless. The country is huge and the capital in general is in the middle of nowhere, doesn't it seem absurd to you? Until the 19th century, the main city controlling the transport hubs of the Moscow-Smolensk Upland at that time was the "key-city" Smolensk, located in the upper reaches of the Dnieper,
where a chain of portages began, connecting the river routes "from the Varangians to the Greeks" and "from the Varangians to the Persians. "at the intersection of trade routes from the Dnieper, Zapadno-Dvinsky, Volkhov, Volzhsky and Oka river basins. And only in the 19th century began a large-scale construction of direct waterways from St. Petersburg to the Volga: the Mariinsky, Tikhvinsky and the reconstruction of the Vyshnevolotsk water systems. 

When copying an article, do not forget to indicate authorship. 

The address of the full version is here: ["Where is the city from? (By ZigZag)"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)


#### 39 comments:

1. Anonymously [February 13, 2014 10:33 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1392280395854#c5835457931127418116)

 Have you ever wondered how the city was destroyed?

 [Answers]
 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[February 13, 2014 1:02 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1392289336866#c2666564399054210643)

 Do you think the real information has survived? I am familiar with the official Bayer-Miller-Schlötzer point of view)))

 2. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[February 13, 2014 1:03 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1392289417346#c3056007275130131839)

 Here is a more detailed version:
 [http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

2. [Unknown](https://www.blogger.com/profile/12192322151514616808)[April 7, 2014 01:05 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1396821938655#c2348033594994177562)

 I just admired the panorama of the city of St. Petersburg on Yandex. If you tick off the panorama from the air and look at the Artillery Museum point, then to the left of the bright red roof there is a construction site. Perhaps I'm exaggerating, but an excavator is digging out the foundation of an old building there, or something similar . I would like to hear your opinions.


 [Answers](javascript:;)
 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[May 14, 2014 1:05 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1400061911068#c3140042953516315991)

 Alas, I cannot get there. If one of the blog readers has a camera and lives and visits St. Petersburg, I will be glad of any interesting photos. Thanks.

3. Anonymously [August 9, 2014 08:09 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1407560948189#c3768326601699454391)
 Peter may have suffered a natural flood. As a result, it was rebuilt.

 [Answers](javascript:;)
 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[August 9, 2014 11:03 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1407614605055#c2315701946757672735)

 [http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/16.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/16.html)


4. Anonymously [September 8, 2014 02:54 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1410134063232#c1434990791513941589)

 [http://www.raremaps.com/gallery/enlarge/27902](http://www.raremaps.com/gallery/enlarge/27902) Have you seen such a map? By the way, there are many of them. They say Magnus 1567. It's a pity the image cannot be attached. Take a close look at Ladoga and the sleeve that connects it to the Gulf of Finland. It's very interesting about Peter. Good luck!

 [Answers](javascript:;)
 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[September 8, 2014 2:08 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1410174506403#c1276378320844493794)

 Many thanks
 

6. [Unknown](https://www.blogger.com/profile/07851975869066470831)[January 8, 2015 00:00](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1420668053094#c3515921236456669764)

 I am from the city of Taganrog, according to the official history, it was founded by Peter-1 in 1698. looking at the layout of the city (its historical part), there are big doubts that it was founded by Peter the Great. ideally flat layout https://www.google.ru/maps/@47.2090577,38.9233675,2451m/data=!3m1!1e3

 7. [Victor Kobeikin](https://www.blogger.com/profile/16477473332320867691)[November 8, 2015 6:45 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1447001120062#c5599963723702055571)

 Maybe Peter was left at the start of the Little Ice Age in 1400? And after 300 years it got warmer and began to settle down again. But then the question is: why are they so carefully hidden?

8. [Victor Kobeikin](https://www.blogger.com/profile/16477473332320867691)[November 8, 2015 6:45 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1447001139400#c5784295712854268134)

 Maybe Peter was left at the start of the Little Ice Age in 1400? And after 300 years it got warmer and began to settle down again. But then the question is: why are they so carefully hidden? 

9. Anonymously [November 22, 2015 3:03 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1448197421637#c6209157221597735612)

 Dear ZigZag. Once again I invite you to an open face-to-face discussion: thesis, arguments, conclusion. Let's take a look at each object and version. While I see a lot of dislocations and little use. You there, for example, saw the Savior and the Mikhailovsky Palace in the image of the church in 1714 (God-received. Semeon and the prophet. Anna) and ... Time, place and tools of argumentation of your choice. Anglinov Konstantin (available in VKontakte), my email. address: anglinov13@mail.ru. Without pathos, yelling ... let's figure it out.

 [Answers]
 
 1. Anonymously [November 28, 2016 5:17 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1480346223780#c3665515993320232108)

 Well, what about the challenge? a year has passed))


10. Michael[December 9, 2015 12:38 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1449614298023#c4321765236757816573)

 I wonder if the challenge will be accepted? That's a lot to say. :)

11. [ELIPS](https://www.blogger.com/profile/08712661121634630106)[December 13, 2015 3:49 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1450014564140#c6700377941748463743)

 Yes, I wonder why Napoleon went to Moscow and not to St. Petersburg (the capital then).

12. Anonymously [January 11, 2016 5:11 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1452525066425#c8106370506561089210)

 I read Leo the Thin and then came to this page.The question arose, but in Western Europe there are analogues ofsuch a decoration of the banks of the river, as in St. Petersburg of the time to which all this is attributed to the official. history? After all, this is not only cut out and put, this is an engineering structure ... It's also very expensive ... and besides, I read it somewhere, it increases the risk of floods, since the water has nowhere to go ...

13. Anonymously [March 12, 2016 9:21 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1457810482416#c811673352651687346)

 Lyudmila: In the book from the "Library of Russian Folklore" series, in the "People's Prose" volume, there is such an interesting phrase in a story recorded by Yakushkin from the words of a carrier across the Volkhov River in Novgorod - "Ivan the Terrible in Novgorod", p. 81, publishing house " SOVIET RUSSIA ": 
 
 > Even for our grandfathers, even Peter was not under our possession, there was Tsar Ivan Vasilyevich the Terrible.

 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[March 13, 2016 1:18 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1457867931678#c715987653466262490)

 thanks

14. Anonymously [March 30, 2016 07:14 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1459311253179#c6483495469667810719)

 Can you please tell me where the map with the location of star cities in Europe came from? 
 
 Let me explain. I live in Kherson myself. I have been interested in alternative history for a long time. Not everything is smooth with the history of Kherson either. Kherson was founded officially (in any case, it received its modern name) in 1778, on the site of the temporary fortification of Aleksndr-Shats, which arose in 1737. Built by Ekaterina and Potemkin in Russia. Moreover, the fortification was in the shape of a star, and after the construction of the Kherson fortress, the construction of a fortress Star in a star turned out. And here is a map of 1740 with the proper name of Kherson on the territory of the Ataman (Ottoman) Empire ????

 [Reply](javascript:;){.comment-reply}[Удалить](https://www.blogger.com/delete-comment.g?blogID=8090482693945194928&postID=6483495469667810719)

 [Answers](javascript:;)
 1. [ZigZag](https://www.blogger.com/profile/07680738609904461754)[April 2, 2016 11:57 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1459587440277#c8078697258035131883)

 Here is from this magazine:
 
 http://chispa1707.livejournal.com/

 [Удалить](https://www.blogger.com/delete-comment.g?blogID=8090482693945194928&postID=8078697258035131883)

 [Ответы](javascript:;)

 2. Anonymously [April 5, 2016 7:15 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1459872931312#c8836291228988111020)

 Thank you, we will look there. In general, the map is strange -
 is there also Sevastopol there?


15. [Unknown](https://www.blogger.com/profile/09225611384342638344)[May 19, 2016 11:35 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1463690151109#c7475891966617083521)

 The city was cleared after the flood of 1690-1693.Before the flood, the city was the center of the world religion of the Sun, where Anty's height of 250-295 cm (doors and ceiling heights) and ordinary people coexisted - the left side was called St-Nicolas and the right side was St Mihail - the religion of the sun wore planetary character and throughout the entire length of the Empire (including the whole of Europe, etc.) During the construction, Casting was widely used (COLUMNS AND ALL MEGALITS) Since then, we have only the words shape-shifters that now have abusive meanings - Shit, Shit, Shout, Fool, and so on, which had a root RA-TEMPLE, BROTHER, Hurray, HEALTHY, HOLIDAY ... AND STILL THE COSMOS DOESN'T EXIST IN THIS UNDERSTANDING - IN WHICH WE ARE TEACHED - OK about that later ...

 1. Anonymously [July 4, 2016 10:36 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1467617762352#c4677876896784717174)

 Vadim about space, please tell us!


 2. [Unknown](https://www.blogger.com/profile/05863250175309414752)[February 10, 2018 01:42 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1518219750048#c7614544366542363682)

 It is very similar to the truth of Anta's height 250-295 cm. It is enough to look at the size of those stones that were laid in the walls around the world (100-300 kg each). For today's bricklayer, this is an impossible task. And a bricklayer must deal with a stone mostly without helpers. 
 
16. [Oleg Griva](https://www.blogger.com/profile/13836990734676811487)[July 23, 2016 11:02 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1469304130133#c4182524666373696215)

 Presumably the flood in the 90s met with the retired guys from Sevastopol. They did interesting research. The absence of cultural words in the territory between the Carpathians and the Urals was taken as a basis. According to their theory, there was a change in the tilt of the Earth's axis. They cited historical facts when they fled from the water to the mountains on horseback. There were broken trees in the Carpathians, brought in by oxen and mixed soil. The fauna of the Baltic Sea and the Black Sea was studied, and in the Black there is what is in the Baltic, but in the Baltic there is not what is in the Black. In the same place, they calculated the outline of the land before the deviation of the axis and assumed the presence of an isthmus of about 1500 km between North America and Kamchatka. There is also a film (did not have time to copy the address) about the flood around 1730-1760 and examples of the excavation of cities 2 meters or more are shown.
 The consequences of the flood are also present on the Black Sea coast: mooring rings in the rocks at an altitude of 100 m, the ruins of Beliaus are influenced by forces from the north, and the building blocks themselves have dimensions that are not comparable with the dimensions of bricks and stones of our days. The same is in the foundation of Kalos-Limen. (Black Sea region) Question: who needs to rewrite history and what is it hiding from the inhabitants of planet Earth?


 [Answers](javascript:;)
 1. [Unknown](https://www.blogger.com/profile/14524053076043835610)[August 3, 2016 3:15 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1470226525703#c8245041494261111998)

 so I'm wondering why hide the flood and the displacement of the earth's axis? what negative can this information give?


 2. Anonymously [February 19, 2018 9:34 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1519068847249#c6913317886258635959)

 For MsKsuta ... if you're interested, I can tell you who to address questions ...
 Try to contact the Jesuits ...

 3. [Unknown](https://www.blogger.com/profile/08120590410340551282)[May 17, 2018 3:13 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1526559214185#c1892849418861502157)

 Read Shemshuk and the school of Priests there is everything about giants and about intergalactic wars and about the seizure of Semli (not the Earth, but from the SEED) with pesinagami and dogglaves Perhaps there were French in St. Petersburg and went to Moscow to conquer it hide that the Russian Empire was already destroyed by the invaders because Nikolai This is GeorgV There were other technologies - // magical // energy from the air-ether caloric flew on carpets-planes and flying ships (as in a cartoon) from a black birch (stupa there), which is now being exterminated and already flew into space there is even a brochure to provide troops for flights to the Moon and Mars, the Priobrazhenskie regiments were transformed, they secretly fought. You can also watch Little Tales on YouTube. I'm on Facebook. They hide EVERYTHING, they lie everywhere. 
 
 For the fall of 18, it is planned to ki p and w It is necessary to read the Requirements of the Old Orthodox Gods to save Semli
 
 (He ate at a hundred un there)
 
 The Kid has it in the rollers - DEMANDS
 
> Do you want it or not?  Everyone decides for himself
 
 You can also calculate your real name by month and by year and by the name of the father and mother (protective) it will come in handy Names of Shemshuk ALL GOOD! It is now time Goddess TANI17 / 05/2018 signs do not put long-should be in English to switch all the way

17. [Unknown](https://www.blogger.com/profile/11846498373735448640)[September 8, 2016 4:53 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1473342812061#c2212810979957141094)

 On the atlas of Ortelius - Atlas Ortelius 1570 - 45. 461. Atlas Ortelius 1570 - 45. 46.
 
 Everything is clear and understandable. On the site of Petersburg, the city of Ilz (in our opinion, Ilion). Instead of Lake Laozh and the Gulf of Finland, the Onega river (it went underground but serves as a source for the Peterhof fountains). Well, Onega flowed into the Baltic in the Sosnovy Bor region, beyond the Oreshek fortress (present-day Orienenbaum) - the essence of ancient Troy. Neva is just a system of lakes and dams diverted from Onega. And here is the atlas itself https://yadi.sk/d/qQgCndG3uuJ6Q

 1. Anonymously [February 19, 2018 9:05 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1519067142856#c807692270957056674)

 This is very similar to the truth ... especially considering the books of E. Blavatsky ...


18. Anonymously [October 7, 2016 03:35 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1475800508866#c8409236691266583436)

 Petersburg - stands on the site of the ancient city of Atlantis, bordering on Hyperborea. What is left there before the times of Peter the Great is minuscule. Is that the layout and foundations. Everything was destroyed by a flood, and then by glaciers and everything was drowned in swamps. There was a great glaciation here in the north. This Baalbek survived in the deserts, but here, alas ... The Alexandrian column was made by Atlanteans, but it was lying in a swamp, and it was raised under Peter, and the king-bath in the Babolovsky palace was hidden from the masses, and it only surfaced after the Germans bombed the palace during the war ...


19. Anonymously [October 7, 2016 9:06 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1475863613254#c2197477693400520440)

 Look around the world scattered so-called "Star Fortresses" - these are the colonial outposts of the Atlanteans in the form of pentacles and Stars of David, which climbed into all lands, like the United States is now. And modern scientists now rack their brains and deny everything. But 12,000 years ago there was a serious confrontation between the Atlanteans and the Hyperboreans, the Atlantes wanted domination over the world and used tectonic weapons, they wanted to win the war with one blow like Hitler "Blitzkrieg", but they themselves paid, Atlantis split and sank in one day, but also Hyperborea not immediately, but also sank - look what a terrible scar remained on the earth from this terrible disaster: the "Mid-Atlantic Ridge"

 1. [Unknown](https://www.blogger.com/profile/17664086315217032402)[December 11, 2016 12:18 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1481408280321#c2834854711326355078)

 +1,500


20. Anonymously [November 5, 2016 7:54 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1478368449136#c6400576694693482935)

 On the stone, in Old Russian, 7177 (ZROZ) is indicated for December 7 (W) day. This is 1669.
 The letter style of dating was used almost until the 20th century,
 which is clearly seen in the antique books of the 17-19 centuries.
 And even in 1912, occasionally, but they wrote in this way In fact,
 the reforms of Peter the Great did not take root in Russia after
 changing the Calendar. Failed to eradicate this tradition.
 Here is the "cheat sheet"
 [http://t.michael.beliu.name/wp-content/uploads/2012/11/zifiry.jpg](http://t.michael.beliu.name/wp-content/uploads/2012/11/zifiry.jpg)


21. [Misha](https://www.blogger.com/profile/14273970831680773255)[April 6, 2017 1:17 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1491473861065#c6722115430579967037)

 The author himself initially changed the concepts. Presenting
 development plans of various architects as Real maps. By that time,
 there were no technologies to create a real map, and of course all
 of them were subject to adjustments by the authorities, something was drawn, something was removed. From here all the cheese is boron.

22. Anonymously [March 3, 2018 6:35 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1520094956011#c5082078525307903286)

 I compared the plans of the cities of St. Petersburg and New York and this is the beauty of one-to-one planning structure, even Nevsky is in America, only the name is different. It is not even worth talking about the identity of architecture in domed structures like Isaac's "Cathedral" (Capitol, St. Peter's Cathedral in Rome, etc.), designs patented in 1961 in the USSR are applied. There are two monuments to Peter (the bronze horseman and another in the Roman style) of such a breed of horses now. There was a trend in painting - drawing ruins. So there is a whole carriage of such "Petrushes". For the construction of buildings, we get geodesy and here is the regularity of 3-8 meters of clay, where is what, and below all you want is a fertile layer, quicksand and that's the charm of bearing soils. They are silent about the last flood. From the Urals to the Far East, such star-shaped fortresses were apparently not visible. And about the fact that all the lakes there are like a blueprint round - the remains after the bombing. But the question was bombed, the lakes are deep. In some places they started to swamp. But who knows the real name of Pugachev? Well, as for real maps, in 1976 she was holding a drawing in her hands - a blueprint dated to the 17th century, made by the Ural plant, made by a computer method, but they say that there was nothing but a goose feather. In the net there is a project carried out by Montferand - well, just a shame. Isaac he did not design unambiguously. Anyone who is familiar with architectural design will immediately understand that history has been replaced for us. Genuine maps could not at that time make regular layouts so competently (I mean all these "great" designers) everything was done before them. Such gostovsky granite blocks for facing embankments will now be made for a very long time, but here even all minor rivers that allegedly did not even enter the city boundaries were covered. A lot of things "Petrusha" heaped up - introduced serfdom, but that's why serfdom was not in the Urals and Siberia only after 1775, when Scythia fell, Catherine learned that there was some kind of Siberian kingdom that she abolished. And even then they could not plant anything the freemen remained. The past of our country was rewritten by the Germans to destroy Russia. when Scythia fell, Catherine learned that there was some kind of Siberian kingdom that she abolished. And even then they could not plant anything the freemen remained. The past of our country was rewritten by the Germans to destroy Russia. when Scythia fell, Catherine learned that there was some kind of Siberian kingdom that she abolished. And even then they could not plant anything the freemen remained. The past of our country was rewritten by the Germans to destroy Russia.


23. [Unknown](https://www.blogger.com/profile/15558446858152493000)[December 18, 2018 3:46 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/1.html?showComment=1545140804952#c30060587731726506)

 well ... well ... they brought everyone into an amazing insight-disbelief that the "city of Peter" was before Peter ... but "spoons are there?" .. who and when built all this ??? .. in doubts it is possible to "get" to the ancient stone cities of Europe, fortunately, the architecture is similar .. and when and who also built them? Again, let's start blaming the Hyperboreans? until you get to the bottom of what culture stood at the origins.
 
 © All rights reserved. The original author retains ownership and rights.
