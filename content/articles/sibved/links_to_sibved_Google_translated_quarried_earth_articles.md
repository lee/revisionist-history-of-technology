title: Evidence Earth has been Quarried - Sibved
date: 2021-07-23 00:03
modified: 2022-04-09 20:03:33
category: 
tags: quarried Earth; links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md


[Ancient Quarries of the Crimea](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/233087.html)

[Ancient Quarries or Winding Rivers](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/252808.html)

[Ancient and Modern Quarries - Part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/242860.html)

[Ancient and Modern Quarries - Part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/253594.html)

[Bohol Island Hills - Philippines](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/230839.html)

[Canyon de Chey - Ancient Quarry](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/242184.html)

[Castles and fortresses on ancient dumps](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/249060.html)

[Caves and Mines](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/248024.html)

[De-watering, Sieving and Megalithic Technology](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/254293.html)

[Deserts, Rivers, Lakes and Volcanoes of Antartica](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/228304.html)

[Pyramidal Hills and Terra-cones](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/251761.html)

[Far Eastern Pyramidal Hills](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/232560.html)

[Foreign Inclusions in Granite](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/211203.html)

[Gold Deposits and Space Quarries](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/252153.html)

[Hills of Rapeseed Fields in China](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/179866.html)

[Hobbit catacombs, stone cart ruts and ancient quarries of Spain](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/238639.html)

[Hydromonitors, Quarries and Canyons](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/233592.html)

[Indications of Artificial Hills and Megaliths](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/216847.html)

[Industrial landscapes of antiquity continued](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/187626.html)

[Kamchatka](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/253099.html)

[Kondur Ring Mountain Massif](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/180882.html)

[Formation of Lignite and Hard Coal](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/224832.    html)

[Layers in Lignite Deposits](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/200768.html)

[Megaliths and Uranium Mines](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/246927.html)

[Mining Systems with Ore Caving and Failures](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/248299.html)

[Moldovan Tunnels](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://5elena4.livejournal.com/22319.html)

[Mounds and Maidens](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/240073.html)

[Ozy and Zmievi Valleys](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/240203.html)

[Rock Processing Remains from the Planet's Past](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/203674.html)

[Scotland's Tap O'Noth - Ruined Fortress or Slag Waste from Metal Refining](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/247508.html)

[Smoothed Mountains](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/208425.html)

[Stone Balls - Ancient Metal Refining Slag](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/260451.html)

[Stone cylinders in Di Cusa cave, Sicily](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/202340.html)

[The Putorana Plateau - Traces of a Giant Quarry](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/235195.html)

[Turkey and Armenia - Hills and Volcanoes](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/232376.html)

[Uluru Rock - Paste Dump or Mud Volcano](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/251595.html)

[Underground Metal Leaching and Megaliths as Waste from Paste Thickening of Rocks](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/245637.html)

[Volcano Valley - Eastern Sayan Mountains](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/241994.html)

[Who Poured Vesusius?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/232891.html)

[Zion National Park, USA](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/242454.html)
