title: Starforts - Evidence and Purpose
date: 2021-07-23 00:02
modified: 2022-04-09 20:13:32
category: 
tags: architecture; links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md

[Aspen Grove Star-fort](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/195142.html)

[Star-forts - Fuel Stations](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/194409.html)


