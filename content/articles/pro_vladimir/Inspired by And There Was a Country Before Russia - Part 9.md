title: Inspired by "And there was a Country before Russia". Part 9
date: 2013-12-17
modified: Fri 02 Apr 2021 20:08:25 BST
category:
tags: architecture, technology
slug:
authors: Vladimir Mamzerev
summary: All of the conventional historical structure is dismantled by this material.
status: published
local: Inspired by And There Was a Country Before Russia - Part 9_files
originally: По мотивам «И была Страна до России». Часть 9. - pro_vladimir — LiveJournal.html

### Translated from:

[https://pro-vladimir.livejournal.com/8535.html](https://pro-vladimir.livejournal.com/8535.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

**Russian:**

Взор скользил по сети в поисках зацепок. Точнее, много что цепляло, но картинка так и оставалась несобранной. В фокус попадали то одни, то другие элементы. Петли запросов боронили сеть, то увеличивая величину ячейки поиска, то сужая её до самого минимума. «издали» прорисовывались контуры одного, второго, третьего. При попытки приблизится, были видны отдельные элементы. Что-то затерлось. От чего остались копии с копий, что и разобрать там чего, в меру своего понимания, не очень-то и получалось. Даже порой, глядя на оригинал какого предмета, накрывало чувство некой досады, что смотришь как на раскрытую страницу учебника по высшей математике. И одновременно понимание, что просто рассматриванием схем конструкций, предметов, сооружений, осознать принцип работы наследия былого, не удастся.

**English:**

Our gaze slid around the Internet looking for clues. We had a lot of pieces, but the picture was still unassembled. The focus was on some elements or other. The loops of inquiry harrowed the Web, one moment increasing the scope of the search terms, the next narrowing it to the minimum. From afar, the contours of one, two, three pieces emerged. When trying to get closer, we could see the individual elements. Something was rubbing off. What remained were copies of copies, which, to my mind, doesn't work out very well. Sometimes, looking at the original for some subject, there was a feeling of some annoyance that one was looking at the open page of a higher mathematics textbook. And at the same time the understanding that to understand the heritage of the past by simply considering the schemes of structures, objects, structures, would be impossible.

**Russian:**

Назначения, параметры, кто, как и зачем. Понять, разобрать, а после возможно и собрать. А если еще и заработает, то и применить. Тяга к поиску проснулась не сразу. Но вот он, этап более плотного уложения ранее пройденного из артефактов текущего и прошлого.

**English:**

Assignments, parameters, who, how and why. Understand, disassemble, and then possibly assemble. And if it also works, then apply it. The urge to search didn't wake up right away. But here it is, the stage of denser stacking previously passed from the artifacts of the current and past.

**Russian:**

От прошлого достались предметы, строения, легенды, мифы разной степени давности. Официальная история и куча попыток мыслить иначе, тех, кто сомневается. Они даже может и не всё знают как в учебниках написано, но их они в процессе поиска перечитывают. То есть, идет этакое сложение, версий, мнений, гипотез. И прикладывание всего этого в глобус миропонимания. Этакая спираль событий причинно следственных связей, что порой довольно лихо закручена.

**English:**

Objects, structures, legends, myths of varying degrees of antiquity have been taken from the past. Those who doubt official history make a lot of attempts to think differently. They may not even know everything about textbooks, but they re-read them while searching. I mean, there's this combination of versions, opinions, hypotheses. And putting it all into the sphere of world understanding. This spiral of events is caused by investigative connections, which are sometimes quite amazingly twisted.

**Russian:**

«Подслеповатые» или назовем их «не до конца прозревшие», они в своём копании останавливаются на каком-то одном шаблоне. Каком-то очень для них главном. И далее они этот шаблон стараются прикладывать ко всему найденному. К примеру по шаблону будет «виноват» «скарабей», они его следы найдут во «всем». Образуя этакие альтернативные мироустройства, центром которого является призма некоего «главного» шаблона. Меняется время, люди, сцены. А те, кто нашли свой «главный шаблон», всё своё строят через эту призму.

**English:**

"Trapped," or let's call them "not fully realized," they're looking for a particular template in their search campaign. Some pattern that's very important to them. And then they try to apply this template to everything they find. For example, the template will "blame" the "scarabs", and then they will find its traces in everything. Forming these alternative world arrangements, the center of which is a prism of this "main" template. Time, people, scenes change. And those who have found their "main pattern", build everything through this prism.

**Russian:**

И спустя какое-то время они костенеют в этих призмочках. Зашориваются. Верх прозорливости оказывается низом понимания, "а что собственно происходит?" А всего-то, просто смыло из потока реки жизни. Вытолкнуло в сторону.

**English:**

And after a while, they ossify in these prisms. They get clogged. The top of foresight is the bottom of understanding of "what's actually going on?" All it does is flush life out of the river stream. It's pushed aside.

**Russian:**

А инструментарий в разных случаях нужен разный. И что хорошо в одних случаях, совсем не годится в других. Что веревкой, к примеру, можно как вытянуть человека из колодца, так и сдернуть с дерева. В обоих случаях вроде результат один, почва под ногами. А «сдернуть» может не совсем хорошо закончится. Но, так то оно да, так то оно конечно. «веревка», «потянуть», «оказаться на уровне земли с соплеменника». Только лететь с дерева больно. А кто-то ж не понимает, что с дерева веревкой надо «стравить» чтоб спустить безопасно. И шпарят по инструкциям . Вот же, «веревка», «потянуть». Что вы нам тут лапшу вешаете, рассказываете, как правильно веревками пользоваться. Мы из колодца вон скока раз доставали и ничего.

**English:**

But the tools - the templates, patterns and instruments of examination - need to be different for different cases. What is good in some cases is not good in others. That a rope, for example, can both pull a man out of a well and hang him from a tree. In both cases, the result seems to be the same, the soil under their feet. "Pulling off" may not end well. But that's the way it is, that's the way it is. The template may be "rope", "pull", "be at ground level with your tribesman". But if the template is misapplied, well, it hurts to dangle from a tree. Sometimes an investigator doesn't understand that you have to hang the rope from the tree to get down it safely. So, they're mis-using the rope according to instructions. Okay, "rope", "pull." What are you doing, telling us how to use the rope? We pulled someone out of the well once so we know how to pull.

**Russian:**

А казалось бы, компоненты и ингредиенты схожы, а результат и порядок использования несколько отличается. И так во всем. В отношениях с окружающими. В отношениях к прочитанному. В отношениях к власти. В отношениях к работе чиновников. В отношениях религиям. В отношениях к наукам. Все стараются найти самый правильный шаблон и его пользовать. Поиск, этакой самой уютной норки, с самым высоким забором и статусом большими буквами на нем. 

**English:**

It would seem that the components and ingredients are similar, and the result and order of use are slightly different. And that's the way it is. In relationships with others. In relationships with what you read. In a relationship to power. In relation to the work of the officials. In relations with religions. In the relationship to the sciences. Everyone tries to find the poshest template and use it. Search... it's like searching for the coziest, most expensive mink fur or the highest wall, the one with the large, high status letters on it.

**Russian:**
А что если любой шаблон всего лишь отвертка? Ну или молоток? И что если базовые правила-шаблоны надо знать? Это как этакий набор инструментов. Что толку, если из всего многообразия инструментов, что можно носить с собой по жизни в чемодане с инструментом, выбрать молоток и колотить им по всему до чего дотянешься? Хотя какое-то количество времени даже что-то и получится. Пока не попадется кто-то с еще большим молотком и отзеркалит данным инструментом в обратку.

**English:**

But what if any pattern is just a screwdriver? Or a hammer? And what if the basic rules-templates need to be known? As with a set of tools, what good is it if, out of all the variety of tools that can be carried around in a toolbox, you select a hammer and then hammer everything with it? With enough time and effort, it may even work. Until you encounter someone who opposes you with an even bigger hammer.

**Russian:**

Что за тяга такая найти самое светлое место, с самыми правильными пассатижами и пытаться ими всё откручивать?

**English:**

What is the attraction of finding the right place with the right pliers and try to unscrew everything with them?

**Russian:**

Фига се отвлекся. Сам в принципе такой. Какое-то время назад начинал с «протоколов». Размахивал флагом борьбы с олигархами. Винил всех и каждого во всем. Прошел путь судьи и забил на него.

**English:**

I was so distracted. It's basically the same. Started with "Protocols" some time ago. Waving the flag to fight the oligarchs. Blamed everyone and everyone for everything. Passed the judge's way and scored one from him.

**Russian:**

Шкурка Моськи, что на слона лаяла, конечно нет нет, да и норовит закрепится в былых точках крепления, да мы как бы понимать стали, что прикладывать свои призмочки к чужим основам миропонимания, нет никакого смысла.

**English:**

In Moscow the elephants trumpet: "No, no", of course, but none of their noise anchors history to its old points of attachment. It makes no sense for us to understand the will with which they apply their prisms to the foundations of other people's understanding of the world.

**Russian:**

А вот подсунуть чего кому, чтоб у тех шаблоны рассыпались стройными рядами. Дык до этого еще дорасти надо. Ну чтоб большее число индивидуумов понимало, что в ящике с инструментом много инструмента. И что культ «самой правильной шлицевой отвертки» «гибнет» при столкновении с культом «самых правильных гаечных ключей».

**English:**

But to comb through the mess, so that those historical templates will fall apart. You've got to cut a hole in the narrative to achieve that. So that more individuals understand that there are a lot of tools in the toolbox. And that the cult of "the most correct slotted screwdriver" "dies" when confronted with the cult of "the most correct wrenches".

**Russian:**

Никому верить нельзя. А перетряхивать ящик с инструментом эпизодически стоит. А то вдруг там давно лежит на самом видном месте самый нужный сейчас инструмент. А мы всё молотком, да молотком.

**English:**

You can't trust anything. It's worth shaking the toolbox occasionally. That way, suddenly, the most necessary tool will now be lying in the most prominent place. At the moment, we'all using a hammer, yes, a hammer.

**Russian:**

Про то, что лежит на поверхности....

**English:**

About what lies on the surface.....

**Russian:**

АРХИ-ВОЛЬТ [http://pro-vladimir.livejournal.com/5781.html](http://pro-vladimir.livejournal.com/5781.html)

**English:**

ARCHI-VOLT [http://pro-vladimir.livejournal.com/5781.html](http://pro-vladimir.livejournal.com/5781.html)

**Russian:**

Кто есть Вольт? или Электрическое оборудование прошлого. Часть 2. [http://pro-vladimir.livejournal.com/4416.html](http://pro-vladimir.livejournal.com/4416.html)

**English:**

Who's volt? Or electrical equipment from the past. Part 2. [http://pro-vladimir.livejournal.com/4416.html](http://pro-vladimir.livejournal.com/4416.html)

**Russian:**

Кто построил Петербург? [http://pro-vladimir.livejournal.com/8369.html](http://pro-vladimir.livejournal.com/8369.html)

**English:**

Who built St. Petersburg? [http://pro-vladimir.livejournal.com/8369.html](http://pro-vladimir.livejournal.com/8369.html)

**Russian:**

По мотивам «И была Страна до России». Часть 8. [http://pro-vladimir.livejournal.com/3857.html](http://pro-vladimir.livejournal.com/3857.html)

**English:**

Based on "And there was a Country Before Russia". Part 8. [http://pro-vladimir.livejournal.com/3857.html](http://pro-vladimir.livejournal.com/3857.html)

**Russian:**

Транспортные системы прошлого. [http://pro-vladimir.livejournal.com/6435.html](http://pro-vladimir.livejournal.com/6435.html)

**English:**

Transport systems of the past. [http://pro-vladimir.livejournal.com/6435.html](http://pro-vladimir.livejournal.com/6435.html)

**Russian:**

Транспортные системы прошлого. Часть 2. [http://pro-vladimir.livejournal.com/6728.html](http://pro-vladimir.livejournal.com/6728.html)

**English:**

Transport systems of the past. Part 2. [http://pro-vladimir.livejournal.com/6728.html](http://pro-vladimir.livejournal.com/6728.html)

**Russian:**

Все собранное оно отталкивается от этого материала:

**English:**

All of the conventional historical structure is dismantled by this material:

**Russian:**

По мотивам «И была Страна до России» [http://dmitrijan.livejournal.com/71535.html](http://dmitrijan.livejournal.com/71535.html)

**English:**

Based on "And there was a Country Before Russia" [http://dmitrijan.livejournal.com/71535.html].

**Russian:**

Иванами да Марьями [http://dmitrijan.livejournal.com/91508.html](http://dmitrijan.livejournal.com/91508.html)

**English:**

Ivan and Mary [http://dmitrijan.livejournal.com/91508.html](http://dmitrijan.livejournal.com/91508.html)

**Russian:**

То есть, фундамент этой перевернутой пирамиды лежит вот на этих краеугольных камнях.

**English:**

That is, the foundation of the inverted pyramid of lies sits on these cornerstones.

**Russian:**

И по чуть разрастается.

**English:**

And it's growing a little bit.

**Russian:**

Несколько изображений АРХИВОЛЬТОВ.

**English:**

Several images of ARCHIVOLTS.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY4I_jHeUjz1TPXc-jaaAhb8ZpFxHJxNw8eLduSz.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY4I_jHeUjz1TPXc-jaaAhb8ZpFxHJxNw8eLduSz.jpeg)

-->

[![](http://www.temples.ru/private/f000002/002_0000585b.jpg#clickable)](http://www.temples.ru/private/f000002/002_0000585b.jpg)

**Russian:**

Ярославль. Церковь Николы Надеина, портал южной двери храма, выходящий в южную галерею.

**English:**

Yaroslavl. The Church of St. Nadine, a portal to the southern door of the temple, overlooking the southern gallery.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_015.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_015.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/2004-07-26_30_komlevb.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/2004-07-26_30_komlevb.jpg)

**Russian:**

Москва. Портал церкви Никиты Великомученика за Яузой .

**English:**

Moscow. Portal of the Church of St. Nikita the Great Martyr behind Yauza.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_016.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_016.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/0708b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/0708b.jpg)

**Russian:**

Вход и часть пояса Рождественского собора в Савво-Звенигородском монастыре Московской губернии. По Каталогу Барщевского N 708.

**English:**

The entrance and part of the Nativity Cathedral belt in the Savvo-Zvenigorodsky Monastery, Moscow Region.  In Barshchevsky's Catalogue N 708.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_002.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_002.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/kaverznev-122b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/kaverznev-122b.jpg)

**Russian:**

Северный портал Никольского собора в Зарайске Московской области.

**English:**

The northern portal of St. Nicholas Cathedral in Zaraysk, Moscow Region.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_018.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_018.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/097_0001114b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/097_0001114b.jpg)

**Russian:**

Западный портал Никольского собора в Николо-Корельском монастыре Архангельской губернии.

**English:**

Western portal of St. Nicholas Cathedral in Nikolo-Korelsky Monastery, Archangelsk Province.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_014.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_014.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/097_0001115b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/097_0001115b.jpg)

**Russian:**

Северный портал Никольского собора в Николо-Корельском монастыре Архангельской губернии.

**English:**

The northern portal of St. Nicholas Cathedral in the Nikolo-Korelsky Monastery, Arkhangelsk Province.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_004.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_004.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/batyushkovo-3531b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/batyushkovo-3531b.jpg)

**Russian:**

Портал Никольской церкви в Батюшково Дмитровского района Московской области.

**English:**

Portal of St. Nicholas Church in Batiushkovo Dmitrovsky district, Moscow region.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_011.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_011.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0002746b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0002746b.jpg)

**Russian:**

Портал в Троицком соборе Ипатьевского монастыря в Костроме.

**English:**

Portal in the Trinity Cathedral of the Ipatiev Monastery in Kostroma.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_017.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_017.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0003154b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0003154b.jpg)

**Russian:**

Портал московской церкви Трифона в Напрудной слободе.

**English:**

The portal of the Moscow Trifon church in Naprudnaya Sloboda.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_006.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_006.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/184_0005616b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/184_0005616b.jpg)

**Russian:**

Успенский собор в Коломне, западный портал.

**English:**

Assumption Cathedral in Kolomna, western portal.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_019.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_019.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/121_2122_RT8b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/121_2122_RT8b.jpg)

**Russian:**

Северный портал московской церкви Спаса на Сетуни.

**English:**

The northern portal of Moscow's Church of the Savior on Setuni.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_003.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_003.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/058_0004703b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/058_0004703b.jpg)

**Russian:**

Портал Пантелеймоновской церкви при Центральном клиническом госпитале ФСБ в Москве.

**English:**

Portal of Panteleimon's Church at the FSB Central Clinical Hospital in Moscow.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_010.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_010.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0006634b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/004_0006634b.jpg)

**Russian:**

Портал Успенской церкви в Иванищах Старицкого района Тверской области.

**English:**

The portal of the Dormition Church in Ivanishchi, Staritsky District, Tver Region.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_009.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_009.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/108_0010631b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/108_0010631b.jpg)

**Russian:**

Портал Благовещенской церкви в Каргополе. Архангельская область.

**English:**

The portal of the Church of the Annunciation in Kargopol. Arkhangelsk region.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPnT5D8.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPnT5D8.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/218_0014617b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/218_0014617b.jpg)

**Russian:**

Входные врата Троицкой церкви Преподобенского монастыря.

**English:**

Entrance gate of the Holy Trinity Church of the Monastery.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_005.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_005.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/067_0014846b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/067_0014846b.jpg)

**Russian:**

Вход в церковь Усекновения Главы Иоанна Предтечи Предтеченского прихода в городе Суздаль.

**English:**

Entrance to the Church of the Beheading of John the Forerunner parish in Suzdal.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_007.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_007.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/242_0021816b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/242_0021816b.jpg)

**Russian:**

Бронницы Московской области. Церковь Успения Пресвятой Богородицы. Декор портала южного фасада основного объёма.

**English:**

Bronnitsy Moscow region. Church of the Assumption of the Blessed Virgin Mary. Portal decor of the southern facade of the main volume.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_013.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_013.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/374_0053121b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/374_0053121b.jpg)

**Russian:**

Портал Вознесенской церкви в Суздале Владимирской области

**English:**

Portal of the Ascension Church in Suzdal, Vladimir region

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_012.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_012.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/204_0065768b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/204_0065768b.jpg)

**Russian:**

Северный портал церкви Входа Господня в Иерусалим в Рязани.

**English:**

The northern portal of the Church of the Entry of the Lord into Jerusalem in Ryazan.

**Russian:**

отсюда: [http://temple_architecture.academic.ru/279/портал](http://temple_architecture.academic.ru/279/%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB)

**English:**

from here: [http://temple_architecture.academic.ru/279/портал](http://temple_architecture.academic.ru/279/%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB)

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/639093_original.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 9_files/639093_original.jpg)

-->

[![](http://ic.pics.livejournal.com/agritura/16089606/639093/639093_original.jpg#clickable)](http://ic.pics.livejournal.com/agritura/16089606/639093/639093_original.jpg)

**Russian:**

Васильевские врата 14 века, снятые с Новгородской Софии и привезенные царем из разорительного похода на Новгород в 1570-м году.

**English:**

Vasilyev's Gate of the 14th century, taken from Novgorod Sofia and brought by the Tsar from the devastating campaign to Novgorod in 1570.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY7lc4YKb8s4t2T8k7Fv-Nfh0tfHCkfZDP0atHaa.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY7lc4YKb8s4t2T8k7Fv-Nfh0tfHCkfZDP0atHaa.jpeg)

-->

[![](http://www.runivers.ru/upload/iblock/894/07-03-26_preview.jpg#clickable)](http://www.runivers.ru/upload/iblock/894/07-03-26_preview.jpg)

**Russian:**

ПОРТАЛЫ обрамленные АРХИВОЛЬТАМИ. В повседневности слово ПОРТАЛ как-то затерлось и замозолило глаза. А карты расчерченные линиями «Розы ветров» зовутся ПОРТУЛАНАМИ. А значение слова ПОРТАЛ, всего-то ничего, «главный проход». Всё как-то крутится вокруг слова ПОРТ.

**English:**

PORTALS framed by ARCHIVOLTS. In everyday life, the word PORTAL somehow rubbed itself and froze its eyes. And the maps calculated with "Wind Rose" lines are called PORTULANS. And the meaning of the word PORTAL is nothing but "main passage." Everything revolves around the word PORT somehow.

**Russian:**

Рассматривая конструкции АРХИВОЛЬТОВ, обращает на себя внимание, что элементы с завидным упорством повторяются. Какие достаточно сложные, какие попроще. Ну прям чисто схемы разной компоновки с некоторым числом совпадений одинаковых деталей.

**English:**

Looking at the construction of ARCHIVOLTS, it is worth noting that the elements are repeated with enviable persistence. Some are rather complicated, some are simpler. Well, they are simply different lay-out schemes with a number of coincidences in their identical details.

**Russian:**

Раскручивая мысль далее. Конструктивно, чтобы собрать в цепь схему храма, там должны быть еще элементы кроме архивольтов. И они в изобилии есть.

**English:**

Spinning the thought further... structurally, in order to chain the scheme of the temple, there should be more elements besides the archivolts. And it turns out there are plenty of them.

**Russian:**

Фактически выпирают во всех ХРамовых сооружениях. Этакие железные связи. Что принято считать служат для придания дополнительной жесткости зданию. Принято то оно принято, а так ли это? а может роль шинопроводов им больше подходит? Ну или как минимум логичней.

**English:**

Actually sticking out in all the temple structures, is a kind of iron bond. Which is usually thought to stiffen the building. That's what's accepted, isn't it? Or maybe the role of "busbars" would be more suitable for them? At least it's more logical.

<!-- Local

[![](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_008.bin)](Inspired by And There Was a Country Before Russia - Part 9_files/oWFjslGPQdjronQR7GMIY8H5DpHmZsWB2OgAgzq3NQ5-ZFFrieQgThPn_008.bin)

-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/108_0007318b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/108_0007318b.jpg)

**Russian:**

Кстати, в последнее время их не стесняются срезать, ибо с чисто строительной точки зрения такое армирование не даёт какого либо полезного функционала.

**English:**

By the way, in later builds they were not ashamed to leave them out, because from a purely construction point of view, such reinforcement does not give any useful functionality.

**Russian:**

Еще один момент который бросился в глаза. Что большая часть вензилей в узорах, так или иначе является векторной записью, схемой, работы той или иной установки. Мы и нынче в технических помещениях размещаем принципиальные технологические схемы. Так, что, всё это, что так радует глаз, схемы работы устройств. Читать их правда разучились.

**English:**

Another aspect that caught my eye is that most of the veneers in the patterns, one way or another, are a vector record, a plan showing this or that installation. We place the basic technological diagrams in technical premises today. All that is so pleasing to the eye, plans showing how devices work. They're really about how to read them.

**Russian:**

Чтоб только надкусить понимание "узоров" можно попробовать почитать тут: [http://dmitrijan.livejournal.com/106799.html](http://dmitrijan.livejournal.com/106799.html)

**English:**

To just nibble at an understanding of "patterns", you can try to read here: [http://dmitrijan.livejournal.com/106799.html](http://dmitrijan.livejournal.com/106799.html)

**Russian:**

Но там материал дается чуть шире. Ну очень много букв.)))

**English:**

But the material is a little wider. Well, there's a lot of letters.)))

Vladimir Mamzerev . 17.12.2013 г.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-10.html)

© All rights reserved. The original author retains ownership and rights.
