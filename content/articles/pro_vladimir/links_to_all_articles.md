title: Links to all pro-vladimir articles
date: 2014-06-30 00:00:02
modified: 2021-09-03 14:54:36
category: 
tags: technology; links
slug:
authors: Vladimir Mamzerev
summary: Links to Google-translated articles
status: draft
local: 
originally: links_to_all_articles.md


### Translated from:

[https://pro-vladimir.livejournal.com/106270.html](https://pro-vladimir.livejournal.com/106270.html)


[http://pro-vladimir.livejournal.com/6435.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/6435.html) Transport systems of the past.  

[http://pro-vladimir.livejournal.com/6728.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/6728.html) Transport systems of the past. Part 2.  

[http://pro-vladimir.livejournal.com/11316.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/11316.html) Transport systems of the past. Part 3.  

[http://pro-vladimir.livejournal.com/15315.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/15315.html) Transport systems of the past. Part 4

[http://pro-vladimir.livejournal.com/16890.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/16890.html) Transport systems of the past. Part 5.  

[http://pro-vladimir.livejournal.com/20182.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/20182.html) Transport systems of the past. Part 6.  

[http://pro-vladimir.livejournal.com/27608.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/27608.html) Transport systems of the past. Part 7.  

[http://pro-vladimir.livejournal.com/53401.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/53401.html) Transport systems of the past. Part 8.  

[http://pro-vladimir.livejournal.com/53778.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/53778.html) Transport systems of the past. Part 9.  

[http://pro-vladimir.livejournal.com/69373.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/69373.html) Transport systems of the past. Part 10.  


[http://pro-vladimir.livejournal.com/76791.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/76791.html) Leafing through old treatises. Part 13. or Transport systems of the past. Part 11.  


[http://pro-vladimir.livejournal.com/7567.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/7567.html) Architecture = Az-RH-Te-K-To-U-Ra  

[http://pro-vladimir.livejournal.com/19699.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/19699.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 2.  

[http://pro-vladimir.livejournal.com/26142.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/26142.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 3.  

[http://pro-vladimir.livejournal.com/31497.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/31497.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 4

[http://pro-vladimir.livejournal.com/33597.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/33597.html)Based on "And there was a country before Russia". Part 12. or Architecture = Az-RH-Te-K-To-U-Ra. Part 5.  

[http://pro-vladimir.livejournal.com/93652.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/93652.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 7.  

[http://pro-vladimir.livejournal.com/97821.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/97821.html)Architecture = Az-RH-Te-K-To-U-Ra. Part 8.  

[http://pro-vladimir.livejournal.com/98345.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/98345.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 9.

[http://pro-vladimir.livejournal.com/98972.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/98972.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 10.  

[http://pro-vladimir.livejournal.com/7994.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/7994.html) Two extremes ...  

[http://pro-vladimir.livejournal.com/11651.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/11651.html) Air Defense = Great Wall of China  

[http://pro-vladimir.livejournal.com/12001.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/12001.html) A little more, a little more.  

[Not sure](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/16086.html) 

http://pro-vladimir.livejournal.com/16086.html Dzhanibekov effect  

[http://pro-vladimir.livejournal.com/24561.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/24561.html) Bronze and DVD, what can they have in common?  

[http://pro-vladimir.livejournal.com/27053.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/27053.html) About gold.  

[http://pro-vladimir.livejournal.com/28598.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/28598.html) Information carriers of the past.  

[http://pro-vladimir.livejournal.com/28918.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/28918.html) Information carriers of the past. Part 2.  

[http://pro-vladimir.livejournal.com/29127.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/29127.html) Information carriers of the past. Part 3.  

[http://pro-vladimir.livejournal.com/29387.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/29387.html) Information carriers of the past. Part 4.  

[http://pro-vladimir.livejournal.com/29661.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/29661.html) Information carriers of the past. Part 5.  

[Not sure](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/33597.html)

[http://pro-vladimir.livejournal.com/35497.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/35497.html) Based on the text "About the Planet".  

[http://pro-vladimir.livejournal.com/38761.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/38761.html) Do we all know about the sun?  

[http://pro-vladimir.livejournal.com/51221.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/51221.html) Sun  

[http://pro-vladimir.livejournal.com/43333.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/43333.html) City of Peter.  

[http://pro-vladimir.livejournal.com/50137.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/50137.html) PRO ... MONEY  

[http://pro-vladimir.livejournal.com/52718.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/52718.html) Leafing through old maps  

[http://pro-vladimir.livejournal.com/62595.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/62595.html) Leafing through old treatises. Part 1.  
[http://pro-vladimir.livejournal.com/62933.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/62933.html) Leafing through old treatises. Part 2.  
[http://pro-vladimir.livejournal.com/63661.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/63661.html) Leafing through old treatises. Part 3.  
[http://pro-vladimir.livejournal.com/64305.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/64305.html) Leafing through old treatises. Part 4.  
[http://pro-vladimir.livejournal.com/64593.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/64593.html) Leafing through old treatises. Part 5.  
[http://pro-vladimir.livejournal.com/65237.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/65237.html) Leafing through old treatises. Part 6.  
[http://pro-vladimir.livejournal.com/66325.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/66325.html) Architecture = Az-RH-Te-K-To-U-Ra. Part 6.  
[http://pro-vladimir.livejournal.com/66591.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/66591.html) Leafing through old treatises. Part 7.  
[http://pro-vladimir.livejournal.com/67047.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/67047.html) Leafing through old treatises. Part 8.  

[http://pro-vladimir.livejournal.com/70477.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/70477.html) Leafing through old treatises. Part 9.  
[http://pro-vladimir.livejournal.com/71020.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/71020.html) Leafing through old treatises. Part 10.  

[http://pro-vladimir.livejournal.com/75407.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/75407.html)Leafing through old treatises. Part 11.  
[http://pro-vladimir.livejournal.com/75663.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/75663.html) Leafing through old treatises. Part 12.  
[http://pro-vladimir.livejournal.com/75919.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/75919.html) Leafing through old treatises. Part 13.  

[http://pro-vladimir.livejournal.com/76791.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/76791.html) Leafing through old treatises. Part 13. or Transport systems of the past. Part 11.  

[http://pro-vladimir.livejournal.com/79143.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/79143.html) Leafing through old treatises. Part 14.  

[http://pro-vladimir.livejournal.com/80291.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/80291.html) Leafing through old treatises. Part 15.

[http://pro-vladimir.livejournal.com/82646.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/82646.html) Leafing through old treatises. Part 16.  


[http://pro-vladimir.livejournal.com/67444.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/67444.html) Clairaudience and more.  

[http://pro-vladimir.livejournal.com/78712.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/78712.html) Inspection of the "cannon" barrels.  

[http://pro-vladimir.livejournal.com/79798.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/79798.html) Are we ready to accept the truth?  

[http://pro-vladimir.livejournal.com/80391.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/80391.html) Library of Alexandria.

[http://pro-vladimir.livejournal.com/82962.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/82962.html) Alexandria Library. part 2.

[http://pro-vladimir.livejournal.com/81340.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/81340.html) A mixture of a bulldog with a rhinoceros.  
[http://pro-vladimir.livejournal.com/82696.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/82696.html) Astronomical and geodetic instruments  

[http://pro-vladimir.livejournal.com/83435.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/83435.html) Keys from the portal?  
[http://pro-vladimir.livejournal.com/84130.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/84130.html) Conversation. A and O.  
[http://pro-vladimir.livejournal.com/84296.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/84296.html) “Magneto-optical holography is already used for three-dimensional storage of information”  
[http://pro-vladimir.livejournal.com/84535.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/84535.html) Alexandria Library. Part 3.  
[http://pro-vladimir.livejournal.com/85261.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/85261.html) How to make a magnetron from a bell tower. A guide for dummies.  
[http://pro-vladimir.livejournal.com/85825.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/85825.html) Inspection of "cannon" barrels. Part [2.http](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87019.html) :  
[//pro-vladimir.livejournal.com/87019.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87019.html)Can all pipes throw gunpowder?  

[http://pro-vladimir.livejournal.com/87086.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87086.html) Transport systems of the future.  

[http://pro-vladimir.livejournal.com/87427.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87427.html) Icons, you say?  
[http://pro-vladimir.livejournal.com/87729.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87729.html) Who built Petersburg?  

[2.http](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87962.html)

[http://pro-vladimir.livejournal.com/87962.html A](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/87962.html) gap in the looking glass.  


[http://pro-vladimir.livejournal.com/88394.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/88394.html) Steel ties and lattices of temples.  

[http://pro-vladimir.livejournal.com/88587.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/88587.html) Steel ties and lattices of temples. Part 2

[not sure](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/89622.html)

[http://pro-vladimir.livejournal.com/89622.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/89622.html)Steel ties and lattices of temples. Part 2. + comments  

[http://pro-vladimir.livejournal.com/89982.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/89982.html) Steel ties and lattices of temples. Part 3.  
[http://pro-vladimir.livejournal.com/91750.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/91750.html) Steel ties and lattices of temples. Part 4.  
[http://pro-vladimir.livejournal.com/92442.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/92442.html) Steel ties and lattices of temples. Part 5.  
[http://pro-vladimir.livejournal.com/92685.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/92685.html) Indict or system for recording past dates.  

[http://pro-vladimir.livejournal.com/94813.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/94813.html) Indict or system for recording past dates. Part 2.  

[http://pro-vladimir.livejournal.com/95866.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/95866.html) Indict or system of recording the dates of the past. Part 3.  

[http://pro-vladimir.livejournal.com/95296.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/95296.html) The time machine was invented and hidden by the Vatican - Chronovisor of the Italian monk  

[http://pro-vladimir.livejournal.com/96138.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/96138.html) Measuring the parameters of the world. Magnetic planetarium.  

[http://pro-vladimir.livejournal.com/96384.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/96384.html) Electric fountains by Gaston Planté  


[http://pro-vladimir.livejournal.com/97024.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/97024.html) All-seeing eye speak?  


[http://pro-vladimir.livejournal.com/99908.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/99908.html) Steel ties and lattices of temples. Part 6.  

[http://pro-vladimir.livejournal.com/101232.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/101232.html) Wave impact.  

[http://pro-vladimir.livejournal.com/102106.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/102106.html) Path.  

[http://pro-vladimir.livejournal.com/100791.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/100791.html) Programmable machines of the past.  
[http://pro-vladimir.livejournal.com/102358.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/102358.html) Programmable machines of the past. Part 2. Wave action.  
[http://pro-vladimir.livejournal.com/102461.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/102461.html) Programmable machines of the past. Part 3. Wave action.  

[http://pro-vladimir.livejournal.com/104489.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/104489.html)Wave action. Part 4. Bronze barrels.  


[http://pro-vladimir.livejournal.com/102680.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/102680.html) Load-handling device from the past.  

[http://pro-vladimir.livejournal.com/103253.html The](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/103253.html) sun  

[http://pro-vladimir.livejournal.com/104159.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/104159.html) Wave impact and a little about the Kremlin.  

[http://pro-vladimir.livejournal.com/105407.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/105407.html) N. Witsen Northern and Eastern Tartary  
[http://pro-vladimir.livejournal.com/105511.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/105511.html) Old printed books.  


© All rights reserved. The original author retains ownership and rights.

