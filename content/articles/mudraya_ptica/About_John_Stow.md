title: About John Stow
date: 2020-12-07
modified: Thu 04 Feb 2021 15:13:30 GMT
category: 
tags: forged history; books
slug:
authors: mudraya_ptica
from: https://mudraya-ptica.livejournal.com/432333.html
originally: About_John_Stow.html
local: About_John_Stow_files
summary: It is not clear what he was actually doing, but at the age of 35 he suddenly became acquainted with famous antiquaries, and a year later he had already published a collection of the works of Chaucer: *The woorkes of Geoffrey Chaucer, newly printed with divers additions whichever have never been in print before*.
status: published

### Translated from:

[https://mudraya-ptica.livejournal.com/432333.html](https://mudraya-ptica.livejournal.com/432333.html)

<!-- Local
![](About_John_Stow_files/1280px-St_Michael_Cornhill_Interior_2_London_UK_-_Diliff.jpg#resized)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg#clickable)](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg/1280px-St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg)

-->

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg/800px-St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg#clickable)](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg/1280px-St_Michael%2C_Cornhill_Interior_2%2C_London%2C_UK_-_Diliff.jpg)

*[https://commons.wikimedia.org/wiki/Category:St_Michael,_Cornhill](https://commons.wikimedia.org/wiki/Category:St_Michael,_Cornhill) David Iliff*

**Russian:**

Хотела быстро написать про одну из реновских церквей, упомянутых в книжке.  Ну как обычно, да.  Зацепилась за имя Джона ~~Сноу~~ Стоу, описАвшего эту церковь.  Узнала, кто это собственно такой.

Оказалось, это бедный и неграмотный антиквар 16 века, издавший и написавший много книжек, в частности, исторические хроники и подробное описание города Лондона.

Бедность выводится из низкой арендной платы за семейное жилье и походами юного Джона за молоком на ближайшую монастырскую ферму, а неграмотность из того, что Джон нигде не учился.

6 шиллингов в год за аренду жилья в [вики-ру](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%BE%D1%83,_%D0%94%D0%B6%D0%BE%D0%BD) превращается в годовой доход папы Джона, торговавшего сальными свечами.

Или их производившего.

Или вообще портного (was the son of Thomas Stow, a tailor).

Или портным стал сам Джон. (Вики-ру ссылается на Британику 1911 года)

Не ясно, чем он на самом деле занимался, но в 35 лет внезапно познакомился с известными антикварами, а через год уже издал собрание сочинений Чосера. («Сочинения Джеффри Чосера, изданные ныне с дополнениями, доселе никогда не публиковавшимися» (The woorkes of Geffrey Chaucer, newly printed with divers additions whiche were never in printe before).)

Далее он занялся хрониками, сведения о которых слегка расходятся.  Британика говорит об одном собрании хроник, вики-а о двух. Надо ли полагать, что за сто лет обнаружились новые издания?  И что значит в понимании Британики "копия"? Просто печатный экземпляр?

> В 1565 году последовало его «Краткое изложение английских хроник (Summarie of Englyshe Chronicles)», которое при его жизни часто переиздавалось с небольшими вариациями. Говорят, что копия первого издания когда-то хранилась в библиотеке Гренвилля. В Британском музее хранятся копии изданий (copies of the editions) 1567, 1573, 1590, 1598 и 1604 годов.

*[Британика 1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> За этим последовало в 1565 году его «Краткое изложение (или собрание) английских хроник (Summarie of Englyshe Chronicles)» (в небольшом формате октаво), а в 1566 году - связанное с ним, но отличное «Собрание английских хроник ... сокращенное (Summarie of Englyshe Chronicles ... Abridged)» (в формате секстодецимо).

*[вики-а](https://en.wikipedia.org/wiki/John_Stow)*

> Обе работы пережили несколько изданий при жизни Стоу: издания Summarie вышли в 1566, 1570, 1574, 1575 и 1590 годах (с дополнительными посмертными изданиями Эдмундом Хоусом в 1607, 1611 и 1618 годах); и Summarie Abridged 1567, 1573, 1584, 1587, 1598 и 1604 года.

Непонятно.

> При покровительстве архиепископа Кентерберийского Мэттью Паркера (Matthew Parker), Стоу удалось опубликовать в 1567 году «Цветы истории» (Flores historiarum) Матфея Вестминстерского (Matthew of Westminster), в 1571 году --- «Всемирную хронику» Матвея Парижского (Matthew Paris), и в 1574 году --- «Краткую историю» (Historia brevis) Томаса Уолсингема".

Три Матвея в одной строке))

> В 1580 году Стоу опубликовал свои Анналы или Хроники Англии от Брута до 1580 года от Рождества Христова (Annales, or a Generate Chronicle of England from Brute until the present yeare of Christ 1580); они были переизданы в 1592, 1601 и 1605 годах, последнее издание вышло 26 марта 1605 года, через десять дней после его смерти; издания, «дополненные» Эдмундом Хоусом, выходили в 1615 и 1631 годах".

*[Б-1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> Самым известным трудом Стоу считается фундаментальное сочинение «Описание Лондона» (A Survey of London, 1598), представляющее собой свод сведений по истории, праву, топографии, архитектуре и обычаях населения английской столицы времён Елизаветы I. Эта работа интересна уникальным объёмом приведенных в ней знаний.

*[вики-ру](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%BE%D1%83,_%D0%94%D0%B6%D0%BE%D0%BD)*

"**Второе издание появилось при его жизни в 1603 году, третье с дополнениями Энтони Мандея в 1618 году, четвертое - в редакции Мандея и Дайсона в 1633 году, пятое - с интерполированными поправками Джона Страйпа в 1720 году и шестое - того же редактора в 1754 году. Издание 1598 года было переиздано под редакцией У. Дж. Томса в 1842 году, в 1846 году и с иллюстрациями в 1876 году**.

> По просьбе Паркера Джон составил довольно большую рукопись об истории Британии, но она была утеряна.

*[Б-1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> Стоу был в тесном контакте со многими ведущими антикварами своего времени, включая архиепископа Мэтью Паркера, Джона Джоселина, Джона Ди, Уильяма Флитвуда, Уильяма Ламбарда, Роберта Гловера, Генри Сэвила, Уильяма Камдена, Генри Феррерса и Томаса Хэтчера.

> Он был активным членом первого Общества антикваров, основанного примерно в 1586 году.

> Он создал обширную библиотеку рукописных и печатных исторических источников, которые стали известны современникам как его «хранилище», и которые он щедро предоставлял другим.

> Большая часть рукописей Стоу хранится в коллекций Харлея в Британском музее, часть в библиотеке Ламбета, фрагменты которых были опубликованы обществом Камден под редакцией Джеймса Гайрднера под названием «Three Fifteenth Century Chronicles, with Historical Memoranda by John Stowe the Antiquary, and Contemporary Notes of Occurrences written by him» в **1880** году.

*[вики-а](https://en.wikipedia.org/wiki/John_Stow)*

В статьях приводится довольно много подробностей, типа адресов домов Джона, его хорошего характера, наличия тяжб с братом за наследство и споров с ~~конкурентом~~ собратом-хронистом, хотя не очень понятно, откуда взяты эти сведения.

Немного ознакомившись с источниками на сайтах архив.орг и Британской библиотеки, кое-что выяснила.

На архив.орг по запросу "Джон Стоу" найдено 119 экземпляров книг, из них:

- 15 принадлежит автору **stow, john, 1525?-1605**

- а 16 автору **john stow**, причем попытка перейти к этому второму автору ведет к первому.

- Писал этот Стоу-2 нечто религиозное в середине 19 века. Больше ничего про него не узнала.

- В 19 веке также упоминаются и другие Стоу - Давид, Барон, финеас, например.

Часть произведений за авторством john stow на самом деле принадлежит stow, john, ну такая неунифицированность в авторах наблюдается.


**English:**

I wanted to quickly write about one of the Reno churches mentioned in the book.  Well, as usual, yes I got hooked on the name of John <del>Snow</del> Stowe, who described this church. Found out who he actually is.

Turns out he was a poor and illiterate 16th century antiquarian who published and wrote many books, particularly historical chronicles and a detailed description of the City of London.

Poverty is derived from the low rent of the family home and young John's hikes for milk at the nearby monastery farm, and illiteracy from the fact that John has not studied anywhere.

Six shillings a year for rent in [wiki-ru](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%BE%D1%83,_%D0%94%D0%B6%D0%BE%D0%BD) turns into a yearly income from Papa John's tallow candle trade.

Perhaps someone else produced the books.

Perhaps he was even a tailor. He was the son of Thomas Stow, a tailor.

Or perhaps John became the tailor. ([Wiki.ru](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%BE%D1%83,_%D0%94%D0%B6%D0%BE%D0%BD) refers to the [1911 *Britannica*](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John).)

It is not clear what he was actually doing, but at the age of 35 he suddenly became acquainted with famous antiquaries, and a year later he had already published a collection of the works of Chaucer. *The woorkes of Geoffrey Chaucer, newly printed with divers additions whichever have never been in print before*.

Next, he took up the *Chronicles*, about which the information is slightly divergent. *Britannica* speaks of one collection of chronicles, *Wiki* speaks of two. Should we assume that new editions have been discovered in a hundred years? And what does "copy" mean according to Britannica? Just a printed copy?

> In 1565 followed his *Summarie of Englyshe Chronicles*, which was frequently reprinted with slight variations during his lifetime. A copy of the first edition is said to have once been preserved in the Grenville Library. The British Museum holds copies of the editions of 1567, 1573, 1590, 1598, and 1604.

*[Britannica 1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> This was followed in 1565 by his *Summarie (or collection) of the English Chronicles (Summarie of Englyshe Chronicles)* (in a small octavo format), and in 1566 by the related but different *Summarie of Englyshe Chronicles ... Abridged (Summarie of Englyshe Chronicles ... Abridged)* (in sextodecimo format).

> Both works survived several editions during Stowe's lifetime: editions of *Summarie* came out in 1566, 1570, 1574, 1575 and 1590 (with additional posthumous editions by Edmund Howes in 1607, 1611 and 1618); and *Summarie Abridged:* 1567, 1573, 1584, 1587, 1598 and 1604.

*[wiki (English)](https://en.wikipedia.org/wiki/John_Stow)*

I don't understand.

> "Under the patronage of Matthew Parker, Archbishop of Canterbury, Stowe succeeded in publishing Matthew of Westminster's *Flores historiarum* in 1567, Matthew of Westminster's *The World Chronicle* in 1571, and Thomas Walsingham's *Historia brevis* in 1574.

Three Matthews in one line! :)

> In 1580 Stowe published his *Annales, or a Generate Chronicle of England from Brute to the present yeare of Christ 1580*; they were reprinted in 1592, 1601 and 1605, the last edition coming out March 26, 1605, ten days after his death; editions 'supplemented' by Edmund Howes came out in 1615 and 1631."

*[Britannica 1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> Stowe's most famous work is considered to be *A Survey of London* (1598), a fundamental work containing information on the history, law, topography, architecture, and customs of the English capital during the reign of Elizabeth I. It is remarkable for the sheer amount of knowledge it contains.

*[Wiki (English)](https://en.wikipedia.org/wiki/John_Stow)*

> The second edition appeared during his lifetime in 1603, the third with additions by Anthony Munday in 1618, the fourth edited by Munday and Dyson in 1633, the fifth with interpolated amendments by John Stipe in 1720, and the sixth by the same editor in 1754. The 1598 edition was reprinted under the editorship of W. J. Thoms in 1842, in 1846, and with illustrations in 1876.

> At Parker's request, John compiled a rather large manuscript on the history of Britain, but it has been lost.

*[Britannica 1911](https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Stow,_John)*

> Stowe was in close contact with many of the leading antiquarians of his time, including Archbishop Matthew Parker, John Jocelyn, John Dee, William Fleetwood, William Lambard, Robert Glover, Henry Savile, William Camden, Henry Ferrers and Thomas Hatcher. [^1]

> He was an active member of the first Society of Antiquaries, founded around 1586.

> He built up an extensive library of manuscript and printed historical sources, which became known to his contemporaries as his "repository," and which he generously made available to others.

> Most of Stowe's manuscripts are in the Harley collections at the British Museum, some in the Lambeth Library, fragments of which were published by the Camden Society edited by James Gairdner under the title *Three Fifteenth Century Chronicles, with Historical Memoranda* by John Stowe the Antiquary, and *Contemporary Notes of Occurrences* written by him, in **1880**.

*[wiki (English)](https://en.wikipedia.org/wiki/John_Stow)*

The articles give quite a few details, such as the addresses of John's houses, his good character, the existence of litigation with his brother over his inheritance, and disputes with a <del>competitor</del> collector-chronicler, though it's not very clear where this information is taken from.

After doing a little research on the [archive.org](https://archive.org/) and [British Library](https://www.bl.uk/) websites, I found out a few things.

I searched [archive.org](https://archive.org/search.php?query=John%20Stowe) for "John Stowe" and found 119 copies of books, including:

- 15 belong to the author **stow, john, 1525?-1605** (Translator's note: Stowe 1)

- and 16 to the author **john stow**, but attempts to navigate to this second author lead to the first John Stow. (Translator's note: Stowe 2)

- This Stowe 2 wrote something religious in the mid-19th century. I didn't learn anything else about him.

- Other Stowes are also mentioned in the 19th century: David, Baron, Phineas, for example.

Some of the works authored by 'john stow' actually belong to 'stow, john'. Well, inconsistencies like this are observed in authorship.

**Russian:**

У Стоу-1 в списке имеются:

- Чосер 1561 года, где он не единственный редактор, кстати, (Thynne's edition, with additional poems appended by John Stow.]);

- [Двухтомник Хроник истории Англии, Шотландии и Ирладии 1587 года](https://archive.org/details/firstsecondvolum00holi_0), где в авторах упоминаются еще 7 человек;

- Анналы [Annals of England to 1603], судя по написанию заголовка, без титульного листа (и с неправильной нумерацией страниц);

- Annales, or, a generall chronicle of England **1631** года;

- Holinshed's Chronicles of England, Scotland, and Ireland .. **1807** года, среди авторов упоминается Стоу;

- *A New and Complete History of the County of York: Illustrated by a Series of ...* **1831** года, среди множества авторов упоминаются George Stow, J. Stowe и широко известный Wenceslaus Hollar;

- *Three fifteenth-century chronicles, with historical memoranda* by John Stowe, the antiquary, and contemporary notes of occurrences written by him in the reign of Queen Elizabeth **1880** года;

- В Бритбиблиотеке доступно издание Анналов как бы 1600 года. (Как бы - потому что в квадратных скобках).

**English:**

Stowe 1 has on his titles list:

- *Chaucer*, 1561, where he is not the only editor, by the way, (Thynne's edition, with additional poems appended by John Stow;)

- [*Two-volume Chronicle of the History of England, Scotland, and Ireland*, 1587](https://archive.org/details/firstsecondvolum00holi_0), which mentions seven other men as authors;

- *Annals*, which is *Annals of England to 1603*, judging by the spelling of the title, without a title page (and misnumbered pages);

- *Annales*, or, *a general chronicle of England* **1631**;

- Holinshed's *Chronicles of England, Scotland, and Ireland*. **1807**, Stowe is mentioned among the authors;

- *A New and Complete History of the County of York: Illustrated by a Series of ...* **1831**. Among the many authors mentioned are George Stow, J. Stowe, and the well-known Wenceslaus Hollar;

- *Three fifteenth-century chronicles, with historical memoranda by John Stowe, the antiquary, and contemporary notes of occurrences written by him in the reign of Queen Elizabeth* **1880**;

An edition of the Annals is available from the British Library as if it were 1600. (Sort of - because it's in square brackets).

Translator's note: The original author of this article is highlighting some, to me, unknown implication in the fact that the British Library lists this book in square brackets. Possibly, it means the book is catalogued as existing, or thought to be in the British Library collection but cannot be found.

<!-- Local
![](About_John_Stow_files/930795_800.jpg#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930795/930795_800.jpg#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930795/930795_original.jpg)

- [[*The Annales of England, faithfully collected out of the most autenticall Authors, Records ... untill ... 1592*. B.L.]](http://explore.bl.uk/BLVU1:LSCOP-ALL:BLL01017883231) [John Stow, 1525?-1605. London : R. Newbery, [1600]

- *The survay of London: containing, the originall, antiquitie, encrease, and more moderne estate of the sayd famous citie...* **1618**;

- *The survey of London: contayning the orignall, increase, moderne estate, and government of that city, methodically set downe* **1633**;

- *[A survey of the cities of London and Westminster](https://archive.org/details/b30454554_0001/page/8/mode/thumb), borough of Southwark, and parts adjacent... being an improvement of Mr. Stow's, and other surveys, by adding whatever alterations have happened in the said cities, &c., to the present year...* **1734**;

- *[A survey of London 1842](https://archive.org/details/b2928823x/page/n16/mode/1up)*;

- *[A Survey of London 1876](https://archive.org/details/surveyoflondon02stow/page/79/mode/thumb)*;

- *Reflections on the Epistles of st. Paul, and on that to the Hebrews, with Scriptural ...* 1847, Oh, no, that's already Stowe 2

- And so is this *Thoughts on the Gospel of Jesus Christ*. By a lay-member of the Church of England [J. Stow]. 1846.

**Russian:**

Есть еще более поздние издания книжки про Лондон, но это уже не очень интересно.

В книжке 1842 года во-первых строках впервые появляется биография Стоу, из текста которой становится понятно, что сведения о жизни Джона взяты в основном из самой книжки про Лондон, где он упоминает и про монастырских коров, и про 6 шиллингов. Папа назван портным.

**English:**

There are still later editions of the London book, but these are not so very interesting.

In the 1842 book the first lines of Stow's biography appear, from the text of which it becomes clear that the information about John's life is taken mainly from the London book itself, where he mentions both the monastery cows and the 6 shillings. Pope is identified as a tailor.

<!-- Local
![](About_John_Stow_files/930341_800.jpg#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930341/930341_800.jpg#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930341/930341_original.jpg)

*[A Survey of London ... New edition](http://explore.bl.uk/BLVU1:LSCOP-ALL:BLL01014830294)*, with illustrations, edited by W. J. Thoms. London : Chatto & Windus, 1876.

**Russian:**

В книжке 1876 года, дополненном переиздании 1842, кроме биографии имеется уже и портрет. А также знакомая чудесная картинка с Гринвичским замком.

Несмотря на объявленную иллюстрированность, помимо этих двух картинок, в наличии еще только две - дворец Якова-Джеймса и какая-то деревня. Все картинки на отдельных листах.

Не знаю, может остальные картинки из книжки повыдирали, а может не все отсканировали, хотя сомнительно.

Количество картинок одинаково что в ББ, что на Архиве, а там экземпляр из другой библиотеки.

Зато книжка 1734 года демонстрирует несколько больше картинок, как в тексте, так и на отдельных страницах.

**English:**

In the 1876 book of 1876, supplemented by the reprint of 1842, there is also a portrait in addition to a biography. There is also a familiar, lovely picture of Greenwich Castle.

Despite the announced illustrations, there are only two other pictures available besides these two - James-James' palace and some village. All the pictures are on separate sheets.

I don't know, maybe the rest of the pictures were ripped out of the book, or maybe not all of them were scanned, though I doubt it.

The number of pictures is the same in BB as it is in the Archive, and there the copy is from a different library.

The 1734 book, on the other hand, shows somewhat more pictures, both in the text and on the individual pages.

<!-- Local
![](About_John_Stow_files/929540_800.jpg#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929540/929540_800.jpg#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929540/929540_original.jpg)

<!-- Local
![](About_John_Stow_files/929288_800.jpg#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929288/929288_800.jpg#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929288/929288_original.jpg)

**Russian:**

И картинки другие, Гринвичский замок не углядела навскидку.

Также это издание содержит в три раза больше страниц (827), чем следующие (240, 220), и деление на несколько томов.

В 1842-76 гг. никаких упоминаний о томах на титулах нет. Может, там в тексте что-то сказано, но весь его читать у меня совершенно нет желания.

Теперь более поздние издания внезапно! вызывают больше интереса))

[Издание 1890 года](https://archive.org/details/survayoflondonco00stowuoft/page/107/mode/thumb) страниц содержит 470, а картинок не имеет вовсе. Деления на вольюмы не упоминается.

[Издание 1893 года](https://archive.org/details/b24879769/page/187/mode/thumb) повторяет 1890.

В биографии по-прежнему числится папа-портной.

А вот издание 1908 года весьма примечательно. Во-первых, томов два, по четыреста с лишним страниц каждый.

Во-вторых, издание стилизовано под 1603 год, только шрифт немного другой и буквы современные (буква s, например).

В-третьих, имеется наукообразная биография с упоминанием источников, библиография изданий Стоу, списки использованных документов, история фамилии (генеалогия), списки рукописей и книг, принадлежавших Джону и хранящихся нынче где-то-там.

А папа и дедушка наконец обретают профессию свечеваров и вполне себе обеспеченность. Правда, сумма в 6 чего-то изображена так, что понять ее я не могу. Что значит in plate тоже не знаю.

**English:**

And the pictures are different, I couldn't see Greenwich Castle at first glance.  Also, this edition contains three times as many pages (827) as the following (240, 220), and the division into several volumes.  There is no mention of the 1842-76 volumes in the titles. Maybe it says something in the text there, but I have absolutely no desire to read the whole thing.  Now the later editions are - suddenly! - attracting more interest. :)

[1890 Edition](https://archive.org/details/survayoflondonco00stowuoft/page/107/mode/thumb) contains 470 pages and has no pictures at all. The division into volumes is not mentioned.

[1893 Edition](https://archive.org/details/b24879769/page/187/mode/thumb) repeats 1890.

In the biography the father is still listed as being a tailor.

The 1908 edition, on the other hand, is quite remarkable.  First, there are two volumes, four hundred-plus pages each.

Secondly, the edition is styled as 1603, only the font is slightly different and the letters are modern (the letter s, for example).

Third, there is a scholarly biography citing sources, a bibliography of Stowe's publications, lists of documents used, a history of the family name (genealogy), and lists of manuscripts and books that belonged to John and are now stored somewhere else.

And the father and the grandfather finally acquire the profession of candlestick-maker and are quite well-to-do. However, the amount of 6 something is depicted in such a way that I can't understand it. I don't know what it means in the plate either.

<!-- Local
![](About_John_Stow_files/930157_300.jpg#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930157/930157_300.jpg#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/930157/930157_original.jpg)

**Russian:**

Да, еще там появился другой портрет.

**English:**

Yeah, there's also another portrait that appeared there.

<!--
![](About_John_Stow_files/929991_1000.png#resized)
-->

[![](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929991/929991_1000.png#clickable)](https://ic.pics.livejournal.com/mudraya_ptica/21727240/929991/929991_original.png)

*From The Gentleman's Magazine for 1837*

**Russian:**

Британика-1911 просто не поспела за полетом научной исторической мысли между 1893 и 1908 годом). В 1912 году уже вышло сокращенное издание двухтомника для школьников на 130 страницах.

Про хроники нашла такой чудесный текст:

> [В 1577 году Рафаэль Холиншед](http://faculty.smu.edu/bwheeler/Joan_of_Arc/OLR/crholinshed.pdf), о котором практически ничего не известно, опубликовал свои «Хроники Англии, Шотландии и Ирландии».

> Для второго издания, выпущенного в 1587 году, Хроники были перередактированы, модернизированы, дополнены и обновлены другими людьми: Джоном Хукером, Авраамом Флемингом и Фрэнсисом Тинном, среди прочих.

> Это было издание, из которого Шекспир черпал материал для многих своих пьес, включая его трактовку Жанны д'Арк в Генрихе VI.

> Хотя стандарты исторической достоверности Холиншеда ни в коем случае не столь строги, как те, которыми дорожат современные историки, он часто ссылается на свои источники, даже замечая некоторые разногласия между ними.

> В своем описании войны с Францией он в значительной степени опирается на «Союз двух благородных и ярких знаменитостей Ланкастера и Йорка, 1398-1547 гг.» (Лондон, 1550 г.) Галле, за исключением материала о Жанне, в котором он часто ссылается на французские источники, хотя ясно, что он без колебаний украсил свой рассказ патриотической английской точкой зрения, в то же время предупредив своих читателей о том, что французские отчеты, несомненно, были пристрастными.

**English:**

*Britannica 1911* simply did not keep up with the flight of scientific historical thought between 1893 and 1908. In 1912 an abridged edition of a two-volume book for schoolchildren was already published in 130 pages.  I found such a wonderful text about *Chronicles*:

> [In 1577 Raphael Holinshed](http://faculty.smu.edu/bwheeler/Joan_of_Arc/OLR/crholinshed.pdf), of whom little is known, published his *Chronicles of England, Scotland, and Ireland*.

> For the second edition, issued in 1587, the Chronicles were re-edited, modernized, supplemented, and updated by other men, John Hooker, Abraham Fleming, and Francis Tinn, among others.

> This was the edition from which Shakespeare drew material for many of his plays, including his treatment of Joan of Arc in *Henry VI*.

> Although Holinshed's standards of historical accuracy are by no means as rigorous as those cherished by modern historians, he often cites his sources, even noting some disagreement between them.

> In his description of the war with France he draws heavily on *The Union of Two Noble and Bright Notables of Lancaster and York, 1398-1547* (London, 1550) by Halle, with the exception of the material on Jeanne, in which he frequently refers to French sources, though it is clear that he did not hesitate to embellish his account with a patriotic English point of view, while at the same time warning his readers that the French accounts were doubtless partial.

© All rights reserved. The original author retains ownership and rights.

[^1]: Translator's note: This piece is hard to translate. This is partly because the writer implies and hints rather than explicitly statements, and partly because the piece relies on reader already having knowledge of the methods and topics used for the creation of fraudulent history. The author writes for readers who already possess a certain level of knowledge of the type of fraud exemplified by the 'John Stow' entity. They are:

    - Creation of fake characters. Often identifiable by:

        - sparse biographies
        
        - inconsistent biographies
        
        - extraordinarily and multi-talented
        
        - unexplained acquisition of skills
        
        - sudden rises to fame
        
        - links to the clergy

        - sudden acquisition of influential patrons, sponsors

    - Creation of books attributable to these fake characters. Subjects tend to be geography, history, morality (often in verse-form).

    - Limited public access to characters' works (for independent testing)
