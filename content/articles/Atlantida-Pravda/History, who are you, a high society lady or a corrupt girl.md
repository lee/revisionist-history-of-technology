title: History, who are you, a high society lady or a tart?
date: 2012-11
modified: Thu 14 Jan 2021 07:20:41 GMT
category:
tags: St Petersburg
slug: 
authors: ZigZag
summary: I got confused by the large number of inconsistencies in a very recent, in my opinion, history. The last 200-300 years is not the illiterate Middle Ages.
status: published
originally: Introductory remarks to the article "Where does the city come from?" History, who are you, a high society lady or a corrupt girl.html
local: ./History, who are you, a high society lady or a corrupt girl_files/

### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html)

[This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/C6FIMiTOPCA.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/C6FIMiTOPCA.jpg)
-->

[![](https://3.bp.blogspot.com/-byQCa6TcfnA/UotwDthERSI/AAAAAAAAFcc/CEI8VZQpZsM/s640/C6FIMiTOPCA.jpg#clickable)](http://3.bp.blogspot.com/-byQCa6TcfnA/UotwDthERSI/AAAAAAAAFcc/CEI8VZQpZsM/s1600/C6FIMiTOPCA.jpg)

> "We are tormented by small, useless problems, we waste our thoughts and feelings in vain, we look at our feet from birth, only at our feet. Remember how we are taught from childhood:" Look at your feet ... Don't fall ... " We are dying without understanding anything: who we are and why we are here. And yet somewhere deep in us there is one aspiration: there, upward, home!" 

*Andrei Mironov's monologue from the film "Faryatyev's Fantasy".*

History is a strange word. But I'm not about the meaning of this word, it can now be easily found on the Internet, I'm about history, as a science, which, like a corrupt girl, every time adjusts to the powerful. The one who pays the most gets the brightest and most colorful version. A whole staff of so-called "historians on the pay" is working on her image tirelessly day and night. She wears her options "up" for correction, creates, sculpts and sculpts from all this a beautiful and majestic statue of the splendor of the state, which by its nature remains the same corrupt girl, every time running to a new owner. Likewise, Russian history rushes from century to century in search of the only one to whom it will be faithful forever. But "the only ones" come and go, centuries change. But she wants to live well...

**Russian:**

Современная история пишется не монахами отшельниками, как в средние века, по страничке в день, сто раз вычитываясь и перепроверяясь, анынешними гаджетами. Тысячи, миллионы, миллиарды знаков в секунду, утонуть в таком объёме информации запросто, но знающий направление --достигнет суши. Современные технологии зомбирования извращаются в соревновании между собой за:

- продажу любой фигни за максимальную цену;

- внушении что белое это чёрное;

- управлению массами. Всего одним сообщением в СМИ, на выдуманную тему,легко выгнать массы на улицу, а дальше достаточно что бы кто-то крикнул "наших бьют!". Таких пунктов можно перечислить огромное количество, от"конца света" до подорожания жетонов на проезд в метро, но я отклоняюсь от темы. 

**English:**

Modern history is written not by hermit monks, as in the Middle Ages, one page a day, a hundred times proofread and double-checked, but by modern gadgets. Thousands, millions, billions of characters per second, it is easy to drown in such volume of information, but he who knows the direction - will reach the land. Modern zombie technology twists and turns in competition with each other for:

- selling any bullshit for the highest price;

- the indoctrination that white is black;

- controlling the masses. It is easy to drive the masses on to the streets with just one message in the media, on an invented topic, and then it is enough for someone to shout "our people are being beaten!". There are a huge number of points like this, from "the end of the world" to the rise in the price of subway tokens, but I digress from the topic.

**Russian:**

Работая над статьёй ["Откуда город?"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html), я запутался в большом количестве нестыковок совсем недавней, на мой взгляд, истории. Промежуток времени в 200-300 лет -- это немалограмотные средние века, когда письменность была редкостью, а книги считались чудом.  Именно самая значимая часть российской истории, самыйпропиаренный государь вызвал у меня большое количество вопросов. Ещё недавно я верил учебнику истории для средней школы, повода не было в нёмусомниться, но перечитывая его в укромном месте, я поймал себя на мысли, что я читаю сказку, всем известного для моего поколения А.С.Пушкина.Государь Пётр Первый выступает в ней, как волшебник: махнул левой рукой -- вырос город, махнул правой -- город заселился знатью и чернью,прихлопнул, и вырылись каналы, сразу облицованные гранитом. Я про постройки молчу, просто пока не придумал, что он делал с рукой, когданужно было сотни тысяч кубов кирпича и гранита (про мрамор упускаю, его там просто негде было взять). При отсутствии дорог, кирпичных заводов,гранитных карьеров, грузовой техники велось колоссальное строительство. А он ещё умудрился фортов наставить, то тут, то там. Наверное, В. И.Чапаев у Петра Алексеевича учился тактике построения боя. Берём картошку, ставим -- будет Исаакиевский собор, а вот эту, поменьше --будет Эрмитаж. Та, что просыпалась, будет крепостями в заливе. И о чудо, наутро всё стояло. А вокруг города все люди жили в деревянных избах,благо строительного дерева было впрок, умения особого и труда такая стройка не требовала. Веками строили из древесины, отличного, хорошо ибыстро обрабатываемого материала, кирпичными на то время были только московский и новгородский Кремль, десяток-два соборов с монастырями, ипара-тройка крепостей в больших городах, построенных ещё при Иване IV Грозном и до него. 

**English:**

While working on the article ["Where is the city from?"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html), I was confused by the many inconsistencies in what I consider to be very recent history. The time span of 200-300 years is the not inconsiderable Middle Ages, when writing was a rarity and books were considered a miracle.  It was the most significant part of Russian history, the most propagandized sovereign, that raised a lot of questions for me. Until recently, I believed the history textbook for high school, there was no reason to doubt it, but rereading it in a secluded place, I caught myself thinking that I was reading a fairy tale, all known to my generation A. In it Peter the Great appears as a magician: he waved his left hand - the city grew, waved his right hand - the city was settled by nobility and nobility, and the channels appeared, immediately lined with granite. I'm silent about the buildings, I just can't figure out what he was doing with his hand when there were hundreds of thousands of cubes of brick and granite (I'm leaving out the marble, there was just nowhere to get it). In the absence of roads, brickyards, granite quarries, trucking equipment was a huge construction. And he still managed to put forts here and there. Probably Chapayev learned the tactics of building a battle from Peter Alekseyevich. If you take a potato, you'll put it in St. Isaac's Cathedral, and a smaller one will be the Hermitage. The one that woke up, will be a fortress in the bay. And - oh a miracle! - in the morning, everything was standing. And around the city, all the people lived in wooden houses, benefit construction wood was in plenty, special skills and labor of such building did not require. For centuries, were built of wood, an excellent, well and quickly workable material, brick at that time were only the Moscow and Novgorod Kremlin, a dozen or two cathedrals with monasteries, and a few or three fortresses in large cities, built under Ivan IV the Terrible and before him.

**Russian:**

Вчитываясь в строчки официального учебника, я наткнулся на ещё болееинтересную подробность истории. Не Пётр Первый был зачинателем сей сказки, её начали писать, вырвав страницы более древней и, на мойвзгляд, более значимой истории. Захват власти Романовыми и тотальное истребление наследников Рюриков, их истории, их деяний, их влияния наЕвропу и Азию, требовал новых страниц, и такие страницы были написаны, после тотального уничтожения церковных летописей времён Рюриков.Странные пожары в архивах церквей вспыхивали то тут, то там, а то, что удалось сберечь, изымалось для сохранности людьми государя. Мы о древнемРиме и древней Греции сейчас знаем больше, чем о временах правления Рюриков. Даже иконы и фрески церквей, были сняты и сколоты по приказуРомановых. А уж если начал врать -- не останавливайся, ибо уличён будешь в непостоянстве. В петровские времена, в связи с переносом столицы вСанкт-Петербург московский Кремль пришел в запустение. В святых для Рюриков стенах играли свадьбы и ставили спектакли, на территории Кремляразместили кабак, а в его подвалах тюрьмы. Когда стал вопрос о ремонте обветшалого Кремля, Пётр денег не дал, ему не было дела до старыхРусских святынь, он смотрел в Европу, которая на удивление не стояла перед ним на коленях, как при Иване IV Васильевиче Грозном, а наоборотвсему учила и управляла. В окружении Петра находились шведы и голландцы, немцы и австрийцы, даже турки. Советов соплеменников он не любил.Московский пожар 1737 года уничтожил не только часть Кремля, он уничтожил архив, расположенный в здании большого дворца, с документамидеяний государя и государства. "Описные дела прошлых лет", карты, данные о границах с 1571 по 1700 год, документы и указы, предоставив темсамым непаханое поле для работы "романовским историкам", сочинять на чистом листе было намного проще, чем ссылаться на первоисточники.Романовы превратили Кремль в большой бордель. К началу 19 века на его территории размещались дома разврата и воровские притоны. Историческиепамятники рюриковской эпохи вызывали у Романовых крайнее раздражение. Соборы времён Рюриков на территории Кремля были либо снесены (Сретенскийсобор, Гербовая башня), либо перестроены (Хлевенный, Кормовой и Сытный дворцы). Уничтожен дворец Ивана Грозного на Воробьёвых горах. В 1806году с аукциона продан дворец Бориса Годунова. Когда в перестройке не было надобности, в ход шли бочки с порохом, как в случае соСвятотроицким Ипатиевским мужским монастырём в Костроме, вотчиной Годуновых, где было захоронено около 60 человек из рода Годуновых.Неужели Романовых кто-то предупредил о том, что со временем будет найдена ДНК экспертиза, и доказать что Борис Годунов из рода Рюриков несоставит труда?

**English:**

Reading through the lines of the official textbook, I stumbled upon an even more interesting detail of history. Peter the Great was not the mastermind of this tale, it began to be written by tearing out the pages of an older and, in my opinion, more significant history. Seizure of power by the Romanovs and the total extermination of the heirs of the Ruriks, their history, their deeds, their influence in Europe and Asia, required new pages, and such pages were written, after the total destruction of the church chronicles of the times of the Ruriks. We about ancient Rome and ancient Greece now know more than about times of board of Rurik's. Even icons and frescoes of churches, were removed and chipped away at the order of the Romanovs. And if you start to lie, do not stop, because you will be found guilty of inconstancy. In Peter's time, in connection with the transfer of the capital in St. Petersburg the Moscow Kremlin came into disrepair. Weddings and plays were performed inside the walls, the Kremlin was used as a tavern, and prisons were set up in the cellars of the Kremlin. When the question arose about the repair of the dilapidated Kremlin, Peter gave no money, he did not care about the old Russian sanctuaries, he looked to Europe, which surprisingly did not kneel before him, as under Ivan IV Vasilievich the Terrible, but rather taught and guided him. Peter's entourage included Swedes and Dutch, Germans and Austrians, even Turks. He did not like the advice of his countrymen.   The Moscow fire of 1737 destroyed not only part of the Kremlin, it destroyed the archive, located in the building of the great palace, with the documents of the sovereign and the state. The "inventory deeds of the past", maps, data on borders from 1571 to 1700, documents and decrees, thus providing a ploughed field for "Romanov's historians", it was much easier to compose on blank paper than to refer to primary sources. The Romanovs turned the Kremlin into a big brothel. By the beginning of the 19th century its territory housed houses of debauchery and thieves' brothels. Historical monuments of Rurik's epoch caused the Romanovs extreme irritation. Cathedrals of times of Rurik's in territory of the Kremlin have been either demolished (Sretensky cathedral, Emblem tower), or reconstructed (Khlevennyj, Kormovoy and Sytnyj palaces). The palace of Ivan the Terrible on the Vorobyovy Hills was destroyed. In 1806 the palace of Boris Godunov was sold at auction.     When it was not necessary to rebuild, they used barrels of gunpowder, as in the case of St. Ipatitsky Monastery in Kostroma, the ancestral home of the Godunovs, where about 60 people of the Godunov family were buried. Is it possible that someone warned the Romanovs that eventually DNA examination will be found and prove that Boris Godunov of the Rurik family will not be difficult?

**Russian:**

Но самое большее раздражение у династии Романовых вызывали письменные источники, где хранились данные "о иерархии государевых людей, ихродстве, заслугах и делах ратных". Все назначения на государственные должности происходили на основе "местничества", той самой иерархии,прописанной в "разрядных книгах". 12 января 1682 года Романовы отменили "местничество" на Руси, уничтожив все старые "разрядные книги", гдеупоминалось невысокое происхождение самих Романовых. Вместо них были заказаны новые, для лояльных и преданных династии людей. Созданная дляэтого "палата родословных дел" составила всего две книги, "бархатную" и утерянную. Первая на проверку оказалась фальсификацией, гдеродословные семейств многих чиновников были написаны с потолка. До конца семнадцатого века в Москве хранилась "степенная книга", составленная в1560-1563гг. по инициативе духовника Ивана Грозного Макария, Митрополита Московского. Книга содержала историю от первых русских князей до времёнИвана IV Васильевича Грозного, грандиозная летопись династии Рюриков. Именно на её основе делались фрески во многих Русских монастырях(Архангельский собор Московского Кремля). В книге утверждалось, что династия Рюриковичей вела свой род от римского императора Августа, но вовремена Алексея Михайловича, книга, хранившаяся в записном приказе под семью замками, таинственным образом исчезает. В 1672 году в посольскомприказе Романовы составляют "Большую государственную книгу" или "Корень российских государей", так называемый "титулярник". Она содержаларисованные портреты всех великих князей от Рюрика до Алексея Михайловича. "Титулярник" был написан произвольно, не опираясь напредыдущую историю, в духе величия династии Романовых, по их же заказу. В это же время австрийский дипломат Лаврентий Хуревич (одна фамилия чегостоит), подданный императора австрийского, Леопольда Первого, побывавший в Москве в 1656 году, составляет новую историю династии Романовых, и какинструкцию по дальнейшему преобразованию истории отправляет её царю. А в 1673 году тот же Хуревич издаёт расширенную историю "РодословиеПресвятейших и Вельможнейших Великих Князей Московии" названой генеалогией, где скрупулёзно обосновывает царскую кровь в жилах АлексеяМихайловича в одном ряду с другими европейскими монархами, и в 1674 году отправляет её  в Москву. Заказ выполнен, деньги переведены, безбеднаястарость и достаток семьи обеспечены, за разглашение тайны -- сами знаете... 

**English:**

But the Romanov dynasty was most irritated by written sources that contained data on "the hierarchy of the sovereign's people, their kinship, merits and deeds of arms. All appointments to government posts were based on "mestnichestvo", the very hierarchy, prescribed in the "rank books. On January 12, 1682 the Romanovs abolished mestnichestvo in Russia, destroying all the old parade books, which referred to the Romanovs' low origins. In their place were ordered new ones for the loyal and devoted to the dynasty people. The "House of Genealogical Affairs" established for this purpose compiled only two books, one "velvet" and the other lost. The first one turned out to be a forgery where the genealogical families of many officials were written from the ceiling. Until the end of the seventeenth century, Moscow housed a "steppe book", compiled in 1560-1563 on the initiative of the confessor of Ivan the Terrible Makarii, Metropolitan of Moscow. The book contained the history from the first Russian princes to the time of Ivan IV Vasilievich the Terrible, a grandiose chronicle of the Rurik dynasty. It was on its basis that frescoes were made in many Russian monasteries (the Archangel Cathedral of the Moscow Kremlin). The book stated that the Rurik dynasty descended from the Roman Emperor Augustus, but at the time of Alexei Mikhailovich, the book, which was stored in the notebook office under seven locks, mysteriously disappears. In 1672 in the ambassadorial prikaz Romanovs compiled "The Big Book of State" or "The Root of the Russian sovereigns," the so-called "titularnik". It contained painted portraits of all the great princes from Rurik to Alexei Mikhailovich.   The "titularnik" was written arbitrarily, without reference to previous history, in the spirit of the greatness of the Romanov dynasty, at their own request. At the same time, an Austrian diplomat Lavrenty Hurevich (one surname), a subject of the Austrian Emperor Leopold the First, who traveled to Moscow in 1656, is a new history of the Romanov dynasty, and as an instruction for further transformation of history sends it to the king. And in 1673 the same Hurevich publishes an expanded history "The genealogy of the Most Holy and Grand Dukes of Moscow" called genealogy, where meticulously substantiates the royal blood in the veins of Alexei Mikhailovich in a row with other European monarchs, and in 1674 sends it to Moscow. The order is fulfilled, the money is transferred, the family's free old age and prosperity are ensured, for the disclosure of the secret - you know...

**Russian:**

В Европе же к Романовым относились снисходительно, не считая ровней, нолюбили по своему, за преданность европейским традициям и отсутствие давления, которое всегда присутствовало у династии Рюриков. Вбольшинстве европейских летописей тех лет, о Романовых, как о царской династии просто не упоминалось. Единственное, что не удалось уничтожить-- это географические карты, скопированные и развезённые путешественниками по всему миру. Иван Кириллович Кирилов был назначенПетром I ответственным за создание географического атласа России, вся работа состояла из трёх томов по 120 карт каждый, но императорскаяакадемия запретила атлас Кирилова, 360 точнейших карт было уничтожено, разломанными были даже печатные доски. Петра I ужаснули размеры техтерриторий, которые остались от Рюриков, и с которыми так бездарно повелись Романовы. Великой Тартарии, с её размахом, могуществом ицарями, ведущими свой род от римских императоров больше не было, значит вспоминать о ней тоже не стоило. И только после смерти Петра I, Кириловиздаёт и готовит к печати [37 карт, 28 из которых сохранились.](http://www.runivers.ru/bookreader/book16667/#page/3/mode/1up) В последнем царе династии Романовых, Николае II практически не было русской крови, но он стал русским по духу своему, именно он поднялгосударство, не прислушиваясь к европейским советчикам, за что и поплатился. На карте мира с тех пор появились новые государства, новыевластители, а это значит, что новые часы очередной, переписанной истории затикали.Теперь уже сложно вспомнить, было так

**English:**

In Europe, the Romanovs were treated indulgently, not considered equals, but loved in their own way, for their loyalty to European traditions and the absence of pressure, which has always been present in the Rurik dynasty. In most European annals of those years, the Romanovs as a royal dynasty were simply not mentioned. The only thing that couldn't be destroyed were the geographic maps, copied and scattered by travelers around the world. Ivan Kirillovich Kirilov was appointed by Peter I responsible for the creation of geographical atlas of Russia, the entire work consisted of three volumes of 120 maps each, but the imperial academy banned Kirilov atlas, 360 most accurate maps were destroyed, even the boards were broken. Peter I was horrified by the size of the territories, which were left by the Ruriks, and with which so mediocrely dealt the Romanovs. The Great Tartary, with its scale and power and the Tsars who were descended from the Roman emperors was no more, so it was not worth remembering it either. And only after the death of Peter I, Kirilov creates and prepares to print [37 maps, 28 of which have survived](http://www.runivers.ru/bookreader/book16667/#page/3/mode/1up). In the last tsar of the Romanov dynasty, Nicholas II had practically no Russian blood, but he became Russian in his spirit, it was he who raised the state, not listening to European advisors, for which he paid the price. Since then, new states and new rulers appeared on the world map, which means that a new clock on another, rewritten history began to tick.

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/a.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/a.jpg)
-->

[![](http://2.bp.blogspot.com/-YSUIwjGXwAU/UgzOG-Vu1ZI/AAAAAAAAEF0/S9VkM0mdkos/s1600/%D0%B0.jpg#clickable)](http://2.bp.blogspot.com/-YSUIwjGXwAU/UgzOG-Vu1ZI/AAAAAAAAEF0/S9VkM0mdkos/s1600/%D0%B0.jpg)

**Russian:**

или так...

**English:**

or so...

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/wojna_mirow.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/wojna_mirow.jpg)
-->

[![](http://4.bp.blogspot.com/-hsJy0YUpcEA/UgzJpQRxu7I/AAAAAAAAEFk/6TcGHtxzsJM/s1600/wojna_mirow.jpg#clickable)](http://4.bp.blogspot.com/-hsJy0YUpcEA/UgzJpQRxu7I/AAAAAAAAEFk/6TcGHtxzsJM/s1600/wojna_mirow.jpg)

**Russian:**

Но как то же оно было)))

**English:**

But that's how it was. )))

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/9lKP47Sp7hY.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/9lKP47Sp7hY.jpg)
-->

[![](http://2.bp.blogspot.com/-wBl8vMBsO7I/UgzYIXlWQOI/AAAAAAAAEGU/fbMKqazP1-A/s1600/9lKP47Sp7hY.jpg#clickable)](http://2.bp.blogspot.com/-wBl8vMBsO7I/UgzYIXlWQOI/AAAAAAAAEGU/fbMKqazP1-A/s1600/9lKP47Sp7hY.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/qdvC9L3bwU8.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/qdvC9L3bwU8.jpg)
-->

[![](http://1.bp.blogspot.com/-eafVSLxUszA/UgzYHlDm2VI/AAAAAAAAEGI/Hj43hOr1frw/s1600/qdvC9L3bwU8.jpg#clickable)](http://1.bp.blogspot.com/-eafVSLxUszA/UgzYHlDm2VI/AAAAAAAAEGI/Hj43hOr1frw/s1600/qdvC9L3bwU8.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/qO1Vq_wuGkQ.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/qO1Vq_wuGkQ.jpg)
-->

[![](http://2.bp.blogspot.com/-5mI6QWyoHIU/UgzYHcdUaYI/AAAAAAAAEGE/zR_oTh21ar0/s1600/qO1Vq_wuGkQ.jpg#clickable)](http://2.bp.blogspot.com/-5mI6QWyoHIU/UgzYHcdUaYI/AAAAAAAAEGE/zR_oTh21ar0/s1600/qO1Vq_wuGkQ.jpg)

**Russian:**

P.S.Вот ещё одна, на мой взгляд, занимательнейшая карта Европы

**English:**

PS: Here's another, I think, fascinating map of Europe:

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/DzQJ2H-4tVI.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/DzQJ2H-4tVI.jpg)
-->

[![](http://4.bp.blogspot.com/-hVyYS0fEHkA/Uhw7McIbvQI/AAAAAAAAEP0/ROSBxf22Iyc/s1600/DzQJ2H-4tVI.jpg#clickable)](http://4.bp.blogspot.com/-hVyYS0fEHkA/Uhw7McIbvQI/AAAAAAAAEP0/ROSBxf22Iyc/s1600/DzQJ2H-4tVI.jpg)

**Russian:**

> "Никакой Римской Империи в первом веке нашей эры не было, и этому множество доказательств, начиная с того, как нагло современные "историки" фабриковали Историю Древнего Рима! Нет, город Рим был и в античные времена, только НЕ БЫЛО НИКАКОЙ РИМСКОЙ ИМПЕРИИ! И чтобыубедиться в том, что это именно так, а не иначе, достаточно взглянуть на реальную карту Античной Европы, созданную в 1595 года знаменитым на весьмир и признанным картографом средних веков А. Ортелиусом. Тот факт, что этот картограф признан и весьма почитаем современнымиисториками, не позволит им же его отрицать! А это очень важно для разоблачения фальсификаторов. Так вот, эта карта составлена А.Ортелиусом в 1595 году. И на этой карте отображена АНТИЧНАЯ ЕВРОПА! Античная Европа по отношению к Средним Векам (1595 год), а это означает,что на этой карте изображена Европа, как минимум, за тысячу лет до создания самой карты! А это означает, что на карте отображена Европа непозже 5-6 века нашей эры! И... что видно на этой карте!? На ней нет НИ ЗАПАДНОЙ, НИ ВОСТОЧНОЙ РИМСКИХ ИМПЕРИЙ!!А согласно современной "истории" они должны были быть и процветать! И эта карта --- не единственная! Как ни старались создатели "истории"уничтожить все реальные свидетельства прошлого, у них это не получилось, хотя они и очень старались. Они --- это социальные паразиты, от властикоторых и хотел освободить иудеев Иисус Христос!!! На карте Античной Европы нет Римской Империи, но на ней... большую частьзанимает Славяно-Арийская Империя, которую в следующем тысячелетии будут называть Великой Тартарией! Только в античные времена Славяно-АрийскаяИмперия занимала почти всю Европу, от неё совсем недавно "откололись" Britannica (Великобритания), Hispania (Испания и Португалия) и Gallia(Франция и Италия). Эти страны уже откололись от единой Империи Белой Расы, но в них ещё некоторое время правила династияМеровингов".(прим.:Меровинги - династия северных Русов).

**English:**

> "There was no Roman Empire in the first century A.D., and there is plenty of evidence for this, starting with how brazenly modern "historians" fabricate the History of Ancient Rome! No, there was a city of Rome in ancient times, but there was NO RIM EMPIRE! And to be sure that this is exactly so, and not otherwise, it's enough to look at the real map of ancient Europe, created in 1595, the world famous and recognized cartographer of Middle Ages A. Ortelius. The fact that this cartographer is recognized and highly esteemed by modern historians will not allow them to deny him! And this is very important to expose falsifiers. So, this map is made by Ortelius in 1595. And this map shows ANTI-CENTURAL EUROPE!    Ancient Europe in relation to the Middle Ages (1595), which means that this map shows Europe at least a thousand years before the map itself! Which means that this map shows Europe no later than the 5th or 6th century AD! And... what does this map show? There are no Western or Eastern Roman Empires on it!!! And according to modern "history" they should have been and prospered! And this map --- is not the only one! No matter how hard the creators of "history" tried to destroy all the real evidence of the past, they failed, although they tried very hard. They are --- the social parasites, from whose power Jesus Christ wanted to free the Jews! The map of ancient Europe does not show the Roman Empire, but on it ... Most of it is taken by the Slavic-Aryan Empire, which in the next millennium will be called the Great Tartary! Only in ancient times the Slavic-Aryan Empire occupied almost all of Europe, from it more recently "broke away" Britannica (Great Britain), Hispania (Spain and Portugal) and Gallia (France and Italy). These countries had already broken away from the White Race Empire, but they were still ruled by the Merovingian dynasty for some time.


**Russian:**

Добавить нечего, разве что цитату:
***В каждой естественной науке заключено столько истины, сколько в ней есть логики и математики.***
Вот тут и начинаешь понимать Носовского и Фоменко, прислушиваться к Левашову и Кунгурову...

А оппоненты пусть орут: Еретики!!! На костёр! Да, на костёр, а куда жеещё, там уже сгорел на Площади Цветов в Риме Джордано Бруно... как и много тех, кто пытался понять ПОЧЕМУ?

Но... "[ОНА ВСЁ ТАКИВЕРТИТСЯ]
(http://ru.wikipedia.org/wiki/%C8_%E2%F1%B8-%F2%E0%EA%E8_%EE%ED%E0_%E2%E5%F0%F2%E8%F2%F1%FF!)"
:) ;) :D :-))) !!!

Материал в тему статьи:

- [Как историки сочиняли Монгольскую империю (часть 1)](http://kungurov.livejournal.com/69966.html)

- [Как историки сочиняли Монгольскую империю (часть 2)](http://kungurov.livejournal.com/70288.html)

- [Как историки сочиняли Монгольскую империю (часть 3)](http://kungurov.livejournal.com/71033.html)

Пару карт напоследок

**English:**

Nothing to add, except a quote:

> Every natural science contains as much truth as it does logic and mathematics.

This is where you begin to understand Nosovsky and Fomenko, listen to Levashov and Kungurov...

And let the opponents shout: "Heretics!!! To the bonfire!" Yes, to the bonfire, and where else, there already burned in the Piazza delle Flower in Rome Giordano Bruno ... as well as many others who tried to understand the question: WHY?

But... "[SHE'S GETTING THERE](http://ru.wikipedia.org/wiki/%C8_%E2%F1%B8-%F2%E0%EA%E8_%EE%ED%E0_%E2%E5%F0%F2%E8%F2%F1%FF!)".
:) ;) :D :-))) !!!

Material to the subject of the article:

- [How Historians Composed the Mongol Empire (Part 1)](http://kungurov.livejournal.com/69966.html)

- [How Historians Composed the Mongol Empire (Part 2)](http://kungurov.livejournal.com/70288.html)

- [How historians composed the Mongol Empire (part 3)](http://kungurov.livejournal.com/71033.html)

A couple of maps to finish up with:

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1575_Francois_De_Belleforest.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1575_Francois_De_Belleforest.jpg)
-->

[![](http://4.bp.blogspot.com/-lu26jOqMFQc/Umi39QSfUvI/AAAAAAAAFEI/ixWOzzfFh6g/s1600/1575_Francois_De_Belleforest.jpg#clickable)](http://4.bp.blogspot.com/-lu26jOqMFQc/Umi39QSfUvI/AAAAAAAAFEI/ixWOzzfFh6g/s1600/1575_Francois_De_Belleforest.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1617_1632_Nicholas_Van_Geelkercken.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1617_1632_Nicholas_Van_Geelkercken.jpg)
-->

[![](http://3.bp.blogspot.com/-67A6kkCcEQk/Umi3X1cV9FI/AAAAAAAAFD4/YCO0kPzuzPU/s1600/1617_1632_Nicholas_Van_Geelkercken.jpg#clickable)](http://3.bp.blogspot.com/-67A6kkCcEQk/Umi3X1cV9FI/AAAAAAAAFD4/YCO0kPzuzPU/s1600/1617_1632_Nicholas_Van_Geelkercken.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1690_Justus_Danckerts.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1690_Justus_Danckerts.jpg)
-->

[![](http://4.bp.blogspot.com/-Hamm_uQc9Ok/Umi3vI26H0I/AAAAAAAAFEA/Gs1jcHKXN6Y/s1600/1690_Justus_Danckerts.jpg#clickable)](http://4.bp.blogspot.com/-Hamm_uQc9Ok/Umi3vI26H0I/AAAAAAAAFEA/Gs1jcHKXN6Y/s1600/1690_Justus_Danckerts.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1702_William_Godson.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1702_William_Godson.jpg)
-->

[![](http://1.bp.blogspot.com/-pTz0lzOegVw/UmjBavkqOgI/AAAAAAAAFEU/fh3ao4dJrko/s1600/1702_William_Godson.jpg#clickable)](http://1.bp.blogspot.com/-pTz0lzOegVw/UmjBavkqOgI/AAAAAAAAFEU/fh3ao4dJrko/s1600/1702_William_Godson.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/tartar2-big.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/tartar2-big.jpg)
-->

[![](http://4.bp.blogspot.com/-Tj-ZPu4smDs/Umo5JTGykZI/AAAAAAAAFFc/JRGEImUVU6Q/s1600/tartar2-big.jpg#clickable)](http://4.bp.blogspot.com/-Tj-ZPu4smDs/Umo5JTGykZI/AAAAAAAAFFc/JRGEImUVU6Q/s1600/tartar2-big.jpg)

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1278745823_karty-3.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1278745823_karty-3.jpg)
-->

[![](http://3.bp.blogspot.com/-ygokVYj3h7s/UozIDFsfYgI/AAAAAAAAFcs/NesiLfRd7W0/s1600/1278745823_karty-3.jpg#clickable)](http://3.bp.blogspot.com/-ygokVYj3h7s/UozIDFsfYgI/AAAAAAAAFcs/NesiLfRd7W0/s1600/1278745823_karty-3.jpg)

Iohann Baptisto Homanno (Homann). Ukrania quae et Terra Cossacorum...

**Russian:**

Нюрнберг, 1716 (розмір 47,5х57 см ). Колекция А.Грегоровича.

**English:**

Nuremberg, 1716 (size 47,5x57 cm). Collection of A. Gregorovich.

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/map1000model.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/map1000model.jpg)
-->

[![](http://1.bp.blogspot.com/-EhG3F81JIMo/Uqa82TPaU_I/AAAAAAAAFuA/Iw6OGJnF5uU/s1600/map1000model.jpg#clickable)](http://1.bp.blogspot.com/-EhG3F81JIMo/Uqa82TPaU_I/AAAAAAAAFuA/Iw6OGJnF5uU/s1600/map1000model.jpg)

**Russian:**

Историческая справка о Тартарии и генеалогическое дерево Правителей Тартарии. Франция, 1719 год.

**English:**

Historical note on Tartary and genealogical tree of the Rulers of Tartary. France, 1719.

<!-- Local
[![](History, who are you, a high society lady or a corrupt girl_files/1719henrichatelain.jpg#clickable)](History, who are you, a high society lady or a corrupt girl_files/1719henrichatelain.jpg)
-->

[![](http://2.bp.blogspot.com/-w4zGXJ0vK30/Umo3rIbq4SI/AAAAAAAAFFU/R3_vLXAZ4KQ/s1600/1719henrichatelain.jpg#clickable)](http://2.bp.blogspot.com/-w4zGXJ0vK30/Umo3rIbq4SI/AAAAAAAAFFU/R3_vLXAZ4KQ/s1600/1719henrichatelain.jpg)

**Russian:**

> *...На каком-то участке своего жизненного пути человек возвращается к здравому смыслу -- это неизбежно. Когда умирают и навеки закапываются необузданные мечты о справедливости и прогрессе, тогда, наконец, наступает пора, когда человек становится спокойным, бесстрастным и неподатливым, как лезвие отточенного на камне меча. Если такой меч подставить под капли дождя, они не оставят на нём следов, так же, как и кровь, которой рано или поздно он будет омыт. Дождь и кровь одинаково сбегают по отточенной стали.*

Спасибо за понимание, Сергей ЗигЗаг. 

Автор: ZigZag


**English:**

> At some point in his life's journey, man returns to common sense - this is inevitable. When unrestrained dreams of justice and progress die and are buried forever, then at last there comes a time when man becomes calm, impassive and unyielding, like the blade of a sword sharpened on a stone. If such a sword is exposed to raindrops, they will leave no trace on it, just like the blood with which it will be washed sooner or later. Rain and blood run down the same way on sharpened steel.

Thank you for your understanding, Sergey

Author: ZigZag

© All rights reserved. The original author has asserted their ownership and rights.

#### Comments:

1. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [24 ноября 2013 г., 14:11](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1385295117197#c1465759580808874618)

Можно обмануть кого-нибудь другого, но невозможно обмануть самого себя. Для чего этот обман нужен и кому? И даже через тысячу лет истина, как и росток через асфальт, произрастет и свой свет явит миру. Даже представления о Природе имеют политико-финансовые корни! Ну что, слабо начать говорить правду?

You can deceive someone else, but you cannot deceive yourself. What is the purpose of this deception, and to whom? And even after a thousand years, the truth, like a sprout through the asphalt, will grow and show its light to the world. Even ideas about Nature have political and financial roots! Well, can't you start telling the truth?

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[4 декабря 2013 г., 09:12](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1386141179030#c7690289275291184298)

 Не слабо, просто не всем нравится, с момента появления статьи "откуда город?", сайт пытались сломать 7 раз, заражали вредоносным ПО, спамили... но гугл (слава великому хранителю данных) делает откат на сутки назад, и всё работает опять.

Not weak, just not everyone likes it, since the article "where's the city from?", the site has been tried to break 7 times, infected with malware, spammed... but google (thanks to the great data keeper) rolls it back 24 hours and everything works again.

 2. ![](History, who are you, a high society lady or a corrupt girl_files/Presentazione252B400px252B7252B.jpg)

 [Olga Maria Lydia](https://www.blogger.com/profile/01305071047479780514)[3 января 2020 г., 17:38](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1578065882247#c3442300053851315749)

 Да врёте Вы все, никто не станет тратить время на саботаж отьявленно глупой писанины и фальшивых карт. Себе цену набиваете, это типичный прием врунов.

 You're all lying, no one is going to waste time sabotaging stupidly stupid writing and fake maps. You're putting a price on yourself, it's a common trick of liars.

2. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

Anonymous [11 декабря 2013 г., 07:40](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1386740432251#c238450991355790172)

 я,просто поражён.Спасибо вам за вашу работую

I'm amazed. Thank you for your work.

3. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

Anonymous [13 декабря 2013 г., 10:35](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1386923759795#c2105104184728547386)

**Russian:**

Спасибо Вам за Ваши труды. Много интересного. Единственное, что хотелось бы сказать. Если история реальная замалчивается, то у этого должны быть руководители и сегодня - кто-то, кто не позволяет энтузиастам добывать новые доказательства. И если бы я был одним из них, понимая, что во времена интернета скрыть инфу крайне сложно, я бы раскрыл карты, но таким образом, как выгодно мне. Т.е. 90% реальных фактов, за которые люди зацепятся и доказательства сами смогут найти, а 10% какой-нить полнейшей х...ни, в которую поверят вдогонку. Это я к вопросу про Фоменко и Ко, Левашова и ко и т.п. Людей, обладающих стройной теорией на тему альтернативной истории надо побаиваться и ОЧЕНЬ критично относится к выводам и размышлениям, а принимать только факты (раскопки, карты, письма, язык, архитектура) и т.п. А реальная картина всплывет и сложится тогда, когда до максимального количества народа дойдет понимание того, что история высосана из пальца, а не то, как было "на самом деле". Мне кажется есть люди, которые расскажут правду, правда вылезет наружу, когда народ начнет говорить об этом. Где-то она должна была сохраниться (ватикан например, какой-нить здравомыслящий папа, или сознательный монарх из старой династии). А нам остается самим строить свои теории и обмениваться инфой, а дальше как всевышнему будет угодно ))) С уважением Алексей
i
**English:**

Thank you for your work. A lot of interesting things. The only thing I would like to say. If the real story is being suppressed, then it must have leaders even today - someone who does not allow enthusiasts to extract new evidence. And if I were one of them, realizing that in the days of the Internet it is extremely difficult to hide information, I would reveal the cards, but in a way that benefits me. I.e. 90% of the real facts, which people will catch on and find evidence for themselves, and 10% of some bullshit, which they will believe on the follow-up. This is on the question of Fomenko & Co, Levashov & Co, etc. People who have a coherent theory on alternative history should be feared and VERY critical of the conclusions and thinking, and accept only the facts (excavations, maps, letters, language, architecture), etc. And the real picture will emerge and emerge when as many people as possible come to the realization that history is sucked out of thin air and not the way it was "really". It seems to me there are people who will tell the truth, the truth will come out when people start talking about it. Somewhere it must have been preserved (the Vatican for example, some sensible pope, or a conscious monarch from an old dynasty). And it remains for us to build our own theories and exchange information, and then as the Almighty will please))) Sincerely Alexey


 1. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [19 марта 2015 г., 05:44](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1426736680125#c7526803325787512813)

 Полностью поддерживаю данное мнение: история - подделка, но найти и доказать это стоит большого труда

4. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [13 декабря 2013 г., 15:37](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1386941856617#c1591064888853956544)

 НАУЧНЫЕ ФАКТЫ - О МИРОВЫХ СЛАВЯНСКИХ ПРАЯЗЫКЕ, ПИСЬМЕННОСТИ и ИСТОРИИ и КРОМЕ НАС - ВСПОМНИМ?!.. ВОЗРОЖДЕНИЕ СЛАВЯНСКОЙ КУЛЬТУРЫ ОБРАЗОВАНИЯ - ЧЕРЕЗ ВОЗРОЖДЕНИЕ ИЗНАЧАЛЬНОЙ ПИСЬМЕННОСТИ ПРЕДКОВ - КОТОРАЯ ИЗНАЧАЛЬНА КО ВСЕМ АЗБУКАМ МИРА!!..
 (- НАДЕЮСЬ ИНТЕРЕСНО! ЕСЛИ ЗАХОТИТЕ ТО САМИ ПОЖАЛУЙСТА ОПУБЛИКУЙТЕ КАК ПОЛОЖЕНО! СПАСИБО!) - ССЫЛКА на: -
 [http://newvesti.info/azbuka-slavyanskaya-azbuka/] - И ТАКЖЕ НА: -
 [http://xn--80ajoghfjyj0a.xn--p1ai/do-kirilla-i-mefodiya-na-rusi-sostavlyali-yuridicheskie-dokumenty]
 files.mail.ru/5CA6B2A97A70486099F9572236191BFE http://yadi.sk/d/XZpp8XKTEF9JJ

 ШОКИРУЮЩИЕ ФАКТЫ - СОВЕРШЕНСТВА СЛАВЯНСКИХ - ПИСЬМЕННОСТЕЙ И ЯЗЫКА ИЗНАЧАЛЬНЫХ КО ВСЕМ ДРУГИМ!

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Unknown](https://www.blogger.com/profile/15980851540887882046)[8 января 2014 г., 11:52](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1389174720361#c4500997257096207406)

 огромное спасибо!


5. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [17 января 2014 г., 17:38](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1389973099395#c1044892138819309001)

 Благодарствую! Побольше бы таких людей как ты (Вы - говорили славяне врагу).

6. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [7 февраля 2014 г., 14:32](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1391776320776#c2007354778950719535)

 Главное не останавливайтесь и не бросайте это дело! Удачи!!!

7. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Maul](https://www.blogger.com/profile/18038016155306654627)[14
 февраля 2014 г., 00:14](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1392329669978#c2702432268507994812)

Author: "We take potatoes, put - there will be St. Isaac's Cathedral, .... And lo and behold, in the morning everything was standing." After reading these lines, I wanted to know the name of the author. But the name of the author, like this "work", turned out to be a "zigzag". Let me remind you that St. Isaac's Cathedral was erected four times. It all started back in 1706. There were wooden and brick buildings, which were demolished and rebuilt. The construction of the latter lasted 40 years and ended in 1858. Is it fast and easy for you? Quote: "I am silent about the buildings .... when I needed hundreds of thousands of cubes of brick and granite (I miss about marble, there was simply nowhere to take it)." I'll tell you where. More than 50 deposits of granite are known in Russia, suitable for use as a piece stone, as well as rubble and rubble, on the Karelian Isthmus, in the Onega and Ladoga regions, Arkhangelsk and Voronezh regions, etc. Do you need marble? you are welcome! In the north-west of Russia, in the Republic of Karelia, the Leningrad region and on the Kola Peninsula, red and pink granites are produced by the Vinga, Ukkomyaki and Shalskoe deposits. The yellow-pink stone is given by Mustavaar. The most famous is the Shokshinskoye deposit. Its stone was used in the construction of Napoleon's sarcophagus in Paris, the monument to Nicholas I in St. Petersburg, the tomb of the Unknown Soldier in Moscow. The gray-pink and red granite of the Kuzrechenskoye deposit in the Murmansk region won the recognition of architects and builders. In the Leningrad region, at the Elizovsky deposit, a gray-brown stone is mined, reminiscent of the famous American granite "dacotamahogany". After such disconnection, it's hard to believe the rest.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[February 14, 2014 09:40 AM](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1392363623645#c8401112658677003189)

You just quoted to me the official, Miller's, version of the city's construction. I will try to be laconic and just provide links. You will familiarize yourself with them, then we will talk more.

[http://ja-rus.ru/isaakievskij_sobor/](http://ja-rus.ru/isaakievskij_sobor/)

[http://levhudoi.blogspot.com/2013/06/blog-post.html](http://levhudoi.blogspot.com/2013/06/blog-post.html)

[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/8.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/8.html)

Let's summarize:

Someone Samson (Semyonovich or Ksenofontovich) Sukhanov with his team dressed the city in granite in two decades, the embankment, canals, building foundations, and these are not khukhry-mukhry, these are millions of tons. He made antique columns, monuments and statues three pieces a day. Then he disappeared without a trace and died in poverty, although no one saw him during his lifetime (there are simply no memories of his contemporaries). The first mentions of such a significant master appeared only at the beginning of the 20th century; in the 19th century, historians did not hear anything about him. Read the official sources, I give the link: Column master S.S. Sukhanov. Someone V.P. Yekimov casted grandiose statues (for some reason, everything was in the same antique style), the good fellow was a talent, it's a pity that no one saw him, he was very modest. The well-known A.A. Montferrand, being just an artist (in his notes, Vigel characterized Montferrand only as a good draftsman, no more) without hesitation, he took on the most ambitious architectural projects, having no idea how to erect them. The equally famous E.M. Falcone easily agreed to install the Bronze Horseman, although history says that he had never done anything similar before. The real caster, the Frenchman B. Ersman, who was invited for this work, refused it. His hand could not be raised to remake the statue. A.A. Betancourt, like a Phoenix bird, is reborn ten years after death, for the installation of the most grandiose monument - the Alexander Column, flashes here and there, leads the builders and troops, celebrates wherever possible. ... there are so many inconsistencies that historians would have to agree among themselves. But it was written at different times, by different inventors, sometimes not imagining that another story was being written nearby. At the end of the work, an acceptable version was chosen, which satisfied the customer, and it was decided not to pay attention to the roughness. But time goes on, censorship falls, and hidden edges appear on the surface, let's not pass by, just take a closer look.

 2. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymously[October 30, 2015 11:50 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1446241828163#c8363056502801504729)

 And do not forget at the same time that the window at this time, the Maunder Minimum lasted exactly until 1715, when they skate on the Thames and at your wonderful deposits in the Karelian tundra, temperatures were minus 50. And of course, yes, with a pick in your hands you would have shod the entire Volga from Seliger to the Caspian in ten years.


 3. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymously[26 августа 2016 г., 12:53](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1472205237611#c5790516601853729376)

 Недавно был в Святотроицком-Ипатьевском монастыре в Костроме,искал бочки с порохом,не нашел.Но нашел усыпальницу бояр Годуновых.Как-то после этого не очень верится во все остальное.

 4. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Маргр[9 июня 2019 г., 17:59](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1560092342758#c7539693244433500710)

 Трезвые мысли, спасибо. Еще мраморный каньон в Рускеале, пудожский камень из Карелии.Фактами обуздать фантазии автора.


8. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [18 февраля 2014 г., 20:18](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1392747515466#c1136981995873233057)

 Сергей, возможно Вам (и вашим читателям) будет полезна и эта доп.информация: http://istclub.ru/topic/1397-екатерина-1-мать-петра-1-портретное-расследован/ , где не совсем по теме "Откуда город", но о его основателе и др. Материал не прост в прочтении и понимании, но помогает из отдельных пазлов, в числе которых и Ваш труд, формировать многомерную картину своего (для каждого из нас) восприятия 'как все же было на самом деле'.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[20 февраля 2014 г., 08:28](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1392877688289#c5560473801336356718)

 Любая информация, заставляющая думать, полезна, спасибо.

9. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [29 марта 2014 г., 03:59](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1396058368289#c5141230095685565156)

 Вот со школы ещё помню Дмитрий Донской и Александр Македонский.А может быть не Македонский,а Михайлович Донской,ну то есть Александр сын Михаила Донской.И такое ощущение ,что прямые кровные родня.Может были ещё Донские,только стертые.???Андрей.Петровск-Забайкальский.


10. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [21 апреля 2014 г., 15:51](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1398084711119#c3209350442431964708)

 Одним Файлом Бес-платно - на: http://yadi.sk/d/NQ-TC91JEFYZe ;
[http://yadi.sk/d/XZpp8XKTEF9JJ]
  О - Воскрешении - ВсеЯСветной Грамоты Славян, Культуры и Цивилизации
 - Перво-Начальных Человечества - или С-Начала - Любовь и Нравственность - Единого Творца - Надо Вос-Становить?!..
 - И От Себя Ещё О - Воскрешении и Объ-Единении Славян С-Начала Через
 Их Перво-Начальные - Язык, Письменность, Культуру и Цивилизацию - Человечества или С-Начала - Любовь и Нравственность - Единого
 Творца - Надо Вос-Становить?!.. - И Знаю Точно Патриот - Не идиот - Но Только Таким Реально Может
 Стать Только Тот - Кто Реально и Постоянно Ощущает - Закон Единства Духа Любви от Единого Творца и Вседержителя - Абсолютно Для Всех,
 Всего и Всегда!!..
 - на -
[http://xn--80auhofkt.xn--p1ai/
 Дискуссионный клуб НОД. Выпуск 16. Всеясветная грамота
 Гости дискуссионного клуба: Ананий Федорович Шубин-Абрамов
 Владимир Викторович Кладинов Ведущий: Александр Богомолов
 Дата: 08.03.2014 21:34:11 / Тэги: нод, Дискуссионный клубПОДРОБНЕЕ ->>>
 - и на -
[http://xn--80ajoghfjyj0a.xn--p1ai/georgiy-sidorov-voinskie-traditsii-ariev-uchebnyy-film
 Георгий Сидоров. Воинские традиции ариев. Учебный фильм

 • Видео-приложение к учебнику по древнеславянскому боевому искусству Уникальный фильм, в котором хранитель древней славянской боевой традиции Георгий Сидоров лично показывает основы воинского искусства наших предков. Базовые стойки, систему передвижений и ударов, а также не имеющую аналогов технику парирования атак соперника. Здесь вы найдёте азы рукопашного боя и боя с оружием. Фильм является видеоприложением к книге "Воинские традиции ариев". Приобрести учебник по древнеславянскому боевому искусству можно здесь:
[http://konzeptual.ru/products/voinskiie-traditsii-ariiev Концептуал ТВ
 - и - Научные Факты - о Мировых Славянских Пра: -Языке, -Письменности и -Истории и Кто Кроме Нас - Вспомнит?!.. - как:
 - Древне-Славянская Исконная Азбука Предков как Перво-Источник ВСЕХ Азбук Мира, - и -
 - Пособие как создать Славянскую Перво-Начальную в Мире Азбуку для Вашего ребёнка и для взрослых!
 (- Надеюсь - Это Имеет Одну из Перво-Очередных Задач! - и Если Захотите То Сами Пожалуйста Опубликуйте как Положено! - Спаси-Бог
 Вас - по Русски!) - Ссылка на: - -
[http://www.programotu.ru/
[ - http://newvesti.info/azbuka-slavyanskaya-azbuka/ -
 http://newvesti.info/kak-sdelat-slavyanskuyu-azbuku-dlya-vashego-rebyonka/ - и также - на:
 - http://xn--80ajoghfjyj0a.xn--p1ai/do-kirilla-i-mefodiya-na-rusi-sostavlyali-yuridicheskie-dokumenty
 - Одним Файлом Бес-Платно - на: http://yadi.sk/d/NQ-TC91JEFYZe ;
[http://yadi.sk/d/XZpp8XKTEF9JJ
 - ССЫЛКИ - для любого уровня образования: верующих и атеистов = никакой мистики!..

11. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Сергей[26 июля 2014 г., 01:33](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1406327625980#c6127052148267129084)

 Спасибо, Сергей! Огромная работа! Очень интересно!

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Unknown](https://www.blogger.com/profile/06413827473519021304)[21 марта 2015 г., 20:44](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1426963490978#c568062861593363423)

 Вообще то перед Elanicus стоит S. получается S.Elanicus S это сокращение от Sin, по нашему что то вроде грязное место, болотистое место. Там же рядом есть S.Bothnicus сейчас это называется Ботнический залив А вот что такое S.Elanicus, мне неизвестно.


14. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [28 апреля 2015 г., 00:08](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1430168904234#c2027339483143002625)

 ..."Вот тут и начинаешь понимать Носовского и Фоменко, прислушиваться к Левашову и Кунгурову..." Точно! Ко мне приходят такие же мысли. Слишком много нестыковок в истории и вопросы, вопросы...! Благодарю, Сергей, интереснейшие открытия, гипотезы. Прекрасная, огромная работа! С нетерпением жду твои новые публикации.


15. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [29 апреля 2015 г., 17:26](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1430317614896#c1066497843473343061)

 Уважаемый Сергей!!! Почему Вы решили, что античная карта Ортелиуса демонстрирует 5-6 век н.э.? Это гадание на кофейной гуще. По логике вещей сам период должен быть указан на карте, в противном случаи её ценность равна нулю как исторический документ. И она имеет точно указанную дату, а именно CVM , т.е 895г.н.э. На карте есть дата (в античных числах CVM) и есть в арабских "1595" , т.е 600лет разницы. Таким образом в 895г н.э , в античной Европе ещё властвовали кельты (Celtae). Похоже, что Фоменко не так далёк от реального смещения шкалы истории. С уважением Александр.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[14 августа 2015 г., 11:02](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439539350702#c8619547598389585775)

 Спорить не буду. Брал датировку официальных историков.



16. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [knigoboz](https://www.blogger.com/profile/05327650306528641331)[13 августа 2015 г., 22:37](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439494624994#c7235779343225336840)

 Здравия! Жаль, что карты не входят в формат шаблона. Вы пробовали редактировать шаблон страниц, чтобы сделать его шире? Редактор шаблона позволяет настроить шаблон по своему усмотрению.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[14 августа 2015 г., 11:01](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439539260987#c5141297551378810669)

 Тогда летит форматировка текста. Да и не все мониторы имеют разрешение больше 1920Х1080 точек на дюйм.



 2. ![](History, who are you, a high society lady or a corrupt girl_files/Presentazione252B400px252B7252B.jpg)

 [Olga Maria Lydia](https://www.blogger.com/profile/01305071047479780514)[3 января 2020 г., 17:34](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1578065681448#c61216378676815258)

 Фальшивые карты нельзя в хорошем качестве выкладывать, будет же видео где подделано, где подтерто, где приписано.



17. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [14 августа 2015 г., 01:11](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439503902370#c4668584500529469531)

 РПЦ наверное волосы на заднице рвёт от таких фактов))) Кто же теперь будет вместо варваров на деревьях сидеть?

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[14 августа 2015 г., 11:14](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439540059168#c3668585038675383742)

 Сейчас РПЦ подменяет историю ещё круче. Как там у Зюганова:
 Христос был первым коммунистом. Сталин - Моисеем, со своим депортациями. Милонов, Мизулина, Миронов, Зюганов, Чуркин и Лавров и др... почти апостолы. Смотрите сами:

[https://youtu.be/unJ7oMgGh3I](https://youtu.be/unJ7oMgGh3I)

[https://youtu.be/p8A4JxCuX7c](https://youtu.be/p8A4JxCuX7c)

[https://youtu.be/TWZnxVwXj_o](https://youtu.be/TWZnxVwXj_o)


 2. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [knigoboz](https://www.blogger.com/profile/05327650306528641331)[14 августа 2015 г.,
 20:43](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1439574194676#c8313166437216545576)

 Присутствие Зюганова в партии - это и показатель того, что это за партия.

 Присутствие Зюганова - это лакмусовая бумажка зрелости нашего общества и эта бумажка показывает, что общество наше, увы, незрело. Присутствие Зюганова - это показатель того, что наше общество утеряло Различение (Добра и Зла) и даже не заморачивается проблемой обретения этого Различения.
 А потому - эпоха безвременья, смуты пока продолжается.


18. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Unknown](https://www.blogger.com/profile/08528942491094958341)[16 сентября 2015 г., 13:39](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1442399947639#c4958046834360531044)

 Настоящая история лежит за гранью любых политических и геополитических передряг. Истина - всегда имеет духовные корни, так и история: ее корни надо искать, начиная с духовных доктрин, которые рано или поздно приведут искателя к Изначальной Гиперборейской Традиции. Познавая ее Принципы, искатель обнаружит, что они - несказанно древнее Ведической традиции, ибо изначальны и относятся ко времени, когда человечество еще было абсолютно свободно и являлось фактически также абсолютным властителем Вселенной, заселив ее огромные нескончаемые (?) пространства. Но это уже внеземная история нас - земного человечества.



19. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Анатолий Скала[3 января 2016 г., 20:15](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1451844937130#c4681066366628653860)

 Спасибо! Одно непонятно: так ли уж было важно в захваченном Государстве приложить столько трудов для переписания истории? Разумеется, нестыковок в истории тысячи. Фоменко с Носовским тоже много сумели найти их. Огромнейший труд. Подобные нестыковки и в археологии. Хотя с ними авторы расправляются проще: все современные вещи, ботинки, часы, велосипеды считают работами мастеров давно сгинувших цивилизаций. Подход мудрый. Но вряд ли единственный, а тем более, вряд ли верный. Автор ни разу не пробовал объяснить все перечисленные стыковки какой-нибудь катастрофой? К примеру возьмём глобальный сдвиг времени плюс к нему сдвиг земельных пластов. Ведь при них тоже могут остаться целыми и какие-то старые карты и даже куски государств, на них прежде существовавших? Тогда и "лето без лета" в 1816 года найдёт объяснение. С уважением Анатолий.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[9 января 2016 г., 12:03](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1452333819687#c4207812652459317677)

 Почему-же. Автор не только катастрофу описал, автор ещё целый термин выдумал - Каспаральское море, погуглите, у меня тысячи перепостов этой статьи.



20. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Unknown](https://www.blogger.com/profile/02640315764588855361)[22 июля 2017 г., 23:38](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1500755880882#c4545188184280247388)

 Лютий пипец.А теперь скиньте сылочку на первоисточники карт.Мне как человеку пол жизни работающему с графическими редакторами будет забавно посмотреть на это.

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[2 августа 2017 г., 11:54](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1501664047972#c5335401586444274881)

 Я вам ещё больше карт подкину, там они подписаны, можете проверить сами. http://atlantida-pravda-i-vimisel.blogspot.com/2013/12/blog-post_1.html


 2. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Unknown](https://www.blogger.com/profile/01107541955650632936)[8 сентября 2018 г., 02:44](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1536363895600#c2115875157353560720)

 "Никакой Римской Империи в первом веке нашей эры не было... Тот факт, что этот картограф признан и весьма почитаем современными историками, не позволит им же его отрицать" Поглядите Виасат Хистори про древний Рим, сплошь опровержения Ортелиуса. : "Так вот, эта карта составлена А. Ортелиусом в 1595 году" Ну да карта составленная в средневековье моментально опровергает все труды тысячи современных учёных во всём мире, от археологов, до историков о Римской империи нашей эры. : "А это означает, что на карте отображена Европа не позже 5-6 века нашей эры! И... что видно на этой карте!? На ней нет НИ ЗАПАДНОЙ, НИ ВОСТОЧНОЙ РИМСКИХ ИМПЕРИЙ!! А согласно современной "истории" они должны были быть и процветать!" Это западная-то процветать была должна? Вы откуда это берёте? Она уже в 4 веке страдала во всю от набегов готов. Почитайте про битву при Адрианополе, после чего З.Р.И. просто стала разваливаться на части.

 "There was no Roman Empire in the first century A.D.. The fact that this cartographer is recognized and highly revered by modern historians does not allow them to deny him" Look at Viassat History about ancient Rome, all denials of Ortelius. "Now, this map was made by A. Ortelius in 1595." Well, a map made in the Middle Ages instantly disproves all the work of a thousand modern scholars around the world, from archaeologists to historians about the Roman Empire of the AD. : "And that means that the map shows Europe no later than the 5th or 6th century AD! And... what does this map show!? It shows neither the Western nor the Eastern Roman Empires!!! And according to modern "history" they should have been and prospered!" Was the Western one supposed to be prospering? Where are you getting that from? It was already suffering in the 4th century from the invasions of the Goths. Read about the battle of Hadrianople, after which Z.R.I. just began to fall apart.

 3. ![](History, who are you, a high society lady or a corrupt girl_files/Presentazione252B400px252B7252B.jpg)

 [Olga Maria Lydia](https://www.blogger.com/profile/01305071047479780514)[3 января 2020 г., 17:12](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1578064344234#c3502969760447175001)

 Романская империя. Не Римская. Только дикари называют Романскую империю римской и потом ничего не понимают ни в картах, ни в мировой культуре. Романская империя прекратила существовать в 1806 году. Набеги и войны это постоянное явление в истории человека и не влияют на процветание или упадок. Россия с первого момента своего появления в позднем средневековье была упадочной, зато до сих пор воюет и является злом и агрессором в мире. Романская империя подразделялась на части: на западную и восточную. Не было отдельных восточной и западной империй. Император был один на всю империю. В восточной части были только короли (базилеусы).



 4. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [Hanzo](https://www.blogger.com/profile/01647382333923347020)[13 июля 2020 г., 20:52](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1594662770974#c7908113951760076645)

 Ты где живёшь? Чморина, сдрисни в свой пиндостан, который уничтожил коренное население Америки. По моему только там где появляются пиндосы, начинается война, Большие любители они стравливать народы, живущие вместе, а потом прибирать всё к своим рукам, Югославия, Ливия, Ирак, Сирия, Вьетнам и др. Нация генетического мусора в мире.

Where do you live? You're a pussy, go back to the country that wiped out the Native American population. I think it's only wherever the pindos show up that war breaks out, they're big fans of pitting nations against each other and then getting their own hands on everything, Yugoslavia, Libya, Iraq, Syria, Vietnam, etc. A nation of genetic garbage in the world.

21. ![](History, who are you, a high society lady or a corrupt girl_files/blank.gif)

 Anonymous [20 февраля 2019 г., 13:46](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1550663178843#c4211683833595452771)

 Поддельную карту "украины" удалите, не позорьтесь. Достаточно того, что в картуше написано Vkrania, а в поле карты - Ukraina - явно вписано позднее, топорно, другим шрифтом и исходное название Russica Rubra даже не затерто, фу... грубо и тупо подделано

The fake map of "Ukraine" should be removed, don't embarrass yourself. It's enough that in the cartouche it says Vkrania, and in the map field - Ukraina - clearly inscribed later, clumsily, in a different font and the original name Russica Rubra is not even rubbed out, ugh... Rude and stupidly forged

 1. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

 [ZigZag](https://www.blogger.com/profile/07680738609904461754)[22 марта 2019 г., 13:10](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1553253047137#c6062202832708141545)

 [http://fakeoff.org/history/rus-ukraina-na-srednevekovykh-kartakh](http://fakeoff.org/history/rus-ukraina-na-srednevekovykh-kartakh)

 2. ![](History, who are you, a high society lady or a corrupt girl_files/Presentazione252B400px252B7252B.jpg)

 [Olga Maria Lydia](https://www.blogger.com/profile/01305071047479780514)[3 января 2020 г., 17:15](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1578064535893#c4029817909937322950)

Ukraine could not be on medieval maps. Since she was Kievan Rus. After the occupation by Muscovy, Kievan Rus was part of Muscovy, and when Muscovy called itself Russia, it passed as part of Russia. It is clear that you yourself are drawing idiotic cards. You steal time from people to read your idiotic delusions.

 3. ![](History, who are you, a high society lady or a corrupt girl_files/zFdxGE77vvD2w5xHy6jkVuElKv-U9_9qLkRYK8OnbDeJPtjSZ82UPq5w6hJ-.png)

24. ![](History, who are you, a high society lady or a corrupt girl_files/Presentazione252B400px252B7252B.jpg)

 [Olga Maria Lydia](https://www.blogger.com/profile/01305071047479780514)[January 3, 2020 5:03 PM](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_516.html?showComment=1578063816257#c7776499297045589288)

The maps are mostly fake. Especially the map from the 18th century, which shows Ukraine. Muscovy occupied Kievan Rus in 1654 and only after that changed the name Rus (Russia) to Ukraine. After the occupation, Kievan Rus figured as part of Muscovy, and when Muscovy renamed itself Russia, then as part of Russia. There could be no Ukraine on the map of the 18th century. So the card is a fake of some Russian falsifier.

For your information, Russians are only the indigenous inhabitants of the Moscow province. They renamed themselves from Rus to Russians after the occupation of Kievan Rus.

The Roman Empire did exist, there was the Roman Empire. It is displayed on all non-fake maps. The Roman Empire ceased to exist in about 1806. On one of the maps Novgorod is registered and there is no Kiev. This is also a fake card. When Kiev was already the capital of Kievan Rus and the Catholic metropolitanate of all Rus (there were several Russes, not one), neither Novgorod nor Moscow yet existed, in their place were villages, they were an undeveloped periphery of Kievan Rus, outskirts - hence the Muscovites (Russians) and the quirk of calling Kievan Rus / Ukraine a suburb. Novgorod was most likely a Karelian, Estonian or Finnish city, after the occupation it was renamed Novgorod, just as the occupied part of Narva was renamed Ivan-Gorod. Only on the maps falsified by Russian psychos there is no Kiev, Roma, Jerusalem, Alexandria and other capitals of European civilization and culture. Roma is a biblical city, along with three others; important cities described in the Bible do not indicate on the maps only Russian savages who are not familiar with world culture. Previously, the borders of countries changed every 30 years due to constant wars. Using photo maps, you should indicate the source where they were taken from, so that everyone can compare the original and check its origin. You yourself could draw maps for this article, many errors in the maps show that it was a Russian subject who was not familiar with the basic concepts of world culture and history who falsified them. Novgorod, which did not actually exist, is a real triumph of savagery. Start learning languages ​​and you will gain access to world culture and history, as well as to the residual information about the history of Rus and Muscovy/Russia.




