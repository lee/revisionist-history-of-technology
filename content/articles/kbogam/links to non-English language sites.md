title: Links to Google-translated kbogam articles
date: 2021-07-03 15:06
modified: 2022-01-27 19:22:49
category: 
tags: links
slug: 
authors: Pavel Verkhov (Alexander Alekseev)
summary: Links to Google-translated articles by kbogam (Pavel Verkhov).
status: published

<!-- RSS feed notes:
- vk.com RSS feeds created using [https://www.mysitemapgenerator.com/rss/create.html](https://www.mysitemapgenerator.com/rss/create.html)
- yandex.ru RSS feeds created with the same tool
- blogspot RSS feed instruction from https://oscarmini.com/discover-rss-feed-address-of-a-blogger-blogs/
-->

### From [Reading Room](https://vk-com.translate.goog/page-168758205_55625230?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Shh, Just Don't Tell Anyone](https://vk-com.translate.goog/page-168758205_55686423?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Underground Friends of Jesus](https://vk-com.translate.goog/@-168758205-sekretnoe-uchebnoe-zavedenie-dlya-sozdatelei-mirovoi-istorii?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Freemasons, Illuminati, Reptilians? Into the Fire! More About the Society of Jesus](https://vk-com.translate.goog/@-168758205-masony-illuminaty-reptiloidy-v-topku-a-vot-obschestvo-iisusa?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Chronology](https://vk-com.translate.goog/page-168758205_55683110?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [DIY History Check](https://vk-com.translate.goog/@-168758205-1-shag-i-2-klika-chtoby-uznat-nastoyaschuu-istoriu-chelovech?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The Forging of History. Where is this Monster?](https://translate.google.com/website?sl=ru&tl=en&u=http://vk.com/@-168758205-podlog-istorii-gde-etot-monstr)
    - [Antique America. Was There One? Or Was There Not?](https://vk-com.translate.goog/@-168758205-antichnaya-amerika-kak-takoe-vozmozhno-shkolnye-uchebniki-ne?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The Very Last, Penultimate Day of Pompeii](https://vk-com.translate.goog/@kbogam-spektakl-po-imeni-pompeya?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [This, if You Don't Understand, is Authentic Chicago](https://translate.google.com/website?sl=ru&tl=en&u=http://vk.com/@-168758205-eto-esli-kto-ne-v-kurse-autentichnyi-chikago)
    - [Want to Change History But Don't Know Where to Start? This way...](https://vk-com.translate.goog/@-168758205-ne-verite-v-podlog-istorii-vcheraposmotrite-na-podgotovku-se?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Silence of the Gods](https://vk-com.translate.goog/page-168758205_55740920?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [When Was Belief in Jesus Implanted in Russia?](https://vk-com.translate.goog/@-168758205-kogda-na-samom-dele-v-rossiu-prishlo-hristianstvo?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Stuffed Cabbage - Which Pecked the Priest in the Butt](https://vk-com.translate.goog/@-168758205-golubec-golubec-ne-kusai-popa-v-popec?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The Secret of the Real Old Believers](https://vk-com.translate.goog/@-168758205-ot-golubcov-k-nemyslimomu-vse-bylo-ne-tak-kak-vy-dumali-drev?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Faith Will Not Leave You Hungry if You Read the Instructions Correctly](https://vk-com.translate.goog/@kbogam-kak-izvlech-metodicheskoe-posobie-po-zemledeliu-iz-blagoi-ve?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [versions](https://vk-com.translate.goog/page-168758205_55750394?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The Missing Gold From the Hermitage, Crown Prince Piotrovsky and the Strange Silence of Authoritative Sources](https://vk-com.translate.goog/@kbogam-ischeznuvshee-zoloto-ermitazha-naslednyi-princ-piotrovskii-i?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Immortality, Climate and Diggers. Naked](https://vk-com.translate.goog/@-168758205-kto-meshaet-nam-stat-bessmertnymi-golymi-zemlekopami?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [When Do Stone Statues Come to Life?](https://vk-com.translate.goog/@kbogam-ozhivshie-kamni-derevya-i-sharlatany?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Is the Sun Cooling or Overheating?](https://vk-com.translate.goog/@-168758205-goregore-krokodil-nashe-solnce-proglotil-nu-pochti?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [How people were created. History of Darwin, God and Aliens. But what's with the pig?](https://vk-com.translate.goog/@kbogam-kak-sozdavali-ludei-istoriya-darvina-boga-i-inoplanetyan-tol?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [We talk about distant galaxies, but we ourselves do not know what is under our feet?](https://vk-com.translate.goog/@kbogam-est-li-u-nas-svedeniya-o-vnutrennem-ustroistve-nashei-planet?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Divine Scalpel](https://vk-com.translate.goog/page-168758205_55683541?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Delicious Degradation](https://vk-com.translate.goog/@kbogam-eda-i-degradaciya?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [To Take Drugs or Not to Take Drugs](https://vk-com.translate.goog/@kbogam-pochemu-razgonyat-mozg-zaprescheno-tolko-v-rossii-i-v-pare-s?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [100% Cancer Cure That Never Happened](https://vk-com.translate.goog/@-168758205-lechenie-raka-kotorogo-kak-by-net?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Is Smoking Dangerous for Your Diseases?](https://vk-com.translate.goog/@-168758205-kurenie-opasno-dlya-vashihnedugov?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Where Have They Hidden the Mass Graves of Smokers?](https://vk-com.translate.goog/@-168758205-stroinye-ryady-mogilok-kurilschikov-gde-ih-pryachut?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Important! Know How to Recognise a Drug Addict](https://vk-com.translate.goog/@-168758205-vazhno-umei-raspoznat-narkomana?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Who Will Reach the End Faster?](https://vk-com.translate.goog/@-168758205-kto-bystree-dobezhit-do-konca-zhizni?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Which Cancer do You Like Best? 5G or the Old Fashioned WiFi?](https://vk-com.translate.goog/@kbogam-rak-mozga-luchshe-razvivaetsyaesli-razgovarivat-po-mobilnomu?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Science](https://vk-com.translate.goog/page-168758205_55750423?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Electricity magic, still magic!](https://vk-com.translate.goog/@kbogam-vy-ponyatiya-ne-imeete-chto-istochaet-vasha-rozetka-uchenye?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Forbidden Energy](https://vk-com.translate.goog/@kbogam-zapreschennaya-energetika-kak-ubivali-efir?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Nature Needs More CO2](https://vk-com.translate.goog/@-168758205-vyrabatyvaete-co2-daite-dva-perevod-amerikanskoi-stati?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Who better to be, God or Engineer?](https://vk-com.translate.goog/@kbogam-kem-luchshe-byt-bogom-ili-inzhenernom?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [On the question of your car, gas prices and various collusions, which, as you know, do not exist](https://vk-com.translate.goog/@kbogam-k-voprosu-o-vashem-avtomobile-cenah-na-benzin-i-razlichnyh-s?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Suppressed invention(s)](https://vk-com.translate.goog/@kbogam-podavlennye-izobreteniya?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Not One Step Away from the Official Version of History](https://vk-com.translate.goog/page-168758205_55699321?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Cocaine and hundreds of mummies against historians](https://vk-com.translate.goog/@kbogam-drevnyaya-egipetskaya-mumiya-kokos-i-istoriki?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Is there any reason to respect the official Roman Empire?](https://vk-com.translate.goog/@kbogam-drevnii-rim-i-ego-imperiya-pustyshki?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Money, Money, Money!](https://vk-com.translate.goog/@kbogam-drevnerimskaya-vakhanaliya-na-kupurah-19-veka?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [USA and their monetary thunderbolts!](https://vk-com.translate.goog/@-168758205-ne-ochen-odetye-ludi-na-kupurah-puritanskoi-ameriki?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Russian serfdom and the lynching of African Americans in the United States](https://vk-com.translate.goog/@-168758205-russkoe-krepostnoe-rabstvo-i-linchevanie-afro-amerikancev-v?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The First World War at the peak of progress - sticks, stones, sling!](https://vk-com.translate.goog/@kbogam-pervaya-mirovaya-voina-na-pike-progressa-palki-kamni-prascha?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Why did the Allies raze Germany to the ground?](https://vk-com.translate.goog/@kbogam-za-chto-souzniki-sravnyali-germaniu-s-zemlei?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [The USA We Have Lost.  Photo report from the places of death of the heritage](https://vk-com.translate.goog/@kbogam-ssha-kotorye-my-poteryali-fotoreportazh-s-mest-gibeli-nasled?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [42 Riddles](https://vk-com.translate.goog/page-168758205_55706094?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [And Yet More Tartaria...](https://vk-com.translate.goog/@kbogam-i-vse-taki-tartariya?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Your Tartaria is my Tartaria. How are we going to divide the earth?](https://vk-com.translate.goog/@kbogam-tvoya-moya-tartariya-kak-zemlu-to-delit-budem?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Peter the Great and the mouth of the Neva. Reconnaissance on the ground](https://vk-com.translate.goog/@-168758205-volshebnaya-stroika-peterburga?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [ATOS PORTOS ARAMIS AND ELECTRONIC](https://vk-com.translate.goog/@-168758205-atos-portos-aramis-i-elektronik?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Russian and American ships cause the death of Messina?](https://vk-com.translate.goog/@-168758205-sovmestnaya-russko-amerikanskaya-operaciya-po-unichtozheniu?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Who sank the Titanic and how?](https://vk-com.translate.goog/@-168758205-kak-i-zachem-zatopili-titanik?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Did we build the Titanic?](https://vk-com.translate.goog/@-168758205-a-my-li-postroili-titanik?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Ice and fire of the Titanic!](https://vk-com.translate.goog/@-168758205-mog-li-titanik-sgoret?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Public toilets can reveal a lot about your past.](https://vk-com.translate.goog/@kbogam-obschestvennye-tualety-mogut-mnogie-raskryt-o-vashem-proshlo?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

[For Any Proof](https://vk-com.transla e.goog/page-168758205_55706039?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

[Collected reasons to doubt orthodox human history](https://vk-com.translate.goog/@-168758205-a-che-ne-tak-otkuda-kipish?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

### Various:

- [Questions to Historians, Scientists and Godsi 1](https://vk-com.translate.goog/kbogam?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
- [Questions to Historians, Scientists and Godsi 2](https://vk-com.translate.goog/kbogam?w=address-168758205&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
- [93 Articles](https://vk-com.translate.goog/@kbogam?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Russia, Europe and the USA against the old world. Fire and sword.](https://vk-com.translate.goog/@kbogam-rossiya-evropa-i-ssha-protiv-starogo-mira-ognem-i-mechom?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Roman Cottage in the Village](https://vk-com.translate.goog/@kbogam-domik-v-derevne?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Vatican Serpentarium](https://vk-com.translate.goog/@kbogam-vatikanskii-serpentarium?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [Magical, and possibly also enchanted, Chinese mirrors](https://vk-com.translate.goog/@kbogam-volshebnye-a-vozmozhno-po-sovmestitelstvu-i-zakoldovannye-ki?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
