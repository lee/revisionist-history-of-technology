title: Chronologists
date: 2023-03-04
modified: 2023-03-11 12:45:48
category: 
tags: forged history
slug: 
authors: Various
summary: This page collects together links to chronology investigators' websites plus links to Google-translated pages.
status: published


- Herbert Illig [http://www.fantomzeit.de/](http://www.fantomzeit.de/), [*(English)*](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://www.fantomzeit.de/)

    > His study of the dark centuries in medieval times brought him to the conclusion that a 297-year phantom period had been inserted between the years 614 and 911.

- Gunnar Heinsohn [https://q-mag.org](https://q-mag.org)

    > Studying archeological stratifications, he came to the conclusion that there is not enough stratigraphy to support the standard history of the first millennium. A gap of 700 years (between 230 and 930) is not supported by archeology.

- Uwe Topper [http://www.chronologiekritik.de/](http://www.chronologiekritik.de/) [*(English)*](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://www.chronologiekritik.de/)

    > Topper came to the conclusion that all ancient history, before the renaissance [(1300-1699)](https://www.history.com/topics/renaissance/renaissance), has been invented, making use of the real Greco-Roman civilization and some real Greco-Roman literature dating from the period immediately before the renaissance.

- Christoph Pfister [https://www.dillum.ch/](https://www.dillum.ch/), [*(English)*](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://www.dillum.ch/) and [Christoph Pfister - wikiwand](https://www.wikiwand.com/de/Christoph_Pfister), [*(English)*](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://www.wikiwand.com)

    > Pfister suspected there was a reformulation of human knowledge of history starting just before 1800 (the 19th century). And that our current understanding of human history originates from that reformulation. Pfister suspected that events from the three centuries before the reformulation (16th, 17th and 18th centuries) were copied, the copies then 'smeared' or 'blurred' by relabelling the characters and relocating the events. Finally, the copied events were redated into the 1,000 years before 1500. The Iron Age, Bronze Age, Stone Age and geological prehistory were created to extend the copy back as 'prehistory'.
    
    
    > Pfister believed the 1800 reformulation took place over a thirty-year period starting near the end of the 17th century. He suspected an earlier break had also occurred just over 100 years earlier (in the middle of the 16th century). He thought that break had also lasted about thirty years.
    
    > The first break marks the start of the Reformation and the second break marks the end of the Reformation and the start of the Modern era.

    See [The Historical Timeline](https://www.dillum.ch/html/1_historical.timeline_historische_zeitasche_2020.htm), [*(English)*](https://www-dillum-ch.translate.goog/html/1_historical.timeline_historische_zeitachse_2020.htm?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=en-GB)

    <!-- - [https://www.dillum.ch/html/geschichtskritik_chronologiekritik_manifest_2016_christoph_pfister.htm](https://www.dillum.ch/html/geschichtskritik_chronologiekritik_manifest_2016_christoph_pfister.htm) [YT: History and Chronology Critic](https://www.youtube.com/watch?v=BymNrwQxCRQ) -->

- Anatoly Fomenko [http://chronologia.org/](http://chronologia.org/), [*(English)*](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://chronologia.org/)

    - From [How Long Was the First Millenium?](https://www.unz.com/article/how-long-was-the-first-millenium/):

    > drawing from the previous work of Russian Nikolai Mozorov (1854-1946), Fomenko and Nosovsky show a striking parallel between the sequences Pompey/Caesar/Octavian and Diocletian/Constantius/Constantine, leading to the conclusion that the Western Roman Empire is, to some extent, a phantom duplicate of the Eastern Roman Empire.
    > 
    > ...according to Fomenko and Nosovsky, ... Constantinople (Eastern Roman Empire) was founded around the tenth or eleventh century AD, and Rome (Western Roman Empire), 330 or 360 years later, i.e. around the fifteenth or sixteenth century AD.

    - [YT: explains Fomenko/Nosovsky's statistical detection of repeating history](https://www.youtube.com/watch?v=HN3S8ncDehY&list=PLCOZWPrcz4SKwqQ1I0Gm6krti4yTzuDlA&index=2&t=0s)

Compiled from [revised chronology summaries](https://stolenhistory.net/threads/gunnar-heinsohn-1947-2023.6480/post-120760)
