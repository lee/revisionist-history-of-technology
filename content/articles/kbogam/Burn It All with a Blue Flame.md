title: Burn It All with a Blue Flame
date: 2019-04-03
modified: 2022-01-27 17:09:50
category: 
tags: technology; cannons
slug: 
authors: Pavel Verkhov (Alexander Alekseev)
summary: So, we have a magical fire that consumes and destroys stones with bricks. Fortunately, this is not its only unique property. In order not to get bored, during the genocide of buildings, it amused itself by, sometimes diligently, bypassing trees, in some cases even leaving the foliage intact.
status: published
local: burn_it_all_with_a_blue_flame_files
from: https://zen.yandex.ru/media/outthere/gori-ono-vse-sinim-plamenem-5ca46c508f7b6100b3fd11ad
originally: Burn It All with a Blue Flame.md

#### Translated from: 

[https://zen.yandex.ru/media/outthere/gori-ono-vse-sinim-plamenem-5ca46c508f7b6100b3fd11ad](https://zen.yandex.ru/media/outthere/gori-ono-vse-sinim-plamenem-5ca46c508f7b6100b3fd11ad)

# Burn it all with a blue flame!

I think that everything is already known, but I will never tire of repeating it. Here's a castrated version of the article. The pictures are small, their number is limited, and in general, there is a problem with multimedia.

But that is not a problem for people who can click a mouse. For example, here: [Questions for historians, scientists and gods](https://vk.com/kbogam). Here you will find everything and more. There are more than four thousand pictures alone. And all of them are mysterious and interesting. In short, Welcome.

And by the way, just in case. As a punishment, my home Anunnaki will not save those who were too lazy to join our VKontakte group when his relatives arrive on Earth for a big harvest. Think it's a good deal!

The topic of the fires of the late 19th century is, in principle, immense. Of these topics, there are so many separate topics that I'm only going to make a separate note about the main items. Each of them is worthy of its own detailed elaboration, from which, naturally, further stories with branching horns of their own sequels could emerge.

It is impossible to end each line with the murder of the hero or his happy wedding, as art writers do, and if you do not finish, you will perish yourself, under the weight of a blossoming historical crown and a billion living, but very heavy Chinese. Here I even got confused in the description of the description!

<!--
![Burn it all with a blue flame!](https://avatars.mds.yandex.net/get-zen_doc/1804213/pub_5ca46c508f7b6100b3fd11ad_5ca46c5518430d00b31ed4eb/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/1.jpeg#resized)

Everyone, I hope, read "Cat's House" as a child? Who has not read, relax, all the same there at the end everything burned out. To the smallest. At the same time, the cat itself survived. There is a loss of property and the formation of a new unit in need, ready to work for food. We will return to this nuance later.

A fairy tale, as they say, a lie, but there is a hint in it! On multiple fires of the past years.

Do you know how many there were? Not? And nobody knows! In all available lists, only the "Great" ones are mentioned. This is when the whole city burned down in fig! A large city, such as a capital. Smaller fires, where not very central cities or large settlements burned down, but not completely, it seems, at all, no one considered.

For example, what was counted on one American site. This list is never complete, even for American settlements, of which there are the majority in the place of birth of the list. It does not matter. There is no purpose to compile an encyclopedia, there is a desire to show the scale.

<!--
![](https://avatars.mds.yandex.net/get-zen_doc/176438/pub_5ca46c508f7b6100b3fd11ad_5ca46c55c2662100b3130902/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/2.jpeg#resized)

Auto English translation (from Yandex):

- 1864 - Atlanta, Georgia, burned after the time allotted for the evacuation of citizens by order of William Tecumseh Sherman
- 1865 - (Fire protection of?) the city of Richmond, Virginia, is burned by the retreating Confederate
- 1865 - Columbia, South Carolina, was burned during occupation by troops under the command of William Tecumseh Sherman
- 1866 - Great Fire of Portland, Maine
- 1868 - Auerbach in der Oberpfalz, Bavaria. As a result of arson, 107 houses and 146 other buildings were destroyed; 4 were killed
- 1870 - The Great Fire in Constantinople, Turkey
- 1870 - Fire in Medina, Ohio, consume everything but two blocks from the business district, almost destroy the entire city
- 1871 - Great Fire in Chicago, Illinois
- 1871 - Great Fire - Fighting In Peshtigo, Wisconsin
- 1871 - The Great Urbana Fire, Ohio
- 1872 - The Great Boston Fire of 1872, destroyed 776 buildings and killed at least 20 people.
- 1873 - The Great Fire in Portland, or
- 1874 - The fire in the city of Chicago, destroyed 812 buildings and killed 20 people.
- 1877 - Fire in St. John, New Brunswick, destroyed 1,600 buildings
- 1878 - The Great Fire of Hong Kong, destroyed 350 to 400 buildings in more than 10 acres of central Hong Kong.
- 1879 - Hakodate fire, Hakodate, Hokkaido, Japan, 67 people, 20,000 homeless
- 1881 - Thumb(?) fire in Michigan
- 1881 - Ringtheater Fire in Vienna, Austria
- 1886 - Great Vancouver Fire, Vancouver, British Columbia
- 1887 - Cannon Falls fires, MN
- 1889 - The Great Fire in Spokane, Washington
- 1889 - Great Bakersfield Fire - destroyed 196 houses and killed 1 person
- 1889 - The Great Fire in Seattle, Washington
- 1889 - The First Great Lynn Fire, Lynn, Massachusetts, destroyed about 100 buildings
- 1891 - The Great Fire of Syracuse
- 1892 - Great Fire in St. John's, Newfoundland, Canada
- 1893 - Fire in Clarksville, Virginia.
- 1894 - Great Hinckley Fire
- 1894 - Great Fire in Shanghai; destroyed more than 1,000 buildings
- 1896 - Great Fire in Paris, Texas, destroyed most of the city
- 1897 - The Great Fire in Windsor, Nova Scotia, Canada, destroyed 80% of the city
- 1898 - The Great Fire of New Westminster, British Columbia
- 1898 - The Great Fire in Park City, Utah
- 1906 - Great San Francisco Fire and earthquake
- 1914 - Great Fire of Salem, Massachusetts - 1,376 buildings
- 1918 - Cloquet, Duluth and Moose Lake

No need to read it. This is just one example of many lists. Very local, by the way. Outside the United States, the nose is not particularly protruding.

What can be taken from this list: there were a lot of fires, they lasted a very long time and all over the globe. I am glad that at least they did not set fire to each other in a chain reaction.

The scale of the destruction suggests that all the buildings before the catastrophe were apparently not just doused with gasoline, kerosene, which is a substitute, but were also built in advance, from wood impregnated with a combustible composition, or better immediately, explosive materials.

Since the main work on the study of "great" fires was carried out by American enthusiasts, the examples will be overseas. In fact, there is no difference, take any city - everything is a blueprint.

##### The Great Boston Fire of 1872.

The Great Boston Fire of 1872 was the largest fire in Boston and is still considered one of the most costly, in terms of property lost, in American history. The fire began at 7:20 pm on November 9, 1872 in the basement of a trading warehouse at Summer Street 83-87. The fire was localized after 12 hours. Prior to that, he managed to destroy about 65 acres (26 hectares - 2600 acres - 260,000 m) of the center of Boston, 776 buildings and most of the financial district. The damage was $ 3.5 million. Despite this destruction, only thirteen people died in this hell.

<!--
![Those who survived the cataclysm are in pessimism .... (c) Popularly beloved minstrel, however, about another case](https://avatars.mds.yandex.net/get-zen_doc/44972/pub_5ca46c508f7b6100b3fd11ad_5ca46c55134d6700b2a4910b/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/3.jpeg#resized)

Those who survived the cataclysm are in pessimism .... (c) Popularly beloved minstrel, however, about another case...

<!--
![The swept American Mamai, if we operate with allegories.](https://avatars.mds.yandex.net/get-zen_doc/27036/pub_5ca46c508f7b6100b3fd11ad_5ca46c5518430d00b31ed4ec/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/4.jpeg#resized)

The swept American Mamai, if we operate with allegories.

<!--
![P..ts, and Africa is on fire, they said in my childhood.](https://avatars.mds.yandex.net/get-zen_doc/192582/pub_5ca46c508f7b6100b3fd11ad_5ca46c56f4bebc00b35a52cb/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/5.jpeg#resized)

P..ts, and Africa is on fire, they said in my childhood.

We will not draw any conclusions yet, let's look at other outstanding results.

##### The Great Chicago Fire of 1871.

The Great Chicago Fire burned from Sunday, October 8, until early Tuesday, October 10, 1871. The fire killed around 300 people, destroyed about 3.3 square miles (9 km2) of Chicago, Illinois itself, and left more than 100,000 residents homeless.

<!--
![1 of 5Here are 5 photos.  Be sure to flip through and take a close look at the remains.  Useful for conclusions.](https://avatars.mds.yandex.net/get-zen_doc/987771/pub_5ca46c508f7b6100b3fd11ad_5ca46c5521a86300b4ca8b6c/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/6.jpeg#resized)

Here are 5 photos. Be sure to flip through and take a close look at the remains. Useful for conclusions.

##### The Great Seattle Fire of 1889.

The Great Seattle Fire was a fire that destroyed the entire Central Business District of Seattle, Washington on June 6, 1889. The fire worked tirelessly for hours, destroying 25 neighborhoods and causing $20 million in damage ($ 527 million in today's dollars). Not a single person died in the fire. The fire raised streets in Seattle's Pioneer Square area 22 feet (6.7 m) above street level prior to the fire. At the same time, wooden buildings were banned and a single city water supply system was installed.

<!--
![1 of 7 Well, kind of the same.](https://avatars.mds.yandex.net/get-zen_doc/99893/pub_5ca46c508f7b6100b3fd11ad_5ca46c59f4bebc00b35a52cc/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/7.jpeg#resized)

Well, kind of the same.

Let's now take at least one non-American city so that there is no feeling of geographic selectivity.

And then, oops, it turns out that after the invention of photography, it burned exclusively in the New World. No, for the company, of course, burned a little bit in the British Empire. But very weakly - indestructible.

There were also a couple of good torches in China and Japan, but firstly, this is almost the same as America, in our case, and secondly, there, before burning, it often shook, and this smears the authenticity of the experiment.

To powerfully and with a spark, one can only recall Smyrna in 1922, but there the fire looks so man-made from all sides that one cannot rely on the results, as on those produced, only by fire, without the intervention of other human forces.

The question is, what prevented Europe from burning with such a massive pyrotechnic race in another part of the world? Were the construction technologies different? Fire safety? No, on the contrary. Historians say - everything ancient is worth it. For five hundred years or more. For hardwood floors and oak finishes, it's time to get some sun and some blue flames. If someone got wet in the flood, it would seem that it should have already dried up for a demonstration fire and burn no worse than an American remake. In reality - a blazing world and unemployment among firefighters in Europe. In Russia, which, as they say, was through and through bast shoes and wood, and then no one is on fire. One Syzran at the beginning of the century. And the one in question.

Anyway. Let there be selective fire. Probably the wind rose just blew in that direction.

What else is interesting about these fires?

The first and, probably, the main thing is the incredible destruction of brick, concrete and other stone buildings. Fire, fire, but the cobblestones where all fell?

<!--
![1 of 7Pay attention to the "architectural excesses" of burnt buildings.  Again "Ancient Rome" is being destroyed.](https://avatars.mds.yandex.net/get-zen_doc/965902/pub_5ca46c508f7b6100b3fd11ad_5ca46c5972723e00b33207c2/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/8.jpeg#resized)

Pay attention to the "architectural excesses" of burnt buildings. Again "Ancient Rome" is being destroyed.

<!--
![Nice photo.  The fire not only burned all the wooden floors, but also disassembled the masonry brick by brick.  It would be great if still folded evenly, for export or there, for later use.](https://avatars.mds.yandex.net/get-zen_doc/985972/pub_5ca46c508f7b6100b3fd11ad_5ca46c59134d6700b2a4910c/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/9.jpeg#resized)

Nice photo. The fire not only burned all the wooden floors, but also disassembled the masonry brick by brick. It would be great if still folded evenly, for export or there, for later use.

Not so long ago, we already discussed something similar, only there it was attributed to an earthquake and the people were laid down significantly more. Here, apparently, there was no such goal, or the results were well disguised.

<!--
![Remove the pieces of wood and it will be very similar.](https://avatars.mds.yandex.net/get-zen_doc/192582/pub_5ca46c508f7b6100b3fd11ad_5ca46c593aaf7b00b2c2fe8b/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/10.jpeg#resized)

Remove the pieces of wood and it will be very similar.

> _Those who have not read about the Russian-American exercises in the Sicily region can familiarize themselves with:_

> [_Bad weather or Russian-American efforts to destroy Messina?_](https://vk.com/@-168758205-sovmestnaya-russko-amerikanskaya-operaciya-po-unichtozheniu)

So, we have a magical fire that consumes and destroys stones with bricks. Fortunately, this is not its only unique property. In order not to get bored, during the genocide of buildings, it amused itself by, sometimes diligently, bypassing trees, in some cases even leaving the foliage intact.

<!--
![1 of 4 A tree with a crown in the middle of a building scattered on stones.](https://avatars.mds.yandex.net/get-zen_doc/170671/pub_5ca46c508f7b6100b3fd11ad_5ca46c5af4bebc00b35a52cd/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/11.jpeg#resized)

A tree with a crown in the middle of a building scattered on stones.

In general, howls about wooden houses could have been more modest. It's worth looking at the plan for Chicago, which depicts the city before the fire.

<!--
![Maybe I’m not a world famous architect, but I don’t see wooden houses here.](https://avatars.mds.yandex.net/get-zen_doc/192582/pub_5ca46c508f7b6100b3fd11ad_5ca46c5bdbb8aa00b4c4e23a/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/12.jpeg#resized)

Maybe I’m not a world famous architect, but I don’t see wooden houses here.

Now about the ashes themselves. Houses, even badly burnt, look like this today:

<!--
![1 of 3 Black soot on walls from smoke.  The walls themselves, which is important.  And even if everything burned, as at the Burning Men festival, the stone-brick-cement base does not disappear into oblivion, falling apart into pieces.](https://avatars.mds.yandex.net/get-zen_doc/192582/pub_5ca46c508f7b6100b3fd11ad_5ca46c5d72723e00b33207c3/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/13.jpeg#resized)

Black soot on walls from smoke. The walls themselves, which is important. And even if everything burned, as at the Burning Men festival, the stone-brick-cement base does not disappear into oblivion, falling apart into pieces.

What and how should burn so that the following remains of the whole city:

<!--
![1 of 3 Diagonally destroyed walls, crumbling brick, but little soot is visible.](https://avatars.mds.yandex.net/get-zen_doc/1790220/pub_5ca46c508f7b6100b3fd11ad_5ca46c5e5ec13d00b4401b96/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/14.jpeg#resized)

Diagonally destroyed walls, crumbling brick, but little soot is visible.

Strange, some kind of fires. What could burn there if there was nothing left of the buildings? Although there is! There is an example from our time! Have you guessed? Correctly ! Twin Towers and Phenomenon 7!

A little bit of fire and in the trash.

And it is possible without fire. Why arrange a circus!

Yes, but mining every city in this way at the end of the 19th century is too dreary and unnecessary, so it's better to compare it with something less noble and manageable. For example, with Dresden World War II:

<!--
![If close, it almost looks like, and if from a distance, then Dresden suffered less.](https://avatars.mds.yandex.net/get-zen_doc/1602847/pub_5ca46c508f7b6100b3fd11ad_5ca46c5e72723e00b33207c5/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/15.jpeg#resized)

If close, it almost looks like, and if from a distance, then Dresden suffered less.

And if it is absolutely certain, then with Hiroshima and Nagasaki it will be closer.

<!--
![1 of 2 The only difference is that the trees have been removed.](https://avatars.mds.yandex.net/get-zen_doc/95163/pub_5ca46c508f7b6100b3fd11ad_5ca46c5f72723e00b33207c6/scale_1200)
-->

The only difference is that the trees have been removed.

I do not insist at all that in the 19th century a permanent nuclear apocalypse was staged in different places in America, but I am curious to know what could have burned and exploded like that? How did it happen that with such large-scale destruction in the annals we see the wildest numbers of the dead ..

<!--
![Burn it all with a blue flame!](https://avatars.mds.yandex.net/get-zen_doc/60743/pub_5ca46c508f7b6100b3fd11ad_5ca46c5fae5b0400b3f26036/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/16.jpeg#resized)

All this together makes us think about the unreality of what was happening or, even worse, about our complete ignorance of the causes, means and executors of the "fires". I hope it is already possible to put this word in quotation marks, since the man-madeness of these events hardly raises doubts even among skeptics.

But the oddities don't end there. American enthusiasts noticed a few more interesting features that were repeated from time to time in every city.

After the incident, instead of mourners killing over the lost belongings, or at worst crowds of firefighters who fought the fire day and night, we see people posing on the ruins. There is no sadness or anxiety on their faces, just a business inspection, work performed and a memorable photo, as with the carcass of a murdered elephant.

<!--
![1 of 4 Pure pioneers with a pile of scrap metal!](https://avatars.mds.yandex.net/get-zen_doc/1136050/pub_5ca46c508f7b6100b3fd11ad_5ca46c61134d6700b2a4910d/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/17.jpeg#resized)

Pure pioneers with a pile of scrap metal!

Well, okay, maybe that was the way it was. Or, as historians would say, fashionable. We took pictures on the remains, it's time to get down to business. Probably, they took it, but for some reason they did not want to record the garbage collection on the camera. Nowhere, in any city, the process of raking and removing the remaining parts of buildings is not captured. That is, thousands and thousands of man-hours, end-hours, cabal-hours, and possibly ostrich-hours, were not even accidentally caught by any photographer.

At the same time, before taking away all the photographic equipment in the district, they take pictures of future buildings, for which places have already been assigned on the ruins. Is it too fast for a new layout?

<!--
![Thank you for not arranging the layouts.](https://avatars.mds.yandex.net/get-zen_doc/30229/pub_5ca46c508f7b6100b3fd11ad_5ca46c616cd1bb021f89cc52/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/18.jpeg#resized)

Thank you for not arranging the layouts.

Well, the final one, no less funny. After the fire, they began to build the city. Very fast. Incredibly fast. Overtaking all possible production capacities of materials from which they were built.

For example, in Seattle, 5,525 buildings were built in 18 months. This is not counting the dismantled rubble and such trifles as 40-something miles of railroad tracks.

Easy mental calculation, it gives us 10 buildings a day, without sleep or rest.

And you know what? At that time there was no panel construction and they were not building barracks for the victims of the fire.

<!--
![1 of 4 Were all Stakhanovites back then?  When did we manage to grind like this?](https://avatars.mds.yandex.net/get-zen_doc/111343/pub_5ca46c508f7b6100b3fd11ad_5ca46c62134d6700b2a4910e/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/19.jpeg#resized)

Were all Stakhanovites back then? When did we manage to grind like this?

There was also about funny reasons for each of the devastating fires.

<!--
![A cow lying down on a kerosene stove and the aftermath of her dance.](https://avatars.mds.yandex.net/get-zen_doc/1602847/pub_5ca46c508f7b6100b3fd11ad_5ca46c623aaf7b00b2c2fe8c/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/20.jpeg#resized)

A cow lying down on a kerosene stove and the aftermath of her dance.

All this, of course, is interesting and entertaining. The number of fires and the speed of recovery, the burning out of solid brick-stone buildings in the firebox and trees untouched nearby. If we focus on admiring these oddities, then the kaleidoscope of amazing historical facts will remain an amusing, but not very useful booth, if you do not combine everything into a single picture. And here already, it may not be funny at all.

<!--
![1 out of 5](https://avatars.mds.yandex.net/get-zen_doc/1542444/pub_5ca46c508f7b6100b3fd11ad_5ca46c6353239a00b3a7611c/scale_1200)
-->

![](./images/burn_it_all_with_a_blue_flame_files/21.jpeg#resized)


© All rights reserved. The original author retains ownership and rights.


