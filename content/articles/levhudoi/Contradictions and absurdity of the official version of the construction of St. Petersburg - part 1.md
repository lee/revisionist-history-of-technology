title: Contradictions and absurdity of the official version of the construction of St. Petersburg - Part 1
date: 2013-10-22
modified: Thu 14 Jan 2021 07:22:28 GMT
category:
tags: St Petersburg
slug:
authors: Lev Hudoi
summary: But the most important document does not exist - how exactly were such complex shapes given to such large blocks manually?
status: published
originally: Contradictions and absurdity of the official version of the construction of St. Petersburg.html
from: https://levhudoi.blogspot.com/2013/06/blog-post.html

### Translated from:

[https://levhudoi.blogspot.com/2013/06/blog-post.html](https://levhudoi.blogspot.com/2013/06/blog-post.html)

**Russian:**

Договоримся о сокращении "ОВ" - официальная, "научная" версия истории.

**English:**

Let's agree we will use the abbreviation "OS" to indicate dates from the 'Official "Scientific"' version of history...

**Russian:**

По ОВ 200 лет назад Александровскую колонну (АК для краткости) подняли вручную за 1 ч 45 мин. Вес колонны 600-700 т. В наши дни в Астане (Казахстан) был построен самый большой шатер в мире "Хан-Шатыр". Для поднятия опоры весом 1500 т лучшие инженеры современного мира под руководством турецкого инженера Селами Гуреля потратили 2 суток.

**English:**

By OS 200 years ago, the Alexander column (AK for short) was raised manually in 1 hour and 45 minutes. The weight of the column of 600-700 t Today, the largest tent in the world "[Khan-Shatyr](https://www.atlasobscura.com/places/khan-shatyr)" was built in Astana (Kazakhstan). The best engineers of the modern world under the guidance of Turkish engineer Selami Gurel spent 2 days to lift the support weighing 1500 tons.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/SelamiGurel1.JPG#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/SelamiGurel1.JPG)
-->

[![](https://4.bp.blogspot.com/-wQi6m6wvqfU/Um1IMa5soII/AAAAAAAACaA/S4dZUJoebqU/s1600/Selami+Gurel1.JPG#clickable)](https://4.bp.blogspot.com/-wQi6m6wvqfU/Um1IMa5soII/AAAAAAAACaA/S4dZUJoebqU/s1600/Selami+Gurel1.JPG)

**Russian:**

Итого, в 21-м веке с помощью машин в 2.5 раза больший вес поднимали в 30 раз дольше, чем вручную в 19-м веке. Причем, по современной технологии, вес поднимаемой башни не влияет на скорость подъема.

**English:**

In total, in the 21st century, machines lifted 2.5 times more weight 30 times longer than manually in the 19th century. Moreover, according to modern technology, the weight of a raised tower does not affect the speed of ascent.

**Russian:**

Мне подсказали [http://rekhmire.ru/viewtopic.php?p=11868#p11868](http://rekhmire.ru/viewtopic.php?p=11868#p11868) что некорректно сравнивать поднятие АК высотой 26 метров и опорной башни Хан Шатыра на высоту 70 м. То есть, если бы башню в Астане поднимали на высоту АК, то не за 48 ч, а за 17. Что не в 30 раз больше, а "всего лишь" в 10.

**English:**

I was prompted by [http://rekhmire.ru/viewtopic.php?p=11868#p11868](http://rekhmire.ru/viewtopic.php?p=11868#p11868) that it is incorrect to compare the elevation of an Alexander column (AK) 26 meters high and support tower of Khan Shatyr at a height of 70 m. That is, if the tower in Astana raised to the height of the AK, then not for 48 hours, but for 17. Which is not 30 times more, and "only" in 10.

**Russian:**

Для меньшего веса нужно меньшее количество гидравлических домкратов и тросов. 3-х минутный ролик об этом:

**English:**

Less weight requires fewer hydraulic jacks and cables. Three-minute video about it:

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/xxc.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/xxc.jpg)
-->

[![](https://3.bp.blogspot.com/-fHp2CiGzIW8/UnlQw1Ow7TI/AAAAAAAACgA/XUKVKr36u-Y/s1600/xxc.jpg#clickable)](https://3.bp.blogspot.com/-fHp2CiGzIW8/UnlQw1Ow7TI/AAAAAAAACgA/XUKVKr36u-Y/s1600/xxc.jpg)

**Russian:**

Храм Христа-Спасителя в Москве был в 19-м веке построен за 44 года, а в наше время он же построен тоже турками за 5 лет (из Википедии) после его разрушения большевиками. Колонна была установлена в 10 раз быстрее, а храм построен в 9 раз медленнее. То есть, примерно в одно и то же время в начале 19-го века 2 объекта в России были построены с разницей в производительности в 10 х 9 = 90 раз. Это можно объяснить тем, что подъем колонны вручную выдумали. Но, космическая скорость подъема Ал-ской колонны - это только конец дела. Еще сложнее было начало - сделать ее и привезти в Питер. В наше время мы бы могли ее установить, пусть и за в 10 раз большее время. А вот создать ее современные технологии пока не способны.

**English:**

The Cathedral of Christ the Saviour in Moscow was built in the 19th century for 44 years, and in our time it was also built by the Turks in 5 years (from Wikipedia) after its destruction by the Bolsheviks. The column was installed in 10 times faster, and the temple is built 9 times slower. That is, approximately in at the same time, at the beginning of the 19th century, 2 objects in Russia were built with a performance difference of 10 x 9 = 90 times. This can be explained the fact that the lifting of the column was manually invented. But, space speed the rise of the al column is only the end of the matter. The beginning was even more difficult:

Create it and bring it to St. Petersburg? In our time, we could install it, albeit taking ten times longer to do so. But our technologies are not yet capable of creating it.

**Russian:**

Александровская колонна - самая большая колонна в мире, сделанная из одного куска гранита. Высота около 27 метров. Выглядит идеально круглой (хотя никто идеальность не проверял), отполирована до блеска. Имеет форму не цилиндра, а усеченного конуса, да еще и с изогнутой боковой линией.

**English:**

The Alexander column is the largest column in the world made from a single piece of granite. The height is about 27 meters. It looks perfectly round (although no one has checked the perfection), polished to a shine. It has the shape not of a cylinder, but of a truncated cone, and even with a curved side line.

**Russian:**

Я сжал по вертикали фотографию колонны в 10 раз. И бочкообразность стала заметна:

**English:**

I compressed the vertical photo of the column 10 times. And the barrel shape became noticeable:

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/0021.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/0021.jpg)
-->

[![](https://3.bp.blogspot.com/-rFjQbRQ5mTk/UkxKYJhMVjI/AAAAAAAAB_E/k6ewS2poPLo/s1600/0021.jpg#clickable)](https://3.bp.blogspot.com/-rFjQbRQ5mTk/UkxKYJhMVjI/AAAAAAAAB_E/k6ewS2poPLo/s1600/0021.jpg)

**Russian:**

Когда я говорил с реставратором Владимиром Сориным на совсем другую тему, он между прочим заметил, что у колонны имеется ... энтазис.

**English:**

When I spoke with the restorer Vladimir Sorin on a completely different topic, he incidentally noticed that the column has ... entasis.

**Russian:**

> *Энта́зис (от [греч.](http://ru.wikipedia.org/wiki/%D0%93%D1%80%D0%B5%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D1%8F%D0%B7%D1%8B%D0%BA) ἔντασις — напряжение), утоне́ние — плавное изменение диаметра сечения колонны вдоль продольной оси от максимального **в пределах нижней трети ствола** до минимального в завершении.* *Применялся для создания зрительного эффекта напряжённости и устранения иллюзии вогнутости ствола колонны*

> *Если поставить круглый столб (правильный цилиндр), то нашему глазу он будет казаться утолщающимся кверху. Для предотвращения этого оптического обмана приходится кверху уменьшать толщину столба.*

> *Это утонение, **очень незначительное**, составляет от 1/5 до 1/6 нижней толщины: верхний диаметр (или радиус) составляет 5/6 нижнего диаметра (или радиуса). Однако обычно утонение колонны начинается не непосредственно снизу, а только начиная с 1/3 высоты ...*

> *Чтобы начертить кривую, пользуемся особой, имеющей разнообразные кривизны, линейкой, которая называется “лекало”.*

**English:**

> Entasis (from Greek. ἔντασις — stress), thinning — a smooth change in the diameter of the column section along the longitudinal axis from the maximum within the lower third of the trunk to the minimum at the end. It was used to create a visual effect of tension and eliminate the illusion of concavity of the column trunk

> If you put a round column (a regular cylinder), then to our eye it will seem to thicken up. To prevent this optical deception, it is necessary to reduce the thickness of the column up.

> This thinning, very slight, is from 1/5 to 1/6 of the lower thickness: the upper diameter (or radius) is 5/6 of the lower diameter (or radius). However, usually the thinning of the column does not begin directly from the bottom, but only starting from 1/3 of the height...

> To draw a curve, we use a special ruler that has a variety of curvatures, which is called a “pattern".

**Russian:**

А теперь приложите это лекало к гранитной глыбе длиной с 10-этажный дом и "нарисуйте" кувалдой и зубилом плавную линию. По ОВ вся эта сложность сделана вручную. Сначала вырезали из скалы гигантский прямоугольный брус, он лежал горизонтально, и что-то там неизвестное делали с ним камнетесы, в результате чего появился гигантский идеальнокруглый полированный очень медленно сужающийся к верху начиная с трети от низа усеченный конус.

**English:**

Now apply this pattern to a granite block the length of a 10-story building and "draw" a smooth line with a sledgehammer and chisel. For the official story ("OS"), all this complexity is done manually. First, a giant rectangular bar was cut out of the rock, it lay horizontally, and something unknown was done with it by stonemasons, as a result of which a giant perfectly round polished truncated cone appeared very slowly tapering to the top starting from a third from the bottom.

**Russian:**

Нам рассказывают: "вот посмотрите - совсем недавно без машин и компьютеров построили эту колонну. **Все ж задокументировано!** Совсем недавно и никаких сомнений в ручном труде нет!" Вот только **не существует самого главного документа - как именно вручную придавались такие сложные формы таким большим глыбам? Более того, не существует не только этого технического документа на бумаге, но не существует и никаких устных знаний об этой технологии. А ведь было это все совсем недавно! Менее 200 лет назад.** **** **Поэтому, никаких доказательств ручного изготовления колонны не существует. Это лишь предмет веры как религия. Но, религии возникли тысячи лет назад, поэтому естественно отсутствие материальных доказательств. А Александровская колонна появилась совсем недавно.**

**English:**

We are told: "Look - just recently, without cars and computers, this column was built. Everything is documented! Quite recently and there is no doubt about manual labor!" But the most important document does not exist - how exactly were such complex shapes given to such large blocks manually? Moreover, not only does this technical document not exist on paper, but there is no oral knowledge about this technology. But it was all quite recently! Less than 200 years ago. Therefore, there is no evidence of manual manufacturing of the column. It is only a matter of faith as a religion. But, religions originated thousands of years ago, so naturally there is no material evidence. And the Alexander column appeared quite recently.

**Russian:**

**Отсутствию доказательств** **ее ручного происхождения на бумаге и в навыках камнетёсов нет никаких разумных объяснений.** Сделать такое в наше время вручную **даже из дерева!** не представляется возможным. Только на токарном станке с ЧПУ. А сделать из гранитного бруса весом около 1000 тонн в наше время нельзя никак ни на каком оборудовании. И сделано это все только "ради предотвращения оптического обмана".

**English:**

#### Lack of evidence
There is no reasonable explanation for the lack of evidence of its manual origin on paper and in the skills of stonemasons. To do this in our time by hand, even from wood! it is not possible. Only on a CNC lathe. And to make a granite bar weighing about 1000 tons in our time can not be done on any equipment. And all this was done only "for the sake of preventing optical deception".

**Russian:**

Если мы верим, что за 200 лет цивилизация деградировала так что мы стали поднимать мегагрузы в 30 раз медленнее, то мы должны поверить, что и бегали раньше в 30 раз быстрее, чем мы передвигаемся сейчас, на деревянных счетах и в столбик вычисления производили в 30 раз быстрее чем современные компьютеры. Это в количественном плане. А в качественном деградировали за последние 200 лет еще больше. Не существует такого токарного станка с ЧПУ, который мог бы крутить заготовку весом в 1000 тонн. Мы такую колонну из гранита сделать не сможем ни за какое время ни за какие деньги. А 200 лет назад справились вручную за несколько месяцев. Спасибо "русскому" архитектору Августу Августовичу

[Монферрану](http://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D1%80%D0%B0%D0%BD,_%D0%9E%D0%B3%D1%8E%D1%81%D1%82)

**English:**

If we believe that over 200 years, civilization has degraded so that we began to lift mega-loads 30 times slower, then we must believe that we used to run 30 times faster than we move now, on wooden accounts and in a column, calculations were made 30 times faster than modern computers. This is in quantitative terms. And in terms of quality, they have degraded even more over the past 200 years. There is no CNC lathe that can turn a workpiece weighing 1,000 tons. We will not be able to make such a column of granite for any time or for any money. And 200 years ago, they managed it manually in a few months. Thanks to the "Russian" architect August Augustovich Montferrand

[Montferrand](http://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D1%80%D0%B0%D0%BD,_%D0%9E%D0%B3%D1%8E%D1%81%D1%82)

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/20130321134206-33c70e7d-me.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/20130321134206-33c70e7d-me.jpg)
-->

[![](https://4.bp.blogspot.com/-NsLzMfN4_ig/Umt_80p_XBI/AAAAAAAACWA/DayOhutjax4/s1600/20130321134206-33c70e7d-me.jpg#clickable)](https://4.bp.blogspot.com/-NsLzMfN4_ig/Umt_80p_XBI/AAAAAAAACWA/DayOhutjax4/s1600/20130321134206-33c70e7d-me.jpg)

**Russian:**

> *Бетанкур Августин — генер.-лейтенант рус. службы (р. 1758, † 1824),...*

> -   *преобразовал Тульский оружейный завод,*
> -   *построил пушечный литейный дом в Казани,*
> -   *ввел новые и улучшил старые машины на Александровской мануфактуре,*
> -   *построил здание экспедиции заготовления государственных бумаг (где им лично придумана большая часть машин),*
> -   *громадный московский экзерциргауз,*
> -   *гостиный двор для нижегородской ярмарки и разные другие здания.*
> -   *По проекту Б. учрежден в С.-Петербурге Институт путей сообщения, куда он и назначен был инспектором.*
> -   *С 1816 г. Б. занимал место председателя комитета о городских строениях в Петербурге,*
> -   *а в 1819 г. на него возложено главное управление путей сообщения. На этой последней должности он оставался до самой своей смерти, последовавшей 14 июля 1824 г.*

**English:**

> Betancourt Augustin — General-Lieutenant of the Russian service (b. 1758, † 1824),...

> - transformed the Tula arms factory,

> - built a cannon foundry in Kazan,

> - introduced new and improved old machines at the Alexander manufactory,

> - he built a building of the expedition of storing state papers (where they personally invented most of the machinery),

> - huge Moscow eckerlilas,

> - Gostiny Dvor for the Nizhny Novgorod fair and various other buildings.

> - According to the project B. established in St. Petersburg Institute of Railways, where he was appointed inspector.

> - From 1816 B. served as Chairman of the Committee on urban buildings in St. Petersburg, and in 1819, it was entrusted with the main Department of Railways. He remained in this latter position until his death on July 14, 1824.

**Russian:**

Пока что нормальная биография. Исаакиевский собор и Александровская колонна никакого отношения к нему не имеет. Но, если он такой талантливый человек, да еще и не русский, то надо же на него повесить строительство Исаакия! Не дикие же русские такой красивый собор построили из гранитных колонн по 120 тонн! И вот более свежая версия его биографии [http://dic.academic.ru/dic.nsf/enc_sp/215/](http://dic.academic.ru/dic.nsf/enc_sp/215/)

**English:**

While that is not a normal biography. St. Isaac's Cathedral and the Alexander column have nothing to do with it. But if he is such a talented person, and not even a Russian, then we must hang the construction of Isaac on him! Not wild Russians built such a beautiful Cathedral from granite columns of 120 tons! And here is a more recent version of his biography [http://dic.academic.ru/dic.nsf/enc_sp/215/](http://dic.academic.ru/dic.nsf/enc_sp/215/)

**Russian:**

> *Б. руководил технической частью строительства Исаакиевского собора*

Источник: *"Санкт-Петербург. Петроград. Ленинград: Энциклопедический справочник. — М.: Большая Российская Энциклопедия. **1992**"*. Прошло каких-то 100 лет после энц-дии Брокгауза Ефрона а его уже на Исаакия руководителем поставили. Проходит еще совсем немного, началась эпоха интернета и... Википедия:

**English:**

> B. supervised the technical part of the construction of St. Isaac's Cathedral

Source: *"Saint Petersburg. Petrograd. Leningrad: Encyclopedic reference book. — M.: Big Russian encyclopedia. 1992"*. It took some 100 years after the encode of Brockhaus Efron, and he was already put in charge of Isaac. It is still quite a bit, the era of the Internet has begun... Wikipedia:

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/30518_real.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/30518_real.jpg)
-->

[![](https://1.bp.blogspot.com/-4mInkLW4YME/UnUhj7-hlGI/AAAAAAAACek/YIFtnWen66o/s1600/30518_real.jpg#clickable)](https://1.bp.blogspot.com/-4mInkLW4YME/UnUhj7-hlGI/AAAAAAAACek/YIFtnWen66o/s1600/30518_real.jpg)

**Russian:**

> *Бетанкур, **Августин Августинович***** **

> *Августи́н де Бетанкур и Молина, полное имя Августин Хосе Педро дель Кармен Доминго де Канделария де Бетанкур и Молина (исп. Agustín José Pedro del Carmen Domingo de Candelaria de Betancourt y Molina);*

> *....*

> ***Принимал участие в постройке Исаакиевского собора.***

> *Был похоронен на Смоленском лютеранском кладбище в Петербурге.*

> *Основные научные труды*

> *«О расширительной силе паров» (Париж, 1790); «О новой системе внутреннего судоходства» (Париж, 1807); «Руководство к составлению машин» (совместно с Х. М. Ланцем, Париж, 1808 г., 1-е издание; 1819 г., 2-е издание; 1840 г., 3-е издание, посмертное).*

> Работы Бетанкура: *1817 год: Московский Манеж **1832** год: Александровская колонна* **

> *Инженер **принимал активное участие** в инженерной стороне строительства монумента.*

**English:**

> Betancourt, Augustin Augustinovich

> Augustin de Betancourt y Molina, full name Augustin josé Pedro del Carmen Domingo de Candelaria de Betancourt y Molina (Spanish: Agustín José Pedro del Carmen Domingo de Candelaria de Betancourt y Molina);

> ....

> He took part in the construction of St. Isaac's Cathedral.

> He was buried at the Smolensk Lutheran cemetery in St. Petersburg.

> The main scientific works

> "On the expanding power of steam" (Paris, 1790); "On the new system of inland navigation" (Paris, 1807); "guide to the compilation of machines" (with H. M. Lanz, Paris, 1808, 1st edition; 1819, 2nd edition; 1840, 3rd edition, posthumous).

> Betancourt's works: 1817: Moscow Manege 1832: Alexander column

> The engineer took an active part in the engineering side of the monument's construction.

**Russian:**

В Википедии дата смерти Бетанкура 1824 год. И в этой же статье он "

***принимал активное участие"* в установке колонны в 1832 г.** И дело не только в википедии. Например, посмотрите, что пишут в блогах великие атеистические пудрецы, верящие в загробную жизнь Бетанкура [http://blog.fontanka.ru/posts/123624/]:

**English:**

In Wikipedia, the date of Betancourt's death is 1824. And in the same article, he "took an active part" in the installation of the column in 1832.

And it's not just about Wikipedia. For example, look at what the great atheist sages who believe in Betancourt's afterlife write in blogs [http://blog.fontanka.ru/posts/123624/](http://blog.fontanka.ru/posts/123624/)

**Russian:**

> *Под руководством Бетанкура в Санкт-Петербурге велись перестройка Исаакиевского собора **и установка знаменитой Александровской колонны**.*

> *Инженер **лично **занимался расчетами фундаментов, проектированием лесов и подъемных механизмов. Августин Бетанкур скончался 26 июля 1824 года.*

> *Настало время приступить к установке колонны на пьедестал. **Этой частью работ также занимался А. А. Бетанкур.** Им в **декабре 1830** года была сконструирована оригинальная подъёмная система.*

> *30 августа **1832 **года ... для приведения колонны в вертикальное положение на Дворцовой площади инженеру А. А. **Бетанкуру** потребовалось привлечь силы 2 000 солдат и 400 рабочих, которые за 1 час 45 минут установили монолит.*

**English:**

> Under the leadership of Betancourt in St. Petersburg, the reconstruction of St. Isaac's Cathedral and the installation of the famous Alexander column were carried out.

> The engineer was personally engaged in calculations of foundations, design of scaffolding and lifting mechanisms. Augustine Betancourt died on July 26, 1824.

> It is time to start installing the column on the pedestal. This part of the work was also done by A. A. Betancourt. He designed the original lifting system in December 1830.

> August 30, 1832 ... to bring the column to a vertical position on the Palace square, engineer A. A. Betancourt needed to attract the forces of 2,000 soldiers and 400 workers, who installed the monolith in 1 hour and 45 minutes.

**Russian:**

К сожалению, авторы этих статей не указывают источники таких сведений. Поиск первоисточника в интернете о загробной жизни Бетанкура ничего не дал. Значит, авторы черпают эти откровения из книг. Пройдет еще пару лет и ему припишут участие в запуске первого спутника.

**English:**

Unfortunately, the authors of these articles do not indicate the sources of such information. A search of the original source on the Internet about Betancourt's afterlife turned up nothing. So the authors draw these revelations from books. It will take another couple of years and he will be credited with participating in the launch of the first satellite.

**Russian:**

Но, надо признать, существуют и силы зла, которые не дают человеку пожить спокойно после смерти. Бетанкуру приписывается не активное участие, а лишь применение после его смерти его разработок. Например здесь

[http://www.km.ru/referats/FF458FC584574384B27EB41290DBB1D6](http://www.km.ru/referats/FF458FC584574384B27EB41290DBB1D6#)

**English:**

But, we must admit, there are also forces of evil that do not allow a person to live in peace after death. Betancourt is not credited with active participation, but only with the application of his developments after his death. For example here:

[http://www.km.ru/referats/FF458FC584574384B27EB41290DBB1D6](http://www.km.ru/referats/FF458FC584574384B27EB41290DBB1D6)

**Russian:**

Косвенным подтверждением наличия человека являются его потомки. У Августа якобы был сын Альфонсо.

[Википедия](http://ru.wikipedia.org/wiki/%D0%91%D0%B5%D1%82%D0%B0%D0%BD%D0%BA%D1%83%D1%80,_%D0%90%D0%BB%D1%8C%D1%84%D0%BE%D0%BD%D1%81_%D0%90%D0%B2%D0%B3%D1%83%D1%81%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%D0%B8%D1%87):

**English:**

Indirect confirmation of the presence of a person is his descendants. Augustus allegedly had a son, Alfonso. [Wikipedia](http://ru.wikipedia.org/wiki/%D0%91%D0%B5%D1%82%D0%B0%D0%BD%D0%BA%D1%83%D1%80,_%D0%90%D0%BB%D1%8C%D1%84%D0%BE%D0%BD%D1%81_%D0%90%D0%B2%D0%B3%D1%83%D1%81%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%D0%B8%D1%87):

**Russian:**

> *Альфонс (Альфонсо) Августинович Бетанкур (1805—1875) — русский военный деятель, генерал-лейтенант...*

> *Умер в 1875 году (по другим данным в 1863 году[3]).*

**English:**

> Alphonse (Alfonso) augustinovich Betancourt (1805-1875) — Russian military leader, Lieutenant General...  He died in 1875 (according to other sources in 1863[3]).

**Russian:**

Вот те на! Весь в отца! Умирает дважды с разницей в 12 лет! Такой потомок не является надежным доказательством реальности родителя. Также настораживает то, что он дожил якобы до эпохи фотографий но его фотографии найти не удалось, в том числе в статье о нем в википедии.

еще один живший параллельно с первым полный теска:

> *Бетанкур Августин Августинович (1805--1863) ген.-адъютант (1855-) В 1824 главноуправляющий Корпусом путей сообщения, почетн. чл. Российск. Минералогического об-ва 1824 г.*

**English:**

Here are those on! Just like my father! Dies twice within 12 years of each other! Such a descendant is not a reliable proof of the parent's reality. It is also alarming that he supposedly lived into the era of photos, but his photos could not be found, including in an article about him in Wikipedia.

Another who lived in parallel with the first full teska:

> Betancourt Augustin augustinovich (1805-1863) General-adjutant (1855-) in 1824 chief Manager Of the corps of Railways, honorary member of the Russian Federation. Of the mineralogical region of 1824.

**Russian:**

Тут жутко перепутаны 2 биографии Бетанкуров отца и сына. Получается, он в 19 лет стал главноуправляющий и почетный член. И случилось это продвижение по службе в год смерти Бетанкура-отца! В 1821 году Бетанкур привез в Россию своего племянника, сына своей сестры. Звали его тоже ... Августин Мотеверде-Бетанкур. Монтеверде - фамилия по отцу, Бетанкур - по матери.

**English:**

Here two biographies of Betancourt father and son are terribly mixed up. It turns out that at the age of 19, he became the chief Executive and honorary member. And this promotion happened in the year of Betancourt's father's death! In 1821, Betancourt brought his nephew, his sister's son, to Russia. His name, too ... Augustine's Monteverde-Betancourt. Monteverde is his father's surname, Betancourt is his mother's.

**Russian:**

> *В 1821 году к Корпусу Инженеров Путей Сообщения прибавляются два испанца. Первый из них - это племянник Бетанкура Августин де Монтеверде и Бетанкур, направленный на строительство дорог в Санкт-Петербурге и в Москве, а позже на организацию снабжения водой Одессы. Второй, Мигель Эспехо*

**English:**

> In 1821, two Spaniards were added to the corps of Railway engineers. The first of them is Betancourt's nephew Augustin de Monteverde and Betancourt, who was sent to build roads in St. Petersburg and Moscow, and later to organize the water supply of Odessa. Second, Miguel Espejo

**Russian:**

Интересно отметить, что 2 "русских" Августа Августовича Бетанкур и Монферран совершили 2 дела: и торжественно подняли колонну и торжественно открыли через 2 года монумент в месяце тоже Августе. Август на августе сидит и августом погоняет.

**English:**

It is interesting to note that 2 "Russian" August Augustovich Betancourt and Montferrand did 2 things: they solemnly raised the column and solemnly opened the monument 2 years later in the Month of August, too. August on August and August sitting whipping.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/b9fa4b6eb585222f14199b2734a.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/b9fa4b6eb585222f14199b2734a.jpg)
-->

[![](https://3.bp.blogspot.com/-e74B5rCmXFU/Uhh4GyE_pCI/AAAAAAAABUc/3haOzR5k3jk/s1600/b9fa4b6eb585222f14199b2734a.jpg#clickable)](https://3.bp.blogspot.com/-e74B5rCmXFU/Uhh4GyE_pCI/AAAAAAAABUc/3haOzR5k3jk/s1600/b9fa4b6eb585222f14199b2734a.jpg)

**Russian:**

Иностранцы так описывали русских рабочих, создавшие это вечное совершенство:

>   Дикий русский ночной
>
>   строитель с фонарем

**English:**

Foreigners described the Russian workers who created this eternal perfection:

> Wild Russian night
> 
> Builder with a lantern

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/1286252639_2021-479_002.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/1286252639_2021-479_002.jpg)
-->

[![](https://1.bp.blogspot.com/-YO-0GDibUUk/UjRAvdh8IVI/AAAAAAAABrs/Ej8Nce4YUik/s1600/1286252639_2021-479.jpg#clickable)](https://1.bp.blogspot.com/-YO-0GDibUUk/UjRAvdh8IVI/AAAAAAAABrs/Ej8Nce4YUik/s1600/1286252639_2021-479.jpg)

**Russian:**

> *"Им, этим простым мужикам в рваных полушубках, **не нужно было прибегать к различным измерительным инструментам**; пытливо взглянув на указанный им план или модель, они точно и изящно их копировали ..., несмотря на **зимнее время** и **13 - 15 градусов мороза**, **работы продолжались даже ночью**.*

> ***Крепко зажав кольцо фонаря зубами**, эти изумительные работники, забравшись **на верх лесов** (должно быть, за кокосами бананами - уточнение Льва Худого), старательно исполняли свое дело. Способность даже простых русских в технике изящных искусств поразительна".*

**English:**

> "They, these simple men in ragged sheepskin coats, did not need to resort to various measuring tools; after looking inquisitively at the plan or model indicated to them, they accurately and elegantly copied them ... despite the winter time and 13 - 15 degrees of frost, the work continued even at night.

> Holding the lantern ring tightly between their teeth, these amazing workers, climbing to the top of the scaffolding (probably for coconuts and bananas - Leo The thin's clarification), diligently performed their work. The ability of even ordinary Russians in the technique of fine arts is amazing."

**Russian:**

Тот, кто это писал, наверное, никогда железного кольца на морозе в рот не брал. Примерзает мгновенно. Кольцо в зубах, кольцо в носу. Также этот человек приписывает гениальным строителям столь совершенных сооружений отсутствие измерительных инструментов. Это полный бред. Мало того, что делали все на глазок, так еще и ночью при свете фонаря. Причем, фонарь в зубах. Чем не дикие обезьяны? Прыгают по строительным лесам с фонарем в зубах. И эта обезьянья цитата почти во всех сайтах, посвященных Питеру.

**English:**

Whoever wrote this probably never put an iron ring in his mouth in the cold. Freezes instantly. A ring in your teeth, a ring in your nose. This person also attributes the lack of measuring tools to the ingenious builders of such perfect structures. This is complete nonsense. Not only did they do everything by eye, but also at night by the light of a lantern. Moreover, the lantern is in the teeth. What's wrong with wild monkeys? Jump on scaffolding with a lantern in their teeth. And this monkey quote is in almost all sites dedicated to Peter

**Russian:**

Реставрировать старые здания от предыдущей римско-русской цивилизации так еще можно. Но не строить. Замазать старые щели и обезьяна сможет.

Но и в наши дни загадочным строителям прошлого продолжают приписывают безграмотность:

**English:**

You can still restore old buildings from the previous Roman-Russian civilization. But don't build. Cover up the old cracks and the monkey can.

But even today, the mysterious builders of the past continue to be attributed to illiteracy:

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/581.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/581.jpg)
-->

[![](https://2.bp.blogspot.com/-DVlwDldZjvQ/Uokp2xpbDAI/AAAAAAAACqE/9hsmQn4IW3A/s1600/581.jpg#clickable)](https://2.bp.blogspot.com/-DVlwDldZjvQ/Uokp2xpbDAI/AAAAAAAACqE/9hsmQn4IW3A/s1600/581.jpg)

**Russian:**

> Владимир Сорин, художник-реставратор: **«Никаких современных систем расчета тогда не было.** Поэтому Монферран мог полагаться только на интуицию».
>
> Недавно восьмое чудо света проверили на прочность с помощью современных технологий.

> **Колонна, в отличие от остальных сооружений дворцовой площади, выдержало смоделированное землетрясение в шесть баллов**. [http://5-tv.ru/news/59204/](http://5-tv.ru/news/59204/)

**English:**

> Vladimir Sorin, artist-restorer: "there were No modern calculation systems then. Therefore, Montferrand could only rely on intuition."

> Recently, the eighth wonder of the world was tested for strength with the help of modern technologies.

> The column, unlike the rest of the Palace square structures, withstood a simulated earthquake of six points. [http://5-tv.ru/news/59204/](http://5-tv.ru/news/59204/)

**Russian:**

Хорошая интуиция, однако! Ничего себе. Стоит длинная палка вертикально и не падает от землетрясения. В то время как все вокруг развалится. Забегая вперед скажу - колонна никак не закреплена с основанием. Просто стоит. Но, если предположить, что Монферран колонну не ставил, а только поставил сверху ангела на место бывшей там антенны, то он действительно мог обойтись без рассчетов сейсмоустойчивости. Эти расчеты сделали те кто колонну поставил до него.

**English:**

Good intuition, though! Wow. A long stick stands upright and does not fall from an earthquake. While everything else falls apart. Looking ahead, the column is not fixed to the base in any way. Just stand. But if we assume that Montferrand did not put the column, but only put an angel on top of it in the place of the antenna that was there, then he could really do without calculations of seismic stability. These calculations were made by those who put the column before him.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/Averincev_Sergey_Sergeevich.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/Averincev_Sergey_Sergeevich.jpg)
-->

[![](https://3.bp.blogspot.com/-HEeyt_MxrGU/Ufv5f0TCHbI/AAAAAAAABDY/aWwyzJYIR6E/s200/Averincev_Sergey_Sergeevich.jpg#clickable)](https://3.bp.blogspot.com/-HEeyt_MxrGU/Ufv5f0TCHbI/AAAAAAAABDY/aWwyzJYIR6E/s200/Averincev_Sergey_Sergeevich.jpg)

**Russian:**

Однажды на телепрограмму пригласили филолога, литературоведа, библеиста, историка и т.д. Сергея Сергеевича Аверинцева. Перед тем, как дать ему слово, ведущий стал представлять его телезрителям, перечисляя несколько минут все его регалии, звания, труды, советские, Российские и иностранные научные награды, но, остановился на полуслове, заявив: "Если я продолжу читать этот список, то у нас не хватит эфирного времени для самого интервью". В 1990 году он получил Государственную премию СССР за фундаментальное исследование «Мифы народов мира». В этом исследовании в статье "Николай Чудотворец" он пишет:

**English:**

Once on a TV program, invited the scholar, literary critic, Bible scholar, historian, etc. Sergei Sergeevich Averintsev. Before giving him the floor, the host began to introduce him to the viewers, listing for a few minutes all his regalia, titles, works, Soviet, Russian and foreign scientific awards, but stopped in mid-sentence, stating: "If I keep reading this list, we won't have enough airtime for the interview itself." In 1990, he received the USSR State prize for fundamental research "Myths of the peoples of the world". In this study, in the article "Nicholas the Wonderworker", he writes:

**Russian:**

> подвергся сильной фольклорной мифологизации, послужив соединительным звеном между дохристианскими персонификациями благодетельных сил и новейшей детской рождественско-новогодней «мифологией» (Санта Клаус — «святой Николай»; его модификации — «батюшка Рождество», Дед Мороз). Предания рассказывают, как уже в младенчестве Н. являет себя образцом аскетической добродетели, до сумерек воздерживаясь от материнской груди по средам и пятницам, а по более поздней версии — обнаруживая чудесную способность стоять сразу же после рождения (распространенный мотив русской иконографии).

**English:**

> it has undergone a strong folklore mythologization, serving as a connecting link between pre-Christian personifications of beneficent forces and the latest children's Christmas and new year "mythology" (Santa Claus — "Saint Nicholas"; its modifications — "father Christmas", Santa Claus). Legends tell how, even in infancy, N. shows himself a model of ascetic virtue, abstaining from the mother's breast until dusk on Wednesdays and Fridays, and later on — revealing a wonderful ability to stand immediately after birth (a common motif of Russian iconography).

**Russian:**

Сильно сказано. Высшую степень религиозного мракобесия мозгов назвал медвежьим культом.

Но все плебейские низовые реликтовые медвежьи религиозные культы превзошел высокомудрый "научно"-атеистический культ. И тоже... медвежий.

**English:**

Strong words. The highest degree of religious obscurantism of the brain called the bear cult.

But all the plebeian grassroots relict bear religious cults were surpassed by the high-minded "scientific" - atheistic cult. Also... bearish.

**Russian:**

Ряд каменных сооружений древнего мира имеет гигантские размеры и высочайшую сложность форм, порой недоступную даже современным технологиям 21-го века. Отсюда делается вывод, что до нашей цивилизации на Земле существовала другая высокоразвитая цивилизация "богов". (Под "богами" подразумеваются любые разумные существа, но не люди нашей цивилизации. Это могут быть инопланетяне, иновремяне, подземляне, подводяне, ангелы, бесы, черти, снежные человеки, гномы, эльфы, люди иной цивилизации и т.д). Либо это могли быть наши высокоразвитые предки до какой-то катастрофы и/или до того как нам переписали историю.

**English:**

A number of stone structures of the ancient world have gigantic dimensions and the highest complexity of forms, sometimes inaccessible even to modern technologies of the 21st century. This leads to the conclusion that before our civilization on Earth there was another highly developed civilization of "gods". ("Gods" means any intelligent beings, but not the people of our civilization. This can be aliens, inovremyane, underground, podvodyane, angels, demons, devils, snowmen, dwarves, elves, people of another civilization, etc.). Or it could have been our highly developed ancestors before some catastrophe and/or before history was rewritten for us

**Russian:**

Но на это существует стандартное возражение:

> "Как же так! В 18-19-м веках Питер ведь построили без машин! Так почему без машин не могли построить пирамиды и другие мегалитические сооружения древнего мира?!"

**English:**

But there is a standard objection to this:

> "How so! In the 18th and 19th centuries, Peter was built without cars! So why couldn't the pyramids and other megalithic structures of the ancient world be built without machines?!"

**Russian:**

То есть, то, что Питер строили простые люди нашей цивилизации с помощью ручных инструментов, преподносится как очевидный факт. Но факт ли это? Есть ли доказательства?

**English:**

That is, the fact that it was built by ordinary people of our civilization with the help of hand tools is presented as an obvious fact. But is this a fact? Is there any evidence?

**Russian:**

**Что может являться доказательством ручного способа строительства Питера, а не пустой болтовней или детским новогодним мифом?**

**Таким доказательством может быть ТОЛЬКО старый документ на бумаге с техническим описанием технологического процесса добычи гранитного блока из скалы и изготовления из этого блока, например, сложно изогнутой формы Александровской колонны с помощью ручных инструментов.**

**Пытаясь найти естественные объяснения возникновения Питерских мегалитов, традиционалисты начинают гадать, выдвигая гипотезы, как, возможно могли бы вырезать Александровскую колонну.**

**English:**

What can be proof of the manual way of building St. Petersburg, and not empty chatter or a children's new year's myth?

Such proof can only be an old document on paper with a technical description of the technological process of extracting a granite block from a rock and making it from this block, for example, a complex curved shape of the Alexander column using hand tools

Trying to find natural explanations for the appearance Of the St. Petersburg megaliths, traditionalists begin to guess, putting forward hypotheses about how they might have carved the Alexander column.

**Russian:**

**Но, в том то и дело, что гадания тут не уместны! Гадания уместны по поводу возникновения доисторических мегалитических сооружений древнего мира. Таких как Баальбек или Мачу-Пикчу. Потому что нет ничего удивительного в том, что через тысячи или миллионы лет не дошла до нас письменная информация о технологиях их ручного изготовления.**

**English:**

But the fact of the matter is that divination is not relevant here! Divination is appropriate for the appearance of prehistoric megalithic structures of the ancient world. Such as Baalbek or Machu Picchu. Because there is nothing surprising in the fact that thousands or millions of years have not reached us written information about the technologies of their manual manufacture.

**Russian:**

А Питер строился всего-то за несколько поколений до нас при жизни Пушкина. В хорошо задокументированый период, когда написаны были горы научной и художественной литературы на современном языке. Не только литература, но даже техническая документация в архивах. И, при этом, нет ни слова об инструментах и способе изготовления идеальных колонн из гранита. Нет ни документов, ни устных знаний у камнетесов следующих поколений.

**English:**

And Peter was built only a few generations before us during the life of Pushkin. In a well-documented period, when mountains of scientific and fiction were written in the modern language. Not only literature, but even technical documentation in the archives. And, at the same time, there is not a word about the tools and method of making perfect columns from granite. The stonemasons of the next generation have no documents or oral knowledge.

**Russian:**

**Почему приходится гадать, буд-то речь идет о допотопных сооружениях вымерших диких неандертальцев на другом конце Земли, все библиотеки которых разнес упавший астероид, сжег пожар и смыло гигантскими волнами цунами Всемирного потопа 10 000 лет назад?**

**English:**

Why do we have to wonder if we are talking about the antediluvian structures of extinct wild Neanderthals on the other side of the Earth, all the libraries of which were destroyed by a falling asteroid, burned by fire and washed away by the giant waves of the tsunami of the world flood 10,000 years ago?

**Russian:**

Официальная наука признает, что 600 тонная Ал. колонна сделана не легким способом из бетона, а сложным - из одного гигантского куска гранита. (Похожая на нее Вандомская колонна в Париже сделана из отдельных маленьких каменных блоков, как обычная заводская труба, и снаружи обложена бронзовыми барельефами).

**English:**

Official science recognizes that the 600 - ton al. column is made not in an easy way from concrete, but in a complex way - from one giant piece of granite. (The similar Vendome column in Paris is made of separate small stone blocks, like an ordinary factory chimney, and is overlaid with bronze bas-reliefs on the outside.)

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/352535_photoshopia.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/352535_photoshopia.jpg)
-->

[![](https://1.bp.blogspot.com/-oMDqLMASITU/Udsv0SC5wHI/AAAAAAAAA4I/QW4IM2nGtvY/s1600/352535_photoshopia.ru_144_Tropinin_Vasiliy_Andreevich_Portret_S._K.Suhanova._1823.jpg#clickable)](https://1.bp.blogspot.com/-oMDqLMASITU/Udsv0SC5wHI/AAAAAAAAA4I/QW4IM2nGtvY/s1600/352535_photoshopia.ru_144_Tropinin_Vasiliy_Andreevich_Portret_S._K.Suhanova._1823.jpg)

**Russian:**

Чтобы доказать, что Петербург из гранита построили не представители иной технологически развитой цивилизации, надо указать человека, который отрубывал от гранитной скалы куски толщиной с 3-этажный дом и длиной с 10-этажный. Угадайте как звали его? Вы не поверите, но его тоже звали... Август Августович! Уже третий. Это была моя веселая шутка-прибаутка. Этот метод якобы развил и воплотил русский мужик с еврейским именем Самсон не то Семенович, не то Ксенофонтович, зато однозначно Суханов.

**English:**

To prove that St. Petersburg was not built from granite by representatives of another technologically advanced civilization, it is necessary to indicate a person who cut off pieces of granite rock as thick as a 3-story house and as long as a 10-story one. Guess what his name was? You won't believe it, but he was also called... August Augustovich! It's the third one. This was my hilarious joke. This method was allegedly developed and implemented by a Russian peasant with a Jewish name Samson, either Semyonovich or Ksenofontovich, but definitely Sukhanov.

**Russian:**

Медвед - самый крупный хищник на Земле. Белый медвед - самый большой медвед. Он больше бурого вдвое.

**English:**

The bear is the largest predator on earth. The polar bear is the biggest bear. It is twice as big as brown.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/Friendship_06.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/Friendship_06.jpg)
-->

[![](https://2.bp.blogspot.com/-mG96MveN5X8/Uf3GGvfNenI/AAAAAAAABEI/T8KuaQQl1KM/s320/Friendship_06.jpg#clickable)](https://2.bp.blogspot.com/-mG96MveN5X8/Uf3GGvfNenI/AAAAAAAABEI/T8KuaQQl1KM/s320/Friendship_06.jpg)

**Russian:**

Современный белый медвед и человек. 200 лет назад они были как минимум вдвое крупнее.

В википедии и других источниках так характеризуется его сила:

**English:**

Modern polar bear and man. 200 years ago, they were at least twice as large.

In Wikipedia and other sources, this is how the bear's power is characterized:

**Russian:**

> Иногда снизу опрокидывает льдину, на которой находятся тюлени
>
> Стоя на 4-х лапах, он высотой в холке почти равен росту человека. Стоя на задних лапах рост 3.5 метра, то есть, 2 человеческих роста. Скорость бега - 40 км/ч. Современные белые мишки весят до тонны. Но они измельчали в связи с глобальным изменением климата за последние 200 лет.

> Sometimes the bottom overturns the ice floe on which the seals are located
>
> Standing on 4 legs, it is almost equal to the height of a person at the withers. Standing on its hind legs growth 3.5 meters, i.e., 2 human growth. Running speed - 40 km/h. Modern white bears weigh up to a ton. But they have been shredded by global climate change over the past 200 years.

**Russian:**

В энц-дии Брокгауза и Ефрона, изданной более 100 лет назад,

[указан вес:](http://ru.wikisource.org/wiki/%D0%AD%D0%A1%D0%91%D0%95/%D0%91%D0%B5%D0%BB%D1%8B%D0%B9_%D0%BC%D0%B5%D0%B4%D0%B2%D0%B5%D0%B4%D1%8C)

[1600 кг](http://ru.wikisource.org/wiki/%D0%AD%D0%A1%D0%91%D0%95/%D0%91%D0%B5%D0%BB%D1%8B%D0%B9_%D0%BC%D0%B5%D0%B4%D0%B2%D0%B5%D0%B4%D1%8C)
("16 центнеров весом")

**English:**

The Brockhaus and Efron encyclopedia, published more than 100 years ago, lists the weight of: [1600 kg]((http://ru.wikisource.org/wiki/%D0%AD%D0%A1%D0%91%D0%95/%D0%91%D0%B5%D0%BB%D1%8B%D0%B9_%D0%BC%D0%B5%D0%B4%D0%B2%D0%B5%D0%B4%D1%8C)) ("16 hundredweight")!

**Russian:**

Самсон Суханов жил еще на 100 лет раньше. Может быть, тогда медведы были еще больше. Цитата оттуда же:

> *Белый медведь очень дик, зол, кровожаден и очень силен, довольно быстро адвижется скачками. Защищается Б. медведь с большим мужеством и является опасным противником, особенно на льду, на котором он движется смело и уверенно. **Охотятся на Б. медведя с ружьями, но эта охота часто стоит жизни охотнику***

**English:**

Samson Sukhanov lived 100 years earlier. Maybe the bears were even bigger then. Quote from the same place:

> The polar bear is very wild, angry, bloodthirsty and very strong, moving quite quickly by leaps and bounds. B. bear defends himself with great courage and is a dangerous opponent, especially on ice, where he moves boldly and confidently. They hunt polar bears with guns, but this hunt often costs the hunter's life

**Russian:**

Поэтому на белого медведа охотятся другим способом, без непосредственного столкновения с ним:

**English:**

Therefore, the polar bear is hunted in a different way, without direct contact with it:

**Russian:**

> *Туземцы употребляют для охоты на Б. медведя следующий способ: острую полоску китового уса, дюйма в 4 длиной, свертывают спиралью, заливают тюленьим жиром, которому дают застыть, и затем кладут как приманку. Когда приманка эта попадет в желудок Б. медведя, то жир растапливается, китовый ус распрямляется и разрывает желудок и другие внутренности, и животное околевает.*

> *Родился Самсон Суханов в глухой деревушке Завотежицы Вологодской губернии в **1768** году, в поле во время косовицы, и был сыном пастуха...*

**English:**

> The natives use the following method for hunting polar bears: a sharp strip of whalebone, 4 inches long, is coiled, filled with seal fat, which is allowed to harden, and then placed as bait. When this bait gets into the stomach of a polar bear, the fat melts, the whalebone straightens and tears the stomach and other entrails, and the animal dies.
>
> Samson Sukhanov was born in a remote village Zavotezhitsy Vologda province in 1768, in a field during kosovitsa, and was the son of a shepherd...

**Russian:**

Запомните эту дату рождения -**1768** год.

Remember this date of birth - the year 1768.

**Russian:**

>... не раз ходил на звериный лов. Однажды встретился **один на &gt; один с белым медведем**. Северный властелин, не сворачивая, шел &gt; прямо на Самсона. Но и парень не отступил. Понимал: бежать нельзя, все &gt; равно догонит. Воткнул лыжи в снег, так как знал, что **на лыжи &gt; медведь никогда не бросится, будет кружить вокруг них.**

**English:**

> ... more than once went to animal fishing. Once I met one - on-one with a polar bear. The Northern Lord, without turning, went straight to Samson. But the guy didn't back down either. He knew he couldn't run, he'd catch up anyway. I stuck my skis in the snow, because I knew that the bear would never rush to the skis, but would circle around them.

**Russian:**

Почему медвед не бросится на лыжи? Кто сказал? Почему медвед ждал, пока Самсон снимет лыжи и вставит их в снег? Он в поддавки играл? Как Самсон смог окружить себя непроходимым частоколом всего из 2-х лыж! Линия Маннергейма! Великая китайская стена! Противотанковые ежи расставил. Медвежий враг не пройдет! Тундра велика да отступать некуда!

**English:**

Why doesn't the bear throw himself on skis? Who said so? Why did the bear wait for Samson to take off his skis and put them in the snow? Did he play giveaway? How Samson was able to surround himself with an impenetrable palisade of just 2 skis! The Mannerheim Line! The great wall of China! Anti-tank hedgehogs set up. The bear enemy will not pass! The tundra is big and there is nowhere to retreat!

**Russian:**

Забегая вперед скажу, что перечитав все книги и пересмотрев все документы по истории строительства Питера, можно найти все что угодно кроме одного - каким реальным понятным техническим образом удавалось резать гигантские куски гранита вручную и выпиливать из них сверхсложные формы колонн? Можно найти любую чушь вплоть до защиты от злого беломедведа с помощью лыж.

**English:**

Looking ahead, I will say that after reading all the books and reviewing all the documents on the history of construction in St. Petersburg, you can find anything but one thing - how did you manage to cut giant pieces of granite manually and cut out super-complex shapes of columns? You can find any nonsense until the protection from evil blooded using skis.

**Russian:**

Продолжаем цитировать:

**English:**

We continue to quote:

> Выставил вперед рогатину и первым пошел на медведя.
>
> Если гора не идет к Магомету, то Магомет пойдет к горе! Вперед за Родину! За Сталина! За официальную медвежью версию истории! За низовой реликтовый медвежий культ! Смерть медведам! Шашки наголо!
>
> Пусть не рыпается враг,
>
> Всех замочим только так,
>
> С шашкою на танк бросится казак.
>
> Если вражеский урод
>
> нападёт на наш народ,
>
> Атаман, веди вперёд!
>
> Шашки наголо!

**English:**

> He put forward a slingshot and went first to the bear.
>
> If the mountain does not go to Mahomet, then Mahomet will go to the mountain! Forward for the Motherland! For Stalin! To the official bearish version of the story! To the grassroots relic bear cult! Death to the bears! Checkers drawn!
>
> Don't let the enemy run away,
>
> We'll kill everyone just like this,
>
> A Cossack will rush at the tank with a saber.
>
> If the enemy is a freak
>
> attack our people,
>
> Ataman, lead the way!
>
> Draw your swords!

**Russian:**

Продолжаем цитировать:


> Медведь наткнулся на рогатину, сломал ее и всей тушей навалился на Самсона.
>
> Рогатину утыкают задним концом в землю, чтобы медведь своей силой на нее сам же и натыкался. Но как можно упереть рогатину в снег или лёд? Причем, написано, что Самсон ПОШЕЛ с рогатиной на медведя, а не упёр ее во что-то и не стоял на месте! Рогатина делается из крепкой палки.
>
> Задний конец рогатины тоже острый чтобы не скользил по опоре. А главное, что он упирается во что то твердое - в пень в данном случае. Плюс охотник с ружьем "помогает".

**English:**

We continue to quote:

> The bear bumped into a slingshot, broke it, and threw its entire bulk at Samson.
>
> The slingshot is stuck with the back end in the ground, so that the bear bumps into it with its own strength. But how can you put a slingshot in the snow or ice? Moreover, it is written that Samson went with a slingshot at the bear, and did not rest it on something and did not stand still! A slingshot is made from a strong stick.
>
>  The back end of the slingshot is also sharp so that it does not slip on the support. And the main thing is that it rests on something solid - a stump in this case. Plus a hunter with a gun "helps".

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/50006.jpeg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/50006.jpeg)
-->

[![](https://2.bp.blogspot.com/-JhIPLRsy5AQ/UrpHirmgP6I/AAAAAAAAC3w/Xm0BILOiftk/s1600/50006.jpeg#clickable)](https://2.bp.blogspot.com/-JhIPLRsy5AQ/UrpHirmgP6I/AAAAAAAAC3w/Xm0BILOiftk/s1600/50006.jpeg)

**Russian:**

Вы можете себе представить чтобы Самсон удержал РУКАМИ палку так крепко, что она треснула от натиска белого медведя, но из рук не выпустил? Крепкая рогатина сломалась, значит Самсон упирался ногами в снег с большей силой чем нужна для того чтобы треснула крепкая деревянная палка для охоты на белого медведя. Но снег ведь скользкий. Это не возможно никак. Зато медведь на льду "весьма ловок" как мы читали выше. Это его родная стихия. Он на льду как рыба в воде.

**English:**

Can you imagine Samson HOLDING the stick so tightly that it cracked from the onslaught of the polar bear, but did not let go? The strong slingshot was broken, so Samson was pushing his feet into the snow with more force than was necessary to crack a strong wooden stick for hunting a polar bear. But the snow is slippery. This is not possible in any way. But the bear on the ice is "very clever" as we read above. This is his native element. He's on the ice like a fish in water.

**Russian:**

Тот не растерялся, выдернул из-за голенища нож и всадил его зверю в &gt; бок. На зимовье вернулся с **медвежьей отметиной через все лицо ото &gt; лба к щеке.

**English:**

The man did not hesitate, pulled a knife from his boot and plunged it into the beast's side. He returned to the winter quarters with a bear mark across his face from forehead to cheek.

**Russian:**

У медведя любого шкура толстая. А у белого и подавно. Плюс очень густая толстая шерсть чтобы плавать в ледяной воде часами. А под шкурой толстый слой жира как и у всех северных животных. Ну и что, что воткнул нож, находясь под медведом? Медвед мгновенно умер? Чтобы медвед сразу умер, его надо как минимум разделить на 2 части вдоль туловища. Например, очередью из шестиствольного барабанного пулемета. А еще лучше на 4 части. Иначе раненый медвед спокойно загрызет человека. Или просто раздавит своим весом.

**English:**

Any bear has a thick skin. And the white one even more so. Plus a very thick thick coat to swim in icy water for hours. And under the skin is a thick layer of fat like all Northern animals. So what if you stuck a knife under a bear? The bear died instantly? To bear immediately died, it must be at least divided into 2 parts along the body. For example, a burst from a six-barrelled drum machine gun. And even better on 4 parts. Otherwise, a wounded bear will calmly bite a person. Or just crush it with its weight.

**Russian:**

Почему он не ходил на медвежью охоту с ружьём, ведь ружья уже были тогда? Это все равно что на танки охотиться с помощью шашки, имея пушку. Напомню, что даже охота с ружьем часто оканчивается смертью охотника. Потому что не только нож, но и пуля не может завалить медведя. Поэтому его предпочитают разрывать изнутри приманкой из заточенного китового уса. Снаружи медведя не возьмешь даже с ружьем.

**English:**

Why didn't he go bear hunting with a gun, since there were guns already? It's like hunting tanks with a saber, having a gun. Let me remind you that even hunting with a gun often ends in the death of the hunter. Because not only a knife, but a bullet can't take down a bear. Therefore, they prefer to break it from the inside with a bait made of sharpened whalebone. You can't take a bear outside with a gun.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/claws1.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/claws1.jpg)
-->

[![](https://3.bp.blogspot.com/-4cnKEDKyymc/Ud2sfkMf4EI/AAAAAAAAA5E/_ALjzhlgO-k/s1600/claws1.jpg#clickable)](https://3.bp.blogspot.com/-4cnKEDKyymc/Ud2sfkMf4EI/AAAAAAAAA5E/_ALjzhlgO-k/s1600/claws1.jpg)

**Russian:**

> *На зимовье вернулся **с медвежьей отметиной через все лицо ото лба к щеке***

**English:**

> He returned to the winter quarters with a bear mark across his face from forehead to cheek

[](https://3.bp.blogspot.com/-4cnKEDKyymc/Ud2sfkMf4EI/AAAAAAAAA5E/_ALjzhlgO-k/s1600/claws1.jpg)

**Russian:**

У белого медведя когти около 10 см (на рисунке справа). Это у современного. А мы читали что 100 лет назад они были вдвое крупнее. Если он оставит на лице отметину...

> "через все лицо ото лба к щеке".

**English:**

The polar bear has claws of about 10 cm (in the picture on the right). This is a modern one. And we read that 100 years ago they were twice as large. If he leaves a mark on his face...

> "across the face from forehead to cheek"

**Russian:**

Вообще-то, между лбом и щекой нет "всего лица". Лоб переходит в щеку через глаз. Может быть, через весь глаз, а не через все лицо? Суханов бы вернулся без глаза.

**English:**

Actually, there is no "whole face" between the forehead and the cheek. The forehead passes into the cheek through the eye. Maybe through the whole eye instead of the whole face? Sukhanov would have returned without an eye.

**Russian:**

Еще цитата:

Another quote:

> Сила этого зверя поистине поразительна. Он способен вытащить на лед и &gt; поднять вверх по склону тушу моржа весом более полутонны. &gt; Тюленя-лахтака, весящего ненамного меньше самого медведя, хищник может &gt; убить, раздробив единственным сокрушительным ударом лапы череп жертвы, &gt; и при необходимости перенести ее тушу в зубах на расстояние до &gt; километра.

**English:**

> The strength of this beast is truly amazing. It is able to pull a walrus carcass weighing more than half a ton onto the ice and lift it up the slope. The predator can kill a lakhtak seal, which weighs not much less than the bear itself, by crushing the victim's skull with a single crushing blow of its paw, and if necessary, transfer its carcass in its teeth to a distance of up to a kilometer.

**Russian:**

А Самсончику только царапинку оставил.

**English:**

And Samsonite just a scratch left.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/1314085106_6_3.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/1314085106_6_3.jpg)
-->

[![](https://2.bp.blogspot.com/-HhQLApi-7oE/Ud2uV8XMSUI/AAAAAAAAA5U/cQtuK3puuz8/s1600/1314085106_6_3.jpg#clickable)](https://2.bp.blogspot.com/-HhQLApi-7oE/Ud2uV8XMSUI/AAAAAAAAA5U/cQtuK3puuz8/s1600/1314085106_6_3.jpg)

**Russian:**

Слева на фотографии лапа бурого медведя. У белого вдвое больше. Обратите внимание на размер когтей. Жаль, не нашел фотку лапы белого медведя на
фоне человека.

**English:**

On the left side of the photo is the paw of a brown bear. White has twice as much. Pay attention to the size of the claws. Sorry, I didn't find a picture of a polar bear's paw against a human background.

**Russian:**

Цитата:

> *Весной 1785 года артель вернулась в Архангельск с моржовыми салом и клыками, голубыми песцовыми шкурками, гагачьим пухом, медвежьими шкурами (две из них были трофеями Суханова)*


**English:**

Quote:

> In the spring of 1785, the artel returned to Arkhangelsk with walrus fat and tusks, blue Arctic Fox skins, eiderdown, and bear skins (two of them were Sukhanov's trophies)

**Russian:**

Ого! Оказывается он не одного а двух белых медведей завалил в рукопашном бою! Обратите внимание на дату. Весна 1785-го года. А родился то он летом 1768-го года! Значит 17 лет ему исполнится летом 1785 года. То есть, обоих беломедведов он завалил в 16 лет!

**English:**

Wow! It turns out that he killed not one but two polar bears in hand to hand combat! Pay attention to the date. Spring of 1785. And he was born in the summer of 1768! So he will be 17 years old in the summer of 1785. That is, he failed both belomedvedov at the age of 16!

**Russian:**

Жаль что к тому времени вымерли мамонты. А то б Самсон Суханов и их валил пачками.

**English:**

It is a pity that by that time the mammoths were extinct. Otherwise, Samson Sukhanov would have brought them down in bundles.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/big.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/big.jpg)
-->

[![](https://3.bp.blogspot.com/-WD8qayl1MSI/Uf3HnLWs4-I/AAAAAAAABEY/XQZYjtLBgyI/s1600/big.jpg#clickable)](https://3.bp.blogspot.com/-WD8qayl1MSI/Uf3HnLWs4-I/AAAAAAAABEY/XQZYjtLBgyI/s1600/big.jpg)

**Russian:**

> Это шкурка того самого медведа,которого завалил Самсон полярной ночью в 16-ти летнем возрасте.

> *Всем любителям художеств известен в Петербурге Самсон **Семенович** Суханов за весьма искусного каменьщика-ваятеля. ... Отец его **Ксенофонт** ...*

**English:**

> This is the skin of the bear, who was taken down by Samson on a polar night at the age of 16.

> All art lovers in St. Petersburg know Samson Semyonovich Sukhanov for a very skilled stonemason-sculptor. .. His father is Xenophon ...

**Russian:**

Отчество Семенович, а отец - Ксенофонт. Это типичный пример зоологического идиотизма.

**English:**

Patronymic S., and the father of Xenophon. This is a typical example of zoological idiocy

**Russian:**

Так Семенович или Ксенофонтович? И эта путанница повторяется почти во всех источниках. Кстати, почему если нерусский то фамилия красивая - Монферран, Бетанкур, а как русский то сразу Свиньин! Словно выдумавшие всх этих лиц специально стремились унизить русских. Товарищ Свиньин продолжает:

> *Мать же его кормилась зимою подаянием, а летом жила в работницах, где и произвела его на свет на сенокосе в 1766 году*

**English:**

So Semyonovich or Ksenofontovich? And this confusion is repeated in almost all sources. By the way, why if the non - Russian name is beautiful-Montferrand, Betancourt, and as a Russian immediately Svinin! As if the invented SOx of these persons specifically sought to humiliate the Russians. Comrade Svinyin continues:

> His mother, on the other hand, lived on alms in the winter, and in the summer lived in working women, where she gave birth to him in the Hayfield in 1766

**Russian:**

Жаль что Свиньин не рассказал от кого она родила на сенокосе - от Семена или Ксенофонта? Это уже вторая версия биографии. По первой он родился на 2 года позже. Наверное в 66-м году родился от Ксенофонта, в 68-м от Семена.

**English:**

It is a pity that Svinyin did not tell from whom she gave birth in the haymaking - from Semyon or Xenophon? This is the second version of the biography. According to the first one, he was born 2 years later. Probably in the 66th year was born from Xenophon, in the 68th from Semyon.

**Russian:**

Далее мы узнаем, что это были ночные бои с медведами в полярную ночь. Конкурс зоологического медвежьего идиотизма продолжается:

**English:**

Next, we learn that these were night fights with bears in the polar night. The competition of Zoological bear idiocy continues:
<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/preved_medved.JPG#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/preved_medved.JPG)
-->

[![](https://3.bp.blogspot.com/-Sl0rZK2UZhs/UkUPwVY5cQI/AAAAAAAAB3M/yq-vj1yXLu4/s1600/preved_medved.JPG#clickable)](https://3.bp.blogspot.com/-Sl0rZK2UZhs/UkUPwVY5cQI/AAAAAAAAB3M/yq-vj1yXLu4/s1600/preved_medved.JPG)

**Russian:**

1-го октября закатилось солнце, настала глубокая темнота...

> *Однажды Самсон, идя по горе с одним из товарищей своих – встретил **страшного белого медведя.** Зверь шел прямо на него **на задних ногах с раскрытою пастью и ужасным ревом***

**English:**

On October 1, the sun went down and it was very dark...

> One day Samson, walking along the mountain with one of his companions, met a terrible polar bear. The beast was coming straight at him on its hind legs with its mouth open and a terrible roar

**Russian:**

Напомню - дикие русские обезяностроители, скакавшие в мороз по строительным лесам, тоже работали в темноте полярной ночи, для чего и держали в зубах кольцо фонаря.

**English:**

Let me remind you that the wild Russian monkey builders who rode through the construction forests in the cold also worked in the dark of the polar night, for which they held a lantern ring in their teeth.

**Russian:**

> Мочной Самсон, оставленный своим товарищем, перекрестясь, ставит лыжи свои на снег, берет рогатину в руки, с стремлением нападает на неприятеля своего и к счастью дает ему глубокую рану в бок: увидь текущую кровь, медведь придается бегству и в виду победителя падает от изнеможения.

**English:**

> Monk Samson, left behind by his comrade, crosses himself, puts his skis on the snow, takes a slingshot in his hands, eagerly attacks his enemy and fortunately gives him a deep wound in the side: see the blood flowing, the bear is given to flight and in sight of the winner falls from exhaustion.

**Russian:**

Оказывается, медведь не наваливался на Самсона, а наоборот, убегал с перепугу! Так что делал медведь: трусливо убежал или мужественно навалился? Сочинявшие этот идиотизм не учли главного. Медведь никогда не убегает получив ранения. Именно на этом принципе основан метод охоты на медведя с помощью рогатины. Медведь натыкается на нее и своей же собственной силой продолжает на нее насаживаться несмотря на боль. Самсоновские же медведы какие-то пугливые. То боятся обойти воткнутые в снег лыжи, то боятся ранений от ножа. Это какие то антимедведы или лжемедведы.

**English:**

It turns out that the bear did not lean on Samson, but on the contrary, ran away with fright! So what did the bear do: cowardly run away or courageously piled on? The authors of this idiocy did not take into account the main thing. A bear never runs away after being injured. It is on this principle that the method of hunting a bear with a slingshot is based. The bear bumps into it and with its own strength continues to impale itself on it despite the pain. Samson's bears are kind of skittish. They are afraid to avoid skis stuck in the snow, or they are afraid of knife wounds. These are some anti-Vedas or false Vedas.

**Russian:**

> Самсон, с помощью трусливого товарища своего, несколькими ударами довершает победу, снимает с него кожу и с торжеством возвращается в жилище свое.

**English:**

> Samson, with the help of his cowardly comrade, completes the victory with a few blows, removes the skin from him, and returns in triumph to his dwelling.

**Russian:**

Представляю что это за картина. Напасть на раненого белого медведя. Раненый - не мертвый. Он еще опаснее чем не раненый. А эти 2 товарища смело к нему подкатили.

**English:**

I can imagine what this picture is. Attack a wounded polar bear. Wounded, not dead. He's even more dangerous than not being injured. And these 2 comrades boldly rolled up to him.

**Russian:**

> В январе 1785 случилось ему, **опять одному, напасть на зверя сего,** и тогда уже он не с трусостью, но с бодростью духа, шел прямо на него и победил.*

**English:**

> In January, 1785, it happened to him, again alone, to attack this beast, and then, not with cowardice, but with cheerfulness of spirit, he went straight to it and won.

**Russian:**

Оказывается, первый мишка убегал, а второй навалился. Так все-таки, медведи пугливые или смелые?

**English:**

It turns out that the first bear ran away, and the second piled on. So after all, are bears timid or bold?

**Russian:**

А потом он напал на динозавра и победил, а потом на трехголового Змея-Горыныча. А когда гады фашисты напали на святую Русь, гранитоломатель Самсон нападал с рогатиной на бронетанк "Пантера". Танк сломал рогатину, навалился всей броней на гранитоломателя, а гранитоломатель голыми руками разорвал танку гусеницу, вынул нож из голенища и снял с танка броню и кусок мяса. А другой танк убегал от Самсона. Но Самсон догнал и тоже снял шкуру. А потом он вышел с рогатиной на бронепоезд и победил его. А потом на броненосец и утопил его. А когда он был маленький атеистический мальчик Самсоша, то нырял в воду и утопил большую белую акулу, кита и кашалота. Самсоша без оружия 2 полуторатонных мишек завалил а дикие русские обезьяны-строители без всяких измерительных приборов построили самый красивый город. Однобокая фантазия. Подобным подвигом прославился и Виктор Степанович Черножмырдин. Только он расстрелял в упор двух новорожденных медвежат размером с кошку. Ознакомьтесь

[http://levhudoi.blogspot.com/2013/09/blog-post_21.html](http://levhudoi.blogspot.com/2013/09/blog-post_21.html)

**English:**

And then he attacked the dinosaur and won, and then the three-headed snake-Gorynych. And when the bastards the Nazis attacked the Holy Russia, gravitometer Samson attacked with a spear on bronmark "Panther". Tank broke the spear, leaned on the whole armor of granitoides and gravitometer bare hands ripped the tank caterpillar, took out a knife from the shaft and removed from the tank armor and a piece of meat. And the other tank was running away from Samson. But Samson caught up and also took off the skin. And then he went out with a slingshot on an armored train and defeated it. And then on the battleship and sank it. And when he was a young atheist boy Samosa, then dived into the water and drowned a great white shark, whale and sperm whale. Samsosha without weapons 2 polutoratonnykh bears filled up and wild Russian monkeys-builders without any measuring devices built the most beautiful city. One-sided fantasy. Such a feat became famous Viktor Stepanovich Chernomyrdin. Only he shot at point-blank range two newborn bear cubs the size of a cat. Read more [http://levhudoi.blogspot.com/2013/09/blog-post_21.html](http://levhudoi.blogspot.com/2013/09/blog-post_21.html)

**Russian:**

А вот [третий вариант биографии Самсона из библиографической энциклопедии](http://enc-dic.com/enc_biography/Suhanov-samson-semenovich-17594.html):

> родился 27 июня 1776 г. в Евской волости Вологодской губернии, умер в 1820-х годах.

**English:**

And here is [the third version of the biography of Samson from the bibliographic encyclopedia:](http://enc-dic.com/enc_biography/Suhanov-samson-semenovich-17594.html)

> born on June 27, 1776 in Evskaya volost, Vologda province, he died in the 1820s.

**Russian:**

И так, он родился сначала в 1766 году, потом в 1768-м и 1776-м. Соответственно, на медведа он ходил уже в 8-ми летнем возрасте. А умер в 1820-х. Казалось бы, 3 дня рождения с разбросом в 10 лет хватит. Но, не тут-то было. Медведоборческий зуд зудит. Как черт из табакерки выскакивает это

[http://www.booksite.ru/fulltext/dvi/nsk/aya/7.htm](http://www.booksite.ru/fulltext/dvi/nsk/aya/7.htm)

**English:**

And so, he was born first in 1766, then in 1768 and 1776. Accordingly, he went to the bear at the age of 8. He died in the 1820s. It would seem that 3 birthdays with a spread of 10 years will be enough. But that wasn't the case. Medvedovski itchy itchy. How the hell does this come out of a snuffbox

[http://www.booksite.ru/fulltext/dvi/nsk/aya/7.htm](http://www.booksite.ru/fulltext/dvi/nsk/aya/7.htm)

**Russian:**

> *Время не сохранило ни дома, ни родственников непревзойденного мастера. Остались лишь документы, позволяющие рассказать об этом удивительном человеке — Самсоне Ксенофонтовиче Суханове.*
>
> ***Первым биографом С. Суханова** был издатель журнала «Отечественные записки» П. П. Свиньин, опубликовавший большую статью — биографию. Работа содержит много ценных сведений, однако, как показали многолетние исследования проф. Н. П. Борисова и проф. Н. Ф. Хомутецкого, в статье допущен ряд неточностей и даже ошибок.*
>
> *В том числе* ***неправильно указан год рождения мастера.** Изучение и проверка данных жизни С. Суханова дают основание теперь сказать, что Самсон Ксенофонтович **родился в 1769 году** в деревне Завотежице, около села Красноборска (ныне Архангельская область).*

**English:**

> Time has not preserved either the house or relatives of the unsurpassed master. There are only documents that allow us to tell About this amazing man — Samson Ksenofontovich Sukhanov.
>
> The first biographer of S. Sukhanov was P. P. Svinin, the publisher of the magazine "Otechestvennye Zapiski" ("Domestic Notes"), who published a large article — biography. The work contains a lot of valuable information; however, as shown by long-term research by prof. N. P. Borisov and prof. N. F. Khomutetsky, the article contains a number of inaccuracies and even errors.
>
> The year of the master's birth is also incorrectly specified. Study and verification of the life of S. Sukhanov give reason now to say that Samson Ksenofontovich was born in 1769 in the village of Sabotiere, near the village of Krasnoborsk (Arkhangelsk oblast now).

**Russian:**

То есть, по **уточненным** данным, медведов замочил в **15-тилетнем** возрасте. Продолжаем читать:

**English:**

That is, according to bears soaked in 15-year-old age. Continue reading:

> *Имущество его в **1837** году было продано на торгах в счет стоимости потонувшей на Ладожском озере баржи с пьедесталами под памятники М. Кутузову и Барклаю де Толли. Новые пьедесталы пришлось изготавливать на собственные деньги.*

> His property in 1837 was sold at auction for the cost of a barge sunk on Lake Ladoga with pedestals for monuments to M. Kutuzov and Barclay de Tolly. New pedestals had to be made with their own money.

**Russian:**

Ну вот опять! Мы же читали в предыдущем источнике, что он умер в 20-х годах! А оказалось после 1837 года опять был жив. Самсон Воскрес! Воистину воскрес!

Они ссылаются на документ:

> *В одном из архивов проф. Н. Хомутецкий обнаружил волнующий документ о последних годах жизни мастера...*

> *Семья Суханова была полностью разорена. Об этом свидетельствует прошение Великому князю от **5 октября 1840 года** (этот документ обнаружил исследователь русского искусства профессор Н. **К**ому**ж**ецкий).*

**English:**

Here we go again! We read in the previous source that he died in the 20s! But it turned out that after 1837 he was alive again. Samson Is Risen! He is risen indeed!

They refer to the document:

> In one of the archives, Prof. N. Khomutetsky found an exciting document about the last years of the master's life...

> The Sukhanov family was completely ruined. This is evidenced by a petition to the Grand Duke dated October 5, 1840 (this document was discovered by a researcher of Russian art, Professor N. Komuzhetsky).

**Russian:**

А ведь только что мы читали, что его обнаружил **Х**ому**т**ецкий.

**English:**

But we just read that it was discovered by Khomutetsky.

**Russian:**

> *В документе старый мастер пишет о том бедственном положении, в котором оказалась его семья и просить выдать "...для насущего питания семейства всемилостивейшее вспоможение". Просьба, судя по всему, осталась без ответа, но этот документ помогает установить примерную дату кончины знаменитого каменотеса.*

> *Умер Самсон Суханов в полной нищете в **сороковых годах** прошлого века.*

**English:**

> In the document, the old master writes about the plight in which his family found themselves and asks for extradition."..for us, the food supply of the family is the most gracious blessing." The request seems to have gone unanswered, but this document helps establish the approximate date of the famous stonemason's death.

> Samson Sukhanov died in complete poverty in the forties of the last century.

**Russian:**

А почему он рождался 4 раза, а отцов только двое? А потому что Семен и Ксенофонт по 2 раза становились его отцами. Но мать так и осталась девственницей.

**English:**

And why was he born 4 times, and only two fathers? But because Semyon and Xenophon 2 times became his fathers. Though his mother remained a virgin.

> **а потом пришла очередь** Стрелки Васильевского острова**. Артель Суханова появилась на Васильевском острове в 1803 году. Ей предстояли земляные и каменные работы. Земляные -** поднять низкий берег и перед зданием Биржи**, потеснив реку, насыпать площадь, одеть ее в гранит. По рисункам И. П. Прокофьева Самсон Ксенофонтович высек** Нептуна с трезубцем и колесницей,** **богиню Навигацию с Меркурием и двумя реками**. Они украсили **фасад и противоположную сторону Биржи.** Пять лет артель Суханова одевала в гранит **мыс Васильевского острова**, а затем были созданы **Ростральные колонны.****

**English:**

And then it was the turn Of the spit/arrow of Vasilievsky island. The Sukhanov artel appeared on Vasilievsky island in 1803. She had to do earth and stone work. Earthen - raise a low bank and in front of the Exchange building, pushing the river, fill the area, dress it in granite. According to the drawings of I. P. Prokofiev, Samson Ksenofontovich carved Neptune with a trident and a chariot, the goddess Navigation with mercury and two rivers. They decorated the facade and the opposite side of the Exchange. For five years, the Sukhanov artel dressed the Cape of Vasilievsky island in granite, and then Rostral columns were created.

**Russian:**

> *В артель к нему **люди пошли охотно.** Видели: человек он справедливый, бывалый, мастер знатный, дотошный - в свободное время выучился грамоте, учится архитектурные чертежи читать - с таким не пропадешь.*

**English:**

> The gang went willingly to his people. We saw that he was a fair, experienced man, a noble master, meticulous - in his spare time, he learned to read and write, he learns to read architectural drawings - you can't lose yourself with this.

**Russian:**

Это специально для тех, кто верит тому, что злые дикие русские цари людей загоняли силой на каменоломни как рабов.

**English:**

This is especially for those who believe that the evil wild Russian tsars drove people by force to the quarries as slaves.

**Russian:**

> *А потом были **колонны Исаакиевского собора.** На **башне Адмиралтейства стоят высеченные им статуи воинов.** Его артель одела камнем **Крюков канал** и построила почти **стометровый Синий канал**, **в Павловском парке - мостик**, а в **Баболовских банях высекли огромную каменную ванну из цельного куска гранита**, в Москве - **пьедестал для памятника Минину и Пожарскому.***

**English:**

> And then there were the columns of St. Isaac's Cathedral. On the Admiralty Tower there are statues of soldiers carved by him. His artel dressed the Kryukov canal with stone and built an almost hundred-meter Blue Canal, a bridge in Pavlovsk Park, and a huge stone bath was carved from a single piece of granite in Babolovsky baths, and a pedestal for the monument to Minin and Pozharsky in Moscow.

**Russian:**

> *Много сделал Самсон Суханов для города на Неве. А вот старость была у него тяжелой. **К середине XIX века, когда классицизм стал выходить из моды, у его артели сократилось число заказчиков.** Началась полоса неудач. Он умер таким же бедняком, каким пришел сюда из-под Вологды.*

**English:**

> Samson Sukhanov did a lot for the city on the Neva. But his old age was hard. By the middle of the Nineteenth Century, when classicism began to go out of fashion, his artel reduced the number of customers. A losing streak began. He died as poor as he came here from Vologda.

**Russian:**

> Какая разница какой стиль? Главное уметь резать камень. Хоть в стиле рококо, хоть барокко. Нептуна он вылепил по рисунку И. П. Прокофьева. Стиль задает не каменщик, а художник. А уж отрубывание глыб в скалах вообще не зависит от стиля моды. Если мода на блондинок прошла то хороший фотограф не сможет фотографировать брюнеток и у него начнется полоса неудач? Почему ему придумали имя Самсон? Потому что Самсо́н (ивр. שִׁמְשׁ‎, Шимшо́н) — ветхозаветный Судья-герой, прославившийся необыкновенной физической силой:

**English:**

> What does it matter what style? The main thing is to be able to cut stone. Even in the Rococo or Baroque style. He modeled Neptune after a drawing by I. P. Prokofiev. The style is set not by the bricklayer, but by the artist. And cutting off blocks in the rocks does not depend at all on the fashion. If the fashion for blondes has passed then will a good photographer not be able to photograph brunettes and he will have a bad streak? Why did they give him the name Samson? Because Samson (Hebrew: שִׁמְשׁוֹן, Shimshon) is an Old Testament judge — a hero who became famous for his extraordinary physical strength:

**Russian:**

> "И сошел на него Дух Господень, и** он растерзал льва**; &gt; а в руке у него ничего не было" (Судей 14:6)*

**English:**

> "And the Spirit of the Lord came upon him, and he tore the lion to pieces; but he had nothing in his hand" (Judges 14:6)

**Russian:**

Вот в честь него и назвали русского сказочного богатыря. Библейский Самсон порвал льва - самого большого хищника в тех местах, а русский Самсон - белого медведя, тоже самого большого хищника в его местах.

**English:**

Here in honor of him and named the Russian fairy-tale hero. The biblical Samson tore up the lion, the largest predator in those places, and the Russian Samson tore up the polar bear, also the largest predator in its area.

<!-- Local
[![](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/fontan-samson.jpg#clickable)](levhudoi.blogspot.com%20Contradictions%20and%20absurdity%20of%20the%20official%20version%20of%20the%20construction%20of%20St.%20Petersburg_files/fontan-samson.jpg)
-->

[![](https://2.bp.blogspot.com/-XZpwd-GMoM4/Udsn1hbgBLI/AAAAAAAAA30/yZqAa9VNrsI/s1600/fontan-samson.jpg#clickable)](https://2.bp.blogspot.com/-XZpwd-GMoM4/Udsn1hbgBLI/AAAAAAAAA30/yZqAa9VNrsI/s1600/fontan-samson.jpg)

**Russian:**

*«Самсон» — центральный фонтан дворцово-паркового ансамбля Петергоф скульптора Михаила Ивановича Козловского, «Самсон, раздирающий пасть льва».*

**English:**

> "Samson" — the Central fountain of the Peterhof Palace and Park ensemble by sculptor Mikhail Ivanovich Kozlovsky, "Samson tearing the lion's mouth".

**Russian:**

А. М. Платунов, автор солидного труда «Так строился Петербург» (издан в 1997 году), написал в сноске: «Самсон — древнегреческий мифический герой, которому приписывалась сверхъестественная физическая сила и отвага» Таким образом, по воле Платунова, Самсон «перекочевал» в древнегреческую мифологию.

> *Нашел он свежую ослиную челюсть, протянув руку свою, взял ее, и убил ею тысячу человек. (Судей 15:15)*

Ну, хорошо, возразите, как он может быть вымышленным персонажем? Ведь наверное есть его могила? Цитирую:

> ***Мы не знаем ни год смерти великого мастера, ни место, где он был похоронен.***

Так он вознесся на небо! На огненной колеснице! Запряженной в ангелов с лицами белых медведей и с крыльями бабочки! Самсон воскрес! Во истину воскрес! Пришли утром атеисты помолиться Самсону, а могила пуста. И только ангел в белой одежде говорит:

> "Что вы пришли искать живого среди мертвых?"
>
> В книгах о городе искусствоведы даже не упоминали его &gt; имени.***

**English:**

A. M. Platunov, author of the solid work *"So Petersburg was Built"* (published in 1997), wrote in a footnote: "Samson is an ancient Greek mythical hero, who was credited with supernatural physical strength and courage". Thus, by Platunov's will, Samson "migrated" to ancient Greek mythology.

> He found a fresh donkey's jaw, stretched out his hand, took it, and killed a thousand people with it. (Judges 15:15)

Well, how can he be a fictional character? There must be a grave for him, right? Quote:

> We do not know the year of the great master's death, nor the place where he was buried.

So he ascended to heaven! On a chariot of fire! Harnessed to angels with the faces of polar bears and butterfly wings! Samson is risen! In truth he is risen! The atheists came to pray to Samson in the morning, but the grave was empty. And only an angel in white clothes says:

> "That you have come to seek the living among the dead?"
>
> In books about the city, art historians did not even mention its name.

**Russian:**

Какое может быть лучшее доказательство несуществования в природе этого Самсона? В книгах о Питере не упоминался раньше, а потом вдруг как прорвало - появляется почти во всех книгах о Питере. Ведь надо кому-то приписать таинственный способ вырубки гранита для всего Питера.

**English:**

What better proof can there be of the non-existence of this Samson in nature? In the books about Peter was not mentioned before, and then suddenly as a burst - appears in almost all the books about Peter. After all, it is necessary to attribute to someone a mysterious way of cutting granite for the whole of St. Petersburg.

**Russian:**

Как можно не знать место погребения и год смерти? И дети его тоже не знают? И внуки? Ведь он был женат по их версии. [Звали ее Евдокия Правдина.](http://www.arhpress.ru/znamya/2003/6/27/5.shtml) Вот откуда пошло название газеты "Правда"!

**English:**

How can you not know the place of burial and the year of death? And the kids don't know him either? And grandchildren? After all, he was married according to their version. [Her name was Evdokia Pravdina](http://www.arhpress.ru/znamya/2003/6/27/5.shtml). That's where the name of the newspaper "Pravda" came from!

**Russian:**

Долгое время мы даже не знали, как он выглядит.

**English:**

For a long time, we didn't even know what it looked like.

**Russian:**

> *Однажды у одного московского коллекционера нашли старый холст. Под толстым слоем копоти и грязи невозможно было разобрать, что на нем изображено. Но реставраторы все-таки заинтересовались холстом. Сантиметр за сантиметром они начали очищать его и увидели подпись художника: «Василий Тропинин» - автор многих картин и портретов знаменитых людей.*

**English:**

> Once an old canvas was found in the possession of a Moscow collector. Under the thick layer of soot and dirt, it was impossible to make out what was depicted on it. But the restorers are still interested in the canvas. Inch by inch they began to clear it and saw the artist's signature: Vasily Tropinin is the author of many paintings and portraits of famous people.

**Russian:**

*Небольшой зоологический идиотизм из другого [источника](http://www.spb300.osis.ru/alternativa/builder/suchanov.htm)*

**English:**

*A little zoological idiocy from another [source](http://www.spb300.osis.ru/alternativa/builder/suchanov.htm)*

**Russian:**

> Деревенский поп отыскал ему редкое имя - Самсон, наверное, и не зная, &gt; что по древним мифам Самсон - силач необыкновенный.*

**English:**

> The village priest found him a rare name - Samson, probably not knowing that according to ancient myths, Samson is an extraordinary strongman.

**Russian:**

Фамилию Суханов мифотворцы тоже взяли не случайную. Сухан - тоже богатырь, но русский.

**English:**

The myth-makers also took the surname Sukhanov for a reason. Sukhan is also a hero, but Russian.

**Russian:**

В ["ПОВЕСТИ О СУХАНЕ"](http://old-ru.ru/08-16.html) неизвестного автора 17 века более чем 90-летний Сухан вырвал с корнем молодой дуб и этим дубом перебил татарское войско, после чего был застрелен из пушки:

> *Во граде КиевЪ бысть при старосте,*
>
> *При великом князе Манамахе Владимеровиче,*
>
> *Был богатырь стар добрЪ,*
>
> ***Больши ему девяноста лЪтъ.*** ...
>
> *И поехалъ Сухан ко дуброве зеленой,*
>
> ***И наехал сыр-зелен падубок,***
>
> ***Да вырвалъ ево и с кореньемъ,***
>
> ***Да едет с ним не очищаюч*** ...
>
> ***Восплакала мать Суханова...***
>
> *И отънесла ево в пещеру каменну:*
>
> *«Тут тебЪ, Суханушко, смертной животъ во вЪки».*

**English:**

In ["The Story of Sukhana"](http://old-ru.ru/08-16.html) by an unknown 17th century author, more than 90-year-old Sukhan uprooted a young oak tree and killed the Tatar army with this oak, after which he was shot from a cannon:

> In the city of Kiev byst under the headman,
>
> Under the Grand Duke Manamakha vladimerovich,
>
> The hero was old and kind,
>
> Give him more than ninety LTS. ...
>
> And Suhan went to the green Dubro,
>
> And ran over the cheese-green Holly,
>
> Yes pulled out Evo and with the root,
>
> Yes goes with him nezchayuch ...
>
> Sukhanov's mother wept...
>
> And she took Evo to Kamenna's cave.
>
> "Here you are, Sukhanushko, with your stomach in your hands."

**Russian:**

У более чем 90 летнего Сухана оказывается была жива мать, то есть ей минимум 110 лет. И так, что мы имеем в сухом остатке? Гигантские грантные блоки отрубывал и обтесывал до совершенных форм медведобой Самсон Ксенофонтович Суханов, сын Семена, трижды рожденный и дважды умерший, а устанавливал мегалиты напопА восставший из ада мертвец Август Августович Бетанкур и живой русский архитектор Монферран, тоже Август Августович.

**English:**

More than 90-year-old Suhan turns out to have a living mother, that is, she is at least 110 years old. So, what do we have on the bottom line? Giant granite blocks were cut and cut to perfect shapes by the bear-hunter Samson Ksenofontovich Sukhanov, Semyon's son, who was born three times and died twice, and the megaliths were installed by the dead man August Augustovich Betancourt, who rose from Hell, and the living Russian architect Montferrand, also August Augustovich.

**Russian:**

Википедия. Обратите внимание на его вторую фамилию.

> *Па́вел Петро́вич Тугой-Свиньи́н (1787-1839)*
>
> **** Родился в усадьбе Ефремово Галичского уезда в семье лейтенанта флота Петра Никитича Свиньина и Екатерины Юрьевны Лермонтовой. Поэт М. Ю. Лермонтов приходился ему двоюродным племянником. **
>
> Не понятно, откуда у него фамилия Тугой. Он Самсону Суханову приписал двух отцов, может быть, потому, что у него самого было 2 отца, и один из них Тугой?
>
> *Служил в Московском архиве Коллегии иностранных дел.*
>
> *Во всех своих многочисленных занятиях Свиньин был **дилетантом**.*
>
> ... в литературном обществе к Свиньину относились по большей части иронически из-за его склонности к **преувеличениям с оттенком сенсации**, а **также из-за готовности прислуживаться перед властям**и (его брат был женат на сестре могущественного [П. А. Клейнмихеля](http://ru.wikipedia.org/wiki/%D0%9A%D0%BB%D0%B5%D0%B9%D0%BD%D0%BC%D0%B8%D1%85%D0%B5%D0%BB%D1%8C,_%D0%9F%D1%91%D1%82%D1%80_%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B5%D0%B2%D0%B8%D1%87)).
>
> *Зло вышучивали его как Пушкин (сказочка **«Маленький лжец»**), так и Вяземский (эпиграмма «Что пользы, — говорит расчетливый Свиньин»).*

**English:**

Wikipedia. Note his middle name.

> Pavel Petrovich Tight-Svin'in (1787-1839)
>
> He was born in the estate of Efremovo, Galich district, in the family of fleet Lieutenant Pyotr Nikitich Svinin and Ekaterina Yuryevna Lermontova. The poet M. Y. Lermontov was his first cousin.
>
> It is not clear where he got the last name Tugoy. He attributed two fathers to Samson Sukhanov, maybe because he himself had 2 fathers, and one of them is Tight?
>
> Served in the Moscow archive of The Board of foreign Affairs.
>
> In all his many pursuits, Svinyin was an Amateur.
>
>.. in literary society, Svinyin was mostly treated ironically because of his tendency to exaggerate with a touch of sensationalism, as well as because of his willingness to serve the authorities (his brother was married to the sister of the powerful [P. A. Kleinmichel](http://ru.wikipedia.org/wiki/%D0%9A%D0%BB%D0%B5%D0%B9%D0%BD%D0%BC%D0%B8%D1%85%D0%B5%D0%BB%D1%8C,_%D0%9F%D1%91%D1%82%D1%80_%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B5%D0%B2%D0%B8%D1%87)).
>
> Both Pushkin (in the fairy tale "The Little Liar") and Vyazemsky (in the epigram "What's the use, says the calculating Svinyin") made fun of him.

**Russian:**

Даже Пушкин назвал его "маленький лжец"! А современные ученые его считают большим правдецом, составившим правдивейшую биографию Самсона Суханова!

**English:**

Pushkin even called him a "little liar"! And modern scientists consider him a great truth-teller, who compiled a true biography of Samson Sukhanov!

**Russian:**

> *«Беспокойно преувеличенный патриотизм» Свиньина проявлялся в увлечении исторической беллетристикой и в том, что «**для каждой книжки своего журнала создавал он какого-нибудь русского гения-самоучку**»»*

**English:**

>Svinyin's "restlessly exaggerated patriotism" was manifested in his passion for historical fiction and in the fact that "for each book in his magazine, he created some self-taught Russian genius"

**Russian:**

Еще вопросы остались? Напоследок, вот сказочка Пушкина о Павле Свиньине:

**English:**

Any other questions? Finally, here is Pushkin's fairy tale about Pavel Svinyin:

**Russian:**

> *Павлуша был опрятный, добрый, прилежный мальчик, но имел большой порок. **Он не мог сказать трех слов, чтоб не солгать.** Папенька его в его именины подарил ему деревянную лошадку. Павлуша уверял, что эта лошадка принадлежала Карлу XII и была та самая, на которой он ускакал из Полтавского сражения. Павлуша уверял, что в доме его родителей находится поваренок-астроном, форрейтор-историк и что птичник Прошка сочиняет стихи лучше Ломоносова. Сначала все товарищи ему верили, но скоро догадались, и никто не хотел ему верить даже тогда, когда случалось ему сказать и правду.* [http://pushkin.niv.ru/pushkin/text/articles/article-051.htm](http://pushkin.niv.ru/pushkin/text/articles/article-051.htm)

**English:**

> Pavlusha was a neat, kind, diligent boy, but had a great vice. He couldn't say three words without lying. His father gave him a wooden horse on his name day. Pavlusha claimed that this horse belonged to Charles XII and was the one on which he rode away from the battle of Poltava. Pavlusha claimed that in his parents ' house there is a kitchen-made-cum-astronomer, forreitor-cum-historian, and that the poultry farmer Proshka composes poems better than Lomonosov. At first all his comrades believed him, but they soon guessed, and no one wanted to believe him even when he happened to tell them the truth. [http://pushkin.niv.ru/pushkin/text/articles/article-051.htm](http://pushkin.niv.ru/pushkin/text/articles/article-051.htm)

**Russian:**

Официальные историки с очень умным видом любят насмехаться над всякими чудоверцами. Какие еще тарелки?! Какие еще зеленые человечки! Это все брехня! Ха-ха-ха! А сами свои официальные правдивые истории строят на сочинениях патологических лжецов, которые не могут сказать трех слов, чтоб не солгать.

**English:**

Official historians with a very smart look like to mock all sorts of miracle workers. What other plates?! What other green men! This is all bullshit! Ha-ha-ha! And they build their official true stories on the writings of pathological liars who can't say three words without lying.

Translator's note: This piece has been split into two parts during translation to English. The second part, in the original Russian, continues in [the original article](https://levhudoi.blogspot.com/2013/06/blog-post.html).
