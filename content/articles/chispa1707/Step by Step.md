title: Complete history of catastrophic crash (variant No. 3 of 1/29/2020)
date: 2020-02-01
modified: Fri 09 Jul 2021 06:14:55 BST
category:
tags: catastrophe
slug:
authors: Andrei Stepanenko
from: https://chispa1707.blogspot.com/ and https://scan1707.blogspot.com/2020/02/3-29012020.html
originally: Step by step.html
local: Step by step_files/
summary: (A long read, large file) The penultimate stage of the flooding is dated 10,000 years ago, but they usually avoid dating of the last, uppermost sea terrace. In which case, the total flooding of Beringia 8,200 years ago is an acceptable assumption. Electrical discharges moving with space velocity liquidate the most part of threat pointwise: the most dangerous meteorites blow up from the bottom side, and most of them do not reach the surface of the planet.
status: published

### Translated from:

[https://chispa1707.blogspot.com/](https://chispa1707.blogspot.com/), [https://scan1707.blogspot.com/2020/02/3-29012020.html](https://scan1707.blogspot.com/2020/02/3-29012020.html) and [Complete history of catastrophic crash (variant № 3 of 1/29/2020)](https://chispa1707.blogspot.com/2020/10/complete-history-of-catastrophic-crash.html)

Translated by Berenkova Violetta Michailovna. RoHT machine translated the final 20% or so.

RoHT Translator's note: This is a very long piece. Due to its importance, we post it without completing full review. It will be updated from time to time.

**CONSIDERING THE CATASTROPHIC CRASH, WE POINT OUT THE MECHANISMS**.

Basic materials

Stages of cometary catastrophic crashes: [http://the-small-joys.blogspot.ru/2017/03/25032017_25.html](http://the-small-joys.blogspot.ru/2017/03/25032017_25.html)

Addition material for this post concerning the stages: [http://the-small-joys.blogspot.ru/2018/02/02-2018.html](http://the-small-joys.blogspot.ru/2018/02/02-2018.html)

On behalf of everyone who contributed: [alexsoff](https://alexsoff.livejournal.com/), [ar-k-om](https://ar-k-om.livejournal.com/), [bioplant](https://bioplant.livejournal.com/), [Brusek Kodluch](https://kodluch.wordpress.com/2018/03/11/%e2%99%ab-off-topic-aleksander-von-humboldt-kosmos-tom-i/), [bgrusnak](https://bgrusnak.livejournal.com/), [bskamalov](https://bskamalov.livejournal.com/), [chispa1707](https://chispa1707.livejournal.com/), [Чумакин М. М.](https://chumakin.livejournal.com/), [cladovschik](https://cladovschik.livejournal.com/), [coralsteel](https://coralsteel.livejournal.com/), [curiousmole](https://curiousmole.livejournal.com/), [despaminator](https://despaminator.livejournal.com/), [dmitriy_ukhlinov](https://my.mail.ru/mail/dmitriy_ukhlinov/), [evan-gcrm](https://evan-gcrm.livejournal.com/), [fantastron1](https://fantastron1.livejournal.com/), [Joe Doe](https://my.mail.ru/mail/ayatola1/), [istbat](https://istbat.ru/), [It4history](http://hist.tk/ory/user:It4history), [is3](https://is3.livejournal.com/), [kiseitch](https://kiseitch.livejournal.com/), [leonid-smetanin](https://leonid-smetanin.livejournal.com/), [muha-vchocolate](https://muha-vchocolate.livejournal.com/), [mygellanov](https://mygellanov.livejournal.com/), [olegs4erbinin](https://olegs4erbinin.livejournal.com/), [paomako](https://paomako.livejournal.com/), [papa-fittih](https://papa-fittih.livejournal.com/), [plamyaognya](https://plamyaognya.livejournal.com/), [radmirkilmatov](https://radmirkilmatov.livejournal.com/), [rodom-iz-tiflis](https://rodom-iz-tiflis.livejournal.com/), [samarka-14](https://samarka-14.livejournal.com/), [sergiusjs](https://sergiusjs.livejournal.com/), [sibved](https://sibved.livejournal.com/), [Борис Бондарчик](https://nu-nu.ru/share/2/1449/b63d75a70802f765a2959cd75c9f2e58.html), [Valeri Polevoy](https://vk.com/id9603461), [Victor Davydov](https://www.facebook.com/profile.php?id=165294434382808), [Zhenya Razuvaev](https://vk.com/boatman), [Padre-Adrian Bankels](https://vk.com/id373250038), [Khupovoy Vladimir Valentinovich](https://hupovoy.livejournal.com/), Andrey Melnik, Andrey Pedko, Andrii Goretskyi, Flonzcraft, Hennadii Brezynskyi, Ilya Nikitin, JAUHIEN HRYBOUSKI, Murgab, Alexander Alexandrovich V., Alexander Viktorovich P., Alexander Vladimirovich B., Alexander Vladimirovich K., Alexander Vyacheslavovich K., Alexander Z., Alexander Ivanovich M., Alexander Igorevich Z, Alexander Sergeyevich P., Alexander Chulkov, Alexander Shakhlay, Alexey Andreyevich K., Alexey Vladimirovich U., Alexey Ivanovich G., Alexey Nikolaevich S., Alexey Olegovich S, Aleksey Sergeyevich R., Aleksey Yurievich B., Andrey Vladimirovich B., Anna Alexandrovna B., Bogdan Alexandrovich O., Vadim Vladimirovich K., Vadim Yurievich, Valery Alexandrovich K, Valery Vladimirovich H., Valery Pavlovich N., Vitaly Nikolaevich K., Vyacheslav Vadimovich M., Grigory Andreevich G., Dmitry Vladimirovich N., Dmitry Vladimirovich Sh., Evgeny Panchenko, Egor Borisovich K, Elena Anatolyevna T., Elena Evgenyevna D., Ivan Ivanovich K., Ivan Nikolayevich K., Igor Ivanovich K., Ildar Ishalin, Ildar Ramilevich H., Irina Petrovna V., Maxim Borisovich S., Marina Valeryevna G., Matvey Alexandrovich Ch., Mikhail Germanovich M., Natalia Valerievna S., Nikolay Andreevich P., Olga Evgenievna L., Olga Yurievna O., Pavel Mikhailovich K., Padalko Andrey, Ranis Nailevich I, Roman Vladimirovich M., Roman Nikolayevich G., Roman Patrikovich I., Ruslan Ravilevich Z., Rustam Vugarovich A., Svetlana Vladimirovna M., Svetlana Gennadyevna F., Sergey Vasilievich K., Sergey Viktorovich S., Sergey Ivanovich L., Sergey Mikhailovich L., Sergey Petrovich R., Sergey H., Tagir Kimovich A., Tatiana Nikolaevna T., Timur Aristanbaev, Timur Ismailovich D., Unknown (27.02.2020 01:56),

### Contents

1. [Tectogenesis - Theory](#tectogenesis_theory)

2. [Electric Discharge - Theory](#electric_discharge_theory)

3. [Electric Discharge and Tectogenesis - Synthesis](#synthesis)

Contents to be completed...

30. [Appendices](#appendices)

The concept of interdisciplinary connections is not unequivocally specified, however distinguishing of those connections has allowed uniting certain scientific theories explaining:

- Character of interaction of the Earth and large meteoric bodies;

- Displacement of an axis of the Earth rotation;

- Displacement of rotational tension in tectonosphere;

- Transgressions and regressions of the seas;

- Permafrost origin;

- Origin of some coal-fields and oil deposits;

- Death of pleistocene megafauna;

- Neolithic revolution.

### <a name='tectogenesis_theory'></a>TECTOGENESIS - THEORY

Transgressions and regressions of the seas and formation of tectonic fault lines have one general origin. First of all, here are some extracts from the textbook.

**1.1. DISPLACEMENT OF THE AXIS AND PRESSURE IN TECTONOSPHERE**

Fig. 1. The scheme of origin and a discharge of pressure in tectonosphere at the expense of change of position of the Earth rotation axis concerning its surface [^1].

<!-- Local
#![](Step%20by%20step_files/01D0A2D18FD0BFD0BAD0B8D0BDD09A.png)
-->

[![](https://1.bp.blogspot.com/-90AfPtwKQG8/X4iEmIXlvKI/AAAAAAAADik/N_NmP9k7r94_M2FuPacuI3p39TWMjXfZQCLcBGAsYHQ/s844/01%2B%25D0%25A2%25D1%258F%25D0%25BF%25D0%25BA%25D0%25B8%25D0%25BD%2B%25D0%259A.%2B%25D0%25A4.%2B%25D0%25A4%25D0%25B8%25D0%25B7%25D0%25B8%25D0%25BA%25D0%25B0%2B%25D0%2597%25D0%25B5%25D0%25BC%25D0%25BB%25D0%25B8%2B%2528%25D1%2583%25D1%2587%25D0%25B5%25D0%25B1%25D0%25BD%25D0%25B8%25D0%25BA%2529.%2B%25E2%2580%2594%2B%25D0%259A%25D0%25B8%25D0%25B5%25D0%25B2.%2B%25D0%2592%25D1%258B%25D1%2581%25D1%2588.%2B%25D1%2588%25D0%25BA.%252C%2B1998.%2B%25E2%2580%2594%2B312%2B%25D1%2581.png#clickable)](https://1.bp.blogspot.com/-90AfPtwKQG8/X4iEmIXlvKI/AAAAAAAADik/N_NmP9k7r94_M2FuPacuI3p39TWMjXfZQCLcBGAsYHQ/s844/01%2B%25D0%25A2%25D1%258F%25D0%25BF%25D0%25BA%25D0%25B8%25D0%25BD%2B%25D0%259A.%2B%25D0%25A4.%2B%25D0%25A4%25D0%25B8%25D0%25B7%25D0%25B8%25D0%25BA%25D0%25B0%2B%25D0%2597%25D0%25B5%25D0%25BC%25D0%25BB%25D0%25B8%2B%2528%25D1%2583%25D1%2587%25D0%25B5%25D0%25B1%25D0%25BD%25D0%25B8%25D0%25BA%2529.%2B%25E2%2580%2594%2B%25D0%259A%25D0%25B8%25D0%25B5%25D0%25B2.%2B%25D0%2592%25D1%258B%25D1%2581%25D1%2588.%2B%25D1%2588%25D0%25BA.%252C%2B1998.%2B%25E2%2580%2594%2B312%2B%25D1%2581.png)

**1.2. THE SOURCE OF TECTOGENESIS FORCES**

Change of position of the rotation axis in the Earth body brings the basic contribution in the field of rotational tension (|99 percentage of the necessary for starting of tectonic the Earth activation). These tensions can reach the critical values equal to the ultimate strength of tectonosphere rocks (107 pascals), i.e. it is a real source of the Earth tectogenesis forces [^2].

**1.3. DEFORMATION OF GEOID**

Change of position of the Earth body rotation axis leads to synchronous deformation of geoid, appearing, first of all, in the change of the World Ocean level: in the points of the terrestrial surface which removed nearer to the poles, the ocean level goes down; in the points which move off the poles, it raises [^3]. There is a basis to expect similar reaction of the solid floor of the ocean to the same change of the position of the Earth rotation axis. Land heights change accordingly.

**1.4. EFFECT OF SCREWING**

Transgressions and regression es of the seas depend not only on displacement of the Earth rotation axis, but also on deformations of continental plates and the ocean floor. In the latter case, transgressions and regression es in relation to the coast are unequal. A good example - transgression of the Littorina (Baltic) Sea:

- In Blekinge - 8 metres;

- In Straits of Öresund and Store Bælt - 10 metres;

- In the southwest part of the basin - 15 metres;

- Around the Gulf of Danzig - 13 metres;

- In the northern part of the Gulf of Finland - 4 metres;

- In the territory of St.-Petersburg - 7 metres [^4].

The effect is explained by different degree of forces of pressure and tension arising synchronously with displacement an axis of rotation - depending on remoteness of the sites of the crust from meridians of the maximum displacement. As a result, the sites "are screwed", and deposits on the basin coast evidence it. The coastal line of the Yoldia Sea screwed as well.

Fig. 2. Depth of the coastal line of the Yoldia Sea[^5].

<!-- Local
[![](Step by step_files/02D09FD0BED0B2D0BED0B4D0BAD0B0.png#clickable)](Step by step_files/02%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png)
-->

[![](https://1.bp.blogspot.com/-Dj-N6XgMwLE/X4iHSgjMk6I/AAAAAAAADiw/kKCH_ix-1f8QZggBZV_DpI3JG6C8AvIeACLcBGAsYHQ/s930/02%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png#clickable)](https://1.bp.blogspot.com/-Dj-N6XgMwLE/X4iHSgjMk6I/AAAAAAAADiw/kKCH_ix-1f8QZggBZV_DpI3JG6C8AvIeACLcBGAsYHQ/s930/02%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png)

**1.5. CRUST FAULTS**

Faults arise in the place of faults and replace screwing there where to fault a plate along a fault is easier, than to screw.

Fig. 3. Piqiang Fault, the People's Republic of China (Takla Makan) [^6].

<!-- Local
[![](Step by step_files/03D0A1D0B4D0B2D0B8D0B3D0BAD0BED180D18B.jpg#clickable)](Step by step_files/03%2B%25D0%25A1%25D0%25B4%25D0%25B2%25D0%25B8%25D0%25B3%2B%25D0%25BA%25D0%25BE%25D1%2580%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-wiix4l-t7YY/XlEx2zT85MI/AAAAAAAADCc/eO-w43I1blkPoZB3onn1JaLRBLODy2bLwCLcBGAsYHQ/s1600/03%2B%25D0%25A1%25D0%25B4%25D0%25B2%25D0%25B8%25D0%25B3%2B%25D0%25BA%25D0%25BE%25D1%2580%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-wiix4l-t7YY/XlEx2zT85MI/AAAAAAAADCc/eO-w43I1blkPoZB3onn1JaLRBLODy2bLwCLcBGAsYHQ/s1600/03%2B%25D0%25A1%25D0%25B4%25D0%25B2%25D0%25B8%25D0%25B3%2B%25D0%25BA%25D0%25BE%25D1%2580%25D1%258B.jpg)

**1.6. SYSTEM OF FAULTS**

As rotational tension can reach the critical values equal to ultimate stress of tectonosphere rocks (107 Pascals), there may be series of faults. The first system of faults becomes a command: in case of further movement of the axis, new tensions are inclined to discharge in the already fixed system in the form of faults. That is, the system of faults existing for today unidirectional because it reflects the initial phase of the rotation axis displacement.

Fig. 4. The scheme of the arrangement of the system of faults indicators (Ukraine) [^7].

<!-- Local
[![](Step by step_files/04D0A0D0B0D0B7D0BBD0BED0BCD18B.jpg#clickable)](Step by step_files/04%2B%25D0%25A0%25D0%25B0%25D0%25B7%25D0%25BB%25D0%25BE%25D0%25BC%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-pEw-kItrJ40/XlEx9B583RI/AAAAAAAADCg/yusyqbl1_bQTOYLjbawUFjScJyxNGpWdQCLcBGAsYHQ/s1600/04%2B%25D0%25A0%25D0%25B0%25D0%25B7%25D0%25BB%25D0%25BE%25D0%25BC%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-pEw-kItrJ40/XlEx9B583RI/AAAAAAAADCg/yusyqbl1_bQTOYLjbawUFjScJyxNGpWdQCLcBGAsYHQ/s1600/04%2B%25D0%25A0%25D0%25B0%25D0%25B7%25D0%25BB%25D0%25BE%25D0%25BC%25D1%258B.jpg)

**2.1. ORIENTATION OF FAULTS**

General orientation of the majority of lineaments in Eurasia indicates to Greenland, and some part - to the present geographical pole. As the system of faults is laid at the very beginning of the displacement of the Earth rotation axis, there were two large acts of displacement, and it is necessary to search for the previous northern geographical pole in Greenland.

Fig. 5. The scheme of lineaments in Eurasia [^8].

<!-- Local
[![](Step by step_files/05D09BD0B8D0BDD0B5D0B0D0BCD0B5D0BDD182D18B.jpg#clickable)](Step by step_files/05%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B5%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-ThGVceoi9Mc/XlEyDWCZrrI/AAAAAAAADCo/6Cu7RUH_BtowTkLwiRuTBXoUbyY_kZvzgCLcBGAsYHQ/s1600/05%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B5%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-ThGVceoi9Mc/XlEyDWCZrrI/AAAAAAAADCo/6Cu7RUH_BtowTkLwiRuTBXoUbyY_kZvzgCLcBGAsYHQ/s1600/05%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B5%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.jpg)

**2.2. THE PREVIOUS POSITION OF THE POLE**

The previous position of northern geographical pole is indicated by not only lineaments, but also by the relic zones of boreal and wood type soils. At first, here is the map of soil zones as it is.

Fig. 6. V. Dokuchayev. Soil zones of the Northern hemisphere 1899 [^9].

<!-- Local
[![](Step by step_files/06D09FD0BED187D0B2D18B1.jpg#clickable)](Step by step_files/06%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B%2B1.jpg)
-->

[![](https://1.bp.blogspot.com/-Kiem7Qv_qKg/XlEyMUj7ogI/AAAAAAAADCw/c9bEI9TylE8ZBh3UWo47bk5FMqXaNkZOQCLcBGAsYHQ/s1600/06%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B%2B1.jpg#clickable)](https://1.bp.blogspot.com/-Kiem7Qv_qKg/XlEyMUj7ogI/AAAAAAAADCw/c9bEI9TylE8ZBh3UWo47bk5FMqXaNkZOQCLcBGAsYHQ/s1600/06%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B%2B1.jpg)

Now here is a fragment of the same map with the pointed out and perfectly differentiated two zones - relic and modern - for each of two types of soils.

Fig. 7. A fragment of the map by V. Dokuchayev. Soil zones of the Northern hemisphere [^9].

<!-- Local
[![](Step by step_files/07D09FD0BED187D0B2D18B.png#clickable)](Step by step_files/07%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B.png)
-->

[![](https://1.bp.blogspot.com/-9TYF2Pt-8zA/XlEySUJ_aFI/AAAAAAAADC0/nm67t0V4ESwA-RFLncm6XzwbBHBmWxpXgCLcBGAsYHQ/s1600/07%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B.png#clickable)](https://1.bp.blogspot.com/-9TYF2Pt-8zA/XlEySUJ_aFI/AAAAAAAADC0/nm67t0V4ESwA-RFLncm6XzwbBHBmWxpXgCLcBGAsYHQ/s1600/07%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B.png)

There are no intermediate zones that indicates the fast enough displacement of northern geographical pole in the terms of geological measures. There are no the zones as well reflecting other position (forthcoming the Greenland one) of the northern geographical pole that indicates a very long steady position of the geographical pole in Greenland.

**2.3. DATING OF DISPLACEMENT OF THE EARTH ROTATION AXIS**

Now we are going to compare two maps: with the displacement of soil zones and with habitat and death of mammoths. The territories confidently coincide. It means that mammoths in the course of extinction moved to the south after the displacement of the climatic zones.

Fig. 8. Displacement of soil zones [^9] and habitat and death of mammoths [^10].

<!-- Local
[![](Step by step_files/08D09FD0BED187D0B2D18B-D09CD0B0D0BCD0BED0BDD182D18B.jpg#clickable)](Step by step_files/08%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B0%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-dWuul6ymoUI/XlEycfeLDRI/AAAAAAAADC4/0LN6adAHg_Qq3-QqkzfJ-K0Uql-7CEs3gCLcBGAsYHQ/s1600/08%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B0%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-dWuul6ymoUI/XlEycfeLDRI/AAAAAAAADC4/0LN6adAHg_Qq3-QqkzfJ-K0Uql-7CEs3gCLcBGAsYHQ/s1600/08%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B0%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg)

As a species, mammoths prospered here for 1.5 million years, and chronological frameworks of extinction of mammoths: 14-10 thousand years ago. It is the ending of stability and the beginning of the epoch of the axis displacement, and climatic and soil zones at the same time. Almost synchronously with the extinction start, 15 thousand years ago transgressions and regressions of the forming Baltic, Caspian and Black Seas began - in strict conformity with the citations from the textbook presented above.

**2.4. PEAK OF CHANGES**

Taking into consideration that the displacement of the Earth rotation axis is accompanied by the synchronous deformation of geoid, so, both synchronous transgressions and regression es of basins, it is necessary to pay attention to the period of about 8200 years ago. Neither in pleistocene, nor in holocene there was no other period with such high density of dramatic tectonic events.

- 8000 years ago - ocean regression for 14 metres

- 8000 years ago - the New Caspian transgression

- 8000 years ago - the Black Sea flood

- 8200 years ago - Storegga Slide

- 8200 years ago - flooding of Doggerland 

- 8200 years ago - Lake Agassiz fault

- 8200 years ago - stop of Gulf Stream

- 8200 years ago - displacement of the floating ice border to the south

- 8200 years ago - transgression of the world ocean

- 8200 years ago - flooding Sunderland (Indonesia-Indochina)

- 8200 years ago - flooding of Sahulend (Australia, New Guinea)

- 8250 years ago - transgression on the Aleutian Islands

- 8300 years ago - oceanic species of fauna in the Baltic Sea

- 8500 years ago - the Littorina Sea

- 10000 years ago - flooding of Beringia, the penultimate stage

The flooding of Beringia is different: the penultimate stage of the flooding is dated 10 thousand years ago, but they usually avoid dating of the last, uppermost sea terrace. In such situation, the total flooding of Beringia 8200 years ago is an acceptable assumption.

After the point 8200 years ago, the following fluctuations of the world ocean level are: transgression - 7200-6500 years ago, regression - 6500-5000 years ago, transgression - 5000-4500, regression - around  4500, transgression - 4500-4000, regression - 4000-3800, transgression - 3800-3700, regression - 3700-2500, transgression - 2500-1500 years ago, regression - 1500 years ago till nowadays [^11]. That is, it is possible to consider the period of 15-14 thousand years ago as the incident beginning as a whole, the date 8200 years ago -- the peak of the displacement of the Earth rotation axis, and the period after the point 8200 years ago - the period of stabilization of the planet.

**2.5. DISPLACEMENT OF SOIL ZONES AND PERMAFROST**

Permafrost is the most obvious natural phenomenon organically connected with the death of the pleistocene megafauna, thus, with displacement of soil zones. Moreover, the frozen subsoil territory has the form of a drop with the geometrical centre lying on the same straight line, as the vector of the displacement of the Earth rotation axis. If the geometrical centre of the frozen subsoil and the former geographical pole had pulled in its side, the present pole would have been the correct compromise.

Fig. 9. Displacement of soil zones [^9] and permafrost [^12].

<!-- Local
[![](Step by step_files/09D09FD0BED187D0B2D18B-D09CD0B5D180D0B7D0BBD0BED182D0B0.jpg#clickable)](Step by step_files/09%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B5%25D1%2580%25D0%25B7%25D0%25BB%25D0%25BE%25D1%2582%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-IA_eyf1tuuY/XlEylsOUBhI/AAAAAAAADDE/EQWbRxjDuEQuPTwpbyQsU4Aqlb6VTcHowCLcBGAsYHQ/s1600/09%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B5%25D1%2580%25D0%25B7%25D0%25BB%25D0%25BE%25D1%2582%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-IA_eyf1tuuY/XlEylsOUBhI/AAAAAAAADDE/EQWbRxjDuEQuPTwpbyQsU4Aqlb6VTcHowCLcBGAsYHQ/s1600/09%2B%25D0%259F%25D0%25BE%25D1%2587%25D0%25B2%25D1%258B-%25D0%259C%25D0%25B5%25D1%2580%25D0%25B7%25D0%25BB%25D0%25BE%25D1%2582%25D0%25B0.jpg)

**2.6. PERMAFROST AND GLACIATION**

Permafrost and glaciation connection - long decrease in average temperatures when the ground continues to freeze deep through, and thaws only in the surface -- is also admitted. Research of an isotope composition of ice has shown that 18-20 thousand years ago average temperatures of January in the north of Yakutia were 25°С, and in the central Yakutia, 15-20°С lower, than today [^13]. It equalises the climate of Yakutia of the epoch of mammoths flowering and the present Antarctic climate. The frozen subsoil can remain, the 2.7 km thick ice layer in the area of Taimyr can remain and accrue in the thickness, but forage reserve for the pleistocene megafauna could hardly remain.

There is also an addition to this contradiction. William Patterson from the Canadian university of Saskatchewan has studied patterns of deposits with the step of 0.5 mm from Lake Lough Monreagh in Ireland. The isotope analysis has shown that the glaciation occurred during half-a-year period, and thus all living, including plants, died [^14]. It was not a long cooling, it was biosphere catastrophic crash, and it was momentary, in evolutionary term.

Both researches are unconditionally correct: these characteristic for low temperatures isotopes are present in the ice and in deposits, contemporary to the pleistocene megafauna; there is lack of interdisciplinary logic connections explaining the paradox.

Moreover, it is important to remember here that all great glaciations coincided with the largest rock-building epochs when the relief of the terrestrial surface was the most contrast, and the area of the seas decreased [^15]. Attempts to match this observation to the present (the average height of mountains and contrast of the relief has become even more considerable) have failed. The conclusion: the mountains are not the point, but the process of rock-building should be taken into consideration, connected first of all with the change of the Earth rotation axis.

There is also certain connection of glaciation with the activity of volcanoes, here again there is also lack of interdisciplinary logic connections. In paleogene volcanoes were active, and there were almost no glaciers. The reason: the activity of volcanoes and glaciation process should coincide, first of all, during the displacement of the Earth rotation axis.

The founding fathers of the glaciation theory Louis Agassiz and James Kroll directly bound coming of the Ice Age to the change of the Earth rotation axis, but for the lack of interdisciplinary logic connections they predictably slipped to obviously secondary climatic factors - less light and long cooling.

**2.7. GLACIATION AND METEORITES**

The basic message of idea to connect meteorites and glaciation is clear: too high speed of glaciation. Even those 10 years that are given for glaciation at present, are not enough for creation with the help of climatic factors of essentially different geological epoch - with moraines, rock-building and glaciers with thickness of up to 4 km. Meteorites are able not only to eclipse the Sun with explosions dust, but also possess energy, sufficient for creating of rather radical changes in the landscape of all planet. There is only one problem: there is no any model; the blow of the necessary force can destroy the planet, and cannot create a clear picture of complex geological evolution.

**2.8. SUMMARIZE OBVIOUS FACTS**

1. The Earth rotation axis passed earlier through the centre of Greenland, and that position remained invariable, at least, from the moment of occurrence of mammoths, that is, 1.5 million years running. It evidences extreme stability of the planet and the unique character of the axis displacement.

2. Displacement of the Earth rotation axis, appearance of the system of faults, screwing of the planet surface and sea-bed areas, transgression and regression of the seas and oceans have one nature and one not yet indicated cause.

3. The unindicated original cause of the coming tectonic events is dated as the beginning of the death of mammoths and the beginning of the end of the Ice Age14-15 thousand years ago.

4. The existing tectonic fault lines is mostly laid at the initial stage of the Earth rotation axis displacement, therefore lineaments are directed mainly to the centre of Greenland.

5. The peak of tectonic motions was 8200 years ago and so it is necessary to date lineaments, directed towards Greenland.

6. Ten transgressions and regressions after that 8200 years ago time point should be ascribed to the stage of stabilization of the planet condition.

7. The Earth rotation axis was displaced approximately for 17 ° in the direction of longitude from 40 to 140.

8. Absence of intermediate soil zones indicates on extremely fast by geological measures displacement of the Earth rotation axis to the present position.

9. Last large tectonic motions occurred in the position of the poles very close the modern one, what the lineaments evidence with their direction at the present pole.

10. Permafrost formation is connected, first of all, with the displacement of the Earth rotation axis, rapid rock-building and volcanic activity.

11. Permafrost formation is connected with the end of the Ice Age, even if it seems paradoxical.

12. The end of the Ice Age is directly connected with extinction of the pleistocene fauna; that is the official position of modern science.

13. Climate fluctuations, change of climatic zones, movements of monsoons and trade winds, warm and cold currents, global desertification and bogging are the consequence of the Earth rotation axis displacement and accompanying them tectonic processes, but they are not their cause.

**2.9. SAFE EXTENSION**

Sometimes extension of the planet is pointed as the reason of global catastrophic crashes, the process not recognised by the science [^16]. However variations of angular speed bring in the field of rotational tension extremely small contribution - (|1 %), that is, the extension of the planet basing on tectogenesis theory is safe.

**2.10. SECURE THAWING OF GLACIERS**

Hypotheses of influence of glaciers' increase and thawing on characteristics of the planet rotation regular arise [^17], however, it can lead only to precession change and to variations of angular velocity, that is, changes in the ocean levels are hardly possible. Acknowledgement is as follows: 1.5 million years of mammoths' prosperity in the same climatic zone, despite of all described glaciation and warming processes.

### <a name='electric_discharge_theory'></a>THE ELECTRIC DISCHARGE. THE THEORY

**3.1. NUANCES OF THE ORIGINAL CAUSE MISSING**

Displacement of the Earth rotation axis can be caused only by change of distribution of masses in the planet bowels. Here is a good citation: "Cumulative action of gravitational and adhesive forces of the planet substance determining its durability and described above exogenous processes, as a result, the surface of the Earth should form regular ellipsoid of revolution - an ideal figure of a rotating body balance. What does actually occur? We see considerable deviations from this form, reaching values of an about ±10 km» [^18].

It means that in bowels there forces creating not ideal form of geoid, and if the surface has not formed ideal ellipsoid outside, the cause of it is in bowels.

Thus, bowels are obviously inclined to stability: 1.wzz5 million years mammoths prospered in one invariable climatic zone. The rotation axis moved for 17 °, during 6 thousand years maximum - scanty term, geologically speaking, and the latest cooling occurred just in half a year. It is a catastrophic crash, there must be some source influencing the bowels. There is source.

**3.2. THE PUBLICATION BY A. P. NEVSKIY**

In 1978 *The Astronomical Bulletin (volume 12, No. 4)* published an article by A. P. Nevskiy: *The phenomenon of the positive stabilized electric charge and effect of electric discharge explosion of large meteoric bodies in flight in the atmospheres of planets* [^19]. For public the essence of the article was rewritten, without formulas, in the magazine *Technics - for young people* 1987-12, in the magazine *The Earth and the Universe* 1990-3, and in A. Voitsekhovsky's interview [^20].

* Nevskiy Alexander Platonovich (1935-2005), Cand. Sc. (Physics and Mathematics), the leading research assistant, TsNIIMash, Russian Aerospace Agency

**3.3. EFFECT OF ELECTRIC DISCHARGE EXPLOSION ACCORDING TO A. P. NEVSKIY **

An electric dipole is formed in a comet with the concentrated positive charge on the surface and scattered negative charge in a plasma tail. The positive charge is stabilized and reaches considerable value. In case of close passing, there appears huge potential difference between the comet and the Earth, which can lead to air layer breakdown. Under standard conditions, the super-power multichannel discharge begins already from the height of 25 kilometres.

Prior breakdown electric fields cause electrostatic levitation, which explains the phenomenon of elevating of large subjects in the air observed in practice (trees, yurtas, soil sites on hills). There is extremely strong light flash, and there where the soil is damp, corona discharge appears.

There are hundreds of thousands of breakdown channels, they result in rigid x-ray radiation, and neutron radiation as the result of reaction of deuterium nuclear synthesis.

Electric discharge high-rise explosion leads to destruction of the meteorite with transformation of its part into vaporous and dust conditions. The meteorite blows up from the bottom side [21; 22], therefore it or its large splinters receive a powerful impulse to the direction from the Earth surface.

There are three shock waves. The first shock wave is product of energy release in a lightning column. The second - explosive destruction of the meteoric body. The third - a ballistic wave resulting from supersonic intrusion of the meteoric body into the atmosphere.

Spreading of currents along water-bearing layers heats water up to formation of boiling reservoirs and huge geysers.

**3.4. CONFIRMATION BY PRACTICE OF SPACE FLIGHTS**

Theory by A.P. Nevskiy appeared as a result of struggle against plasma shielding of radio-waves of going down spacecrafts (SC), thus it is based on extremely specific practice. Interaction of the Earth and SC really goes under the scheme described in the theory, up to explosions [^23].

Fig. 10. Composition of technogenic objects in low-Earth orbits[^23]

<!-- Local
[![](Step by step_files/10D092D0B7D180D18BD0B2.png#clickable)](Step by step_files/10%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png)
-->

[![](https://1.bp.blogspot.com/-kfv4Vsk6--c/X4iNPptWuYI/AAAAAAAADi8/Fq5T2aVBErI59RXut8QyH-jNimp-stJjwCLcBGAsYHQ/s776/10%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png#clickable)](https://1.bp.blogspot.com/-kfv4Vsk6--c/X4iNPptWuYI/AAAAAAAADi8/Fq5T2aVBErI59RXut8QyH-jNimp-stJjwCLcBGAsYHQ/s776/10%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png)

Sometimes SC are blown up deliberately (India, 2019), but other explosions are admitted as "casual" ones, but, as a whole, fragments of the spacecrafts destroyed by explosions are threat № 1 for all those flying in space.

Fig. 11. The project of dangerous objects removal from near-Earth orbits [^23]

<!-- Local
[![](Step by step_files/11D092D0B7D180D18BD0B2.png#clickable)](Step by step_files/11%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png)
-->

[![](https://1.bp.blogspot.com/-gHRj-Hlu80I/X4iN4zexd5I/AAAAAAAADjE/CJryHf5eHCkRfiWPcD03McMtG2EQ0zdWQCLcBGAsYHQ/s1150/11%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png#clickable)](https://1.bp.blogspot.com/-gHRj-Hlu80I/X4iN4zexd5I/AAAAAAAADjE/CJryHf5eHCkRfiWPcD03McMtG2EQ0zdWQCLcBGAsYHQ/s1150/11%2B%25D0%2592%25D0%25B7%25D1%2580%25D1%258B%25D0%25B2.png)

**3.5. CRITICISM OF THE A. P.NEVSKIY'S THEORY **

Basically, nobody criticizes the theory due to its good validity. Critics reject [24; 25; 26] only formula № 22 (V = dV*R/D), which A. P. Nevskiy used for calculation of difference of potentials between the meteoroid and the Earth surface. Because of space velocity of the meteoroid, this difference is of space scale too.

It does not suit the opponents replacing the high-speed object in A. P.Nevsky's model by a static condenser with almost zero energy or a Christmas-tree decoration wrapped up with foil and successfully proving that this model is incorrect.

**3.6. UNKNOWN FACTS ABOUT METEOROIDS**

The problem with the formula № 22 could be solved, if we knew self positive charge (before entering into the atmosphere) of the meteoroid, but such data are absent.

Diameter of the visible part of the comet coma can reach millions of kilometres. This coma is strictly spherical and is not rumpled under the pressure of the solar wind that indicates that the solid body of the comet has its own centrosymmetrical positive electric field [^27], strong enough to resist to the solar wind and to keep the spherical form of the coma of the enormous sizes.

Meteoroids of the comets differ only in their size and quantity: for comets the usual indicated weight is from 0.1 to 1 million tons, and for the shower of Perseids - 10-100 thousand times more [^28]. Obviously the positive charge of the bodies of such shower does not become less because of that the coma and tails of single meteors are not visible via a telescope.

It is difficult to estimate the charge of a meteoric shower, for example, according to its weight: for multielectronic atom, there are concepts of the second, third and etc. ionization potentials. For argon energy of consecutive ionization I1 and I7 differs 8 times, and for sulphur - in 27 times [^29]. So, if the Tungus meteorite made the water of underground layers boil and break upward as geysers, the question is - what the shower of several millions of such bodies could do?

**3.7. SCIENTIFIC CONFIRMATION**

In 2017, two news appeared in press [140, 141] about the discovery made by the Russian science: lightnings really appear between meteoroids and planets with extremely serious consequences - up to kimberlite pipes formation. The theory by A.P. Nevskiy is fully confirmed.

Let's match the consequences of the electric discharge and tectogenesis as result.

### <name='synthesis'></a>THE ELECTRIC DISCHARGE AND TECTOGENESIS. SYNTHESIS

**4.1. THE KEY METEORIC FACTOR**

The electric discharge makes the Earth a cathode, and the meteoric shower passing around -- an anode, therefore lightnings in such contact stike from below upwards [^20]. Judging by the behaviour of the lightnings striking upwards [^21], atmosphere is the first to react to threat, then the underlaying layer (oceans and the crust), and only then the planet entirely. The Earth-cathode loses electrons and warm in large quantities, and meteoric bodies acquire electrons and warm, that leads to their explosive destruction .

**4.2. SELECTIVITY OF THE ELECTRIC DISCHARGE**

The electric discharge chooses the shortest ways - to the greatest charge and with the least resistance of the environment. For this reason, the lightnings from below first choose the most charged space objects. On the other hand, conductivity of bowels depends on the character of faults, and composition of the mineralized waters layers, therefore the discharge moves along the most vulnerable due to tectonic consequences zones of the Earth.

**4.3. DEFENSIVE SYSTEM OF THE PLANET**

Electrical discharges moving with space velocity liquidate the most part of the threat point-wise. At that, the most dangerous meteorites disintegrate starting from the bottom side and most of them do not reach the surface of the planet.

**4.4. THE MOST EVIDENT CONSEQUENCE OF THE ELECTRIC DISCHARGE**

When the Earth-cathode gives its electrons, elements in oceans and bowels are ionized, and this process is endothermic [^30], that is, goes with heat removal. The acute shortage of electrons starts the whole series of geochemical processes, and almost all these processes are endothermic as well. The most evident consequence of the electric discharge is permafrost. Ionization reaction goes with almost momentary removal of heat in all bulk of rocks, especially in the electric discharge zone. Thus, the glaciation of mammoths proceeding faster and more effectively, than in freezer chests of meat processing and packing factories, finds its simple explanation.

**4.5. CHILLING OF THE ATMOSPHERE AND OCEANS**

The strongest ionization happens with the second after oxygen prevalent element on the earth - hydrogen. The atmosphere and ocean are cooled at once, without long climatic factors that explains paradox of fast glaciation [^14]. There appear the isotopes indicating extremely low temperature [^13], impossible for these climatic zones.

**5.1. THE CONDITION OF DISPLACEMENT OF THE EARTH ROTATION AXIS**

After the electric discharge, it is necessary to expect high concentration of opposite charged ions on the different sides of the geoid with an axis passing through the geometrical centre of the electric influence on the Arctic regions. It is approximately 34 ° from the old pole in the centre of Greenland or 17 ° from the new pole towards longitude 140. That is, electric polarization of the geoid has been compulsorily displaced, and between these new electric poles, there were new, differently directed interacting forces and the plane of electric balance displaced concerning the equatorial plane. New geochemical processes, focal change of magma density will begin; therefore the bowels bulk, and together with them and the Earth rotation axis, will start to displace.

Fig. 12. New electric balance of the planet bowels

<!-- Local
[![](Step by step_files/12D091D0B0D0BBD0B0D0BDD181.png#clickable)](Step by step_files/12%2B%25D0%2591%25D0%25B0%25D0%25BB%25D0%25B0%25D0%25BD%25D1%2581.png)
-->

[![](https://1.bp.blogspot.com/-EenCzroqRZA/X47W4FoYHoI/AAAAAAAADjY/GxnEuMgLwcIhhQC82wuYpYc7qdpiTZVzgCLcBGAsYHQ/s844/12%2B%25D0%2591%25D0%25B0%25D0%25BB%25D0%25B0%25D0%25BD%25D1%2581.png#clickable)](https://1.bp.blogspot.com/-EenCzroqRZA/X47W4FoYHoI/AAAAAAAADjY/GxnEuMgLwcIhhQC82wuYpYc7qdpiTZVzgCLcBGAsYHQ/s844/12%2B%25D0%2591%25D0%25B0%25D0%25BB%25D0%25B0%25D0%25BD%25D1%2581.png)

**5.2. DIGRESSION OF THE MAGNETIC FIELD OF THE EARTH**

The situation of the electric disbalance of the planet as the reason of inversion of the magnetic field looks ideal. However, even for the short-term digression of the magnetic field of the Earth going on by different estimations from 4 [^31] till 250 years, it is required to change the rotation direction - either of the Earth or theoretical "geodynamo" in its core.

Without the application of additional energy only Dzhanibekov's effect of is able to change the direction of the planet rotation: after overturn, the Sun ascends in the west and moves in the opposite direction absolutely organically. As a result, orientation of the magnetic field of the Earth concerning the pole changes, and concerning the Solar system remains to the same.

**5.3. DZHANIBEKOV'S EFFECT AND THE EARTH**

Dzhanibekov's effect is absolutely safe to apply it to the Earth owing to dynamic balance: the carriage of a roller coaster turns over too, but nobody drops out. Formula Rossa roller coaster have speed of 241 km/hour. A person on the equator moves round the axis 7 times faster, and the Earth flies forward with the speed of 107 thousand km per hour that is 445 times faster than the carriage. The Earth will turn over, as a single whole, and the gapers will not even spill their coffee out of a paper cup.

The change of familiar parts of the world is safe too. If the carriage also rotates, in the top part of a circular way the Sun will rise for the passenger on one side, and in the bottom -- on the other. Change of the parts of the world in the process of overturn of the Earth according to Dzhanibekov is not the conflict of forces, but their mechanic continuation.

There were four dated annalistic evidences identifying such overturn (suspension of the visible Sun movement) - in 1123, 1391, 1547 and 1683. The Direct consequence is earthquakes, severe frosts and drying up of the rivers during the following and the next two years in all cases. The tsunamis did not happen, there was only one inundation. Planet overturn is safe in itself and in the proximate consequences. The difficult place is obligatory change of the flows direction, but they seem to pass without great problems: fleet in those key three years did not sink, and overturn cities were not flooded.

**5.4. CONDITIONS FOR OVERTURN **

Wikipedia about Dzhanibekov's effect writes the following: the theorem of the tennis racket is ... a consequence of laws of the classical mechanics describing movement of a firm body with three various main inertia moments [^32].

**WHAT IS "INERTIA MOMENT"**

The inertia moment is ... an inertia measure in a rotary motion round an axis. Each body has three moments of inertia, and each of them is related to the main central axis of inertia. Each of these three axes passes through the center of mass and coincides with the possible axis of rotation.

In figure below relative to axis AA, cylinder "C" has the least moment of inertia, and cylinder "B" - the greatest.

Fig. 13. The least and greatest moments of inertia

<!-- Local
[![](Step by step_files/13D09CD0BED0BCD0B5D0BDD182D18B1.png#clickable)](Step by step_files/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B%2B1.png)
-->

[![](https://1.bp.blogspot.com/-h2EGBdyN8rw/X80fQigVDSI/AAAAAAAADo4/HonVVqKs-rEafF5ENn_DwU__3mt0YfVMgCLcBGAsYHQ/s960/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B%2B1.png#clickable)](https://1.bp.blogspot.com/-h2EGBdyN8rw/X80fQigVDSI/AAAAAAAADo4/HonVVqKs-rEafF5ENn_DwU__3mt0YfVMgCLcBGAsYHQ/s960/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B%2B1.png)

In fig. 13.2. the body with three moments of inertia is shown. Relative to the main central axis of inertia "A" the inertia moment has the least value; relative to "B" axis -- intermediate value, and relative to "C" axis - the least. Rotation of the body round "B" axis will be unstable.

Fig. 14. The main central axes of inertia

<!-- Local
[![](Step by step_files/D09ED181D0B82.png#clickable)](Step by step_files/%25D0%259E%25D1%2581%25D0%25B8%2B2.jpg)
-->

[![](https://1.bp.blogspot.com/-AruyhgVPrNY/X5rpoA6_I5I/AAAAAAAADlA/_fysVCmGifwWceWKy1PURQRloQ8xePLQgCLcBGAsYHQ/s835/%25D0%259E%25D1%2581%25D0%25B8%2B2.jpg#clickable)](https://1.bp.blogspot.com/-AruyhgVPrNY/X5rpoA6_I5I/AAAAAAAADlA/_fysVCmGifwWceWKy1PURQRloQ8xePLQgCLcBGAsYHQ/s835/%25D0%259E%25D1%2581%25D0%25B8%2B2.jpg)

The Earth is a little flattened from the poles rotates round the axis with the greatest moment of inertia as well. Two axes with the least moment of inertia which are perpendicular to it lie in the equator plane and are equal to each other that makes rotation steady.

**APPLICATION OF DZHANIBEKOV'S EFFECT TO THE PLANET**

For realization of Dzhanibekov's effect, three main axes and rotation of the Earth round its axis with the intermediate (not the greatest and not the least) moment of inertia [^32] are required. The condition is executable in one, and only one, exclusively rare case: as the result of displacement of the planet's center of mass.

Emergence of two displaced maxima of bowels ionization relative to poles for 34° also changed local magma density and behaviour that displaced the center of mass as well. When the center of the planet mass changed, the direction of all three main central axes of inertia also exchanged. Two axes passing through the equator lost the accordant position, and their moments of inertia definitely ceased to be equal that, at least, led to instability of rotation.

As geoid has a sphere-like form (deviation of 15 km in height or 0.2 % from the planet radius), the difference between three moments of inertia is insignificant, and the situation in which the axis of inertia passing through the geographical poles, acquires intermediate value of the moment of inertia, becomes real. The fact of displacement of soil zones for 17 ° specifies that such geological changes certainly occurred in the past.

Hardly the Earth rotation axis acquires intermediate value, the main condition for planet overturn according to Dzhanibekov is satisfied.

**PLANET REHABILITATION**

There are two unresolved questions:

- whether the intermediate moment of inertia remained relative to the Earth rotation axis during all time of overturns or it periodically accepted the greatest values;

- how quickly and coinciding with overturns and values of the moments of inertia the axis of rotation of a planet was displaced.

However, there is also a reliable constant.

The end of overturns is definitely connected with displacement of the Earth rotation axis for 17 ° - to its present position where the planet appropriately rotates round its axis with the greatest moment of inertia, and two least moments of inertia are equal again.

Fig. 15. Displacement of the Earth rotation axis as summary result of overturns according to Dzhanibekov

<!-- Local
[![](Step by step_files/13D09CD0BED0BCD0B5D0BDD182D18B.png#clickable)](Step by step_files/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.png)
-->

[![](https://1.bp.blogspot.com/-KX_orEmLEFs/X47XC5yOgxI/AAAAAAAADjc/L0kJfQCJHP0GcZ0cLuJfPbaZ5SQP1PKuwCLcBGAsYHQ/s844/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.png#clickable)](https://1.bp.blogspot.com/-KX_orEmLEFs/X47XC5yOgxI/AAAAAAAADjc/L0kJfQCJHP0GcZ0cLuJfPbaZ5SQP1PKuwCLcBGAsYHQ/s844/13%2B%25D0%259C%25D0%25BE%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D1%258B.png)

**THE BASIC CONNECTED EVENTS**

There were three closely connected basic events. Three dramatic interdependent events proceed within the limits of a uniform process and without the application of additional energy:

- Displacement of the Earth rotation axis;

- Overturn of the Earth according to Dzhanibekov;

- Digression of the magnetic field of the Earth.

The main difference of the planet from the screw nut is as follows: masses in it do not replace, that is why the third main axis, and thirst to turn over remain in the screw nut eternally. For the Earth, returning to stability is a matter of time, geologically speaking, absolutely small time.

**5.5. OPINION OF GEOLOGISTS OF THE XIX CENTURY**

Let's have a look at the citation from the book of Karl-Friedrich Mohr"History of the Earth. Geology on new bases" 1868 "Исторiя Земли. Геологiя на новыхъ основанiяхъ" 1868 г.

These great, pronounced and diverse changes in the nature of substances consistently depositing from water, are considered by geologists, as consequences of those events which they name earth overturns. Though it is hard to tell about overturns in details, but nevertheless their existence is not subject to doubt (!!).

Fig. 16. Karl-Friedrich Mohr "History of the Earth. Geology on new bases" 1868

<!-- Local
[![](Step by step_files/14D0BFD0B5D180D0B5D0B2D0BED180D0BED182D18B.jpg#clickable)](Step by step_files/14%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-QRkr7RcjmGM/X47XW42C_XI/AAAAAAAADjo/XS2PfvFbId8wjn9SLqltYmit20VXYXzCACLcBGAsYHQ/s768/14%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-QRkr7RcjmGM/X47XW42C_XI/AAAAAAAADjo/XS2PfvFbId8wjn9SLqltYmit20VXYXzCACLcBGAsYHQ/s768/14%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg)

One more citation.

Now geology belongs to the exact sciences. The number of works, which it consists of, can't be measured; the collected facts are as numerous, as well-researched, and some of general results which are obtained from them, deserve our attention in rather high degree for they offer explanations (!) of the initial condition of the Earth and terrible (!) overturns which it experienced from time to time.

Fig. 17. Karl-Friedrich Mohr "History of the Earth. Geology on new bases" 1868

<!-- Local
[![](Step by step_files/15D0BFD0B5D180D0B5D0B2D0BED180D0BED182D18B.jpg#clickable)](Step by step_files/15%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-ZgWznYKJj9Q/X47XeJXg5fI/AAAAAAAADjs/Y4XiuEmxN8wQopArl4P6pn2RqGWcR3uWACLcBGAsYHQ/s768/15%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-ZgWznYKJj9Q/X47XeJXg5fI/AAAAAAAADjs/Y4XiuEmxN8wQopArl4P6pn2RqGWcR3uWACLcBGAsYHQ/s768/15%2B%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582%25D1%258B.jpg)

The reason of confidence of the geologists of the XIX century -- existence of the repeating scenario.

**6.1. PARADOX OF THE SCENARIO REPEATING**

The main paradox that the pleistocene catastrophic crash scenario, absolutely unique in its nature, was rather fully reflected in the antique and medieval history. It is possible to explain the situation, for example, like that: the meteoric shower, which started pleistocene catastrophic crash, came back not once, and the entire situation repeated again. Owing to natural dispersion of the flow, the force of the electric discharge was less every new meeting; however, set of catastrophic events, basically, remained the same, as annals describe. Judging by lineaments, directed towards the present pole, the last insignificant displacement of the axis, and so repeating of all complex of events, could occur relatively recently.

**6.2. THE CATASTROPHIC CRASH SCHEDULE**

Stage 1: meteoric incident according to A. P. Nevsky's scheme

Stage 2: displacement of the Earth rotation axis according to K.F. Tyapkin's scheme, together with the Earth overturn according to V.A. Dzhanibekov's effect

Stage 3: normalization of processes in a new stable condition

Let's consider each stage in the chronological sequence. When analogues are fixed in the history, we present evidences of eyewitnesses.

**[METEORIC INCIDENT. THE BAISC FACTS]**

**7.1. THE SOURCE OF THE METEORIC SHOWER - THE PHAETON**

The source of the catastrophic meteoric shower is Solar system. The science confidently produces meteoroid swarms from comets for a long time [^33], and comets have strictly local character, and J. Oort's hypothesis is most developed for today (till 1950), according to which comets are parts of the lost planet Olbersa (Phaeton) [^34]. There are three serious arguments for this hypothesis.

**7.2. PALLASIT - METEORIC MINERAL FROM THE PLANET MANTLE**

Pallasit scattered on the surface of the Earth [^35] is of meteoric origin, however, besides iron, it contains olivine, typical for mantle rocks of the planet [^36].

Fig. 18. Pallasit

<!-- Local
[![](Step by step_files/16D09FD0B0D0BBD0BBD0B0D181D0B8D182.jpg#clickable)](Step by step_files/16%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581%25D0%25B8%25D1%2582.jpg)
-->

[![](https://1.bp.blogspot.com/-Re1OSjxflu4/X5GQwR_w-FI/AAAAAAAADkA/UevKx4YQwu8DyjlCvM3E_poWF8m6UMHlQCLcBGAsYHQ/s1163/16%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581%25D0%25B8%25D1%2582.jpg#clickable)](https://1.bp.blogspot.com/-Re1OSjxflu4/X5GQwR_w-FI/AAAAAAAADkA/UevKx4YQwu8DyjlCvM3E_poWF8m6UMHlQCLcBGAsYHQ/s1163/16%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581%25D0%25B8%25D1%2582.jpg)

**7.3. GOLD - THE ELEMENT FROM THE PLANET CORE**

Elements harder than iron were formed at early stages of star evolution as a result of destruction of the neutron stars, therefore almost all gold of the Earth is in the core [^37]. Thus, the appreciable part of that gold that is present in earth crust and mantle is delivered to the Earth by asteroids, and the other source of this asteroid gold, except destruction of cores of big or minor planets, has not been stated yet.

I will present fragments of two maps - gold mining zones in Russia [^38] and zones of glaciation [^39], and we have already seen the connection of zones of glaciation, permafrost and the electric discharge from the supposed meteoric incident.

Fig. 19. Gold mining zones in Russia and zones of glaciation

<!-- Local
[![](Step by step_files/17D097D0BED0BBD0BED182D0BED0B8D0BBD0B5D0B4.png#clickable)](Step by step_files/17%2B%25D0%2597%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BE%2B%25D0%25B8%2B%25D0%25BB%25D0%25B5%25D0%25B4.png)
-->

[![](https://1.bp.blogspot.com/-MMbUwQi5Drs/X5GQ8Q0A5cI/AAAAAAAADkE/2SPcKcxYZeEeBMocjcIF8CPAusRAtalOQCLcBGAsYHQ/s1300/17%2B%25D0%2597%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BE%2B%25D0%25B8%2B%25D0%25BB%25D0%25B5%25D0%25B4.png#clickable)](https://1.bp.blogspot.com/-MMbUwQi5Drs/X5GQ8Q0A5cI/AAAAAAAADkE/2SPcKcxYZeEeBMocjcIF8CPAusRAtalOQCLcBGAsYHQ/s1300/17%2B%25D0%2597%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BE%2B%25D0%25B8%2B%25D0%25BB%25D0%25B5%25D0%25B4.png)

Humboldt, being in Russia, noticed that the gold nuggets found behind the Ural Mountains region often had a rotation figure. Let's read some citations from Humboldt's work.

... the same remainders of bones of antediluvian animals (so well-known in the low land countries of Kama and Irtysh), mixed on a chain ridge, in the high plains between Berezovsk and Ekaterinburg, with alluvial soil, rich in gold, diamonds and platinum.

The broken up layers containing gold, platinum, copper and cinnabar, are mixed on the ridges of the Ural Mountains with the same fossil bones of huge terrestrial animals of the primitive world, which are in low lands of Siberia, on the Irtysh and Tobol banks.

Fig. 20. Scans of Humboldt's book about is travel to Asia

<!-- Local
[![](Step by step_files/18D09DD0B0D0BDD0BED181D18B.png#clickable)](Step by step_files/18%2B%25D0%259D%25D0%25B0%25D0%25BD%25D0%25BE%25D1%2581%25D1%258B.png)
-->

[![](https://1.bp.blogspot.com/-7rPfzlsdrd4/X5GRMIZmT6I/AAAAAAAADkQ/GxaJpxPANRIph3tK8QRJKfUOkKz4aO2tQCLcBGAsYHQ/s1100/18%2B%25D0%259D%25D0%25B0%25D0%25BD%25D0%25BE%25D1%2581%25D1%258B.png#clickable)](https://1.bp.blogspot.com/-7rPfzlsdrd4/X5GRMIZmT6I/AAAAAAAADkQ/GxaJpxPANRIph3tK8QRJKfUOkKz4aO2tQCLcBGAsYHQ/s1100/18%2B%25D0%259D%25D0%25B0%25D0%25BD%25D0%25BE%25D1%2581%25D1%258B.png)

That is, the geological layer contains also material remainders of fauna destruction, and alluvial gold-bearing rocks. Add to it the data about the found recently meteoric particles which were stuck in tusks, and the picture becomes clear.

To challenge the hypothesis about Phaethon origin of the meteoric shower which moved the Earth rotation axis in pleistocene, it is necessary to prove the origin of gold beyond planet cores and olivineа beyond mantles.

**7.4. THE PHAETON AND THE NEAREST PLANETS**

In the line "Venus-Earth-Mars" the Mars is followed by the hypothetical Phaeton, our close the neighbour. Evidences proved that besides the suffered Earth, both Venus and Mars regularly were involved as well. In 1855 Venus was visible in the afternoon with open years, that is, possessed not only reflected, but also its own strong glow [^40]. Probably, space garbage burnt in the Venus atmosphere. In myths, Venus is closely connected with catastrophic crashes, in particular, with cooling [^41]. The Mars as constantly changed its characteristics, and the matter is not in existence of channels on the Mars, but in considerable and suspiciously one-time changes on three nearest planets, in a tiny time interval according to astronomical standards.

**7.5. VALUE OF THE PHAETON HYPOTHESIS**

1. The destruction of a planet is a unique phenomenon, and repeating of such event is astronomically rear that reduces apocalyptical expectations in the society.

2. Gradual dispersion of the flow of fragments in space annually reduces statistical risks of one more meeting with the flow, making these risks close to zero in the long term.

3. As the most dangerous factor proved to be the electric discharge, but not falling of single meteorites, flow dispersion is the most reliable guarantee of the future safety.

4. In case of casual meeting with such fragment, with high degree of probability the Earth will destroy it itself by means of electric discharge of adequate capacity and with inaccessible for human technics speed.

5. As the flow of fragments regularly crossing the Earth orbit, has dissipated not immediately, it becomes clear why the mankind had time to get used to the idea of the Apocalypse regularity.

6. In such a situation, it is possible to consider all major prophecies already happened that would certainly decrease the pressure and put more actual tasks before the religious communities.

In myths, the catastrophic crash current is described with high accuracy [^42], however, myths are not a scientific source. The recognized source is annals. As there are many sources, there will be one reference [^43] - to a database. Now, here is the course of meteoric incident, one after another and with annalistic certificates when they are present.

**8.1. SUN ECLIPSE**

The sun was eclipsed in an unusual way; it was neither eclipse nor atmospheric smoke. Meteors are sometimes directly mentioned. Kepler considered it was caused by comet matter.

592 the Sun were eclipsed from the edge to the middle of its disk

603 half of the Sun was eclipsed within 10 months

626 half of the Sun darkened, and it lasted from October until July

February 12th, 1106 "sun eclipse" which was accompanied by meteors

April 20-22th, 1547 the sun was so much eclipsed that round it the stars were shining in the sky. Astronomer Kepler thought that it could be connected with "wide spread diffusion of some comet matter".

The meteoric phenomena were often connected with the Doomsday ('the end of the light'), and it is better to understand it literally as a light decrease, instead of the end of the world, - languages were simpler that time, and the words meant exactly what was written.

**8.2. THE METEORIC SHOWER OVER THE ARCTIC**

The fact that the meteoric shower passed over the Arctic in the approximate direction from Hudson Bay to the river Lena mouth is indicated by the permafrost outlines and depth of shelves flooding increasing to the north direction - from 40-60 metres at Gibraltar to 135-200 metres at Beringia and Doggerland. There are no direct annalistic evidences about that stage of catastrophic crash owing to absence of the survived witnesses, however the consequences of that incident was noticed not only in the Arctic.

562 pestilence and lightning in the Northern country 

735 there was a sign in the north, and ashes fell from the sky in some places 

**8.3. PRIOR BREAKDOWN FIELDS**

Prior breakdown electric fields cause electrostatic levitation which explains the observed phenomena of elevating of large subjects into the air (trees, yurtas, ground sites on hills). There is the greatest light flash, and there where the soil is damp, corona discharge appears.

1908 the Tungus meteorite. Local residents, who were interviewed by scientists of the expedition groups, pointed out that a moment before the terrible flash trees, yurtas, some ground sites (on the hills) elevated into the air here and there, waves went against their current in small rivers.

**8.4. CORONA DISCHARGE**

Intrusion of large meteoric bodies into the atmosphere with supersonic speed causes electric discharges of corona type ("Lights of St. Elm"). Currents are induced in the electric chains. There is radio noise.

Electric chains de facto are ore manifestation and layers of mineralized underground waters. The behavior of the currents induced in them is an opened question, but the Lights of St. Elm are mentioned in the annals.

526 A wonderful phenomenon was noticed in the Roman army: edges of spears began to shine with bright fire. Those seeing it were stricken with fear

1453 Lights of St. Elm was observed during the siege of Constantinople

**8.5. THE ELECTRIC DISCHARGE AND FRONT OF LIGHTNINGS**

Judging by numerous illustrations and direct annalistic evidences, appearance of lightnings in a close connection with comets pass is a banal phenomenon. The question is in scales.

684 "The star with a tail" is responsible for ... downpours ... accompanied with great lightnings

Fig. 21. Lubenets comet crisis [^44]

<!-- Local
[![](Step by step_files/19Lubienietsky_Comet_Crisis.jpg#clickable)](Step by step_files/19%2BLubienietsky_Comet_Crisis.jpg)
-->

[![](https://1.bp.blogspot.com/-l0TpVFzryPs/X7O3L_ro_OI/AAAAAAAADmI/HvF4DETtEW0wxAiT0iNDuxjc4zC8zYhsQCLcBGAsYHQ/s637/19%2BLubienietsky_Comet_Crisis.jpg#clickable)](https://1.bp.blogspot.com/-l0TpVFzryPs/X7O3L_ro_OI/AAAAAAAADmI/HvF4DETtEW0wxAiT0iNDuxjc4zC8zYhsQCLcBGAsYHQ/s637/19%2BLubienietsky_Comet_Crisis.jpg)

303 The lightning descended from the heavens and has incinerated all idols ... the fiery whirlwind came down from the sky

362 Apollo's Temple in Daphne, outside of Antioch, was destroyed by mysterious fire

1490 In Constantinople ... lightning destroyed 800 houses

A comet pass is often accompanied with ionic "moustaches" - as if it is the Chinese dragon.

530 A big star from which there was a white beam upwards, and lightning was created

Fig. 22. Donati's comet [^45]

<!-- Local
[![](Step by step_files/20D094D0BED0BDD0B0D182D0B8.jpg#clickable)](Step by step_files/20%2B%25D0%2594%25D0%25BE%25D0%25BD%25D0%25B0%25D1%2582%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-yvLe9aaV2kk/X7O3XwKIXZI/AAAAAAAADmM/ZsmLn9DReokJu5BHYz_aPNHfVAZ1AsU0wCLcBGAsYHQ/s1024/20%2B%25D0%2594%25D0%25BE%25D0%25BD%25D0%25B0%25D1%2582%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-yvLe9aaV2kk/X7O3XwKIXZI/AAAAAAAADmM/ZsmLn9DReokJu5BHYz_aPNHfVAZ1AsU0wCLcBGAsYHQ/s1024/20%2B%25D0%2594%25D0%25BE%25D0%25BD%25D0%25B0%25D1%2582%25D0%25B8.jpg)

**8.6. THE ELECTRIC DISCHARGE DISPOSITION**

Permafrost location and form indicate the disposition of the electric discharge and even the vector of the meteoric shower movingt: in the Arctic, drop-shaped, with the main movement direction from Hudson Bay to the mouth of the river Lena and the geometrical centre over the Novosibirsk Islands.

**8.7. DURATION OF THE ELECTRIC DISCHARGE**

Frozen subsoil parameters clarify the duration of the electric discharge. The Earth rotates: that is why first contact with the meteoric shower occurred not further east than the Bering Strait (170 ° West), and the last contact occurred not further to the west than the Yamal Peninsula (70 ° East). This distance covers 120° of the Earth's circumference. That is, about 8 hours. That seems to be the normal duration: in 1833, the planet was in a powerful meteoric Leonid shower for 9 hours [^46].

Fig. 23. Turn of the Earth for about 120 ° during the contact with the meteoric shower

<!-- Local
[![](Step by step_files/21D09FD0BED0B2D0BED180D0BED182.jpg#clickable)](Step by step_files/21%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582.jpg)
-->

[![](https://1.bp.blogspot.com/-EyXz4k38Xkc/X7O3pSyXZSI/AAAAAAAADmY/wXJ1bLYzTgQEnGv3Itrqs-e6xY35pieOwCLcBGAsYHQ/s800/21%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582.jpg#clickable)](https://1.bp.blogspot.com/-EyXz4k38Xkc/X7O3pSyXZSI/AAAAAAAADmY/wXJ1bLYzTgQEnGv3Itrqs-e6xY35pieOwCLcBGAsYHQ/s800/21%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D1%2580%25D0%25BE%25D1%2582.jpg)

**9.1. ELECTRIC DISCHARGE HIGH-ALTITUDE EXPLOSION**

Having received electrons and heat from the Earth-cathode, meteoric bodies blow up from below, away from the Earth that protects the planet from carpet bombing and destruction. The blown up meteoric bodies become electrically neutral, lose their own speed and some part of this bulk, in the result of plural collisions, changes the direction and is sowed on the Earth.

**9.2. SEPARATION OF METEORIC SUBSTANCE**

Owing to rotation of the Earth and resistance of atmosphere, it is more likely to expect drifting of the fragments of meteoric bodies that have lost their own speed to the West and their latitude zones separation according, depending on weight and density of the bodies.

**9.3. METEORIC GOLD**

Gold deposits lie most close to the zone of the meteoric shower passing and as we have already mentioned, match the contours of the meteoric incident (glaciation) - from Kamchatka to the Ural Mountains. Gold is very dense; therefore, nuggets with the rotation form were found in the Ural Mountains area and not found at the equator. Thus, gold nuggets in the Murmansk area and tektites from Australia have equally big amount of bodies of the button form.

Fig. 24. Nuggets in the Murmansk area [^47] and tektites in Australia [^48]

<!-- Local
[![](Step by step_files/22D0A2D0B5D0BAD182D0B8D182D18B.jpg#clickable)](Step by step_files/22%2B%25D0%25A2%25D0%25B5%25D0%25BA%25D1%2582%25D0%25B8%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-_dQDtAnWWQI/X7O4vWOH5KI/AAAAAAAADmk/KNMtEmUThgkSyzZ4Btzj-wOfteo9QMcbACLcBGAsYHQ/s1600/22%2B%25D0%25A2%25D0%25B5%25D0%25BA%25D1%2582%25D0%25B8%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-_dQDtAnWWQI/X7O4vWOH5KI/AAAAAAAADmk/KNMtEmUThgkSyzZ4Btzj-wOfteo9QMcbACLcBGAsYHQ/s1600/22%2B%25D0%25A2%25D0%25B5%25D0%25BA%25D1%2582%25D0%25B8%25D1%2582%25D1%258B.jpg)

There are nuggets of other forms - spongiform; precisely the same form molten copper takes on when poured into the water during experiments [^49].

Fig. 25. The molten copper poured into the water, and a gold nugget

<!-- Local
[![](Step by step_files/23D0A1D0B0D0BCD0BED180D0BED0B4D0BAD0B8.png#clickable)](Step by step_files/23%2B%25D0%25A1%25D0%25B0%25D0%25BC%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B8.png)
-->

[![](https://1.bp.blogspot.com/-JhkCddHSI5M/X7O45Qz0_KI/AAAAAAAADmo/v1NBznY082MfTZZhXFy9mXIny4BtsLb1QCLcBGAsYHQ/s972/23%2B%25D0%25A1%25D0%25B0%25D0%25BC%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B8.png#clickable)](https://1.bp.blogspot.com/-JhkCddHSI5M/X7O45Qz0_KI/AAAAAAAADmo/v1NBznY082MfTZZhXFy9mXIny4BtsLb1QCLcBGAsYHQ/s972/23%2B%25D0%25A1%25D0%25B0%25D0%25BC%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B8.png)

Legends of Indians say: the flame rose to heavens, and many stars melted, so they poured with rain of molten metal on the earth, forming ore searched for by the white man. This description is ideally exact: the electric discharge going from the Earth upwards, destruction of meteoric bodies and dropping of melted metal down on the Earth.

**9.4. BOG IRON**

Bog iron lies a bit further from the zone of the meteoric shower passing and has its own zones as well - in Germany, Poland, Belarus, eastern part of Russia and in the USA in the states of New Jersey, Maryland, Maine, Virginia, Massachusetts, and Dakota. The deposits are located at identical distance from the northern pole in Greenland; it is known that they were formed recently, and they have superficial location, therefore they influenced ferrous metallurgy development in Europe and the USA [^50]. Such deposits are found nowhere else.

Fig. 26. Deposits of bog iron

<!-- Local
[![](Step by step_files/24D091D0BED0BBD0BED182D0BDD0BED0B5.jpg#clickable)](Step by step_files/24%2B%25D0%2591%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BD%25D0%25BE%25D0%25B5.jpg)
-->

[![](https://1.bp.blogspot.com/-DmAiNJ_9nGg/X7O5DodU2iI/AAAAAAAADms/XYWrjMHVHcgbnbZKyVP0hAa3_yteTmU1ACLcBGAsYHQ/s1597/24%2B%25D0%2591%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BD%25D0%25BE%25D0%25B5.jpg#clickable)](https://1.bp.blogspot.com/-DmAiNJ_9nGg/X7O5DodU2iI/AAAAAAAADms/XYWrjMHVHcgbnbZKyVP0hAa3_yteTmU1ACLcBGAsYHQ/s1597/24%2B%25D0%2591%25D0%25BE%25D0%25BB%25D0%25BE%25D1%2582%25D0%25BD%25D0%25BE%25D0%25B5.jpg)

**9.5. METEORIC GLASS**

Some part of meteoric bodies, which had lost their speed, but not charge, dropped out on the Earth in the melted form and became "Libyan glass" or even dropped out in the form of plasma. Professor Claude Schaeffer from the Parisian University, studying stratigraphy of excavations, pointed out the soil layers located in India, Western Asia and North Africa, caked to the condition of glass and indicating "falling of fire from the sky". It led to the life termination approximately in 2350, 2100, 1700, 1450, 1365, and 1235 BC (according to the accepted date scale) [^51]. In Schlieman Troy the earth caked to the glass condition. It is necessary to note that latitudinal orientation is typical for this type of bodies as well, though there are some exceptions.

Today the science again comes back to the conclusions made by professor Claude Schaeffer. Here it makes sense to quote the following.

At Abu Hureyra (AH), Syria, the 12,800-year-old Younger Dryas boundary layer (YDB) contains peak abundances in meltglass, nanodiamonds, microspherules, and charcoal. AH meltglass comprises 1.6 wt.% of bulk sediment, and crossed polarizers indicate that the meltglass is isotropic. High YDB concentrations of iridium, platinum, nickel, and cobalt suggest mixing of melted local sediment with small quantities of meteoritic material. Approximately 40% of AH glass display carbon-infused, siliceous plant imprints that laboratory experiments show formed at a minimum of 1200°-1300 °C; however, reflectance-inferred temperatures for the encapsulated carbon were lower by up to 1000 °C. Alternately, melted grains of quartz, chromferide, and magnetite in AH glass suggest exposure to minimum temperatures of 1720 °C ranging to >2200 °C. This argues against formation of AH meltglass in thatched hut fires at 1100°-1200 °C, and low values of remanent magnetism indicate the meltglass was not created by lightning. Low meltglass water content (0.02-0.05% H2O) is consistent with a formation process similar to that of tektites and inconsistent with volcanism and anthropogenesis. The wide range of evidence supports the hypothesis that a cosmic event occurred at Abu Hureyra ~12,800 years ago, coeval with impacts that deposited high-temperature meltglass, melted microspherules, and/or platinum at other YDB sites on four continents. [^144]

Fig. 27. Caked stones of the Czech Republic ancient settlement mounds

<!-- Local
[![](Step by step_files/25D09ED0B3D0BED0BDD18CD181D0BDD0B5D0B1D0B0.jpg#clickable)](Step by step_files/25%2B%25D0%259E%25D0%25B3%25D0%25BE%25D0%25BD%25D1%258C%2B%25D1%2581%2B%25D0%25BD%25D0%25B5%25D0%25B1%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-kHFH6_DNoCo/X7O5fX86M0I/AAAAAAAADm4/5Jnpd0Q1G9EIWZR2xM8er1mwlwYbj_rEwCLcBGAsYHQ/s879/25%2B%25D0%259E%25D0%25B3%25D0%25BE%25D0%25BD%25D1%258C%2B%25D1%2581%2B%25D0%25BD%25D0%25B5%25D0%25B1%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-kHFH6_DNoCo/X7O5fX86M0I/AAAAAAAADm4/5Jnpd0Q1G9EIWZR2xM8er1mwlwYbj_rEwCLcBGAsYHQ/s879/25%2B%25D0%259E%25D0%25B3%25D0%25BE%25D0%25BD%25D1%258C%2B%25D1%2581%2B%25D0%25BD%25D0%25B5%25D0%25B1%25D0%25B0.jpg)

Detailed report of the Czech professor V.O. Pich informed Russian archeologists about specific settlements in the Czech Republic which can be subdivided into three categories: 1) settlement with mounds of fire caked stones, 2) settlement with mounds of stones put together without cement and 3) settlement with earth mounds. According to the referent's mind, 1st category settlements were of the Slavs' origin but not of the Boys' ne s it had been supposed earlier.

**ANNALISTIC EVIDENCES**

352 While Pap and Athenogenes (Pap and Athanagines) with wives and children drank (feasted) in the church, fire fell from above and plunged them in chasm

362 Apollo's Temple in Daphne, in Antioch suburbs, was destroyed by some mysterious fire

451 Fire fell down in many places

500 Heavenly fire burnt Baalbek

500 Fire fell down on Antioch

501 Big fire was visible from the north side which burnt all night long 22 aba

710 Fiery hailstones made all sea boil and when gum in the ships thawed they sank with people on the sea bottom 

841 Fire appeared three nights long. It was rain which peeled crust from trees and felled stones

1011 Fire fell down on Antioch and burnt Evangelist Luke church.

1110-1111 in Armenia where "during the night flame shower poured on Lake Van which waves gave an awful roar and rushed on the coast

**9.6. THE GREEK FIRE**

The annalistic evidence of the fiery hailstones which destroyed the fleet, has confidently something in common with evidences of use of the mythical "Greek fire".

Technical possibilities did not allow to throw out fire mixture with necessary initial speed. Any gust of head wind turns the weapon against the shooting person. Fighters of engineering-assault crews apply fire-resistant masks and regularly face return blow of mixture though present flame throwers work on compressed air and beat for 50-200 metres. But you can't put on a fire-resistant mask on your sails.

Sea rolling puts the following unsolvable problem: the loose siphon is dangerous, and fixed one fixes the bombardment sector and gives to the opponent "blind zones". Even if the siphon was mobile, as modern howitzer D-30, on 50 metres distance the crew was easily destroyed with arrow shooting. It was not a great problem to target into a ceramic pot with the fire mixture. Moreover, it is important, not taking into consideration the mixture compound, nobody mentions how it ignited at the moment of shooting [^52].

Annalistic evidences show that use of Greek fire coincides with others, going at the same time and in the same place, real catastrophic crashes.

677 the Arabian fleet is destroyed by the Greek fire and flooded by a storm.

972 the fleet is destroyed by the Greek fire, and the city of Fraxinetum is wiped off the face of the Earth.

1453 the Greek fire is used at the siege of Constantinople in which at that time there was unusual fog, and Lights of St. Elm shone. The same year cholera, thunder-storm, hailstones and downpour which flooded the streets fell upon Constantinople, the dust closed the Sun, and the was plague and abnormal storm tides in Europe.

In such situation, the Greek fire in annals is some attempt of the latest editors to somehow explain the inexplicable facts.

**9.7. HOMOGENEOUS SAND OF NOT CLEAR ORIGIN**

The large quantity of the sand, which does not have a clear source of origin, covered the river systems of the Western Africa and the coast of Brazil and created sandy sea banks far from the river mouths.

Fig. 28. Coastal sand of Lençóis Maranhenses (Brazil)

<!-- Local
[![](Step by step_files/26D094D18ED0BDD18B.png#clickable)](Step by step_files/26%2B%25D0%2594%25D1%258E%25D0%25BD%25D1%258B.png)
-->

[![](https://1.bp.blogspot.com/-Zvur2hGmncc/X7O553i2DMI/AAAAAAAADnE/N4qZyFuQ0UocoxTqXKKdDJM4kGjVV_ZwwCLcBGAsYHQ/s1600/26%2B%25D0%2594%25D1%258E%25D0%25BD%25D1%258B.png#clickable)](https://1.bp.blogspot.com/-Zvur2hGmncc/X7O553i2DMI/AAAAAAAADnE/N4qZyFuQ0UocoxTqXKKdDJM4kGjVV_ZwwCLcBGAsYHQ/s1600/26%2B%25D0%2594%25D1%258E%25D0%25BD%25D1%258B.png)

At old metal works, slag was poured out into water. In result, it was pure and homogeneous sand. Heated to the melting state, and after high-altitude explosion - even to vaporous and dust-like conditions - meteoric masses passing through the saturated with water steams atmosphere could give a similar product [^53].

**9.8. THE SPACE DUST**

The space dust would deposit not earlier than makes some turns round the planet, and, being pulled together in the polar regions under the influence of the Earth electromagnetic field. So did the dust after nuclear explosions in 1960-1970.

Atmosphere played the role of the ideal sieve, which divided products in strict conformity with the parametres.

Now we will see action of shock waves.

**[BALLISTICS. THE BASIC INFORMATON]**

**10. SHOCK WAVES**

As the result of close passing of the meteoric shower five variants of shock waves are formed:

- The wave as a product of energy release of the lightnings front;

- The safe wave formed as the result of the destruction of meteoric bodies directed away from the Earth;

- The input ballistic shock wave formed in the result of supersonic intrusion of the bodies into the atmosphere;

- A series of periodic waves;

- The output ballistic wave.

**10.1. THE SHOCK WAVE OF THE FRONT OF LIGHTNINGS**

The speed of a lightning in the atmosphere is from 10 to 100 thousand km a second, that is, it is cosmic velocity, and the speed of distribution of the electric field (actually, electric current) is equal to the velocity of light. This is the speed of the electric discharge moving in case of a contact of the planet with the meteoric shower. As the discharge moves from the bottom upwards, from the bowels to space, the shock wave of the front of lightnings begins under the Earth surface, probably, in the mantle, and comes to the end in the atmosphere; it is a single process. There are hundreds of thousands discharges simultaneously, and the capacity of this complex shock wave is extremely great. In the bowels the wave moves faster, in the atmosphere -- more slowly, but to differentiate them or to single them out of other hurricanes and earthquakes in annals is difficult though clearly that the Lighthouse of Alexandria could be destroyed by a shock wave and could not be wrecked by a hurricane.

639 Strong wind eradicated and carried away the greatest trees, subverted columns

666 Great wind blew from the north, destroyed a church

880 The reconstructed the Lighthouse of Alexandria was destroyed by the sudden wind

1106 During the hurricane the Column of Constantine was destroyed

1460 The wind of extraordinary power broke down many mansions and churches, destroyed many houses

Fig. 29. Destruction of the Tower of Babel [^62]

<!-- Local
[![](Step by step_files/27D092D0B0D0B2D0B8D0BBD0BED0BD.png#clickable)](Step by step_files/27%2B%25D0%2592%25D0%25B0%25D0%25B2%25D0%25B8%25D0%25BB%25D0%25BE%25D0%25BD.png)
-->

[![](https://1.bp.blogspot.com/-nHZUnxI6mVw/X8feIqXD3XI/AAAAAAAADnk/CMTY2iZYrug2eqMyAIFeH_lFEK3q7f8IgCLcBGAsYHQ/s625/27%2B%25D0%2592%25D0%25B0%25D0%25B2%25D0%25B8%25D0%25BB%25D0%25BE%25D0%25BD.png#clickable)](https://1.bp.blogspot.com/-nHZUnxI6mVw/X8feIqXD3XI/AAAAAAAADnk/CMTY2iZYrug2eqMyAIFeH_lFEK3q7f8IgCLcBGAsYHQ/s625/27%2B%25D0%2592%25D0%25B0%25D0%25B2%25D0%25B8%25D0%25BB%25D0%25BE%25D0%25BD.png)

There are hundred thousand of electrical discharges so, separate shock waves as well, there appears resonance in some places, thus, the forces of pressure and tension  reach their critical values, and mountains would collide, and the crust would burst, and cities - sink in hell. The experts point out the following fact. The hard substance behind the shock wave front behaves as ideal compressed liquid, that is, it seems to lack both intermolecular and internuclear bonds, and substance strength does not influence the wave [^55].

528 The Earth split and half of the city with inhabitants sank through the earth 

734 In the Desert of Sheba the mountain collapsed with the mountain, and settlements sank

791 The mountains of Kogat collapsed. The mountain fell and stopped the flow of the Euphrates for the whole day 

901 In Thrace the earth opened wide and swallowed many churches

1011 All city of Yezenga sank

1348 More than seventy castles and country houses sank higher the river Drava, and all was turned upside down. A huge mountain divided into two halves, filled all valley and blocked up the river mouth along ten versts

1524 In Hungary houses and churches sank into the earth

1692 The earth began to tremble, and because of that tremble cracks opened and closed there. Many people got into those cracks; thus, some got squeezed in the earth so that half of them had their top part of the trunk above the earth

1771 Earthquake at the Caucasus near Beshtau town, some part of Mashuk town sank

1783 The town of Messina greatly suffered from the cracks formed because of the earthquake like huge precipices: some of them reached considerable sizes, sometimes about 70 sazhens width and half of verst length. Some of such cracks remained after the earthquake, others, on the contrary, momentarily closed

1804 In Kentucky the whole mountain which was called Klink, failed, having erupted the clouds of such dense smoke that in 20 miles from that place there was absolute darkness

1857 The speed of such earth opening and closings was really great ... after the earthquake of 1857, near the city of Naples a hen was found, whose feet were jammed in soil, but the trunk remained absolutely free above the earth surface

**10.2. SHOCK WAVE DEGENERATION**

The shock wave degenerates into the sound one, considering the planet parametres, in the infrasonic range, first of all. The infrasound causes vibration of large objects as it drops into resonance with them [^56]. That is the reason of evidences of destruction of cities and structures with sound.

And blew the horns ... and because of it the wall fell to its very foundations 

400 That year the earth roared in Rome for whole seven days

1380 The underground depths opened wide, the earth began terribly to tremble. The earth at the Don moaned and an underground rumble came in the distant part of the land, and on the other side of the Don, terrible and ruthless 

The Storegga submarine landslide could be quite possibly provoked by an infrasonic wave.

**10.3. INFRASOUND INFFLUENCE ON BIOTHAT**

The infrasound is capable to kill, causing damage of the nervous system, endocrine system and causing break of lung alveoli [^56]. The possible reason of such damages is fractal arrangement of these systems and organs (so broadband antennas are arranged now) - owing to resonance origin on the system sites which length coincides with the length of the infrasound wave, - such sites are present in the systems with fractal arrangement. Here is the citing. 

**10.4. THE REASON OF MASS DESTRUCTION IN PLEISTOCENE**

(Citation)

Suffocation also is supposed to happen in case of four other frozen giants. Voloshovich came to the conclusion that his second (found in 1910) buried mammoth had suffocation. The third example was the mammoth baby Dima whose "lung alveoli bring the idea of asphyxia (suffocation) after "great efforts just before the death". Pallas' rhinoceroses also have asphyxia symptoms. Blood vessels and even small capillaries were found out to be filled with brown coagulated blood, which in many cases still had its red colour. It is the type of the proof which we search, when we want to know whether the animal drowned or died because of suffocation. Asphyxia (suffocation) is always accompanied with filling of capillaries with blood.

Schrenk's rhinoceroses were found with the expanded nostrils and an open mouth. Researchers concluded that "the animal died because of suffocation which it tried to avoid, having opened its nostrils widely". As a whole, three mammoths and two rhinoceroses, apparently, suffocated. No other causes of death were revealed in case of these frozen giants (Farrand, 1961). [^57]

Some large animals communicate by means of infrasound, for example, whales and elephants, therefore even there, where the power of infrasound was insufficient to kill, the mass disorientation of all pleistocene megafauna was inevitable. 

In fact, it could be not drowning and infrasound influence; barotrauma causes just the same consequences [^58], it could really happen as the result of some large meteoric shower passing over the Arctic.

**10.5. THE POSSIBLE REASONS OF THE BLACK DEATH**

The most sensible reason of the Black Death could be telluric and/or volcanic gases poisoning, but infrasonic strike and barotrauma have similar symptoms: alveoli damage, blood overfilling of capillaries, dark cyanotic stains on skin [59; 60] and very fast death, as it is shown in evidences and illustrations. Healthy people blackened, began to cough out their lungs and fell there where they were overtaken by the disaster - without any incubatory period.

Fig. 30. The Black Death [^61]

<!-- Local
[![](Step by step_files/28D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/28%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-xp4wPPCylQA/X8fecSOTs2I/AAAAAAAADns/tIIvgd9s3RMq07vOyTeeX-InP9Q-XXpJgCLcBGAsYHQ/s585/28%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-xp4wPPCylQA/X8fecSOTs2I/AAAAAAAADns/tIIvgd9s3RMq07vOyTeeX-InP9Q-XXpJgCLcBGAsYHQ/s585/28%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

Fig. 31. The Black Death of 1721 in Marseilles [^62]

<!-- Local
[![](Step by step_files/29D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/29%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-Y-g2aMFZFGg/X8fenIydroI/AAAAAAAADnw/o7YrQfKJh8w00nw68Xxn2X677znH8gfyACLcBGAsYHQ/s493/29%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-Y-g2aMFZFGg/X8fenIydroI/AAAAAAAADnw/o7YrQfKJh8w00nw68Xxn2X677znH8gfyACLcBGAsYHQ/s493/29%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

**11. THE WAVE FORMED AS THE RESULT OF THE DESTRUCTION OF METEORS**

For a planet, it is almost safe, as it is directed away from the Earth.

**12. BALLISTIC WAVES**

There are two types of the ballistic waves forming as the result of supersonic intrusion of meteoric bodies into the atmosphere: the basic wave and a series following each other periodic waves. The speed of meteoric showers is usually between 11 and 72 km/s, that is, at input a ballistic wave would be like a shock wave.

Fig. 32. A body flying with the supersonic speed [^63]

<!-- Local
[![](Step by step_files/30D0A1D0B2D0B5D180D185D0B7D0B2D183D0BA.jpg#clickable)](Step by step_files/30%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg)
-->

[![](https://1.bp.blogspot.com/-5zy1pWP9dlA/X8fiHFtAxzI/AAAAAAAADoA/tezBPoqjZFUrAVjFOfIo59AKMat5bC8bQCLcBGAsYHQ/s1272/30%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg#clickable)](https://1.bp.blogspot.com/-5zy1pWP9dlA/X8fiHFtAxzI/AAAAAAAADoA/tezBPoqjZFUrAVjFOfIo59AKMat5bC8bQCLcBGAsYHQ/s1272/30%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg)

Just behind the Mach cone the elevated pressure zone is formed - for a bullet it is from 6 to 9 atmospheres, and for meteoric bodies, probably, it is much more. The land surface of Canada and Siberia and the waters of the Arctic Ocean weakened by the electric discharge would be torn off with the speed of explosion.

**THE EVIDENCE IN LEGENDS**

Owing to high danger of this phase of meteoric incident, evidences of its process are absent. There is only one exception: the undated legend of the Mansi. According to one of the legends, numerous "woody islands" were brought "from the north wind side" by Winged god Kurk iki ("Eagle-old man"). When he was caring on himself the earth-load, its pieces fell down and islands were formed [^64].

There is also the rarefied atmosphere zone. For the case of 1mm diameter needle fling with the speed of 30 km/s diameter, the rarefied atmosphere zone would be not less than 10 sm, there would be practical vacuum. The length of the vacuum channel is equal to the half of diameter of the rarefied atmosphere zone multiplied by the relation of the subject speed to the sound velocity and would be not less than 5 metres [^65]. For a 30 metres diameter meteorite fling with the same speed, the diameter of the rarefied atmosphere zone would be 2.6 km, and the length of the vacuum channel would be 114 km.

**12.1. WAVE MULTIPLICITY** 

We consider a meteoric shower consisting of millions of bodies, that is, high and low pressure zones and vacuum caverns created by them would constantly cross, pressure in the oscillatory regime would change with dramatic frequency and amplitude, all both live and lifeless objects got into the sphere of activity of this channel would pass millions acts of barotraumas. Temperature fluctuations of the same frequency and abnormal amplitude, following from the Gay-Lussac, Boyle--Mariotte and Charles laws would be the organic component of the process of pressure fluctuation.

Fig. 33. Two bodies, flying with the supersonic speed [^66]

<!-- Local
[![](Step by step_files/31D0A1D0B2D0B5D180D185D0B7D0B2D183D0BA.jpg#clickable)](Step by step_files/31%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg)
-->

[![](https://1.bp.blogspot.com/-GfLYI0VPx04/X8fiT8-MjDI/AAAAAAAADoE/cXERx1yapSsGS5f7xFMaQxYeoaAtaiVswCLcBGAsYHQ/s660/31%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg#clickable)](https://1.bp.blogspot.com/-GfLYI0VPx04/X8fiT8-MjDI/AAAAAAAADoE/cXERx1yapSsGS5f7xFMaQxYeoaAtaiVswCLcBGAsYHQ/s660/31%2B%25D0%25A1%25D0%25B2%25D0%25B5%25D1%2580%25D1%2585%25D0%25B7%25D0%25B2%25D1%2583%25D0%25BA.jpg)

**12.2. PERIODIC WAVES**

The basic shock wave is followed by the periodic waves contributing to the overall picture of destruction. Periodic waves, undoubtedly, left their traces during the displacement of the meteoric shower from the east on the west.

**12.3. THE ZONE OF RAREFIED ATMOSPHERE **

Once having entered into atmosphere, the meteoric shower would keep the rarefied atmosphere zone approximately in the same borders.

Fig. 34. Hydrogasoanalogy of the shock wave [^67]

<!-- Local
[![](Step by step_files/32D093D0B8D0B4D180D0BED0B3D0B0D0B7D0BED0B0D0BDD0B0D0BBD0BED0.jpg#clickable)](Step by step_files/32%2B%25D0%2593%25D0%25B8%25D0%25B4%25D1%2580%25D0%25BE%25D0%25B3%25D0%25B0%25D0%25B7%25D0%25BE%25D0%25B0%25D0%25BD%25D0%25B0%25D0%25BB%25D0%25BE%25D0%25B3%25D0%25B8%25D1%258F.jpg)
-->

[![](https://1.bp.blogspot.com/-rCyLghMQoQg/X8fifWCcbzI/AAAAAAAADoM/29knC-gvdKMvDK73lpMtQxwAI70UBCz7gCLcBGAsYHQ/s814/32%2B%25D0%2593%25D0%25B8%25D0%25B4%25D1%2580%25D0%25BE%25D0%25B3%25D0%25B0%25D0%25B7%25D0%25BE%25D0%25B0%25D0%25BD%25D0%25B0%25D0%25BB%25D0%25BE%25D0%25B3%25D0%25B8%25D1%258F.jpg#clickable)](https://1.bp.blogspot.com/-rCyLghMQoQg/X8fifWCcbzI/AAAAAAAADoM/29knC-gvdKMvDK73lpMtQxwAI70UBCz7gCLcBGAsYHQ/s814/32%2B%25D0%2593%25D0%25B8%25D0%25B4%25D1%2580%25D0%25BE%25D0%25B3%25D0%25B0%25D0%25B7%25D0%25BE%25D0%25B0%25D0%25BD%25D0%25B0%25D0%25BB%25D0%25BE%25D0%25B3%25D0%25B8%25D1%258F.jpg)

**12.4. THE INFLUENCE ZONE**

The rarefied atmosphere zone and the zone of influence of the meteoric shower owing to the Earth rotation would be displaced from the east on the west that is marked by the location of the ridge and hollow complexes from the Taimyr Peninsula to the Ural Mountains.

Fig. 35. The summary map of the location of the ridge and hollow complexes [^68].

<!-- Local
[![](Step by step_files/33D092D0BED0BBD0BDD18B.jpg#clickable)](Step by step_files/33%2B%25D0%2592%25D0%25BE%25D0%25BB%25D0%25BD%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-46uCWB_9zOo/X8fipgS5WAI/AAAAAAAADoU/lhOkYztvQrMSaHW55kABU4-GMO3nOEJsgCLcBGAsYHQ/s958/33%2B%25D0%2592%25D0%25BE%25D0%25BB%25D0%25BD%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-46uCWB_9zOo/X8fipgS5WAI/AAAAAAAADoU/lhOkYztvQrMSaHW55kABU4-GMO3nOEJsgCLcBGAsYHQ/s958/33%2B%25D0%2592%25D0%25BE%25D0%25BB%25D0%25BD%25D1%258B.jpg)

1 - the maximum border of last glaciation;

2 - spreading of sheeted flows (along the Baer knolls, mane ridges, other traces);

3 - ancient drain hollows;

4 -- skirting valleys;

5 - presumptive continuation of the catastrophic drain;

6 - basic ways of the subglacial drain.

We already see that the ridge and hollow complexes, as well as all other signs of meteoric incident are wrongly accepted for result of the glaciation. We also see that incident has the direct relation to permafrost formation. However, the permafrost spot is wider than the sector of the ballistic wave influence.

Here it is necessary to distinguish the action of the high-altitude electric discharge creating permafrost from the action of the ballistic wave tearing off and faulting the soil layer. The electric discharge easily goes through vacuum that is why it could start prier the atmosphere contact with the meteoric shower (at the Chukchi Peninsula), and come to the end only after the meteors leaving the atmosphere (over Scandinavia). The operating zone of the ballistic wave is narrower - from the Lena River to the Ural Mountains. The zone of the ridge and hollow complexes is even narrower - from the Taimyr Peninsula to the Ural Mountains, as the first blow moving towards the Lena dragged there the least dense substances - water and charred biota, so the ridges were not formed.

Fig. 36. Stacking of vectors the ridge and hollow complexes on the glaciation map [68; 44]

<!-- Local
[![](Step by step_files/34D092D0B5D0BAD182D0BED180D18B.jpg#clickable)](Step by step_files/34%2B%25D0%2592%25D0%25B5%25D0%25BA%25D1%2582%25D0%25BE%25D1%2580%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-g3G4RH-YVe8/X8fi1yxo_fI/AAAAAAAADoc/NV-ADuwOBVQHa9Q7YZ51XrGTw4bggM24ACLcBGAsYHQ/s890/34%2B%25D0%2592%25D0%25B5%25D0%25BA%25D1%2582%25D0%25BE%25D1%2580%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-g3G4RH-YVe8/X8fi1yxo_fI/AAAAAAAADoc/NV-ADuwOBVQHa9Q7YZ51XrGTw4bggM24ACLcBGAsYHQ/s890/34%2B%25D0%2592%25D0%25B5%25D0%25BA%25D1%2582%25D0%25BE%25D1%2580%25D1%258B.jpg)

Boundary formations around the White Sea are directed under the same corner, as the Lena mouth, and it is possible to consider just this formation as a casual one and to ascribe it to the beginning of the meteoric incident.

Fig. 37. Boundary glacial formations around the White Sea [^69].

<!-- Local
[![](Step by step_files/35D09FD0BBD0B0D182D0BED09FD183D182D0BED180D0B0D0BDD0B0.jpg#clickable)](Step by step_files/35%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2582%25D0%25BE%2B%25D0%259F%25D1%2583%25D1%2582%25D0%25BE%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-pnGVXrDOB5s/X8fjCEWM__I/AAAAAAAADog/ldfZaJdD4nUf6OVlRVVSBKYHKlyXxb5uACLcBGAsYHQ/s988/35%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2582%25D0%25BE%2B%25D0%259F%25D1%2583%25D1%2582%25D0%25BE%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-pnGVXrDOB5s/X8fjCEWM__I/AAAAAAAADog/ldfZaJdD4nUf6OVlRVVSBKYHKlyXxb5uACLcBGAsYHQ/s988/35%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2582%25D0%25BE%2B%25D0%259F%25D1%2583%25D1%2582%25D0%25BE%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0.jpg)

The main boundary glacial formations (finite-morainal belts, zones of glaciodislocation) and directions of the ice movement in the Northern Eurasia to the West from the Lena delta. 

Epoch of the last glaciation. The centre of ice spreading -- at the place of the Kara Sea:

1 - continental slope; 2 - boundary glacial formations; 3 - orientation of glacial scars and shifting of boulders; 4 -- trog-canals on the shelf - canals of ice drain; 5 - a site with detailed dating of the Siberia glaciation traces 

**12.5. INFLUENCE DETAILS**

That fact that woolly mammoths survived on Wrangel Island and the Pribilof Islands (around 180-170 ° longitudes) and died out much later, about 4000 years ago, points at the extremely eastern frontier of the incident. Neither the front of lightnings, nor the shock wave causing barotraumas did not reach there. The mutation of mtDNA worked there in the latter period.

The ballistic wave partially stroke the western part of the Ural Mountains region, but mostly passed from the east starting from the Ural Mountains where it crossed with front of the waves moving from the northeast having started by the same process earlier.

The front of waves losing its initial impulse while moving southwards was displaced to the west with the speed of the Earth surface rotation at that latitude - about 1185 km per hour.

**12.6. OUTPUT BALLISTIC WAVE**

When the Earth atmosphere leaves the sphere of the meteoric shower ballistic influence (judging by ridge and hollow complexes, on the longitude of the Ural Mountains), the rarefied atmosphere zone would collapse with the speed exceeding the sound velocity. The exact value of this wave speed depends on the relation of the pressure in front of the wave (almost vacuum) and behind it (tens of atmospheres), and in that case the speed was enormous. The discharge of the front of lightnings could go on, but the rarefied atmosphere zone would already disappear.

**[THE ELECTRIC DISCHARGE. THE COURSE OF EVENTS]**

**13. THE ORDER OF THE CATASTROPHIC CRASH**

The main factors are described here, so now it is the right time to present the whole list of the basic consequences.

**13.1. PRIOR BREAKDOWN ELECTRIC FIELDS**

Prior breakdown electric fields weaken the Arctic soil, first of all, in Canada, and start electrolytic processes in the underground mineralized waters and the Arctic Ocean. Electric activity of ocean saline and underground waters dramatically increases.

**13.2. INFRA-RED RADIATION**

The source of infra-red radiation is both meteoric glow and energy of explosions, and it proceeds during all time of the incident (about 8 hours). A good illustration of it is the evidence of the witness of the Tungus meteorite passing through the atmosphere.

1908, Simeon Semyonov: "Suddenly in the north the sky split, and in it there was wide fire high over the wood which covered all northern part of the sky... I felt so hot as if my shirt started burning on me" [^70].

In the presence of oxygen (outside the rarefied atmosphere zone) the infra-red radiation promotes burning, and in case of oxygen lack (inside the rarefied atmosphere zone) - pyrolysis of biota (its transformation into coal).

**13.3. HYDROGEN IONIZATION**

The ionized hydrogen of the atmosphere, water and bowels becomes "proton gas".

**13.4. IONIZATION OF THE ATMOSPHERE GASES**

Ionization of the atmosphere gases causes its fast chilling. A good example is in annals: the birds frozen in their flight in 528, 524, 548, 554, and 1709.

**13.5. POLARIZATION OF THE ATMOSPHERE AND LAKE EAST**

The effect of the gases' polarization forms a cloud of the ionized hydrogen at the South Pole which after the end of the incident, in approximately 8 hours, would be oxidized to the water state and would create 190 metres of ice over Lake East (between 3538 and 3728 metres of depth); it is physically, chemically and biologically sterile, entirely monolithic and without any features of seasonal structure [^71]. Just under this ice layer, there is the ice of the lake itself, that is, this sterile ice is the beginning of the formation of the ice layer of Antarctica.

**13.6. FORMATION OF ISOTOPES**

In the rarefied atmosphere zone and in the endothermic reactions zone, isotopes are formed typical for low temperatures that makes paleoclimatologists come to the conclusion of low average winter temperatures in Siberia during the Pleistocene epoch.

**13.7. DEUTERIUM SYNTHESIS AND MUTATIONS**

There are hundreds of thousands of breakdown channels, hard x-ray appears, as well as neutron radiation, as the result of the reaction of deuterium nuclear synthesis. In the long term, it leads to mutations and growth of the variety of subspecies; so the Coregonidae as a species appeared in the Pleistocene.

**13.8. WATER DECOMPOSITION**

In the zone of the Arctic Ocean and the Arctic coasts, both in the ocean and the bowels, the some part of the water decomposes as hydrogen and oxygen because of influence of hundreds of thousands electrical discharges from below that increases substance volume 1867 times. In the soil layers, there are cavities. Water in the electric discharge places foams with the formation of ice crystals that dramatically increases mobility of ocean substance and promotes its release caused by the shock waves to Siberia region and into the atmosphere. In addition, pressure upon the seabed decreases quickly that promotes disintegration of methane hydrates sea deposits.

**13.9. BIOTA CARBONIZATION**

Fauna remains in coal layers is a typical fact. Here is the citation: the Vorkutsk series. In general, the series represents a layer of carboniferous rocks ... fauna and flora remainders are numerous. The fauna is presented by marine and nonmarine representatives [^72].

The electric discharge caused a phenomenon similar to IV degree electrical shock both in continental and marine flora and fauna: the ionized hydrogen migrates, and the carbon without any organic component remains. The hydrogen having left the molecules is plentifully found in the deposits of oil, coal and combustible slates, considerably complicating their mining.

The shock waves buried the charred fauna and flora. The presence of coal and salinized soil the Lena basin together with the lack of "glaciation" signs, that is, any granite boulders, indicates substance separation according to its specific weight, and that the suspension flow moved to the Lena basin from ocean, mainly.

**13.10. WOOD PYROLYSIS**

Wood goes through processes that are more complicated: there is also hydrogen release as the result of the electric discharge and pyrolysis under the influence of infra-red radiation for lack of oxygen. In the Lena basin, there are coal deposits of both continental and marine origin, and they have one thing in common - specific weight thanks to that the ballistic waves managed to collect different species of coal in one place.

**13.11. ROTATION OF SEA BASINS**

Electromagnetic influence of the meteoric shower increases the electrolyte circulation speed of the seawater in the Arctic basin [^73] that increases tidal effects according to Jusup Khizirov [^74] and facilitates release of oceanic water to Siberia region and into the atmosphere.

**13.12. ELECTROLYSIS IN AQUIFEROUS LAYERS**

Endothermic electrolysis process arises in the layers of mineralized underground waters. This process also forms the permafrost, of abyssal type as well, for example, in Poland and the Czech Republic [75; 76; 77]. On the contrary, boiling geysers are formed at anode edges of the water layers, for example, near the faults.

**13.13. ANODE HEATING ON SHELVES**

Anode electrolytic heating in the places where underground mineralized waters flow into the ocean, that is, on along perimeter of all continents, warms up waters of the shelves, in some places it even causes boiling. Probably, the electrolyte nature of the seawater will stimulate the process.

**13.14. DISINTEGRATION OF GAS HYDRATES**

Deposits of gas hydrates are located along the continents' edges. The reason of their formation can be that pressure in the sea and bowels in identical depth differs 2.5 times, and natural gases of the continent are squeezed out in the sea direction, as paste from a tube. Moreover, there are already conditions in the sea for formation gas hydrates from the gas. Where exactly they would lay down, depends on the direction of the currents.

Fig. 38. Dislocation of gas hydrates [^78]

<!-- Local
[![](Step by step_files/36D0B3D0B8D0B4D180D0B0D182D18B.jpg#clickable)](Step by step_files/36%2B%25D0%25B3%25D0%25B8%25D0%25B4%25D1%2580%25D0%25B0%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-bFvxAQ2cu84/X80eyWjEy-I/AAAAAAAADow/w-fzb3WQ-d8vz3PlUy491cf9tt3Dt6pWwCLcBGAsYHQ/s1031/36%2B%25D0%25B3%25D0%25B8%25D0%25B4%25D1%2580%25D0%25B0%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-bFvxAQ2cu84/X80eyWjEy-I/AAAAAAAADow/w-fzb3WQ-d8vz3PlUy491cf9tt3Dt6pWwCLcBGAsYHQ/s1031/36%2B%25D0%25B3%25D0%25B8%25D0%25B4%25D1%2580%25D0%25B0%25D1%2582%25D1%258B.jpg)

Anode electrolytic heating warms up nearby deposits of methane hydrates and starts chain reaction of their disintegration.

**13.15. SHELVE METHANE HYDRATES DISINTEGRATION **

There are three starting mechanisms of shelve methane hydrates disintegration:

- Anode electrolytic heating in the places where underground mineralized waters flow into the ocean;

- Dramatic pressure drop on the seabed owing to water decomposition and foaming;

- Direct release of electrons owing to electrical discharges going upward

**13.16. FORMATION OF GAS AND WATER FOAM**

The released hydrocarbons exceed the volume of the initial gas hydrate 160-180 times, and seawater foams even more intensely. Constant electrical discharges initiate the process of electrocracking of the natural gas released from hydrates [^79] with the formation of one share of acetylene and about three shares of hydrogen. The volume of the gas mass increases four times. In its turn, acetylene decomposes to carbon and hydrogen, and finally greatly overcooled gas and water foam increases in volume 600-700 times. All pointed out processes go with heat removal, and, besides, ice crystals are also formed in the ocean with the specific weight 9 % less than at water weight.

**13.17. NEAR-BOTTOM ONSET OF OCEANIC WATER**

Hardly ice crystals and products of gas hydrates disintegration would begin elevating upwards, their place would be occupied with dense and heavy waters from the nearby areas. Near-bottom onset of waters would start with the direction to the shelves. Its combination with the tidal effects of electromagnetic influence of the meteoric shower on the ocean saline [^73] would make Jusup Hizirov's effect [^74] show to the full and promote the release of disintegration products go upwards and to the land.

**13.18. RELEASE OF THE GAS AND WATER AND ICE SUSPENSION**

The gas and water and ice suspension is regularly thrown out by blows of periodic ballistic waves from the ocean to the land and into the atmosphere. Complete disintegration of constituent parts of methane hydrates under the influence of the electric discharge saves the planet both from the greenhouse effect and methane extinction.

**13.19. CAVITATION IN GAS AND WATER AND ICE SUSPENSION**

As speed of the ballistic waves is extremely high, the suspension thrown out by them on the land and into the atmosphere is filled with cavitation blisters, - the higher share of liquid water is present, the more blisters it has. Gases in cavitation blisters possess higher chemical aggressiveness, and their collapse causes the set of hydraulic impacts and destroys even steel blades of screw propellers of motor boats. It is the important possible factor explaining formation of landscapes which formation does not have an explanation for today.

**13.20. BURNING AND DETONATIONS**

On the borders of the rarefied atmosphere zone the displaced hydrogen and oxygen can both burn and detonate in the form of a detonating gas with formation of a large quantity of superfluous for atmosphere water that leads to 40-day rains and abnormal increase of mountain glaciers, and then to slides and avalanching.

The fact that three thousand created by person and perfectly well preserved subjects are found in Norway UNDER the GLACIER [^143], confirms this model perfectly well and allows to correct out-of-date ideas about the process chronology.

**13.20. THE SOURCE OF PURE GRAPHITE**

The products of methane hydrates' disintegration thrown out into the depression zone play a various role: hydrogen is ionized and carries out the role of proton gas, in particular, in the bowels; and carbon due to density separation of water and soil suspension and lack of enough oxygen drops out as it is, in the form of pure graphite. These deposits of graphite, dated by to the Glacial Age, are described, in particular, by James Kenneth [^80].

**Russian:**

[БАЛЛИСТИКА. ХОД СОБЫТИЙ]

**English:**

BALLISTICS. HOWEVER

**Russian:**

**14.1. СРЫВ БИОТЫ И ВОДЫ**

**English:**

**14.1. BIOTA AND WATER DISRUPTION**

**Russian:**

По мере приближения часть метеорных тел касается атмосферы, и начинаются баллистические эффекты. Поначалу баллистическая волна слабая, и ее силы хватает, чтобы сорвать вещества с наименьшим удельным весом -- вспененную воду и биоту. Уголь и океаническая вода ложатся в бассейн реки Лена.

**English:**

As we approach, some of the meteoroid bodies touch the atmosphere, and ballistic effects begin. At first, the ballistic wave is weak, and its strength is enough to disrupt the substances with the lowest specific gravity -- foamy water and biota. The coal and ocean water lodge in the Lena River basin.

**Russian:**

**14.2. ОБРАЗОВАНИЕ ЗОНЫ РАЗРЕЖЕНИЯ**

**English:**

**14.2. FORMATION OF A RAREFACTION ZONE**

**Russian:**

В сфере баллистического влияния метеорного потока создается устойчивая зона разрежения, переживающая колебания температуры и давления с той же частотой, с какой проходят отдельные метеоры потока.

**English:**

In the ballistic sphere of influence of the meteoroid stream creates a stable rarefaction zone, experiencing fluctuations in temperature and pressure with the same frequency with which the individual meteors of the stream pass.

**Russian:**

**14.3. КОЛЕБАНИЯ ДАВЛЕНИЯ**

**English:**

**14.3. PRESSURE FLUCTUATIONS**

**Russian:**

В условиях резкого колебания давления баротравмы получают даже те живые организмы, что находятся на удалении от метеорного потока. Идет периодический процесс возгонки, при котором возгоняются даже те вещества, что в обычном представлении являются нелетучими [^81]. Ослабляются грунты, разрушаются породы, особенно имеющие в составе воздух или воду.

**English:**

Under conditions of sharp pressure fluctuations, even those living organisms that are at a distance from the meteoroidal flux receive barotrauma. There is a periodic process of sublimation, in which even those substances that in the conventional view are non-volatile [^81]. Soils are weakened, and rocks, especially those containing air or water, are destroyed.

**Russian:**

**14.4. ВЗВЕСЬ В АТМОСФЕРЕ И СЕЛЬ НА ПОВЕРХНОСТИ**

**English:**

**14.4. SUSPENSION IN THE ATMOSPHERE AND MUD ON THE SURFACE**

**Russian:**

В зоне разрежения повисает выброшенная баллистической волной и взбалтываемая ударами периодических волн взвесь из воды, льда, пара, пыли, газов и продуктов возгонки -- сильно ионизированная и чрезвычайно подвижная. На поверхности взвесь ведет себя как селевая масса, так же чрезвычайно подвижная. Именно эта подвижность позволила сформировать гляциотектоническую структуру острова Новая Сибирь и подобные ей.

**English:**

In the rarefaction zone there hangs a suspension of water, ice, steam, dust, gases, and combustion products, strongly ionized and extremely mobile, ejected by a ballistic wave and agitated by impacts of periodic waves. On the surface, the suspension is an extremely mobile debris-flow mass. It is this mobility that allowed the formation of the glaciotectonic structure of New Syberia Island and similar ones.

**Russian:**

Рис. 39. Гляциотектоническая структура острова Новая Сибирь на участке Деревянных гор [^82].

**English:**

Fig. 39. Glaciotectonic structure of New Syberia Island in the Wooden Mountains [^82] section.

<!-- Local
[![](Step by step_files/36D09FD0BBD0B0D181D182D18B.jpg#clickable)](Step by step_files/36%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-T2eS36glrNA/XlE40E_wXCI/AAAAAAAADGU/zgH31KFQVog2CiewGSPkekwKrlNffCHFQCLcBGAsYHQ/s1600/36%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-T2eS36glrNA/XlE40E_wXCI/AAAAAAAADGU/zgH31KFQVog2CiewGSPkekwKrlNffCHFQCLcBGAsYHQ/s1600/36%2B%25D0%259F%25D0%25BB%25D0%25B0%25D1%2581%25D1%2582%25D1%258B.jpg)

**Russian:**

**14.5. ШЛИФОВКА ВАЛУНОВ И СКАЛ**

**English:**

**14.5. BOULDER AND ROCK GRINDING**

**Russian:**

Почти все инициированные электроразрядами процессы идут с отбором тепла, поэтому взвесь охлаждена, возможно, до космических температур. При ударных воздействиях переохлажденной взвеси на ландшафт промороженные выступы шелушатся, а отдельные камни (валуны) и целые скальные массивы полируются (зеркала скольжения ледников). Захороненные переохлажденной взвесью мамонты промерзают чрезвычайно быстро.

**English:**

Almost all processes initiated by electric discharges proceed with heat extraction, so the suspension is cooled, perhaps, to cosmic temperatures. During impact impacts of supercooled suspension on the landscape, the frozen protrusions are flaked, and individual stones (boulders) and entire rock massifs are polished (glacier slip mirrors). Mammoths buried by supercooled suspension freeze extremely fast.

**Russian:**

Дополнительным фактором, влияющим на создание ледниковых зеркал скольжения, должна быть кавитация, создающая при схлопывании пузырьков множество гидравлических ударов и отдающая газы, отличающиеся повышенной химической агрессивностью.

**English:**

An additional factor influencing the creation of glacial sliding mirrors must be cavitation, which creates many hydraulic shocks when the bubbles collapse and gives off gases that are chemically aggressive.

**Russian:**

Кавитация внутри потоков взвеси могла сыграть заметную роль и при формировании ложа ленточных озер и низин грядово-ложбинных комплексов.

**English:**

Cavitation within sediment flows may have played an appreciable role in the formation of ribbon lake beds and lowland ridgy-lobbin complexes as well.

**Russian:**

**14.6. МАССОВАЯ ОКАТКА ГАЛЬКИ**

**English:**

**14.6. MASS SCALE OF PEBBLES**

**Russian:**

При движении на поверхности взвесь ведет себя как высокоскоростная селевая масса. При толщине слоя даже в несколько метров давление в нижних слоях достаточное, чтобы в считанные часы отшлифовать взаимным трением под давлением до округлого состояния осколок любой породы.

**English:**

When moving on the surface the suspension behaves like a high velocity mudflow mass. When the layer is even several meters thick, the pressure in the lower layers is enough to grind a fragment of any rock to a rounded state by mutual friction under pressure in a few hours.

**Russian:**

**14.7. СЕПАРАЦИЯ ВЕЩЕСТВ**

**English:**

**14.7. SEPARATION OF SUBSTANCES**

**Russian:**

Неизбежна сепарация водно-грунтовой взвеси, например, с отделением морской соли от воды. Взвесь будет оседать в зависимости от плотности и размеров частиц и текущего направления воздушных потоков. Возможно, именно так образовались некоторые месторождения соли [^83].

**English:**

Inevitably, water-soil sediment separation is inevitable, e.g. with separation of sea salt from water. The suspended sediment will be deposited depending on the density and size of the particles and the current direction of the air currents. This is probably how some salt deposits were formed [^83].

**Russian:**

**14.8. СМЕЩЕНИЕ ВЗВЕСИ НА ЗАПАД**

**English:**

**14.8. SUSPENSION SHIFT TO THE WEST**

**Russian:**

В силу вращения Земли взвесь сносит на запад, что формирует грядовые комплексы Казахстана, Южного Урала и Южной России в целом. Скорость сноса зависит от скорости вращения поверхности Земли на данной широте, поэтому широтная ориентация комплексов плавно меняется от 90° у Ямала до 0° у Маныча. Причина: скорость выброса взвеси на сушу от 75° к 45° широты падает до нуля, а скорость вращения поверхности Земли вырастает с 435 до 1189 км в час.

**English:**

Due to the rotation of the Earth, the sediment is drifting westward, which forms the ridge complexes of Kazakhstan, the Southern Urals, and southern Russia as a whole. The rate of drift depends on the rate of rotation of the Earth's surface at a given latitude, so the latitudinal orientation of the complexes changes smoothly from 90° near Yamal to 0° near Manych. Reason: speed of slurry release to the land from 75° to 45° of latitude falls to zero, and speed of Earth surface rotation grows from 435 to 1189 km per hour.

**Russian:**

Рис. 40. Широтная ориентация грядово-ложбинных комплексов

**English:**

Fig. 40. Latitudinal orientation of ridge-and-lodge complexes

<!-- Local
[![](Step by step_files/37D0A1D0BDD0BED181.jpg#clickable)](Step by step_files/37%2B%25D0%25A1%25D0%25BD%25D0%25BE%25D1%2581.jpg)
-->

[![](https://1.bp.blogspot.com/-ehLiOeAO0Xg/XlE4-1XI6OI/AAAAAAAADGY/7bBa3V_Sw_4RHXDLZk3DDPr9vGhJy34zwCLcBGAsYHQ/s1600/37%2B%25D0%25A1%25D0%25BD%25D0%25BE%25D1%2581.jpg#clickable)](https://1.bp.blogspot.com/-ehLiOeAO0Xg/XlE4-1XI6OI/AAAAAAAADGY/7bBa3V_Sw_4RHXDLZk3DDPr9vGhJy34zwCLcBGAsYHQ/s1600/37%2B%25D0%25A1%25D0%25BD%25D0%25BE%25D1%2581.jpg)

**Russian:**

Самые западные грядовые комплексы видны на Тамани, а самые западные наносные грунты охряных тонов есть даже в Румынии и Болгарии.

**English:**

The westernmost ridge complexes can be seen in Taman, and the westernmost ochre-colored sedimentary soils are even found in Romania and Bulgaria.

**Russian:**

У Таймыра и восточнее угол наклона следов взвеси более 90°. Все вместе эти следы позволяют рассчитать характеристики движения взвеси с достаточной точностью.

**English:**

At Taimyr and to the east, the angle of inclination of the sediment traces is more than 90°. Taken together, these tracks allow the characteristics of suspended sediment movement to be calculated with sufficient accuracy.

**Russian:**

**14.9. ФОРМИРОВАНИЕ ЛАНДШАФТА САХАРЫ**

**English:**

**14.9. SAHARA LANDSCAPE FORMATION**

**Russian:**

Аналоги ленточных озер и особенно грядово-ложбинных комплексов есть и в Сахаре; Гугл-карты позволяют их рассмотреть, а предлагаемая теория - в деталях объяснить порядок образования. Для начала осмотрим окрестности вулкана Эми-Куси в республике Чад; первое, что бросается в глаза, - грядово-ложбинные комплексы, огибающие вулкан по кривой.

**English:**

The analogues of ribbon lakes and especially ridge-lobule complexes are also found in the Sahara; Google maps allow us to consider them and the proposed theory allows us to explain in detail the order of their formation. First, let us examine the vicinity of the volcano Amy-Kusi in the Republic of Chad; the first thing that catches the eye are the ridge-and-lobule complexes enveloping the volcano in a curve.

**Russian:**

Рис. 41. Вулкан Эми-Куси

**English:**

Figure 41. Amy-Kusi volcano

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B00.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B0.png)
-->

[![](https://1.bp.blogspot.com/-Vp98DNhj-lE/X3omjS0ywxI/AAAAAAAADdk/6PvzZiH8nhYMScHA9Mzl9tGqLC7Hu3G3gCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B0.png#clickable)](https://1.bp.blogspot.com/-Vp98DNhj-lE/X3omjS0ywxI/AAAAAAAADdk/6PvzZiH8nhYMScHA9Mzl9tGqLC7Hu3G3gCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B0.png)

**Russian:**

Рассмотрим эти комплексы поближе. Плавное искривление налицо; такое же искривление, причем, в том же порядке - с севера на юго-запад, а затем и на запад, есть и у грядово-ложбинных комплексов России.

**English:**

Let us take a closer look at these complexes. The smooth curvature is evident; the same curvature, and, in the same order - from north to southwest, and then to the west, is in the ridge-tomb complexes of Russia.

**Russian:**

Рис. 42. Грядово-ложбинные комплексы в окрестностях вулкана Эми-Куси

**English:**

Fig. 42. Ridge-and-lodge complexes in the vicinity of the Emi-Kusi volcano

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B01.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B1.png)
-->

[![](https://1.bp.blogspot.com/-HgqFOnp5Jy8/X3onhUrkegI/AAAAAAAADds/-bO-K0816ZMTAtilLMFuvVRLnBMYTNA9QCLcBGAsYHQ/s1904/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B1.png#clickable)](https://1.bp.blogspot.com/-HgqFOnp5Jy8/X3onhUrkegI/AAAAAAAADds/-bO-K0816ZMTAtilLMFuvVRLnBMYTNA9QCLcBGAsYHQ/s1904/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B1.png)

**Russian:**

Рассмотрим еще ближе. Роль гряд выполняют скальные массивы, и эти массивы имеют "тени" с юго-запада. Вызвавший эрозию скал фактор двигался с северо-востока, поэтому части скального массива, укрытые высотами, лучше сохранились с "подветренной" стороны.

**English:**

Let us take a closer look. The role of ridges is performed by rock massifs, and these massifs have "shadows" from the south-west. The factor that caused erosion of the rocks moved from the northeast, so parts of the rock massif, sheltered by heights, were better preserved from the "leeward" side.

**Russian:**

Рис. 43. Эрозионные "тени"

**English:**

Figure 43: Erosion "shadows

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B02.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B2.png)
-->

[![](https://1.bp.blogspot.com/-qbNHuNuMBM4/X3on6gM5uSI/AAAAAAAADd4/Lb-y5ZRJkvQGfcMkSFr4FTNq5-YxvFXWACLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B2.png#clickable)](https://1.bp.blogspot.com/-qbNHuNuMBM4/X3on6gM5uSI/AAAAAAAADd4/Lb-y5ZRJkvQGfcMkSFr4FTNq5-YxvFXWACLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B2.png)

**Russian:**

Остается вопрос, почему эти комплексы искривлены. Самое простое предположение: ветер, несущий песок-абразив, огибает препятствия. Нечто подобное мы видим на кольцевой структуре юго-восточнее вулкана Эми-Куси: вблизи кольца гряды плавно изгибаются.

**English:**

The question remains as to why these complexes are warped. The simplest assumption is that the wind, carrying abrasive sand, bends around obstacles. We see something similar at the ring structure southeast of the Emi-Kusi volcano: the ridges curve smoothly near the ring.

**Russian:**

Рис. 44. Легкое обтекание кольцевой структуры.

**English:**

Fig. 44. Light streamlining of a ring structure.

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B03.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B3.png)
-->

[![](https://1.bp.blogspot.com/-VrogsAghA50/X3os-pM-SiI/AAAAAAAADeE/bFc3EI3sI24sh7_QMuaMyTnS0EpvrP4LgCLcBGAsYHQ/s1917/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B3.png#clickable)](https://1.bp.blogspot.com/-VrogsAghA50/X3os-pM-SiI/AAAAAAAADeE/bFc3EI3sI24sh7_QMuaMyTnS0EpvrP4LgCLcBGAsYHQ/s1917/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B3.png)

**Russian:**

Если смотреть издалека, то видно, что точно так же гряды искривляются вокруг всего горного массива, в юго-восточной части которого расположен вулкан Эми-Куси.

**English:**

If you look from afar, you can see that exactly the same ridges curve around the entire mountain range, in the southeastern part of which the Amy-Kusi volcano is located.

**Russian:**

Рис. 45. Обтекание горного массива

**English:**

Fig. 45. Streamlining of a mountain massif

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B04.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B4.png)
-->

[![](https://1.bp.blogspot.com/-JAn0TCmS_KQ/X3ot3icCu5I/AAAAAAAADeM/k-n42VNZQzACiOcdw2fWTezNWCLybpHKwCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B4.png#clickable)](https://1.bp.blogspot.com/-JAn0TCmS_KQ/X3ot3icCu5I/AAAAAAAADeM/k-n42VNZQzACiOcdw2fWTezNWCLybpHKwCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B4.png)

**Russian:**

Основное препятствие для теории о ветровом источнике образования гряд - карта основных сезонных ветров, среди которых расположенных вдоль широты нет.

**English:**

The main obstacle for the theory about the wind source of ridge formation is the map of the main seasonal winds, among which those located along the latitude are absent.

**Russian:**

Рис. 46. Сезонные ветры Африки

**English:**

Figure 46. Seasonal winds in Africa

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B0.jpg#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-7wZ5q3Q_2xA/X3ovLSa7BoI/AAAAAAAADeY/_Q7pne3SrNoYgCX29QolOhbJRfuk4qEsQCLcBGAsYHQ/s569/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-7wZ5q3Q_2xA/X3ovLSa7BoI/AAAAAAAADeY/_Q7pne3SrNoYgCX29QolOhbJRfuk4qEsQCLcBGAsYHQ/s569/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0.jpg)

**Russian:**

В реальном ландшафте Сахары идущие вдоль широты или с небольшим отклонением грядово-ложбинные комплексы представлены чрезвычайно хорошо, и они всегда в южной части пустыни. Такая ориентация не отвечает ни пассатам, ни местным ветрам ни при нынешнем положении полюса, ни при полюсе в Гренландии. Вывод: происхождение комплексов не связано с ветровой эрозией.

**English:**

In the real Saharan landscape, the ridge-lobule complexes running along the latitude or with a small deviation are extremely well represented, and they are always in the southern part of the desert. This orientation does not correspond to either the trade winds or local winds neither at the present pole position, nor at the pole in Greenland. Conclusion: the origin of the complexes is not related to wind erosion.

**Russian:**

Рис. 47. Направление грядово-ложбинных комплексов Сахары

**English:**

Figure 47. Direction of Saharan ridge-and-lodge complexes

<!-- Local
[![](Step by step_files/D0A1D0B0D185D0B0D180D0B06.png#clickable)](Step by step_files/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B6.png)
-->

[![](https://1.bp.blogspot.com/-NypuF2BrZSg/X3o1fsK-DvI/AAAAAAAADek/t5uClEzLjFUObfa7XGdy9A-3YsRaKFxSQCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B6.png#clickable)](https://1.bp.blogspot.com/-NypuF2BrZSg/X3o1fsK-DvI/AAAAAAAADek/t5uClEzLjFUObfa7XGdy9A-3YsRaKFxSQCLcBGAsYHQ/s1920/%25D0%25A1%25D0%25B0%25D1%2585%25D0%25B0%25D1%2580%25D0%25B0%2B6.png)

**Russian:**

Отпадает и регулярно выдвигаемая гипотеза о цунами; в силу ее кратковременности цунами не способна проточить скалы, причем, рядами с примерно одинаковым шагом меж собой. Плюс, для водной эрозии требуется значительное время, а предположить постоянный, на протяжении многих лет мощный водный поток через Сахару с именно этой ориентацией сложно.

**English:**

The regularly voiced hypothesis of tsunami also disappears; because of its short duration tsunami is not capable to flow through rocks, moreover, in rows with approximately equal step between each other. Plus, it takes considerable time for water erosion, and it is difficult to assume constant, for many years, powerful water flow through Sahara with exactly this orientation.

**Russian:**

Причина примерно одинакового шага между грядами может быть объяснена волновыми свойствами вызвавшего эрозию потока. Именно волновые свойства потоков создают ритмичное расположение барханов, песчаных кос в Азовском море и причудливых форм наледи зимой в прибрежных районах Причерноморья.

**English:**

The reason for approximately the same pitch between the ridges can be explained by the wave properties of the stream that caused the erosion. It is the wave properties of the flows that create the rhythmic arrangement of barchans, sand spits in the Azov Sea and bizarre forms of ice in winter in the coastal areas of the Black Sea.

**Russian:**

Причина податливости скальных массивов кроется в том, что акт эрозии совпал с масштабным выбросом лавовых либо грязевых потоков из африканских вулканов. Лаву резало "по-живому", и там, где верхний застывший слой был снят, нижние пока еще мягкие слои снимались многократно быстрее. Обратите внимание, грядово-ложбинные комплексы Сахары явно тяготеют к вулканам и скальным массивам, имеющим в Гугл-картах те же цветовые и структурные характеристики, что и массивы явно вулканического характера.

**English:**

The reason of suppleness of rock massifs is that the act of erosion has coincided with large eruption of lava or mud streams from African volcanoes. Lava was cut "alive", and where the upper frozen layer was removed, the lower still soft layers were removed many times faster. Note, the ridge-and-track complexes of the Sahara obviously gravitate to volcanoes and rock massifs which have the same color and structural features as those of obviously volcanic nature on the Google Maps.

**Russian:**

Ответ на вопрос, что собой представляла среда, создавшая эти комплексы, содержится в этом тексте чуть выше: это главным образом продукты разрушения метеоритов, уже не имеющие собственной космической скорости (до 72 км/сек) и оседающие на Землю со сносом против направления вращения планеты со скоростью ее вращения на этой широте. В случае с Сахарой (20 градусов широты) это порядка 1600 км в час.

**English:**

The answer to the question of what was the environment that created these complexes is contained in this text just above: it is mainly the products of meteorite destruction, no longer having their own space velocity (up to 72 km/sec) and deposited on the Earth with drift against the direction of the planet with the speed of its rotation at this latitude. In the case of the Sahara (20 degrees of latitude) this is about 1600 km per hour.

**Russian:**

Что такое 1600 км в час? При объемной обработке стекла пескоструйной машиной достаточно скорости 650 км в час, а гидроабразивная резка стали требует скорости выброса струи 3600 км в час. То есть, существующий ландшафт Сахары при оседании метеорных обломков, а главное, метеорного песка со скоростью 1600 км в час мог быть сформирован в считанные часы.

**English:**

What is 1600 km per hour? For volumetric blasting of glass with a sandblaster 650 km per hour is sufficient, and waterjet cutting of steel requires a jet velocity of 3600 km per hour. That is, the existing landscape of the Sahara when meteoric debris, and more importantly, meteoric sand, settles at a speed of 1600 km per hour could be formed in a matter of hours.

**Russian:**

Ну, а поскольку, согласно недавнему исследованию в Абу-Хурейре, Сирия, температура, как минимум, части этой песчано-обломочной среды составляла от 1720 ° C до > 2200 ° C [^144], возможности формирования ландшафта у этой "песчано-плазменной бури" были по нашим меркам почти безграничны.

**English:**

Well, since, according to a recent study in Abu Hureira, Syria, the temperature of at least part of this sandy-clastic environment ranged from 1720 ° C to > 2200 ° C [^144], the landscape formation possibilities for this "sand-plasma storm" were almost limitless by our standards.

**Russian:**

Остается последний вопрос: почему поток огибал скальные массивы вулканического происхождения. Ответ видится один: задетая метеорным электроразрядом магма изменила характеристики, в том числе, давление, и вулканы активно извергались. Зона извержения вулкана создавала собственное давление газов вверх, отклонявшее проходящий над ней поток песчано-обломочной смеси. Дополнительная гипотеза: недавно выброшенная магма все еще двигалась по склонам в стороны, сдвигая уже прорезанные гряды.

**English:**

The last question remains: why did the flow skirt the rock massifs of volcanic origin. The answer seems to be one: the magma affected by the meteoric electrical discharge changed its characteristics, including pressure, and volcanoes actively erupted. The volcanic eruption zone created its own upward gas pressure which deflected the flow of sandy debris mixture passing over it. Additional hypothesis: recently ejected magma was still moving along the slopes sideways, shifting the already cut ridges.

**Russian:**

При всей схожести параметров грядово-ложбинных комплексов России и Сахары и, несмотря на то, что они сформированы в одни и те же пол-суток, среда, их сформировавшая различна: в России это взвесь из льда, газа и грунта, а в Сахаре - обломочный и песчаный метеоритный материал. Плавное изменение направления грядово-ложбинных комплексов в обоих случаях вызвано разной скоростью вращения поверхности Земли на разных широтах: чем ближе к экватору, тем выше скорость и, соответственно, значительнее снос падающих тел в западном направлении.

**English:**

Despite the similarity of the parameters of the ridge-lobule complexes of Russia and the Sahara, and despite the fact that they were formed in the same half-days, the environment that formed them is different: in Russia it is a suspension of ice, gas and soil, while in the Sahara - the debris and sandy meteoritic material. A smooth change in the direction of the ridge-lobule complexes in both cases is caused by different velocities of the Earth's surface rotation at different latitudes: the closer to the equator, the higher the speed and, accordingly, a more significant drift of falling bodies in the western direction.

**Russian:**

**14.10. ВАРИАЦИИ ДВИЖЕНИЯ ВЗВЕСИ**

**English:**

**14.10. VARIATIONS OF SUSPENSION MOVEMENT**

**Russian:**

Плотность восточных и западных частей метеорного потока менялась произвольно, то есть, были произвольные и многократные удары по ослабленному грунту со всех сторон, что создало иллюзию нескольких разных по характеристикам оледенений. Обязательны и разрывы метеорного потока, и смена каких-то его характеристик -- от величины тел до величины среднего заряда, что определенно отразилось и в его следах на Земле. Слои формируются произвольно, и наукой делаются выводы о чрезвычайно быстрых наступлениях и отступлениях ледников.

**English:**

The density of the eastern and western parts of the meteor shower varied arbitrarily, i.e., there were arbitrary and repeated impacts on the weakened ground from all sides, which created the illusion of several different characteristics of glaciation. The discontinuities of the meteor shower and the change of some of its characteristics -- from the size of the bodies to the size of the average charge, which was definitely reflected in its traces on the Earth. Layers are formed arbitrarily, and science draws conclusions about extremely rapid advances and retreats of glaciers.

**Russian:**

**14.11. ОБЩИЙ ХАРАКТЕР ВЗВЕСИ**

**English:**

**14.11. GENERAL NATURE OF THE SUSPENSION**

**Russian:**

Грядовые комплексы прокладываются с игнорированием естественных препятствий на местности, а валуны заносятся в гору, что представляет трудности для объяснения. На деле, ни гипотеза об оледенении, ни гипотеза о потопе по отдельности не дадут точного описания происходившего: взвесь ведет себя и как ледовый щит, и как водно-грязевой поток, и как снежно-песчаная буря.

**English:**

Ridge complexes are laid out ignoring the natural obstacles in the terrain, and boulders are brought uphill, which presents difficulties for explanation. In fact, neither the glaciation hypothesis, nor the flood hypothesis separately will provide an accurate description of what happened: the slurry behaves both as an ice sheet, as a water-mud stream, and as a snow-sand storm.

**Russian:**

То, как в итоге сформировались ячейки мерзлоты, лучше всего указывает на ее начальную консистенцию: жидкую и склонную к расслоению.

**English:**

The way permafrost cells eventually formed is best indicated by its initial consistency: liquid and prone to stratification.

**Russian:**

Рис. 48. Ячейки вечной мерзлоты и высохшая земля

**English:**

Figure 48. Permafrost cells and dried ground

<!-- Local
[![](Step by step_files/38D0AFD187D0B5D0B9D0BAD0B8.png#clickable)](Step by step_files/38%2B%25D0%25AF%25D1%2587%25D0%25B5%25D0%25B9%25D0%25BA%25D0%25B8.png)
-->

[![](https://1.bp.blogspot.com/-sAAkMFxMigU/XlE5IFT76tI/AAAAAAAADGg/c7s07SRE9fEzw99kaZUdd0YPIXLuGyaNQCLcBGAsYHQ/s1600/38%2B%25D0%25AF%25D1%2587%25D0%25B5%25D0%25B9%25D0%25BA%25D0%25B8.png#clickable)](https://1.bp.blogspot.com/-sAAkMFxMigU/XlE5IFT76tI/AAAAAAAADGg/c7s07SRE9fEzw99kaZUdd0YPIXLuGyaNQCLcBGAsYHQ/s1600/38%2B%25D0%25AF%25D1%2587%25D0%25B5%25D0%25B9%25D0%25BA%25D0%25B8.png)

**Russian:**

**14.12. СКОРОСТЬ ОЛЕДЕНЕНИЯ**

**English:**

**14.12. GLACIATION RATE**

**Russian:**

Промерзание почвы и озер наступает быстро, менее, чем за полгода, что и подтверждается исследованием изотопов и погибшей биоты.

**English:**

Freezing of soils and lakes occurs quickly, in less than half a year, as evidenced by the study of isotopes and dead biota.

**Russian:**

**14.13. НЕОБУГЛЕННАЯ БИОТА**

**English:**

**14.13. UNCHARGED BIOTA**

**Russian:**

Часть леса и плейстоценовой фауны захоронена без признаков обугливания, что объяснимо неоднородностью и несинхронностью проходивших на местности процессов.

**English:**

Part of the forest and Pleistocene fauna is buried without signs of charring, which can be explained by heterogeneity and non-synchronous processes that took place in the area.

**Russian:**

**14.14. ОЛЕДЕНЕНИЕ В ТРОПИКАХ**

**English:**

**14.14. GLACIATION IN THE TROPICS**

**Russian:**

Нет препятствий для повторения частей этой схемы и в тропических регионах, просто в меньших масштабах. Это объясняет «оледенение» без изменения климатической зоны и уничтожения всей тропической флоры и фауны целиком.

**English:**

There is no obstacle to repeating parts of this pattern in tropical regions as well, just on a smaller scale. This explains "glaciation" without changing the climatic zone and destroying all tropical flora and fauna entirely.

**Russian:**

**14.15. ЛЕТОПИСНОЕ СВИДЕТЕЛЬСТВО**

**English:**

**14.15. ANNALISTIC EVIDENCE**

**Russian:**

Живых очевидцев инцидента быть не должно, однако на расстоянии кое-что описано очень точно: резкое похолодание, падение давления, электроразряды, ударная волна, вырождение ударной волны в звуковую, эффект Прандтля-Глоерта со взрывной конденсацией атмосферной влаги в виде ливня и в условиях похолодания в виде града.

**English:**

There should be no living witnesses to the incident, but from a distance something is described very accurately: a sharp cooling, pressure drop, electrical discharges, shock wave, shock wave degeneration into a sound wave, Prandtl-Gloert effect with explosive condensation of atmospheric moisture in the form of downpour and in conditions of cooling in the form of hail.

**Russian:**

**217 г. Битва при Плацентии**. Дождь и ветер хлестали пунийцев прямо в лицо и с такой силой, что они... падали наземь... ветер захватывает им дыхание и щемит грудь. Вдруг над их головами застонало, заревело, раздались ужасающие раскаты грома, засверкали молнии... грянул ливень, а ветер подул ещё сильнее. Тучи... стали сыпать градом... за градом последовал такой сильный мороз, что, если кто в этой жалкой куче людей и животных хотел приподняться и встать, он долго не мог этого сделать. В продолжение двух дней оставались они на этом месте, как бы в осаде; погибло много людей, много вьючных животных, а также и семь слонов

**English:**

**217 г. The battle of Placentia**. The rain and wind whipped the Puni men right in the face and with such force that they fall to the ground... the wind catching their breath and crushing their chests. Suddenly above their heads there was a wail, a roar, a terrifying roar of thunder, a flash of lightning... a downpour came, and the wind blew even harder. The clouds... began to rain hail... The hail was followed by such a severe frost that if anyone in this miserable pile of people and animals wanted to get up and stand up, he could not do it for a long time. During two days they remained on this place as if in siege; many people died, many pack animals, and also seven elephants

**Russian:**

**[ЗАВЕРШЕНИЕ МЕТЕОРНОГО ИНЦИДЕНТА]**

**English:**

**[METEOR INSIDE] **

**Russian:**

**15.1. ЗАТЯЖНЫЕ АТМОСФЕРНЫЕ ОСАДКИ**

**English:**

**15.1. PROLONGED ATMOSPHERIC PRECIPITATION**

**Russian:**

Эффект Прандтля-Глоерта, сопровождает скоростной уход зоны разрежения и конденсирует выброшенную вверх и лишнюю для атмосферы влагу.

**English:**

The Prandtl-Gloert effect, accompanies the velocity escape of the rarefaction zone and condenses the moisture ejected upwards and excess to the atmosphere.

**Russian:**

396-м до Р. Х. в Риме снег шел 40 дней.

**English:**

396 B.C. It snowed in Rome for 40 days.

**Russian:**

831 г. Идет дождь в продолжение 40 дней

**English:**

831 It rains for 40 days

**Russian:**

1432 г. Снег падал в Ливонии 40 дней подряд

**English:**

1432 Snow fell in Livonia for 40 consecutive days

**Russian:**

1454 г. Выпал снег в течение 40 дней к югу от реки Янцзы

**English:**

1454 Snow fell for 40 days south of the Yangtze River

**Russian:**

1636 г. В Испании в течение 40 дней шел дождь

**English:**

1636 It rained in Spain for 40 days

**Russian:**

1161 г. В день св. Креста пошел снег на 25 четвертей (4,5 метра); а в Индии на 44 четвертей (7,92 метра). Вследствие этого умерли все рыбы, птицы и все роды животных.

**English:**

1161 On the day of St. Cross it snowed 25 quarters (4.5 meters); and in India 44 quarters (7.92 meters). As a consequence, all fish, birds, and all kinds of animals died.

**Russian:**

**15.2. УХОД ЗОНЫ РАЗРЕЖЕНИЯ**

**English:**

**15.2. LEAVING THE RAREFACTION ZONE**

**Russian:**

Поскольку воздух заполняет зону разрежения (местами вакуум) с огромной скоростью, это сопровождается не только выбросом переохлажденной влаги в атмосферу, но и ударным эффектом (выходная баллистическая волна). Именно в такой ситуации в Северном Причерноморье дикие и домашние животные могли быть сметены в одну кучу и коллективно вморожены в лед 9-метровой толщины.

**English:**

As the air fills the rarefaction zone (vacuum in places) with enormous velocity, this is accompanied not only by ejection of supercooled moisture into the atmosphere, but also by a shock effect (exit ballistic wave). It was in such a situation in the Northern Black Sea region that wild and domestic animals could be swept into one pile and collectively frozen into 9-meter-thick ice.

**Russian:**

л. м. 6255, р. х. 755. Северная часть моря на сто миль от берега превратилась в камень на тридцать локтей (13,89 метра) в глубину... Когда снег выпал на столь толстый лед, то толщина его увеличилась еще на двадцать локтей (9,26 метра), и море приняло вид суши... В феврале... этот лед Божьим велением разделился на куски, подобно горам высоким, которые сильным ветром принесены к Дафнусии и к Иерону, и в тесноте сгустились у города до Пропонтиды и до острова Абидоса, наполнили все море, чему мы сами были очевидцами, всходили на льдину человек тридцать товарищей и там играли. Находились там и дикие звери и домашние скоты уже мертвые.

**English:**

l. m. 6255, p. x. 755. The northern part of the sea a hundred miles from the shore turned to stone thirty cubits (13.89 metres) deep... When snow fell on such thick ice, its thickness increased by another twenty cubits (9.26 meters), and the sea took on the appearance of land... In February... This ice by the will of God was divided into pieces, like high mountains, which were brought by a strong wind to Dethnusias and to Hieron, and they crowded together at the city to the Propontid and to the island of Abydos, filling the whole sea, which we ourselves were witnesses to; thirty men went up onto the ice and played there. There were also wild beasts and domestic cattle already dead.

**Russian:**

л. м. 6255, р. х. 755. В том же году в марте месяце видимы были звезды падающие с небес в великом множестве, и все, смотря на это падение их, предполагали скончание настоящего века; при этом были столь великие жары, что пересохли даже источники

**English:**

l. m. 6255, p. x. 755. In the same year, in March, the stars were seen falling from the sky in great multitude, and everyone, looking at this falling of them, suggested the end of the present age; while there were such great heat that even the springs dried up

**Russian:**

Предварительно наросший лед в 13,89 метра рассмотрим ниже, а здесь интересно то, что названа потенциальная первопричина -- метеорный поток. Поскольку год начинался в марте, то метеоры прошли в первом месяце года, а месяц февраль, в котором лед уже принесло в Константинополь, - последний месяц этого же года. Пересыхание источников можно объяснить запущенными процессами электролиза в подземных водных слоях.

**English:**

The preliminary ice of 13.89 meters will be considered below, but here it is interesting that the potential root cause - a meteor shower - is named. Since the year began in March, the meteors passed in the first month of the year, and the month of February, in which the ice was already brought to Constantinople, is the last month of the same year. The drying up of the springs can be explained by the running processes of electrolysis in the underground water layers.

**Russian:**

**15.3. ФОРМИРОВАНИЕ КАМЕННОЙ ПЕНЫ**

**English:**

**15.3. STONE FOAM FORMATION**

**Russian:**

Выходная баллистическая волна производит масштабный выброс переохлажденной пыли, влаги и продуктов возгонки вверх -- вслед за уходящим фронтом волн. Проявится эффект Прандтля-Глоерта со взрывной конденсацией атмосферной влаги вокруг пылевых частиц и продуктов возгонки, что вызовет химически и физически активный грязевой ливень. Именно на этом этапе в Неваде и на Мангышлаке формируются идентичные ландшафты с окаменевшей пеной. Имеет смысл проверить, при каких условиях возгоняются вещества, прилипшие к внутренней поверхности ячеек.

**English:**

The exit ballistic wave produces a large-scale release of supercooled dust, moisture, and sublimation products upward following the outgoing wave front. The Prandtl-Glott effect with explosive condensation of atmospheric moisture around the dust particles and sublimation products will occur, causing a chemically and physically active mud shower. Exactly at this stage in Nevada and on Mangyshlak identical landscapes with fossilized foam are formed. It makes sense to check under what conditions the substances stuck to the inner surface of the cells are erupted.

**Russian:**

Рис. 49. Идентичный ландшафт в Неваде и на Мангышлаке [84; 85]

**English:**

Figure 49. Identical landscape in Nevada and Mangyshlak [84; 85]

<!-- Local
[![](Step by step_files/39D09DD0B5D0B2D0B0D0B4D0B0D09CD0B0D0BDD0B3D18BD188D0BBD0B0D0.jpg#clickable)](Step by step_files/39%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0%2B%25D0%259C%25D0%25B0%25D0%25BD%25D0%25B3%25D1%258B%25D1%2588%25D0%25BB%25D0%25B0%25D0%25BA.jpg)
-->

[![](https://1.bp.blogspot.com/-_cxKxxRt9LU/XlE5a8HGwPI/AAAAAAAADGs/a5oxkiie6xA6hJOnKcKj1FzeJsEVzDv-ACLcBGAsYHQ/s1600/39%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0%2B%25D0%259C%25D0%25B0%25D0%25BD%25D0%25B3%25D1%258B%25D1%2588%25D0%25BB%25D0%25B0%25D0%25BA.jpg#clickable)](https://1.bp.blogspot.com/-_cxKxxRt9LU/XlE5a8HGwPI/AAAAAAAADGs/a5oxkiie6xA6hJOnKcKj1FzeJsEVzDv-ACLcBGAsYHQ/s1600/39%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0%2B%25D0%259C%25D0%25B0%25D0%25BD%25D0%25B3%25D1%258B%25D1%2588%25D0%25BB%25D0%25B0%25D0%25BA.jpg)

**Russian:**

Именно это обстоятельство следует счесть совпадением, но относительно нынешнего полюса Невада и Мангышлак расположены поворотно-симметрично, примерно на одной широте и на противолежащей долготе. Столь же симметрично расположены и ленточные озера в Сибири и в США чуть севернее Великих озер, хотя в США они мельче.

**English:**

Exactly this circumstance should be considered a coincidence, but relative to the current pole Nevada and Mangyshlak are located rotationally symmetrical, approximately at the same latitude and on the opposite longitude. Equally symmetrically located are the ribbon lakes in Siberia and in the United States just north of the Great Lakes, although in the United States they are shallower.

**Russian:**

В летописном свидетельстве ниже интересно синхронное упоминание о грязи, оставленных обозах и 40-дневном ливне, - именно так и должно быть: пылевые частицы в атмосфере способствуют конденсации влаги.

**English:**

In the annalistic record below, the synchronous mention of mud, abandoned carts and a 40-day downpour is interesting - this is how it should be: dust particles in the atmosphere contribute to the condensation of moisture.

**Russian:**

728 г. войска мусульман сражались в течение 30 или даже 40 дней в грязи и под проливным дождём, из за чего кампания получила название «грязный поход». В итоге арабы одолели хазар 17 сентября 728 года. Когда арабы возвращались домой, они попали в хазарскую засаду, после чего просто бежали, бросив обозы.

**English:**

In 728 year armies of Moslems battled within 30 or even 40 days in a dirt and pouring rain, from what the campaign has received the name " dirty campaign ". In a result Arabs have overcome Khazarian on September 17, 728. When the Arabs were returning home, they were ambushed by the Khazars, after which they simply fled, abandoning the wagons.

**Russian:**

**[ПОСЛЕДСТВИЯ МЕТЕОРНОГО ИНЦИДЕНТА]**

**English:**

**[METEOR INDICATION] **

**Russian:**

**15.4. ЛЕДНИКОВО-ПОДПРУДНЫЕ ОЗЕРА**

**English:**

**15.4. GLACIAL-POND LAKES**

**Russian:**

В строгом соответствии с выводами науки формируются ледниково-подпрудные озера, но причина не ледник (в районе Карского моря толщиной 2,7 км), а сглаженный, покрытый промерзшими наносными осадками рельеф.

**English:**

Glacial subglacial lakes are formed in strict accordance with the conclusions of science, but the cause is not a glacier (in the region of the Kara Sea it is 2.7 km thick), but a smoothed out, covered with frozen drift sediments relief.

**Russian:**

Рис. 50. Последнее оледенение территории СССР [^86]

**English:**

Fig. 50. Recent glaciation of the USSR [^86]

**Russian:**

1 -- ледниковые покровы равнин и гор; 2 -- плавучие шельфовые ледники; 3 -- озера; 4 -- каналы стока талых вод; 5 -- направления их стока; 6 -- осушенные шельфы; 7 -- свободный от ледников океан. Числа у озер -- их уровни

**English:**

1 -- glacial covers of plains and mountains; 2 -- floating ice shelves; 3 -- lakes; 4 -- channels of melt water runoff; 5 -- directions of their runoff; 6 -- drained shelves; 7 -- ice-free ocean. Numbers at lakes -- their levels

<!-- Local
[![](Step by step_files/40D09ED0B7D0B5D180D0B0.jpg#clickable)](Step by step_files/40%2B%25D0%259E%25D0%25B7%25D0%25B5%25D1%2580%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-ez60MlmIpxM/XlE5obfC3lI/AAAAAAAADGw/dy8S4_i8IJ8wowOyPk-3-b6jffdYMnQXACLcBGAsYHQ/s1600/40%2B%25D0%259E%25D0%25B7%25D0%25B5%25D1%2580%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-ez60MlmIpxM/XlE5obfC3lI/AAAAAAAADGw/dy8S4_i8IJ8wowOyPk-3-b6jffdYMnQXACLcBGAsYHQ/s1600/40%2B%25D0%259E%25D0%25B7%25D0%25B5%25D1%2580%25D0%25B0.jpg)

**Russian:**

Относительно нынешнего географического полюса ледниково-подпрудный Мансийский бассейн расположен симметрично ледниково-подпрудному озеру Агассис в западном полушарии.

**English:**

Relative to the present geographic pole, the glacial-submerged Mansi Basin is located symmetrically to the glacial-submerged Lake Agassiz in the western hemisphere.

**Russian:**

Рис. 51. Ледниково-подпрудное озеро Агассис

**English:**

Figure 51. Glacial-pond lake Agassiz

<!-- Local
[![](Step by step_files/40.jpg#clickable)](Step by step_files/40.2%2BAgassiz.jpg)
-->

[![](https://1.bp.blogspot.com/-4E7WEWPUwLQ/XlPzHeJSNLI/AAAAAAAADMs/HvUdg8kvE98n0s7nMAor64_xfWKW56kWACLcBGAsYHQ/s1600/40.2%2BAgassiz.jpg#clickable)](https://1.bp.blogspot.com/-4E7WEWPUwLQ/XlPzHeJSNLI/AAAAAAAADMs/HvUdg8kvE98n0s7nMAor64_xfWKW56kWACLcBGAsYHQ/s1600/40.2%2BAgassiz.jpg)

**Russian:**

При подвижке оси вращения Земли будут заложены разломы, по которым сток в океаны в обоих полушариях возобновится. Плюс, кора над залежами углеводородов просядет, Берингия затонет, и появится знакомый нам по физическим картам Арктики рельеф.

**English:**

At shift of an axis of rotation of the Earth there will be laid faults on which the drain to oceans in both hemispheres will renew. Plus, the crust above deposits of hydrocarbons will subside, Beringia will sink, and the relief familiar to us on physical maps of the Arctic will appear.

**Russian:**

Русла главных сибирских рек ориентированы на нынешний полюс, - как и вторая группа линеаментов, что говорит о позднем периоде стока ледниково-подпрудных озер в арктические моря.

**English:**

The channels of the main Siberian rivers are oriented towards the present pole - as well as the second group of lineaments, which suggests a late period of glacial-pond lake outflow into the Arctic seas.

**Russian:**

**15.5. ФОРМИРОВАНИЕ ОЗЕРНЫХ КОНКРЕЦИЙ**

**English:**

**15.5. FORMATION OF LAKE NODULES**

**Russian:**

С запада от Каспийского моря и в Большом Бассейне в США формируются схожие «ледниково-подпрудные» водоемы с огромным количеством мелкодисперсных наносных масс на дне. Именно в этой одинаковой полужидкой среде синхронно формируются сферические конкреции.

**English:**

To the west of the Caspian Sea and in the Great Basin in the USA, similar "glacial-ponded" reservoirs with huge amounts of fine-dispersed sedimentary masses at the bottom are being formed. It is in this same semi-liquid medium that spherical nodules are formed synchronously.

**Russian:**

Рис. 52. Конкреции в Нью-Мексико и на Мангышлаке

**English:**

Figure 52. Concretions in New Mexico and Mangyshlak

<!-- Local
[![](Step by step_files/41D09AD0BED0BDD0BAD180D0B5D186D0B8D0B8.jpg#clickable)](Step by step_files/41%2B%25D0%259A%25D0%25BE%25D0%25BD%25D0%25BA%25D1%2580%25D0%25B5%25D1%2586%25D0%25B8%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-MIHnICKHP0U/XlE5uEEikxI/AAAAAAAADG4/h1joB3XxIgghSgCG4zESMgMJfqw1JSgLwCLcBGAsYHQ/s1600/41%2B%25D0%259A%25D0%25BE%25D0%25BD%25D0%25BA%25D1%2580%25D0%25B5%25D1%2586%25D0%25B8%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-MIHnICKHP0U/XlE5uEEikxI/AAAAAAAADG4/h1joB3XxIgghSgCG4zESMgMJfqw1JSgLwCLcBGAsYHQ/s1600/41%2B%25D0%259A%25D0%25BE%25D0%25BD%25D0%25BA%25D1%2580%25D0%25B5%25D1%2586%25D0%25B8%25D0%25B8.jpg)

**Russian:**

**15.6. НАНОСЫ КАК УСЛОВИЕ ЛАНДШАФТООБРАЗОВАНИЯ**

**English:**

**15.6. SEDIMENT AS A CONDITION OF LANDSCAPE FORMATION**

**Russian:**

Позже, при подвижке оси вращения Земли, когда наклон геоида изменится, воды сойдут, стащив за собой большую часть все еще полужидких наносов, и сформируют на симметричных широтах и долготах планеты совершенно идентичный ландшафт.

**English:**

Later, during the shift of the Earth's rotation axis, when the inclination of the geoid will change, water will run down, taking away with it the most part of still half-liquid deposits, and will form on the symmetric latitudes and longitudes of the planet absolutely identical landscape.

**Russian:**

Рис. 53. Ландшафт плато Устюрт и Невады

**English:**

Figure 53. Landscape of the Ustyurt Plateau and Nevada

<!-- Local
[![](Step by step_files/42D0A3D181D182D18ED180D182D09DD0B5D0B2D0B0D0B4D0B0.png#clickable)](Step by step_files/42%2B%25D0%25A3%25D1%2581%25D1%2582%25D1%258E%25D1%2580%25D1%2582%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0.png)
-->

[![](https://1.bp.blogspot.com/-GnONHq1UMSs/XlE50CMahSI/AAAAAAAADG8/4lI3T7EGUmclHIayWLWEvRlNN1a5G4ohwCLcBGAsYHQ/s1600/42%2B%25D0%25A3%25D1%2581%25D1%2582%25D1%258E%25D1%2580%25D1%2582%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0.png#clickable)](https://1.bp.blogspot.com/-GnONHq1UMSs/XlE50CMahSI/AAAAAAAADG8/4lI3T7EGUmclHIayWLWEvRlNN1a5G4ohwCLcBGAsYHQ/s1600/42%2B%25D0%25A3%25D1%2581%25D1%2582%25D1%258E%25D1%2580%25D1%2582%2B%25D0%259D%25D0%25B5%25D0%25B2%25D0%25B0%25D0%25B4%25D0%25B0.png)

**Russian:**

При том направлении смещения оси, что следует из карты почвенных зон В. Докучаева, полужидкие массы плато Устюрт сойдут в Каспийское море, а массы из Большого бассейна в США, видимо, - в Калифорнийский залив.

**English:**

In that direction of axial displacement, which follows from the map of soil zones of V. Dokuchaev, the semi-liquid masses of the Ustyurt Plateau will descend into the Caspian Sea, and the masses from the Great Basin in the USA, apparently, into the Gulf of California.

**Russian:**

**15.7. СНИЖЕНИЕ ИНСОЛЯЦИИ**

**English:**

**15.7. REDUCED INSOLATION**

**Russian:**

Выброшенная в стратосферу пыль надолго снижает инсоляцию, и это не дым от пожаров, который летописцы обычно отмечают.

**English:**

The dust ejected into the stratosphere reduces insolation for a long time, and it is not the smoke from fires that chroniclers usually note.

**Russian:**

44 г. до н.э. Солнце было затемнено на целый год, и воздух был холодным и туманным

**English:**

44 B.C. The sun was darkened for a whole year, and the air was cold and foggy

**Russian:**

137 г. Весь год Солнце являло печальный свет без лучей наподобие Луны

**English:**

137 All the year the Sun showed a sad light without rays like the Moon

**Russian:**

526 г. Солнце без лучей, с блеском, подобным луне, мерцало в продолжении целого года

**English:**

526 The sun without rays, with a lustre like the moon, flickered for a whole year

**Russian:**

536 г. В Месопотамии Солнце затемнялось 18 месяцев

**English:**

536 In Mesopotamia, the Sun dimmed for 18 months

**Russian:**

562 г. Солнце затмилось в продолжение 8 месяцев.

**English:**

562 The sun was eclipsed for 8 months.

**Russian:**

564 г. В течение 18 месяцев Солнце светило лишь по три часа в день

**English:**

564 For 18 months the Sun shone only three hours a day

**Russian:**

624 г. Солнце потемнело в месяце Арек до месяца Кагота

**English:**

624 The sun darkened in the month of Arek to the month of Kagota

**Russian:**

934 г. Солнце потеряло свой обычный свет в течение нескольких месяцев

**English:**

934 The sun lost its normal light for several months

**Russian:**

1371 г. Почти два месяца стояла по земле великая непроглядная мгла, нельзя было и за две сажени видеть человека в лицо

**English:**

1371 For almost two months there was a great impenetrable darkness over the land, it was impossible to see a man's face even two fathoms away

**Russian:**

1384 г. Мгла и помрачение, которые продолжались многие дни. Птицы не могли летать

**English:**

1384 A haze and gloom that lasted many days. The birds could not fly

**Russian:**

1601 г. Весь год, а также следующий год из за высотного дыма солнце и луна постоянно были красноватые, бледные и без блеска

**English:**

1601 г. The whole year, and the next year because of the high-altitude smoke the sun and the moon were constantly reddish, pale and without luster

**Russian:**

**15.8. ПЫЛЕВЫЕ ОСАДКИ**

**English:**

**15.8. DUST FALLOUT**

**Russian:**

Какая-то часть выброшенной в атмосферу мельчайшей пыли сделает несколько витков вокруг планеты, частью стянется в полярные области. Возможно, поэтому полярные почвы по количеству гумуса сопоставимы с украинскими. Наносы именно этого типа лежат на высотах слоями равномерной толщины, без признаков работы воды, чем вводят исследователей в смущение. Возможно, лёссовые отложения, содержащие мелкие обломки костей грызунов и мамонтов и до 50-60 % пузырьков воздуха, также -- следствие этого процесса.

**English:**

Some part of the fine dust emitted into the atmosphere will make several rotations around the planet, part of it will flock to the polar regions. Perhaps, that is why polar soils are comparable with Ukrainian soils by the amount of humus. The sediments of this very type lie at the heights in layers of uniform thickness, with no signs of water activity, and confuse researchers. Perhaps loess deposits, containing small fragments of rodent and mammoth bones and up to 50-60% air bubbles, are also the result of this process.

**Russian:**

Рис. 54. Лёссовые отложения

**English:**

Figure 54. Loess deposits

<!-- Local
[![](Step by step_files/43D0BBD0B5D181D181.jpg#clickable)](Step by step_files/43%2B%25D0%25BB%25D0%25B5%25D1%2581%25D1%2581.jpg)
-->

[![](https://1.bp.blogspot.com/-cKEMFw12enQ/XlE57HHPjrI/AAAAAAAADHA/rT98MDs5MZMeX9YANuBcIrnZAiUDPuxfgCLcBGAsYHQ/s1600/43%2B%25D0%25BB%25D0%25B5%25D1%2581%25D1%2581.jpg#clickable)](https://1.bp.blogspot.com/-cKEMFw12enQ/XlE57HHPjrI/AAAAAAAADHA/rT98MDs5MZMeX9YANuBcIrnZAiUDPuxfgCLcBGAsYHQ/s1600/43%2B%25D0%25BB%25D0%25B5%25D1%2581%25D1%2581.jpg)

**Russian:**

**15.9. НИЗКОТЕМПЕРАТУРНЫЕ ИЗОТОПЫ**

**English:**

**15.9. LOW TEMPERATURE ISOTOPES**

**Russian:**

Поскольку во взвеси закономерно присутствуют изотопы, указывающие на аномально низкие температуры, исследование изотопного состава именно на этот фактор и указывает, что приводит ученых к выводу о более низких средних температурах зимних месяцев.

**English:**

Since isotopes indicative of abnormally low temperatures are naturally present in the suspension, a study of the isotopic composition points to this very factor, leading scientists to conclude that the average temperatures of the winter months are lower.

**Russian:**

**15.10. «ПОТЕПЛЕНИЕ» И ГИБЕЛЬ ФАУНЫ**

**English:**

**15.10. "WARMING" AND THE DEMISE OF FAUNA**

**Russian:**

Поскольку по завершению выглядевшего, как Ледниковый период, метеорного инцидента на поверхности не остается признаков жизни, делается ожидаемый вывод о гибели плейстоценовой фауны в тесной связи с потеплением.

**English:**

Since no signs of life remain on the surface at the end of what looked like an Ice Age meteoric incident, the expected conclusion is that Pleistocene fauna have died in close association with warming.

**Russian:**

**15.11. ФОРМИРОВАНИЕ НАНОСНЫХ ОСТРОВОВ**

**English:**

**15.11. SEDIMENT ISLAND FORMATION**

**Russian:**

По завершению инцидента в поймах крупнейших рек Сибири формируются наносные быстро тающие острова и заторы из битумизированного леса на Новой Земле.

**English:**

At the end of the incident, sediment-filled, rapidly melting islands and bituminized forest jams are forming in the floodplains of major Siberian rivers on Novaya Zemlya.

**Russian:**

**15.12. ВОССТАНОВЛЕНИЕ ОКИСЛОВ НЕДР**

**English:**

**15.12. RECOVERY OF SUBSURFACE OXIDES**

**Russian:**

Ионизированный водород становится «протонным газом» с высочайшей проникающей способностью. В недрах идет эндотермическая реакция восстановления водородом окислов [^87]. Возникают рудопроявления, а породы промерзают.

**English:**

Ionized hydrogen becomes a "proton gas" with the highest penetrability. In the subsurface there is an endothermic reaction of hydrogen reduction of oxides [^87]. Ore occurrences arise and rocks freeze.

**Russian:**

**15.13. СИНТЕЗ НЕФТИ**

**English:**

**15.13. SYNTHESIS OF OIL**

**Russian:**

В результате электроразряда в залежах природного газа водород, как самый подвижный элемент, отрывается, а обрывки молекул образуют соединения, в которых водорода намного меньше, чем в природном газе, то есть, нефть. В научных кругах связь подземных гроз и образования нефти уже обсуждается [^88]. Реакция эндотермическая.

**English:**

As a result of electric discharge in natural gas deposits, hydrogen, as the most mobile element, breaks away, and fragments of molecules form compounds in which hydrogen is much less than in natural gas, i.e., oil. The connection between underground thunderstorms and oil formation has already been discussed in scientific circles [^88]. The reaction is endothermic.

**Russian:**

**15.14. РАЗЛОЖЕНИЕ МЕТАНГИДРАТОВ**

**English:**

**15.14. DECOMPOSITION OF METHANE HYDRATES**

**Russian:**

Метангидраты недр под действием электроразряда разлагаются.

**English:**

Methane hydrates of the subsoil are decomposed under the action of electric discharge.

**Russian:**

**15.15. ПРОСЕДАНИЕ ЗЕМНОЙ КОРЫ**

**English:**

**15.15. CRUSTAL SUBSIDENCE**

**Russian:**

Плотность нефти в два с лишним раза выше плотности жидкого природного газа, то есть, синтез нефти должен сопровождаться сокращением углеводородного слоя в объеме и проседанием коры. В результате прибрежная часть коры затонула и стала шельфом, что добавляет нюансов к процессу трансгрессии морей. Толщина слоев нефти в породах местами оценивается в 1100-1200 метров, что легко объясняет затопление берегов эпохи Доггерленда и Берингии на 8-135 метров. Чем ближе к зоне электроразряда (Арктика), тем затопление значительнее. При разложении метангидратов недр слои метангидратов потеряют в объеме около 11 %. Нынешние слои газовых гидратов имеют толщину 400-800 метров, то есть проседание коры при таком сценарии было бы заметным.

**English:**

The density of oil is more than twice that of liquid natural gas, which means that oil synthesis must be accompanied by reduction of the hydrocarbon layer in volume and crustal subsidence. As a result, the coastal part of the crust has sunk and become a shelf, which adds nuance to the process of seas transgression. The thickness of oil layers in the rocks is in some places estimated to be 1,100-1,200 meters, which easily explains the 8-135 meter flooding of the Doggerland and Beringian shores. The closer to the electric discharge zone (Arctic), the more significant the flooding. When the subsoil methane hydrates decompose, the methane hydrate layers will lose about 11% in volume. The current gas hydrate layers are 400-800 meters thick, i.e. crustal subsidence in such a scenario would be noticeable.

**Russian:**

При сокращении объема углеводородов недр, начнут образовываться каверны, возникнет очаговое падение давления, и распад газовых гидратов недр примет характер цепной реакции. Падение давления вызовет еще большее охлаждением пород.

**English:**

When the volume of subsurface hydrocarbons decreases, caverns will begin to form, a focal pressure drop will occur, and the decomposition of gas hydrates in the subsurface will take on the nature of a chain reaction. Pressure drop will cause even more cooling of rocks.

**Russian:**

**15.16. ВОЗНИКНОВЕНИЕ ВЕЧНОЙ МЕРЗЛОТЫ**

**English:**

**15.16. EMERGENCE OF PERMAFROST**

**Russian:**

Поскольку Земля выполняет функцию катода, теряющего тепло при электронной эмиссии [^89], почти все описанные выше процессы эндотермические, идущие с отбором тепла, что способствует быстрому по геологическим стандартам формированию вечной мерзлоты, в том числе и глубинной. Этот эффект вносит свой вклад и в сверхбыструю заморозку мамонтов.

**English:**

Since the Earth performs the function of a cathode losing heat during electron emission [^89], almost all the above processes are endothermic, proceeding with heat extraction, which contributes to rapid formation of permafrost, including deep permafrost, by geological standards. This effect also contributes to superfast mammoth freezing.

**Russian:**

**15.17. ДВА СЛОЯ МЕРЗЛОТЫ**

**English:**

**15.17. TWO LAYERS OF PERMAFROST**

**Russian:**

Слои современной и древней вечной мерзлоты могут залегать как слитно, так и порознь: подошва верхнего слоя на 50-100 метрах, а кровля верхнего -- на 80-200 метрах [^90], что может быть результатом двух разных проходов одного и того же метеорного потока над Арктикой.

**English:**

Layers of modern and ancient permafrost may occur both fused and separately: the bottom of the upper layer at 50-100 meters and the top of the upper layer at 80-200 meters{[^90], which may be the result of two different passages of the same meteor shower over the Arctic.

**Russian:**

**15.18. СИНТЕЗ ДЕЙТЕРИЯ И МУТАЦИИ**

**English:**

**15.18. DEUTERIUM SYNTHESIS AND MUTATIONS**

**Russian:**

В результате синтеза дейтерия начнутся мутации, но незначительные; большая часть мутаций связана со следующим этапом развития катастрофы.

**English:**

As a result of deuterium synthesis, mutations will begin, but minor ones; most of the mutations are associated with the next stage of catastrophe development.

**Russian:**

**15.19. ПРЕДПОСЫЛКИ НЕОЛИТИЧЕСКОЙ РЕВОЛЮЦИИ**

**English:**

**15.19. PREREQUISITES OF THE NEOLITHIC REVOLUTION**

**Russian:**

Этот этап катастрофы заложит часть предпосылок для неолитической революции: заложит мощные рудопроявления, местами раскидает золото и болотное железо прямо по земле, сформирует и сделает более доступными месторождения нефти и уничтожит большую часть плейстоценовой фауны, необратимо подорвав этим охотничьи угодья. Человек просто будет вынужден брать кормовые ресурсы под личный контроль: скотина и птица только под присмотром, а зерно и репа для скотины только на своем поле.

**English:**

This phase of the catastrophe will lay some of the prerequisites for the Neolithic Revolution: it will lay powerful ore occurrences, scatter gold and bog iron directly over the ground in places, form and make more accessible oil deposits and destroy most of the Pleistocene fauna, irreversibly undermining the hunting grounds with it. Man will simply be forced to take personal control of forage resources: livestock and poultry only under supervision, and grain and turnips for cattle only in his field.

**Russian:**

Запорожские археологи нашли на месте древнего поселения загоны для домашних животных, появившиеся 8200 лет назад [^91]. Эта дата чрезвычайно адекватна событиям эпохи и явно указывает на самые первые в истории цивилизации загоны.

**English:**

Archaeologists from Zaporozhye found pens for domestic animals on the site of an ancient settlement, which appeared 8200 years ago [^91]. This date is extremely adequate to the events of the era and clearly indicates the very first pens in the history of civilization.

**Russian:**

**РЕЗЮМЕ**

**English:**

**SUMMARY**

**Russian:**

Эти пункты отвечают на тот же круг вопросов, что и теория оледенения. Данные полевых исследований остаются неколебимы; меняется только командная теория. Неразрешенные вопросы разрешены, не требуются допущения вроде ледника высотой от 2,7 до 4,0 км, а главное, видна понятная динамика событий.

**English:**

These points answer the same range of questions as the glaciation theory. The field survey data remain unshaken; only the command theory changes. Unresolved issues are resolved, assumptions like a glacier with a height of 2.7 to 4.0 km are not required, and, most importantly, the understandable dynamics of events is visible.

**Russian:**

**[ТЕКТОГЕНЕЗ. ХОД СОБЫТИЙ]**

**English:**

**[TEXTOGENEZ. HOWEVER] **

**Russian:**

**16.1. ВОЗДЕЙСТВИЕ ЭЛЕКТРОРАЗРЯДА НА ДВИЖЕНИЕ МАГМЫ**

**English:**

**16.1. EFFECT OF ELECTRIC DISCHARGE ON MAGMA MOTION**

**Russian:**

Следует ожидать двойного воздействия электроразряда на магму: кратковременного изменения циркуляции магмы под воздействием электромагнитного поля метеорного потока и активизации новых геохимических процессов вследствие утраты магмой части электронов. В перспективе это ведет к целой серии событий:

**English:**

We should expect a double impact of electric discharge on magma: short-term changes in magma circulation under the influence of electromagnetic field of meteoroid flow and activation of new geochemical processes due to loss of magma part of electrons. In perspective, this leads to a whole series of events:

**Russian:**

- очаговому изменению плотности магмы;

**English:**

- focal changes in magma density;

**Russian:**

- перераспределению массы внутри геоида;

**English:**

- to the redistribution of mass within the geoid;

**Russian:**

- появлению новой главной оси с наименьшим моментом инерции;

**English:**

- the appearance of a new major axis with the lowest moment of inertia;

**Russian:**

- перевороту планеты по Джанибекову;

**English:**

- of the Janibekov planetary upheaval;

**Russian:**

- смещению оси вращения Земли в новое положение;

**English:**

- shifting the axis of rotation of the Earth to a new position;

**Russian:**

- изменению хода атмосферных течений;

**English:**

- changes in the course of atmospheric currents;

**Russian:**

- изменению хода океанических течений;

**English:**

- of the changing course of ocean currents;

**Russian:**

- трансгрессии и регрессии морей из-за смещения оси и изменения течений;

**English:**

- transgression and regression of the seas due to axis shifts and changes in currents;

**Russian:**

- смещению зон пассатов и муссонов;

**English:**

- shifting trade winds and monsoon zones;

**Russian:**

- изменению климатических зон;

**English:**

- changing climate zones;

**Russian:**

- изменению ротационных напряжений;

**English:**

- to change the rotational stresses;

**Russian:**

- появлению системы разломов и сдвигов;

**English:**

- the emergence of a system of faults and shifts;

**Russian:**

- затяжным общепланетарным землетрясениям;

**English:**

- of protracted planetary earthquakes;

**Russian:**

- одновременным извержениям по всему миру;

**English:**

- of simultaneous eruptions around the world;

**Russian:**

- выбросу теллурических и вулканических газов.

**English:**

- the release of telluric and volcanic gases.

**Russian:**

Просмотрим исторические свидетельства, не забывая, что все это либо убывающие по интенсивности повторы полной схемы, впервые состоявшейся порядка 8200 лет назад, либо просто очень хорошие примеры, из которых что-то можно понять.

**English:**

Let us review the historical evidence, keeping in mind that these are all either decreasingly intense repetitions of a complete scheme first held some 8200 years ago, or just very good examples from which something can be understood.

**Russian:**

**16.2. СВИДЕТЕЛЬСТВА О ПЕРЕВОРОТАХ ЗЕМЛИ В ПРЕДАНИЯХ**

**English:**

**16.2. EVIDENCE OF LAND UPHEAVALS IN LORE**

**Russian:**

1. Эскимосы Гренландии рассказывали миссионерам, как земля однажды перевернулась.

**English:**

1 The Eskimos of Greenland told the missionaries how the earth once turned over.

**Russian:**

2. Помпоний Мела, ссылаясь на древнеегипетские рукописные источники, говорил о том, что с начала развития их цивилизации звёзды меняли свой ход четыре раза, а Солнце дважды всходило на западе.

**English:**

2. Pomponius Mela, referring to ancient Egyptian manuscript sources, said that since the beginning of the development of their civilization the stars changed their course four times, and the Sun twice rose in the west.

**Russian:**

3. В Египте за 11340 лет... солнце четыре раза восходило не на своем обычном месте: именно, дважды восходило там, где теперь заходит, и дважды заходило там, где ныне восходит. И от этого не произошло в Египте никакой перемены в смысле плодородия почвы и растений, режима реки, болезней или людской смертности.

**English:**

3. In Egypt during 11340 years ... The sun ascended four times out of its usual place: namely, twice ascending where it now ascends, and twice descending where it now ascends. And there was no change in the fertility of the soil and plants, in the regime of the river, in disease, or in the mortality of the people in Egypt.

**Russian:**

4. Археолог Харрис нашёл папирус, в котором говорилось о том, что юг и север поменялись местами.

**English:**

4. archaeologist Harris found a papyrus that said south and north had switched places.

**Russian:**

5. В Сирии (г. Угарит), был найден текст говоривший о том, что богиня Анат сменила движение звёзд и светил.

**English:**

5. In Syria (Ugarit), a text was found saying that the goddess Anat changed the movement of stars and luminaries.

**Russian:**

6. Мексиканские кодексы содержат описание четырёх направлений движения Солнца. Причём в кодексах говорится о «четырех доисторических Солнцах» и непостоянных сторонах света. Явление переворота Земли в кодексах именуется «уходом четырёхсот южных звёзд».

**English:**

6. Mexican codices contain a description of the four directions of the Sun. Moreover, the codices talk about "four prehistoric Suns" and non-permanent sides of the world. The phenomenon of revolution of the Earth in the codices is called "departure of four hundred southern stars".

**Russian:**

7. Сафуан ибн 'Ассаль сказал, что слышал от самого Пророка Мухаммада такие слова означающее: «Аллах Всевышний создал на западе Врата покаяния, ширина которых 70 лет. Они не закроются до тех пор, пока солнце не взойдет со стороны этих врат».

**English:**

7. Safwan ibn 'Assal said that he heard from the Prophet Muhammad himself such words meaning: "Allah Almighty has created in the West a Gate of Repentance whose width is 70 years. They will not close until the sun rises from the side of this gate.

**Russian:**

**ПРИМЕЧАНИЕ**: в трех преданиях Солнце дважды восходило на западе, (и дважды возвращалось в привычное положение), что отвечает двум основным направлениям линеаментов. Очень важно, что в Египте переворот сам по себе не стал причиной катастроф.

**English:**

** NOTE**: in the three legends the Sun rose twice in the west, (and twice returned to its usual position), which corresponds to the two main directions of the lineaments. It is very important that in Egypt the coup d'état itself did not cause disasters.

**Russian:**

**16.3. ДРУГОЙ ЮГ НА ИЗОБРАЖЕНИЯХ**

**English:**

**16.3. THE OTHER SOUTH IN THE IMAGES**

**Russian:**

Как минимум, три мастера, - Вази, Пиранези и неизвестный автор, изобразивший строительство собора св. Петра, - системно отображают юг в противоположной стороне. Это не отзеркаливание, и не ошибка, это сделано сознательно.

**English:**

At least three masters - Vasi, Piranesi and the unknown author who depicted the construction of St. Peter's Cathedral - systematically depict the south in the opposite direction. This is not a mirroring, nor is it a mistake, it is done deliberately.

**Russian:**

Рис. 55. Джованни Баттиста Пиранези, 1773-1778 гг. Юг в противоположной стороне.

**English:**

Figure 55. Giovanni Battista Piranesi, 1773-1778. South in the opposite direction.

<!-- Local
[![](Step by step_files/44D09FD0B8D180D0B0D0BDD0B5D0B7D0B8.jpg#clickable)](Step by step_files/44%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-XJJYDwvGRT0/XlE6VbQ5dsI/AAAAAAAADHQ/iSfX5UkydbkadZn31Vcw2vOfozq4mO6BACLcBGAsYHQ/s1600/44%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-XJJYDwvGRT0/XlE6VbQ5dsI/AAAAAAAADHQ/iSfX5UkydbkadZn31Vcw2vOfozq4mO6BACLcBGAsYHQ/s1600/44%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Рис. 56. Джованни Баттиста Пиранези, 1748 год. Юг в противоположной стороне.

**English:**

Figure 56. Giovanni Battista Piranesi, 1748. South in the opposite direction.

<!-- Local
[![](Step by step_files/45D09FD0B8D180D0B0D0BDD0B5D0B7D0B8.jpg#clickable)](Step by step_files/45%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-diNmsM6gV9Q/XlE6Z2cbw9I/AAAAAAAADHU/Nr-d9YuYTOctnf9_4GyCJ_L66IHkFXT-gCLcBGAsYHQ/s1600/45%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-diNmsM6gV9Q/XlE6Z2cbw9I/AAAAAAAADHU/Nr-d9YuYTOctnf9_4GyCJ_L66IHkFXT-gCLcBGAsYHQ/s1600/45%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Рис. 57. Джузеппе Вази,1774 год. Юг в противоположной стороне.

**English:**

Figure 57. Giuseppe Vazi, 1774. South on the opposite side.

<!-- Local
[![](Step by step_files/46D092D0B0D0B7D0B8.jpg#clickable)](Step by step_files/46%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/--h0MBjsQ3Jk/XlE6eoxXh2I/AAAAAAAADHY/gTADQZA1HdAUZgM7ToSj9zYNAKAGdDwHwCLcBGAsYHQ/s1600/46%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/--h0MBjsQ3Jk/XlE6eoxXh2I/AAAAAAAADHY/gTADQZA1HdAUZgM7ToSj9zYNAKAGdDwHwCLcBGAsYHQ/s1600/46%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Рис. 58. Джузеппе Вази, 1753 год. Юг в противоположной стороне.

**English:**

Figure 58. Giuseppe Vazi, 1753. South on the opposite side.

<!-- Local
[![](Step by step_files/47D092D0B0D0B7D0B8.jpg#clickable)](Step by step_files/47%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-b9Kc-0emsxA/XlE6jJVOLvI/AAAAAAAADHc/VNmmjxFzEecpVavDPn2FbkWJclRftz14gCLcBGAsYHQ/s1600/47%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-b9Kc-0emsxA/XlE6jJVOLvI/AAAAAAAADHc/VNmmjxFzEecpVavDPn2FbkWJclRftz14gCLcBGAsYHQ/s1600/47%2B%25D0%2592%25D0%25B0%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Место, занимаемое ныне главным алтарем, всегда в тени, но у Пиранези все наоборот, и Католическая церковь не возразила.

**English:**

The place now occupied by the high altar is always in the shadows, but Piranesi has the opposite, and the Catholic Church has not objected.

**Russian:**

Рис. 59. Джованни Батиста Пиранези 1768 год. Юг в противоположной стороне.

**English:**

Figure 59. Giovanni Batista Piranesi 1768. South on the opposite side.

<!-- Local
[![](Step by step_files/48D09FD0B8D180D0B0D0BDD0B5D0B7D0B8.jpg#clickable)](Step by step_files/48%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-r03fEO2-tOM/XlE6oc-tZcI/AAAAAAAADHk/iGRatKQH_CQHb8cn1uXFvrj3whde0CsrQCLcBGAsYHQ/s1600/48%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-r03fEO2-tOM/XlE6oc-tZcI/AAAAAAAADHk/iGRatKQH_CQHb8cn1uXFvrj3whde0CsrQCLcBGAsYHQ/s1600/48%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Рис. 60. Джованни Батиста Пиранези 1730 год. Юг в противоположной стороне.

**English:**

Figure 60. Giovanni Batista Piranesi 1730. South in the opposite direction.

<!-- Local
[![](Step by step_files/49D09FD0B8D180D0B0D0BDD0B5D0B7D0B8.jpg#clickable)](Step by step_files/49%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)
-->

[![](https://1.bp.blogspot.com/-qyjj3JUPWH4/XlE6spLFWvI/AAAAAAAADHs/NI6tjYVw2UQMKRIW6jrbM1UvblSklYm1wCLcBGAsYHQ/s1600/49%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg#clickable)](https://1.bp.blogspot.com/-qyjj3JUPWH4/XlE6spLFWvI/AAAAAAAADHs/NI6tjYVw2UQMKRIW6jrbM1UvblSklYm1wCLcBGAsYHQ/s1600/49%2B%25D0%259F%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B5%25D0%25B7%25D0%25B8.jpg)

**Russian:**

Рис. 61. Строительство собора св. Петра. Солнце на северо-востоке.

**English:**

Fig. 61. Construction of St. Peter's cathedral. The sun in the north-east.

<!-- Local
[![](Step by step_files/50ApostolicPalace-StPetersFresco.jpg#clickable)](Step by step_files/50%2BApostolicPalace-StPetersFresco.jpg)
-->

[![](https://1.bp.blogspot.com/-3wEwBmXwA28/XlFBINEaNeI/AAAAAAAADIA/wRWdu4YPkGMCWJoNcJoV48cuj_0X_JkxwCLcBGAsYHQ/s1600/50%2BApostolicPalace-StPetersFresco.jpg#clickable)](https://1.bp.blogspot.com/-3wEwBmXwA28/XlFBINEaNeI/AAAAAAAADIA/wRWdu4YPkGMCWJoNcJoV48cuj_0X_JkxwCLcBGAsYHQ/s1600/50%2BApostolicPalace-StPetersFresco.jpg)

**Russian:**

**ПРИМЕЧАНИЕ**: Картины отражают не постоянное положение Солнца, а краткосрочный феномен. Даже если планета Земля перевернулась, Солнце продолжит светить со стороны экватора, но в сам период переворота Солнце может временно оказаться над меняющим положение северным полюсом. Обратите внимание, никаких природных катастроф в этот момент не происходит. Катастрофы придут позже, когда по итогам переворота океаническим течениям придется менять маршруты, причем, с учетом сместившейся оси вращения планеты.

**English:**

**NOTE**: The pictures do not reflect a permanent position of the Sun, but a short-term phenomenon. Even if the planet Earth is reversed, the Sun will continue to shine on the equatorial side, but during the reversal period itself, the Sun may be temporarily over the reversing North Pole. Note, no natural disasters occur at this point. Disasters will come later when following the revolution the oceanic currents should change routes, and, taking into account the displaced axis of rotation of the planet.

**Russian:**

**16.4. ОСТАНОВИВШЕЕСЯ СОЛНЦЕ**

**English:**

**16.4. STOPPED SUN**

**Russian:**

Вне зависимости от того, был переворот Земли или нет, смещение оси вращения Земли должно сопровождаться либо кажущейся приостановкой Солнца, либо кажущимся ускорением его движения. Во время переворота планеты время суток так же обязано приостановиться. Свидетельства об этом феномене есть как в преданиях, так и в летописях, - в том числе хорошо датированные.

**English:**

Regardless of whether there was a revolution of the Earth or not, the shift of the axis of rotation of the Earth must be accompanied by either an apparent suspension of the Sun, or the apparent acceleration of its motion. During the revolution of the planet time of day must also be suspended. There is evidence of this phenomenon both in legends and chronicles - including well dated.

**Russian:**

**В ПРЕДАНИЯХ**

**English:**

** IN THE LORE **

**Russian:**

В одной из легенд ацтеков рассказывается, что однажды Солнце и Луна на небе остановились, и Земля погрузилась во тьму.

**English:**

One Aztec legend tells us that one day the sun and moon in the sky stopped, and the Earth was plunged into darkness.

**Russian:**

В Китае во времена императора Яо в течение 10 дней солнце стояло на небосклоне. 10 дней солнце светило и в Индии, 9 - в Иране. А в Египте на смену 7-дневному «солнцестоянию» пришла такая же по длине ночь.

**English:**

In China, during the time of Emperor Yao, the sun stood in the sky for 10 days. The sun shone for 10 days in India, and for 9 days in Iran. And in Egypt the 7-day "solstice" was replaced by the same long night.

**Russian:**

У индейцев Перу сохранилось предание о ночи, которая стояла пять суток, после чего океан вышел из берегов и затопил землю.

**English:**

The Indians of Peru have a legend of a night that stood for five days, after which the ocean burst its banks and flooded the land.

**Russian:**

В преданиях манси Потоп сопровождался семидневным исчезновением Солнца.

**English:**

In Mansi legends the Flood was accompanied by seven-day disappearance of the Sun.

**Russian:**

**В ЛЕТОПИСЯХ**

**English:**

** IN THE ANNALS**

**Russian:**

Датированных летописных свидетельств об остановившемся Солнце четыре.

**English:**

There are four dated annalistic evidences of the stopped Sun.

**Russian:**

1123 г. Святитель Феоктист епископ Черниговский был свидетелем чудес, бывших при кончине благочестивого князя Давида Святославича и его погребении. Святитель хотел отложить погребение на другой день, но солнце остановилось на небе и не заходило, пока князь не был погребен.

**English:**

Theoktist Bishop of Chernigov witnessed miracles happening at the death of the pious Prince David Svyatoslavich and his burial in 1123. The sainted one wanted to postpone the funeral till the next day, but the sun stopped in the sky and did not set until the prince was buried.

**Russian:**

1391 г. во время правления одного из царей инков в течение 20 часов не наступал рассвет. Расчеты показали, что это событие должно было произойти в 1391 году.

**English:**

1391 during the reign of one of the Inca kings, there was no dawn for 20 hours. Calculations showed that this event must have happened in 1391.

**Russian:**

1547 года, 24 апреля чудо Навина якобы снова повторилось при Карле V во время Мальбергской битвы).

**English:**

1547, on April 24, the miracle of Naveen was allegedly repeated under Charles V at the Battle of Malberg).

**Russian:**

1683 Будет ти слоньце сьветило длужей [длинней] три годины [часа].

**English:**

1683 There shall be thou an elephant long three years and three hours.

**Russian:**

**16.5. СБОЙ СЕЗОНОВ ГОДА**

**English:**

**16.5. FAILURE OF SEASONS OF THE YEAR**

**Russian:**

Переворот Земли по Джанибекову приводит к сбою в сезонах года: второй раз приходит зима либо лето. Приведенные свидетельства косвенные, поскольку с погодой и не такое случается, но лучше их из внимания не выпускать.

**English:**

Turn of the Earth according to Dzhanibekov leads to failure in the seasons of the year: the second time comes winter or summer. The given evidences are indirect, as not such things happen with weather, but it is better not to let them out of attention.

**Russian:**

1289 г. в Риге выдалась зима... без зимы. В полях росли цветы, на Рождество девушки украшали себя фиалками.

**English:**

In 1289 Riga had a winter... without winter. Flowers grew in the fields, on Christmas girls decorated themselves with violets.

**Russian:**

1303 г. Схожая зима. Причем не только в Латвии. Одна из древнерусских летописей гласит: «Бысть зима тепла, без снега».

**English:**

1303 A similar winter. And not only in Latvia. One of the old Russian chronicles says: "There was a winter of warmth, without snow".

**Russian:**

1471 г. Среди февраля на деревьях распускались почки.

**English:**

1471 In the middle of February, buds were blossoming on the trees.

**Russian:**

1471 г. Осень -- в Европе повторно зацвели плодовые деревья.

**English:**

1471 Autumn -- In Europe, the fruit trees bloomed again.

**Russian:**

1473 г. Октябрь -- в Русских землях вторично зацвели деревья.

**English:**

October 1473 - in Russian lands trees blossomed for the second time.

**Russian:**

1493 г. В январе в Ливонии цвели сады, птицы вили гнезда

**English:**

1493 In January in Livonia, gardens were in bloom, birds were nesting

**Russian:**

1521 г. Январь -- в Германии цвели черешни.

**English:**

1521 January -- in Germany, cherry trees were in bloom.

**Russian:**

1522 г. Осень -- в Европе повторно цвели деревья, повторно собирали лесные ягоды.

**English:**

1522 Autumn - In Europe, trees were blooming again, forest berries were being picked repeatedly.

**Russian:**

1530 г. Январь -- в Европе цвели полевые цветы.

**English:**

1530 г. January -- Wildflowers were in bloom in Europe.

**Russian:**

1539 г. Зима -- в Европе цвели фиалки.

**English:**

1539 Winter - violets were in bloom in Europe.

**Russian:**

1540 г. Январь -- зацвели вишни в Тироле, Австрия.

**English:**

1540 January -- Cherry trees blossomed in Tyrol, Austria.

**Russian:**

1540 г. Зима -- Теплая зима в Западной Европе, второй раз цвели розы.

**English:**

1540 Winter -- A warm winter in Western Europe, the second time roses bloomed.

**Russian:**

1586 г. Зима -- теплая в Западной Европе. Деревья в Западной Европе цвели повторно.

**English:**

1586 Winter is warm in Western Europe. Trees in Western Europe bloomed again.

**Russian:**

1592 г. Осень -- в западнорусских землях листья с деревьев не опали, и деревья были такими же зелеными, как весной.

**English:**

1592 Autumn - in Western Russia the leaves did not fall off the trees, and the trees were as green as in the spring.

**Russian:**

1617 г. В январе в садах под Ригой цвели вишни...

**English:**

1617 г. In January cherry trees blossomed in the gardens near Riga...

**Russian:**

1617 г. Январь -- цвели вишни в Прибалтике.

**English:**

1617 г. January -- cherry blossoms in the Baltics.

**Russian:**

1816 г. Год без лета

**English:**

1816 г. A year without summer

**Russian:**

1819 г. Ноябрь -- в Западной Европе повторно зацвели деревья.

**English:**

1819 November -- Trees in Western Europe bloomed again.

**Russian:**

1822 г. Сентябрь -- в Западной Европе снова цвели яблони.

**English:**

1822 September -- In Western Europe, apple trees were in bloom again.

**Russian:**

1822 г. В Тамбовской губернии в праздник Рождества Христова в полях показалась зелень, рвали свежую крапиву для щей.

**English:**

1822. In Tambov Province on the feast of Christmas in the fields showed green, fresh nettles were torn for soup.

**Russian:**

1822 г. В начале ноября в Гамбурге ели свежую землянику и малину.

**English:**

1822 At the beginning of November, Hamburg was eating fresh strawberries and raspberries.

**Russian:**

1823 г. В январе в Риге было минус 30, а в феврале прогремела первая гроза.

**English:**

1823 In January Riga was at minus 30, and in February the first thunderstorm hit.

**Russian:**

**16.6. КАЛЕНДАРНЫЙ СБОЙ**

**English:**

**16.6. CALENDAR FAILURE**

**Russian:**

Новый год в античности и средневековье это время расчета по долгам и налогам -- сразу после урожая, когда закрома полны. Поэтому перенос начала года на полгода вперед с марта на сентябрь означает сбой сезонов. Вот даты реформ с переносом начала года с марта на сентябрь.

**English:**

New Year in antiquity and medieval times was a time of settling debts and taxes -- just after the harvest, when the stash is full. So moving the beginning of the year six months ahead from March to September means a disruption of the seasons. Here are the dates of the reforms with the shift of the beginning of the year from March to September.

**Russian:**

31 г. до н. э. Восточные провинции Римской империи

**English:**

31 B.C. Eastern provinces of the Roman Empire

**Russian:**

284 г. Коптский (Александрийский) календарь

**English:**

284 Coptic (Alexandrian) calendar

**Russian:**

312 г. Византия

**English:**

312 г. Byzantium

**Russian:**

462 г. Византия

**English:**

462 Byzantium

**Russian:**

537 г. Византия

**English:**

537 Byzantium

**Russian:**

988 г. Византия

**English:**

988 Byzantium

**Russian:**

1342 г. Россия (без уточнения)

**English:**

1342 Russia (unspecified)

**Russian:**

1348 г. Московия

**English:**

1348 Moscovia

**Russian:**

1425 г. Московия

**English:**

1425 г. Moscovia

**Russian:**

1492 г. Московия

**English:**

1492 Moskovia

**Russian:**

**16.7. СБОЙ В ЦЕРКОВНОЙ ТРАДИЦИИ**

**English:**

**16.7. FAILURE IN CHURCH TRADITION**

**Russian:**

1479 г. При освящении Успенского собора в московском кремле 12 августа ростовский архиепископ Вассиан и чудовский архимандрит Геннадий заявили, что митрополит Геронтий нарушил церковное предание, совершив крестный ход «не по солнечному всходу», то есть не «посолонь».

**English:**

On August 12, 1479 at the consecration of the Assumption Cathedral in the Moscow Kremlin, Archbishop Vassian of Rostov and Archimandrite Gennady of Chudovo declared that Metropolitan Gerontius violated Church tradition by making the procession "not according to the solar ascent", that is, not "according to the sun".

**Russian:**

**ПРИМЕЧАНИЕ**: Именно такие конфликты обязаны были возникнуть в Церквях после переворота Земли по Джанибекову, и эти конфликты хорошо документированы.

**English:**

**NOTE**: These are the kinds of conflicts that were bound to arise in the Churches after Janibekov's coup d'état, and these conflicts are well documented.

**Russian:**

**16.8.1. ЭКСКУРСЫ МАГНИТНОГО ПОЛЯ**

**English:**

**16.8.1. MAGNETIC FIELD EXCURSIONS**

**Russian:**

Официально экскурсов магнитного поля Земли в ближайшие 50 тысяч лет было два: событие Лашан (41400 лет назад) и событие озера Моно (34 тысячи лет назад). Полных магнитных инверсий было и того меньше. Однако именно эта тема на сегодня чересчур горяча, хотя бы потому, что полосчатые магнитные аномалии на дне океанов, идущие с шагом в 40 км, даже в науке воспринимаются как указание на фантастически регулярные магнитные инверсии [^92].

**English:**

Officially excursions of a magnetic field of the Earth in the next 50 thousand years were two: event Lashan (41400 years ago) and lake Mono event (34 thousand years ago). Full magnetic inversions were even less. However, this very topic is too hot today, if only because the striated magnetic anomalies at the bottom of the oceans, going with a step of 40 km, are perceived even in science as an indication of fantastically regular magnetic inversions [^92].

**Russian:**

Несколько лет назад в печати прошло свидетельство палеомагнитологов об экскурсе, оставившем следы в кермаике этрусков, но сегодня обсуждение этого факта подходит лишь для бульварной прессы. Наука предпочитает быть осторожной в суждениях.

**English:**

Some years ago in the press there was a testimony of paleomagnetologists about the excursion leaving traces in Kermaic of Etruscans, but today the discussion of this fact is suitable only for the tabloid press. Science prefers to be cautious in judgments.

**Russian:**

**16.8.2. СМЕЩЕНИЕ МАГНИТНЫХ ПОЛЮСОВ**

**English:**

**16.8.2. MAGNETIC POLE SHIFT**

**Russian:**

Процитирую: Исследователи из Великобритании и Дании проанализировали спутниковые геофизические данные за 20 лет и обнаружили, что смещение северного магнитного полюса Земли определяется «конкуренцией» двух магнитных аномалий, одна из которых располагается под северной Канадой, а другая -- под сибирским шельфом. Ранее геологи НАСА выяснили, что смещение оси Земли может быть связано как с окончанием ледникового периода, так и с движением потоков мантии... [^138].

**English:**

I quote: Researchers from Great Britain and Denmark have analyzed satellite geophysical data for 20 years and found that the shift of the Earth's northern magnetic pole is determined by a "competition" of two magnetic anomalies, one of which is located under northern Canada and the other under the Siberian shelf. NASA geologists have previously determined that the Earth's axis shift may be related to both the end of the Ice Age and the movement of mantle flows... [^138].

**Russian:**

Это ровно то, что происходит в нашей модели.

**English:**

That's exactly what's happening in our model.

**Russian:**

**16.9. СВИДЕТЕЛЬСТВА В ЯЗЫКЕ**

**English:**

**16.9. EVIDENCE IN LANGUAGE**

**Russian:**

Прямое свидетельство о смене сторон света сохранилось в персидском языке.

**English:**

Direct evidence of the change of sides of the world is preserved in the Persian language.

**Russian:**

خاور [хавäр]

**English:**

خاور [havdar]

**Russian:**

1) восток; Восток;

**English:**

1) East; East;

**Russian:**

2) книжн. солнце;

**English:**

2) bookish. sun;

**Russian:**

3) устаревшее: запад; Запад;

**English:**

3) obsolete: west; west;

**Russian:**

خاوران [хавäран] устаревшее

**English:**

خاوران [haväran] obsolete

**Russian:**

1) Восток;

**English:**

1) East;

**Russian:**

2) восток и запад;

**English:**

2) east and west;

**Russian:**

باختر [бахтäр]

**English:**

باختر [bahtär]

**Russian:**

1) запад;

**English:**

1) West;

**Russian:**

2) устаревшее: восток;

**English:**

2) obsolete: east;

**Russian:**

3) ист. Бактрия

**English:**

(3) Bactria ist.

**Russian:**

Полный текст и ссылки в примечании за № 46.

**English:**

Full text and references in footnote for No. 46.

**Russian:**

**16.10. ДВА ВОСТОКА И ДВА ЗАПАДА**

**English:**

**16.10. TWO EAST AND TWO WEST**

**Russian:**

В Коране регулярно встречается выражение «два востока и два запада», при том что с севером и югом никаких проблем нет. Именно такая ситуация должна возникать при переворотах Земли по Джанибекову. Цитаты и ссылки в примечании за № 47.

**English:**

In the Quran the expression "two east and two west" regularly occurs, while there is no problem with the north and the south. This is exactly the situation that should occur during the upheavals of the Earth according to Dzhanibekov. Citations and references in the note for #47.

**Russian:**

**16.11. СПИСОК УКАЗАНИЙ НА ПЕРЕВОРОТ ЗЕМЛИ**

**English:**

**16.11. LIST OF INDICATIONS OF LAND FLIP**

**Russian:**

1. Предания о смене сторон света

**English:**

1. Legends about the change of sides of the world

**Russian:**

2. Предания об остановившемся Солнце

**English:**

2. The legends about the stopped Sun

**Russian:**

3. Летописные свидетельства об остановившемся Солнце

**English:**

3. Annals about the stopped Sun

**Russian:**

4. Изображения иначе стоящего Солнца

**English:**

4. Images of the Sun standing otherwise

**Russian:**

5. Мнение геологов XIX века

**English:**

5. Opinion of 19th century geologists

**Russian:**

6. Сбой сезонов (два лета или две зимы подряд)

**English:**

6. Disruption of seasons (two summers or two winters in a row)

**Russian:**

7. Календарный сбой на полгода

**English:**

7. Calendar failure for half a year

**Russian:**

8. Сбой в церковной традиции

**English:**

8. Failure in church tradition

**Russian:**

9. Гипотетический экскурс магнитного поля в Этрурии

**English:**

9. Hypothetical excursion of the magnetic field in Etruria

**Russian:**

10. Свидетельства в языке

**English:**

10. Evidence in language

**Russian:**

11. Два запада и два востока в Коране

**English:**

11. Two West and Two East in the Qur'an

**Russian:**

**17.1. СМЕЩЕНИЕ ОСИ ВРАЩЕНИЯ**

**English:**

**17.1. ROTATION AXIS DISPLACEMENT**

**Russian:**

На смещение оси вращения мог бы указывать календарный сбой менее чем на полгода. Такой сбой зафиксирован во Франции 1792-1794 годов [^93]: ряды знаковых событий повторяются со сдвигом в среднем на 2 месяца и 6 дней, как если бы фиксировались в разных календарных системах. Однако вряд ли это станет доказательством: календарные сбои проще объяснить сбоями счета, чем сбоями хода светил.

**English:**

The shift of the axis of rotation could be indicated by a calendar shift of less than half a year. Such a glitch was recorded in France 1792-1794 [^93]: series of significant events are repeated with a shift of an average of 2 months and 6 days, as if they were fixed in different calendar systems. However, it is unlikely to become evidence: calendar failures are easier to explain by failures of counting, than by failures of the course of the luminaries.

**Russian:**

Есть и два прямых научных свидетельства. Одно поставлено под сомнение.

**English:**

There are also two pieces of direct scientific evidence. One is questioned.

**Russian:**

1783 г. Британские острова страдали от жары, гроз, чумы, ос и вездесущего сухого тумана. Некоторые ученые подозревали, что ось Земли сдвинулась. Жители Каслтона в Дербишире жили в тени близлежащей горы большую часть зимы. Они утверждали, что первое солнце года вошло в их дома после зимнего солнцестояния на несколько дней раньше, чем это было 50 лет назад, и что тень их горы была теперь на несколько ярдов короче, чем должна быть на середине зимы.

**English:**

1783 The British Isles were suffering from heat waves, thunderstorms, plagues, wasps and ubiquitous dry fog. Some scientists suspected that the Earth's axis had shifted. The people of Castleton in Derbyshire lived in the shadow of a nearby mountain for much of the winter. They claimed that the first sun of the year entered their homes after the winter solstice several days earlier than it had been 50 years earlier, and that the shadow of their mountain was now several yards shorter than it should be at midwinter.

**Russian:**

А вот второе свидетельство имеет полную научную силу.

**English:**

The second testimony, on the other hand, has full scientific validity.

**Russian:**

**ЦИТАТА**: Наблюдения Ле Монье, сделанные в 1743 году с помощью гномона в церкви Сен-Сюльпис в Париже, показали, по сравнению с измерениями, сделанными Джованни Кассини в Болонье в 1656 году, на уменьшение наклона эклиптики.

**English:**

**CITATION**: The observations of Le Monnier, made in 1743 with a gnomon at the church of Saint-Sulpice in Paris, showed, in comparison with the measurements made by Giovanni Cassini at Bologna in 1656, a decrease in the inclination of the ecliptic.

[https://es.m.wikipedia.org/wiki/Pierre_Charles_Le_Monnier](https://es.m.wikipedia.org/wiki/Pierre_Charles_Le_Monnier)

**Russian:**

**17.2. СБОЙ ШИРОТЫ В 1831-1832 ГГ. НА 2-4 °. ДОКУМЕНТЫ**

**English:**

**17.2. LATITUDE VARIATIONS IN 1831-1832. 2-4°. DOCUMENTS**.

**Russian:**

Взяты ежедневные данные восходов и заходов Солнца и Луны для Санкт-Петербурга и Москвы, в интервале с 1 по 31 марта (по юлианскому календарю). С 1831 на 1832 год «прибавка за месяц» восхода в Санкт-Петербурге скачком увеличилась примерно на 10 минут, а Москве - на 9. До 1831 года Солнце «прибывало» весной медленнее, и это означает, что географически города находились южнее на 2-4 градуса [^94].

**English:**

Taken daily data of sunrises and moonsets for St. Petersburg and Moscow, in the interval from 1 to 31 March (according to the Julian calendar). From 1831 to 1832 "increment for a month" of sunrise in St. Petersburg increased by about 10 minutes and in Moscow - by 9 minutes. Before 1831, the Sun "arrived" in the spring slower, which means that geographically the cities were 2-4 degrees south [^94].

**Russian:**

Питер (современная широта 59°57'), за 31 день (30 суток) Солнце встает раньше на:

**English:**

Peter (modern latitude 59°57'), in 31 days (30 days) the Sun rises earlier by:

**Russian:**

90 мин (1833г) Источник: http://elib.shpl.ru/

**English:**

90 min (1833) Source: http://elib.shpl.ru/

**Russian:**

91 мин (1832г) Источник: http://elib.shpl.ru/

**English:**

91 min (1832) Source: http://elib.shpl.ru/

**Russian:**

81 мин (1831г) Источник: http://elib.shpl.ru/

**English:**

81 min (1831) Source: http://elib.shpl.ru/

**Russian:**

81 мин (1829г) Источник: http://elib.shpl.ru/

**English:**

81 min (1829) Source: http://elib.shpl.ru/

**Russian:**

Москва (современная широта 55°45'), за 31 день (30 суток) Солнце встает раньше на:

**English:**

Moscow (modern latitude 55°45'), in 31 days (30 days) the Sun rises earlier on:

**Russian:**

77 мин (1833г) Источник: http://elib.shpl.ru/

**English:**

77 min (1833) Source: http://elib.shpl.ru/

**Russian:**

78 мин (1832г) Источник: http://elib.shpl.ru/

**English:**

78 min (1832) Source: http://elib.shpl.ru/

**Russian:**

69 мин (1831г) Источник: http://elib.shpl.ru/

**English:**

69 min (1831) Source: http://elib.shpl.ru/

**Russian:**

68 мин (1829г) Источник: http://elib.shpl.ru/

**English:**

68 min (1829) Source: http://elib.shpl.ru/

**Russian:**

**17.3. ДРУГИЕ ШИРОТЫ НА КАРТЕ 1813 ГОДА**

**English:**

**17.3. OTHER LATITUDES ON THE 1813 MAP**

**Russian:**

Карта театра войны Российской империи против Франции и ее союзников. Изд. титул. Советником Ал. Савинковым, 1813 год [^95].

**English:**

Map of the theater of war of the Russian Empire against France and its allies. Ed. tit. Counselor Al. Savinkov, 1813 [^95].

**Russian:**

Житомир, старая широта 48° 25'

**English:**

Zhytomyr, old latitude 48° 25'

**Russian:**

Житомир, современная широта 50°15′53″ с.ш.

**English:**

Zhytomyr, present latitude 50°15′53″ N.

**Russian:**

Яссы, старая широта 45°50'

**English:**

Iasi, old latitude 45°50'.

**Russian:**

Яссы, современная широта 47°10′00″ с.ш.

**English:**

Iasi, present latitude 47°10′00″ N.

**Russian:**

Псков, старая широта 54°

**English:**

Pskov, old latitude 54°

**Russian:**

Псков, современная широта 57°48′48″ с.ш.

**English:**

Pskov, present latitude 57°48′48″N.

**Russian:**

Рига, старая широта 53°35'

**English:**

Riga, old latitude 53°35'.

**Russian:**

Рига, современная широта 56°56′45″ с.ш.

**English:**

Riga, present latitude 56°56′45″ N.

**Russian:**

**17.4. ДРУГОЕ НАПРАВЛЕНИЕ НА ПОЛЮС**

**English:**

**17.4. OTHER POLE DIRECTION**

**Russian:**

На картах Крыма за 1750, 1782 и 1787 годы [^146] географическая сетка с иной ориентацией на полюс, со склонением на 17° к западу.

**English:**

The maps of the Crimea for 1750, 1782, and 1787 [^146] show a geographic grid with a different pole orientation, with a declination of 17° to the west.

**Russian:**

Рис. 62. Сопоставление градусных сеток на современной карте (1922) и на картах 1750, 1782 и 1787 гг.

**English:**

Figure 62. Comparison of degree grids on the modern map (1922) and on the maps of 1750, 1782, and 1787.

<!-- Local
[![](Step by step_files/59_2D09AD180D18BD0BC.jpg#clickable)](Step by step_files/59_2%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC.jpg)
-->

[![](https://1.bp.blogspot.com/-8NLaycSWXZ8/X9ZNgNEWT_I/AAAAAAAADpk/Di9LS3riCNU6icYC6uSK2dFC0ni905vxgCLcBGAsYHQ/s1184/59_2%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC.jpg#clickable)](https://1.bp.blogspot.com/-8NLaycSWXZ8/X9ZNgNEWT_I/AAAAAAAADpk/Di9LS3riCNU6icYC6uSK2dFC0ni905vxgCLcBGAsYHQ/s1184/59_2%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC.jpg)

**Russian:**

Теперь смотрим оригиналы.

**English:**

Now looking at the originals.

**Russian:**

Рис. 63. Карта 1750 года

**English:**

Figure 63. Map of 1750

<!-- Local
[![](Step by step_files/59_3D09AD180D18BD0BC1750.jpg#clickable)](Step by step_files/59_3%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1750.jpg)
-->

[![](https://1.bp.blogspot.com/-P1cj0i1Ro_U/X9ZNylaHm7I/AAAAAAAADps/64ikEXXD6XUj0EEJZ6QG5aJfqPeSa46iQCLcBGAsYHQ/s2048/59_3%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1750.jpg#clickable)](https://1.bp.blogspot.com/-P1cj0i1Ro_U/X9ZNylaHm7I/AAAAAAAADps/64ikEXXD6XUj0EEJZ6QG5aJfqPeSa46iQCLcBGAsYHQ/s2048/59_3%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1750.jpg)

**Russian:**

На карте 1782 года виден процесс сращивания карт разного происхождения: в северной части сетка современная, а широта г. Очакова занимает промежуточное положение.

**English:**

The map of 1782 shows the process of splicing maps of different origins: in the northern part of the grid is modern, and the latitude of Ochakov occupies an intermediate position.

**Russian:**

Рис. 64. Карта 1782 года

**English:**

Figure 64. Map of 1782

<!-- Local
[![](Step by step_files/59_4D09AD180D18BD0BC1782.jpg#clickable)](Step by step_files/59_4%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1782.jpg)
-->

[![](https://1.bp.blogspot.com/-KTjaHulyR9Q/X9ZN_QevjwI/AAAAAAAADpw/vOskM6YRUH8ZyDkXu8EXVDan-158GDpMgCLcBGAsYHQ/s2048/59_4%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1782.jpg#clickable)](https://1.bp.blogspot.com/-KTjaHulyR9Q/X9ZN_QevjwI/AAAAAAAADpw/vOskM6YRUH8ZyDkXu8EXVDan-158GDpMgCLcBGAsYHQ/s2048/59_4%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1782.jpg)

**Russian:**

Рис. 65. Карта 1787 года

**English:**

Fig. 65. Map of 1787

<!-- Local
[![](Step by step_files/59_5D09AD180D18BD0BC1787.jpg#clickable)](Step by step_files/59_5%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1787.jpg)
-->

[![](https://1.bp.blogspot.com/-pU68TknAXco/X9ZOJHBjg6I/AAAAAAAADp4/GKAJWY-_YlMfhtYZN50D5nw7zKluHvNPwCLcBGAsYHQ/s2048/59_5%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1787.jpg#clickable)](https://1.bp.blogspot.com/-pU68TknAXco/X9ZOJHBjg6I/AAAAAAAADp4/GKAJWY-_YlMfhtYZN50D5nw7zKluHvNPwCLcBGAsYHQ/s2048/59_5%2B%25D0%259A%25D1%2580%25D1%258B%25D0%25BC%2B1787.jpg)

**Russian:**

То же склонение имеет направление на север на плане Царицынской линии 1724 года.

**English:**

The same declination has the same direction to the north on the plan of the Tsaritsynsk line of 1724.

**Russian:**

Рис. 66. Планы Царицынской сторожевой линии 1902 и 1724 гг.

**English:**

Figure 66. Plans of the 1902 and 1724 Tsaritsyn Watch Line.

<!-- Local
[![](Step by step_files/59_6D09BD0B8D0BDD0B8D18F1724.jpg#clickable)](Step by step_files/59_6%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B8%25D1%258F%2B1724.jpg)
-->

[![](https://1.bp.blogspot.com/-bk4Sw0QOEoE/X9ZOR6R3q2I/AAAAAAAADqA/BBQE0S7poYkTxbLt5GoVVFwQB_aSi3GdwCLcBGAsYHQ/s1148/59_6%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B8%25D1%258F%2B1724.jpg#clickable)](https://1.bp.blogspot.com/-bk4Sw0QOEoE/X9ZOR6R3q2I/AAAAAAAADqA/BBQE0S7poYkTxbLt5GoVVFwQB_aSi3GdwCLcBGAsYHQ/s1148/59_6%2B%25D0%259B%25D0%25B8%25D0%25BD%25D0%25B8%25D1%258F%2B1724.jpg)

**Russian:**

То же склонение имеют, как минимум, десятки планов городов 1770-1824 гг., из полного собрания законов Российской Империи, и та же ситуация со старыми планами городов Европы. Практика ориентации планов и, тем более, градусных сеток больших карт на магнитный полюс в картографии не принята, то есть, мы видим смещение не магнитного, а именно географического полюса. Ниже пара карт для примера.

**English:**

The same declination have at least dozens of city plans 1770-1824, from the full collection of laws of the Russian Empire, and the same situation with the old city plans of Europe. The practice of orientation of plans and, moreover, grids of large maps on the magnetic pole in cartography is not accepted, that is, we see a shift not magnetic, but precisely the geographic pole. Below are a couple of maps for example.

**Russian:**

Рис. 67. Планы Нижнего Новгорода за 1824 и 1896 гг.

**English:**

Fig. 67. Plans of Nizhny Novgorod for 1824 and 1896.

<!-- Local
[![](Step by step_files/59_7D09DD09DD0BED0B2D0B3D0BED180D0BED0B41824.jpg#clickable)](Step by step_files/59_7%2B%25D0%259D%25D0%259D%25D0%25BE%25D0%25B2%25D0%25B3%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%2B1824.jpg)
-->

[![](https://1.bp.blogspot.com/-FRYZcWs6TF0/X9ZOigiQYoI/AAAAAAAADqM/mCLEHLuEJbIcsyWznR2bxrp5EENhKhl4QCLcBGAsYHQ/s1315/59_7%2B%25D0%259D%25D0%259D%25D0%25BE%25D0%25B2%25D0%25B3%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%2B1824.jpg#clickable)](https://1.bp.blogspot.com/-FRYZcWs6TF0/X9ZOigiQYoI/AAAAAAAADqM/mCLEHLuEJbIcsyWznR2bxrp5EENhKhl4QCLcBGAsYHQ/s1315/59_7%2B%25D0%259D%25D0%259D%25D0%25BE%25D0%25B2%25D0%25B3%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%2B1824.jpg)

**Russian:**

**18.1. ИЗМЕНЕНИЕ ХОДА АТМОСФЕРНЫХ ТЕЧЕНИЙ**

**English:**

**18.1. CHANGE IN THE COURSE OF ATMOSPHERIC CURRENTS**

**Russian:**

1692 г. Ямайка. Со времени землетрясения ветров с суши часто не бывает и вместо них часто дуют ночью морские ветры: явление, прежде бывшее чрезвычайно редким, а теперь сделавшееся обыкновенным

**English:**

1692 Jamaica. Since the earthquake there are often no winds from the land and instead of them sea winds often blow at night: a phenomenon which formerly was extremely rare, but now has become common

**Russian:**

**19.1. ТРАНСГРЕССИИ И РЕГРЕССИИ МОРЕЙ**

**English:**

**19.1. TRANSGRESSIONS AND REGRESSIONS OF THE SEAS**

**Russian:**

Перед тем, как начать, имеет смысл процитировать базовый тезис из инициативного проекта РФФИ 02-05-64428. «Существующие гипотезы и модели не дают удовлетворительного объяснения причин катастрофических трансгрессий внутренних морей Евразии» [^96].

**English:**

Before we begin, it makes sense to quote the basic thesis from the RFBR initiative project 02-05-64428. "The existing hypotheses and models do not provide a satisfactory explanation of the causes of catastrophic transgressions of the Eurasian inland seas". [^96].

**Russian:**

Трансгрессии и регрессии морей могут иметь много причин:

**English:**

Transgressions and regressions of the seas can have many causes:

**Russian:**

1. Смещение оси вращения

**English:**

1. Offset of the rotation axis

**Russian:**

2. Переток из соседнего бассейна (Каспий-Маныч-Азов)

**English:**

2. Overflow from the adjacent basin (Caspian-Manych-Azov)

**Russian:**

3. Изменение хода течений

**English:**

3. changing the course of currents

**Russian:**

3. Изменение электромагнитных характеристик бассейна (эффект Юсупа Хизирова)

**English:**

3. change of electromagnetic characteristics of the basin (Yusup Khizirov effect)

**Russian:**

4. Винтовая поводка ложа бассейна или соседствующих плит

**English:**

4. Screwing of the basin bed or adjacent slabs

**Russian:**

5. Просадки или подъемы плит по итогам смещения оси вращения

**English:**

5. Slab sagging or lifting as a result of rotation axis displacement

**Russian:**

Понятно, что летописцы причин трансгрессий и регрессий не знают, хотя отражают ситуацию предельно точно.

**English:**

It is clear that chroniclers do not know the reasons for transgressions and regressions, although they reflect the situation very precisely.

**Russian:**

365 г. Сократ Схоластик: «Моря изменили свои привычные пределы, в некоторых местах трясло столь сильно, что там, где люди могли раньше ходить они могут теперь плавать. В других местах море отошло столь далеко, что морское дно стало сухим».

**English:**

365 Socrates Scholasticus: "The seas have changed their usual limits, in some places shaking so much that where people used to be able to walk they can now swim. In other places the sea has receded so far that the seabed has become dry".

**Russian:**

**19.2. ПЕРЕТОКИ ПРИ СМЕЩЕНИИ ОСИ ВРАЩЕНИЯ ЗЕМЛИ**

**English:**

**19.2. OVERFLOWS WHEN THE EARTH ROTATION AXIS IS SHIFTED**

**Russian:**

В результате смещения оси вращения Земли воды стремятся:

**English:**

As a result of the shift in the Earth's axis of rotation, water tends to:

**Russian:**

- в секторе от Камчатки до Кавказа -- на юг и на запад;

**English:**

- in the sector from Kamchatka to the Caucasus -- to the south and west;

**Russian:**

- в секторе от Камчатки до Калифорнии -- на юг и на восток;

**English:**

- in a sector from Kamchatka to California -- south and east;

**Russian:**

- в секторе от Атлантики до Кавказа -- на север и на восток;

**English:**

- in a sector from the Atlantic to the Caucasus -- north and east;

**Russian:**

- в секторе от центра Атлантики до Северной Америки -- на север и на запад;

**English:**

- in a sector from the mid-Atlantic to North America -- north and west;

**Russian:**

- в Австралии -- на юг и на запад;

**English:**

- in Australia -- south and west;

**Russian:**

- в Южной Америке -- на север и на запад;

**English:**

- in South America, north and west;

**Russian:**

- в Южной Африке -- на север и на восток;

**English:**

- in South Africa, to the north and to the east;

**Russian:**

- в юго-восточной части Тихого океана -- на юг и на восток.

**English:**

- in the Southeast Pacific -- to the south and east.

**Russian:**

**19.3. СВЕРХВАЖНЫЙ КАСПИЙСКИЙ ТЮЛЕНЬ**

**English:**

**19.3. SUPERNUMERARY CASPIAN SEAL**

**Russian:**

Каспийский тюлень предпочитает холодную воду и биологически связан со льдами, на которых размножается, выкармливает детенышей и линяет. Эти факты работают на гипотезу об океаническом происхождении тюленя. При этом условия, при которых тюлень мог попасть в Каспий, жестки: это не период метеорного инцидента, убивавшего все живое, и не период стока ледниково-подпрудных озер на север по разломам. Наиболее вероятно, что тюлень попал к нам в момент смещения оси вращения Земли, при котором часть воды Северного Ледовитого океана перетекла в Каспий. Создана компьютерная модель, показывающая реальность такого сценария [^97].

**English:**

The Caspian seal prefers cold water and is biologically linked to the ice where it breeds, rears calves and molts. These facts work for the hypothesis of an oceanic origin of the seal. At the same time, the conditions under which the seal could have come to the Caspian Sea are harsh: it is not the period of the meteoric incident that killed all life, and not the period of the glacier-pond lakes flowing northward along the rifts. Most likely, the seal came to us at the time of the Earth's rotation axis shift, when some of the water of the Arctic Ocean flowed into the Caspian Sea. A computer model has been created that shows the reality of such a scenario [^97].

**Russian:**

Рис. 68. Океаническая вода в Каспийском море [^97]

**English:**

Figure 68. Ocean water in the Caspian Sea [^97]

<!-- Local
[![](Step by step_files/51D09CD0BED0B4D0B5D0BBD18C.png#clickable)](Step by step_files/51%2B%25D0%259C%25D0%25BE%25D0%25B4%25D0%25B5%25D0%25BB%25D1%258C.png)
-->

[![](https://1.bp.blogspot.com/-nhgDN9hppoY/XlFBf5xe4ZI/AAAAAAAADII/3jFQliJi0YsgJqW2ySEaR-vTOcP7fvGRgCLcBGAsYHQ/s1600/51%2B%25D0%259C%25D0%25BE%25D0%25B4%25D0%25B5%25D0%25BB%25D1%258C.png#clickable)](https://1.bp.blogspot.com/-nhgDN9hppoY/XlFBf5xe4ZI/AAAAAAAADII/3jFQliJi0YsgJqW2ySEaR-vTOcP7fvGRgCLcBGAsYHQ/s1600/51%2B%25D0%259C%25D0%25BE%25D0%25B4%25D0%25B5%25D0%25BB%25D1%258C.png)

**Russian:**

**19.4. ПОТОП ИЗ-ЗА СМЕЩЕНИЯ ОСИ**

**English:**

**19.4. FLOOD DUE TO AXIS MISALIGNMENT**

**Russian:**

Известно, что наиболее крупные массивы суши -- Доггерленд, Сундаленд, Сахуленд и Берингия -- затоплены еще в плейстоцене, примерно 8200 лет назад, однако в книге «Тонущие города» перечислены, как минимум, 90 известных городов «античности, средневековья и более поздних периодов» ныне находящиеся под водой, как правило, на глубине 8-12 метров, но местами и глубже.

**English:**

The largest land masses of Doggerland, Sundaland, Sahooland and Beringia are known to have been submerged since the Pleistocene some 8,200 years ago, but the book "Drowning Cities" lists at least 90 known cities from "ancient, medieval and later periods" that are now underwater, generally 8-12 meters, but some even deeper.

**Russian:**

Причина затопления -- смещение оси, возможно, не за один раз. Восточные бассейны для центробежных сил сразу оказываются выше западных -- на сотни метров. Сначала водой Северного Ледовитого океана в Каспий заносит тюленей. Затем из ледниково-подпрудных озер вокруг Арала топит города западного Каспия, в частности, хазарский Семендер. Именно в этот период формируется «марсианский» ландшафт Мангышлака и Невады. Затем водой Каспия по Кумо-Манычской перетоке топит Азов и Черное море. И в финале, черноморской водой через Босфор топит города Средиземноморья. Именно поэтому на дне Эгейского моря лежат черноморские сапропели.

**English:**

The reason for the flooding is axis displacement, perhaps not all at once. The eastern basins for the centrifugal forces immediately turn out to be higher than the western ones by hundreds of meters. At first, the water of the Arctic Ocean brings seals into the Caspian Sea. Then, from the glacier-pond lakes around the Aral Sea drowns the western Caspian cities, in particular, the Khazar Semender. It is during this period that the "Martian" landscape of Mangyshlak and Nevada is formed. Then the water of the Caspian Sea through the Kumo-Manych River flows into Azov and the Black Sea. And finally, the Black Sea water through the Bosporus drowns the cities of the Mediterranean. That is why the Black Sea sapropels lie at the bottom of the Aegean Sea.

**Russian:**

**НАУЧНОЕ ПОДТВЕРЖДЕНИЕ ЗАЯВЛЕННОГО**

**English:**

**SCIENTIFIC PROOF OF CLAIM**

**Russian:**

Выполнены палеогидрологические реконструкции внутренних бассейнов Евразии, представляющих собой Каскад Евразийских Бассейнов от Арала через Узбой к Хвалынскому морю, далее через Маныч-Керченский пролив к Новоэвксинскому морю и через Босфор в Мраморное море вплоть до Средиземного моря. Для этих бассейнов выполнены расчеты уровней, площадей акватории и затопления, объемов водных масс, солености, а также объемов водообмена между ними. [^96].

**English:**

Paleohydrological reconstructions of Eurasian inland basins representing the Cascade of Eurasian Basins from the Aral Sea through Uzboy to the Khvalyn Sea, then through the Manych-Kerch Strait to the Novoevksin Sea and through the Bosporus to the Marmara Sea up to the Mediterranean Sea have been performed. For these basins, calculations have been made of the levels, water area and flooding, water mass volumes, salinity, and the volume of water exchange between them. [^96].

**Russian:**

**СПИСОК ЗАТОНУВШИХ ГОРОДОВ**

- **Адриатическое море**: Вибион, Матамауцо, Спина, Эпидавр Иллирийский.

- **Азовское море**: Таганрог.

- **Каспийское море**: Абескун, Бандовян, Кегня-Бильгях, Нарын-Кале, Терки.

- **Восточное Средиземноморье**: Гелика, Коринф, Пирей, Гифион, Фарос, Атлит, Кесария, Цезарея, Вургунда, Тристомо, Лефкос, Ватхипотамос, Саламин, Мариумполис, Пафос, Куриас, Аматос, Мохлос, Херсонес, Панормус, Металлон, Амнисас, Мони Арви, Элунда, Псира, Спиналонга, Итанос, Капа Плака, Закрос, Ампелос, Сидон, Тир, Аполлония, Птолемаида, Таухира, Арвад, Айфуза, Тапс

- **Западное Средиземноморье**: Иол, Вайя, Поццуоли, Мизенум, Фос.

- **Черное море**: Аполлония, Дионисополис, Месембрия, Одессос, Анакопия, Диоскурия-Себастополис, Геюнэсса, Фанагория, Гермонасса-Тьмутаракань, Томы, Истрия, Веляус, Калос, Лимен, Нимфей, Пантикапей, Сугдея, Феодосия, Херсонес-Корсунь, Китей, Зефирий, Акра, Ольвия.

- **Эгейское море**: Фей, Лорензон, Кардамиль, Минна, Тигани, Пилус, Калидон, Эритрея, Смирна, Теос, Нотиум, Милет, Баргилия, Андриасмира. [^98]

**English:**

**LIST OF SUNKEN CITIES**

- **Adriatic Sea**: Vibion, Matamauto, Spina, Epidaurus Illyrian.

- **Azov Sea**: Taganrog.

- **Caspian Sea**: Abeskun, Bandovyan, Kegnya-Bilgakh, Naryn-Kale, Terki.

- **Eastern Mediterranean**: Helika, Corinth, Piraeus, Githion, Faros, Atlitus, Caesarea, Caesarea, Vourgounda, Tristomo, Lefkos, Vathipotamos, Salamin, Mariumpolis, Paphos, Kourias, Amathos, Mochlos, Chersonesos, Panormus, Metalon, Amnissas, Moni Arvi, Elounda, Psira, Spinalonga, Itanos, Kapa Plaka, Zakros, Ampelos, Sidon, Tyre, Apollonia, Ptolemaida, Tauhira, Arvad, Aifusa, Taps

- **Western Mediterranean**: Iol, Vaia, Pozzuoli, Mizenum, Fos.

- **Black Sea**: Apollonia, Dionysopolis, Mesembria, Odessos, Anacopia, Dioscuria-Sebastopolis, Geunesse, Phanagoria, Hermonassa-Tjmutarakan, Tomy, Istria, Velyaus, Kalos, Limen, Nymphaeum, Pantikapeum, Sugdaia, Feodosia, Chersonesos-Korsun, Kitay, Zephyr, Akra, Olbia.

- ** Aegean Sea**: Fey, Lorenzon, Cardamil, Minna, Tigani, Pilus, Calidon, Eritrea, Smyrna, Theos, Notium, Miletus, Bargilia, Andriasmira. [^98]

**Russian:**

**19.5. ПОСЛЕДНЯЯ ТРАНСГРЕССИЯ В РЕГИОНЕ**

**English:**

**19.5. LAST TRANSGRESSION IN THE REGION**

**Russian:**

В 1808 году уровень моря у Мезии (Румыния) поднялся на 4 метра, и важность этого факта сложно переоценить.

**English:**

In 1808, the sea level of Mezia (Romania) rose by 4 meters, and the importance of this fact is difficult to overestimate.

**Russian:**

**19.6. МАССОВЫЕ ЗАХОРОНЕНИЯ НА МАНЫЧЕ**

**English:**

**19.6. MASS GRAVES ON THE MANYCHE**

**Russian:**

Первым делом смотрим, как выглядит реальный Манычский курган - без скифского золота.

**English:**

The first thing is to see how the real Manych barrow looks like - without the Scythian gold.

**Russian:**

**СВИДЕТЕЛЬСТВО ОЧЕВИДЦА**

**English:**

**WITNESS TESTIMONY**

**Russian:**

Когда рыли каналы для орошения, много раскопали на трассе курганов. Если одиночное захоронение, там все в норме. Одежда, утварь, оружие. В курганах же где сотнями скелеты свалены, ничего подобного нет от слова совсем. Одни кости. Наваленные как попало. Ни тряпочки, ни железки.

**English:**

When they were digging canals for irrigation, a lot of mounds were excavated along the route. If it's a single burial, it's all normal. Clothes, utensils, weapons. In the mounds where skeletons are piled up by the hundreds, there's nothing like that at all. Just bones. All piled up randomly. Not a rag or a piece of iron.

**Russian:**

Сотни курганов... Курганный могильник "Киевка-1" это тот, который размывает. Кости присыпаны метровым слоем от подошвы до вершины. Пытались рыть на вершине окопчик для охоты на утку, через сантиметров 70 пошли сплошные кости. Курган 8 метров высотой. Там полк закопан. Я думаю, что если нанести координаты курганов на карту, они покажут береговую линию разлива Маныча [^128].

**English:**

Hundreds of barrows... Kurgan burial mound "Kievka-1" is the one that erodes. The bones are covered with a meter layer from the bottom to the top. We tried to dig a trench on the top for duck hunting, after 70 centimeters there were solid bones. The barrow was 8 meters high. There's a regiment buried there. I think that if you put the coordinates of the mounds on the map, they will show the shoreline of the Manych spill [^128].

**Russian:**

**КОЛИЧЕСТВО КУРГАНОВ**

**English:**

** NUMBER OF MOUNDS**

**Russian:**

В Приказе Министерства культуры Ставропольского края от 15 мая 2006 года N 56 перечислены 115 курганных могильников и **[^529]** курганных насыпей (полный список в Приложении № 44). В Калмыкии исследовано **[^1289]** курганов, а общее их количество оценивают в тысячи. Е. Д. Фелицын, кубанский войсковой офицер составил карту курганов Краснодарского края, куда их вошло **[^6000]**. Как минимум, несколько сотен курганов есть и в Ростовской области.

**English:**

The Order of the Ministry of Culture of the Stavropol Territory of 15 May 2006 No. 56 lists 115 kurgan burial mounds and **[^529]** kurgan mounds (the full list is in Appendix No. 44). In Kalmykia, **[^1289]** mounds have been investigated, and their total number is estimated at thousands. Е. D. Felitsyn, a Kuban army officer, made a map of the kurgans of the Krasnodar region, which included them **[^6000]**. At least several hundred kurgans exist in the Rostov region as well.

**Russian:**

**КОММЕНТАРИЙ**

**English:**

**COMMENTARY**

**Russian:**

Переток по Манычу вовлекал, прежде всего, поверхностные воды Каспия - те, в которых плавают раздувшиеся трупы. В такой ситуации по берегам Манычской перетоки просто обязаны были образоваться наносы из трупов, подобные выброшенным на берег водорослям - полосой.

**English:**

The Manich overflow involved, first of all, the surface waters of the Caspian Sea - those in which bloated corpses were floating. In such a situation, sediments of corpses, similar to algae thrown on the shore - a strip - simply had to form along the banks of the Manich overflow.

**Russian:**

Для Романовых было бы логичным организовать сбор этих компактных наносов останков в курганы с последующей их присыпкой грунтом. Поэтому, действительно, расположение курганов с массовыми захоронениями должно вторить берегу Маныча в период активного перетока.

**English:**

It would have been logical for the Romanovs to organize the collection of these compact deposits of remains into barrows, followed by their covering with soil. Therefore, indeed, the location of the mounds with mass graves should echo the banks of the Manych during the period of active overflow.

**Russian:**

Одежды на костях нет, и это означает, что сборщики костей собирали именно кости, а остатки одежды и личных предметов так и должны лежать полосой там, где прежде лежали трупы. Такая полоса, будь она найдена, представляла бы колоссальный археологический интерес, поскольку отражала бы единовременный срез всех культур прикаспийской зоны.

**English:**

There are no clothes on the bones, and this means that the collectors of bones collected just bones, and the remains of clothing and personal items should lie in a strip where the corpses used to lie. Such a strip, if it were found, would be of enormous archaeological interest, as it would reflect a single slice of all the cultures of the Caspian Sea area.

**Russian:**

В русской прессе середины XIX века (1842-1849) было сообщение о найденном торчащим из глинистого берега Маныча древнеримском корабле великолепной сохранности.

**English:**

In the Russian press of the middle of the XIX century (1842-1849) there was a report about an ancient Roman ship, perfectly preserved, which was found sticking out of the Manich shore.

**Russian:**

**СЕРБСКИЙ АНАЛОГ МАНЫЧА**

**English:**

**SERBIAN COUNTERPART TO MANIC **

**Russian:**

Википедия. Мёзия. Когда Приск в составе византийского посольства проезжал в 448 году через Наисс (нынешний сербский Ниш), он нашёл его «безлюдным и разрушенным неприятелями... ***[по берегу реки всё было покрыто костями]*** убитых в сражении».

**English:**

Wikipedia. Moesia. When Priscus, as part of a Byzantine embassy, passed through Naiss (present-day Serbian Nis) in 448, he found it "deserted and destroyed by the enemy... ***[along the river bank everything was covered with bones]*** killed in battle".

**Russian:**

**19.7. ПОТОП ИЗ-ЗА РОСТА ОКЕАНИЧЕСКИХ ХРЕБТОВ**

**English:**

**19.7. FLOOD DUE TO RISING OCEAN RIDGES**

**Russian:**

В целом земли затоплены даже там, где море должно бы отступить. Доггерленд затонул метров на 200, Берингия -- на 135 метров, а Камчатка, обязанная подняться, в итоге затонула на 126 метров. Почти целиком затонул плейстоценовый материк, частью которого является Новая Зеландия [^99]. Причина: рост срединных океанических хребтов, что уменьшило емкость бассейнов.

**English:**

As a whole the grounds are flooded even where the sea should recede. Doggerland sank 200 meters, Beringia sank 135 meters, and Kamchatka, obliged to rise, ended up sinking 126 meters. The Pleistocene continent, of which New Zealand is [^99] a part, sank almost entirely. The reason: the growth of mid-ocean ridges, which reduced the capacity of the basins.

**Russian:**

Катастрофический этап роста срединно-океанических хребтов представляется очевидным следствием сдавливания геоида противоположно заряженными полярными областями. Спрединг (раздвигание плит) не справлялся с новым уровнем давления снизу, и хребты поднялись, а емкость бассейнов океанов уменьшилась.

**English:**

The catastrophic stage of the growth of mid-ocean ridges seems to be an obvious consequence of the geoid squeezing by the oppositely charged polar regions. Spreading (plate spreading) could not cope with the new level of pressure from below, and the ridges rose and the ocean basin capacity decreased.

**Russian:**

Возможен и вариант механической компенсации: хребты растут, а шельфы тонут -- как на качели-балансире из доски.

**English:**

A variant of mechanical compensation is also possible: the ridges grow and the shelves sink -- like on a seesaw from a plank.

**Russian:**

Рис. 69. Срединно-атлантический хребет

**English:**

Figure 69. Mid-Atlantic Ridge

<!-- Local
[![](Step by step_files/52D0A5D180D0B5D0B1D0B5D182.png#clickable)](Step by step_files/52%2B%25D0%25A5%25D1%2580%25D0%25B5%25D0%25B1%25D0%25B5%25D1%2582.png)
-->

[![](https://1.bp.blogspot.com/-_nuFj5gevcA/XlFB5u1c_0I/AAAAAAAADIQ/TFX6rbRUSeQvVgJZB0lGsYlA7DMuK3UswCLcBGAsYHQ/s1600/52%2B%25D0%25A5%25D1%2580%25D0%25B5%25D0%25B1%25D0%25B5%25D1%2582.png#clickable)](https://1.bp.blogspot.com/-_nuFj5gevcA/XlFB5u1c_0I/AAAAAAAADIQ/TFX6rbRUSeQvVgJZB0lGsYlA7DMuK3UswCLcBGAsYHQ/s1600/52%2B%25D0%25A5%25D1%2580%25D0%25B5%25D0%25B1%25D0%25B5%25D1%2582.png)

**Russian:**

Для побережий такое затопление может идти неодинаково, например, в зависимости от активности и близости срединно-океанических хребтов.

**English:**

For coasts, such inundation may occur in different ways, for example, depending on the activity and proximity of mid-ocean ridges.

**Russian:**

**19.8. ОБРАЗОВАНИЕ ФЬОРДОВ**

**English:**

**19.8. FJORD FORMATION**

**Russian:**

Фьорды, то есть, затопленные горные долины характерны, прежде всего, для полярных областей. Хорошие примеры: Норвегия и Антарктида.

**English:**

Fjords, i.e. flooded mountain valleys, are primarily characteristic of the polar regions. Good examples: Norway and Antarctica.

**Russian:**

Рис. 70. Рельеф поверхности Антарктиды без ледникового покрова [^100]

**English:**

Figure 70. Surface topography of Antarctica without ice sheet [^100]

<!-- Local
[![](Step by step_files/53D0A4D18CD0BED180D0B4D18B.jpg#clickable)](Step by step_files/53%2B%25D0%25A4%25D1%258C%25D0%25BE%25D1%2580%25D0%25B4%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-kH4qo9OfO7Q/XlFCSH6tIYI/AAAAAAAADIY/R9azTz9xepYizlksbtoIweoj7-Wp_UM-ACLcBGAsYHQ/s1600/53%2B%25D0%25A4%25D1%258C%25D0%25BE%25D1%2580%25D0%25B4%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-kH4qo9OfO7Q/XlFCSH6tIYI/AAAAAAAADIY/R9azTz9xepYizlksbtoIweoj7-Wp_UM-ACLcBGAsYHQ/s1600/53%2B%25D0%25A4%25D1%258C%25D0%25BE%25D1%2580%25D0%25B4%25D1%258B.jpg)

**Russian:**

Электрическая катастрофа в Арктике определенно отразилась и на Антарктике, - как на втором электроде в растворе электролита. Результат: сдавливание геоида противоположно заряженными полярными областями. Но могут быть еще две крупных причины: смещение оси вращения Земли и рост срединно-океанических хребтов.

**English:**

The electrical catastrophe in the Arctic was definitely reflected in the Antarctic as well - as a second electrode in an electrolyte solution. The result: squeezing of the geoid by oppositely charged polar regions. But there could be two other major causes: the shift in the Earth's axis of rotation and the growth of mid-ocean ridges.

**Russian:**

**19.9. ОТРЫВ ЛЕДОВЫХ ЩИТОВ**

**English:**

**19.9. BREAKAWAY ICE SHEETS**

**Russian:**

Судя по глубине затопления Доггерленда, Гренландия опустилась метров на 200. Это означает затопление, отрыв и всплывание кусков ледового щита по всему побережью Гренландии. Исландия была блокирована паковыми льдами с перерывами с 1420 г. по 1822 г., и в английском языке под паковым льдом подразумевают лед, оторвавшийся от ледников на суше. Это означает, что одной из дат затопления части Гренландии может быть 1420 год.

**English:**

Judging by the depth of the Doggerland sinking, Greenland has sunk by 200 meters. This means that chunks of the ice sheet along the entire coast of Greenland have sunk, broken away and surfaced. Iceland was blocked by pack ice intermittently from 1420 to 1822, and in English, pack ice refers to ice that broke away from glaciers on land. This means that one of the dates of inundation of part of Greenland could be 1420.

**Russian:**

**19.10. КОМПЛЕКСНОЕ ЗАТОПЛЕНИЕ АТЛАНТИКИ**

**English:**

**19.10. COMPREHENSIVE ATLANTIC INUNDATION**

**Russian:**

В Северной Атлантике затопление земель должно иметь комплексный характер: как из-за смещения оси, так и из-за роста хребтов.

**English:**

In the North Atlantic, land inundation must be complex: due to both axis displacement and ridge growth.

**Russian:**

**19.11. ДАТЫ БЕЗВОЗВРАТНОГО ЗАТОПЛЕНИЯ ЧАСТИ ЗЕМЕЛЬ**

**English:**

**19.11. DATES OF IRRETRIEVABLE INUNDATION OF PART OF THE LAND**

**Russian:**

Атлантический регион, описание есть для каждой даты.

**English:**

Atlantic region, there is a description for each date.

**Russian:**

Франция 1696

**English:**

France 1696

**Russian:**

Дания 1362

**English:**

Denmark 1362

**Russian:**

Бельгия 1375, 1477, 1530

**English:**

Belgium 1375, 1477, 1530

**Russian:**

Германия 1287, 1362, 1436, 1477, 1634, 1703

**English:**

Germany 1287, 1362, 1436, 1477, 1634, 1703

**Russian:**

Британия 1287, 1347, 1348, 1356, 1362, 1385, 1607, 1665, 1688, 1703, 1755,

**English:**

Britain 1287, 1347, 1348, 1356, 1362, 1385, 1607, 1665, 1688, 1703, 1755,

**Russian:**

Голландия 838, 1164, 1170, 1196, 1219, 1248, 1280, 1282, 1287, 1362, 1401, 1404, 1421, 1424, 1470, 1477, 1509, 1530, 1532, 1551, 1570, 1583, 1584, 1632, 1703

**English:**

Holland 838, 1164, 1170, 1196, 1219, 1248, 1280, 1282, 1287, 1362, 1401, 1404, 1421, 1424, 1470, 1477, 1509, 1530, 1532, 1551, 1570, 1583, 1584, 1632, 1703

**Russian:**

**19.12. ЗАТОНУВШИЕ ГОРОДА АТЛАНТИКИ**

**English:**

**19.12. SUNKEN CITIES OF THE ATLANTIC**

**Russian:**

Рассмотрим конкретные населенные пункты.

**English:**

Consider specific localities.

**Russian:**

**АНГЛИЯ**

**English:**

**ANGLIA**

**Russian:**

В 1287 году разрушены и перенесены повыше города Винчелси и Брумхилл

**English:**

In 1287 the towns of Winchelsea and Broomhill were destroyed and relocated

**Russian:**

С 1362 года город Равенсродд полностью под водой, но виноваты не просадки, а штормы.

**English:**

Since 1362 the town of Ravensrodd has been completely under water, but storms, not subsidence, are to blame.

**Russian:**

В 1607 году в районе Бристоля затонуло 520 кв. км суши, но это тоже не считается.

**English:**

In 1607, 520 square kilometres of land sank near Bristol, but that doesn't count either.

**Russian:**

В 1665 году в Южной Англии затонул город Брайтон, ныне он примерно в 40 метрах вниз.

**English:**

In 1665 the town of Brighton sank in Southern England, now about 40 metres down.

**Russian:**

Город Данвич затонул в девять приемов в 1286, 1287, 1328, 1347, 1348, 1385, 1688, 1717, 1755 годах.

**English:**

The town of Dunwich sank in nine receptions in 1286, 1287, 1328, 1347, 1348, 1385, 1688, 1717, 1755.

**Russian:**

**ГОЛЛАНДИЯ**

**English:**

**GOLLAND**

**Russian:**

С 1248 по 1632 год затонули города Гронинген, Дорт, Слидрехт, Ваард, Схевенинген, Вальхерен, Арнемуден, Реймерсваль, Саифтинг, Oostmanskapelle. Синхронно целыми провинциями тонули и сельские земли, но названий деревень не сохранилось.

**English:**

Between 1248 and 1632 the towns of Groningen, Dort, Slidrecht, Vaard, Scheveningen, Walheren, Arnemuden, Reimerswal, Saifting and Oostmanskapelle sank. Simultaneously, whole provinces also sank, but the names of the villages have not survived.

**Russian:**

**ГЕРМАНИЯ**

**English:**

**GERMANY**

**Russian:**

В 1362 году затонул город Рангхолт на острове Странд. Словосочетание «untergegangenes Bistum» означает как раз «затонувшая епархия», однако большая часть епархий Германии, числившихся затонувшими, ныне находится на суше. Или море отступило, или население вместе с топонимами переместилось.

**English:**

In 1362 the town of Rangholt on the island of the Strand sank. The phrase "untergegangenes Bistum" means exactly "sunken diocese", but most of the dioceses in Germany that were listed as sunken are now on land. Either the sea has receded, or the population, along with the toponyms, has moved.

**Russian:**

Das Bistum Merseburg ist ein untergegangenes Bistum.

**English:**

Das Bistum Merseburg ist ein untergegangenes Bistum.

**Russian:**

Das Bistum Halberstadt ist ein untergegangenes Bistum mit Sitz in Halberstadt.

**English:**

Das Bistum Halberstadt ist ein untergegangenes Bistum mit Sitz in Halberstadt.

**Russian:**

Das Bistum Havelberg ist ein untergegangenes Bistum mit Sitz in Havelberg im heutigen Landkreis Stendal.

**English:**

Das Bistum Havelberg ist ein untergegangenes Bistum mit Sitz in Havelberg im heutigen Landkreis Stendal.

**Russian:**

Es geht zuruck auf ein untergegangenes Bistum der antiken Stadt Adramyttion im Nordwesten Kleinasiens

**English:**

Es geht zuruck auf ein untergegangenes Bistum der antiken Stadt Adramyttion im Nordwesten Kleinasiens

**Russian:**

Das `Bistum Chiemsee` ist ein untergegangenes Bistum der Romisch-Katholischen Kirche.

**English:**

Das Bistum Chiemsee ist ein untergegangenes Bistum der Romisch-Katholischen Kirche.

**Russian:**

Das `Bistum Bremen` ist ein untergegangenes Bistum der romisch-katholischen Kirche in Deutschland.

**English:**

Das Bistum Bremen ist ein untergegangenes Bistum der romisch-katholischen Kirche in Deutschland.

**Russian:**

Ein Weihbischof wird immer auf ein untergegangenes Bistum geweiht (i.p.i. = in partibus infidelibus).

**English:**

Ein Weihbischof wird immer auf ein untergegangenes Bistum geweiht (i.p.i. = in partibus infidelibus).

**Russian:**

Рис. 71. Затонувшие епархии

**English:**

Figure 71. Sunken dioceses

<!-- Local
[![](Step by step_files/54Bistum.png#clickable)](Step by step_files/54%2BBistum.png)
-->

[![](https://1.bp.blogspot.com/--bfhxAX8IhQ/XlFCg9KHXAI/AAAAAAAADIc/1xnFvmq4KlcjfHSCCnRx0l5RAj87mxdCQCLcBGAsYHQ/s1600/54%2BBistum.png#clickable)](https://1.bp.blogspot.com/--bfhxAX8IhQ/XlFCg9KHXAI/AAAAAAAADIc/1xnFvmq4KlcjfHSCCnRx0l5RAj87mxdCQCLcBGAsYHQ/s1600/54%2BBistum.png)

**Russian:**

**ПОРТУГАЛИЯ**

**English:**

**PORTUGAL**

**Russian:**

Старая набережная Лиссабона после землетрясения 1755 года лежит глубоко под водой. Сам город стоит выше и поэтому затонувшим не считается.

**English:**

Lisbon's old seafront lies deep under water after the 1755 earthquake. The city itself stands taller and therefore is not considered sunken.

**Russian:**

**ВЕСТ-ИНДИЯ**

**English:**

**WEST INDIA**

**Russian:**

1692 г. Порт-Ройал

**English:**

1692 Port Royal

**Russian:**

**США, ФЛОРИДА**

**English:**

** USA, FLORIDA**

**Russian:**

Точной даты затопления города Сент-Огастин нет. Предположительно это 1669 год (в этот же год затонули Терки на Каспии), когда был издан приказ о переносе крепости подальше на сушу.

**English:**

There is no exact date of the sinking of the town of St. Augustine. Presumably it was 1669 (the same year the Terki on the Caspian Sea sank), when an order was issued to move the fortress further inland.

**Russian:**

**19.13. ПОДЪЕМЫ ПОБЕРЕЖЬЯ**

**English:**

**19.13. COASTAL UPLIFTS**

**Russian:**

Берега должны подниматься, прежде всего, на Дальнем Востоке и в Южной Америке. Наибольшее поднятие произошло 8200 лет назад, когда высоко на берегах Чили (Атакама), Аргентины и Аляски оказались полные скелеты китов. Но и в исторический период остаточные процессы еще идут.

**English:**

The shores should be rising primarily in the Far East and South America. The greatest uplift occurred 8200 years ago, when whale skeletons were found high up on the shores of Chile (Atacama), Argentina and Alaska. But even in the historical period, residual processes are still going on.

**Russian:**

1822 г. землетрясение 19 ноября 1822 года подняло все Чили

**English:**

1822 An earthquake on 19 November 1822 lifted all of Chile

**Russian:**

1822 г. В Вальпараисо корабль, разбившийся у берега, оказался после землетрясения совершенно на суше

**English:**

1822 In Valparaiso, a ship wrecked off the coast was completely on land after an earthquake

**Russian:**

1822 г. Берега озера Квинтеро, соединяющегося с морем, были подняты, и очень высоко, над уровнем воды

**English:**

1822 The shores of Lake Quintero, connecting with the sea, were raised, and very high, above the water level

**Russian:**

В Чили Дарвин стал свидетелем сильного землетрясения 20 февраля 1835 года, и видел признаки, указывающие на то, что земля только что поднялась. Этот поднявшийся пласт включал раковины двустворчатых моллюсков, которые оказались выше уровня высокого прилива.

**English:**

In Chile, Darwin witnessed a massive earthquake on 20 February 1835 and saw signs indicating that the land had just risen. This uplifted stratum included bivalve shells that appeared above the high tide.

**Russian:**

**ПРИМЕЧАНИЕ**: в Чили прилив достигает 3-5 метров, то есть, в 1835 году побережье Чили на этом участке поднялось более чем на 5 метров.

**English:**

**NOTE**: In Chile, the tide reaches 3-5 meters, which means that in 1835, the Chilean coast in this area rose by more than 5 meters.

**Russian:**

**19.14. ПОВОДКИ УЧАСТКОВ КОРЫ ВИНТОМ**

**English:**

**19.14. BARK SECTION LEASHES WITH SCREW**

**Russian:**

После смещения оси вращения Земли массы геоида, включая кору, тоже смещаются, чтобы занять новое, оптимальное для центробежных сил положение. Максимум смещения на север проходит по Атлантике, на 40° западной долготы, а максимум смещения на юг проходит по Дальнему Востоку, на 140° восточной долготы. Результат: каждый цельный участок коры испытывает конфликт сил, и участок ведет винтом. В Старом Свете вниз должно вести юго-восточный и северо-западный углы бассейнов; в Новом Свете -- юго-западный и северо-восточный.

**English:**

After the shift of the Earth's rotation axis, the masses of the geoid, including the crust, also shift to occupy a new, optimal position for the centrifugal forces. The maximum northward displacement passes through the Atlantic, at 40° West, and the maximum southward displacement passes through the Far East, at 140° East. The result: each solid section of the crust experiences a conflict of forces, and the section is propeller driven. In the Old World, the southeast and northwest corners of the basins should lead downward; in the New World -- southwest and northeast.

**Russian:**

Рис. 72. Варианты поводки бассейнов

**English:**

Figure 72. Pool leash options

<!-- Local
[![](Step by step_files/55D09FD0BED0B2D0BED0B4D0BAD0B0.png#clickable)](Step by step_files/55%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png)
-->

[![](https://1.bp.blogspot.com/-2Wz6-jU8r8o/XlFCrBu9SDI/AAAAAAAADIk/7DDRfIXURU4dnXrUGHbUV596cKocn9yLgCLcBGAsYHQ/s1600/55%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png#clickable)](https://1.bp.blogspot.com/-2Wz6-jU8r8o/XlFCrBu9SDI/AAAAAAAADIk/7DDRfIXURU4dnXrUGHbUV596cKocn9yLgCLcBGAsYHQ/s1600/55%2B%25D0%259F%25D0%25BE%25D0%25B2%25D0%25BE%25D0%25B4%25D0%25BA%25D0%25B0.png)

**Russian:**

Иольдиевое море именно так и повело. Точно так же повело и Каспий, переживший трансгрессии в 1669 и 1728 годах. Цитаты: «От устья Тюменки до моря расстояние сокращалось - сначала 30 верст, потом 5, а в 1669 г. Терки пришлось перенести из-за «потопления города морскою водою» [101; 102]. «...город Терки в 1728 году срыт... После того место, где стоял город Терки, затоплено морем, и ныне совсем не видно» [^103].

**English:**

The Ioldian Sea behaved in exactly the same way. The Caspian Sea, which experienced transgressions in 1669 and 1728, behaved the same way. Quotes: "From the mouth of the Tyumenka to the sea the distance was shrinking - first 30 versts, then 5, and in 1669 Terki had to be relocated because of the "sinking of the town by the sea water" [101; 102]. In 1728 "...the town of Terki was dug up. After that the place where was the town of Terki was flooded by the sea and now it is not seen at all" [^103]. [^103].

**Russian:**

Красными стрелками показаны затонувшие ныне области дельты Узбоя и дельты Терека и поднявшиеся области устья Куры и Мангышлака. Закономерно, что в нынешней пойме Куры населенных пунктов, основанных ранее середины XIX века, нет. Город-крепость Терки, судя по документам, следует искать в 30 верстах от береговой линии на глубине около 40 метров. То, что описывается, как трансгрессия или регрессия моря, часто и то, и другое одновременно.

**English:**

The red arrows show the now sunken areas of the Uzboy and Terek deltas and the raised areas of the mouths of the Kura and Mangyshlak. It is logical that there are no settlements established earlier than the middle of the 19th century in the present floodplain of the Kura River. Judging by the documents, the fortress-city of Terki should be sought in 30 versts from the shoreline at a depth of about 40 meters. What is described as sea transgression or regression is often both at the same time.

**Russian:**

Рис. 73. Очертания Каспийского моря в 1720 году [^104], до скручивания ложа и современные.

**English:**

Figure 73. Outlines of the Caspian Sea in 1720 [^104], prior to the twisting of the bed and present day.

<!-- Local
[![](Step by step_files/56D09AD0B0D181D0BFD0B8D0B91720.png#clickable)](Step by step_files/56%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9%2B1720.png)
-->

[![](https://1.bp.blogspot.com/-OE-J7_zhnXk/XlFCwri6UeI/AAAAAAAADIo/wPXBrhiPlf0wbObdBb-SYcMxp89xSFs_wCLcBGAsYHQ/s1600/56%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9%2B1720.png#clickable)](https://1.bp.blogspot.com/-OE-J7_zhnXk/XlFCwri6UeI/AAAAAAAADIo/wPXBrhiPlf0wbObdBb-SYcMxp89xSFs_wCLcBGAsYHQ/s1600/56%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9%2B1720.png)

**Russian:**

Дата затопления города Терки (1669) не уникальна; в том же 1669 году затонувшую в океане крепость города Сент-Огастин во Флориде решено отстраивать заново [^105], - развалины старой крепости были обнаружены во время сильного отлива в 1871 году [^106]; на 1670 год приходится второе свидетельство об отклонении русла Амударьи [^107], а в 1665 году затонул город Брайтон в Южной Англии -- на те же самые 40 метров [^108].

**English:**

The date of the sinking of Terkey (1669) is not unique; in the same year, 1669, the Florida fortress of St. Augustine, sunk in the ocean, was chosen to be rebuilt[^105] - ruins of the old fortress were discovered during a high tide in 1871 [^106]; 1670 is the second evidence of the deviation of the Amu Darya riverbed [^107], and in 1665 the town of Brighton in Southern England sank - by the same 40 meters [^108].

**Russian:**

В этот же исторический период затонули несколько частей Голландии (1646, 1651, 1654, 1664), часть Греции (1650) и США (1649, 1657) и зафиксирован «шведский потоп» в Польше (1655-1660), в результате которого страна потеряла более трети населения.

**English:**

During the same historical period some parts of Holland (1646, 1651, 1654, 1664), a part of Greece (1650) and the USA (1649, 1657) have sunk and "the Swedish Flood" in Poland (1655-1660) as a result of which the country has lost more than third of the population is fixed.

**Russian:**

**ТРАНСГРЕССИЯ КАСПИЯ В 1669 ГОДУ**

**English:**

** THE TRANSGRESSION OF THE CASPIAN SEA IN 1669**

**Russian:**

Цитата: «От устья Тюменки до моря расстояние сокращалось - сначала 30 верст, потом 5, а в 1669 г. Терки пришлось перенести из-за «потопления города морскою водою» [101; 102].

**English:**

Citation: "From Tyumenka estuary to the sea the distance shortened - at first 30 versts, then 5, and in 1669 Terki had to be moved because of "sinking of the city by the sea water". [101; 102].

**Russian:**

На карте звездочками указаны оба положения крепости Терки, и перенос на 35 верст означает, что до начала трансгрессии 1669 года выкрашенная желтым цветом область была сушей. Есть и свидетельство. Александр Балацкий: Как бывший житель Мангистау подтверждаю: местные казахи говорили, что когда-то от форт Шевченко в Астрахань ходили на верблюдах.

**English:**

The map has asterisks indicating both positions of the Terky fort, and the 35 verst transfer means that the yellow-painted area was land before the 1669 transgression. There is also evidence. Alexander Balatsky: As a former resident of Mangistau I confirm: local Kazakhs said that once from Shevchenko fort to Astrakhan went on camels.

**Russian:**

Рис. 74. Трансгрессия Каспия в 1669 году (желтым)

**English:**

Figure 74. Caspian Sea transgression in 1669 (yellow)

<!-- Local
[![](Step by step_files/57D09AD0B0D181D0BFD0B8D0B9.png#clickable)](Step by step_files/57%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9.png)
-->

[![](https://1.bp.blogspot.com/-J1fi7y44xig/XlFC2pZ-kbI/AAAAAAAADIw/7HVR4FEcnaYARe0b_QRDa5xqr2fFLN_qACLcBGAsYHQ/s1600/57%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9.png#clickable)](https://1.bp.blogspot.com/-J1fi7y44xig/XlFC2pZ-kbI/AAAAAAAADIw/7HVR4FEcnaYARe0b_QRDa5xqr2fFLN_qACLcBGAsYHQ/s1600/57%2B%25D0%259A%25D0%25B0%25D1%2581%25D0%25BF%25D0%25B8%25D0%25B9.png)

**Russian:**

**СХОДНЫЕ ЛЕТОПИСНЫЕ СВИДЕТЕЛЬСТВА**

**English:**

**SIMILAR ANNALISTIC EVIDENCE**

**Russian:**

1325 г. Каспийское море прибывает каждый год на одну ладонь, и уже несколько хороших городов разрушены, так что в конце концов Каспийское море соединится с морем Таны

**English:**

1325 г. The Caspian Sea arrives every year by one palm, and several good cities have already been destroyed, so that eventually the Caspian Sea will join the Tana Sea

**Russian:**

1344 г. В Ven-tcheou в результате землетрясений переполнилось море

**English:**

1344 In Ven-tcheou the sea overflowed as a result of earthquakes

**Russian:**

1649 г. Обмеление реки Гвадалквивир, что не позволяло более морским судам швартоваться в Севилье

**English:**

1649 Shallowing of the Guadalquivir River, which no longer allowed ships to moor in Seville

**Russian:**

1812 г. В Миссисипи полоса земли длиною около 500 верст и шириною около 200 верст опустилась на восемь футов ниже своего первоначального уровня

**English:**

1812 г. In the Mississippi, a strip of land about 500 versts long and 200 versts wide has sunk eight feet below its original level

**Russian:**

1819 г. землетрясение на границе Индии и Пакистана, разрывы на местности и опускание тектонических блоков ниже уровня моря

**English:**

1819 India-Pakistan border earthquake, rupture and lowering of tectonic blocks below sea level

**Russian:**

1855 г. Новая Зеландия, береговая линия поднялась на 2 метра

**English:**

1855 New Zealand, the coastline has risen by 2 metres

**Russian:**

Полный вердикт по каждому случаю вынести сложно, однако важно, что на Пиренейском полуострове гавань Лиссабона затонула, а гавань Севильи поднялась, - так и должно быть.

**English:**

It is difficult to reach a full verdict on each case, but it is significant that on the Iberian Peninsula the harbour of Lisbon sank and the harbour of Seville rose - as it should be.

**Russian:**

**ГРАНИЦЫ ЭКСПЕДИЦИЙ ЕКАТЕРИНЫ II**

**English:**

**FRONTS OF CATHERINE THE GREAT'S EXPEDITIONS**

**Russian:**

В 1769 году Академия Наук отправила в Арало-Каспийский регион пять экспедиций [^139]:

**English:**

In 1769, the Academy of Sciences sent five expeditions to the Aral-Caspian region [^139]:

**Russian:**

- Оренбургскую экспедицию академика П. С. Палласа из Берлина;

**English:**

- Orenburg expedition of Academician P.S. Pallas from Berlin;

**Russian:**

- Астраханскую экспедицию академика С. Г. Гмелина из Тюбингена;

**English:**

- Academician S. G. Gmelin's Astrakhan expedition from Tübingen;

**Russian:**

- Астраханскую экспедицию адъюнкта Академии И. А. Гюльденштедта из Риги;

**English:**

- Astrakhan expedition of Academy adjunct I.A. Guldenstedt from Riga;

**Russian:**

- Оренбургскую экспедицию ботаника С.-Петербургского медицинского сада И. П. Фалька из Швеции;

**English:**

- Orenburg expedition botanist St. Petersburg Medical Garden IP Falk from Sweden;

**Russian:**

- Оренбургскую экспедицию адъюнкта Академии доктора Ивана Лепехина.

**English:**

- The Orenburg expedition of Dr. Ivan Lepekhin, adjunct of the Academy.

**Russian:**

В это же время и с той же целью состоялись "экскурсии" адъюнкта Георги и Н. П. Рычкова. Маршруты экспедиций хорошо видны по тем населенным пунктам, в которых ученые побывали.

**English:**

At the same time and with the same purpose took place "excursions" of Adjunct Georgi and N.P.Rychkov. The routes of the expeditions can be clearly seen from the settlements in which the scientists visited.

**Russian:**

Рис. 75 Маршруты экспедиций

**English:**

Figure 75 Expedition routes

<!-- Local
[![](Step by step_files/D09FD0B0D0BBD0BBD0B0D181.png#clickable)](Step by step_files/%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581.png)
-->

[![](https://1.bp.blogspot.com/-ljcmfz4_YeE/XukWNSd0R3I/AAAAAAAADSY/itEqLxTsIUAJmPEKeyfjVxpVpXPXnY4uQCLcBGAsYHQ/s1600/%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581.png#clickable)](https://1.bp.blogspot.com/-ljcmfz4_YeE/XukWNSd0R3I/AAAAAAAADSY/itEqLxTsIUAJmPEKeyfjVxpVpXPXnY4uQCLcBGAsYHQ/s1600/%25D0%259F%25D0%25B0%25D0%25BB%25D0%25BB%25D0%25B0%25D1%2581.png)

**Russian:**

**ОСНОВНОЕ**:

**English:**

**MEMBERS ONLY**:

**Russian:**

Экспедиции назвали территорию Казахстана "Туранской котловиной" и пришли к выводу, что это - дно моря, стекшего по Кумо-Манычской впадине в Азов. Границы исследований экспедиций достаточно точно совпадают с государственными границами Республики Казахстан, и этих границ экспедиции за редким исключением не пересекают. Одну из основных поставленных задач - исследовать Аральский регион - ни одна из пяти экспедиций выполнить не смогла.

**English:**

The expeditions named the territory of Kazakhstan "Turan hollow" and came to the conclusion that it is the bottom of the sea flowing down the Kumo-Manych depression to Azov. The boundaries of the expeditions' research coincide rather precisely with the state boundaries of the Republic of Kazakhstan, and these boundaries are not crossed by the expeditions with rare exceptions. None of the five expeditions was able to achieve one of their main objectives - to explore the Aral Sea region.

**Russian:**

Виден интерес к устью Оби - месту перетока воды из Арктики по Тургайской ложбине в Мансийский бассейн и Хвалынское море и обратного стока из Мансийского бассейна в Карское море. Последний инцидент зафиксирован в устных преданиях населяющих Приобье коренных народов, и мог представлять для экспедиций самый живой интерес. Примерно в это же время одно из казахских племен поведало русскому офицеру предание о потопе, пережитом племенем на одной из сопок.

**English:**

The interest to the mouth of Ob is visible - a place of water overflow from Arctic along Turgai hollow to Mansiysky basin and Khvalynsky sea and back flow from Mansiysky basin to Kara sea. The last incident is fixed in the oral legends of the indigenous people inhabiting Priob'ye, and could be of the liveliest interest for expeditions. Approximately at the same time one of the Kazakh tribes told a Russian officer a legend about a flood experienced by the tribe on one of the hills.

**Russian:**

**19.15. ИЗМЕНЕНИЕ ВОДНОГО РЕЖИМА РЕК**

**English:**

**19.15. CHANGE OF WATER REGIME OF RIVERS**

**Russian:**

Крупнейшие реки мира текут по разломам. Причин две: а) так проще, б) на линии разлома водоносные слои разорваны, и грунтовые воды подпитывают текущую по разлому реку с обеих сторон. На сегодня около трети годового стока рек в Мировой океан формируется как раз за счёт подземных вод [^145].

**English:**

The largest rivers in the world flow along faults. There are two reasons for this: a) it's easier, and b) the aquifers are faulted and groundwater recharges the river on both sides of the fault. Today, about a third of the annual flow of rivers into the World Ocean is formed just at the expense of groundwater [^145].

**Russian:**

Есть реки, текущие не по разломам, - и в Африке, и в Азии, - однако в засушливые периоды они просто пересыхают. Сходная ситуация зафиксирована и на ранних этапах геологической эволюции: водные ресурсы суши ограничивались озероподобными водоемами. Для создания масштабных речных сетей разломы необходимы, - так Лена и Енисей текут строго на стыках разных плит, а Тобол и Обь, хотя и текут не по разлому, питаются уральскими реками, берущими начало тоже на стыке плит. Все эти разломы сонаправлены группе линеаментов, идущих на нынешний полюс, и здесь главная деталь в том, что две основные группы существующих линеаментов появились в тесной причинно-следственной связи со смещением оси вращения Земли -- синхронно с гибелью плейстоценовой мегафауны и началом экспансии человека.

**English:**

There are rivers that do not flow along faults - both in Africa and Asia - but in dry periods they simply dry up. A similar situation is documented in the earliest stages of geological evolution: water resources on land were confined to lake-like reservoirs. Large-scale river networks require faults - the Lena and Yenisei flow strictly along plate boundaries while Tobol and Ob, though not flowing along a fault, are fed by Ural rivers originating at the same plate boundary. All of these rifts are oriented by a group of lineaments going to the present pole, and the main detail here is that the two main groups of existing lineaments appeared in close causal connection with the shift in the Earth's rotation axis - synchronously with the death of the Pleistocene megafauna and the beginning of human expansion.

**Russian:**

Связь экспансии человека с появлением непересыхающих рек логична. Едва появились такие реки, появилась и возможность постоянных путешествий, то есть, обмена, а затем и торговли. Показательно, что в индийских преданиях указан конкретный мифологический акт освобождения Индрой вод из-под земли, после чего реки и потекли, - все это в тесной связи с убийством Змея, проглотившего Солнце, и освобождением утренней зари, что ясно перекликается с переворотом Земли по Джанибекову и сменой сторон света. При этом в мифах река Ганг была всегда, что тоже логично: Инд и Ганг обязаны стать первыми настоящими, то есть, питающимися подземными водами, реками на Земле -- сразу после стыковки двух плит -- Индостанской и Евразийской.

**English:**

The connection between human expansion and the emergence of non-drying rivers is logical. As soon as such rivers appeared, the possibility of constant travel, i.e. exchange and then trade, appeared. It is indicative that the Indian legends indicate a specific mythological act of Indra's liberation of waters from under the ground, after which the rivers flowed - all this in close connection with the killing of the Snake that swallowed the Sun and the liberation of the morning dawn, which clearly echoes the overturning of the Earth by Janibek and the change of sides of the world. And in the myths the river Ganges has always been, what is also logical: Indus and Ganges must become the first real, that is fed by underground water, rivers on Earth, immediately after the joining of the two plates, the Hindu and Eurasian.

**Russian:**

В отношении водного режима северных рек России можно утверждать, что четыре крупных события определенно связаны и произошли в одну геологическую эпоху:

**English:**

With regard to the water regime of Russia's northern rivers, it can be argued that four major events are definitely related and occurred in the same geological epoch:

**Russian:**

1) поводка плит с затоплением Берингии;

**English:**

1) of the slab leash with flooding of Beringia;

**Russian:**

2) появление группы линеаментов, направленных на Северный полюс;

**English:**

2) appearance of a group of lineaments directed to the North Pole;

**Russian:**

3) сток Мансийского бассейна в Карское море;

**English:**

3) the runoff of the Mansi Basin into the Kara Sea;

**Russian:**

4) формирование нынешних русел Оби, Енисея и Лены.

**English:**

4) formation of the current Ob, Yenisei and Lena channels.

**Russian:**

**19.16. СПЕЦИАЛЬНЫЕ СЛУЧАИ**

**English:**

**19.16. SPECIAL CASES**

**Russian:**

Скелеты ископаемых китов на суше в Египте вполне могут указывать на временную поводку Восточного Средиземноморья, опустившую Египет ниже уровня моря. Но это может быть и результат банального цунами.

**English:**

The fossil whale skeletons on land in Egypt may well indicate a temporary Eastern Mediterranean leash that lowered Egypt below sea level. But it could also be the result of a trivial tsunami.

**Russian:**

Ось вращения Земли не обязательно смещалась по прямой линии, а значит, возможны самые парадоксальные варианты местных потопов. Пустыни Гоби и Такла-Макан явственно несут следы воды, но откуда она пришла, - от Шантарских островов или из Желтого моря, - и не было ли влияния разломов, предстоит еще выяснять.

**English:**

The axis of rotation of the Earth did not necessarily shift in a straight line, which means that the most paradoxical variants of local floods are possible. The Gobi and Takla Makan deserts clearly bear traces of water, but where it came from - from the Shantar Islands or the Yellow Sea - and whether it was influenced by faults remains to be clarified.

**Russian:**

**20. ИЗМЕНЕНИЕ КОРМОВОЙ БАЗЫ**

**English:**

**20. CHANGES IN THE FEED BASE**

**Russian:**

Затопление и осушение шельфов уничтожает кормовую базу и роддома прибрежных животных, например, гигантских черепах или той же Стеллеровой коровы. Человек во всем винит себя, но в ситуации, когда все побережья всех материков в несколько приемов опускаются или поднимаются на 40-200 метров, ориентиры меняются, а кормовая база прибрежной фауны массово гибнет.

**English:**

Flooding and drying of shelves destroys the food base and maternity houses of coastal animals, for example giant turtles or the same Steller's sea cow. Man blames himself for everything, but in a situation when all coasts of all continents are lowered or raised by 40-200 meters in several steps, landmarks are changed and the foraging base of coastal fauna is massively destroyed.

**Russian:**

**21. СМЕЩЕНИЕ КЛИМАТИЧЕСКИХ ЗОН**

**English:**

**21. SHIFTING OF CLIMATIC ZONES**

**Russian:**

Синхронно со смещением оси вращения земли меняется и широтная ориентация муссонов и пассатов. Там, где сухие и холодные пассаты падают на море, они подходят к суше уже полными тепла и влаги, но если пассат падает на сушу, влаги взять неоткуда, и получается Сахара. Крымская средиземноморская фауна и флора становится реликтом. Западная часть Каспийского побережья выходит из зоны средиземноморского климата и входит в зону континентального.

**English:**

The latitudinal orientation of monsoons and trade winds changes synchronously with the shift in the Earth's rotation axis. Where dry and cold trade winds strike the sea, they approach the land with warmth and moisture, but if the trade winds strike the land, there is no place to take moisture, and Sahara appears. Crimean Mediterranean fauna and flora become relicts. The western part of the Caspian coast leaves the Mediterranean climate zone and enters the continental climate zone.

**Russian:**

Есть множество карт XVIII века с изображением Mer ou Baye de L'Ouest (Моря Западного), и эти порой откровенно фантастические карты отражают геологическую преемственность правильно. Еще в 1849 году Большой Бассейн в Неваде был полон соленых озер.

**English:**

There are many 18th century maps showing the Mer ou Baye de L'Ouest (Sea of the West), and these sometimes frankly fantastic maps reflect the geological continuity correctly. Back in 1849, the Great Basin in Nevada was full of salt lakes.

**Russian:**

Рис. 76. Большой бассейн эпохи плейстоцена и Море Западное в 1753 году

**English:**

Figure 76. The Great basin of the Pleistocene Epoch and the Western Sea in 1753

<!-- Local
[![](Step by step_files/58D091D0B0D181D181D0B5D0B9D0BD.jpg#clickable)](Step by step_files/58%2B%25D0%2591%25D0%25B0%25D1%2581%25D1%2581%25D0%25B5%25D0%25B9%25D0%25BD.jpg)
-->

[![](https://1.bp.blogspot.com/-0C8aBqEPt6o/XlFDF2LewKI/AAAAAAAADI4/F23P_EmxFm0HjD3Sm9E2lcZ7hZ-Oovv6ACLcBGAsYHQ/s1600/58%2B%25D0%2591%25D0%25B0%25D1%2581%25D1%2581%25D0%25B5%25D0%25B9%25D0%25BD.jpg#clickable)](https://1.bp.blogspot.com/-0C8aBqEPt6o/XlFDF2LewKI/AAAAAAAADI4/F23P_EmxFm0HjD3Sm9E2lcZ7hZ-Oovv6ACLcBGAsYHQ/s1600/58%2B%25D0%2591%25D0%25B0%25D1%2581%25D1%2581%25D0%25B5%25D0%25B9%25D0%25BD.jpg)

**Russian:**

По сути, Great Salt Lake и Арал высыхают синхронно, и для этих озер Ледниковый период -- общее и совсем недавнее прошлое. Это не преувеличение: Гумбольдт, посещая Россию, отметил, что вода из Аральского моря используется для полива, то есть, еще достаточно пресна, и все еще перетекает в Каспийское море.

**English:**

In fact, Great Salt Lake and the Aral Sea are drying up synchronously, and for these lakes the Ice Age is a common and very recent past. This is not an exaggeration: Humboldt, while visiting Russia, noted that water from the Aral Sea is used for irrigation, that is, is still quite fresh, and still flows into the Caspian Sea.

**Russian:**

Эрнан Кортес видел в Мексике 1519 года влажный климат и развитую ирригацию [^109], и даже в 1827 году город Мехико все еще стоит посреди огромного озера.

**English:**

Hernan Cortez saw Mexico in 1519 as having a humid climate and advanced irrigation [^109], and even in 1827 the city of Mexico City still stands in the middle of a huge lake.

**Russian:**

Рис. 77. Мехико в 1827 году

**English:**

Figure 77. Mexico City in 1827

<!-- Local
[![](Step by step_files/59D09CD0B5D185D0B8D0BAD0BE.png#clickable)](Step by step_files/59%2B%25D0%259C%25D0%25B5%25D1%2585%25D0%25B8%25D0%25BA%25D0%25BE.png)
-->

[![](https://1.bp.blogspot.com/-HVjUlhOR62g/XlFDLKj0x2I/AAAAAAAADJA/9cMv15ruTaIAN1Z1rcOmsDMYI94WMYPYQCLcBGAsYHQ/s1600/59%2B%25D0%259C%25D0%25B5%25D1%2585%25D0%25B8%25D0%25BA%25D0%25BE.png#clickable)](https://1.bp.blogspot.com/-HVjUlhOR62g/XlFDLKj0x2I/AAAAAAAADJA/9cMv15ruTaIAN1Z1rcOmsDMYI94WMYPYQCLcBGAsYHQ/s1600/59%2B%25D0%259C%25D0%25B5%25D1%2585%25D0%25B8%25D0%25BA%25D0%25BE.png)

**Russian:**

Исторические документы свидетельствуют о процветающей в XIV веке Средней Азии, а сегодня мы видим брошенные либо затопленные (Аральское море, Каспийское море) города. Археолог Fekri Hassan отыскал точный период, в котором количество поступающей в Египет вместе с сезонными дождями влаги резко сократилось, а число селений уменьшилось на порядок. Супруга Фекри, как и он, археолог, датировала кострища в Сахаре из влаголюбивой акции XV веком. В том же XV веке в Судане зафиксированы носороги и павианы, и для этого нужен климат саванны.

**English:**

Historical documents testify about prosperous Central Asia in XIV century, and today we see abandoned or flooded (Aral Sea, Caspian Sea) cities. Archaeologist Fekri Hassan has found the exact period in which the amount of moisture coming into Egypt together with the seasonal rains has sharply decreased, and the number of settlements has decreased by an order of magnitude. Fekri's wife, an archaeologist like him, dated the fire pits in the Sahara from the moisture-loving stock to the fifteenth century. Rhinoceroses and baboons were recorded in Sudan in the same 15th century, and this requires a savannah climate.

**Russian:**

**22.1. АКТИВИЗАЦИЯ ВУЛКАНОВ**

**English:**

**22.1. VOLCANO ACTIVATION**

**Russian:**

В силу появления новых сил сжатия и растяжения просыпаются вулканы, причем, в силу единовременности появления этих сил, синхронно во всех концах Земли. В 1829 году при оценке хода Лиссабонского землетрясения 1755 года даже родилась теория о подземных тоннелях, по которым внутренний огонь планеты мгновенно перемещается с континента на континент.

**English:**

Due to the appearance of new forces of compression and tension volcanoes wake up, and, due to the simultaneous appearance of these forces, synchronously in all ends of the Earth. In 1829, when assessing the course of the Lisbon earthquake of 1755, even a theory was born about the underground tunnels through which the inner fire of the planet instantly moves from continent to continent.

**Russian:**

***[Вулканическое течение]***, разделясь при горах Лаутербрунских, проникло в Германию, потрясло Франконию, Баварию; на Дунае потрясся Донаверт. ***[Соединительные проходы ]***сообщили удары в Голландию, Англию, Швецию, Норвегию. Совершенным разрушением города Квито, у подошвы горы Пихинки, Америка подтвердила ***[сообщение с древним светом]***.

**English:**

***[Volcanic current]***, dividing at the mountains of Lauterbrun, penetrated into Germany, shook Franconia, Bavaria; on the Danube shook Donavert. ***[Connecting passages ]*** reported blows to Holland, England, Sweden, Norway. By the perfect destruction of the city of Quito, at the foot of Mount Pihinka, America confirmed ***[communication with the ancient world]***.

**Russian:**

**22.2. ВЫХОДЫ ФЛЮИДОЛИТОВ**

**English:**

**22.2. FLUIDOLITE OUTPUTS**

**Russian:**

В такой ситуации гипотеза об активном формировании «пластилиновых» гор типа Эль-Фуэрте в Боливии (со следами человеческой деятельности или без них) выходами водно-грязевых потоков (холодных флюидолитов, геобетона, минерального туфа) [^127] вполне уместна. Прекратиться процесс выхода грязи и воды мог совсем недавно. Какая-то часть масс каменеет, а какая-то остается пластичной.

**English:**

In such situation the hypothesis of active formation of "plasticine" mountains like El Fuerte in Bolivia (with or without traces of human activity) by outputs of water-mud streams (cold fluidolites, geobeton, mineral tuff) [^127] is quite appropriate. The process of mud and water escape could stop only recently. Some part of masses stiffens, and some part remains plastic.

**Russian:**

Рис. 78. Два слоя неокаменевшей глины в слоях камня. Адыгея, Хаджох, недалеко от туристического объекта "Турецкий базар". Фото chispa1707.

**English:**

Fig. 78. Two layers of unrocked clay in layers of stone. Adygeya, Khadzhokh, near the tourist site "Turkish Bazaar". Photo by chispa1707.

<!-- Local
[![](Step by step_files/DSC_0013.jpg#clickable)](Step by step_files/DSC_0013.jpg)
-->

[![](https://1.bp.blogspot.com/-AOVxY2byE40/Xy53PV-p19I/AAAAAAAADc0/8EBSKazgLNULzkL2l2ROtSbZLUUIgUc9QCLcBGAsYHQ/s1388/DSC_0013.jpg#clickable)](https://1.bp.blogspot.com/-AOVxY2byE40/Xy53PV-p19I/AAAAAAAADc0/8EBSKazgLNULzkL2l2ROtSbZLUUIgUc9QCLcBGAsYHQ/s1388/DSC_0013.jpg)

**Russian:**

Рис. 79. Плейстоценовые моллюски меж селевой массой и синей вулканической глиной в Адыгее (в черте г. Майкоп). Фото chispa1707.

**English:**

Fig. 79. Pleistocene molluscs between mudflats and blue volcanic clay in Adygea (within the city of Maikop). Photo by chispa1707.

<!-- Local
[![](Step by step_files/59_002.jpg#clickable)](Step by step_files/59.2%2B%25D0%259C%25D0%25BE%25D0%25BB%25D0%25BB%25D1%258E%25D1%2581%25D0%25BA.jpg)
-->

[![](https://1.bp.blogspot.com/-DgVOTClTk6w/XlF8mbCYRaI/AAAAAAAADMY/MWIX1DvWxgk0VegWwODXa9bkn-mOj48dACLcBGAsYHQ/s1600/59.2%2B%25D0%259C%25D0%25BE%25D0%25BB%25D0%25BB%25D1%258E%25D1%2581%25D0%25BA.jpg#clickable)](https://1.bp.blogspot.com/-DgVOTClTk6w/XlF8mbCYRaI/AAAAAAAADMY/MWIX1DvWxgk0VegWwODXa9bkn-mOj48dACLcBGAsYHQ/s1600/59.2%2B%25D0%259C%25D0%25BE%25D0%25BB%25D0%25BB%25D1%258E%25D1%2581%25D0%25BA.jpg)

**Russian:**

Рис. 80. Частично окаменевшая вулканическая глина (г. Майкоп). Фото chispa1707.

**English:**

Fig. 80. Partially fossilized volcanic clay (Maikop). Photo by chispa1707.

<!-- Local
[![](Step by step_files/59.jpg#clickable)](Step by step_files/59.3%2B%25D0%259F%25D0%25BB%25D0%25B8%25D1%2582%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-baWR0X8u6P0/XlF8sKOg-oI/AAAAAAAADMc/iDKCSPnm3RIJSNFsMAse3Z6AzcvVWzWpQCLcBGAsYHQ/s1600/59.3%2B%25D0%259F%25D0%25BB%25D0%25B8%25D1%2582%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-baWR0X8u6P0/XlF8sKOg-oI/AAAAAAAADMc/iDKCSPnm3RIJSNFsMAse3Z6AzcvVWzWpQCLcBGAsYHQ/s1600/59.3%2B%25D0%259F%25D0%25BB%25D0%25B8%25D1%2582%25D0%25B0.jpg)

**Russian:**

**23. МНОГОМЕСЯЧНЫЕ ЗЕМЛЕТРЯСЕНИЯ**

**English:**

**23. MULTI-MONTH EARTHQUAKES**

**Russian:**

Многомесячные землетрясения свидетельствуют о постоянно идущей масштабной разрядке глобальных (то есть, не местных) тектонических напряжений, и смещение оси вращения Земли -- подходящий их первоисточник.

**English:**

Monthly earthquakes provide evidence of an ongoing large-scale discharge of global (i.e., not local) tectonic stresses, and the shift in the Earth's rotation axis is a suitable prime mover.

**Russian:**

430 г. В Константинополе происходили страшные землетрясения в продолжении четырех месяцев

**English:**

430 г. In Constantinople there were terrible earthquakes for four months

**Russian:**

732 г. Море в некоторых странах выбежало из пределов своих, и землетрясение продолжалось двенадцать месяцев

**English:**

732 The sea in some countries ran out of its limits, and the earthquake lasted twelve months

**Russian:**

1114 г. В Эрец-Исраэль серия землетрясений, продолжавшаяся 5 месяцев

**English:**

1114 г. In Eretz Yisrael a series of earthquakes lasting 5 months

**Russian:**

1170 г. В Израиле началась серия землетрясений, продолжавшаяся 5 месяцев

**English:**

1170 A series of earthquakes began in Israel which lasted 5 months

**Russian:**

1481 г. Землетрясения на Родосе. Крупнейшая серия, которая длилась 10 месяцев

**English:**

1481 Earthquakes in Rhodes. The largest series, which lasted 10 months

**Russian:**

1509 Землетрясение в Турции, Греции и дельте Нила несколько месяцев и на обширной территории

**English:**

1509 Earthquake in Turkey, Greece and the Nile Delta for several months and over a wide area

**Russian:**

1598 г. Землетрясения в Амассии и Чоруме (Турция), не прекращались в течение 4 месяцев, разрушено много зданий

**English:**

1598 Earthquakes in Amassia and Çorum (Turkey), continued for 4 months, destroyed many buildings

**Russian:**

1638 г. Землетрясения в провинции Шаньси (Китай), продолжались несколько месяцев

**English:**

1638 Earthquake in Shanxi Province, China, lasting several months

**Russian:**

1797 г. В продолжение почти 8-ми месяцев были потрясаемы Антильские острова

**English:**

1797 For nearly 8 months the Antilles were shaken

**Russian:**

**24. БЫСТРОЕ ЗАМЕРЗАНИЕ МОРЕЙ**

**English:**

**24. RAPID FREEZING OF THE SEAS**

**Russian:**

Разрядка напряжений после землетрясений приводит к падению давления в недрах, а значит, и падению температуры. Обратите внимание на несовпадение дат замерзания разных морей, то есть, причина замерзания, не затяжное понижение температуры на планете.

**English:**

Discharge of stresses after earthquakes leads to a drop in pressure in the bowels, and hence a drop in temperature. Note the non-coincidence of the dates of freezing of different seas, i.e., the cause of freezing, not the prolonged drop in temperature on the planet.

**Russian:**

Дельта Нила замерзала в 829 году и в зиму 1010-1011 гг.

**English:**

The Nile Delta froze in 829 and in the winter of 1010-1011.

**Russian:**

Средиземное море замерзало в 1323, 1326, 1621, 1769 годах.

**English:**

The Mediterranean Sea froze over in 1323, 1326, 1621, 1769.

**Russian:**

Адриатическое море замерзало в 859, 860, 1210, 1233, 1490, 1708, 1709 годах.

**English:**

The Adriatic Sea was frozen in 859, 860, 1210, 1233, 1490, 1708, 1709.

**Russian:**

Черное море замерзало в 299, 400, 557, 558, 703, 761, 761, 763, 763, 763, 763, 801, 1010, 1011, 1543, 1596, 1620, 1669, 1788 годах.

**English:**

The Black Sea was frozen in 299, 400, 557, 558, 703, 761, 761, 763, 763, 763, 801, 1010, 1011, 1543, 1596, 1620, 1669, 1788.

**Russian:**

Очень важно, что именно в 1326 году, когда Средиземное море целиком замерзло, вовсю шла уничтожившая город Сарай-Бату трансгрессия Каспийского моря. Тектонические плиты проседали и занимали новое положение.

**English:**

It is very important that in 1326, when the Mediterranean Sea was completely frozen, the Caspian Sea transgression, which destroyed the city of Saray-Batu, was in full swing. The tectonic plates were subsiding and occupying a new position.

**Russian:**

Вероятна ситуация, когда лед образуется на дне, в зоне контакта с остывшими недрами, а затем всплывает. Именно так мог образоваться парадоксальный черноморский лед 755 года: почти на 14 метров в глубину и всего на 100 миль от берега.

**English:**

It is likely that ice forms on the bottom, in the zone of contact with the cooled subsoil, and then floats up. This is exactly how the paradoxical Black Sea ice of 755 could have formed: almost 14 meters deep and only 100 miles from the shore.

**Russian:**

л. м. 6255, р. х. 755. Северная часть моря на сто миль от берега превратилась в камень на тридцать локтей (13,89 метра) в глубину

**English:**

l. m. 6255, p. x. 755. The northern part of the sea a hundred miles from the shore turned to stone thirty cubits (13.89 meters) deep

**Russian:**

Иногда такой придонный лед всплывает вместе с метаном, а значит, и вместе с погибшей морской фауной. Рыба в верхних слоях моря погибла и всплыла раньше, чем намерз, оторвался и всплыл придонный лед, а потому лежит поверх льда и кажется "выброшенной".

**English:**

Sometimes such near-bottom ice floats up together with methane and, hence, together with dead sea fauna. Fish in the upper sea layers died and surfaced before the near-bottom ice froze, broke off and surfaced, and therefore lies on top of the ice and seems to be "thrown out".

**Russian:**

л. м. 6101, р. х. 601. В том же году случилась жестокая зима, море покрылось льдом, и много рыбы выброшено было на лед

**English:**

l. m. 6101, p. h. 601. In the same year there was a severe winter, the sea was covered with ice, and many fish were thrown on the ice

**Russian:**

По этой же причине, из-за охлаждения недр, а не в силу похолодания атмосферы, в 1468 г. в Бургундии замерзло вино в подвалах.

**English:**

For the same reason, because of the cooling of the subsoil, and not because of the cooling of the atmosphere, the wine in the cellars of Burgundy froze in 1468.

**Russian:**

**25. ПЕРЕСЫХАНИЕ РЕК И РУЧЬЕВ**

**English:**

**25. DRYING UP OF RIVERS AND STREAMS**

**Russian:**

При смещении оси водоносные горизонты меняют наклон, и прекращается поступление воды в источники, колодцы и реки. Это обычно трактуется как результат засухи, хотя ни одна засуха двух последних столетий не привела к полному пересыханию Сены и Луары одновременно.

**English:**

When the axis shifts, aquifers change slope and water stops flowing into springs, wells and rivers. This is usually interpreted as a result of drought, although no drought in the last two centuries has resulted in the Seine and Loire drying up completely at the same time.

**Russian:**

1132 г. Высох Рейн

**English:**

1132 The Rhine is dry

**Russian:**

1470 г. В Чехии пересохли реки

**English:**

1470 The rivers of Bohemia dried up

**Russian:**

1471 г. Обмелела р. Дунай

**English:**

1471 The Danube River shallows

**Russian:**

1473 г. Обмелела р. Дунай

**English:**

1473 The Danube River shallows

**Russian:**

1534 г. Обмелели реки Дунай, Висла, Тибр

**English:**

1534 The Danube, the Vistula and the Tiber rivers shallowed

**Russian:**

1538 г. Высохли Сена и Луара

**English:**

1538 The Seine and the Loire dried up

**Russian:**

1554 г. Пересохла река Эльба

**English:**

1554 The Elbe River dried up

**Russian:**

1615 г. высохли колодцы и пруды во Франции и Швейцарии

**English:**

1615 wells and ponds dried up in France and Switzerland

**Russian:**

1715 г. Высохли реки Европы

**English:**

1715 г. The rivers of Europe have dried up

**Russian:**

**26. ОБРАЗОВАНИЕ РАЗЛОМОВ**

**English:**

**26. FAULT FORMATION**

**Russian:**

Там, где участку поверхности Земли проще растрескаться, чем пойти винтом, возникают разломы. Это сопровождается выдавливанием подземных вод и углеводородов и пожарам. Наиболее интересное, хотя и недатированное описание сохранилось в преданиях манси. Ситуация отражает образование разломов, по которым запертая в мансийском ледниковом бассейне вода сошла на север с одновременным выбросом из этих разломов горящих углеводородов.

**English:**

Where an area of the Earth's surface is easier to crack than to go screw, faulting occurs. This is accompanied by squeezing of ground waters and hydrocarbons and fires. The most interesting, though undated description is preserved in the Mansi legends. The situation reflects the formation of faults along which the water trapped in the Mansi glacial basin descended to the north with simultaneous ejection of burning hydrocarbons from these faults.

**Russian:**

Семь дней вода кипела - огонь и вода вместе шли. Вода была везде, кроме Урала. Когда вода прибывала, на высоких склонах гор собирались люди и звери... Люди из рода Питлор ёх утверждают, что их «сверху откуда-то водой принесло, один из кусочков земли оторвало в районе Мужей (Выен курт ёх), а другой - куда-то за Салехард унесло...» Потоп стал своеобразной точкой отсчета появления большого числа пришельцев с юга и складывания новых родовых групп. По традиции родственниками считаются те, чьи предки во время потопа остались на одной горе (холме, бугре) или осели на одном островке... Предания о семидневном потопе сохранились и у лесных ненцев. В них, как и в угорских легендах, разбушевавшаяся стихия разносит по разным местам людей... [^64]

**English:**

For seven days the water was boiling - fire and water went together. Water was everywhere except in the Urals. When the water arrived, people and beasts gathered on the high slopes of the mountains... People of the Pitlor yokh family claim that they "were brought from somewhere by water, one of the pieces of land was torn off near Muzhey (Vyen kurt yokh), and the other was carried away somewhere beyond Salekhard...". The Flood became a kind of a reference point for the appearance of a large number of newcomers from the south and the formation of new clan groups. According to tradition relatives are considered those whose ancestors at the time of the Flood stayed on one mountain (hill, hillock) or settled on one island... The legends about the seven-day deluge survived among forest Nenets people as well. In them, as in Ugrian legends, the raging elements carry people to different places... [^64]

**Russian:**

Именно в ситуации образования разломов реки осваивают новые русла по заданному разломами маршруту, а наводнения сопровождаются потоками воды, бьющими из вершин холмов. Полный список огромен, здесь только самое интересное.

**English:**

It is when faults form that rivers develop new channels along their fault routes and when floods are accompanied by torrents of water gushing from the tops of hills. The full list is huge, here is only the most interesting.

**Russian:**

1338 г. В районах Рейна и во Франции произошли сильные наводнения... на вершинах гор вырывались источники, и сухие участки оказались под водой необъяснимым образом.

**English:**

1338 Great floods occurred in areas of the Rhine and in France springs burst out on the tops of the mountains and dry areas were inexplicably under water.

**Russian:**

1573 г. Амударья повернула из Саракамыша в Аральское море

**English:**

1573 The Amu Darya turned from Sarakamysh to the Aral Sea

**Russian:**

1669 г. Землетрясение поглотило город Николоси. Взрыв 11 марта привел к образованию трещины длиной 19 километров и шириной около двух метров.

**English:**

1669 An earthquake engulfed the town of Nikolosi. An explosion on 11 March caused a fracture 19 kilometres long and about two metres wide.

**Russian:**

1692 г. Порт-Ройял (Ямайка) Вода, вышедшая из солеварного холма, проложила себе путь в двадцати или тридцати местах. Большей частию она была от 18 до 20 футов в вышину

**English:**

1692 Port Royal (Jamaica) The water that came out of the salt mound made its way in twenty or thirty places. For the most part it was from eighteen to twenty feet in height

**Russian:**

1834 г. Перерыв многолетнего русла Евфрата

**English:**

1834 Interruption of the perennial bed of the Euphrates

**Russian:**

1851 г. Река Хуанхэ в Китае повернула на север и потекла по своему современному руслу

**English:**

1851 The Huang He River in China turns north and flows in its present course

**Russian:**

**27. УГЛЕВОДОРОДНЫЕ ПОЖАРЫ**

**English:**

**27. HYDROCARBON FIRES**

**Russian:**

Здесь же становятся понятны необъяснимые пожары, когда пламя перепрыгивает через Дунай, или город внезапно вспыхивает целиком. Выдавленные из разломов углеводороды не обязательно вспыхивают сразу, и могут быть подожжены, когда газ войдет в город, - от первого же очага.

**English:**

Here, too, unexplained fires become understandable when flames leap across the Danube, or a city suddenly erupts in its entirety. The hydrocarbons squeezed out of the rifts do not necessarily erupt immediately, and can be ignited when the gas enters the city - from the first hearth.

**Russian:**

1471 г. Потоки пламени перепрыгивали через Дунай

**English:**

1471 Flames streamed across the Danube

**Russian:**

1356 г. Город Москва в один-два часа сгорел без остатка

**English:**

1356 The city of Moscow burned without a trace in one or two hours

**Russian:**

Хорошая иллюстрация процесса - т. н. горизонт сожженных домов.

**English:**

A good illustration of the process is the so-called burned house horizon.

**Russian:**

**ЦИТАТА**: В археологии неолитической Европы горизонт сожженных домов - это географическая протяженность феномена, предположительно, преднамеренно сожженных поселений. Это была широко распространенная и давняя традиция в регионе юго-восточной и восточной Европы. Заметным представителем этой традиции является культура Триполье-Кукутень, которая была сосредоточена в горизонте сожженных домов как географически, так и во времени [^142].

**English:**

**CITATION**: In Neolithic European archaeology, the burned house horizon is the geographical extent of the phenomenon of presumably deliberately burned settlements. This was a widespread and long-standing tradition in the region of southeastern and eastern Europe. A notable representative of this tradition is the culture of Tripolye-Cucuteni, which was concentrated in the horizon of burned houses both geographically and in time [^142].

**Russian:**

Рис. 81. Горизонт сожженных домов.

**English:**

Figure 81. Horizon of the burned houses.

<!-- Local
[![](Step by step_files/D09ED0B3D0BDD0B5D0BDD0BDD18BD0B9D0B3D0BED180D0B8D0B7D0BED0BD.png#clickable)](Step by step_files/%25D0%259E%25D0%25B3%25D0%25BD%25D0%25B5%25D0%25BD%25D0%25BD%25D1%258B%25D0%25B9%2B%25D0%25B3%25D0%25BE%25D1%2580%25D0%25B8%25D0%25B7%25D0%25BE%25D0%25BD%25D1%2582.png)
-->

[![](https://1.bp.blogspot.com/-0ypbbIm-YAI/XvxDu7RHR_I/AAAAAAAADSw/YzUzjveuAekCvM9fY7gN8cI3OVKwRiW0wCLcBGAsYHQ/s1600/%25D0%259E%25D0%25B3%25D0%25BD%25D0%25B5%25D0%25BD%25D0%25BD%25D1%258B%25D0%25B9%2B%25D0%25B3%25D0%25BE%25D1%2580%25D0%25B8%25D0%25B7%25D0%25BE%25D0%25BD%25D1%2582.png#clickable)](https://1.bp.blogspot.com/-0ypbbIm-YAI/XvxDu7RHR_I/AAAAAAAADSw/YzUzjveuAekCvM9fY7gN8cI3OVKwRiW0wCLcBGAsYHQ/s1600/%25D0%259E%25D0%25B3%25D0%25BD%25D0%25B5%25D0%25BD%25D0%25BD%25D1%258B%25D0%25B9%2B%25D0%25B3%25D0%25BE%25D1%2580%25D0%25B8%25D0%25B7%25D0%25BE%25D0%25BD%25D1%2582.png)

**Russian:**

**28. СТОЛПЫ ОГНЕННЫЕ**

**English:**

**28. PILLARS OF FIRE**

**Russian:**

По мере приближения к современности причина феномена становится все яснее.

**English:**

As we approach modernity, the cause of the phenomenon becomes clearer.

**Russian:**

912 г. Погорело небо, и столпы огненные ходили от Руси ко Греции, сражаясь.

**English:**

912 г. The sky was burnt, and pillars of fire went from Russia to Greece, fighting.

**Russian:**

1110 г. Над Киевом и Новгородом появляется «столп огненный». В то же время молния освещает всю землю, и в небе раздается великий гром.

**English:**

1110 г. A "pillar of fire" appears over Kiev and Novgorod. At the same time, lightning illuminates the whole earth, and great thunder is heard in the sky.

**Russian:**

1348 г. Большие и редкие метеоры во многих местах и огненный столп в Авиньоне

**English:**

1348 Large and rare meteors in many places and a pillar of fire in Avignon

**Russian:**

1824 г. 2 Ноября. Поднялся вертикально из воды огромный огненный столп

**English:**

1824 November 2. A huge pillar of fire rose vertically from the water

**Russian:**

1571 г. 21 сентября. В море перед битвой при Лепанто. «Вдруг в воздухе стало бушевать огромное и сияющее пламя в форме колонны, которое все с великим удивлением наблюдали в течение долгого времени».

**English:**

1571 September 21. At sea before the battle of Lepanto. "Suddenly a huge and shining column-shaped flame began to rage in the air, which everyone watched with great amazement for a long time.

**Russian:**

1927 г. землетрясение в Крыму ночью с 11 на 12 сентября, - страшные разрушения, сотни жертв, загорелось... море. Причиной стал газ из его пучины. Шла гроза, молнии били в море, они и подожгли газ. Очевидцы увидели столбы пламени, которые поднимались из моря на высоту в сотни метров.

**English:**

1927 an earthquake in the Crimea on the night of September 11-12, - terrible destruction, hundreds of victims, the sea caught fire... the sea. The cause was gas from its abyss. There was a thunderstorm, lightning struck the sea, and it ignited the gas. Eyewitnesses saw columns of flame rising from the sea to a height of hundreds of meters.

**Russian:**

**29. ЭФФЕКТ ОТРАВЛЕННЫХ РЕК И КОЛОДЦЕВ**

**English:**

**29. EFFECT OF POISONED RIVERS AND WELLS**

**Russian:**

Идущие в недрах геохимические процессы или даже банальный выход углеводородов вбрасывают в грунтовые воды новые вещества и делают воду в колодцах непригодной или даже опасной. Поскольку реки в значительной мере питаются именно грунтовыми водами, а текут по тектонически активным разломам, вода начинает смердеть, что и отражается в летописях, в том числе и в виде подозрений в отравлениях.

**English:**

Geochemical processes taking place in the subsoil or even a banal release of hydrocarbons introduces new substances into groundwater and makes water in wells unsuitable or even dangerous. As rivers are largely fed by groundwater and flow along tectonically active faults, water begins to stink, which is reflected in the chronicles, including in the form of suspicions of poisoning.

**Russian:**

1824 г. Как пишут из Нарвы, вода во всех тамошних колодезях волновалась и как бы кипела... Рыба салакушка совершенно исчезла

**English:**

1824 According to the reports of Narva, the water in all local wells was in an uproar and boiled... The herring fish had completely disappeared

**Russian:**

253 г. Из моря и из рек и из озер ветром нужным износимо дыхание смрадно, яко гной мертвых телес, и от сего тяжкие и неисцелимые болезни... погибель человеком...

**English:**

253 From the sea and from the rivers and from the lakes the necessary wind brought stinking breath, as pus of dead bodies, and from this grave and incurable diseases... the destruction of man...

**Russian:**

1247 г. Германия. Евреев обвинили в отравлении рек порошком.

**English:**

1247 Germany. Jews were accused of poisoning rivers with powder.

**Russian:**

1248 г. Повсюду евреев обвиняли в отравлении колодцев

**English:**

1248 Jews everywhere were accused of poisoning wells

**Russian:**

1258 г. Некие враги пытались отравить всех англичан

**English:**

1258. Some enemies tried to poison all the English

**Russian:**

1321 г. Королю сообщили, что все колодцы Аквитании отравлены прокаженными

**English:**

1321 The king was told that all the wells of Aquitaine were poisoned with leprosy

**Russian:**

1321 г. Пять тысяч убитых в Дофине по обвинению в отравлениях

**English:**

1321 Five thousand killed in the Dauphin on charges of poisoning

**Russian:**

1322 г. Народ Guienne счел, что прокаженные создали заговор уничтожить соотечественников, отравляя колодцы и фонтаны

**English:**

1322 The people of Guienne believed that lepers had conspired to destroy their countrymen by poisoning wells and fountains

**Russian:**

1348 г. Евреев в Цюрихе, Швейцария обвиняют в отравлении колодцев

**English:**

1348 Jews in Zurich, Switzerland accused of poisoning wells

**Russian:**

1348-1349 гг. Европа. В эпидемии морового поветрия обвинили иудеев, отравивших реки порошком

**English:**

1348-1349. Europe. The pestilence epidemic was blamed on the Jews, who poisoned the rivers with powder

**Russian:**

1349 г. Люди Кремс в Австрии обвиняют евреев в отравлении колодцев

**English:**

1349 Krems' men in Austria accuse Jews of poisoning wells

**Russian:**

1349 г. Герцог Брабанта приказал казнить всех евреев в Брюсселе, обвинив их в отравлении колодцев

**English:**

1349 The Duke of Brabant ordered the execution of all Jews in Brussels, accusing them of poisoning wells

**Russian:**

1349 г. Быстро распространяются слухи, что евреи отравили колодцы

**English:**

1349 Word quickly spread that Jews had poisoned the wells

**Russian:**

1350 г. Обвинение, что евреи отравили колодцы в Мекленбурге

**English:**

1350 г. Accusation that Jews poisoned wells in Mecklenburg

**Russian:**

1391 г. Обвинение евреев в отравлении колодцев в Мюльхаузене

**English:**

1391 Blaming the Jews for the poisoning of wells in Mülhausen

**Russian:**

1643 г. При дворе Людовика XIV разразилась своеобразная эпидемия отравлений

**English:**

1643 A peculiar epidemic of poisoning broke out at the court of Louis XIV.

**Russian:**

1679 г. Приказ арестовать Мэри Хейл и Мари Боссе, подозреваемых отравителей

**English:**

1679 Order to arrest Mary Hale and Marie Bosse, suspected poisoners

**Russian:**

1679 г. Создание специального суда для рассмотрения дел отравителей

**English:**

1679 Creation of a special court to try poisoners

**Russian:**

1831 г. Беспорядки в Венгрии, где народ подозревал отравления

**English:**

1831 Disturbances in Hungary, where people suspected poisoning

**Russian:**

1832 г. Анри Gisquet осужден за отравление общественных фонтанов

**English:**

1832 Henri Gisquet is convicted of poisoning public fountains

**Russian:**

**XIX ВЕК. ИЗМЕНЕНИЕ ОЦЕНОК**

**English:**

**XIXTH CENTURY. CHANGE OF ESTIMATES**

**Russian:**

1855 г. Вода в Бостоне внезапно перестала вонять

**English:**

1855. Boston's water suddenly stopped stinking.

**Russian:**

1855 г. На Грин-Ривер вода при перемешивании лопастями пароходов испускает газ, мгновенно воспламеняющийся при контакте с пламенем

**English:**

1855 At Green River, water when stirred by steamer blades emits gas that instantly ignites on contact with flames

**Russian:**

1858 г. Великое зловоние Темзы. Горожане массово бежали из Лондона

**English:**

1858 The great stench of the Thames. Townspeople fled London en masse

**Russian:**

В последнем случае винят канализационные системы, однако тот же сероводород появляется не только в результате гниения, но и напрямую из недр.

**English:**

In the latter case, sewage systems are blamed, but the same hydrogen sulfide appears not only as a result of rotting, but also directly from the subsoil.

**Russian:**

**30. КРОВАВЫЕ РЕКИ**

**English:**

**30. BLOODY RIVERS**

**Russian:**

Поскольку реки предпочитают течь по разломам, а разломы в такой ситуации активны, вода часто меняет цвет, например, краснеет.

**English:**

Because rivers prefer to flow along faults, and faults are active in this situation, the water often changes color, such as turning red.

**Russian:**

869 г. Реки, наполненные кровью и выступающие из берегов

**English:**

869 Rivers filled with blood and protruding from their banks

**Russian:**

1043 г. Казалось, будто излившийся из рек поток крови окрасил море

**English:**

1043 It seemed as if a flood of blood poured from the rivers stained the sea

**Russian:**

1111 г. Море Хлатское выступило на сушу и обратилось в кровь

**English:**

1111 г. The sea of Chlatsky has acted on land and turned to blood

**Russian:**

Много свидетельств и о кровавых дождях, возможно смывавшихся в реки. Часть кровавых дождей прямо связывается летописцами с проходом метеорного потока.

**English:**

There are also many evidences of bloody rains, possibly washed into the rivers. Part of the bloody rains is directly associated by chroniclers with the passage of the meteor shower.

**Russian:**

869 г. н. э. Описываются невообразимо громадные черные птицы, заполняющие все небо и несущие в клюве огненные угли; реки, наполненные кровью и выступающие из берегов. Кровь покрывает хлеб и другую еду. На одежде появляются громадные кресты, нарисованные кровью.

**English:**

869 A.D. Unimaginably huge black birds are described, filling the whole sky and carrying coals of fire in their beaks; rivers filled with blood and protruding from their banks. Blood covers bread and other food. Huge blood-stained crosses appear on their clothes.

**Russian:**

**[ГЛАВНЫЕ ГУМАНИТАРНЫЕ КАТАСТРОФЫ]**

**English:**

**[MAIN HUMANITARIAN CATASTROPHS] **

**Russian:**

**31. ЧЕРНАЯ СМЕРТЬ**

**English:**

**31. BLACK DEATH**

**Russian:**

Чумная палочка (Yersinia pestis) была отождествлена со средневековой Черной смертью в 1894 году, но лауреат Нобелевской премии по биохимии Дж. Ледерберг со знанием дела показал, что клиническая картина средневековой Черной смерти, искусственно подогнана под клинику современной чумы [^110].

**English:**

The plague bacillus (Yersinia pestis) was identified with the medieval Black Death in 1894, but Nobel Prize laureate in biochemistry J. Lederberg has shown with knowledge that the clinical picture of the medieval Black Death, has been artificially fitted to the clinic of the modern plague [^110].

**Russian:**

Симптомам Черной смерти отвечают отравление вулканическими и/или теллурическими газами, усиливающееся в сырую погоду и на понижениях местности, баротравма и удар инфразвуком, - но не чумная палочка.

**English:**

Symptoms of the Black Death are consistent with volcanic and/or telluric gas poisoning, increasing in wet weather and on lower terrain, barotrauma and infrasound shock - but not plague bacillus.

**Russian:**

**31.1. НЕСООТВЕТСТВИЕ БАЗОВЫХ СИМПТОМОВ**

**English:**

**31.1. MISMATCH OF BASELINE SYMPTOMS**

**Russian:**

Чумная палочка (Yersinia pestis) не синтезирует истинных экзотоксинов, способных вызывать основные симптомы «черной смерти».

**English:**

Plague bacillus (Yersinia pestis) does not synthesize true exotoxins capable of causing the major symptoms of "black death".

**Russian:**

Тела людей, погибших от «черной смерти», быстро чернели и выглядели «обугленными». Это возможно, если гибели людей предшествовало массовое развитие геморрагий, однако такая неспецифическая реакция зависит от наличия в геноме человека аллели (TNF2). Эта аллель -- результат мутации гена TNF-a, и гомозиготные по ней индивидуумы составляют около 5% популяции. Это исключает эпидемию, а в поселениях Европы от Черной смерти разом гибли до 95-100 % жителей.

**English:**

The bodies of people who died from "black death" quickly turned black and looked "charred". This is possible if the deaths were preceded by the massive development of hemorrhages, but such a nonspecific reaction depends on the presence of an allele (TNF2) in the human genome. This allele is the result of a mutation in the TNF-a gene, and individuals homozygous for it constitute about 5% of the population. This rules out an epidemic, and in European settlements up to 95-100% of the population has been killed by the Black Death at one time.

**Russian:**

Инкубационный период бубонной чумы длится 3-6 суток, при легочной (очень редкой) форме - 1-2 дня. Температура тела повышается до 39°С и выше. Появляются озноб, сильная головная боль, головокружение, чувство разбитости, мышечные боли, иногда рвота. В отличие от чумы, Черная смерть убивала в Багдаде за несколько часов, а в Авиньоне - за ночь.

**English:**

The bubonic plague has an incubation period of 3-6 days; in the pneumonic (very rare) form it lasts 1-2 days. The body temperature rises to 39°C or higher. Chills, severe headache, dizziness, weakness, muscle pain and sometimes vomiting occur. Unlike the plague, the Black Death killed in Baghdad in a few hours and in Avignon - overnight.

**Russian:**

**31.2. СИНОПТИЧЕСКИЙ ФАКТОР**

**English:**

**31.2. SYNOPTIC FACTOR**

**Russian:**

Yersinia pestis (чума) расширяет ареал обитания, лишь когда влаги становится меньше, наступают степные зоны и, соответственно, увеличивается численность полевых грызунов. Наступление Черной смерти в Европе жестко связано с периодами теплой и сырой погоды.

**English:**

Yersinia pestis (plague) only expands its range when there is less moisture, steppe zones come into play and field rodent populations increase accordingly. The onset of the Black Death in Europe is tightly linked to periods of warm and wet weather.

**Russian:**

**31.3. НЕСООТВЕТСТВИЕ ОЧАГОВ**

**English:**

**31.3. MISMATCH OF FOCI**

**Russian:**

Бубонная чума, даже осложненная легочной, не выходит за пределы своего природного очага, например, в Туве и Монголии. Эти природные очаги чумы находятся за тысячи километров от реликтовых очагов Черной смерти в Европе.

**English:**

Bubonic plague, even complicated by pneumonic plague, does not spread beyond its natural foci, for example, in Tuva and Mongolia. These natural plague outbreaks are thousands of kilometres away from the relict Black Death outbreaks in Europe.

**Russian:**

**31.4. ОЧАГИ ЧЕРНОЙ СМЕРТИ В ЕВРОПЕ**

**English:**

**31.4. HOTBEDS OF THE BLACK DEATH IN EUROPE**

**Russian:**

Все реликтовые очаги Черной смерти расположены в долинах рек или их устьях на побережьях, то есть, местах, наиболее влажных. Вот список [^110]:

**English:**

All of the relict Black Death hotspots are located in river valleys or river mouths on the coasts, i.e., the places that are the wettest. Here is the list [^110]:

**Russian:**

- долина реки Вольтурно (Неаполь);

**English:**

- the valley of the Volturno river (Naples);

**Russian:**

- долина реки Рона;

**English:**

- the Rhone valley;

**Russian:**

- долина реки Дуэро (Пиренеи);

**English:**

- the valley of the river Duero (Pyrenees);

**Russian:**

- долина реки Гвадалквивир (Севилья);

**English:**

- Guadalquivir Valley (Seville);

**Russian:**

- низовья реки Турия (Валенсия);

**English:**

- the lower reaches of the river Turia (Valencia);

**Russian:**

- долина реки Эбро (Сарагоса);

**English:**

- the valley of the river Ebro (Zaragoza);

**Russian:**

- долина реки Гаронна;

**English:**

- the valley of the Garonne River;

**Russian:**

- долины рек Сена, Марна, Сомма;

**English:**

- the valleys of the Seine, the Marne, the Somme;

**Russian:**

- долина реки Темза;

**English:**

- the Thames River valley;

**Russian:**

- долины рек Рейн, Везер, Эльба (сливаются с очагами побережий пролива Ла-Манш, Северного и Балтийского морей);

**English:**

- valleys of the rivers Rhine, Weser, Elbe (merging with the coastal foci of the English Channel, North Sea and Baltic Sea);

**Russian:**

- бассейн реки Дунай;

**English:**

- Danube River Basin;

**Russian:**

- очаги северных отрогов Альп (Женева, Базель, Берн, Цюрих, Мюнхен, Линц, Вена, Краков);

**English:**

- the foci of the northern spurs of the Alps (Geneva, Basel, Bern, Zurich, Munich, Linz, Vienna, Krakow);

**Russian:**

- полуостров Корнуолс;

**English:**

- Cornish Peninsula;

**Russian:**

- очаг в Сицилии.

**English:**

- a hotbed in Sicily.

**Russian:**

В 1348 году в Сирии погибли именно жители долин, в Антиохии люди, спасались в горах и погибали в долинах.

**English:**

In 1348 it was the people of the valleys who perished in Syria; in Antioch the people were saved in the mountains and perished in the valleys.

**Russian:**

**31.5. МАРШРУТЫ ЧЕРНОЙ СМЕРТИ**

**English:**

**31.5. BLACK DEATH ROUTES**

**Russian:**

В разносе Черной смерти винят купцов, но на всех картах видно, что Черная смерть накрывает сразу целые районы -- с юго-запада на северо-восток, полностью игнорируя движение людей и товаров -- речными коридорами с юго-востока на северо-запад. Чума и купцы движутся под прямым углом друг к другу.

**English:**

Merchants are blamed for the spread of the Black Death, but all the maps show that the Black Death covers entire areas at once -- from southwest to northeast, completely ignoring the movement of people and goods -- by river corridors from southeast to northwest. The plague and the merchants move at right angles to each other.

**Russian:**

Рис. 82. Продвижение Черной смерти

**English:**

Figure 82. Promotion of the Black Death

<!-- Local
[![](Step by step_files/60D09CD0B0D180D188D180D183D182.jpg#clickable)](Step by step_files/60%2B%25D0%259C%25D0%25B0%25D1%2580%25D1%2588%25D1%2580%25D1%2583%25D1%2582.jpg)
-->

[![](https://1.bp.blogspot.com/-rHPCUboMJRA/XlFEd7h8uCI/AAAAAAAADJQ/OKycpqRK5Bwf3VPwom_7uK-0ZkPPuhnEwCLcBGAsYHQ/s1600/60%2B%25D0%259C%25D0%25B0%25D1%2580%25D1%2588%25D1%2580%25D1%2583%25D1%2582.jpg#clickable)](https://1.bp.blogspot.com/-rHPCUboMJRA/XlFEd7h8uCI/AAAAAAAADJQ/OKycpqRK5Bwf3VPwom_7uK-0ZkPPuhnEwCLcBGAsYHQ/s1600/60%2B%25D0%259C%25D0%25B0%25D1%2580%25D1%2588%25D1%2580%25D1%2583%25D1%2582.jpg)

**Russian:**

Изолинии движения Черной смерти, скорее развивают контуры устилающей дно моря тефры вулкана Санторини. И чрезвычайно интересно, что начавшаяся в Крыму "эпидемия", имеет изолинию от 31 декабря 1347 года, проходящую через Босфор, Италию о краешек Туниса синхронно. Для облака пепла или газа это нормально, а для эпидемии - нет.

**English:**

Isolines of the Black Death movement, rather develop the contours of the tephra of the volcano Santorini covering the bottom of the sea. And it is extremely interesting that the "epidemic" that started in the Crimea, has an isoline from December 31, 1347, passing through the Bosporus, Italy and the edge of Tunisia synchronously. This is normal for an ash or gas cloud, but not for an epidemic.

**Russian:**

Рис. 83. Тефра вулкана Санторини на дне моря

**English:**

Figure 83. The tephra of Santorini volcano on the seabed

<!-- Local
[![](Step by step_files/61D0A2D0B5D184D180D0B0.jpg#clickable)](Step by step_files/61%2B%25D0%25A2%25D0%25B5%25D1%2584%25D1%2580%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-jm0Tnx10sy0/XlFEimonCzI/AAAAAAAADJU/hwRwvYkUnO04EfqtgM9fE9aNodmg88PaQCLcBGAsYHQ/s1600/61%2B%25D0%25A2%25D0%25B5%25D1%2584%25D1%2580%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-jm0Tnx10sy0/XlFEimonCzI/AAAAAAAADJU/hwRwvYkUnO04EfqtgM9fE9aNodmg88PaQCLcBGAsYHQ/s1600/61%2B%25D0%25A2%25D0%25B5%25D1%2584%25D1%2580%25D0%25B0.jpg)

**Russian:**

**31.6. МАРШРУТЫ ФИТОФТОРОЗА**

**English:**

**31.6. PHYTOPHTHORA ROUTES**

**Russian:**

Аналогично, словно облако пепла или газа, игнорируя транспортные артерии, движется эпифитотия фитофтороза --- массовое заражение картофельных посевов патогенным оомицетом Phytophthora infestans в 1845 году.

**English:**

Similarly, like a cloud of ash or gas, ignoring transportation arteries, the Phytophthorosis epiphytotaxy - a massive infestation of potato crops by the pathogenic oomycete Phytophthora infestans in 1845 is moving.

**Russian:**

Рис. 84. Продвижение фитофтороза в 1845 году [^111]

**English:**

Figure 84. Progression of Phytophthora in 1845 [^111].

<!-- Local
[![](Step by step_files/62D0A4D0B8D182D0BED184D182D0BED180D0BED0B7.jpg#clickable)](Step by step_files/62%2B%25D0%25A4%25D0%25B8%25D1%2582%25D0%25BE%25D1%2584%25D1%2582%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B7.jpg)
-->

[![](https://1.bp.blogspot.com/-Y5-y-26dHKs/XlFEoE7fvEI/AAAAAAAADJY/CiEO2h9gQq8Nrq04039_P0nsCKlqMfaWwCLcBGAsYHQ/s1600/62%2B%25D0%25A4%25D0%25B8%25D1%2582%25D0%25BE%25D1%2584%25D1%2582%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B7.jpg#clickable)](https://1.bp.blogspot.com/-Y5-y-26dHKs/XlFEoE7fvEI/AAAAAAAADJY/CiEO2h9gQq8Nrq04039_P0nsCKlqMfaWwCLcBGAsYHQ/s1600/62%2B%25D0%25A4%25D0%25B8%25D1%2582%25D0%25BE%25D1%2584%25D1%2582%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B7.jpg)

**Russian:**

Наиболее очевидная причина гибели посевов, а затем и людей -- выбросы теллурических газов из лежащего под Бельгией, недалеко от города Ипр крупного тектонического разлома.

**English:**

The most obvious cause of the deaths of crops and then of people is the release of telluric gases from a major tectonic fault underneath Belgium, not far from the city of Ypres.

**Russian:**

**31.6. ОПИСАНИЕ ЧЕРНОЙ СМЕРТИ**

**English:**

**31.6. DESCRIPTION OF THE BLACK DEATH**

**Russian:**

- сначала были вонючий дым и дождь из огня, сжигавший все;

**English:**

- first there was the stinking smoke and the rain of fire that burned everything;

**Russian:**

- затем пришло черное облако, убивавшее за полдня;

**English:**

- then came the black cloud, killing in half a day;

**Russian:**

- смерть пришла вслед за сильным ветром;

**English:**

- death came in the wake of a strong wind;

**Russian:**

- смерть происходила от смрадного воздуха, умирали от запаха;

**English:**

- death came from the stinking air, dying from the smell;

**Russian:**

- после выпадения дождя погибали скот, люди, птицы и дикие звери;

**English:**

- cattle, people, birds and wild animals were killed after the rain fell;

**Russian:**

- погибала рыба в реках и даже морские гады;

**English:**

- fish in rivers and even sea creatures were dying;

**Russian:**

- люди умирали за плугом, с удочкой в руках, за столом в чайханах;

**English:**

- people were dying at the plough, with a fishing rod in their hands, at the table in teahouses;

**Russian:**

- в огромных регионах вымирали все сто процентов населения;

**English:**

- in vast regions, all one hundred percent of the population was dying out;

**Russian:**

- заразившись смертью, стали гнить деревья;

**English:**

- the trees began to rot, infected with death;

**Russian:**

- мясо птиц и животных стало черным и зловонным;

**English:**

- the meat of birds and animals became black and stinky;

**Russian:**

- смерть связывали с отравлениями рек, скважин и колодцев;

**English:**

- deaths were attributed to the poisoning of rivers, wells and wells;

**Russian:**

- смерть связывали с внезапными и сильными пожарами;

**English:**

- deaths were associated with sudden and severe fires;

**Russian:**

- смерть связывали с выходом газов из трещин после землетрясений;

**English:**

- deaths were attributed to gas escaping from cracks after earthquakes;

**Russian:**

- на одежде появлялись черные маслянистые кресты.

**English:**

- black oily crosses appeared on the clothes.

**Russian:**

Это не признаки инфекции, а вот признакам отравления газами -- теллурическими или вулканическими -- описание отвечает.

**English:**

It's not a sign of infection, but it's a sign of gas poisoning -- telluric or volcanic -- the description fits.

**Russian:**

**31.7. ЧУМА 1783-1784 ГОДОВ**

**English:**

**31.7. PLAGUE OF 1783-1784**

**Russian:**

Сегодня ученые уверены, что две эпидемии смертей в Британии 1783 и 1784 года -- результат извержения исландского вулкана Лаки. За восемь месяцев Лаки выбросил в атмосферу около 122 мегатонн диоксида серы, и часть облака легла на Европу. Погибло порядка 6 миллионов человек. Однако в Австрии, Восточной Европе и Турции, не извещенных о происходящем в Исландии, массовые смерти 1783-1784 годов до сих пор считаются эпидемией чумы.

**English:**

Scientists today believe that the two plagues of death in Britain in 1783 and 1784 were the result of the eruption of the Icelandic volcano Lucky. Over a period of eight months, Laki released about 122 megatons of sulphur dioxide into the atmosphere, and part of the cloud covered Europe. Some 6 million people were killed. However, in Austria, Eastern Europe and Turkey, unaware of what was happening in Iceland, the mass deaths of 1783-1784 are still considered a plague epidemic.

**Russian:**

**31.8. ДОКУМЕНТАЛЬНАЯ СВЯЗЬ ИЗВЕРЖЕНИЙ И ЧЕРНОЙ СМЕРТИ**

**English:**

**31.8. DOCUMENTATION OF ERUPTIONS AND BLACK DEATH

**Russian:**

Связь между извержениями и Черной смертью видна в частоте употребления этих двух слов в англоязычной печати с 1710 по 1840 год (Google Ngram Viewer). Такая же связь видна и во французских источниках.

**English:**

The connection between eruptions and the Black Death can be seen in the frequency of use of these two words in the English-language press from 1710 to 1840 (Google Ngram Viewer). The same connection can be seen in French sources.

**Russian:**

Рис. 85. Частота употребления терминов «plague» и «eruption» в английской печати

**English:**

Figure 85. Frequency of the terms "plague" and "eruption" in the English press

<!-- Local
[![](Step by step_files/63D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/63%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-DZoz-D-sOBk/XlFEwcTWUmI/AAAAAAAADJg/xkbCveqgpn0290-zAx4reat4l92a5y3lQCLcBGAsYHQ/s1600/63%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-DZoz-D-sOBk/XlFEwcTWUmI/AAAAAAAADJg/xkbCveqgpn0290-zAx4reat4l92a5y3lQCLcBGAsYHQ/s1600/63%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

**Russian:**

Рис. 86. Частота употребления терминов «peste» и «l'eruption» во французской печати

**English:**

Figure 86. Frequency of use of the terms "peste" and "l'eruption" in the French press

<!-- Local
[![](Step by step_files/64D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/64%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-ByhR_uymyt8/XlFE0pJ3bPI/AAAAAAAADJo/iJQxxBzmaLQSedql8yGwGzkJNI-5VB8zwCLcBGAsYHQ/s1600/64%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-ByhR_uymyt8/XlFE0pJ3bPI/AAAAAAAADJo/iJQxxBzmaLQSedql8yGwGzkJNI-5VB8zwCLcBGAsYHQ/s1600/64%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

**Russian:**

Ученые об этой связи хорошо осведомлены и даже подумывают о том, что вулканические аэрозоли способствуют размножению некоторых тлетворных бактерий.

**English:**

Scientists are well aware of this connection and are even thinking that volcanic aerosols contribute to the reproduction of some pest bacteria.

**Russian:**

На деле, описания Черной смерти точно отражают прямые последствия извержений: хлопья опасного для скота фтороводородного пепла, дождь из огня, облака отравляющих газов и воздействие этих газов. В присутствие влаги (дождь, туман) газы становятся парами кислот и сжигают легкие и кожу. Эти же кислотные пары в виде конденсата расползаются по нитям тканей, создавая на одежде кресты, указывающие на приход Черной смерти. Парижские медики в 1348 году инструктировали людей вполне профессионально: «Остерегаться холода, сырости, дождя, ничего не варить на дождевой воде... особенно для тех, которые живут на берегах моря или на островах, на которые подул гибельный ветер» [^112].

**English:**

In fact, the descriptions of the Black Death accurately reflect the direct consequences of the eruptions: flakes of hydrofluoric ash dangerous to livestock, rain of fire, clouds of poisonous gases and exposure to these gases. In the presence of moisture (rain, fog) the gases become acid vapours and burn the lungs and skin. These same acid vapors in the form of condensation spread on the threads of fabrics, creating crosses on clothing, indicating the arrival of the Black Death. Parisian physicians in 1348 instructed people quite professionally: "Beware of cold, dampness, rain, do not cook anything on the rain water... especially for those living on the seashore or on islands that have been blown over by a destructive wind." [^112].

**Russian:**

Эти газы тяжелее воздуха, а потому склонны собираться в долинах рек, что и отражено в списке «реликтовых чумных очагов» Европы. По этой же причине большинство пострадавших от Черной смерти -- женщины, вынужденно спускавшиеся к рекам постирать белье и набрать воды.

**English:**

These gases are heavier than air and therefore tend to collect in river valleys, as is reflected in the list of "relic plague hotspots" in Europe. For the same reason, most of the victims of the Black Death were women who had to go down to the rivers to wash laundry and fetch water.

**Russian:**

**31.9. ВУЛКАНИЧЕСКИЕ ГАЗЫ**

**English:**

**31.9. VOLCANIC GASES**

**Russian:**

Вот перечень основных вулканических газов.

**English:**

Here is a list of the major volcanic gases.

**Russian:**

Водяной пар (H2O),

**English:**

Water vapor (H2O),

**Russian:**

Диоксид углерода (CO2),

**English:**

Carbon dioxide (CO2),

**Russian:**

Оксид углерода (CO),

**English:**

Carbon monoxide (CO),

**Russian:**

Азот (N2),

**English:**

Nitrogen (N2),

**Russian:**

Диоксид серы (SO2),

**English:**

Sulphur dioxide (SO2),

**Russian:**

Оксид серы (SO),

**English:**

Sulphur oxide (SO),

**Russian:**

Газообразная сера (S2),

**English:**

Gaseous sulfur (S2),

**Russian:**

Водород (H2),

**English:**

Hydrogen (H2),

**Russian:**

Аммиак (NH3),

**English:**

Ammonia (NH3),

**Russian:**

Хлористый водород (HCl),

**English:**

Hydrogen chloride (HCl),

**Russian:**

Фтористый водород (HF),

**English:**

Hydrogen fluoride (HF),

**Russian:**

Сероводород (H2S),

**English:**

Hydrogen sulfide (H2S),

**Russian:**

Метан (CH4),

**English:**

Methane (CH4),

**Russian:**

Борная кислота (H3BO3),

**English:**

Boric acid (H3BO3),

**Russian:**

Хлор (Cl),

**English:**

Chlorine (Cl),

**Russian:**

Аргон (Ar),

**English:**

Argon (Ar),

**Russian:**

Преобразованные H2O и СО2.

**English:**

Converted H2O and CO2.

**Russian:**

Также присутствуют хлориды щелочных металлов и железа.

**English:**

Alkali metal and iron chlorides are also present.

**Russian:**

Директивой начальника штаба гражданской обороны ДНГО № 2 от 4.12.1990 г. пять веществ из этого списка отнесены к СДЯВ -- сильно действующим ядовитым веществам:

**English:**

Five substances from this list are referred to as SVD - highly active poisonous substances according to Directive of the Chief of Civil Defence Headquarters of the DNHO ¹ 2 dated 4.12.1990:

**Russian:**

Аммиак (NH3),

**English:**

Ammonia (NH3),

**Russian:**

Хлор (Cl),

**English:**

Chlorine (Cl),

**Russian:**

Хлористый водород (HCl),

**English:**

Hydrogen chloride (HCl),

**Russian:**

Фтористый водород (HF),

**English:**

Hydrogen fluoride (HF),

**Russian:**

Сероводород (H2S).

**English:**

Hydrogen sulfide (H2S).

**Russian:**

**31.10. СРАВНЕНИЕ ПОКАЗАНИЙ**

**English:**

**31.10. COMPARISON OF READINGS**

**Russian:**

При отравлениях лимфатические узлы могут быть опухшие и увеличены в размере, на разрезе могут иметь сиренево-розовую окраску, участки кровоизлияний, воспалительные процессы и т.д. При отравлениях животных и во всех случаях подозрений на отравление... на туше вскрывают и осматривают подлежащие экспертизе основные лимфатические узлы. Именно это регистрировали средневековые врачи при «черной смерти», и первым делом они вскрывали и рассматривали забитые токсинами лимфатические узлы.

**English:**

In cases of poisoning the lymph nodes may be swollen and enlarged in size, may be lilac-pink on section, areas of haemorrhage, inflammation, etc. In case of animal poisoning and in all cases of suspected poisoning on the carcass, the major lymph nodes to be examined are opened and examined. This is what medieval doctors recorded in "black death", and the first thing they did was to dissect open and examine the toxin-clogged lymph nodes.

**Russian:**

Сравним данные современной медицины об отравлениях этими газами с тем, что было зафиксировано медициной средневековой.

**English:**

Let us compare the data of modern medicine about poisoning by these gases with what was recorded by medieval medicine.

**Russian:**

**ОТРАВЛЕНИЕ АММИАКОМ**

**English:**

** AMMONIA POISONING**

**Russian:**

В тяжелых случаях - жгучая боль в горле, чувство удушья, возможен отек гортани, легких, токсический бронхит, пневмония. При попадании концентрированных растворов в желудочно-кишечный тракт образуются глубокие некрозы, в острой стадии приводящие к болевому шоку. Массивные пищеводно-желудочные кровотечения, асфиксия в результате ожога и отека гортани, тяжелая ожоговая болезнь, реактивный перитонит. Смерть может наступить в первые часы и сутки от болевого шока, а в более поздние сроки - от ожоговой болезни и присоединившихся осложнений (массивное кровотечение, аспирационная пневмония, перфорация пищевода и желудка, медиастинит). Пары аммиака поражают кожные покровы -- вплоть до химического ожога.

Именно так описывается «черная смерть» очевидцами:

- кашель, кровохарканье (черная смерть в Англии, Норвегии, России);

- кровавая рвота (черная смерть в Англии);

- смерть в первые часы или первые сутки болезни (Авиньон, Багдад, Норвегия);

Под вопросом только масштабы ожогов кожных покровов.

**English:**

In severe cases - searing pain in throat, choking sensation, possible laryngeal edema, lung edema, toxic bronchitis, pneumonia. When concentrated solutions enter the gastrointestinal tract deep necroses are formed, in the acute stage leading to painful shock. Massive oesophageal-gastric bleeding, asphyxia due to burns and laryngeal edema, severe burn disease, reactive peritonitis. Death may occur in the first hours and 24 hours from painful shock, and at a later date from burn disease and complications (massive bleeding, aspiration pneumonia, esophageal and gastric perforation, mediastinitis). Ammonia vapors affect the skin -- up to and including chemical burns.

This is how the "black death" is described by eyewitnesses:

- coughing, hemoptysis (black death in England, Norway, Russia);

- bloody vomiting (the black death in England);

- death in the first hours or first 24 hours of illness (Avignon, Baghdad, Norway);

Only the extent of dermal burns is in question.

**Russian:**

**ОТРАВЛЕНИЕ СЕРОВОДОРОДОМ**

**English:**

** HYDROGEN SULFIDE POISONING**

**Russian:**

Жжение и резь в глазах, слезотечение, блефароспазм, головная боль, головокружение, психомоторное возбуждение, Тошнота, рвота, диарея. Чувство стеснения и боли в груди, кашель, одышка, признаки диффузного бронхита. В тяжёлых случаях --- кома, судороги, коллапс, токсический отёк лёгких; возможна молниеносная смерть (апоплексическая форма).

**English:**

Burning and burning eyes, lacrimation, blepharospasm, headache, dizziness, psychomotor agitation, Nausea, vomiting, diarrhoea. Chest tightness and pain, cough, shortness of breath, signs of diffuse bronchitis. In severe cases - coma, seizures, collapse, toxic pulmonary edema; lightning death is possible (apoplectic form).

**Russian:**

На рисунке именно молниеносная смерть: позы женщины и ребенка естественны для падения на улице. Именно так погибали кара-китаи -- в седлах, рыбаки на озере Баралас -- в лодках с удочками, крестьяне в Газзе -- за плугом, горожане Лудд и Рамле -- прямо в чайханах. И именно так, почти мгновенно погибли жители Помпей. Никто не мучается по 3-6 дней.

**English:**

It is the lightning death in the drawing: the postures of the woman and child are natural for a fall in the street. That is how the Kara-Kitai died -- in saddles, the fishermen on Lake Baralas -- in boats with fishing rods, the peasants in Ghazza -- at the plow, the townspeople of Ludd and Ramla -- right in the teahouses. And that's how the people of Pompeii died, almost instantly. No one sufferered for three to six days.

*Рис. 87. Черная смерть*

*Figure 87. Black Death*

<!-- Local
[![](Step by step_files/65D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/65%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-EsnsAEk_5ks/XlFE-cy25PI/AAAAAAAADJw/R8PioYE9tlsz5Lb6AfDIzKdGMxSTkcMMwCLcBGAsYHQ/s1600/65%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-EsnsAEk_5ks/XlFE-cy25PI/AAAAAAAADJw/R8PioYE9tlsz5Lb6AfDIzKdGMxSTkcMMwCLcBGAsYHQ/s1600/65%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

**Russian:**

Рис. 88. Великая чума в Лондоне 1665 года.

**English:**

Figure 88. The Great Plague in London in 1665.

<!-- Local
[![](Step by step_files/66D0A7D183D0BCD0B0.jpg#clickable)](Step by step_files/66%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-Hw9OcFkxORI/XlFFC_kpwqI/AAAAAAAADJ0/A8E83sMJcaIlVinZCXPtpNPpQRfBJV09ACLcBGAsYHQ/s1600/66%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-Hw9OcFkxORI/XlFFC_kpwqI/AAAAAAAADJ0/A8E83sMJcaIlVinZCXPtpNPpQRfBJV09ACLcBGAsYHQ/s1600/66%2B%25D0%25A7%25D1%2583%25D0%25BC%25D0%25B0.jpg)

**Russian:**

Довольно точно было выбрано могильщиками и средство защиты от чумы: постоянное курение трубки. Понятно, что дело не «мадаке» -- как бы лекарственной смеси табака и опия. Дело в постоянной фильтрации воздуха через постоянно сменяемые горящие растительные волокна. Не факт, что это поможет, но это интуитивно верный прием.

**English:**

Quite accurately was also chosen by the gravediggers a means of protection against the plague: constant smoking of a pipe. It is clear that it was not a matter of "madake" -- a kind of medicinal mixture of tobacco and opium. It's about the constant filtration of air through constantly changing burning plant fibers. It's not a sure thing to help, but it's an intuitively correct technique.

**Russian:**

**ОТРАВЛЕНИЕ ДИОКСИДОМ СЕРЫ**

**English:**

**SULFUR DIOXIDE POISONING**

**Russian:**

Раздражение дыхательных путей: чихание, кашель (иногда кровохарканье), одышка, хрипы в лёгких. Возможны тошнота и рвота. В тяжёлых случаях -- токсический отёк лёгких.

**English:**

Respiratory tract irritation: sneezing, coughing (sometimes hemoptysis), shortness of breath, wheezing in lungs. Nausea and vomiting may occur. In severe cases - toxic pulmonary edema.

**Russian:**

А теперь -- внимание! -- вспомним предостережения средневековых медиков об опасности сырости. Ниже токсикологическая оценка диоксида серы.

**English:**

Now, pay attention. -- recall the warnings of medieval medics about the dangers of damp. Below is a toxicological assessment of sulfur dioxide.

**Russian:**

Действие диоксида серы на органы дыхания усиливается в присутствии водяного пара (тумана) и дыма. Это происходит в связи с тем, что основная часть газообразного SO2 во влаге слизистых оболочек рта и носа и в виде аэрозоля может проникать во внутренние органы дыхания, где преобразуется в серную кислоту - превращение, которое в присутствии воды, копоти и золы частично происходит уже в аэрозольном состоянии. Загрязнение атмосферы SO2 ,особенно при продолжительных туманах, вызывает обострение заболеваний верхних дыхательных путей, что может привести к значительному увеличению смертности.

**English:**

The action of sulphur dioxide on the respiratory organs is enhanced in the presence of water vapour (fog) and smoke. This is due to the fact that most of the gaseous SO2 in the moisture of the mucous membranes of the mouth and nose and in the form of aerosol can penetrate to the internal respiratory organs, where it is converted to sulfuric acid - a transformation that in the presence of water, soot and ash occurs partially already in the aerosol state. Atmospheric pollution with SO2, especially in prolonged fog, causes exacerbation of upper respiratory tract diseases, which may lead to a significant increase in mortality.

**Russian:**

Парижские медики дали абсолютно точные рекомендации.

**English:**

The Paris medics made absolutely precise recommendations.

**Russian:**

**ОТРАВЛЕНИЕ ФТОРИСТЫМ ВОДОРОДОМ**

**English:**

** HYDROGEN FLUORIDE POISONING**

**Russian:**

Жидкий фтористый водород является лучшим из всех известных растворителем белков.

**English:**

Liquid hydrogen fluoride is the best known solvent for proteins.

**Russian:**

При соединении с влагой воздуха пары фтористого водорода (1-й класс опасности) образуют плавиковую кислоту.

**English:**

When combined with air moisture, hydrogen fluoride vapour (hazard class 1) forms hydrofluoric acid.

**Russian:**

Все соли плавиковой кислоты ядовиты. Сама она при попадании на кожу вызывает образование болезненных и трудно заживающих ожогов (особенно под ногтями). Фтористый водород поражает слизистые оболочки глаз, рта, гортани, бронхов, желудка и легких, оказывает общее токсическое действие.

**English:**

All salts of hydrofluoric acid are poisonous. It itself in contact with skin causes formation of painful and hard-to-heal burns (especially under fingernails). Hydrogen fluoride affects mucous membranes of eyes, mouth, larynx, bronchi, stomach and lungs and has general toxic effect.

**Russian:**

Перед нами ясные причины для всех основных симптомов Черной Смерти:

**English:**

We have clear reasons for all the major symptoms of the Black Death in front of us:

**Russian:**

- массовый и внезапный характер поражения;

**English:**

- the massive and sudden nature of the defeat;

**Russian:**

- распухшие лимфатические узлы;

**English:**

- swollen lymph nodes;

**Russian:**

- обожженная кожа, незаживающие язвы;

**English:**

- burned skin, non-healing ulcers;

**Russian:**

- кровавая рвота, кровь из легких;

**English:**

- bloody vomit, bloody lungs;

**Russian:**

- быстрая смерть.

**English:**

- a quick death.

**Russian:**

**31.11. ЧЕРНОМОРСКАЯ ЧУМА 1348 ГОДА**

**English:**

**31.11. BLACK SEA PLAGUE OF 1348**

**Russian:**

Общемировая эпидемия Черной смерти 1348 года началась в Крыму, во время осады Каффы. Однако с востока от Черного моря нет значимых вулканов, и есть огромные запасы подземных газов. Стоит вспомнить, что именно в 1348 году, 25 января началось масштабное общеевропейское землетрясение со сходящимися горами и уходящими под землю городами. Черная смерть пришла следом, уже весной, как закономерный результат масштабных разломов и тектонических подвижек. В такой ситуации чудовищные запасы сероводорода на дне Черного моря это то, что вышло из недр юга России, но не сумело прорваться через Босфор в Европу. Этот сероводород -- отличный кандидат в инициаторы «эпидемии», но его происхождение не вулканическое, а теллурическое.

**English:**

The worldwide epidemic of the Black Death of 1348 began in the Crimea, during the siege of Caffa. However, there are no significant volcanoes to the east of the Black Sea, and there are huge reserves of underground gases. It is worth to remember that it was in 1348, on the 25th of January, when the large-scale all-European earthquake began with the converging mountains and the cities going under the ground. The Black Death came afterwards, already in spring, as a logical result of large faults and tectonic movements. In such a situation, the monstrous reserves of hydrogen sulphide at the bottom of the Black Sea are what came out of the bowels of the south of Russia, but failed to break through the Bosporus to Europe. This hydrogen sulfide is an excellent candidate for the initiators of the "epidemic", but its origin is not volcanic but telluric.

**Russian:**

**31.12. ИЗМЕНЕНИЕ ОТНОШЕНИЯ К ВОПРОСУ**

**English:**

**31.12. CHANGE OF ATTITUDE

**Russian:**

Википедия внесла в статью о Черной смерти весьма серьезные правки. Указаны работы, направленные против отождествления Черной смерти и Y. pestis.

**English:**

Wikipedia has made some very serious edits to the article on the Black Death. Works against the identification of the Black Death and Y. pestis.

**Russian:**

- книга британского зоолога Грэма Твигга «Чёрная смерть: пересмотр господствующей теории с точки зрения биологии» (англ. The Black Death: A Biological Reappraisal);

**English:**

- The Black Death: A Biological Reappraisal, a book by British zoologist Graham Twigg;

**Russian:**

- работа демографа Сюзанны Скотт в соавторстве с биологом Кристофером Дунканом «Биология чумных эпидемий» (англ. The Biology of Plagues);

**English:**

- The Biology of Plagues, co-authored by demographer Susannah Scott and biologist Christopher Duncan;

**Russian:**

- работа Сэмюэля Кона, профессора медиевистики из Университета Глазго «Нетрадиционный взгляд на проблему Чёрной смерти» (англ. Black Death Transformed).

**English:**

- Samuel Cohn, professor of medieval studies at the University of Glasgow, in Black Death Transformed, an unconventional perspective.

**Russian:**

**31.13. АРГУМЕНТЫ КРИТИКОВ**

**English:**

**31.13. ARGUMENTS OF THE CRITICS**

**Russian:**

Чума в норме движется со скоростью около 20 миль в год, в то время как для Чёрной смерти эта цифра составляла 2,5 мили в сутки.

**English:**

The plague normally moves at about 20 miles per year, while for the Black Death the figure was 2.5 miles per day.

**Russian:**

В Индии заболеваемость чумой падала зимой и возобновлялась весной, в то время как для Чёрной смерти подобного не замечено.

**English:**

In India, the incidence of plague dropped in winter and resumed in spring, while no such thing was seen for the Black Death.

**Russian:**

Чуме предшествует массовая гибель крыс, чего в свидетельствах о Черной смерти не отмечалось.

**English:**

The plague is preceded by a mass death of rats, which was not noted in the accounts of the Black Death.

**Russian:**

Третья пандемия чумы унесла в Индии около 3 % населения, в то время как Чёрная смерть сократила население Европы, по самым скромным подсчётам, на треть.

**English:**

The third pandemic plague killed about 3% of the population in India, while the Black Death reduced the population of Europe, by the most conservative estimate, by a third.

**Russian:**

Основная форма Черной смерти «лёгочная», а при чуме больных с поражениями лёгких от 3 % до 15-25 %.

**English:**

The main form of Black Death is "pulmonary," and in plague patients with lung lesions range from 3% to 15-25%.

**Russian:**

Для Черной смерти характерны бубоны за ушами и на шее, а для чумы -- на ногах, где обычно блоха и кусает.

**English:**

The Black Death is characterized by buboes behind the ears and on the neck, while the plague is characterized by buboes on the legs, where the flea usually bites.

**Russian:**

Для Черной смерти характерны гангренозное воспаление горла и лёгких, сильные боли в области сердца, кровохаркание и кровавая рвота, а также тяжёлый запах, исходивший от больных. Чума этого набора симптомов не дает [^135].

**English:**

Black Death is characterized by gangrenous inflammation of the throat and lungs, severe pain in the heart area, hemoptysis and bloody vomiting, and a heavy odor emanating from the sick. Plague does not give this set of symptoms [^135].

**Russian:**

Чуму понимали как отравление, в частности, падающим с неба пеплом, и лечили, в частности, «териаком» и соединениями ртути (мочегонные), и это лечение вполне адекватно диагнозу.

**English:**

The plague was understood to be poisoning by ash falling from the sky in particular, and was treated by "teriak" and mercury compounds (diuretics), among others, and this treatment was quite adequate for the diagnosis.

**Russian:**

**32. МАССОВЫЕ ПСИХОЗЫ XI-XVIII ВВ**

**English:**

**32. MASS PSYCHOSES OF THE XI-XVIII CC**

**Russian:**

Вот краткий перечень известных видов психических эпидемий:

**English:**

Here is a brief list of known types of mental health epidemics:

**Russian:**

- пляска св. Витта, тарантизм, эпидемии неистовой пляски;

**English:**

- the St. Vitus dance, tarantism, epidemics of frantic dancing;

**Russian:**

- эпидемии конвульсионеров, эпидемии судорог, икоты и тика;

**English:**

- an epidemic of convulsions, an epidemic of seizures, hiccups and tics;

**Russian:**

- эпидемии кликушества, бесоодержимость, звероодержимость;

**English:**

- epidemics of cliquishness, cluelessness, bestiality;

**Russian:**

- самобичевание;

**English:**

- self-abuse;

**Russian:**

- эпидемии массовых самосожжений и массовый суицид.

**English:**

- epidemics of mass self-immolations and mass suicides.

**Russian:**

Энциклопедии рассматривают массовые психозы с позиций ницшеанства: общество это безвольная животная толпа под гипнозом лидера, пребывающего в состоянии личной иллюзии. Физиологические проявления «психических эпидемий» с узко медицинских позиций не рассматриваются.

**English:**

Encyclopedias look at mass psychoses from a Nietzschean point of view: society is a willless animal mob under the hypnosis of a leader in a state of personal illusion. Physiological manifestations of "psychic epidemics" are not considered from narrowly medical positions.

**Russian:**

**32.1. ФИЗИОЛОГИЧЕСКИЕ ПРОЯВЛЕНИЯ ПСИХОЗОВ**

**English:**

**32.1. PHYSIOLOGICAL MANIFESTATIONS OF PSYCHOSES**

**Russian:**

1021 год. В Дессау у оставшихся в живых, всю жизнь тряслись члены.

**English:**

1021. In Dessau, the survivors had their dicks shaking all their lives.

**Russian:**

1479 год. Болезнь выражалась в мучительных пароксизмах и конвульсиях.

**English:**

1479. The disease was expressed in excruciating paroxysms and convulsions.

**Russian:**

1628 год. Монахини бились в судорогах и конвульсиях.

**English:**

1628. The nuns were beating in convulsions and convulsions.

**Russian:**

1728 год. Больные предавались кривляньям и подергиваниям.

**English:**

1728. The sick indulged in wiggling and twitching.

**Russian:**

1844 год. Эпидемия икоты в Моравии.

**English:**

1844. Hiccup epidemic in Moravia.

**Russian:**

1848 год. Эпидемия судорог близ Парижа [^113].

**English:**

1848. A cramp epidemic near Paris [^113].

**Russian:**

**32.2. СВЯЗЬ ПСИХОЗОВ И ЧЕРНОЙ СМЕРТИ**

**English:**

**32.2. THE CONNECTION BETWEEN PSYCHOSIS AND BLACK DEATH**

**Russian:**

Психические эпидемии уверенно связаны с приходом Черной смерти, - в простой выборке то и другое совпало в 35 % случаев. Как пример, возьмем по 1 совпадению на столетие, этого достаточно.

**English:**

Mental epidemics are strongly associated with the arrival of the Black Death - in a simple sample both coincided in 35% of cases. As an example, let's take 1 coincidence per century, that's enough.

**Russian:**

1260-1261 гг. Годы Черной смерти в Европе.

**English:**

1260-1261. The years of the Black Death in Europe.

**Russian:**

1260--1261 гг. Всеевропейская эпидемия самобичевания.

**English:**

1260--1261. A pan-European epidemic of self-abuse.

**Russian:**

1346-1352 гг. «Черная смерть» убила 25 млн. человек.

**English:**

1346-1352. "The Black Death killed 25 million people.

**Russian:**

1346--1352 гг. Всеевропейские психические эпидемии.

**English:**

1346--1352. All-European mental epidemics.

**Russian:**

1418 год. Черная смерть во Франции.

**English:**

1418. The Black Death in France.

**Russian:**

1418 год. Эпидемия неистовой пляски во Франции.

**English:**

1418. An epidemic of frantic dancing in France.

**Russian:**

1551 год. Эпидемия чумы в Европе.

**English:**

1551. Plague epidemic in Europe.

**Russian:**

1551 год. Эпидемии самобичевания в Европе.

**English:**

1551. Epidemics of self-abuse in Europe.

**Russian:**

1606 год. Эпидемия чумы в России.

**English:**

1606. Plague epidemic in Russia.

**Russian:**

1606 год. Эпидемия кликушества в России.

**English:**

1606. An epidemic of cuneiformity in Russia.

**Russian:**

1769 год. Эпидемия чумы в России.

**English:**

1769. Plague epidemic in Russia.

**Russian:**

1769 год. Эпидемия кликушества в России.

**English:**

1769. An epidemic of cuneiformity in Russia.

**Russian:**

**32.3. ТОЧНЫЙ ДИАГНОЗ**

**English:**

**32.3. ACCURATE DIAGNOSIS**

**Russian:**

Черная смерть это в большинстве случаев отравление газами. При таких отравлениях поражаются и базальные ганглии (подкорковые нейронные узлы). В результате возникает заболевание, имеющее конкретный медицинский диагноз Delirium acutum и десять симптомов, целиком идентичных симптомам психических эпидемий. Стоит подчеркнуть: диагноз Delirium acutum исчерпывает все без исключения варианты массовых психозов средневековья.

**English:**

Black death is mostly gas poisoning. In such poisonings the basal ganglia (subcortical neural nodes) are also affected. The result is a disease that has a specific medical diagnosis of Delirium acutum and ten symptoms that are entirely identical with those of psychotic epidemics. It is worth emphasizing: the diagnosis of Delirium acutum exhausts all variants of mass psychoses of the Middle Ages without exception.

**Russian:**

**32.4. СРАВНЕНИЕ СИМПТОМОВ**

**English:**

**32.4. SYMPTOM COMPARISON**

**Russian:**

Сравним симптомы Delirium acutum и St. Vitus' Dance

**English:**

Let's compare the symptoms of Delirium acutum and St. Vitus' Dance

**Russian:**

**СИМПТОМ № 1**

**English:**

**SYMPTOM NUMBER 1**

**Russian:**

**[Delirium acutum]**: клонические и тонические судороги, жевательные движения, эпилептиформные припадки.

**English:**

**[Delirium acutum]**: clonic and tonic convulsions, masticatory movements, epileptiform seizures.

**Russian:**

**ПОЯСНЕНИЕ № 1**: Клонические спазмы это синхронные толчкообразные сокращения мышц, чередующиеся с расслаблением (классика танца).

**English:**

** EXPLANATION #1**: Clonic spasms are synchronous thrust muscle contractions alternating with relaxation (classical dance).

**Russian:**

**ПОЯСНЕНИЕ № 2**: Тонические спазмы это длительное напряжение мышц.

**English:**

**EXPLANATION #2**: Tonic spasms are prolonged muscle tension.

**Russian:**

Рис. 89. Танцевальная эпидемия

**English:**

Figure 89. Dance epidemic

<!-- Local
[![](Step by step_files/67D098D181D182D0BED189D0B5D0BDD0B8D0B5.jpg#clickable)](Step by step_files/67%2B%25D0%2598%25D1%2581%25D1%2582%25D0%25BE%25D1%2589%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25B5.jpg)
-->

[![](https://1.bp.blogspot.com/-H_vRYmusae4/XlFFZDJmJwI/AAAAAAAADKA/IU5kLW3HhZ0oV8xaPXR9lRUE74W96qdywCLcBGAsYHQ/s1600/67%2B%25D0%2598%25D1%2581%25D1%2582%25D0%25BE%25D1%2589%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25B5.jpg#clickable)](https://1.bp.blogspot.com/-H_vRYmusae4/XlFFZDJmJwI/AAAAAAAADKA/IU5kLW3HhZ0oV8xaPXR9lRUE74W96qdywCLcBGAsYHQ/s1600/67%2B%25D0%2598%25D1%2581%25D1%2582%25D0%25BE%25D1%2589%25D0%25B5%25D0%25BD%25D0%25B8%25D0%25B5.jpg)

**Russian:**

**[St Vitus' Dance]**: на судороги прямо указано в эпидемиях 1628, 1848, 1882, 1894 годов.

**English:**

**[St Vitus' Dance]**: Convulsions are explicitly referred to in the epidemics of 1628, 1848, 1882, 1894.

**Russian:**

**Припадки с конвульсиями** прямо отмечены в 1479, 1480, 1490, 1491, 1628, 1632, 1642, 1728, 1762 годах.

**English:**

**Convulsive seizures** are expressly noted in 1479, 1480, 1490, 1491, 1628, 1632, 1642, 1728, 1762.

**Russian:**

**Одержимость бесами и звероодержимость** так же предполагают эпилептиформные припадки и конвульсии, а это эпидемии в 1431, 1484, 1499, 1504, 1507, 1515, 1527, 1534, 1550, 1552, 1554, 1564, 1566, 1574, 1577, 1579, 1580, 1582, 1590, 1609, 1610, 1613, 1627, 1628, 1632, 1652, 1669, 1681, 1687, 1732, 1733, 1857, 1878, 1878, 1883, 1893 годах. Всего -- 41 эпидемия.

**English:**

**Possession by demons and bestiality** also suggest epileptiform seizures and convulsions, and these are epidemics in 1431, 1484, 1499, 1504, 1507, 1515, 1527, 1534, 1550, 1552, 1554, 1564, 1566, 1574, 1577, 1579, 1580, 1582, 1590, 1609, 1610, 1613, 1627, 1628, 1632, 1652, 1669, 1681, 1687, 1732, 1733, 1857, 1878, 1878, 1883, 1893. Total -- 41 epidemics.

**Russian:**

**Кликушество**: больной катается по полу, беспорядочно мечется, бьет руками и ногами об пол, извивается. Эпидемии 1606, 1666, 1677, 1714, 1718, 1729, 1762, 1769, 1785, 1815, 1827, 1843, 1861 годов. Всего - 13 эпидемий.

**English:**

**Cliciousness**: sick person rolls on floor, rushes about disorderly, beats hands and feet on floor, wriggles. Epidemics of 1606, 1666, 1677, 1714, 1718, 1729, 1762, 1769, 1785, 1815, 1827, 1843, 1861. Thirteen epidemics in all.

**Russian:**

Ну, и сами **неистовые пляски** (тот же «тарантизм») выражаются прежде всего в «мучительных пароксизмах», конвульсиях и «подлинном мышечном сумасшествии». Даты эпидемий: 1021, 1237, 1237, 1278, 1370, 1374, 1375, 1375, 1375, 1376, 1418, 1418, 1418, 1418, 1479, 1482, 1490, 1518, 1518, 1518, 1643, 1680, 1686, 1715, 1728, 1762, 1840 годов. Всего -- 27 эпидемий.

**English:**

Well, the **neist dances** themselves (the same "tarantism") are expressed first of all in "excruciating paroxysms", convulsions and "true muscular madness". Dates of the epidemics: 1021, 1237, 1237, 1278, 1370, 1374, 1375, 1375, 1376, 1418, 1418, 1418, 1418, 1479, 1482, 1490, 1518, 1518, 1518, 1643, 1680, 1686, 1715, 1728, 1762, 1840. A total of -- 27 epidemics.

**Russian:**

**СИМПТОМ № 2**

**English:**

**SYMPTOM #2**

**Russian:**

**[Delirium acutum]**: гиперкинезы (хореатический, атетоидный, миоклонический) - неистовое, некоординированное двигательное возбуждение.

**English:**

**[Delirium acutum]**: Hyperkinesias (choreatic, athetoid, myoclonic) - frenetic, uncoordinated motor agitation.

**Russian:**

**ПОЯСНЕНИЕ № 1**: Гиперкинез хореатический, хорея (chorea) представляет собой пестрый ряд неритмических и весьма разнообразных быстрых движений, из которых каждое сходно с произвольным сокращением, вследствие чего больные нередко производят первое впечатление капризных, непоседливых людей, проделывающих излишние гримасы и движения. Движения эти не ограничиваются какой-нибудь определенной мышечной группой, а появляются то в одном, то в другом отделе.

**English:**

**EXPLANATION #1**: Hyperkinesis, chorea, is a motley series of irrhythmic and highly varied rapid movements, of which each resembles an arbitrary contraction, whereby the patients often give the first impression of capricious, fidgety people, making excessive grimaces and movements. These movements are not confined to any particular muscle group, but appear now and then in one or another department.

**Russian:**

**ПОЯСНЕНИЕ № 2**: Гиперкинез атетоидный: характеризуется непроизвольными медленными стереотипными вычурными движениями.

**English:**

**EXPLANATION #2**: Athetoid hyperkinesis: characterized by involuntary slow, stereotyped pretentious movements.

**Russian:**

**ПОЯСНЕНИЕ № 3**: Гиперкинез миоклонический: выделяют: локальные и генерализованные, одно- или двусторонние, синхронные и несинхронные, ритмичные и неритмичные формы миоклонии.

**English:**

** EXPLANATION NO. 3**: Myoclonic hyperkinesis: there are: localized and generalized, unilateral or bilateral, synchronous and nonsynchronous, rhythmic and nonrhythmic forms of myoclonias.

**Russian:**

Рис. 90. Хорея Сиденхема

**English:**

Figure 90. Sidenham's chorea

<!-- Local
[![](Step by step_files/68Horeomania.jpg#clickable)](Step by step_files/68%2BHoreomania.jpg)
-->

[![](https://1.bp.blogspot.com/-xWVdymTzmV8/XlFFrTmKkUI/AAAAAAAADKI/9rNGWmum8NgNdKBQa3losBw8SHW6LxyVgCLcBGAsYHQ/s1600/68%2BHoreomania.jpg#clickable)](https://1.bp.blogspot.com/-xWVdymTzmV8/XlFFrTmKkUI/AAAAAAAADKI/9rNGWmum8NgNdKBQa3losBw8SHW6LxyVgCLcBGAsYHQ/s1600/68%2BHoreomania.jpg)

**Russian:**

**[St Vitus' Dance]**: картина всех 27-ми эпидемий неистовых плясок совпадает с перечисленной выше симптоматикой до малейших деталей. Сюда же относятся все эпидемии конвульсионеров, бесоодержимости и кликушества.

**English:**

**[St Vitus' Dance]**: The pattern of all 27 epidemics of frantic dancing coincides with the symptomatology listed above to the smallest detail. This also includes all the epidemics of convulsions, insomniacs and cliques.

**Russian:**

**СИМПТОМ № 3**

**English:**

**SYMPTOM #3**

**Russian:**

**[Delirium acutum]**: чувство физического недомогания, головные боли, расстройство сна с кошмарными сновидениями, настроение то капризно-подавленное, то умиленно-оптимистическое.

**English:**

**[Delirium acutum]**: feeling physically unwell, headaches, sleep disturbance with nightmarish dreams, moods alternately moody and depressed and moodily optimistic.

**Russian:**

**[St Vitus' Dance]**: в летописях отражены все варианты настроения.

**English:**

**[St Vitus' Dance]**: The annals reflect all mood options.

**Russian:**

**СИМПТОМ № 4**

**English:**

**SYMPTOM #4**

**Russian:**

**[Delirium acutum]**: речь бессвязна и состоит из отдельных слов или выкриков.

**English:**

**[Delirium acutum]**: speech is incoherent and consists of single words or shouts.

**Russian:**

**[St Vitus' Dance]**: Бессвязная речь кликуш -- классика. Подходят все 13 эпидемий.

**English:**

**[St Vitus' Dance]**: The incoherent speech of the cliques -- classic. All 13 epidemics fit.

**Russian:**

А вот как описываются конвульсивно-танцевальное безумие в Сен-Медар: «среди всего этого нестройного шабаша слышатся только пение, рев, свист, декламация, пророчества и мяуканье». То есть, все 27 эпидемий этот симптом имеют.

**English:**

And this is how the convulsive-dancing madness at Saint-Medar is described: "amidst all this discordant coven one hears only singing, roaring, whistling, reciting, prophesying and meowing". That is, all 27 epidemics have this symptom.

**Russian:**

То же относится и к 41 эпидемии бесоодержимости и звероодержимости.

**English:**

The same is true of the 41 epidemics of beastlessness and bestiality.

**Russian:**

**СИМПТОМ № 5**

**English:**

**SYMPTOM #5**

**Russian:**

**[Delirium acutum]**: галлюцинации, бредовые высказывания.

**English:**

**[Delirium acutum]**: hallucinations, delusions.

**Russian:**

**[St Vitus' Dance]**: вот хороший пример. 1491-1494 эпидемия в монастыре Камбрэ. Монахини бегали собаками, порхали птицами, карабкались кошками и т. д. Относительно одной монахини выяснилось, что она находилась в связи с дьяволом с девяти лет. Во время эпидемии пляски св. Витта в 1375 г. в Германии многие утверждали, что видели под ногами струящиеся по земле потоки крови, из которых они старались выскочить и потому высоко подбрасывали ноги.

**English:**

**[St Vitus' Dance]**: Here's a good example. 1491-1494 epidemic in the convent of Cambrai. Nuns ran dogs, fluttered birds, climbed cats, etc. Concerning one nun it was discovered that she had been in communion with the devil since she was nine years old. During the epidemic of St Witt's dance in Germany in 1375, many people claimed they saw streams of blood streaming along the ground under their feet, from which they tried to jump out and therefore threw their feet high.

**Russian:**

Рис. 91. Танцевальная галлюционация

**English:**

Figure 91. Dance hallucination

<!-- Local
[![](Step by step_files/69pest.jpg#clickable)](Step by step_files/69%2Bpest.jpg)
-->

[![](https://1.bp.blogspot.com/-aGnp2fCkBV4/XlFF8J9dOeI/AAAAAAAADKQ/1YqGU_dyxoEMkvRl1rT6fUECc1RFqzfAgCLcBGAsYHQ/s1600/69%2Bpest.jpg#clickable)](https://1.bp.blogspot.com/-aGnp2fCkBV4/XlFF8J9dOeI/AAAAAAAADKQ/1YqGU_dyxoEMkvRl1rT6fUECc1RFqzfAgCLcBGAsYHQ/s1600/69%2Bpest.jpg)

**Russian:**

Подобных признаний (часто добровольных и вполне искренних) в архивах инквизиции -- тысячи, они характерны и для пляшущих, и для бесо-зверо-одержимых, и для кликуш.

**English:**

There are thousands of such confessions (often voluntary and sincere) in the archives of the Inquisition and they are typical for the dancing, the bezo-confessed and the hysterical.

**Russian:**

**СИМПТОМ № 6**

**English:**

**SYMPTOM #6**

**Russian:**

**[Delirium acutum]**: помрачение сознания, обычно в форме аменции или онейроида.

**English:**

**[Delirium acutum]**: lucidity, usually in the form of amnesia or oneroid.

**Russian:**

**ПОЯСНЕНИЕ**: Онейроидный синдром (онейроид) характеризуется особым видом качественного нарушения сознания (грезоподобная дезориентировка) с наличием развёрнутых картин сновидных и псевдогаллюцинаторных переживаний. Дезориентировка во времени и пространстве (иногда и в собственной личности) имеет особенности: при онейроиде больной является участником переживаемой псевдогаллюцинаторной ситуации. Окружающие люди могут включаться больным в виде участников в переживаемую ситуацию.

**English:**

**MEMBERSHIP**: Oneroid syndrome is characterized by a specific type of qualitative disorientation of consciousness (dreamlike disorientation) with the presence of detailed pictures of dream and pseudo-hallucinatory experiences. Disorientation in time and space (sometimes in the patient's own personality) has peculiarities: in the case of oneiroid the patient is a participant in the experienced pseudo-hallucinatory situation. Surrounding people can be included by the patient as participants in the experienced situation.

**Russian:**

Рис. 92. Ликантропия 1512 года

**English:**

Figure 92. Lycanthropy 1512

<!-- Local
[![](Step by step_files/70D09BD0B8D0BAD0B0D0BDD182D180D0BED0BFD0B8D18F1512.png#clickable)](Step by step_files/70%2B%25D0%259B%25D0%25B8%25D0%25BA%25D0%25B0%25D0%25BD%25D1%2582%25D1%2580%25D0%25BE%25D0%25BF%25D0%25B8%25D1%258F%2B1512.png)
-->

[![](https://1.bp.blogspot.com/-De6J_Taqd0A/XlFHeq5Ac_I/AAAAAAAADKg/ZNm39-_KGhg9p06Fb7nJswBfkbTOWH3iACLcBGAsYHQ/s1600/70%2B%25D0%259B%25D0%25B8%25D0%25BA%25D0%25B0%25D0%25BD%25D1%2582%25D1%2580%25D0%25BE%25D0%25BF%25D0%25B8%25D1%258F%2B1512.png#clickable)](https://1.bp.blogspot.com/-De6J_Taqd0A/XlFHeq5Ac_I/AAAAAAAADKg/ZNm39-_KGhg9p06Fb7nJswBfkbTOWH3iACLcBGAsYHQ/s1600/70%2B%25D0%259B%25D0%25B8%25D0%25BA%25D0%25B0%25D0%25BD%25D1%2582%25D1%2580%25D0%25BE%25D0%25BF%25D0%25B8%25D1%258F%2B1512.png)

**Russian:**

В течении онейроида иногда различают несколько этапов: начальный (стадия аффективных расстройств), стадия бредового настроения, стадия бреда инсценировки с ложными узнаваниями, стадия фантастической парафрении, стадия истинного онейроида.

**English:**

The course of oneyroidism sometimes distinguishes between several stages: the initial (affective disorder stage), delusional mood stage, staging delirium stage with false recognitions, fantastical paraphrenia stage, and true oneyroid stage.

**Russian:**

**[St Vitus' Dance]**: можно целиком поместить сюда все, сказанное в предыдущем пункте, а акцентировать внимание на следующем: «стадия бреда инсценировки с ложными узнаваниями». С таким симптомом больной мог «узнать» агента сатаны в любом человеке, да еще и включить в псевдогаллюцинаторную ситуацию себя.

**English:**

**[St Vitus' Dance]**: you can put everything said in the previous paragraph here in its entirety, but focus on the following: "staging delirium stage with false recognitions." With such a symptom, the patient could "recognize" a Satanic agent in any person, and even include himself in the pseudo-hallucinatory situation.

**Russian:**

Рис. 93. Коллективный оммаж

**English:**

Figure 93. Collective omens

<!-- Local
[![](Step by step_files/71Maleficarum-1628.jpg#clickable)](Step by step_files/71%2BMaleficarum-1628.jpg)
-->

[![](https://1.bp.blogspot.com/-imH7loAYnkk/XlFHY5YWshI/AAAAAAAADKc/8fQKIjXfaGkj6te92Jvqi7a9-PJ4-BBXQCLcBGAsYHQ/s1600/71%2BMaleficarum-1628.jpg#clickable)](https://1.bp.blogspot.com/-imH7loAYnkk/XlFHY5YWshI/AAAAAAAADKc/8fQKIjXfaGkj6te92Jvqi7a9-PJ4-BBXQCLcBGAsYHQ/s1600/71%2BMaleficarum-1628.jpg)

**Russian:**

Инквизиции можно было себя не утруждать: подскажи базовый сценарий, а остальное больной создаст в уме сам.

**English:**

The Inquisition could not be bothered: give a basic scenario, and the patient will create the rest in his mind by himself.

**Russian:**

**СИМПТОМ № 7**

**English:**

**SYMPTOM #7**

**Russian:**

**[Delirium acutum]**: помрачение сознания в виде делирия, тревожно-депрессивный аффект или страх.

**English:**

**[Delirium acutum]**: confusion in the form of delirium, anxious-depressive affect or fear.

**Russian:**

М. б. бред отравления - характеризуется идеей о применении по отношению к больному ядовитых веществ, о добавлении их в пищу, воду, о распылении их в воздухе с целью нанесения вреда здоровью больного или его убийства.

**English:**

Poisoning delirium is characterized by the idea of applying poisonous substances to the patient, adding them to food, water, spraying them in the air in order to harm or kill the patient.

**Russian:**

**[St Vitus' Dance]**: этот симптом идеально подогревало то, что на поля Европы выпадал серый порошок, скорее всего, фтороводородный вулканический пепел, ядовитый для скота и, отчасти, для людей. При французском дворе разразилась психическая «эпидемия отравлений», то же произошло и в Италии, а народ охотно воспринял идею о намеренном отравлении врагами колодцев, скважин и рек. В итоге идея о вредительстве вселенского масштаба вошла даже в культуру.

**English:**

**[St Vitus' Dance]**: this symptom was perfectly fueled by the fact that gray powder, most likely hydrofluoric volcanic ash, poisonous to cattle and, in part, to humans, was falling over the fields of Europe. A psychotic 'poisoning epidemic' broke out at the French court, and the same occurred in Italy, with people eagerly embracing the idea of enemies deliberately poisoning wells, boreholes and rivers. Eventually, the idea of sabotage on a universal scale entered even the culture.

**Russian:**

**СИМПТОМ № 8**

**English:**

**SYMPTOM #8**

**Russian:**

**[Delirium acutum]**: легко возникают множественные кровоподтеки.

**English:**

**[Delirium acutum]**: Multiple bruises easily occur.

**Russian:**

**[St Vitus' Dance]**: именно поэтому инквизиция в первую очередь искала на теле «знаки» бесовского одержания. С легкой руки Голливуда принято считать, что это обязательно родинки, но тот же «Молот ведьм» описывает и другое: например, вдумчивые поиски улик под снятой кожей обвиняемого. Под кожей родинок нет, а вот гематома может иметь самую причудливую форму. Больные манией «бесовских» плясок (Delirium acutum) с их уязвимостью кожи были исключительно легкой добычей.

**English:**

**[St Vitus' Dance]**: this is why the Inquisition was primarily looking for "signs" of demonic possession on the body. With Hollywood's lighter hand, it's accepted that these are necessarily moles, but the same "Hammer of Witches" also describes other things: for example, the thoughtful search for evidence under the skin of the accused that was removed. There are no moles under the skin, but the hematoma can have the most bizarre shape. The mania of "demonic" dancing (Delirium acutum), with its vulnerability of the skin, was exceptionally easy prey.

**Russian:**

Крайне важно и то, что кровоподтеки (геморрагии) -- особенность Черной смерти.

**English:**

Crucially, bruising (hemorrhages) is also a feature of the Black Death.

**Russian:**

**СИМПТОМ № 9. САМОБИЧЕВАНИЯ**

**English:**

**SIMPTOM NO. 9. SELF-ABUSE**

**Russian:**

**[Delirium acutum]**: для возбуждения и помраченного сознания больного характерен негативизм. Больной склонен причинять вред себе и другим.

**English:**

**[Delirium acutum]**: Negativity is characteristic of the patient's agitation and clouded consciousness. The patient tends to harm himself or others.

**Russian:**

**[St Vitus' Dance]**: Сказанное -- очень ясный общий симптом: и для эпидемий неистовых плясок, и для эпидемий самобичевания. Вот пара примеров причинения вреда себе.

**English:**

**[St Vitus' Dance]**: Said -- a very clear general symptom: for both epidemics of frenzied dancing and epidemics of self-harm. Here are a couple of examples of self-harm.

**Russian:**

1375 лето, эпидемия неистовой пляски. Больные... требовали, чтобы окружающие крепко перевязывали им животы полотенцами, топтали их ногами или били кулаками, что очень напоминает неистовства, случившиеся годами позже в Париже.

**English:**

1375 summer, an epidemic of frantic dancing. The sick... demanded that those around them tie towels tightly around their bellies, trample on them with their feet or beat them with their fists, very reminiscent of the rampages that occurred years later in Paris.

**Russian:**

1418 июля 14, Сен-Медар. «Женщины, растянувшись во весь рост, приглашают жителей бить их по животу и бывают довольны только тогда, когда десять или двенадцать мужчин обрушиваются на них сразу всей своей тяжестью»

**English:**

1418 July 14, Saint-Medar. "The women, stretched to their full height, invite the inhabitants to beat them on their bellies, and are only satisfied when ten or twelve men come down on them at once with all their weight."

**Russian:**

«Бывают довольны» не имеет сексуального или мазохистского подтекста. Вспомните зубную боль, когда не знаешь, куда голову засунуть, чтобы это прекратилось или хотя бы ослабло. Перед нами -- именно это.

**English:**

"Being content" has no sexual or masochistic overtones. Think of a toothache where you don't know where to put your head so it stops, or at least subsides. That's exactly what we have in front of us.

**Russian:**

Тот же посыл содержится в эпидемиях самобичевальщиков.

**English:**

The same message is contained in the epidemics of self-injurers.

**Russian:**

Рис. 94. Флагеллант

**English:**

Figure 94. Flagellant

<!-- Local
[![](Step by step_files/72flagellant.jpg#clickable)](Step by step_files/72%2Bflagellant.jpg)
-->

[![](https://1.bp.blogspot.com/-kroa47uTwkc/XlFHy3VnLRI/AAAAAAAADKs/FRwsqTPMCJMxAuDxyjTU0OmOVx0M75lbwCLcBGAsYHQ/s1600/72%2Bflagellant.jpg#clickable)](https://1.bp.blogspot.com/-kroa47uTwkc/XlFHy3VnLRI/AAAAAAAADKs/FRwsqTPMCJMxAuDxyjTU0OmOVx0M75lbwCLcBGAsYHQ/s1600/72%2Bflagellant.jpg)

**Russian:**

Больные наносили удары, как себе, так и другим. Вот годы этих эпидемий: 1260, 1261, 1266, 1296, 1333, 1348, 1349, 1414, 1414, 1414, 1551, 1812.

**English:**

Sick people struck, both themselves and others. Here are the years of these epidemics: 1260, 1261, 1266, 1296, 1333, 1348, 1349, 1414, 1414, 1551, 1812.

**Russian:**

Если же, возвращаясь к симптомам Delirium acutum, рассматривать манию причинения вреда другим людям, то в истории годится очень многое. Похоже, что все дело в степени заболевания: легкая форма, и ты -- среди палачей; тяжелая -- и ты -- в числе заведомых жертв.

**English:**

But if we return to the symptoms of Delirium acutum and consider mania of hurting other people, there is a lot of history. It seems to be all about the degree of the disease: a mild form, and you are among the executioners; a severe one, and you are among the known victims.

**Russian:**

**СИМПТОМ № 10. ФИНАЛЬНЫЙ**

**English:**

**SYMPTOM #10. FINAL**

**Russian:**

**[Delirium acutum]**: истощение, галопирующее развитие симптомов с частым смертельным исходом. Без лечения смерть частенько наступает спустя несколько дней -- в состоянии гипертермической комы. Выжившие -- из-за поражения подкорковых нейронных узлов -- часто остаются инвалидами.

**English:**

**[Delirium acutum]**: Exhaustion, galloping onset of symptoms with frequent death. Without treatment, death often occurs a few days later -- in a state of hyperthermic coma. Survivors, due to subcortical neuronal damage, are often disabled.

**Russian:**

**[St Vitus' Dance]**: свидетельств такому исходу масса. Я приведу четыре примера.

**English:**

**[St Vitus' Dance]**: The evidence for this outcome is overwhelming. I'll give you four examples.

**Russian:**

1021 год. Дессау. Крестьяне продолжали пляску до тех пор, пока некоторые из них не упали замертво; у прочих, оставшихся в живых, всю жизнь тряслись члены.

**English:**

1021. Dessau. The peasants continued dancing until some of them fell dead; others, who survived, had their dicks shaking all their lives.

**Russian:**

1337 год. Эпидемия неистовой пляски (св. Витта) детей в Эрфурте. Многие из этих детей умерли, другие навсегда остались подверженными пляске св. Витта.

**English:**

1337. An epidemic of frenzied dancing (St. Vitus) of children in Erfurt. Many of these children died, others were forever exposed to the dance of St. Vitus.

**Russian:**

1458 год. Никто из детей, принимавших участие в походе, не вернулся: часть погибла, как считают, от холода и голода.

**English:**

1458. None of the children who took part in the campaign returned: some are believed to have perished from cold and hunger.

**Russian:**

1518 год. Среди танцующих десятки погибли от сердечных приступов, инсультов и просто от истощения.

**English:**

1518. Among the dancers, dozens died of heart attacks, strokes and simply from exhaustion.

**Russian:**

**РЕЗЮМЕ**: все 10 симптомов Delirium acutum идентичны всем десяти характеристикам массовых психозов. Конкретное проявление психоза зависит от характера и степени поражения базальных ганглий.

**English:**

**SUMMARY**: All 10 symptoms of Delirium acutum are identical to all ten characteristics of mass psychosis. The specific manifestation of psychosis depends on the nature and extent of basal ganglia involvement.

**Russian:**

**32.5. УТОЧНЕНИЕ ИСТОЧНИКА БОЛЕЗНИ**

**English:**

**32.5. CLARIFICATION OF THE SOURCE OF THE DISEASE**

**Russian:**

Delirium acutum может возникать при:

**English:**

Delirium acutum can occur when:

**Russian:**

- пневмонии;

**English:**

- pneumonia;

**Russian:**

- плеврите;

**English:**

- pleurisy;

**Russian:**

- сепсисе;

**English:**

- sepsis;

**Russian:**

- галопирующей форме прогрессивного паралича;

**English:**

- a galloping form of progressive paralysis;

**Russian:**

- фебрильной шизофрении;

**English:**

- of febrile schizophrenia;

**Russian:**

- эпилепсии;

**English:**

- epilepsy;

**Russian:**

- инфекционных болезнях (холере, брюшном и сыпном тифе и др.);

**English:**

- Infectious diseases (cholera, typhoid, typhoid fever, etc.);

**Russian:**

- интоксикациях (например, отравлении тетраэтилсвинцом).

**English:**

- intoxications (e.g. tetraethyl lead poisoning).

**Russian:**

Причиной массового помешательства могут стать лишь две нижние строчки: инфекция и групповая интоксикация. Инфекция не подходит, поскольку не существует вирусов или бактерий, способных одновременно создавать: и симптомы Delirium acutum, и условия для эпидемического развития. По отдельности это возможно; тот же японский энцефалит закономерно приводит к гиперкинезу, - но не к эпидемии. Остается интоксикация.

**English:**

Only the bottom two lines can be the cause of mass insanity: infection and group intoxication. Infection is not suitable, since there are no viruses or bacteria capable of simultaneously creating: both the symptoms of Delirium acutum and the conditions for epidemic development. Individually it is possible; the same Japanese encephalitis legitimately leads to hyperkinesis - but not to an epidemic. That leaves intoxication.

**Russian:**

**32.6. ИСТОЧНИК ПСИХОЗОВ**

**English:**

**32.6. SOURCE OF PSYCHOSIS**

**Russian:**

Наличие тройной связи между массовыми психозами, Черной смертью и вулканическими газами объясним: 3 из 5 вулканических газов, отнесенных к СДЯВ (сильно действующим ядовитым веществам), относятся еще и к нейротропным ядам.

**English:**

The triple link between mass psychosis, the Black Death and volcanic gases can be explained: 3 of the 5 volcanic gases classified as poisonous substances are also neurotropic poisons.

**Russian:**

Хлористый водород (HCl),

**English:**

Hydrogen chloride (HCl),

**Russian:**

Фтористый водород (HF),

**English:**

Hydrogen fluoride (HF),

**Russian:**

Аммиак (NH3).

**English:**

Ammonia (NH3).

**Russian:**

Процесс доказательства завершен. Причина массовых психозов средневековья -- отравление вулканическими газами, обладающими нейротропным действием.

**English:**

The process of proof is complete. The cause of the mass psychoses of the Middle Ages is poisoning by volcanic gases, which have a neurotropic effect.

**Russian:**

**32.7. СОВРЕМЕННЫЙ АНАЛОГ**

**English:**

**32.7. MODERN COUNTERPART**

**Russian:**

Технология разрыва пластов для добычи сланцевого газа дает побочный эффект: водоносные пласты, а затем и вода из-под крана насыщаются органическими соединениями (бензол, толуол, ксилол, этилбензол), а также сероводородом и метиленхлоридом, что позволяет медикам прогнозировать долговременные неврологические проблемы [^134].

**English:**

Fracturing technology for shale gas extraction has a side effect: aquifers and then tap water are saturated with organic compounds (benzene, toluene, xylene, ethylbenzene) as well as hydrogen sulfide and methylene chloride, which allows medics to predict long-term neurological [^134] problems.

**Russian:**

**32.8. СОБЫТИЯ НА КЛАДБИЩЕ САН-МЕДАР**

**English:**

**32.8. EVENTS AT THE SAN-MEDAR CEMETERY**

**Russian:**

Психическая эпидемия парижских конвульсионеров на кладбище Сан-Медар, судя по наличию у янсенистов медитативных техник и четырем томам списка исцелений, это организованная попытка снимать у пациентов конвульсии без применения дефицитных спазмолитиков и обезболивающих (марихуаны и опиума, в частности). Множественные чудеса, на деле, это ожидаемые выздоровления тех, кого еще можно было спасти.

**English:**

The psychotic epidemic of Parisian convulsionists in the Saint-Medar Cemetery, judging by the Jansenists' meditative techniques and four volumes of healing lists, is an organized attempt to relieve patients from convulsions without the use of scarce antispasmodics and analgesics (marijuana and opium, in particular). The multiple miracles, in fact, are the expected recoveries of those who could still be saved.

**Russian:**

Рис. 95. Врачи и пациентка на кладбище Сан-Медар

**English:**

Figure 95. Doctors and a patient in the San-Medar cemetery

<!-- Local
[![](Step by step_files/73D181D0B0D0BDD0BCD0B5D0B4D0B0D180.jpg#clickable)](Step by step_files/73%2B%25D1%2581%25D0%25B0%25D0%25BD%25D0%25BC%25D0%25B5%25D0%25B4%25D0%25B0%25D1%2580.jpg)
-->

[![](https://1.bp.blogspot.com/-wUpq8cydsMc/XlFIBl6nJQI/AAAAAAAADKw/qYNZnWN5vKkYs8RTWiORzObf3wyFA6etQCLcBGAsYHQ/s1600/73%2B%25D1%2581%25D0%25B0%25D0%25BD%25D0%25BC%25D0%25B5%25D0%25B4%25D0%25B0%25D1%2580.jpg#clickable)](https://1.bp.blogspot.com/-wUpq8cydsMc/XlFIBl6nJQI/AAAAAAAADKw/qYNZnWN5vKkYs8RTWiORzObf3wyFA6etQCLcBGAsYHQ/s1600/73%2B%25D1%2581%25D0%25B0%25D0%25BD%25D0%25BC%25D0%25B5%25D0%25B4%25D0%25B0%25D1%2580.jpg)

**Russian:**

Взлет популярности этой практики, особенно среди знати, видимо, связан с перебоями в поставках опиума и марихуаны, а падение популярности и вырождение в карго-культ -- с возобновлением регулярных поставок медикаментов и снижением числа пострадавших.

**English:**

The rise in popularity of the practice, especially among the nobility, seems to be related to the disruptions in the supply of opium and marijuana, and the decline in popularity and degeneration into a kargo-cult with the resumption of regular supply of medicines and a decline in the number of victims.

**Russian:**

**Важная деталь**: среди конвульсионеров, арестованных в период между 1732 и 1774 годами, женщин было 90 %. Как и в случае с Черной смертью, женщины заболевают чаще, уже в силу того, что вынужденно спускаются к рекам постирать белье и набрать воды. Средневековые медики указывают на этот источник заболевания прямо, с четким пониманием происходящего. И, напротив, среди заключенных в Бастилию янсенистов 82 % - мужчины [^114]. Это означает, что янсенисты и конвульсионеры это две различные страты: янсенисты -- лекари, точный аналог врачей-священников, а конвульсионеры -- пациенты.

**English:**

**Important detail**: among the convulsionists arrested between 1732 and 1774, 90% were women. As in the case of the Black Death, women fell ill more frequently, already by virtue of having to go down to the rivers to wash laundry and draw water. Medieval medics point to this source of disease directly, with a clear understanding of what is happening. Conversely, among the Jansenist prisoners in the Bastille, 82% were men [^114]. This means that the Jansenists and the Convulsionists are two different strata: the Jansenists are the healers, the exact equivalent of the priestly doctors, and the Convulsionists are the patients.

**Russian:**

**[ГЕНЕТИЧЕСКИЕ ПОСЛЕДСТВИЯ]**

**English:**

**[Genetic Consequences]**

**Russian:**

**33.1. МАСШТАБНЫЕ МУТАЦИИ**

**English:**

**33.1. LARGE SCALE MUTATIONS**

**Russian:**

Воздействие химически активных (вулканических и теллурических) веществ на этапе смещения оси способно вызвать мутации не меньших масштабов, чем синтез дейтерия на предыдущем (метеорном) этапе. Не случайно культурные сорта риса, генетически отличающиеся от диких родственников, произошли около 8200 лет назад [^115], как раз на пике событий, связанных со смещением оси вращения Земли. На тот же период падают первые свидетельства о возделывании пшеницы и бобовых в долине Инда [^116] -- просто потому, что дикие формы мутировали, и эти культуры впервые появились. На этой единовременности появления культурных растений палеоботаники настаивают вполне уверенно.

**English:**

Exposure to chemically active (volcanic and telluric) substances during the axis shift stage is capable of causing mutations on no less a scale than deuterium synthesis during the previous (meteoric) stage. It is no coincidence that cultivated varieties of rice, genetically different from their wild relatives, occurred about 8200 years ago [^115], just at the peak of the Earth rotation axis shift events. In the same period falls the first evidence of wheat and legume cultivation in the Indus Valley [^116] -- simply because the wild forms mutated and these crops first appeared. Paleobotanists insist quite confidently on this simultaneous occurrence of cultivated plants.

**Russian:**

**33.2. МУТАЦИИ ДНК ЧЕЛОВЕКА**

**English:**

**33.2. HUMAN DNA MUTATIONS**

**Russian:**

Вместе с флорой и фауной переживет целый ряд мутаций мтДНК и человек -- не половым путем.

**English:**

Along with flora and fauna will survive a number of mtDNA mutations and humans -- non-sexually.

**Russian:**

1187 г. у всех родившихся в этот год во Франции вместо тридцати двух зубов было двадцать два [^117].

**English:**

1187 all those born in that year in France had twenty-two [^117] teeth instead of thirty-two.

**Russian:**

Овидий о времени Фаэтона: Тогда-то, говорят, у эфиопов разгорячённая кровь прилила к коже и очернила её навсегда.

**English:**

Ovid on the time of Phaeton: Then, they say, the Ethiopians' heated blood rushed to their skin and blackened it forever.

**Russian:**

Гигин о времени Фаэтона: У индов же от жара близкого солнца потемнела кровь и они стали черными.

**English:**

Hyginus of the time of Phaeton: The Indians had their blood darkened from the heat of the close sun, and they became black.

**Russian:**

Обратите внимание, гаплогруппы на картах напрочь игнорируют два фактора: естественные преграды (как водные, так и горные) и любимые человеком транспортные коридоры: реки, проливы и проходы между гор. Мутации ведут себя так, как повело бы себя несомое воздушными потоками и оседающее на землю радиоактивное облако -- прямо через море и наискосок через горный хребет. Именно поэтому, а не в силу перекрестных браков, разные этнические группы порой имеют идентичные мутации мт-ДНК.

**English:**

Note, the haplogroups on the maps completely ignore two factors: natural obstacles (both water and mountains) and favorite human transport corridors: rivers, straits and the passages between the mountains. Mutations behave like a stream-borne radioactive cloud would behave, straight across the sea and obliquely over a mountain range. This is why, not because of cross-breeding, different ethnic groups sometimes have identical mt-DNA mutations.

**Russian:**

Рис. 96. Гаплогруппа T

**English:**

Figure 96. Haplogroup T

<!-- Local
[![](Step by step_files/74D094D09DD09A.png#clickable)](Step by step_files/74%2B%25D0%2594%25D0%259D%25D0%259A.png)
-->

[![](https://1.bp.blogspot.com/-DEIggOcR8TE/XlFINIFSfMI/AAAAAAAADK4/p0O0E0g6DIshgc6kxRgpxwjt9m7GtZT7ACLcBGAsYHQ/s1600/74%2B%25D0%2594%25D0%259D%25D0%259A.png#clickable)](https://1.bp.blogspot.com/-DEIggOcR8TE/XlFINIFSfMI/AAAAAAAADK4/p0O0E0g6DIshgc6kxRgpxwjt9m7GtZT7ACLcBGAsYHQ/s1600/74%2B%25D0%2594%25D0%259D%25D0%259A.png)

**Russian:**

Так ведут себя даже базовые, наиболее фундаментальные гаплогруппы. Это заметно снижает роль человека в самоселекции и повышает роль внешних по отношению к человеку не половых факторов.

**English:**

Even the most basic, most fundamental haplogroups behave this way. This noticeably reduces the role of humans in self-selection and increases the role of external non-sex factors in relation to humans.

**Russian:**

Рис. 97. Базовые гаплогруппы

**English:**

Figure 97. Basic haplogroups

<!-- Local
[![](Step by step_files/75D094D09DD09A.jpg#clickable)](Step by step_files/75%2B%25D0%2594%25D0%259D%25D0%259A.jpg)
-->

[![](https://1.bp.blogspot.com/-wQ-wvARJMMc/XlFIRwjTb9I/AAAAAAAADLA/0Nu2tlLKvXEWqkmPAjyfwxmpAF1oD0GTACLcBGAsYHQ/s1600/75%2B%25D0%2594%25D0%259D%25D0%259A.jpg#clickable)](https://1.bp.blogspot.com/-wQ-wvARJMMc/XlFIRwjTb9I/AAAAAAAADLA/0Nu2tlLKvXEWqkmPAjyfwxmpAF1oD0GTACLcBGAsYHQ/s1600/75%2B%25D0%2594%25D0%259D%25D0%259A.jpg)

**Russian:**

Достаточно важно, что разделение людей на расы произошло синхронно с экскурсом магнитного поля Земли (событие Лашан). Здесь не важна дата; ее можно и пересмотреть; здесь важна прочная логическая связь между экскурсом и всем пакетом сопутствующих явлений, включая и мутации мт-ДНК.

**English:**

It is important enough that the division of humans into races occurred synchronously with the excursion of the Earth's magnetic field (the Lashan event). The date is not important here; it can also be revised; what is important here is a strong logical connection between the excursion and the entire package of related phenomena, including mtDNA mutations.

**Russian:**

**[СТАБИЛИЗАЦИЯ ПЛАНЕТЫ]**

**English:**

**[PLANET STABILIZATION] **

**Russian:**

**34. ЗАВЕРШЕНИЕ КАТАСТРОФЫ**

**English:**

**34. COMPLETION OF THE CATASTROPHE**

**Russian:**

Последние свидетельства, указывающие на плейстоценовый катастрофический сценарий, приходятся на XVIII-XIX века. Иссякают к 1880-м годам. Перечислим самое яркое.

**English:**

The latest evidence pointing to a Pleistocene catastrophic scenario comes from the 18th and 19th centuries. Exhausted by the 1880s. Let us list the most striking.

**Russian:**

**34.1. ФИНАЛЬНЫЕ СВИДЕТЕЛЬСТВА**

**English:**

**34.1. FINAL CERTIFICATES**

**Russian:**

1769 г. Замерзло Средиземное море

**English:**

1769 The Mediterranean Sea froze over

**Russian:**

1782 г. о. Формоза (Тайвань) почти весь на время затонул

**English:**

1782 The island of Formosa (Taiwan) was almost entirely submerged for a time

**Russian:**

1783 г. Заподозрено смещение оси вращения Земли

**English:**

1783 Earth rotation axis shift is suspected

**Russian:**

1808 г. 4-метровая трансгрессия Черного моря у берегов Румынии

**English:**

1808 г. 4-meter transgression of the Black Sea off the coast of Romania

**Russian:**

1812 г. В Миссисипи полоса земли 500 на 200 верст опустилась на восемь футов

**English:**

1812 г. In Mississippi a strip of land 500 by 200 versts has dropped eight feet

**Russian:**

1816 г. Год без лета

**English:**

1816 г. A year without summer

**Russian:**

1818 г. На части Калифорнии исчезло коренное население

**English:**

1818 г. In parts of California, the native population disappeared

**Russian:**

1822 г. Год без зимы, повсеместный сбой сезонов года

**English:**

1822 Year without winter, widespread disruption of the seasons

**Russian:**

1822 г. Исландия деблокирована от паковых льдов (после 402 лет блокады)

**English:**

1822 Iceland is unblocked from the pack ice (after 402 years of blockade)

**Russian:**

1825 г. В Боливии 98 % шахт (4847 из 4949) разом затопило

**English:**

1825 г. In Bolivia, 98% of mines (4,847 out of 4,949) flooded at once

**Russian:**

1832 г. В Париже холера, и в связи с этим осужден отравитель фонтанов

**English:**

1832 Cholera in Paris and the poisoner of the fountains is condemned in connection with it.

**Russian:**

1846 г. В Лионе кровавый дождь

**English:**

1846. Bloody rain in Lyon.

**Russian:**

1848 г. Эпидемия судорог близ Парижа

**English:**

1848, an epidemic of seizures near Paris.

**Russian:**

1851 г. Река Хуанхэ в Китае повернула на север и потекла по своему современному руслу

**English:**

1851 The Huang He River in China turns north and flows in its present course

**Russian:**

1855 г. Венера сияет посреди солнечного дня

**English:**

1855 Venus shining in the middle of a sunny day

**Russian:**

1855 г. США. Вода в Нью-Джерси настолько чиста, что есть рыба

**English:**

1855 USA. The water in New Jersey is so clear that there are fish

**Russian:**

1855 г. США. Вода Огайо содержит 46-48% сульфата извести

**English:**

1855 USA. Ohio water contains 46-48% lime sulfate

**Russian:**

1855 г. США. Дождевая вода для питья непригодна

**English:**

1855 USA. Rainwater is not suitable for drinking

**Russian:**

1855 г. США. Кейп-Флорида в этом году сместился на 6 миль

**English:**

1855 USA. Cape Florida shifted 6 miles this year

**Russian:**

1855 г. Соль из Египта и Ливии пахнет аммиаком

**English:**

1855 Salt from Egypt and Libya smells like ammonia

**Russian:**

1857 г. Самое сильное землетрясение в истории Европы (Базиликата)

**English:**

1857 Greatest earthquake in the history of Europe (Basilicata)

**Russian:**

1870 г. Массовая галлюцинация в прирейнских областях длилась несколько месяцев, созерцаемые видения повсюду были одинаковы

**English:**

1870 A mass hallucination in the Prussian provinces lasted several months, the contemplated visions were the same everywhere

**Russian:**

1871 г. Затонувший город Сент-Огастин (Флорида) ненадолго поднялся из воды

**English:**

1871 The sunken city of St. Augustine, Florida, rose briefly from the water

**Russian:**

1871 г. Одновременно в 21:30 вспыхнул пожар почти на всей территории США

**English:**

1871 At the same time, at 9:30 p.m., a fire broke out across almost all of the U.S.

**Russian:**

**И отдельно**:

**English:**

**And separately**:

**Russian:**

Гумбольдт зафиксировал намного большее, чем теперь, количество водорода в атмосфере. По меньшей мере, до 1855 года, зодиакальный свет был прекрасно виден. Гумбольдт утверждал, что это результат отражения света от пыли, окружающей Землю; считалось, что Земля окружена кольцом пыли, как Сатурн. В первой половине XIX века чрезвычайно актуальна задача постоянного наблюдения за колебаниями высот рельефа: в Баку, Казани, на Балтике. Учитывая трансгрессию Черного моря на 4 метра в 1808 году, удивляться не приходится.

**English:**

Humboldt recorded a much larger amount of hydrogen in the atmosphere than now. At least until 1855, zodiacal light was perfectly visible. Humboldt argued that this was the result of the reflection of light from the dust surrounding the Earth; the Earth was thought to be surrounded by a ring of dust, like Saturn. In the first half of the 19th century, the task of continual observation of elevation fluctuations was extremely relevant: in Baku, Kazan, and the Baltic. Given the transgression of the Black Sea by 4 meters in 1808, it is not surprising.

**Russian:**

**35.1. ПЫЛЬНЫЕ БУРИ**

**English:**

**35.1. DUST STORMS**

**Russian:**

В период 1842-1849 гг. прошла серия пыльных бурь, не имеющая аналогов в истории. Такого не случалось даже в период Черной смерти. Вопрос, что такое же беспрецедентное прямо перед этим произошло?

**English:**

Between 1842 and 1849 there was a series of dust storms unparalleled in history. Such a thing had not even happened during the Black Death. The question is, what was equally unprecedented just before it happened?

**Russian:**

1842 г. Ураган, засыпавший Закавказье песком

**English:**

1842 Hurricane that covered Transcaucasia with sand

**Russian:**

1842 г. Черный ураган 6 июня 1842 г. в Киеве

**English:**

1842 Black Hurricane on June 6, 1842 in Kiev

**Russian:**

1843 г. Сильный пыльный фён в Тироле и Зальцбурге.

**English:**

1843 Heavy dusty fog in Tyrol and Salzburg.

**Russian:**

1845 г. Цветные закаты в Европе с апреля по октябрь

**English:**

1845 Coloured sunsets in Europe from April to October

**Russian:**

1846 г. В Генуе и Сицилии сирокко с красно-желтой пылью, разрушениями и жертвами

**English:**

1846 In Genoa and Sicily, sirocco with red-yellow dust, destruction and casualties

**Russian:**

1846 г. Над Лионом ураган с красным дождем и красной пылью

**English:**

1846 Hurricane over Lyon with red rain and red dust

**Russian:**

1847 г. Красная пыль в Тироле

**English:**

1847 Red dust in Tyrol

**Russian:**

1847 г. Красная сирокковая пыль выпадает в Альпах

**English:**

1847 Red Sirocco dust falls in the Alps

**Russian:**

1848 г. Красно-бурый ураган с красным снегом в Австрии

**English:**

1848 Red-brown storm with red snow in Austria

**Russian:**

1849 г. Пыль выпала (1500 на 500 верст) на Украине

**English:**

1849 Dust fell (1,500 by 500 versts) in Ukraine

**Russian:**

1849 г. Черный дождь в Ирландии

**English:**

1849 Black rain in Ireland

**Russian:**

**35.2. ОСВОБОЖДЕНИЕ ЗАТОПЛЕННЫХ ЗЕМЕЛЬ**

**English:**

**35.2. RELEASE OF FLOODED LAND**

**Russian:**

Вопрос, что же такое беспрецедентное прямо перед пыльными бурями произошло? Ответ: освобождение затопленных земель от воды вследствие последних крупных тектонических подвижек. Хороший пример: Северное Причерноморье -- то самое, где в 755 году описана полоса льда в 23 метра толщиной и вмерзшие в этот лед дикие и домашние животные. С тех пор южнее Киева человек, по сути, не спускался. Именно с таких участков просыхающей почвы с еще не сформировавшейся корневой системой растений, - в Индии, в Африке, в Казахстане, - пыль и сдувало.

**English:**

The question is, what is so unprecedented right before the dust storms happened? The answer: the release of flooded land from water due to recent major tectonic shifts. A good example: The northern Black Sea coast -- the same one where in 755 a strip of ice 23 meters thick was described and wild and domestic animals frozen into that ice. Since then, as a matter of fact, man has not descended south of Kiev. It is from such patches of drying soil with a still unformed root system of plants - in India, Africa, Kazakhstan - the dust has been blown away.

**Russian:**

Рис. 98. Городища XIII века, города XVI века, храмы к XIX веку (52; 53; 54)

**English:**

Fig. 98. Settlements of XIII century, towns of XVI century, temples by XIX century (52; 53; 54).

<!-- Local
[![](Step by step_files/76D093D0BED180D0BED0B4D0B8D189D0B0.jpg#clickable)](Step by step_files/76%2B%25D0%2593%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25B8%25D1%2589%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-x9HURUlgHSQ/XlFIeUDkbvI/AAAAAAAADLI/EYwIL5iB8EQHzsFm9YkbdY6gZuW0prjVgCLcBGAsYHQ/s1600/76%2B%25D0%2593%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25B8%25D1%2589%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-x9HURUlgHSQ/XlFIeUDkbvI/AAAAAAAADLI/EYwIL5iB8EQHzsFm9YkbdY6gZuW0prjVgCLcBGAsYHQ/s1600/76%2B%25D0%2593%25D0%25BE%25D1%2580%25D0%25BE%25D0%25B4%25D0%25B8%25D1%2589%25D0%25B0.jpg)

**Russian:**

**35.3. ФОРМИРОВАНИЕ ПРОЛИВОВ БОСФОР И КАТТЕГАТ**

**English:**

**35.3. BOSPORUS AND KATTEGAT STRAITS FORMATION**

**Russian:**

И здесь возникает новый вопрос, а когда именно лишняя вода Черного моря сошла через Босфор, освободила земли Северного Причерноморья и сформировала на дне Эгейского моря сапропелевый слой? Видимо, не особо давно, поскольку вплоть до Крымской войны проливами Босфор и Каттегат (Дания) не ходят. В истории зафиксировано буквально по одному проходу флотов на пролив, и оба сомнительные.

**English:**

And here a new question arises, and when exactly the excess water of the Black Sea descended through the Bosporus, freed the lands of the Northern Black Sea and formed a sapropel layer at the bottom of the Aegean Sea? Apparently, not particularly long ago, because up to the Crimean War the Bosporus and Kattegat (Denmark) straits do not go. History records literally one passage of fleets to the strait, and both are questionable.

**Russian:**

Это пишется не столько для переоценки истории (это отдельная тема), сколько для получения комплексного видения прошедших катастрофических процессов. Если есть пыль, значит, есть и земля без травы; если нет городов, то это место для жизни было непригодным, а если вода сошла в соседний бассейн, тому должна быть причина. Тектонические события в качестве причины подходят.

**English:**

This is written not so much to re-evaluate history (that is a separate topic), but rather to gain a comprehensive view of past catastrophic processes. If there is dust, then there is land without grass; if there are no cities, then this place was unfit for life, and if water descended into the neighboring basin, there must be a reason for it. Tectonic events are suitable as a cause.

**Russian:**

**35.4. СМЫВ ПЫЛИ В НИЗОВЬЯ РЕК**

**English:**

**35.4. DUST WASHING INTO LOWER REACHES OF RIVERS**

**Russian:**

Итак, в 1842-1849 гг. прошла серия пыльных бурь, а уже 22 февраля 1851 года в г. Сан-Франциско вдоль стоящих на приколе кораблей появилась (пишут «была заполнена») Глиняная улица. Уже 26 марта штат отказался от права собственности на все участки ниже отметки верхнего уровня воды (high-water mark) в пределах города Сан-Франциско. 21 декабря 1852 года некий ливень нанес судоходству на причалах огромный ущерб, и в 1853 году начались аукционы по продаже водных участков на правах земельных.

**English:**

So, a series of dust storms took place between 1842 and 1849, and as early as February 22, 1851, Clay Street appeared (they write "was filled in") along the mooring ships in San Francisco. As early as March 26, the state relinquished ownership of all lots below the high-water mark (high-water mark) within the City of San Francisco. On December 21, 1852, a certain downpour caused tremendous damage to navigation on the levees, and in 1853 auctions began for the sale of water lots on land rights.

**Russian:**

**ГИПОТЕЗА**: Бухта Сан-Франциско была заполнена илом, снесенным в залив реками Сакраменто и Сан-Хоакин. Ил образовался из пыли, обильно выпадавшей по всей планете в период 1842-1849 гг.

**English:**

**HYPOTHEOSIS**: San Francisco Bay was filled with silt blown into the bay by the Sacramento and San Joaquin Rivers. The silt was formed from dust that fell profusely across the planet between 1842 and 1849.

**Russian:**

**РАСЧЕТ**

**English:**

**CALCULATION**

**Russian:**

Река Сакраменто: площадь водосборного бассейна --- 71432 км².

**English:**

Sacramento River: catchment area - 71432 km².

**Russian:**

Река Сан-Хоакин: Площадь бассейна --- 45000 км².

**English:**

San Joaquin River: Basin area - 45,000 km².

**Russian:**

Общая площадь бассейнов 116 тыс. км².

**English:**

The total area of the basins is 116 thousand km².

**Russian:**

Площадь залива Сан-Франциско до 4,1 тыс. км².

**English:**

The area of San Francisco Bay is up to 4.1 thousand km².

**Russian:**

Если в бассейны рек смоет 1 см пыли с каждого кв. метра, в заливе появится слой ила толщиной 28 см. Если считать, что ил не растекался по всему заливу, а собирался там, куда бьет течение, толщина наносов будет десятки метров -- как раз у Сан-Франциско.

**English:**

If 1 cm of dust per square meter washed into the river basins, that would leave a 28-cm-thick layer of silt in the Bay. If you assume that the silt is not spreading all over the bay, but collecting where the current is hitting, the thickness of the sediment would be tens of meters -- just outside San Francisco.

**Russian:**

Вот берег реки Сакраменто. Дерна и сейчас, по сути, нет. Грунт неустойчивый - красно-желтый, - как и тот, что отмечен исследователями пыли сирокко в 1842-1849 годах.

**English:**

This is the bank of the Sacramento River. The sod is still essentially gone. The soil is unstable - red-yellow - just like the one noted by sirocco dust researchers in 1842-1849.

**Russian:**

Рис. 99. Река Сакраменто

**English:**

Figure 99. Sacramento River

<!-- Local
[![](Step by step_files/77D0A0D0B5D0BAD0B0D0A1D0B0D0BAD180D0B0D0BCD0B5D0BDD182D0BE.jpg#clickable)](Step by step_files/77%2B%25D0%25A0%25D0%25B5%25D0%25BA%25D0%25B0%2B%25D0%25A1%25D0%25B0%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D0%25BE.jpg)
-->

[![](https://1.bp.blogspot.com/-g9NrlhPl82w/XlFI0y9cipI/AAAAAAAADLY/Nr6du_0EDKk3SDdo86Ur_c6_XAddAI4OgCLcBGAsYHQ/s1600/77%2B%25D0%25A0%25D0%25B5%25D0%25BA%25D0%25B0%2B%25D0%25A1%25D0%25B0%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D0%25BE.jpg#clickable)](https://1.bp.blogspot.com/-g9NrlhPl82w/XlFI0y9cipI/AAAAAAAADLY/Nr6du_0EDKk3SDdo86Ur_c6_XAddAI4OgCLcBGAsYHQ/s1600/77%2B%25D0%25A0%25D0%25B5%25D0%25BA%25D0%25B0%2B%25D0%25A1%25D0%25B0%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BC%25D0%25B5%25D0%25BD%25D1%2582%25D0%25BE.jpg)

**Russian:**

**ИСТОРИЧЕСКИЕ ПРЕЦЕДЕНТЫ**

**English:**

**HISTORICAL PRECEDENTS**

**Russian:**

1287 г. В Англии заиливание привело к тому, что стоящие у моря города оказались не имеющими выхода к морю

**English:**

1287 In England, siltation resulted in landlocked towns standing by the sea

**Russian:**

1425 г. Крупную гавань в Брюгге заволокло илом, так что город был отрезан от моря

**English:**

1425 г. A large harbour in Bruges was blocked by silt, so that the city was cut off from the sea

**Russian:**

1571 г. Ввиду заиления гавани древней Пизы порт перенесен в Ливорно

**English:**

1571 Due to the silting up of the harbour of ancient Pisa, the port is moved to Livorno

**Russian:**

1789 г. Индия. Устье р. Янаона стало несудоходным из-за наносов

**English:**

1789 India. The mouth of the Yanaon River became inoperative due to sedimentation

**Russian:**

**35.5. СХОД ОПОЛЗНЕЙ, СЕЛЕЙ И ЛАВИН**

**English:**

**35.5. LANDSLIDES, MUDFLOWS AND AVALANCHES**

**Russian:**

Оползни, сели и лавины XIX века следует считать частью процесса стабилизации планеты: горы освобождаются от заведомо лишней влаги. Из 756 исторических свидетельств можно выделить сходы лавин в Приэльбрусье в 1817 году, сопряженные с чумой, извержениями на Тамани и мгновенным закреплением России на Кавказе - без адекватных масштабам экспансии военных затрат. Повторное заселение целого ряда пустых ущелий местным населением шло уже во второй половине XIX века, причем, с верховий.

**English:**

Landslides, mudflows and avalanches of the XIX century should be considered as part of the process of stabilization of the planet: mountains are freed from knowingly excessive moisture. Among 756 historical evidences one can single out avalanches in the Elbrus region in 1817, associated with plague, eruptions in Taman and instantaneous consolidation of Russia in the Caucasus - without military expenditures adequate to the scale of expansion. Repopulation of a number of empty gorges by the local population proceeded already in the second half of XIX century, and from the upper reaches.

**Russian:**

Нуждается в исследовании единовременная гибель 96 кавказских аулов, зафиксированная в 1858 году, на следующий год после самого крупного в истории Европы землетрясения Базиликата и вскоре после аномальных ливней на Кавказе и серии пыльных бурь, осадки от которых обязаны были сильно перегрузить ледники, в том числе, и на Кавказе. Сочетание факторов способствует сходу беспрецедентных лавин. Насколько велико могло быть дополнительное давление на ледники, можно судить по остаткам наносов в горах Швейцарии.

**English:**

The one-time loss of 96 Caucasian auls recorded in 1858, the year following the largest Basilicata earthquake in European history and shortly after abnormal rains in the Caucasus and a series of dust storms, precipitation from which was bound to overload glaciers, including those in the Caucasus, needs to be investigated. The combination of factors contributes to unprecedented avalanches. How great the additional pressure on the glaciers might have been can be judged from the sediment remains in the Swiss mountains.

**Russian:**

Рис. 100. Наносы в горах Швейцарии.

**English:**

Figure 100. Nanoses in the Swiss mountains.

<!-- Local
[![](Step by step_files/100D188D0B2D0B5D0B9D186D0B0D180D0B8D18F.jpg#clickable)](Step by step_files/100%2B%25D1%2588%25D0%25B2%25D0%25B5%25D0%25B9%25D1%2586%25D0%25B0%25D1%2580%25D0%25B8%25D1%258F.jpg)
-->

[![](https://1.bp.blogspot.com/-vR1fCKUEbPI/X9ZT-vWIJwI/AAAAAAAADqY/TMmO8P7QIFE315hrwzD29AOWRQ7-dp7KwCLcBGAsYHQ/s650/100%2B%25D1%2588%25D0%25B2%25D0%25B5%25D0%25B9%25D1%2586%25D0%25B0%25D1%2580%25D0%25B8%25D1%258F.jpg#clickable)](https://1.bp.blogspot.com/-vR1fCKUEbPI/X9ZT-vWIJwI/AAAAAAAADqY/TMmO8P7QIFE315hrwzD29AOWRQ7-dp7KwCLcBGAsYHQ/s650/100%2B%25D1%2588%25D0%25B2%25D0%25B5%25D0%25B9%25D1%2586%25D0%25B0%25D1%2580%25D0%25B8%25D1%258F.jpg)

**Russian:**

**36. ОСОБЕННОСТИ КАТАСТРОФЫ**

**English:**

**36. PECULIARITIES OF THE CATASTROPHE**

**Russian:**

1. Главная особенность происшедшей катастрофы -- ее уникальность, причем уникально все: и разрушение планеты, и то, что разрушился именно близлежащий Фаэтон. Вероятность повторения подобного инцидента астрономически мала.

**English:**

1. The main peculiarity of the catastrophe is its uniqueness, and everything is unique: both the destruction of the planet and the fact that it was the nearby Phaeton that collapsed. The probability of such an incident happening again is astronomically low.

**Russian:**

2. Космическая катастрофа имела строго местный характер, коснувшийся только Солнечной системы.

**English:**

2. The cosmic catastrophe was of strictly local character, affecting only the solar system.

**Russian:**

3. Катастрофа двусоставная: метеорное нарушение стабильности на планете Земле и восстановление новой стабильности через перемещение масс в недрах, смещение оси, разломы, трансгрессии и регрессии океанов и частичное затопление коры.

**English:**

3. The catastrophe is twofold: meteoric disturbance of stability on planet Earth and restoration of new stability through displacement of masses in the interior, axis displacement, rifting, transgression and regression of the oceans and partial flooding of the crust.

**Russian:**

4. Каждая из реакций планеты носит или прямой оборонительный характер (разряд), или восстановительный, призванный достичь новой стабильности.

**English:**

4. Each of the planet's reactions is either direct defensive (discharge) or reductive, designed to achieve new stability.

**Russian:**

5. Катастрофа носила повторяющийся характер: метеорный поток периодически встречал Землю на орбите, периодически (но, возможно, не каждый раз) вызывая сходные последствия.

**English:**

5. The catastrophe was of a recurring nature: the meteor shower periodically encountered the Earth in orbit, periodically (but probably not every time) causing similar effects.

**Russian:**

6. Катастрофа носила угасающий характер: по мере естественного рассеивания метеорного потока в пространстве, его плотность падала, и тяжесть последствий от встречи с ним с течением времени слабела.

**English:**

6. The catastrophe had a fading character: as the natural dispersion of the meteoroid flow in space, its density decreased, and the severity of the consequences of the meeting with it over time weakened.

**Russian:**

7. Последствия катастрофы необратимы: климатические зоны сместились, флора и фауна плейстоцена сильно разрежена и частично погибла, рудопроявления и месторождения нефти сформировались, мутации прошли.

**English:**

7. The consequences of the catastrophe are irreversible: climate zones have shifted, Pleistocene flora and fauna are highly sparse and partially dead, ore occurrences and oil deposits have formed, and mutations have passed.

**Russian:**

8. Мутации можно счесть компенсационным эффектом: число видов резко сократилось, а число генетически различных подвидов внутри выживших видов резко увеличилось.

**English:**

8. Mutations can be thought of as a compensatory effect: the number of species declined sharply and the number of genetically distinct subspecies within the surviving species increased sharply.

**Russian:**

9. Часть катастрофических последствий, например, повышенное содержание влаги на ледниках и в подземных слоях, воспринималась людьми, как желательная норма, но таковой не являлась.

**English:**

9. Some of the catastrophic consequences, such as increased moisture content on glaciers and in subterranean layers, were perceived by people as a desirable norm, but they were not.

**Russian:**

10. Снижение запасов влаги и таяние мерзлоты -- завершающая часть стабилизации планеты в ее новом состоянии.

**English:**

10. Decreasing moisture reserves and thawing of permafrost is the final part of stabilization of the planet in its new state.

**Russian:**

**37.1. СТАТИСТИКА ПРИРОДНЫХ КАТАСТРОФ**

**English:**

**37.1. STATISTICS OF NATURAL DISASTERS**

**Russian:**

Использована база данных Excel-2007, скачивается свободно [^43]. База содержит 32205 свидетельств о примерно 34 тысячах природных катастроф. Наиболее любопытным выглядит процент совпадений катаклизмов разного типа в один год и в одном регионе. Выборка чрезвычайно жесткая: если в свидетельстве указан точный штат (область), например, Дофине, то сопоставляем только то, что произошло в этом году именно в Дофине. Если указана страна, то сопоставляем только со страной, а если регион, к примеру, Африка в целом, то только то, что произошло в этом году в Африке в целом, а не в Сомали и не в Северной или Западной Африке.

**English:**

Excel-2007 database used, freely downloadable [^43]. The database contains 32205 evidences of about 34 thousand natural disasters. The most curious is the percentage of coincidence of cataclysms of different types in one year and in one region. The sample is extremely rigid: if an exact state (region) is specified in the certificate, for example, Dauphiné, then we compare only what happened that year exactly in Dauphiné. If a country is indicated, only the country is matched, and if the region, for example Africa as a whole, only what happened that year in Africa as a whole, not in Somalia or in North or West Africa.

**Russian:**

79 типов катаклизмов дают 6241 пар возможных сочетаний. Из них 4538 пар (72,7%) имеют ноль совпадений, то есть, между катастрофами в этих парах статистических связей нет. Малыми связями можно счесть совпадение менее чем в 17 % случаев, и это очень важная информация. Так, 1324 случая Черной смерти только 3 раза (0,22%) совпали в одном месте и в одно время с 1029 случаями засух -- условием эпидемии чумы, и это означает, что Черная смерть не чума.

**English:**

79 types of cataclysms give 6241 pairs of possible combinations. Of them 4538 pairs (72.7%) have zero coincidences, i.e. there are no statistical relations between cataclysms in these pairs. Small links can be considered as coincidence in less than 17% of cases, and this is very important information. Thus, 1324 cases of the Black Death only 3 times (0.22%) coincided in the same place and at the same time with 1029 cases of drought -- a condition of plague epidemics, which means that the Black Death is not a plague.

**Russian:**

В целом можно сделать 6241 тот или иной вывод, поэтому рассмотрим только пары с 17% и более совпадений и только те, что имеют прямое отношение к данному исследованию. Для примера просмотрим очевидные связи: так и должно быть, что 371 случай неурожая в 141 случае (38%) совпадает с упоминанием голода.

**English:**

In general 6241 one or the other conclusion can be drawn, so let us consider only pairs with 17% or more coincidences and only those directly relevant to this study. For an example, let's look at the obvious connections: it should be the case that 371 cases of crop failure in 141 cases (38%) coincide with a reference to famine.

**Russian:**

Общ. Совп. % Катаклизм ----- Совпадает с

**English:**

Total Coincidence % Cataclysmic

**Russian:**

371 141 38 Неурожай ----- Голод

**English:**

371,141,38 Bad harvests {------ Famine

**Russian:**

371 67 18 Неурожай ----- Жара, засуха, бесснежная зима

**English:**

371 67 18 Bad harvests {-------- Hot, dry, snowless winters

**Russian:**

371 89 24 Неурожай ----- Морозы, похолодания

**English:**

371 89 24 Bad harvests {------- Frosts, cold snaps

**Russian:**

371 126 34 Неурожай ----- Осадки, град, снег

**English:**

371,126,34 Bad harvest

**Russian:**

371 67 18 Неурожай ----- Ураганы, бури, торнадо

**English:**

371 67 18 Bad harvests {------ hurricanes, storms, tornadoes

**Russian:**

1738 591 34 Наводнения ----- Осадки, град, снег

**English:**

1738 591 34 Floods {------- Rainfall, hail, snow

**Russian:**

1738 417 24 Наводнения ----- Ураганы, бури, торнадо

**English:**

1738 417 24 Floods {------ Hurricanes, storms, tornadoes

**Russian:**

457 260 57 Дым, пыль, пепел, тьма ----- Извержения

**English:**

457 260 57 Smoke, dust, ash, darkness {------- eruptions

**Russian:**

1029 175 17 Жара, засуха, бесснежная зима ----- Голод

**English:**

1029 175 17 Heat, drought, snowless winter

**Russian:**

1948 584 30 Осадки, град, снег ----- Наводнения

**English:**

1948 584 30 Precipitation, hail, snow

**Russian:**

1948 643 33 Осадки, град, снег ----- Ураганы, бури, торнадо

**English:**

1948 643 33 Precipitation, hail, snow ------- Hurricanes, storms, tornadoes

**Russian:**

2493 424 17 Ураганы, бури, торнадо ----- Наводнения

**English:**

2493 424 17 Hurricanes, storms, tornadoes

**Russian:**

2493 648 26 Ураганы, бури, торнадо ----- Осадки, град, снег

**English:**

2493 648 26 Hurricanes, storms, tornadoes {-------- Precipitation, hail, snow

**Russian:**

2132 490 23 Цунами ----- Землетрясения

**English:**

2132 490 23 Tsunami {----- Earthquakes

**Russian:**

2132 469 22 Цунами ----- Извержения

**English:**

2132 469 22 Tsunami {------ eruptions

**Russian:**

19 12 63 Шум ----- Землетрясения

**English:**

19 12 63 Noise ----- Earthquakes

**Russian:**

19 4 21 Шум ----- Цунами

**English:**

19 4 21 Noise ------- tsunami

**Russian:**

15 11 73 Змей в небе ----- Кометы

**English:**

15 11 73 Snakes in the sky -------- Comets

**Russian:**

А теперь смотрим те статистически значимые связи, что подтверждают правоту сказанного в тексте.

**English:**

And now let's look at those statistically significant relationships that confirm the correctness of what is said in the text.

**Russian:**

Общ. Совп. % Катаклизм ----- Совпадает с

**English:**

Total Coincidence % Cataclysmic

**Russian:**

880 167 19 Голод ----- Эпидемии (плюс эпизоотии)

**English:**

880 167 19 Famine ------ Epidemics (plus epizootics)

**Russian:**

185 37 20 Знамения ----- Эпидемии (плюс эпизоотии)

**English:**

185 37 20 Omen ------- Epidemics (plus epizootics)

**Russian:**

275 66 24 Психозы (в т.ч. тарантизм) ----- Эпидемии (плюс эпизоотии)

**English:**

275 66 24 Psychosis (incl. Tarantism) ------ epidemics (plus epizootics)

**Russian:**

38 8 21 Тарантизм ----- Эпидемии (плюс эпизоотии)

**English:**

38 8 21 Tarantism ------ Epidemics (plus epizootics)

**Russian:**

19 4 21 Огонь с неба ----- Химически активные осадки

**English:**

19 4 21 Fire from the sky

**Russian:**

19 5 26 Огонь с неба ----- Эпидемии (плюс эпизоотии)

**English:**

19 5 26 Fire from the sky ------ Epidemics (plus epizootics)

**Russian:**

162 41 25 Химически активные осадки ----- Осадки, град, снег

**English:**

162 41 25 Reactive precipitation ------- Precipitation, hail, snow

**Russian:**

162 53 33 Химически активные осадки ----- Эпидемии (плюс эпизоотии)

**English:**

162 53 33 Chemically active precipitation ------ epidemics (plus epizootics)

**Russian:**

162 34 21 Химически активные осадки ----- Кровавые дожди

**English:**

162 34 21 Reactive precipitation ------ Bloody rains

**Russian:**

97 29 30 Тектоника ----- Наводнения

**English:**

97 29 30 Tectonics-----/- Floods

**Russian:**

42 9 21 Кровавые дожди ----- Наводнения

**English:**

42 9 21 Bloody rains ------ Floods

**Russian:**

42 14 33 Кровавые дожди ----- Дым, пыль, пепел, тьма

**English:**

42 14 33 Bloody showers. Smoke, dust, ash, darkness.

**Russian:**

42 12 29 Кровавые дожди ----- Кометы

**English:**

42 12 29 Bloody rains ------ Comets

**Russian:**

42 24 57 Кровавые дожди ----- Осадки, град, снег

**English:**

42 24 57 Bloody rains ------- Rainfall, hail, snow

**Russian:**

42 12 29 Кровавые дожди ----- Ураганы, бури, торнадо

**English:**

42 12 29 Bloody rains ------- Hurricanes, storms, tornadoes

**Russian:**

42 34 81 Кровавые дожди ----- Химически активные осадки

**English:**

42 34 81 Bloody rains --------- Chemical rainfall

**Russian:**

8 3 38 Проказа ----- Психозы (в т.ч. тарантизм)

**English:**

8 3 38 Leprosy ------- Psychosis (Including Tarantism)

**Russian:**

8 4 50 Проказа ----- Химически активные осадки

**English:**


**Russian:**

65 13 20 Реки высохли, родники иссякли ----- Эпидемии (плюс эпизоотии)

**English:**

65 13 20 Rivers dry up, springs run dry... epidemics (plus epizootics)

**Russian:**

3 1 33 Землетрясения 4 месяца ----- Чума

**English:**

3 1 33 Earthquakes 4 months ------ plague

**Russian:**

40 9 23 Огненные торнадо ----- Землетрясения

**English:**

40 9 23 Fiery tornadoes {------ Earthquakes

**Russian:**

40 21 53 Огненные торнадо ----- Ураганы, бури, торнадо

**English:**

40 21 53 Fiery tornadoes {------- Hurricanes, storms, tornadoes

**Russian:**

3 1 33 Две пролетающих звезды ----- Дым, пыль, пепел, тьма

**English:**

3 1 33 Two shooting stars. Smoke, dust, ash, darkness.

**Russian:**

3 1 33 Две пролетающих звезды ----- Землетрясения

**English:**

3 1 33 Two shooting stars ----- Earthquakes

**Russian:**

На сегодня работа с численными характеристиками базы данных исторических свидетельств -- наиболее продуктивный способ устанавливать истину.

**English:**

Today, working with the numerical characteristics of the historical evidence database is the most productive way to establish the truth.

**Russian:**

**37.2. СТАТИСТИКА КЛЮЧЕВЫХ КАТАСТРОФ**

**English:**

**37.2. KEY DISASTER STATISTICS**

**Russian:**

Смотрим события, имеющие отношение к смещению оси вращения Земли: затопление земель, высыхание рек, многомесячные землетрясения, гипотетическое смещение оси вращения Земли и сбой сезонов года. Основные события происходят в периоде 1100-1900 гг. и хронологически одно с другим связаны.

**English:**

Let's look at the events related to the shift of the Earth's rotation axis: flooding of lands, drying up of rivers, multi-month earthquakes, hypothetical shift of the Earth's rotation axis and disruption of seasons. The main events occur in the period 1100-1900 and chronologically one is connected with another.

**Russian:**

Рис. 101. Хронологическая связь событий разного типа

**English:**

Figure 101. Chronological relation of events of different types

<!-- Local
[![](Step by step_files/78D0A1D0B2D18FD0B7D18C.png#clickable)](Step by step_files/78%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png)
-->

[![](https://1.bp.blogspot.com/-1vRtrETht5U/XlWaSrM_sSI/AAAAAAAADOA/Qi5ZknWaSPkIWwnyJ538gCmKZnL2rjCvgCLcBGAsYHQ/s1600/78%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png#clickable)](https://1.bp.blogspot.com/-1vRtrETht5U/XlWaSrM_sSI/AAAAAAAADOA/Qi5ZknWaSPkIWwnyJ538gCmKZnL2rjCvgCLcBGAsYHQ/s1600/78%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png)

**Russian:**

Теперь - то же самое, но подробнее, не по 100-летиям, а по 25-летиям, и чуть иначе представленное. Перед нами все тот же затяжной и определенно единый процесс, достигающий пика в период «эпидемии чумы» 1348 года, давший последние высокие цифры в начале XIX века (1812-1822) и завершающийся к 1880-м годам.

**English:**

Now - the same, but in more detail, not by 100-years, but by 25-years, and presented in a slightly different way. We are facing the same protracted and definitely unified process which peaked in the period of the "plague epidemic" in 1348, gave its last high numbers in the beginning of the 19th century (1812-1822) and finished by the 1880s.

**Russian:**

Рис. 102. Хронологическая связь событий разного типа

**English:**

Figure 102. Chronological relation of events of different types

<!-- Local
[![](Step by step_files/79D0A1D0B2D18FD0B7D18C.png#clickable)](Step by step_files/79%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png)
-->

[![](https://1.bp.blogspot.com/-77tTgyzggCI/XlWaaRWcK0I/AAAAAAAADOE/cLoGrr1Yqc0zPCRPO8VTF2YWgHhogskZQCLcBGAsYHQ/s1600/79%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png#clickable)](https://1.bp.blogspot.com/-77tTgyzggCI/XlWaaRWcK0I/AAAAAAAADOE/cLoGrr1Yqc0zPCRPO8VTF2YWgHhogskZQCLcBGAsYHQ/s1600/79%2B%25D0%25A1%25D0%25B2%25D1%258F%25D0%25B7%25D1%258C.png)

**Russian:**

Наиболее простое толкование: перед нами тот период смещения оси вращения Земли, что заложил линеаменты (разломы), направленные почти точно на нынешний географический полюс. Если так, то предыдущая серия линеаментов с направлением на Гренландию была заложена в 1-м тысячелетии, когда беженцы покидали Причерноморье и Доггерленд.

**English:**

The simplest interpretation: we are facing that period of the Earth rotation axis shift that laid down the lineaments (faults) pointing almost exactly to the current geographical pole. If so, the previous series of lineaments with the direction to Greenland was laid in the 1st millennium, when refugees left the Pontic and Doggerland.

**Russian:**

**37.3. ДВА МАРШРУТА ВЕЛИКОГО ПЕРЕСЕЛЕНИЯ НАРОДОВ**

**English:**

**37.3. THE TWO ROUTES OF THE GREAT MIGRATION OF PEOPLES**

(полный текст по ссылке [^133])

**Russian:**

Великое переселение народов -- косвенное, и при этом самое объективное свидетельство о масштабе происходившей катастрофы.

**English:**

The Great Migration of Peoples is an indirect, but objective testimony to the extent of this catastrophe.

**Russian:**

С севера шли гепиды, готы, франки, алеманы, бургунды, свевы, англы, саксы, фризы, юты, аланы, лангобарды, викинги, норманны (14 народов).

**English:**

From the north came the Goths, Goths, Franks, Alemians, Burgundians, Sveves, Angles, Saxons, Frisians, Jutes, Alans, Lombards, Vikings, Normans (14 nations).

**Russian:**

С востока шли герулы, готы, остготы, вестготы, варвары, вандалы, булгары, гунны, аланы, венгры, авары (11 народов).

**English:**

From the east there were Geruli, Goths, Ostgoths, Visigoths, Barbarians, Vandals, Bulgars, Huns, Alans, Hungarians, Avars (11 peoples).

**Russian:**

Переселение идет синхронно. Шестнадцать переселявшихся племен - алеманы, авары, булгары, вандалы, венгры, вестготы, гепиды, герулы, готы, гунны, куманы, лангобарды, остготы, печенеги, славяне, франки -- так или иначе посетили транспортный узел между Рейном и Дунаем, - земли современных Австрии, Чехии, Словакии, Венгрии и Румынии (на карте помечено желтым).

**English:**

The resettlement is synchronous. Sixteen migrating tribes - Alemans, Avars, Bulgars, Vandals, Hungarians, Visigoths, Hepids, Heruls, Goths, Huns, Cumans, Langobards, Ostgoths, Pechenegs, Slavs, Franks - somehow visited the transport hub between the Rhine and Danube - the lands of modern Austria, Czechia, Slovakia, Hungary and Romania (marked yellow on the map).

**Russian:**

Рис. 103. Великое переселение народов

**English:**

Figure 103. The Great Migration of Peoples

<!-- Local
[![](Step by step_files/80D092D09FD09D.jpg#clickable)](Step by step_files/80%2B%25D0%2592%25D0%259F%25D0%259D.jpg)
-->

[![](https://1.bp.blogspot.com/-TEqaz4M31IA/XlWaiGJ7LOI/AAAAAAAADOI/xNgqxVqMejQGfFZtB356ONZOXTd0av-dgCLcBGAsYHQ/s1600/80%2B%25D0%2592%25D0%259F%25D0%259D.jpg#clickable)](https://1.bp.blogspot.com/-TEqaz4M31IA/XlWaiGJ7LOI/AAAAAAAADOI/xNgqxVqMejQGfFZtB356ONZOXTd0av-dgCLcBGAsYHQ/s1600/80%2B%25D0%2592%25D0%259F%25D0%259D.jpg)

**Russian:**

Для большинства народов исток экспансии не указан, есть только промежуточные стоянки уже на входе в Европу: в устье Дуная и в низовьях северных рек. Это означает, что люди переселялись с тонущего Доггерленда и из нескольких десятков тонущих городов (они и сейчас на шельфе стоят) Северного Причерноморья, - больше неоткуда.

**English:**

For the majority of the peoples, the source of the expansion is not specified, there are only intermediate stops already at the entrance to Europe: at the mouth of the Danube and in the lower reaches of the northern rivers. This means that people moved from the sinking Doggerland and from several dozens of sinking towns (they are still standing offshore) of the Northern Black Sea coast, - there is nowhere else.

**Russian:**

Рис. 104. Земля до затопления (одна из реконструкций)

**English:**

Figure 104. Land before flooding (one of the reconstructions)

<!-- Local
[![](Step by step_files/81D0B4D0BED0BFD0BED182D0BED0BFD0B0.jpg#clickable)](Step by step_files/81%2B%25D0%25B4%25D0%25BE%25D0%25BF%25D0%25BE%25D1%2582%25D0%25BE%25D0%25BF%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-KX-cPvDfltM/XlWapAiqtxI/AAAAAAAADOQ/wDg9cuQsu_ooNJWj0PeQdfck1HcxPOP5gCLcBGAsYHQ/s1600/81%2B%25D0%25B4%25D0%25BE%25D0%25BF%25D0%25BE%25D1%2582%25D0%25BE%25D0%25BF%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-KX-cPvDfltM/XlWapAiqtxI/AAAAAAAADOQ/wDg9cuQsu_ooNJWj0PeQdfck1HcxPOP5gCLcBGAsYHQ/s1600/81%2B%25D0%25B4%25D0%25BE%25D0%25BF%25D0%25BE%25D1%2582%25D0%25BE%25D0%25BF%25D0%25B0.jpg)

**Russian:**

Смотрим на статистику.

**English:**

Looking at the stats.

**Russian:**

**37.4. НЕСОВПАДЕНИЕ ПЕРЕСЕЛЕНИЯ И ЗАТОПЛЕНИЙ**

**English:**

**37.4. MISMATCH OF RESETTLEMENT AND FLOODING**

**Russian:**

Чтобы не увязнуть в мелочах, суммируем события по столетиям; 1501 означает период с 1501 по 1600 год.

**English:**

To avoid getting bogged down in trivia, let's summarize the events by centuries; 1501 means the period from 1501 to 1600.

**Russian:**

Рис. 105. Великое переселение народов и затопление земель

**English:**

Figure 105. The Great Migration of Peoples and the Inundation of Land

<!-- Local
[![](Step by step_files/82D092D09FD09D.png#clickable)](Step by step_files/82%2B%25D0%2592%25D0%259F%25D0%259D.png)
-->

[![](https://1.bp.blogspot.com/-PpvjOJ6PPZs/XlWaxNYUpjI/AAAAAAAADOY/Su2i3bK29cwxsvVJM-Ye9rU65FFmvi4fQCLcBGAsYHQ/s1600/82%2B%25D0%2592%25D0%259F%25D0%259D.png#clickable)](https://1.bp.blogspot.com/-PpvjOJ6PPZs/XlWaxNYUpjI/AAAAAAAADOY/Su2i3bK29cwxsvVJM-Ye9rU65FFmvi4fQCLcBGAsYHQ/s1600/82%2B%25D0%2592%25D0%259F%25D0%259D.png)

**Russian:**

Великое переселение народов завершается до начала эпохи затопления частей Западной Европы. Причина проста: целиком затонувшие ранее XII века Доггерленд и Причерноморье перестали быть субъектами истории, а потому и точные сведениях об их затоплении в историю не попали.

**English:**

The Great Migration of Peoples ends before the beginning of the epoch of flooding of parts of Western Europe. The reason is simple: completely flooded before XII century Doggerland and Black Sea Coast have ceased to be subjects of a history, and consequently also the exact data on their flooding have not got in a history.

**Russian:**

**37.5. ПРОТИВОРЕЧИВЫЙ ВЫВОД**

**English:**

**37.5. CONTRADICTORY FINDING**

**Russian:**

Если так, то гибель мамонтов началась, где и должно, - 14-15 тысяч лет назад, а период 8200 лет назад, когда возникли лианементы с направлением на Гренландию, и произошло затопление Доггерленда, совпадает с 1-м тысячелетием, когда люди с Доггерленда побежали.

**English:**

If so, the death of mammoths began where it should - 14-15 thousand years ago, and the period 8200 years ago, when there were lianements with the direction of Greenland, and occurred flooding Doggerland, coincides with the 1st millennium, when people from Doggerland ran away.

**Russian:**

Противоречие разрешимо, просто не в рамках данного исследования.

**English:**

The contradiction is solvable, just not within the scope of this study.

**Russian:**

**38. ЭТАПЫ МЕТЕОРНОЙ КАТАСТРОФЫ - ОСНОВНОЕ**

**English:**

**38. STAGES OF THE METEOR DISASTER - BASIC**

**Russian:**

Список этапов

**English:**

List of stages

**Russian:**

1. Метеорный поток Ольберса

**English:**

1. Olbers meteoroid flow

**Russian:**

2. Воздействие потока на Землю

**English:**

2. Flux impact on the Earth

**Russian:**

3. Электрические процессы в недрах

**English:**

3. electrical processes in the subsoil

**Russian:**

4. Физические изменения в геоиде

**English:**

4. Physical changes in the geoid

**Russian:**

5. Тектонические процессы

**English:**

5. Tectonic processes

**Russian:**

6. Климатические изменения

**English:**

6. Climate change

**Russian:**

**МЕТЕОРНЫЙ ПОТОК ОЛЬБЕРСА**

**English:**

** METEORIC ALBERS FLOW**

**Russian:**

Разрушение планеты Ольберса (Фаэтон) создало в Солнечной системе метеорный поток, несколько раз прошедший рядом с планетами Марс, Венера и Земля, и ныне рассеявшийся. Три ясных доказательства:

**English:**

The collapse of the planet Olbers (Phaeton) created a meteor shower in the solar system that passed near the planets Mars, Venus and Earth several times and has now dissipated. Three clear proofs:

**Russian:**

- одна недостающая планета Солнечной системы;

**English:**

- one missing planet in the solar system;

**Russian:**

- метеориты из палласита (оливинового слоя мантии планет);

**English:**

meteorites from pallasite (the olivine layer of the planets mantle);

**Russian:**

- золото астероидного происхождения (из планетного ядра) на поверхности Земли.

**English:**

- gold of asteroidal origin (from the planetary core) on the Earth's surface.

**Russian:**

**ВОЗДЕЙСТВИЕ ПОТОКА НА ЗЕМЛЮ**

**English:**

**FLOW IMPACT ON THE GROUND**

**Russian:**

Проход метеорного потока близко к Земле вызвал три основных последствия:

**English:**

The passage of a meteor shower close to Earth caused three main effects:

**Russian:**

- срыв, выброс в атмосферу и скоростное оседание арктического грунта и воды, чем и сформирован ландшафт зоны оледенения;

**English:**

- disruption, ejection into the atmosphere and rapid subsidence of Arctic soil and water, which formed the landscape of the glaciation zone;

**Russian:**

- оседание продуктов разрушения метеоритов (ливийское стекло, песок, плазма) по траектории Иран-Сахара-Мексика;

**English:**

- Deposition of meteorite destruction products (Libyan glass, sand, plasma) along the Iran-Sahara-Mexico trajectory;

**Russian:**

- серию электрических разрядов между положительно заряженными метеорными телами и планетой.

**English:**

- a series of electrical discharges between positively charged meteoric bodies and a planet.

**Russian:**

**ЭЛЕКТРИЧЕСКИЕ ПРОЦЕССЫ В НЕДРАХ**

**English:**

**ELECTRICAL PROCESSES IN THE SUBSURFACE**

**Russian:**

Электрический разряд ведет к утрате недрами части электронов, что вызывает три основных последствия:

**English:**

Electrical discharge leads to the loss of some of the electrons in the subsurface, which causes three main effects:

**Russian:**

- прямой отбор тепла, что привело к быстрому образованию, как минимум, одного из двух слоев вечной мерзлоты;

**English:**

- direct heat extraction, which led to the rapid formation of at least one of the two permafrost layers;

**Russian:**

- запуск эндотермических геохимических реакций, так же ответственных за промерзание грунта в Северном полушарии;

**English:**

- triggering of endothermic geochemical reactions, also responsible for ground freezing in the Northern Hemisphere;

**Russian:**

- изменение структуры, плотности и поведения части мантии.

**English:**

- changes in the structure, density, and behavior of a portion of the mantle.

**Russian:**

**ФИЗИЧЕСКИЕ ИЗМЕНЕНИЯ В ГЕОИДЕ**

**English:**

**PHYSICAL CHANGES IN THE GEOID**

**Russian:**

Изменения в плотности и поведении мантии приводит к прямым физическим изменениям в геоиде:

**English:**

Changes in mantle density and behavior lead to direct physical changes in the geoid:

**Russian:**

- смещению центра тяжести планеты в силу ротации вещества в слоях мантии;

**English:**

- displacement of the planet's center of gravity due to the rotation of matter in the mantle layers;

**Russian:**

- периодическому появлению третьего, промежуточного момента инерции планеты, что привело к четырем переворотам Земли по Джанибекову;

**English:**

- periodic appearance of the third, intermediate moment of inertia of the planet, which led to four coups d'état by Janibekov;

**Russian:**

- вызванному смещением центра тяжести смещению оси вращения Земли на 17 градусов;

**English:**

-induced shift of the center of gravity by 17 degrees of the Earth's axis of rotation;

**Russian:**

- вызванным четырьмя переворотами Земли по Джанибекову четырем актам инверсии магнитного поля.

**English:**

-caused by four Janibekov flips of the Earth four acts of magnetic field inversion.

**Russian:**

**ТЕКТОНИЧЕСКИЕ ПРОЦЕССЫ**

**English:**

**TECTONIC PROCESSES**

**Russian:**

В результате изменения положения оси вращения Земли форма земной коры перестает отвечать центробежным силам, что вызывает ряд последствий:

**English:**

As a result of changes in the position of the Earth's axis of rotation, the shape of the Earth's crust ceases to respond to centrifugal forces, which causes a number of consequences:

**Russian:**

- возникают зоны сжатия и зоны растяжения, причем, напряжения достигают критических значений, равных пределу прочности пород тектоносферы, что и формирует две основные системы разломов -- с направлением на центр Гренландии и на нынешний северный полюс;

**English:**

- compression and tension zones arise, with stresses reaching critical values equal to the tectonospheric tensile strength, which forms two main fault systems -- toward the center of Greenland and toward the present-day North Pole;

**Russian:**

- изменение направления центробежных сил, относительно формы земной коры приводит к изменению относительных высот суши и дна океанов и уровня морей относительно побережий;

**English:**

- a change in the direction of centrifugal forces, relative to the shape of the Earth's crust leads to a change in the relative heights of land and the ocean floor and sea level relative to the coasts;

**Russian:**

- центробежные силы устанавливают новую форму земной коры -- по геологическим стандартам очень быстро, часто с кардинальным изменением ландшафта.

**English:**

- Centrifugal forces are establishing a new shape of the Earth's crust -- by geologic standards very rapidly, often with a dramatic change in the landscape.

**Russian:**

**КЛИМАТИЧЕСКИЕ ПОСЛЕДСТВИЯ**

**English:**

**CLIMATIC EFFECTS**

**Russian:**

Четыре переворота Земли по Джанибекову каждый раз меняли направление морских и атмосферных течений -- часто на обратное, с поправкой на очередное смещение оси вращения Земли.

**English:**

Janibekov's four upheavals of the Earth each time changed the direction of sea and atmospheric currents -- often to the reverse, corrected for the next shift in the Earth's axis of rotation.

**Russian:**

Вследствие общего смещения оси вращения Земли на 17 градусов, сместились и климатические зоны, что уверенно подтверждается свидетельствами 15-16 веков и сохранившимися очертаниями прежних почвенных зон.

**English:**

Owing to the general shift of an axis of rotation of the Earth on 17 degrees, climatic zones that is confidently confirmed by certificates of 15-16 centuries and the kept outlines of former ground zones have shifted also.

**Russian:**

**[ПРИЛОЖЕНИЯ]**

**English:**

### <name='appendices'></a>APPENDICES

**Russian:**

**39.1. ПРИЧИНА МАССОВЫХ ВЫМИРАНИЙ**

**English:**

**39.1. CAUSE OF MASS EXTINCTIONS**

**Russian:**

Земля пережила 25 массовых вымираний. Википедия [^118] приводит целый список возможных причин, и ни одна из них не признается наукой достаточной.

**English:**

Earth has experienced 25 mass extinctions. Wikipedia [^118] gives a whole list of possible causes, and none of them is recognized by science as sufficient.

**Russian:**

Понижение уровня мирового океана

**English:**

Decrease in global sea level

**Russian:**

Выбросы углекислого газа

**English:**

Carbon dioxide emissions

**Russian:**

Глобальное похолодание либо потепление

**English:**

Global cooling or warming

**Russian:**

Дефицит кислорода

**English:**

Oxygen deficiency

**Russian:**

Отравление сероводородом

**English:**

Hydrogen sulfide poisoning

**Russian:**

Отравление токсичными металлами

**English:**

Toxic metal poisoning

**Russian:**

Воздействие солнечной радиации из-за нарушения озонового слоя

**English:**

Exposure to solar radiation due to ozone depletion

**Russian:**

Запыление атмосферы

**English:**

Atmospheric dusting

**Russian:**

Столкновение с небесными телами

**English:**

Collision with celestial bodies

**Russian:**

Излияния базальта

**English:**

Basalt outpourings

**Russian:**

Космические лучи

**English:**

Cosmic rays

**Russian:**

Вспышки сверхновых на близком расстоянии

**English:**

Supernova flashes at close range

**Russian:**

Взаимодействие с облаками межзвездной пыли

**English:**

Interaction with interstellar dust clouds

**Russian:**

Достаточным было бы совместное воздействие сразу нескольких факторов, но даже вероятность каждого из них по отдельности исчезающее мала, а уж для того, чтобы собрать в одно место и время хотя бы 3-4 фактора, нужна просто фантастическая неудачливость -- каждый из 25 раз.

**English:**

Joint influence of several factors at once would suffice, but even probability of each of them separately is vanishingly small, and to collect in one place and time at least 3-4 factors, it is required simply fantastic unluckiness -- each of 25 times.

**Russian:**

Вывод на график показывает хронологическую связь серии массовых вымираний и серии трансформаций древних суперконтинентов. Эти два процесса начинаются, развиваются и завершаются одновременно.

**English:**

The derivation of the graph shows the chronological relationship between the series of mass extinctions and the series of transformations of ancient supercontinents. These two processes begin, evolve, and end simultaneously.

**Russian:**

Рис. 106. Связь массовых вымираний и трансформаций суперконтинентов [119; 120; 121; 122]

**English:**

Figure 106. Relationship of mass extinctions and supercontinent transformations [119; 120; 121; 122]

<!-- Local
[![](Step by step_files/78D09FD0B0D0BBD0B5D0BE.png#clickable)](Step by step_files/78%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)
-->

[![](https://1.bp.blogspot.com/-l2fdeUaOBOo/XlFJFUdSbeI/AAAAAAAADLg/pWOcfX-k7ukEXQ0PeRZwOBqA4dWT93bpgCLcBGAsYHQ/s1600/78%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png#clickable)](https://1.bp.blogspot.com/-l2fdeUaOBOo/XlFJFUdSbeI/AAAAAAAADLg/pWOcfX-k7ukEXQ0PeRZwOBqA4dWT93bpgCLcBGAsYHQ/s1600/78%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)

**Russian:**

Если посмотреть, какие виды и когда вымирали, то логика обнаруживается, - при условии допущения гипотезы расширяющейся Земли. Падение угловой скорости снижает и центробежные силы, компенсирующие силы притяжения; результат -- сразу же выбывают из строя морские существа без скелетов, и так на каждом шаге -- вплоть до динозавров, у которых заметно возрастают энергетические затраты на добычу еды. Наступает момент, когда соотношение затрат на охоту и получаемых килокалорий для этой формы жизни становится энергетически невыгодным. Процессы масштабного вымирания запускались не катастрофическими факторами, а эволюционными.

**English:**

If we look at what species have died out and when, the logic emerges -- assuming the expanding Earth hypothesis. Falling angular velocity also reduces centrifugal forces compensating for gravitational forces; the result is that sea creatures without skeletons immediately go extinct, and so at each step up to dinosaurs, whose energy expenditure for food increases markedly. There comes the moment, when the ratio of expenses for hunting and received kilocalories for this form of life becomes energetically unprofitable. The processes of large-scale extinction were triggered not by catastrophic factors, but by evolutionary ones.

**Russian:**

Второй плюс допущения: при еще не расширившейся толком Земле понятны постоянные схождения и расхождения материковых плит -- в самых разных сочетаниях. Материкам не надо гоняться друг за другом за 15-17 тысяч километров, они на первых этапах совсем рядом, а их миграция легко объяснима переменами в направлении центробежных сил.

**English:**

The second advantage of the assumption: the constant convergence and divergence of continental plates in various combinations is understandable for the Earth that has not yet expanded properly. The continents do not have to chase each other for 15-17 thousand kilometers, they are very close to each other at the first stages, and their migration can be easily explained by changes in the direction of centrifugal forces.

**Russian:**

**39.2. ЭПОХА ТЕМПЕРАТУРНЫХ КОЛЕБАНИЙ**

**English:**

**39.2. THE ERA OF TEMPERATURE FLUCTUATIONS**

**Russian:**

В эпоху вымирания приводятся два периода похолоданий, однако с вымиранием форм жизни их не связывают. Системно температурные колебания начинаются сразу по завершению процесса расширения Земли, что подразумевает геохимическую природу таковых колебаний, - планета вошла в новый этап развития недр. Часть геохимических процессов, выбрасывающих из недр воду или формирующих горы, эндотермическая, а часть -- экзотермическая, и оба типа процессов явно находятся в динамическом равновесии.

**English:**

There are two periods of cooling in the epoch of extinction, but they are not associated with the extinction of life forms. The systemic temperature fluctuations begin immediately after the completion of the expansion of the Earth, which implies the geochemical nature of such fluctuations - the planet has entered a new stage in the development of the bowels. Part of the geochemical processes ejecting water from the subsurface or forming mountains is endothermic and part is exothermic, and both types of processes are clearly in dynamic equilibrium.

**Russian:**

Рис. 107. Завершение эпохи вымираний и начало температурных колебаний [^119]

**English:**

Figure 107. End of the extinction era and the beginning of temperature fluctuations [^119]

<!-- Local
[![](Step by step_files/79D09FD0B0D0BBD0B5D0BE.png#clickable)](Step by step_files/79%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)
-->

[![](https://1.bp.blogspot.com/-4aURC2vnJic/XlFJMU6OCMI/AAAAAAAADLk/yCiYEcaj--8JxCKxmwUiFS2IePke7zJOwCLcBGAsYHQ/s1600/79%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png#clickable)](https://1.bp.blogspot.com/-4aURC2vnJic/XlFJMU6OCMI/AAAAAAAADLk/yCiYEcaj--8JxCKxmwUiFS2IePke7zJOwCLcBGAsYHQ/s1600/79%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)

**Russian:**

**39.3. ФОРМИРОВАНИЕ МОРЕЙ**

**English:**

**39.3. FORMATION OF SEAS**

**Russian:**

Порядка 14 млн. лет назад начинается формирование морей. Чаще всего очередные этапы совпадают с похолоданием, реже -- с потеплением, однако важнее всего то, что ритмики потеплений и похолоданий формирование морей не нарушает.

**English:**

About 14 million years ago the formation of the seas began. Most often the successive stages coincide with cooling and less often with warming, but the most important thing is that the rhythm of warming and cooling does not break the formation of the seas.

**Russian:**

В выборку попали Акчегыльское море, Апшеронское море, Бакинская трансгрессия, Киммерийское море, Мессинский кризис, Мэотическое море, Паннонский водоем, Позднеехазарское море, Понтическое море, Раннехазарское море, Сарматское море, Эвксинский водоем.

**English:**

The Akchegyl Sea, Apsheron Sea, Baku Transgression, Cimmerian Sea, Messinian Crisis, Meotian Sea, Pannonian Sea, Late Khazar Sea, Pontic Sea, Early Khazar Sea, Sarmatian Sea, Euxinian reservoir were included in the sample.

**Russian:**

Рис. 108. Формирование морей

**English:**

Figure 108. Formation of seas

<!-- Local
[![](Step by step_files/80D09FD0B0D0BBD0B5D0BE.png#clickable)](Step by step_files/80%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)
-->

[![](https://1.bp.blogspot.com/-Z1_Ui9OCrx8/XlFJRrJ492I/AAAAAAAADLo/F0tBLDwJ05cVLzJkthsGHH35dLjoY6_uwCLcBGAsYHQ/s1600/80%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png#clickable)](https://1.bp.blogspot.com/-Z1_Ui9OCrx8/XlFJRrJ492I/AAAAAAAADLo/F0tBLDwJ05cVLzJkthsGHH35dLjoY6_uwCLcBGAsYHQ/s1600/80%2B%25D0%259F%25D0%25B0%25D0%25BB%25D0%25B5%25D0%25BE.png)

**Russian:**

Можно думать, что формирование морей -- поверхностные процессы, не влияющие сколько-нибудь заметно на геохимические процессы в недрах. 120 тысяч лет назад, в период позднехазарской трансгрессии процесс формирования бассейнов большей частью завершен, и береговая линия Каспия выглядит узнаваемо.

**English:**

One can think that the formation of seas is a surface process that does not affect the geochemical processes in the interior in any noticeable way. 120 thousand years ago, during the Late Khazar transgression, the process of basin formation was mostly completed, and the Caspian coastline looks recognizable.

**Russian:**

Рис. 109. Очертания Хазарского моря

**English:**

Fig. 109. Outlines of the Khazar Sea

<!-- Local
[![](Step by step_files/81D0A5D0B0D0B7D0B0D180D181D0BAD0BED0B5.jpg#clickable)](Step by step_files/81%2B%25D0%25A5%25D0%25B0%25D0%25B7%25D0%25B0%25D1%2580%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5.jpg)
-->

[![](https://1.bp.blogspot.com/-fRaUYeXBXMY/XlFJWWSn0oI/AAAAAAAADLs/IHdYrswhWAAdmiAyiD60gVFOxguOpZZjACLcBGAsYHQ/s1600/81%2B%25D0%25A5%25D0%25B0%25D0%25B7%25D0%25B0%25D1%2580%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5.jpg#clickable)](https://1.bp.blogspot.com/-fRaUYeXBXMY/XlFJWWSn0oI/AAAAAAAADLs/IHdYrswhWAAdmiAyiD60gVFOxguOpZZjACLcBGAsYHQ/s1600/81%2B%25D0%25A5%25D0%25B0%25D0%25B7%25D0%25B0%25D1%2580%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B5.jpg)

**Russian:**

**39.4. ВЫМИРАНИЕ ПЛЕЙСТОЦЕНОВОЙ МЕГАФАУНЫ**

**English:**

**39.4. EXTINCTION OF PLEISTOCENE MEGAFAUNA**

**Russian:**

В период вымирания плейстоценовой фауны динамика температурных колебаний на планете не меняется. То есть, даже смещение оси вращения Земли на температурные колебания не повлияло. Запас прочности планеты оказался достаточен, чтобы пережить инцидент.

**English:**

During the Pleistocene fauna extinction period, the dynamics of temperature fluctuations on the planet did not change. That is, even the shift in the axis of rotation of the Earth did not affect the temperature fluctuations. The safety margin of the planet turned out to be sufficient to survive the incident.

**Russian:**

Второе важное обстоятельство: вымирание прошло на конкретном, очень коротком отрезке времени, и фактор, запустивший процесс вымирания, определенно не связан с периодическим потеплением и похолоданием.

**English:**

The second important fact is that the extinction took place on a specific, very short stretch of time, and the factor that triggered the extinction process is definitely not related to periodic warming and cooling.

**Russian:**

Рис. 110. Период вымирания плейстоценовой фауны

**English:**

Figure 110. Extinction period of Pleistocene fauna

<!-- Local
[![](Step by step_files/82D09CD0B5D0B3D0B0D184D0B0D183D0BDD0B0.png#clickable)](Step by step_files/82%2B%25D0%259C%25D0%25B5%25D0%25B3%25D0%25B0%25D1%2584%25D0%25B0%25D1%2583%25D0%25BD%25D0%25B0.png)
-->

[![](https://1.bp.blogspot.com/-VMVrljTwHp4/XlFJcmebGvI/AAAAAAAADL0/pc4i7SfxAbIRqFkzDA6vDV2AFBLBAi1EgCLcBGAsYHQ/s1600/82%2B%25D0%259C%25D0%25B5%25D0%25B3%25D0%25B0%25D1%2584%25D0%25B0%25D1%2583%25D0%25BD%25D0%25B0.png#clickable)](https://1.bp.blogspot.com/-VMVrljTwHp4/XlFJcmebGvI/AAAAAAAADL0/pc4i7SfxAbIRqFkzDA6vDV2AFBLBAi1EgCLcBGAsYHQ/s1600/82%2B%25D0%259C%25D0%25B5%25D0%25B3%25D0%25B0%25D1%2584%25D0%25B0%25D1%2583%25D0%25BD%25D0%25B0.png)

**Russian:**

**39.5. ВОЗМОЖНОСТЬ ОПРОВЕРЖЕНИЯ**

**English:**

**39.5. REBUTTABILITY**

**Russian:**

Шанс опровергнуть часть этих тезисов есть, но для этого потребуется принять результаты фундаментальных экспериментов по стратификации, проведенных в Engineering Research Center, Colorado State University и почетным членом ассоциации седиментологов Ги Берто [123; 124]. Из экспериментов следует, что слои, расположенные ниже, могут быть моложе верхних слоев, а целый ряд слоев мог формироваться в рамках одного инцидента. Это означает, что кропотливо датированные слои, отражающие этапы потепления и похолодания, могли сформироваться по геологическим меркам мгновенно, а значит, и тезисы могут быть пересмотрены.

**English:**

There is a chance to refute some of these theses, but it would require accepting the results of fundamental stratification experiments conducted at Engineering Research Center, Colorado State University and Guy Berto, honorary member of the Association of Sedimentologists [123; 124]. From the experiments, it appears that the layers below may be younger than the upper layers, and a number of layers may have formed within a single incident. This means that painstakingly dated layers reflecting warming and cooling phases could have formed instantaneously by geological standards, which means that theses may be revised.

**Russian:**

**ПРИМЕЧАНИЕ**

**English:**

** NOTE**

**Russian:**

Интересно экспоненциальное сокращение интервалов между соседними эволюционными событиями (их 105 всего) - от 59 млн. лет до 1 тысячи лет. В среднем, каждый новый интервал сокращается в 1,11 раз.

**English:**

The exponential decrease in the intervals between neighboring evolutionary events (there are 105 in total), from 59 million years to 1 thousand years, is interesting. On average, each new interval is reduced by a factor of 1.11.

**Russian:**

**40. ДИНОЗАВРЫ НЕ ИЗ ГОЛЛИВУДА**

**English:**

**40. DINOSAURS AREN'T FROM HOLLYWOOD**

**Russian:**

Из динозавров сделали то, чем они не являлись, - успешных и опасных охотников, абсолютно доминирующих не только в море, но и на суше.

**English:**

Dinosaurs were made into what they were not - successful and dangerous hunters, absolutely dominant not only in the sea, but also on land.

**Russian:**

**40.1. ВОДНЫЙ ОБРАЗ ЖИЗНИ ДИНОЗАВРОВ**

**English:**

**40.1. THE AQUATIC LIFESTYLE OF THE DINOSAURS**

**Russian:**

Брайан Форд, британский биолог выдвинул теорию, что динозавры жили в воде. Аргументы понятны: большая масса, длинная шея, у ряда видов хождение на задних ногах. Плюс, травянистые хвощи и папоротники той высоты, о которой нам говорят, могли существовать, лишь если росли в воде. Логично и другое использование длинной шеи: чтобы собирать корм со дна (как современные гуси и лебеди) [^130].

**English:**

Brian Ford, a British biologist put forward the theory that dinosaurs lived in water. The arguments are clear: large mass, long necks, a number of species walking on their hind legs. Plus, herbaceous horsetails and ferns of the height we are told could only have existed if they grew in water. Another use of the long neck is also logical: to collect food from the bottom (like modern geese and swans) [^130].

**Russian:**

Рис. 111. Водный образ жизни динозавров

**English:**

Figure 111. Aquatic lifestyle of dinosaurs

<!-- Local
[![](Step by step_files/06D092D0BED0B4D0B0.jpg#clickable)](Step by step_files/06%2B%25D0%2592%25D0%25BE%25D0%25B4%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-pSYB2_3V5c4/XlUdtPZRdxI/AAAAAAAADM4/0Px_qDAFQaw-SX-Ic_srBiKoLYyctItagCLcBGAsYHQ/s1600/06%2B%25D0%2592%25D0%25BE%25D0%25B4%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-pSYB2_3V5c4/XlUdtPZRdxI/AAAAAAAADM4/0Px_qDAFQaw-SX-Ic_srBiKoLYyctItagCLcBGAsYHQ/s1600/06%2B%25D0%2592%25D0%25BE%25D0%25B4%25D0%25B0.jpg)

**Russian:**

Палеонтологи знают о водно-болотном характере прежнего ландшафта, и если прибрежные зоны поднялись или опустились на 50-200 метров, это означает гибель кормовой базы, а вместе с ней и динозавров. Отбери у крокодилов реку - долго ли протянут? А тут еще масса как у кита и лапы - ни в море уйти, ни на суше остаться.

**English:**

Paleontologists know about the wetland character of the former landscape, and if the riparian zones have risen or fallen by 50-200 meters, this means the death of the forage base, and with it the dinosaurs. If you take away the crocodiles' river, how long would they last? And there's a mass like a whale's and paws - neither to go to the sea, nor to stay on land.

**Russian:**

**40.2. КАРТА КЛАДБИЩ ДИНОЗАВРОВ**

**English:**

**40.2. DINOSAUR GRAVEYARD MAP**

**Russian:**

Места обитания динозавров тяготеют к морским побережьям, что прямо указывает на образ жизни.

**English:**

Dinosaur habitats gravitate towards seashores, a direct indication of lifestyle.

**Russian:**

Рис. 112. Основные кладбища динозавров

**English:**

Figure 112. Major dinosaur cemeteries

<!-- Local
[![](Step by step_files/03D0BAD0B0D180D182D0B0.jpg#clickable)](Step by step_files/03%2B%25D0%25BA%25D0%25B0%25D1%2580%25D1%2582%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-HLfzOtOGMHM/XlUd0Pma-_I/AAAAAAAADM8/bWWc236yewcimDT76q1T_tb0htUZrRoQwCLcBGAsYHQ/s1600/03%2B%25D0%25BA%25D0%25B0%25D1%2580%25D1%2582%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-HLfzOtOGMHM/XlUd0Pma-_I/AAAAAAAADM8/bWWc236yewcimDT76q1T_tb0htUZrRoQwCLcBGAsYHQ/s1600/03%2B%25D0%25BA%25D0%25B0%25D1%2580%25D1%2582%25D0%25B0.jpg)

**Russian:**

В США, Канаде и Китае кладбища динозавров расположены в местах с явными следами сверхкрупных водных катастроф, то есть, весь трупный материал здесь наносной.

**English:**

In the United States, Canada, and China, dinosaur cemeteries are located in areas with clear evidence of ultra-large water disasters, meaning that all of the carcass material here is sediment.

**Russian:**

Рис. 113. Кладбище динозавров в Китае (Чунцин)

**English:**

Figure 113. Dinosaur cemetery in China (Chongqing)

<!-- Local
[![](Step by step_files/04D0A7D183D0BDD186D0B8D0BD.jpg#clickable)](Step by step_files/04%2B%25D0%25A7%25D1%2583%25D0%25BD%25D1%2586%25D0%25B8%25D0%25BD.jpg)
-->

[![](https://1.bp.blogspot.com/-C2aCiAvo4Fk/XlUd6a9T4PI/AAAAAAAADNA/2fggaaN-w349jUa07JhfF5xhfarKqQPvwCLcBGAsYHQ/s1600/04%2B%25D0%25A7%25D1%2583%25D0%25BD%25D1%2586%25D0%25B8%25D0%25BD.jpg#clickable)](https://1.bp.blogspot.com/-C2aCiAvo4Fk/XlUd6a9T4PI/AAAAAAAADNA/2fggaaN-w349jUa07JhfF5xhfarKqQPvwCLcBGAsYHQ/s1600/04%2B%25D0%25A7%25D1%2583%25D0%25BD%25D1%2586%25D0%25B8%25D0%25BD.jpg)

**Russian:**

Рис. 114. Канада, Альберто, национальный парк Дайносор (Динозавр)

**English:**

Figure 114. Canada, Alberto, Dinosaur (Dinosaur) National Park

<!-- Local
[![](Step by step_files/05D094D0B0D0B9D0BDD0BED181D0BED180.jpg#clickable)](Step by step_files/05%2B%25D0%2594%25D0%25B0%25D0%25B9%25D0%25BD%25D0%25BE%25D1%2581%25D0%25BE%25D1%2580.jpg)
-->

[![](https://1.bp.blogspot.com/-g82ANFD5tQM/XlUeIkCk7-I/AAAAAAAADNQ/MVHANbPsPFccFYnCq1iFGTyHqdrZixyXQCLcBGAsYHQ/s1600/05%2B%25D0%2594%25D0%25B0%25D0%25B9%25D0%25BD%25D0%25BE%25D1%2581%25D0%25BE%25D1%2580.jpg#clickable)](https://1.bp.blogspot.com/-g82ANFD5tQM/XlUeIkCk7-I/AAAAAAAADNQ/MVHANbPsPFccFYnCq1iFGTyHqdrZixyXQCLcBGAsYHQ/s1600/05%2B%25D0%2594%25D0%25B0%25D0%25B9%25D0%25BD%25D0%25BE%25D1%2581%25D0%25BE%25D1%2580.jpg)

**Russian:**

**40.3. ПЛАВАЮЩИЕ РЕПТИЛИИ**

**English:**

**40.3. FLOATING REPTILES**

**Russian:**

Плавающей рептилии требуется всплывать (жабр нет), а плавники не приспособлены для скоростного перемещения, что делает охоту на ту же акулу невозможной. Рыбам такие рептилии проигрывают безоговорочно. В чем-то эти рептилии повторяют путь касаток и тюленей, однако, невзирая на красоту и силу, эти виды, судя по доле в общей биомассе океана, тупиковые. Интеллект и социальная организация у таких видов, скорее, механизм компенсации анатомических недостатков.

**English:**

A floating reptile needs to float (no gills) and its fins are not adapted for high-speed movement, which makes hunting the same shark impossible. These reptiles lose to fishes unequivocally. In some ways, these reptiles follow the path of orcas and seals, but despite their beauty and strength, these species, judging by their share in the total ocean biomass, are deadlocked. Intelligence and social organization in such species is rather a mechanism for compensating for anatomical defects.

**Russian:**

Рис. 115. Плиозавр на охоте

**English:**

Figure 115. Plyosaurus on the hunt

<!-- Local
[![](Step by step_files/02D0BED185D0BED182D0B0.jpg#clickable)](Step by step_files/02%2B%25D0%25BE%25D1%2585%25D0%25BE%25D1%2582%25D0%25B0.jpg)
-->

[![](https://1.bp.blogspot.com/-TVWooeW-RDg/XlUeOr4ibJI/AAAAAAAADNY/UPn5FaQEh3s-ZYYShcivqNsf03XGCscbgCLcBGAsYHQ/s1600/02%2B%25D0%25BE%25D1%2585%25D0%25BE%25D1%2582%25D0%25B0.jpg#clickable)](https://1.bp.blogspot.com/-TVWooeW-RDg/XlUeOr4ibJI/AAAAAAAADNY/UPn5FaQEh3s-ZYYShcivqNsf03XGCscbgCLcBGAsYHQ/s1600/02%2B%25D0%25BE%25D1%2585%25D0%25BE%25D1%2582%25D0%25B0.jpg)

**Russian:**

**40.4. ДВУНОГИЕ ПЛОТОЯДНЫЕ РЕПТИЛИИ**

**English:**

**40.4. BIPEDAL CARNIVOROUS REPTILES**

**Russian:**

Мелкие не плавающие рептилии прокормятся и в пустыне, но крупные тяготеют к воде, к основной кормовой базе. Рептилий справедливо сравнивают с птицами, но следует уточнить: с водными птицами. Как водные птицы, так и рептилии охотятся с помощью точного броска, - для долгой погони мышечный аппарат не подходит. Птицы решили проблему равновесия переносом ног ближе к шее, а ходящие на двух ногах рептилии -- массивным хвостом, но саму проблему равновесия решали и те, и другие. Площадь опоры у ног большая, что выгодно при медленном перемещении по илу и болотам и невыгодно при погоне. Огромные когти ног хороши для разгребания грунта, куры этим пользуются активно.

**English:**

Smaller, non-swimming reptiles will also feed in the desert, but larger reptiles gravitate towards water, their main food source. Reptiles are rightly compared to birds, but it should be clarified: water birds. Both water birds and reptiles hunt with a precise throw - the muscular apparatus is not suitable for a long chase. Birds solved the problem of balance by placing the legs closer to the neck and reptiles walking on two legs used a massive tail, but the problem of balance was solved by both of them. Large legs supported large flanks, good for walking across marshy slopes or river sloughs, but not so good for running after them. Huge claws of legs are good for raking the ground, and chickens use it actively.

**Russian:**

Рис. 116. Аист и тираннозавр

**English:**

Figure 116. Stork and Tyrannosaurus

<!-- Local
[![](Step by step_files/01D0A2D0B8D180D0B0D0BDD0BED0B7D0B0D0B2D180.jpg#clickable)](Step by step_files/01%2B%25D0%25A2%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.jpg)
-->

[![](https://1.bp.blogspot.com/-ORirCgx_ltM/XlUeVZW7xxI/AAAAAAAADNc/3qBZzKM7Ch0gs8EKXJC_MaR1dcEkgXLeQCLcBGAsYHQ/s1600/01%2B%25D0%25A2%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.jpg#clickable)](https://1.bp.blogspot.com/-ORirCgx_ltM/XlUeVZW7xxI/AAAAAAAADNc/3qBZzKM7Ch0gs8EKXJC_MaR1dcEkgXLeQCLcBGAsYHQ/s1600/01%2B%25D0%25A2%25D0%25B8%25D1%2580%25D0%25B0%25D0%25BD%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.jpg)

**Russian:**

Тираннозавр вполне прокормит себя прибрежной падалью или моллюсками, а вот чтобы преследовать добычу, нужны четыре лапы, - как у ягуара. Крупный челюстной аппарат хорош для разгрызания панцирей моллюсков и для заглатывания целиком или большими кусками, что характерно для птиц и рептилий и не характерно для тех, кто находится на вершине пищевой пирамиды. На сегодня моллюсками питаются преимущественно опять-таки птицы и рептилии.

**English:**

Tyrannosaurs can feed themselves with coastal carrion or shellfish but, like the jaguar, they need four paws to chase their prey. Large jaws are good for gnawing shells of molluscs and for swallowing them whole or in big pieces, which is characteristic of birds and reptiles but not characteristic of those on the top of food pyramid. Today, molluscs are predominantly eaten, again, by birds and reptiles.

**Russian:**

**40.5. ПЛЕЙСТОЦЕНОВЫЕ РЕПТИЛИИ**

**English:**

**40.5. PLEISTOCENE REPTILES**

**Russian:**

Смотрим статью Википедии «Вымирание в четвертичном периоде».

**English:**

See Wikipedia article "Quaternary Extinction".

**Russian:**

Вымерло представителей мегафауны:

**English:**

Megafauna are extinct:

**Russian:**

В Африке к югу от Сахары 8 из 50 (16 %)

**English:**

In sub-Saharan Africa 8 out of 50 (16%)

**Russian:**

В Азии 24 из 46 (52 %)

**English:**

In Asia 24 out of 46 (52%)

**Russian:**

В Европе 23 из 39 (59 %)

**English:**

In Europe, 23 out of 39 (59%)

**Russian:**

В Австралазии 19 из 27 (71 %)

**English:**

In Australasia, 19 out of 27 (71%)

**Russian:**

В Северной Америке 45 из 61 (74 %)

**English:**

In North America, 45 out of 61 (74%)

**Russian:**

В Южной Америке 58 из 71 (82 %)

**English:**

In South America 58 out of 71 (82%)

**Russian:**

В статье целиком отсутствуют данные о вымирании в плейстоцене рыб, китообразных и рептилий.

**English:**

The article is entirely missing data on the extinction of fish, cetaceans and reptiles in the Pleistocene.

**Russian:**

На деле, вымершие гигантские рептилии плейстоцена известны: Маврикийская гигантская черепаха, Тойотамафимейя (крокодил), Мегалания (варан) и Вонамби (змея). В основном, это Австралия. Жили рептилии плейстоцена и на Байкале -- жабы и лягушки. Однако в целом водная фауна в плейстоцене не пострадала; водная фауна (и особенно мегафауна) пострадала в периоды, датирующиеся миллионами лет назад -- не ближе.

**English:**

In fact, the extinct giant reptiles of the Pleistocene are known: the Mauritian giant tortoise, Toyotamafimea (crocodile), Megalania (varan) and Wanambi (snake). Mostly Australia. There were Pleistocene reptiles in Baikal as well - toads and frogs. However, in general, aquatic fauna was not affected in the Pleistocene; aquatic fauna (and especially megafauna) was affected during periods dating back millions of years -- no closer.

**Russian:**

Такая ситуация невозможна, например, потому, что именно в плейстоцене произошло смещение оси вращения Земли, а шельфы начало местами топить, а местами осушать. Именно в это время прибрежная кормовая база пострадала совершенно беспрецедентно, а значит, гигантские крокодилы, игуаны и змеи и крупные прибрежные млекопитающие типа Стеллеровой коровы, должны пострадать именно в этот период.

**English:**

Such a situation is impossible, for example, because it was during the Pleistocene that the Earth's axis of rotation shifted, and the shelves began to sink in places and dry up in places. It was during this time that the coastal foraging base suffered quite unprecedentedly, which means that giant crocodiles, iguanas and snakes, and large coastal mammals like the Steller's sea cow, must have suffered during this very period.

**Russian:**

Проблема в том, что рыбы, китообразные и рептилии объединены водной средой, в которой осадки формируются иначе, чем на суше. И, судя по результату, эти осадки иначе датируются, и на бумаге мегафауна воды игнорирует планетарную плейстоценовую катастрофу и гибнет асинхронно с мегафауной суши. Хороший пример: погибший 110 миллионов лет назад и почти окаменевший нодозавр с хорошо сохранившимися мумифицированными мягкими тканями: каждая из его остеодермий имеет слой кератина (тот материал, из которого сделаны ногти). Конфуз настолько велик, что сейчас мумию называют окаменелостью, а о сохранившемся кератине помалкивают.

**English:**

The problem is that fish, cetaceans and reptiles are united in an aquatic environment where sediments are formed differently than on land. And, judging by the result, these sediments are differently dated, and on paper, the megafauna of the water ignore the planetary Pleistocene catastrophe and die asynchronously with the megafauna of the land. A good example: a dead 110 million years ago and nearly fossilized Nodosaurus with well-preserved mummified soft tissues: each of its osteoderms has a layer of keratin (the material from which nails are made). The confusion is so great that the mummy is now called a fossil, and the preserved keratin is hushed up.

**Russian:**

Рис. 117. Мумия нодозавра [^125]

**English:**

Figure 117. Nodosaurus mummy [^125].

<!-- Local
[![](Step by step_files/83D09DD0BED0B4D0BED0B7D0B0D0B2D180.png#clickable)](Step by step_files/83%2B%25D0%259D%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.png)
-->

[![](https://1.bp.blogspot.com/-1aVug498qCE/XlUed_UCsJI/AAAAAAAADNk/Uqfz9qIC4WwynpDp0OnicK94DQHi-p3EwCLcBGAsYHQ/s1600/83%2B%25D0%259D%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.png#clickable)](https://1.bp.blogspot.com/-1aVug498qCE/XlUed_UCsJI/AAAAAAAADNk/Uqfz9qIC4WwynpDp0OnicK94DQHi-p3EwCLcBGAsYHQ/s1600/83%2B%25D0%259D%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B7%25D0%25B0%25D0%25B2%25D1%2580.png)

**Russian:**

По сообщению BBC [^131] в стенках кровеносных сосудов эмбрионов люфенгозавра, найденного на Тайване, сохранились фрагменты коллагена и белков, датируемых 195 миллионами лет назад. Коллаген так же выделен из костей утконосого динозавра, жившего около 80 млн. лет назад [^132].

**English:**

The BBC [^131] reports that fragments of collagen and proteins dating back 195 million years have been preserved in the blood vessel walls of embryos of Lufengosaurus found in Taiwan. Collagen has also been isolated from the bones of a duck-billed dinosaur that lived about 80 million years ago [^132].

**Russian:**

В такой ситуации главный вопрос даже не возраст белка, а почему в четвертичном периоде не вымерло 52-82 % видов рептилий, китов и рыб? Что за избирательность природы? Нет ли здесь системной ошибки? В Азии ареал мамонтов и ареал кладбищ динозавров соседствуют.

**English:**

In such situation the main question is not even the age of protein, but why in Quaternary period 52-82 % of species of reptiles, whales and fishes did not become extinct? What is this selectivity of nature? Is there not a system error here? In Asia, the area of mammoths and area of cemeteries of dinosaurs adjoin.

**Russian:**

Рис. 118. Ареал мамонтов и ареал кладбищ динозавров.

**English:**

Figure 118. Mammoth habitat and dinosaur cemetery habitat.

<!-- Local
[![](Step by step_files/D09CD0BED0BCD0BED0BDD182D18B.jpg#clickable)](Step by step_files/%25D0%259C%25D0%25BE%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg)
-->

[![](https://1.bp.blogspot.com/-F7OKVkHhCAg/XlUelkG5DPI/AAAAAAAADNw/B-b4ouCyxkU0s0InbYrrm9RW88fvsalAACLcBGAsYHQ/s1600/%25D0%259C%25D0%25BE%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg#clickable)](https://1.bp.blogspot.com/-F7OKVkHhCAg/XlUelkG5DPI/AAAAAAAADNw/B-b4ouCyxkU0s0InbYrrm9RW88fvsalAACLcBGAsYHQ/s1600/%25D0%259C%25D0%25BE%25D0%25BC%25D0%25BE%25D0%25BD%25D1%2582%25D1%258B.jpg)

**Russian:**

**ПРЕДАНИЯ МАНСИ**

**English:**

** MANSI TRADITIONS**

**Russian:**

Вместе с огненной водой несло множество огромных ящеров и змей. Они взбирались на плоты и поедали людей. Много народу тогда погибло... [^64]

**English:**

Many huge lizards and snakes were carried along with the fiery water. They climbed onto rafts and ate people. A lot of people died at that time... [^64]

**Russian:**

**41. ПОДОГРЕВ ПОЛЯРНЫХ ОБЛАСТЕЙ**

**English:**

**41. HEATING OF POLAR REGIONS**

**Russian:**

В Антарктиде у горы Ахернар обнаружены пни деревьев диаметром от 10 до 18 см. Вокруг пней отпечатки опавших листьев. Годовые кольца лишены следов морозов. Датируется лес 4 тысячами лет назад. Сходная ситуация в Арктике. Изучая Новосибирские острова, Эдуард фон Толль обнаружил останки фруктового дерева высотой 27 метров. Дерево хорошо сохранилось, с корнями и семенами. На ветвях все еще держались зеленые листья и плоды. Вопрос, откуда тепло?

**English:**

In Antarctica, tree stumps 10 to 18 cm in diameter were found near Mount Ahernar. Around the stumps there are imprints of fallen leaves. The annual rings are devoid of frost traces. The forest is dated 4 thousand years ago. The situation in the Arctic is similar. While studying the Novosibirsk islands, Eduard von Toll found the remains of a fruit-tree 27 meters high. The tree was well preserved, with roots and seeds. The branches still held green leaves and fruit. The question is, where did the heat come from?

**Russian:**

Дополнительное тепло вполне могло подаваться в полярные области снизу, от срединно-океанических хребтов.

**English:**

Additional heat could well have been supplied to the polar regions from below, from the mid-ocean ridges.

**Russian:**

Рис. 119. Срединно-океанические хребты

**English:**

Figure 119. Mid-ocean ridges

<!-- Local
[![](Step by step_files/84D09FD0BED0B4D0BED0B3D180D0B5D0B2.jpg#clickable)](Step by step_files/84%2B%25D0%259F%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B3%25D1%2580%25D0%25B5%25D0%25B2.jpg)
-->

[![](https://1.bp.blogspot.com/-8PsoldvJ81w/XlFJt1RxNbI/AAAAAAAADMI/K_XrPFYrbIIVAwzfykJdAkBPw6gyuP6gQCLcBGAsYHQ/s1600/84%2B%25D0%259F%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B3%25D1%2580%25D0%25B5%25D0%25B2.jpg#clickable)](https://1.bp.blogspot.com/-8PsoldvJ81w/XlFJt1RxNbI/AAAAAAAADMI/K_XrPFYrbIIVAwzfykJdAkBPw6gyuP6gQCLcBGAsYHQ/s1600/84%2B%25D0%259F%25D0%25BE%25D0%25B4%25D0%25BE%25D0%25B3%25D1%2580%25D0%25B5%25D0%25B2.jpg)

**Russian:**

**42. РАСШИРЯЮЩАЯСЯ ЗЕМЛЯ. ДИСПУТ**

**English:**

**42. EXPANDING EARTH. DISPUTE**

**Russian:**

Однако трудно представить, что Земля вследствие совершенно неясных причин смогла увеличить свой размер по диаметру вдвое и даже втрое. Объем земного шара при этом должен возрасти соответственно в 8 и даже в 27 раз. Некоторые защитники этой гипотезы считают, что расширение Земли происходит без привноси нового вещества извне. Увеличивается только объем. Но это невозможно, так как средняя плотность вещества Земли сегодня равна 5,52 грамма на кубический сантиметр, и если Земля была раньше по диаметру в 2 раза и по объему в 8 раз меньше, то ее плотность в то время должна была составлять 44,16 грамма на кубический сантиметр. Но такая плотность вещества ни у планет, ни у спутников в нашей системе, да и в нашей Галактике, пока не встречается [^92].

**English:**

However, it is difficult to imagine that the Earth due to absolutely unclear reasons could increase its size in diameter twice or even three times. At that, the volume of the globe should increase by 8 and even 27 times, respectively. Some defenders of this hypothesis believe that the expansion of the Earth occurs without bringing in a new substance from the outside. Only the volume increases. But it is impossible, as the average density of the substance of the Earth today is 5.52 grams per cubic centimeter. And if the Earth before by diameter was 2 times and by volume 8 times less, its density at that time should have been 44.16 grams per cubic centimeter. But such a density of matter neither at planets, nor at satellites in our system, and in our Galaxy, does not yet occur [^92].

**Russian:**

**КОНТРАРГУМЕНТ**

**English:**

**CONTRARGUMENT**

**Russian:**

Ранее предполагалось, что золото образовывалось при нуклеосинтезе сверхновых звёзд, однако по новой теории предполагается, что золото и другие элементы тяжелее железа образовались в результате разрушения нейтронных звёзд [^42]. Сегодня основная масса золота находится в ядре. Это означает, что планета Земля формировалась из продуктов распада очень плотных объектов -- порядка 4*10 в 17-й степени кг на кубический метр, и на начальных этапах развития могла быть даже еще меньше, чем оспаривается.

**English:**

Previously, gold was thought to be formed by the nucleosynthesis of supernovae, but a new theory suggests that gold and other elements heavier than iron were formed by the collapse of neutron stars{[^42]. Today, the bulk of the gold is in the core. This means that planet Earth was formed from the decay products of very dense objects -- on the order of 4*10 to the 17th power of kg per cubic meter, and may have been even smaller than disputed in the initial stages of evolution.

**Russian:**

Слоистая структура осадочных пород планеты лучше всего доказывает процесс роста ее объема: кислород, калий, кальций, фосфор могли появиться только из недр и только из тех тяжелых элементов, из которых планета была сформирована на начальном этапе. В такой ситуации рост объема продуктов распада физически закономерен.

**English:**

The layered structure of sedimentary rocks of the planet best proves the process of its volume growth: oxygen, potassium, calcium, phosphorus could appear only from the bowels and only from those heavy elements from which the planet was formed at the initial stage. In such a situation, the growth of the volume of decay products is physically logical.

**Russian:**

**43. ПОЗИТИВНЫЙ СЦЕНАРИЙ ПОТЕПЛЕНИЯ**

**English:**

**43. POSITIVE WARMING SCENARIO**

**Russian:**

Таяние ледников Антарктиды и Гренландии сильно ослабит вес и давление этих плит, что может привести к выходу шельфов Антарктиды и Гренландии из воды и по принципу балансира -- опустить близлежащие срединно-океанические хребты. Объем океанического бассейна в этих областях увеличится и примет лишнюю для планеты ледниковую воду. Бонусом, возможно, всплывет материк в районе Новой Зеландии.

**English:**

Melting of glaciers of Antarctica and Greenland will strongly weaken weight and pressure of these plates that can lead to release of shelves of Antarctica and Greenland from water and by a principle of a balancer - to lower the nearby mid-ocean ridges. The volume of oceanic pool in these areas will increase and will accept superfluous for a planet of glacial water. As a bonus, probably, the continent in area of New Zealand will float up.

**Russian:**

**44. ПОЗИТИВНЫЙ СЦЕНАРИЙ РАСШИРЕНИЯ ПЛАНЕТЫ**

**English:**

**44. POSITIVE SCENARIO FOR PLANETARY EXPANSION**

**Russian:**

Земля сейчас находится в стадии похолодания, что означает пассивное поведение срединно-атлантических хребтов и пониженный выброс главного продукта распада глубинных геохимических процессов -- воды. Следующая стадия активизирует расширение, добавит тепла в полярных областях, увеличит емкость океанических бассейнов, возвратит нам Берингию и Доггерленд и увеличит подачу пресной воды из недр.

**English:**

The Earth is now in a cooling phase, which means passive behaviour of the mid-Atlantic ridges and reduced release of the main product of the decay of deep geochemical processes - water. The next stage will intensify expansion, add heat to the polar regions, increase ocean basin capacity, return Beringia and Doggerland to us and increase the supply of fresh water from the subsurface.

**Russian:**

**45. ОБ ОТСУТСТВИИ ЛУНЫ**

**English:**

**45. ABOUT THE ABSENCE OF THE MOON**

**Russian:**

В V веке до н. э. греческий философ и астроном Анаксагор из Клазомен, пользовался не дошедшими до нас источниками, где утверждалось, что Луна появилась на небе позже возникновения Земли. В II веке до н.э. главный смотритель Александрийской библиотеки, Аполлоний Родосский в сочинении «Аргонавтика» привел слова Аристотеля о древних обитателях горных районов Аркадии, которые «питались желудями, а было это в те времена, когда на небе еще не было Луны». Плутарх говорит о правителе Аркадии по имени Проселенос, что значит «долунный», его подданных проселенитах, первых обитателях Аркадии. В Египетской мифологии, говорится, что вместе с Луной появился новый бог Тот, который прибавил пять дней в году и создал приливы с отливами. По Платону атланты поклонялись звездам и Солнцу, а про Луну молчали или ничего не знали. ***[Современные ученые не отрицают того, что в раннюю эпоху существования человечества Луна на небосклоне отсутствовала]***. По одной из версий, Луна первоначально являлась одной из планет Солнечной системы, но вследствие космического катаклизма сошла с орбиты и превратилась в земной спутник [^126].

**English:**

In the 5th century B.C. the Greek philosopher and astronomer Anaxagoras of Clazomenes, used sources that have not survived, which claimed that the Moon appeared in the sky later than the emergence of the Earth. In the II century BC, the chief curator of the Library of Alexandria, Apollonius of Rhodes, in his work "Argonautica" cited the words of Aristotle about the ancient inhabitants of the mountainous regions of Arcadia, who "ate acorns, and it was in those days when there was no moon in the sky yet". Plutarch speaks of the ruler of Arcadia named Proselenos, which means "long moon", and his subjects the Proselenites, the first inhabitants of Arcadia. In Egyptian mythology, it is said that along with the moon came a new god Thoth, who added five days to the year and created the ebb and flow of the tides. According to Plato, the Atlanteans worshipped the stars and the Sun, but were silent or knew nothing about the Moon. ***[Modern scientists do not deny that there was no moon in the sky in the early era of humanity]***. According to one version, the moon was originally one of the planets of the solar system, but as a result of a cosmic cataclysm fell out of orbit and turned into an earthly satellite [^126].

**Russian:**

Оригинальные названия есть лишь у восьми римских месяцев, но, если есть Луна, то 12 месяцев намного удобнее. Луна структурно сходна с ядром планеты, то есть, вполне может оказаться отдавшим весь водород губчатым ядром погибшего Фаэтона.

**English:**

Only eight Roman months have their original names, but if there is a Moon, 12 months is much more convenient. The Moon is structurally similar to the planet's core, i.e., it may well be the spongy core of the dead Phaeton that gave away all the hydrogen.

**Russian:**

**46. КУРГАННЫЕ МОГИЛЬНИКИ**

**English:**

**46. KURGAN BURIAL GROUNDS**

**Russian:**

Приказ Министерства культуры Ставропольского края от 15 мая 2006 года N 56 [^129].

**English:**

Order of the Ministry of Culture of the Stavropol Territory of May 15, 2006 N 56 [^129].

**Russian:**

Эпоха бронзы, средневековье. Количество курганных насыпей, название курганного могильника. Всего могильников 115, всего насыпей 529.

**English:**

Bronze Age, Middle Ages. Number of barrow mounds, name of the barrow mound. Total number of burial mounds 115, total mounds 529.

(Насыпей) Могильник

(5) Арзгир-3

(3) Бекешевский-2

(3) Бешпагирский-1

(1) Бешпагирский-1

(1) Бешпагирский-2

(1) Бешпагирский-3

(1) Благодарный-1

(2) Богомолов-1

(4) Большая Кугульта-1

(5) Большая Кугульта-2

(4) Большая Кугульта-3

(2) Буйвола-2

(1) Величаевский-1

(4) Воздвиженское-14

(5) Воздвиженское-15

(16) Вознесеновский-1

(2) Вознесеновский-2

(9) Вознесеновский-3

(1) Дмитриевский-1

(4) Дмитриевский-2

(3) Донской-1

(2) Донской-2

(3) Дыдымкин-1

(13) Дыдымкин-2

(3) Дыдымкин-3

(3) Дыдымкин-4

(14) Журавский-1

(2) Загорный-2

(2) Зеркальный- 1

(1) Ивановский-10

(5) Ивановский-9

(2) Ипатово-11

(3) Ипатово-12

(6) Калиновский-1

(9) Кизиловский-1

(1) Кизиловский-2

(1) Константиновский-3

(9) Константиновский-4

(4) Константиновский-5

(5) Константиновский-6

(5) Кочубеевский-9

(1) Кочубей-1

(2) Красногвардейский-2

(4) Лохматый-1

(6) Любительский-1

(3) Малая Кугульта-1

(2) Малая Кугульта-2

(4) Малая Кугульта-3

(3) Малый Гок-1

(1) Михайловский-1

(9) Московский-1

(8) Московский-2

(2) Надеждинский-2

(4) Надеждинский-3

(4) Новая жизнь-3

(2) Новая жизнь-4

(1) Новая жизнь-5

(10) Новая жизнь-6

(1) Новая жизнь-7

(5) Новоборгустанский-2

(3) Новоборгустанский-3

(5) Новоекатериновский-1

(2) Новоекатериновский-2

(2) Новоекатериновский-3

(2) Новомихайловский-2

(1) Новопавловский-2

(2) Новотерновский-1

(3) Новотерновский-2

(3) Новотроицкий-1

(1) Новотроицкий-2

(2) Падинский-1

(4) Пелагиадский-1

(31) Подгорный-1

(4) Подлесное-1

(2) Присадовый-4

(2) Пролетарский-2

(4) Пятигорский-1

(2) Рагули-2

(23) Рагули-3

(4) Рагули-4

(2) Рагули-5

(1) Родники-1

(3) Садовый-1

(3) Северный-1

(10) Серноводский-1

(3) Серноводский-2

(23) Солнечный-1

(8) Солнечный-2

(1) Спасское-1

(1) Спорный-1

(2) Спорный-2

(4) Спорный-3

(15) Стародубский-1

(4) Староизобильненский-1

(5) Староизобильненский-2

(9) Старомарьевский-1

(1) Старомарьевский-2

(4) Старомарьевский-3

(3) Старомарьевский-4

(4) Сухоозерский-1

(8) Темижбекский-2

(8) Тищенский-1

(5) Тищенский-2

(2) Труновский-1

(1) Тугулук-1

(3) Тугулук-2

(12) Тугулук-3

(2) Урожайный-1

(1) Урожайный-2

(10) Урожайный-3

(19) Урожайный-4

(1) Уч-Тюбе-1

(1) Штурм-1

(1) Энергетик-2

(5) Юца-4

**Russian:**

**47. ВЗАИМОЗАМЕНЯЕМОСТЬ СТОРОН СВЕТА В ПЕРСИДСКОМ ЯЗЫКЕ**

**English:**

**47. INTERCHANGEABILITY OF THE SIDES OF THE WORLD IN THE PERSIAN LANGUAGE**

**Russian:**

Из комментария [^136].

**English:**

From the comment [^136].

**Russian:**

Digital Dictionaries of South Asia

**English:**

Digital Dictionaries of South Asia

[https://dsal.uchicago.edu/dictionaries/steingass/](https://dsal.uchicago.edu/dictionaries/steingass/)

[https://dsal.uchicago.edu/cgi-bin/app/steingass_query.py?qs=east&matchtype=default](https://dsal.uchicago.edu/cgi-bin/app/steingass_query.py?qs=east&matchtype=default)

**Russian:**

15) با ختر (p. 136) با ختر bāḵẖtar, The west; the east; Bactria.

**English:**

15) با ختر (p. 136) با ختر bāḵẖtar, The west; the east; Bactria.

**Russian:**

52) خاور (p. 445) خاور ḵẖāwar, The west, but often used by the poets for the east; the sun; a thorn.

**English:**

52) خاور (p. 445) خاور ḵẖāwar, The west, but often used by the poets for the east; the sun; a thorn.

**Russian:**

53) خاوران (p. 445) خاوران ḵẖāwarān, East and west; a district in Khurāsān, birth-place of the poet Anwar.

**English:**

53) خاوران (p. 445) خاوران ḵẖāwarān, east and west; a district in Khurāsān, birth-place of the poet Anwar.

**Russian:**

55) خاورى (p. 445) خاورى ḵẖāwarī, Western; eastern; the sun; surname of the poet Anwar

**English:**

55) خاورى (p. 445) خاورى ḵẖāwarī, western; eastern; the sun; surname of the poet Anwar

**Russian:**

Перевод:

**English:**

Translation:

**Russian:**

15 )با ختر (стр. 136) با ختر bāḵẖtar, Запад; Восток; Бактрия

**English:**

15 )با ختر (p. 136) با ختر bāḵẖtar, West; East; Bactria

**Russian:**

52 )ااور (стр. 445 )ااورāāwar, Запад, но часто используется поэтами для Востока; солнце; шип.

**English:**

52 )ااور (p. 445 )ااورāwar, West, but often used by poets for East; sun; sp.

**Russian:**

53 )ااوران (стр. 445 )ااورانāāwarān, восток и запад; район в Хурасане, место рождения поэта Анвара.

**English:**

53 )ااوران (p. 445 )ااورانāwarān, east and west; a district in Khurasan, birthplace of the poet Anvar.

**Russian:**

55 )ااورى (стр. 445 )ااورāāwarī, Западный; Восточный; солнце; фамилия поэта Анвар

**English:**

55 )ااورى (p. 445 )ااورāwarī, Western; Eastern; sun; surname of poet Anwar

[https://dsal.uchicago.edu/cgi-bin/app/steingass_query.py?qs=east&matchtype=default](https://dsal.uchicago.edu/cgi-bin/app/steingass_query.py?qs=east&matchtype=default)

**Russian:**

**48. ДВА ВОСТОКА И ДВА ЗАПАДА**

**English:**

**48. TWO EAST AND TWO WEST**

**Russian:**

Неоднозначные отношения со сторонами света складывались не только у персидских поэтов, но и у составителей Корана:

**English:**

Not only Persian poets, but also the compilers of the Qur'an had an ambiguous relationship with the sides of the world:

**Russian:**

**СУРА 70: «АЛЬ-МААРИДЖ» («СТУПЕНИ»)**

**English:**

**SURA 70: "AL-MAARIJ" ("STEPS") **

**Russian:**

70:40

**English:**

70:40

**Russian:**

فَلَا أُقْسِمُ بِرَبِّ الْمَشَارِقِ وَالْمَغَارِبِ إِنَّا لَقَادِرُونَ

**English:**

فَلَا أُقْسِمُ بِرَبِّ الْمَشَارِقِ وَالْمَغَارِبِ إِنَّا لَقَادِرُونَ

**Russian:**

Фаля Уксиму Бираббиль-Машарики Уаль-Магариби Инна Лякадируун.

**English:**

Fala Uximu Birabbil-Mashariki Oul-Magaribi Inna Lyakadiruun.

**Russian:**

Клянусь Господом востоков и западов! Мы в состоянии...

**English:**

By the Lord of the east and the west! We are able...

**Russian:**

**Сура 55 «Милосердный»**

**English:**

**Sura 55 "Merciful" **

[https://read-quran.ru/s/43](https://read-quran.ru/s/43)

(17). Господь обоих востоков

**Russian:**

и Господь обоих западов.

**English:**

and the Lord of both the West.

**Russian:**

**Сура 55 «Милосердный»**

**English:**

**Sura 55 "Merciful" **

[https://ru.quranacademy.org/quran/55](https://ru.quranacademy.org/quran/55)

[https://falaq.ru/quran/poro/55](https://falaq.ru/quran/poro/55)

**Russian:**

17. Владыка двух Восходов и Закатов,

**English:**

17. Lord of the two Sunrises and Sunsets,

**Russian:**

**СУРА 37: «АС-САФФАТ» («СТОЯЩИЕ В РЯД»)**

**English:**

**SURA 37: "AS-SAFFAT" ("STANDING IN A ROW") **

[https://islam.global/verouchenie/koran/sura-37-as-saffat-stoyashchie-v-ryad-/](https://islam.global/verouchenie/koran/sura-37-as-saffat-stoyashchie-v-ryad-/)

**Russian:**

37:5

**English:**

37:5

**Russian:**

رَبُّ السَّمَاوَاتِ وَالْأَرْضِ وَمَا بَيْنَهُمَا وَرَبُّ الْمَشَارِقِ

**English:**

رَبُّ السَّمَاوَاتِ وَالْأَرْضِ وَمَا بَيْنَهُمَا وَرَبُّ الْمَشَارِقِ

**Russian:**

Раббус-Самауати Уаль-Арды Уа Ма Байнахума Уа Раббуль-Машариик.

**English:**

Rabbus-Samawati Ual-Ardy Ua Ma Bainahuma Ua Rabbul-Mashariq.

**Russian:**

Он - Господь небес, земли и того, что между ними, Господь восходов.

**English:**

He is the Lord of the heavens and the earth and that which is between them, the Lord of the rising.

**Russian:**

**Сура 37 «Стоящие В Ряд»**

**English:**

**Sura 37 "Standing In Line "**

[https://falaq.ru/quran/krac/37](https://falaq.ru/quran/krac/37)

**Russian:**

5. (5). Владыка небес, и земли, и того. что между ними,

**English:**

5. (5). Lord of the heavens, and of the earth, and that which is between them,

**Russian:**

и Владыка востоков!)

**English:**

and Lord of the Orient!)

**Russian:**

Тут что особенно интересно: в арабоязычном Коране, в суре "Милосердный" слово

**English:**

Here it is especially interesting: in the Arabic Koran, in Sura "Merciful" the word

**Russian:**

مَشْرِقٌ [masẖ̊riquⁿ]

**English:**

مَشْرِقٌ [masẖ̊riquⁿ]

**Russian:**

‪мн.‬ ‫مَشَارِقُ‬

**English:**

m. مَشَارِقُ

**Russian:**

восход; восток

**English:**

sunrise; east

**Russian:**

употреблено в двойственном числе и в части русских переводов представлено в виде "оба востока", "оба запада".

**English:**

is used in the dual number and in some Russian translations is represented as "both east", "both west".

**Russian:**

Двойственность еще можно как-то трактовать -солнце и луна - у каждого по востоку и по западу, да есть и отдельные строки, посвященные светилам. Можно постараться и отсебятину вписать 
[https://falaq.ru/quran/munt/55](https://falaq.ru/quran/munt/55) :

**English:**

The duality can still be interpreted somehow - the sun and the moon - in each the east and west, but there are also separate lines devoted to the luminaries. It is possible to try to write your own words [https://falaq.ru/quran/munt/55](https://falaq.ru/quran/munt/55):

**Russian:**

17 . Он - Господь обоих восходов солнца и обоих его закатов летом и зимой.

**English:**

17 . He is Lord of both sunrises and both sunsets in summer and winter.

**Russian:**

Но в сурах "Ступени" и "Стоящие в ряд" востоков уже больше двух - там это слово употреблено во множественном числе, нужно было исхитряться, что-то еще придумать, и выкрутились таки: 40-41. Клянусь Господом восхода и захода солнца, луны и планет! [https://falaq.ru/quran/munt/70](https://falaq.ru/quran/munt/70), хотя ни о каких планетах в исходнике речи не ведется.

**English:**

But in surahs "Steps" and "Standing in a row" there are more than two Orientals - there this word is used in plural, it was necessary to make up something else, and they managed to make it up: 40-41. I swear by God the sunrise and sunset, the moon and the planets! [https://falaq.ru/quran/munt/70](https://falaq.ru/quran/munt/70), though there are no planets in the source.

**Russian:**

Из текстов переводов видно, что явного указания на то, что восток - это запад, а запад - восток нет, но трактовать употребление этих понятий во множественном числе толкователям однозначно нечем.

**English:**

From the texts of translations we can see that there is no clear indication that East is West and West is East, but there is no clear interpretation of the use of these concepts in the plural by interpreters.

**Russian:**

**ПРИМЕЧАНИЕ**: при этом с севером и востоком никаких проблем нет, и именно такая ситуация обязана сложиться при перевороте Земли по Джанибекову.

**English:**

**NOTE**: there is no problem with the north and east, and this is the situation that is bound to occur during the turnover of the Earth by Janibekov.

**Russian:**

(продолжение следует)

**English:**

(To be continued)

**Russian:**

**СПИСОК ЛИТЕРАТУРЫ**:

**English:**

**LIST OF REFERENCES**:

**Russian:**

[^1]: Тяпкин К. Ф. Физика Земли (учебник). --- Киев. Высш. шк., 1998. --- 312 с

**English:**

[^1]: Tyapkin K. F. Physics of the Earth (textbook). - Kyiv. Vyssh. shk., 1998. - 312 с

**Russian:**

[^2]: Изменение положения оси вращения в теле Земли: причина, механизм и использование для объяснения глобальных тектонических процессов в земной коре © К. Ф. Тяпкин, 2012

**English:**

2. Change of the rotation axis position in the Earth's body: cause, mechanism and use for explanation of global tectonic processes in the Earth's crust © K.F. Tyapkin, 2012

[http://dspace.nbuv.gov.ua/bitstream/handle/123456789/98303/08-Tyapkin.pdf?sequence=1](http://dspace.nbuv.gov.ua/bitstream/handle/123456789/98303/08-Tyapkin.pdf?sequence=1)

**Russian:**

[^3]: Хизаношвили Г. Д. Динамика земной оси вращения и уровней океанов. --- Тбилиси: ЦОДНА, 1960. --- 143 с.

**English:**

3. Khizanoshvili G.D. Dynamics of the Earth's Rotation Axis and Ocean Levels. - Tbilisi: TSODNA, 1960. - 143 с.

**Russian:**

[^4]: Википедия. Литориновое море

**English:**

4. Wikipedia. Littorine Sea

**Russian:**

[^5]: Википедия. Иольдиевое море

**English:**

5. Wikipedia. Ioldian Sea

**Russian:**

[^6]: Википедия. Разлом

**English:**

6. Wikipedia. Fault

**Russian:**

[^7]: Тяпкин К. Ф. Физика Земли (учебник). --- Киев. Высш. шк., 1998.

**English:**

7. Tyapkin K. F. Physics of the Earth (textbook). - Kyiv. Vyssh. shk., 1998.

**Russian:**

[^8]: Магидович И.П., Магидович В.И. 'Очерки по истории географических открытий. Том 5' - Москва: Просвещение, 1986, глава 8. Линеаменты Европы и Азии

**English:**

8. Magidovich I.P., Magidovich V.I. "Essays on the history of geographical discoveries. Volume 5' - Moscow: Prosveshcheniye, 1986, Chapter 8. Lineaments of Europe and Asia

[http://antarctic.su/books/item/f00/s00/z0000015/st083.shtml](http://antarctic.su/books/item/f00/s00/z0000015/st083.shtml)

**Russian:**

[^9]: В. Докучаев. Почвенные зоны Северного полушария 1899 год

**English:**

9. V. Dokuchaev. Soil zones of the Northern Hemisphere 1899

**Russian:**

[^10]: Елена Наймарк. Вымирание позднечетвертичных животных управлялось климатом

**English:**

10. Elena Naimark. Late Quaternary extinctions were controlled by climate

[https://elementy.ru/novosti_nauki/431711/Vymiranie_pozdnechetvertichnykh_zhivotnykh_upravlyalos_klimatom](https://elementy.ru/novosti_nauki/431711/Vymiranie_pozdnechetvertichnykh_zhivotnykh_upravlyalos_klimatom)

**Russian:**

[^11]: Археология СССР с древнейших времен до средневековья в 20 томах. Трансгрессия и регрессия - изменения уровня моря в древности

**English:**

11. archaeology of the USSR from ancient times to the Middle Ages in 20 volumes. Transgression and regression - sea level changes in ancient times

**Russian:**

[^12]: Localización de permafrost en el planeta 1998

**English:**

12. Localización de permafrost en el planeta 1998

[http://www.igme.es/SalaPrensa/BancoImagenes/foto.asp?id=580](http://www.igme.es/SalaPrensa/BancoImagenes/foto.asp?id=580)

**Russian:**

[^13]: Наука и жизнь. Доисторическая климатология. Кандидат географических наук В. Николаев.

**English:**

13. Science and Life. Prehistoric climatology. Candidate of Geographical Sciences V. Nikolaev.

[http://www.nkj.ru/archive/articles/6576/](http://www.nkj.ru/archive/articles/6576/)

**Russian:**

[^14]: Учёные открыли скоростной приход мини-версии ледникового периода

**English:**

14. Scientists discover speedy arrival of a mini-version of the Ice Age

[http://www.membrana.ru/particle/14385](http://www.membrana.ru/particle/14385)

**Russian:**

[^15]: Википедия. Ледниковый период

**English:**

15. Wikipedia. Ice Age

**Russian:**

[^16]: Википедия. Гипотеза расширяющейся Земли

**English:**

16. Wikipedia. The expanding Earth hypothesis

**Russian:**

[^17]: Подъем уровня океана будет сильнее в северном полушарии

**English:**

17. Ocean level rise will be stronger in the northern hemisphere

[https://ria.ru/20090206/161164007.html](https://ria.ru/20090206/161164007.html)

**Russian:**

[^18]: Новый взгляд на геотектогенез, обусловленный изменением положения тектоносферы Земли относительно оси ее вращения. Тяпкин, К.Ф. 2014

**English:**

18. A new view on geotectogenesis caused by changes in the position of the Earth's tectonosphere relative to the axis of its rotation. Tyapkin, K.F. 2014

[https://cyberleninka.ru/article/n/novyy-vzglyad-na-geotektogenez-obuslovlennyy-izmeneniem-polozheniya-tektonosfery-zemli-otnositelno-osi-ee-vrascheniya](https://cyberleninka.ru/article/n/novyy-vzglyad-na-geotektogenez-obuslovlennyy-izmeneniem-polozheniya-tektonosfery-zemli-otnositelno-osi-ee-vrascheniya)

**Russian:**

[^19]: Теория электроразрядного взрыва Невского А.П.

**English:**

19. The theory of electrodischarge explosion by A.P. Nevsky.

[http://www.insiderrevelations.ru/forum/forum18/topic2049/](http://www.insiderrevelations.ru/forum/forum18/topic2049/)

**Russian:**

[^20]: Тунгусский метеорит и загадки кометы Галлея. А. Войцеховский, 2001.

**English:**

20. The Tunguska meteorite and the mysteries of Halley's comet. A. Voitsekhovsky, 2001.

[https://studfile.net/preview/8064975/](https://studfile.net/preview/8064975/)

[21. МОЛНИЯ БЬЕТ СВЕРХУ ВНИЗ ИЛИ СНИЗУ ВВЕРХ?](https://www.blogger.com/goog_1656161564)

[http://www.vokrugsveta.ru/quiz/320510/](http://www.vokrugsveta.ru/quiz/320510/)

**Russian:**

[^22]: Молнии бьющие вверх

**English:**

22. Lightning striking upwards

[https://morefactov.ru/fact/Molnii-bjushhie-vverh-neobychnoe-javlenie-prirody](https://morefactov.ru/fact/Molnii-bjushhie-vverh-neobychnoe-javlenie-prirody)

**Russian:**

[^23]: Новиков Л. С. Воздействие твердых частиц естественного и искусственного происхождения на космические аппараты. Учебное пособие. -- М.: Университетская книга, 2009.

**English:**

23. Novikov L. S. Impact of particulate matter of natural and artificial origin on spacecraft. Tutorial. -- M.: University book, 2009.

[http://lib.sinp.msu.ru/static/tutorials/83_Textbook_Novikov_SpaceDebris.pdf](http://lib.sinp.msu.ru/static/tutorials/83_Textbook_Novikov_SpaceDebris.pdf)

**Russian:**

[^24]: Электроразрядный взрыв. Геннадий Ершов

**English:**

24. Electrodischarge explosion. Gennady Ershov

[https://gennady-ershov.ru/tungusskij-meteorit/padenie_tungusskogo_meteorita.html](https://gennady-ershov.ru/tungusskij-meteorit/padenie_tungusskogo_meteorita.html)

**Russian:**

[^25]: Тунгусский Вестник КСЭ №6 » А.Ю.ОЛЬХОВАТОВ, ОБ ОШИБКАХ В РАБОТЕ А. П. НЕВСКОГО

**English:**

25. Tungus Bulletin of the CSE No. 6 " A. Y. OLKHOVATOV, ON ERRORS IN THE WORK OF A. P. NEVSKOGOY

[http://tunguska.tsc.ru/ru/science/tv/6/13/](http://tunguska.tsc.ru/ru/science/tv/6/13/)

**Russian:**

[^26]: A Debate over the Asquisition of an Electric Potential by a Meteoroid. Yu. L. Raizer, August, 5, 2002.

**English:**

26. A Debate over the Asquisition of an Electric Potential by a Meteoroid. Yu. L. Raizer, August, 5, 2002.

[https://github.com/it4history/reconstruction/blob/master/Wos/books/Raizer%202003.pdf](https://github.com/it4history/reconstruction/blob/master/Wos/books/Raizer%202003.pdf)

**Russian:**

[^27]: Новое о кометах. Л.М. Топтунова

**English:**

27. New about the comets. L.M. Toptunova

[http://sceptic-ratio.narod.ru/kp/kp78.htm](http://sceptic-ratio.narod.ru/kp/kp78.htm)

**Russian:**

[^28]: В.В. Федынский. Метеоры. 7. МЕТЕОРНЫЕ ПОТОКИ

**English:**

28. V.V. Fedynsky. Meteors. 7. METEOR STREAMS

[http://www.astronet.ru/db/msg/1198013/08.html](http://www.astronet.ru/db/msg/1198013/08.html)

**Russian:**

[^29]: Энергия ионизации атома

**English:**

29. Ionization energy of an atom [http://itchem.ru/energiya_ionizacii_atoma](http://itchem.ru/energiya_ionizacii_atoma)

**Russian:**

[^30]: Википедия. Ионизация

**English:**

30. Wikipedia. Ionization

**Russian:**

[^31]: Континенты пришли в движение, планета готовится к перевороту. Часть 1.

**English:**

31. Continents are on the move, the planet is preparing for a coup. Part 1.

[https://cont.ws/@starfire/1175354](https://cont.ws/@starfire/1175354)

**Russian:**

[^32]: Википедия. Эффект Джанибекова

**English:**

32. Wikipedia. Janibekov effect

**Russian:**

[^33]: Происхождение и эволюция метеороидных роев. Ю.В.ОБРУБОВ

**English:**

33. The origin and evolution of meteoroidal swarms. Y.V. OBRUBOV

[http://www.astronet.ru/db/msg/1171216](http://www.astronet.ru/db/msg/1171216)

**Russian:**

[^34]: Происхождение комет: новый взгляд на старую проблему. Ф. А. Цицин

**English:**

34. Origins of comets: a new look at an old problem. F. A. Tsitsin

[http://www.astronet.ru/db/msg/1187689](http://www.astronet.ru/db/msg/1187689)

**Russian:**

[^35]: Википедия. Палласит

**English:**

35. Wikipedia. Pallasite

**Russian:**

[^36]: Википедия. Оливин

**English:**

36. Wikipedia. Olivine

**Russian:**

[^37]: Википедия. Золото

**English:**

37. Wikipedia. Gold

**Russian:**

[^38]: Месторождения золота в России

**English:**

38. Gold deposits in Russia

[https://www.garshin.ru/evolution/geology/geosphere/gold/goldfields/gold-of-russia.htm](https://www.garshin.ru/evolution/geology/geosphere/gold/goldfields/gold-of-russia.htm)

**Russian:**

[^39]: Рис. 138. Карта наибольшего оледенения Сибири

**English:**

39. Fig. 138. Map of the greatest glaciation of Siberia

[http://www.geologam.ru/geology/basics/imagedata/4680/24](http://www.geologam.ru/geology/basics/imagedata/4680/24)

**Russian:**

[^40]: Scientific American, 1855

**English:**

40. Scientific American, 1855

[https://archive.org/stream/scientific-american-1855-06-30/scientific-american-v10-n42-1855-06-30#page/n0/mode/2up](https://archive.org/stream/scientific-american-1855-06-30/scientific-american-v10-n42-1855-06-30#page/n0/mode/2up)

**Russian:**

[^41]: Столкновение миров. Иммануил Великовский

**English:**

41. The Clash of Worlds. Immanuel Velikovsky

[https://www.rulit.me/books/stolknovenie-mirov-read-149022-58.html](https://www.rulit.me/books/stolknovenie-mirov-read-149022-58.html)

**Russian:**

[^42]: Игнатиус Доннелли. ГИБЕЛЬ БОГОВ В ЭПОХУ ОГНЯ И КАМНЯ

**English:**

42. Ignatius Donnelly. THE DEMISE OF THE GODS IN THE AGE OF FIRE AND STONE

**Russian:**

[^43]: База данных по состоянию на 01 января 2020 г., Excel-2007. 156205 датированных исторических свидетельств, 29,3 Мб.

**English:**

43. Database as at 01 January 2020, Excel-2007. 156205 dated historical evidence, 29.3 MB.

[https://drive.google.com/file/d/1UfWe2Ezeauh1N93mMnj8HH8d6CRTg61K/view?usp=sharing](https://drive.google.com/file/d/1UfWe2Ezeauh1N93mMnj8HH8d6CRTg61K/view?usp=sharing)

**Russian:**

[^44]: Woodcut showing destructive influence of a fourth century comet. (Image courtesy NASA/JPL)

**English:**

44. Woodcut showing destructive influence of a fourth century comet. (Image courtesy NASA/JPL)

[https://stardustnext.jpl.nasa.gov/education/pdfs/TalesCometsTell_Cards.pdf](https://stardustnext.jpl.nasa.gov/education/pdfs/TalesCometsTell_Cards.pdf)

**Russian:**

[^45]: Википедия. C/1858 L1 (Донати)

**English:**

45. Wikipedia. C/1858 L1 (Donati)

**Russian:**

[^46]: ЛЕОНИДЫ. JOE LYNCH, N6C

**English:**

46. LEONIDES. JOE LYNCH, N6C

[http://rfanat.qrz.ru/s5/leonidi.html](http://rfanat.qrz.ru/s5/leonidi.html)

**Russian:**

[^47]: В Мурманской области найдено четыре рудопроявления золота и платины

**English:**

47. Four gold and platinum occurrences found in the Murmansk Region

[https://www.opentown.ru/novosti/17695](https://www.opentown.ru/novosti/17695)

**Russian:**

[^48]: Найден источник австралийских тектитов --- крупнейший за последний миллион лет метеоритный кратер

**English:**

48. Australian tectite source found - largest meteor crater in last million years

[https://elementy.ru/novosti_nauki/433594/Nayden_istochnik_avstraliyskikh_tektitov_krupneyshiy_za_posledniy_million_let_meteoritnyy_krater](https://elementy.ru/novosti_nauki/433594/Nayden_istochnik_avstraliyskikh_tektitov_krupneyshiy_za_posledniy_million_let_meteoritnyy_krater)

**Russian:**

[^49]: ЧТО ЕСЛИ ВЫЛИТЬ РАСКАЛЕННУЮ ЛАВУ в 1100 ГРАДУСОВ в БАССЕЙН ..?!

**English:**

49. WHAT IF PURCHASED LAVA IN 1100 GRADUES IN A POOL ...?!

[https://www.youtube.com/watch?v=OewQG2zZQNk&feature=emb_title](https://www.youtube.com/watch?v=OewQG2zZQNk&feature=emb_title)

**Russian:**

[^50]: Kilka map (02 a -- Informacje o rudach żelaza)

**English:**

50. Kilka map (02 a -- Informacje o rudach żelaza)

[https://kodluch.wordpress.com/2019/12/22/%e2%99%ab-off-topic-kilka-map-02-a-informacje-o-rudach-zelaza/](https://kodluch.wordpress.com/2019/12/22/%e2%99%ab-off-topic-kilka-map-02-a-informacje-o-rudach-zelaza/)

**Russian:**

[^51]: The burning of Troy. By Alfred de Grazia

**English:**

51. The burning of Troy. By Alfred de Grazia

[http://quantavolution.net/vol_11/burning_of_troy_p1_06.htm](http://quantavolution.net/vol_11/burning_of_troy_p1_06.htm)

**Russian:**

[^52]: Евгений Киселев. Чем был «греческий огонь»?

**English:**

52. Evgeny Kiselev. What was the "Greek fire"?

**Russian:**

[^53]: Комментарий от Виктор Давыдов.

**English:**

53. Comment from Victor Davydov.

[https://chispa1707.livejournal.com/3113948.html](https://chispa1707.livejournal.com/3113948.html)

**Russian:**

[^54]: Cornelis Anthonisz (Netherlandish, 1507--1553). The Tower of Babel, 1547. Etching. Rijksmuseum, Amsterdam. Image courtesy of Rijksmuseum, Amsterdam

**English:**

54. Cornelis Anthonisz (Netherlandish, 1507--1553). The Tower of Babel, 1547. Etching. Rijksmuseum, Amsterdam. Image courtesy of Rijksmuseum, Amsterdam.

[https://www.arts-ny.com/art-nyc-renaissance-etching-met-museum/](https://www.arts-ny.com/art-nyc-renaissance-etching-met-museum/)

**Russian:**

[^55]: Википедия. Ударная волна

**English:**

55. Wikipedia. Shockwave

**Russian:**

[^56]: Википедия. Инфразвук

**English:**

56. Wikipedia. Infrasound

**Russian:**

[^57]: ПРО ГИБЕЛЬ МАМОНТОВ (И ВСЕ ЧТО С НЕЙ СВЯЗАНО) А.М. Люхин

**English:**

57. About the death of mammoths (and everything connected with it) A.M. Lyukhin

[http://lyukhin.ru/wp-content/uploads/2014/04/%D0%9F%D1%80%D0%BE-%D0%BC%D0%B0%D0%BC%D0%BE%D0%BD%D1%82%D0%BE%D0%B2.pdf](http://lyukhin.ru/wp-content/uploads/2014/04/%D0%9F%D1%80%D0%BE-%D0%BC%D0%B0%D0%BC%D0%BE%D0%BD%D1%82%D0%BE%D0%B2.pdf)

**Russian:**

[^58]: Википедия. Баротравма

**English:**

58. Wikipedia. Barotrauma

**Russian:**

[^59]: Исследование особенностей восприятия инфразвуковых колебаний психикой человека. Студента ИП-ПС-09-1 Молчановой Дарьи Дмитриевны. Научный руководитель: доцент КОП Унарова С.Н. Якутск 2011

**English:**

59. Research of features of perception of infrasonic vibrations by human psyche. Student IP-PS-09-1 Molchanova Daria Dmitrievna. Supervisor: Associate Professor Unarova S.N. Yakutsk 2011

[http://www.decoder.ru/list/all/topic_105/](http://www.decoder.ru/list/all/topic_105/)

**Russian:**

[^60]: Повреждения от изменений барометрического давления и действия ионизирующей радиации (лекция) Колкутин В.В. --- 1996.

**English:**

60. Damage from changes in barometric pressure and action of ionizing radiation (lecture) Kolkutin V.V. - 1996.

[https://www.forens-med.ru/book.php?id=548](https://www.forens-med.ru/book.php?id=548)

**Russian:**

[^61]: BUBONIC-PLAGUE-WEATHER-HISTORY

**English:**

61. BUBONIC-PLAGUE-WEATHER-HISTORY

[https://www.almanac.com/image/bubonic-plague-weather-history](https://www.almanac.com/image/bubonic-plague-weather-history)

**Russian:**

[^62]: The Plague of Marseille 1721

**English:**

62. The Plague of Marseille 1721

[https://historic-catholic.online/index.php/category/plague-of-marseille/](https://historic-catholic.online/index.php/category/plague-of-marseille/)

**Russian:**

[^63]: Несекретные материалы. Теория всего

**English:**

63. Unclassified materials. Theory of everything

[https://topwar.ru/28971-ne-sekretnye-materialy-teoriya-vsego.html](https://topwar.ru/28971-ne-sekretnye-materialy-teoriya-vsego.html)

**Russian:**

[^64]: Перевалова Е. В. Северные ханты: этническая история. Екатеринбург: УрО РАН, 2004. 414 с.

**English:**

64. Perevalova E. V. Northern Khanty: ethnic history. Ekaterinburg: Ural Branch of RAS, 2004. 414 с.

[http://ethnobs.ru/library/publications/_aview_b18259](http://ethnobs.ru/library/publications/_aview_b18259)

**Russian:**

[^65]: Чужой техноген. Никакой мистики - только физика

**English:**

65. Alien technogen. No mysticism, just physics.

[https://oko-planet.su/phenomen/phenomenday/253136-chuzhoy-tehnogen-nikakoy-mistiki-tolko-fizika.html](https://oko-planet.su/phenomen/phenomenday/253136-chuzhoy-tehnogen-nikakoy-mistiki-tolko-fizika.html)

**Russian:**

[^66]: Understanding the Effects of Gyroscopic Motion for Shooting Accurately

**English:**

66. Understanding the Effects of Gyroscopic Motion for Shooting Accurately

[https://loadoutroom.com/thearmsguide/understanding-effects-wind-weather-shooting-accurately/](https://loadoutroom.com/thearmsguide/understanding-effects-wind-weather-shooting-accurately/)

**Russian:**

[^67]: Википедия. Ударная волна

**English:**

67. Wikipedia. Shockwave

**Russian:**

[^68]: Евразийские гидросферные катастрофы и оледенение Арктики. Автор: М. Гросвальд. Источник: Москва, «Научный мир» 1999. 5.2. Еще раз о географии и генезисе грядово-ложбинных систем

**English:**

68. Eurasian hydrospheric catastrophes and Arctic glaciation. Author: M. Grosswald. Source: Moscow, "Scientific World" 1999. 5.2. Once again on the geography and genesis of the ridge-and-lodge systems

**Russian:**

[^69]: Последнее великое оледенение территории СССР. Автор: М. Гросвальд. Источник: альманах «Науки о Земле», 10/1989. Полярные ледниковые покровы

**English:**

69. The last great glaciation of the USSR territory. Author: M. Grosswald. Source: Earth Sciences Almanac, 10/1989. Polar ice sheets

**Russian:**

[^70]: Станислав СМИРНОВ, «Тунгусский метеорит» был огненным столбом

**English:**

70. Stanislav SMIRNOV, "The Tunguska meteorite" was a pillar of fire

**Russian:**

[^71]: Что важного мы можем узнать из изучения ледяных кернов

**English:**

71. What important things we can learn from studying ice cores

[http://xn----8sbeybxdibygm.ru-an.info/%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8/%D1%87%D1%82%D0%BE-%D0%B2%D0%B0%D0%B6%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BC%D1%8B-%D0%BC%D0%BE%D0%B6%D0%B5%D0%BC-%D1%83%D0%B7%D0%BD%D0%B0%D1%82%D1%8C-%D0%B8%D0%B7-%D0%B8%D0%B7%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BB%D0%B5%D0%B4%D1%8F%D0%BD%D1%8B%D1%85-%D0%BA%D0%B5%D1%80%D0%BD%D0%BE%D0%B2/](http://xn----8sbeybxdibygm.ru-an.info/%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8/%D1%87%D1%82%D0%BE-%D0%B2%D0%B0%D0%B6%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BC%D1%8B-%D0%BC%D0%BE%D0%B6%D0%B5%D0%BC-%D1%83%D0%B7%D0%BD%D0%B0%D1%82%D1%8C-%D0%B8%D0%B7-%D0%B8%D0%B7%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BB%D0%B5%D0%B4%D1%8F%D0%BD%D1%8B%D1%85-%D0%BA%D0%B5%D1%80%D0%BD%D0%BE%D0%B2/)

**Russian:**

[^72]: Палеонтологический атлас пермских отложений Печорского угольного бассейна. Ленинград. "Наука". Ленинградское отделение. 1983 г.

**English:**

72. Paleontological atlas of Permian deposits of the Pechora coal basin. Leningrad. "Nauka. Leningrad Branch. 1983 г.

**Russian:**

[^73]: Водоворот электролита в магнитном поле

**English:**

73. Whirlpool of electrolyte in a magnetic field

[https://www.youtube.com/watch?v=ADyUsRd9lkg](https://www.youtube.com/watch?v=ADyUsRd9lkg)

**Russian:**

[^74]: Приливы и отливы - результат прецессии водоворотов. Юсуп Хизиров

**English:**

74. Tides are the result of precession of whirlpools. Yusup Khizirov

[https://ru-ru.facebook.com/facepla/posts/809845249099991](https://ru-ru.facebook.com/facepla/posts/809845249099991)

**Russian:**

[^75]: Deep-seated relict permafrost in northeastern Poland, Jan Szewczyk (e-mail: jan.szewczyk@pgi.gov.pl) and Jerzy Nawrocki, Polish Geological Institute - National Research Institute, Rakowiecka 4, 00-975 Warszawa, Poland

**English:**

75. Deep-seated relict permafrost in northeastern Poland, Jan Szewczyk (e-mail: jan.szewczyk@pgi.gov.pl) and Jerzy Nawrocki, Polish Geological Institute - National Research Institute, Rakowiecka 4, 00-975 Warszawa, Poland

[http://onlinelibrary.wiley.com/doi/10.1111/j.1502-3885.2011.00218.x/abstract](http://onlinelibrary.wiley.com/doi/10.1111/j.1502-3885.2011.00218.x/abstract)

**Russian:**

[^76]: Википедия. Добшинская ледяная пещера

**English:**

76. Wikipedia. Dobshinskaya ice cave

**Russian:**

[^77]: Википедия. Деменовская ледяная пещера

**English:**

77. Wikipedia. Demenov Ice Cave

**Russian:**

[^78]: Новые источники нефти и газа - 3. Газовые гидраты. Василий Панюшкин

**English:**

78. New sources of oil and gas - 3. Gas hydrates. Vasily Panyushkin

**Russian:**

[^79]: Электрокрекинг природного газа в производстве

**English:**

79. Electric cracking of natural gas in production

[http://chem21.info/info/405260/](http://chem21.info/info/405260/)

**Russian:**

[^80]: Вновь подтверждена гипотеза о «комете Кловис»

**English:**

80. The hypothesis of the "Clovis comet" was confirmed again

**Russian:**

http://helionews.ru/31306
(http://helionews.ru/31306

**English:**

http://helionews.ru/31306
(http://helionews.ru/31306

**Russian:**

[^81]: Практикум по органической химии. Н.Д.Прянишников

**English:**

81. Seminar on organic chemistry. N.D.Pryanishnikov

[http://www.ximicat.com/ebook.php?file=prianishnikov.djv&page=14](http://www.ximicat.com/ebook.php?file=prianishnikov.djv&page=14)

**Russian:**

[^82]: Последнее великое оледенение территории СССР. Автор: М. Гросвальд. Источник: альманах «Науки о Земле», 10/1989. Ледниковые покровы северо-востока Евразии

**English:**

82. The last great glaciation of the USSR territory. Author: M. Grosswald. Source: Earth Sciences Almanac, 10/1989. Glacial covers of northeastern Eurasia

**Russian:**

[^83]: Комментарий от 
[https://paklonnik.livejournal.com/](https://paklonnik.livejournal.com/)

**English:**

83. Commentary from
[https://paklonnik.livejournal.com/](https://paklonnik.livejournal.com/)

[https://chispa1707.livejournal.com/2512101.html](https://chispa1707.livejournal.com/2512101.html)

**Russian:**

[^84]: Маленькая Финляндия. Пустыня в Неваде

**English:**

84. Little Finland. Desert in Nevada

[http://daypic.ru/travel/46274](http://daypic.ru/travel/46274)

**Russian:**

[^85]: Вдоль хребта Мангистау

**English:**

85. Along the Mangistau ridge

[http://frantsouzov.livejournal.com/110047.html](http://frantsouzov.livejournal.com/110047.html)

**Russian:**

[^86]: Последнее великое оледенение территории СССР. Автор: М. Гросвальд, Источник: альманах «Науки о Земле», 10/1989.

**English:**

86. The last great glaciation of the USSR territory. Author: M. Grosswald, Source: Earth Sciences Almanac, 10/1989.

[http://www.ladoga-lake.ru/pages/artcl-geology-last-glacier-ussr-grosvald-08.php](http://www.ladoga-lake.ru/pages/artcl-geology-last-glacier-ussr-grosvald-08.php)

**Russian:**

[^87]: ЭСБЕ/Восстановление

**English:**

87. ESBE/Recovery

**Russian:**

[^88]: Подземные грозы и их роль в образовании нефти

**English:**

88. Underground thunderstorms and their role in oil formation

[https://neftegaz.ru/science/development/332470-podzemnye-grozy-i-ikh-rol-v-obrazovanii-nefti/](https://neftegaz.ru/science/development/332470-podzemnye-grozy-i-ikh-rol-v-obrazovanii-nefti/)

**Russian:**

[^89]: Охлаждение катода при электронной эмиссии

**English:**

89. Cathode cooling in electron emission

[https://www.chem21.info/info/593724/](https://www.chem21.info/info/593724/)

**Russian:**

[^90]: Н.А. ШПОЛЯНСКАЯ. ОСНОВНЫЕ ЗАКОНОМЕРНОСТИ РАСПРОСТРАНЕНИЯ ВЕЧНОЙ МЕРЗЛОТЫ ЗАПАДНОЙ СИБИРИ И ЭТАПЫ ЕЕ РАЗВИТИЯ

**English:**

90. N.A. SHPOLYANSKAYA. MAIN PATTERNS OF PERMAFROST DISTRIBUTION IN WESTERN SIBERIA AND STAGES OF ITS DEVELOPMENT

[http://www.evgengusev.narod.ru/antropogen/shpolyanskaya-1971.html](http://www.evgengusev.narod.ru/antropogen/shpolyanskaya-1971.html)

**Russian:**

[^91]: Источник: http://earth-chronicles.ru/news/2012-06-26-25543

**English:**

91. Источник: http://earth-chronicles.ru/news/2012-06-26-25543

**Russian:**

[^92]: С. Григорьев, М. Емцев. Скульптор лика земного. Срединно-океанические хребты и дрейф континентов. © Зооинженерный факультет МСХА

**English:**

92. С. Grigoryev, M. Emtsev. Sculptor of the Face of the Earth. Mid-oceanic ridges and continental drift. © Zooengineering Faculty of MSHA

[https://www.activestudy.info/sredinno-okeanicheskie-xrebty-i-drejf-kontinentov/](https://www.activestudy.info/sredinno-okeanicheskie-xrebty-i-drejf-kontinentov/)

**Russian:**

[^93]: Сдвиги внутри революции

**English:**

93. Shifts within the Revolution

[https://chispa1707.livejournal.com/2367922.html](https://chispa1707.livejournal.com/2367922.html)

**Russian:**

[^94]: Борис. Бондарчик. Простое и неоспоримое доказательство смещения земной оси в начале XIX века (не позднее 1831 года)

**English:**

94. Boris. Bondarchik. Simple and indisputable proof of displacement of the Earth's axis in the beginning of XIX century (not later than 1831)

[https://nu-nu.ru/share/2/1244/088fb57db7da47c7f02ac022c7e00a06.html](https://nu-nu.ru/share/2/1244/088fb57db7da47c7f02ac022c7e00a06.html)

**Russian:**

[^95]: Борис Бондарчик. Карточные фокусы начала XIX века и смещение земной оси

**English:**

95. Boris Bondarchik. Card tricks of the early 19th century and the shift of the Earth's axis

[https://nu-nu.ru/share/2/1449/b63d75a70802f765a2959cd75c9f2e58.html](https://nu-nu.ru/share/2/1449/b63d75a70802f765a2959cd75c9f2e58.html)

**Russian:**

[^96]: Инициативный проект РФФИ 02-05-64428. «Катастрофические позднечетвертичные морские трансгрессии и их связь с эпохами экстремально высокого речного стока в бассейнах Понто-Каспия».

**English:**

96. RFBR Initiative Project 02-05-64428. "Catastrophic Late Quaternary marine transgressions and their relation to epochs of extremely high river flow in Ponto-Caspian basins".

[https://www.rfbr.ru/rffi/ru/project_search/o_216223](https://www.rfbr.ru/rffi/ru/project_search/o_216223)

**Russian:**

[^97]: Океаническая вода в Каспийском море

**English:**

97. Ocean water in the Caspian Sea

[https://it4history.livejournal.com/20696.html](https://it4history.livejournal.com/20696.html)

**Russian:**

[^98]: Г.А. Разумов, М.Ф. Хасин Тонущие города © Наука, 1978 © Разумов Г.А., Хасин М.Ф., 1991, с изменениями

**English:**

98. G.A. Razumov, M.F. Khasin Sinking Cities © Nauka, 1978 © Razumov G.A., Khasin M.F., 1991, with modifications

[http://www.bibliotekar.ru/goroda/index.htm](http://www.bibliotekar.ru/goroda/index.htm)

**Russian:**

[^99]: Геологи рассказали об истории "скрытого" подводного континента

**English:**

99. Geologists told the history of the "hidden" underwater continent

[https://ria.ru/20200212/1564619791.html](https://ria.ru/20200212/1564619791.html)

**Russian:**

[^100]: Википедия. Антарктида

**English:**

100. Wikipedia. Antarctica

**Russian:**

[^101]: Википедия. Тюменка (река на Северном Кавказе)

**English:**

101. Wikipedia. Tyumenka (river in the North Caucasus)

**Russian:**

[^102]: Книга Большому Чертежу» (1627 г.)

**English:**

102. The Book of the Great Drawing" (1627)

**Russian:**

[^103]: Новый и полный географическій словарь Россійскаго государства, или лексикон... Авторы: Өедор Аөанасьевич Полунин, Герард Фридрих Миллер, 1788 г.

**English:**

103. New and Complete Geographical Dictionary of the Russian State, or Lexicon... Authors: Өedor A. Polunin, Gerard Friedrich Miller, 1788.

**Russian:**

[^104]: Карта Каспийского моря Жерара ван Кёлена. 1720 г.

**English:**

104. Map of the Caspian Sea by Gerard van Keulen. 1720 г.

[http://www.atyrau-city.kz/img_encyclopedia/1720_Map_of_Caspian_sea_by_Gerard_van_Keulen.pdf](http://www.atyrau-city.kz/img_encyclopedia/1720_Map_of_Caspian_sea_by_Gerard_van_Keulen.pdf)

**Russian:**

[^105]: https://en.wikipedia.org/wiki/St._Augustine,_Florida

**English:**

105. https://en.wikipedia.org/wiki/St._Augustine,_Florida

**Russian:**

[^106]: Всемирная иллюстрация, 1874

**English:**

106. World Illustration, 1874

[http://bskamalov.livejournal.com/4179920.html](http://bskamalov.livejournal.com/4179920.html)

**Russian:**

[^107]: Всемирная иллюстрация, № 183, стр. 14.

**English:**

107. World Illustration, No. 183, p. 14.

**Russian:**

[^108]: Е. К. МЕЙЕНДОРФ Путешествие из Оренбурга в Бухару. Книга вторая. Заметки о ханствах, соседних с Бухарией

**English:**

108. Е. K. MEIENDORF Journey from Orenburg to Bukhara. Book Two. Notes on Khanates Neighboring Bukhara

**Russian:**

[^109]: Плавающие сады в Мехико

**English:**

109. Floating gardens in Mexico City

[http://method-estate.com/archives/1942](http://method-estate.com/archives/1942)

**Russian:**

[^110]: Супотницкий Михаил Васильевич, Супотницкая Надежда Семеновна. Очерки истории чумы. Очерк V. «Черная Смерть» --- второе пришествие чумы в Европу (1346---1351)

**English:**

110. Supotnitskyi Mikhail Vasilyevich, Supotnitskaya Nadezhda Semyonovna. Essays on the history of plague. Sketch V. *The Black Death - The Second Coming of the Plague to Europe (1346---1351)*

[http://www.supotnitskiy.ru/book/book3-5.htm](http://www.supotnitskiy.ru/book/book3-5.htm)

**Russian:**

[^111]: Карта продвижения фитофтороза в 1845 году

**English:**

111. Map of Phytophthora progression in 1845

[https://imgur.com/a/lYnW1Jg](https://imgur.com/a/lYnW1Jg)

**Russian:**

[^112]: Documents inedits sur la grand peste de 1348. Paris, Londres et New-York, 1860

**English:**

112. Documents inedits sur la grand peste de 1348. Paris, Londres and New-York, 1860

**Russian:**

[^113]: А. Л. Чижевский «Земля в объятиях Солнца»

**English:**

113. A. L. Chizhevsky "Earth in the Embrace of the Sun

**Russian:**

[^114]: [https://en.m.wikipedia.org/wiki/Convulsionnaires_of_Saint-Médard](https://en.m.wikipedia.org/wiki/Convulsionnaires_of_Saint-M%C3%A9dard)

**English:**

114. [https://en.m.wikipedia.org/wiki/Convulsionnaires_of_Saint-Médard](https://en.m.wikipedia.org/wiki/Convulsionnaires_of_Saint-M%C3%A9dard)

**Russian:**

[^115]: История одомашнивания флоры и фауны

**English:**

115. History of domestication of flora and fauna

[http://www.garshin.ru/history/archeology/agricultural-origin/index.html](http://www.garshin.ru/history/archeology/agricultural-origin/index.html)

**Russian:**

[^116]: Википедия. История сельского хозяйства

**English:**

116. Wikipedia. History of agriculture

**Russian:**

[^117]: Люшер А. Французское общество времен Филиппа-Августа

**English:**

117. Lusher A. French society at the time of Philippe-Auguste

**Russian:**

[^118]: Википедия. Массовое вымирание

**English:**

118. Wikipedia. Mass extinction

**Russian:**

[^119]: Википедия. Ледниковый период

**English:**

119. Wikipedia. Ice Age

**Russian:**

[^120]: Википедия. Лавразия

**English:**

120. Wikipedia. Lavrasia

**Russian:**

[^121]: Википедия. Пангея

**English:**

121. Wikipedia. Pangea

**Russian:**

[^122]: Википедия. Бореальный период

**English:**

122. Wikipedia. Boreal period

**Russian:**

[^123]: Фундаментальные эксперименты по стратиграфии [https://www.youtube.com/watch?time_continue=5&v=fQSm0kk_DwY&feature=emb_title](https://www.youtube.com/watch?time_continue=5&v=fQSm0kk_DwY&feature=emb_title)

**English:**

123. Fundamental experiments on stratigraphy [https://www.youtube.com/watch?time_continue=5&v=fQSm0kk_DwY&feature=emb_title](https://www.youtube.com/watch?time_continue=5&v=fQSm0kk_DwY&feature=emb_title)

**Russian:**

[^124]: Анализ основных принципов стратиграфии на основе экспериментальных данных. Новый подход: палеогидродинамика. Ги Берто, почетный член международной ассоциации седиментологов

[https://vimeo.com/16313169](https://vimeo.com/16313169)

[www.sedimentology.fr](http://www.sedimentology.fr/)

**English:**

124. Analysis of the basic principles of stratigraphy based on experimental data. A new approach: paleohydrodynamics. Guy Berthaud, Honorary Member of the International Association of Sedimentologists

[https://vimeo.com/16313169](https://vimeo.com/16313169)

[www.sedimentology.fr](http://www.sedimentology.fr/)

**Russian:**

[^125]: нодозавриды -- Nodosauridae

**English:**

125. nodosaurids -- Nodosauridae

[https://ru.qwe.wiki/wiki/Nodosauridae](https://ru.qwe.wiki/wiki/Nodosauridae)

**Russian:**

[^126]: Три вещи о Луне, о которых мы не знали

**English:**

126. Three things about the Moon we didn't know

[https://www.pravda.ru/science/1228659-moon/](https://www.pravda.ru/science/1228659-moon/)

**Russian:**

[^127]: Sibved. «Пластилиновая» гора Эль-Фуэрте в Боливии

**English:**

127. Sibved. The "plasticine" mountain of El Fuerte in Bolivia

[https://zen.yandex.ru/media/sibved24/plastilinovaia-gora-elfuerte-v-bolivii-5d8cbb8f74f1bc00adad5e1c](https://zen.yandex.ru/media/sibved24/plastilinovaia-gora-elfuerte-v-bolivii-5d8cbb8f74f1bc00adad5e1c)

**Russian:**

[^128]: [fantastron1](https://fantastron1.livejournal.com/). Другие курганы. Совсем другие.

**English:**

128. [fantastron1](https://fantastron1.livejournal.com/). Different mounds. Very different.

[https://chispa1707.livejournal.com/2912924.html](https://chispa1707.livejournal.com/2912924.html)

**Russian:**

[^129]: Приказ Министерства культуры Ставропольского края от 15 мая 2006 года N 56 «Об утверждении списка выявленных объектов культурного наследия Ставропольского края» [http://docs.cntd.ru/document/444814454](http://docs.cntd.ru/document/444814454)
**English:**

129. Order of the Ministry of Culture of the Stavropol Territory of 15 May 2006 N 56 *On approval of the list of identified objects of cultural heritage of the Stavropol Territory*.  [http://docs.cntd.ru/document/444814454](http://docs.cntd.ru/document/444814454)

**Russian:**

[^130]: Комментарий от [Валера Полевой](https://vk.com/id9603461)

**English:**

130. Comment from [Valera Polevoy](https://vk.com/id9603461)

[https://chispa1707.livejournal.com/402889.html](https://chispa1707.livejournal.com/402889.html)

**Russian:**

[^131]: Хелен Бриггс, Би-би-си, Ученые нашли белок в костях динозавра возрастом 200 млн лет

**English:**

131. Helen Briggs, BBC, Scientists find protein in 200 million-year-old dinosaur bones

[https://www.bbc.com/russian/other-news-38817261](https://www.bbc.com/russian/other-news-38817261)

**Russian:**

[^132]: Александр Храмов/Infox.ru. Ученые выделили белок из кости динозавра возрастом 195 млн лет

**English:**

132. Alexander Khramov/Infox.ru. Scientists isolate protein from 195 million year old dinosaur bone

[https://webcache.googleusercontent.com/search?q=cache:hVZGOFzJJT4J:https://www.infox.ru/news/232/171129-ucenye-vydelili-belok-iz-kosti-dinozavra-vozrastom-195-mln-let+&cd=9&hl=ru&ct=clnk&gl=ru](https://webcache.googleusercontent.com/search?q=cache:hVZGOFzJJT4J:https://www.infox.ru/news/232/171129-ucenye-vydelili-belok-iz-kosti-dinozavra-vozrastom-195-mln-let+&cd=9&hl=ru&ct=clnk&gl=ru)

**Russian:**

[^133]: Великое переселение народов

**English:**

133. The Great Migration of Peoples

[https://scan1707.blogspot.com/2019/10/blog-post_17.html](https://scan1707.blogspot.com/2019/10/blog-post_17.html)

**Russian:**

[^134]: Beth Weinberger, Lydia H. Greiner, Leslie Walleigh and David Browna. Health symptoms in residents living near shale gas activity: A retrospective record review from the Environmental Health Project

**English:**

134. Beth Weinberger, Lydia H. Greiner, Leslie Walleigh and David Browna. Health symptoms in residents living near shale gas activity: A retrospective record review from the Environmental Health Project

[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5633856/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5633856/)

**Russian:**

[^136]: Комментарий от muha_vchocolate

**English:**

136. Comment from muha_vchocolate

[https://chispa1707.livejournal.com/3282919.html](https://chispa1707.livejournal.com/3282919.html)

**Russian:**

[^137]: Комментарий от muha_vchocolate

**English:**

137. Comment from muha_vchocolate

[https://chispa1707.livejournal.com/3282919.html?thread=38847975#t38847975](https://chispa1707.livejournal.com/3282919.html?thread=38847975#t38847975)

**Russian:**

138. Названа причина смещения северного магнитного полюса из Канады в Россию

**English:**

[^138]: The cause of the north magnetic pole shift from Canada to Russia has been named

[https://vz.ru/news/2020/5/15/1039687.html](https://vz.ru/news/2020/5/15/1039687.html)

**Russian:**

139. Труды Арало-Каспийской экспедиции издаваемые под редакцией О. А. Гримма. С.-Петербург 1875.

**English:**

[^139]: Proceedings of the Aral-Caspian Expedition, edited by O.A. Grimm. St. Petersburg 1875.

[https://rusneb.ru/catalog/000199_000009_003545752/](https://rusneb.ru/catalog/000199_000009_003545752/)

**Russian:**

140. Михаил Алексеев. Хвосты из будущего. Военно-промышленный курьер № 6 (670) 15-21 февраля 2017 года.

**English:**

[^140]: Mikhail Alexeev. Tails from the future. Military Industrial Courier No. 6 (670) February 15-21, 2017.

[http://www.vesvks.ru/public/flippingbook/vpk62017/20/assets/basic-html/index.html#10](http://www.vesvks.ru/public/flippingbook/vpk62017/20/assets/basic-html/index.html#10)

**Russian:**

141. Алексей Архипов. Молнии - разрушители цивилизаций. Комсомольская правда 6 мая 2017 года.

**English:**

[^141]: Alexei Arkhipov. Lightning is the destroyer of civilizations. Komsomolskaya Pravda May 6, 2017.

[https://www.penza.kp.ru/daily/26676.7/3698193/](https://www.penza.kp.ru/daily/26676.7/3698193/)

**Russian:**

142. Burned house horizon

**English:**

142. Burned house horizon

[https://en.wikipedia.org/wiki/Burned_house_horizon](https://en.wikipedia.org/wiki/Burned_house_horizon)

**Russian:**

143. Crossing the ice: an Iron Age to medieval mountain pass at Lendbreen, Norway. Lars Pilø1, Espen Finstad1 & James H. Barrett

**English:**

[^143]: Crossing the ice: an Iron Age to medieval mountain pass at Lendbreen, Norway. Lars Pilø1, Espen Finstad1 & James H. Barrett

[https://www.cambridge.org/core/services/aop-cambridge-core/content/view/F6C3FDBC94AD652EF4D2E79ED1697F1A/S0003598X20000022a.pdf/crossing_the_ice_an_iron_age_to_medieval_mountain_pass_at_lendbreen_norway.pdf](https://www.cambridge.org/core/services/aop-cambridge-core/content/view/F6C3FDBC94AD652EF4D2E79ED1697F1A/S0003598X20000022a.pdf/crossing_the_ice_an_iron_age_to_medieval_mountain_pass_at_lendbreen_norway.pdf)

**Russian:**

144. Evidence of Cosmic Impact at Abu Hureyra, Syria at the Younger Dryas Onset ( 12.8 ka): High-temperature melting at >2200 °C

**English:**

144. Evidence of Cosmic Impact at Abu Hureyra, Syria at the Younger Dryas Onset ( 12.8 ka): High-temperature melting at >2200 °C

[https://ui.adsabs.harvard.edu/abs/2020NatSR..10.4185M/abstract](https://ui.adsabs.harvard.edu/abs/2020NatSR..10.4185M/abstract)

**Russian:**

[^145]: Н. И. Макавеев, К. Г. Тихоцкий.

**English:**

145. N. I. Makaveev, K. G. Tikhotsky.

[https://www.booksite.ru/fulltext/1/001/008/106/492.htm](https://www.booksite.ru/fulltext/1/001/008/106/492.htm)

**Russian:**

[^146]: Сборник карт Российской империи конца XVIII в.

**English:**

146. Collection of maps of the Russian Empire in the late 18th century.

[http://dkhramov.dp.ua/Art.RussiaEndOfXVIII](http://dkhramov.dp.ua/Art.RussiaEndOfXVIII)

[Posted by Andrew](https://www.blogger.com/profile/11838398754796181212 "author profile") at [06:01](https://chispa1707.blogspot.com/2020/10/complete-history-of-catastrophic-crash.html "permanent link")

© All rights reserved. The original author retains ownership and rights.

