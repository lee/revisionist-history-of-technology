title: gorojanin_iz_b's Forged History articles - Google-translated
date: 2021-01-13
modified: 2022-10-20 12:00:46
category:
tags: forged history; links
slug:
authors: Anton Sizykh
from: https://gorojanin-iz-b.livejournal.com/89184.html
originally: 89184.html
local: 89184_files
summary: It's about time we made a list post for material on history-fabrication and construction of 'historical artifacts'. Covering: Mysterious Civilizations of Pre-Columbian America. Mysteries and Unravellings. Part 1. The inscrutable Sacsayhuaman Part 2. The Mad Monuments of Cuzco Part 3. Delirium of the Architect of Tiwanaku Part 4. Ancient..."
status: published

#### Translated from:

[https://gorojanin-iz-b.livejournal.com/89184.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/89184.html)

**Russian:**

Материалы по созданию истории

**English:**

Materials for fabricating history

**Russian:**

Давно пора сделать списочный пост для материалов о создании истории и постройке памятников истории.

**English:**

It's about time we did a list post of history-fabrication and history-creating articles.

**Russian:**

Делаю:

**English:**

I cover:

**Russian:**

Таинственные цивилизации доколумбовой Америки. Загадки и разгадки.

**English:**

The mysterious civilizations of pre-Columbian America. Riddles and clues.

**Russian:**

- [Часть 1. Непостижимый Саксайуаман](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83925.html)

**English:**

- [Part 1. The inscrutable Saxayhuaman](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83925.html)


**Russian:**

- [Часть 2. Безумные памятники Куско](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84097.html)

**English:**

- [Part 2. Crazy Monuments of Cusco](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84097.html)

**Russian:**

- [Часть 3. Делирий архитектора Тиуанако](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84385.html)

**English:**

- [Part 3. The Architect Tiwanaku's Delirium](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84385.html)

**Russian:**

- [Часть 4. Древние города майя, говорите...](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84666.html)

**English:**

- [Part 4. Ancient Mayan cities, say...](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/84666.html)


**Russian:**

Египетский морок.

**English:**

An Egyptian morass.

**Russian:**

- [Часть 1. Достройка колоссов, Сфинкса, пирамид на заре эры фотографий в 19 веке](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80010.html)

**English:**

- [Part 1. Completion of the Colossi, Sphinx, Pyramids at the dawn of the era of photography in the 19th century](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80010.html)

**Russian:**

- [Часть 2. Достройка пирамид на заре эры фотографий в 19 веке](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://uctopuockon-pyc.livejournal.com/3404728.html)

**English:**

- [Part 2. Completion of the pyramids at the dawn of the era of photography in the 19th century](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://uctopuockon-pyc.livejournal.com/3404728.html)

**Russian:**

- [Часть 3. Египтологи на защите древнегипетских реликвий. Асуанский обелиск](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80593.html)

**English:**

- [Part 3. Egyptologists on the protection of ancient Egyptian relics. Aswan obelisk](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80593.html)

**Russian:**

- [Часть 4. В египетскую кампанию Наполеона Великие пирамиды еще строились](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83036.html)

**English:**

- [Part 4. During Napoleon's Egyptian campaign the Great Pyramids were still under construction](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83036.html)

**Russian:**

- [Часть 5. Старейшие памятники Египта. Когда достраивался Абу-Симбел?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/86656.html)

**English:**

- [Part 5. Egypt's oldest monuments. When was Abu Simbel completed?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/86656.html)

**Russian:**

- [Часть 6. Строители древнего Египта](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83445.html)

**English:**

- [Part 6. Builders of ancient Egypt](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83445.html)

**Russian:**

- [Часть 7. Лагерь строителей древнего Египта](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/87047.html)

**English:**

- [Part 7. Ancient Egyptian builders' camp](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/87047.html)


**Russian:**

- [Документ о доисторической Москве, уцелевший по недосмотру](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://911tm.livejournal.com/26786.html)

**English:**

Miscellaneous

- [Document about prehistoric Moscow, survived by oversight](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://911tm.livejournal.com/26786.html)

**Russian:**

- [Наделение историей. Сказки Александрийского столпа](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80878.html)

**English:**

- [Empowering History. Tales of the Alexander Column](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/80878.html)

**Russian:**

- [Эллинистический Гиппос, строительство Пальмиры, немного древнего Египта](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/81937.html)

**English:**

- [Hellenistic Hippos, construction of Palmyra, a little bit of ancient Egypt](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/81937.html)

**Russian:**

- [Липовые лики богов на нью-йоркском ломе](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83490.html)

**English:**

- [Fake Faces of the Gods on New York Scrap](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/83490.html)

**Russian:**

- [Идиоты из труппы ИГИЛ рушат официальную историю](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/75511.html)

**English:**

- [ISIS troupe idiots ruin official history](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/75511.html)

**Russian:**

- [Клинические идиоты из труппы ИГИЛ продолжают рушить официальную историю](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/75580.html)

**English:**

- [The clinical idiots in the ISIS troupe continue to ruin the official story](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/75580.html)

**Russian:**

- [Японский средневековый замок оказался пустой древностью](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://elemental1111.livejournal.com/44911.html)

**English:**

- [Japanese medieval castle turned out to be an empty antiquity](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://elemental1111.livejournal.com/44911.html)

**Russian:**

- [Античный Линдос, затопленный Долихисте и другие постройки создателей истории](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/82785.html)

**English:**

- [Antique Lindos, the flooded Dolihiste and other constructions of history makers](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/82785.html)

**Russian:**

- [Открытие академиком Фоменко Тартарии в 2000 году](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/#entry-boeing_is_back-721642)

**English:**

- [Fomenko's discovery of Tartaria in 2000](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/#entry-boeing_is_back-721642)
<!-- Local
- [Fomenko's discovery of Tartaria in 2000](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://boeing-is-back.livejournal.com/721642.htm)
-->

**Russian:**

- [Строительство Стоунхенджа с нулевого цикла. Кинo- и фотодокументы 1949-58 гг](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/64046.html)

**English:**

- [Construction of Stonehenge from ground zero. Film and photographic documentation 1949-58](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/64046.html)

**Russian:**

- [Великая загадка Великой Стены](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/88583.html)

**English:**

- [The Great Mystery of the Great Wall](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/88583.html)

**Russian:**

- [Главная тайна истуканов острова Пасхи](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/88859.html)

**English:**

- [The main mystery of the Easter Island icons](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/88859.html)

**Russian:**

И несколько слов по вопросам, на которые приходится отвечать десятки раз в обсуждении каждого из постов

**English:**

And a few words on questions that have to be answered dozens of times in the comments at the end of each of the posts...

**Russian:**

Памятники истории делаются не для туризма, не для амбиций каких-нибудь местных элит, не для геополитики. Памятники истории делаются для создания истории. Сначала топовые исторические объекты создаются в литературе, словесности, текстах культа, затем воплощаются в натуре.

**English:**

Historical monuments are not built for tourism, nor for the ambitions of some local elites, nor for geopolitics. Historical monuments are built for the creation of history. First, the top historical objects are created in literature, cult texts, and then they are embodied in nature, ie in the real world.

**Russian:**

Создатели истории - вне государств, но все государства и все их ведомства поддерживают проекты.

**English:**

History makers are outside the states, but all states and all their agencies support the projects.

**Russian:**

Основной метод постройки памятников истории, разумеется, формовка твердеющей смеси с опалубным (строительным) грунтом. Минимально технологичный, не требующий строительной техники, доступный для ограниченной группы людей практически везде. Мега-памятники истории, сделанные этим методом, требуют в сотни и тысячи крат меньше ресурсов, логистики, трудозатрат, нежели все методы академических и альтернативных историков. Подробнее всего он показан в египетском цикле.

**English:**

The main method for building historical monuments, of course, is moulding a curing mix in a formwork (construction) on the ground. Minimally technological, requiring no construction machinery, accessible to a limited group of people almost everywhere. The mega historical monuments made by this method require hundreds and thousands of times less resources, logistics, labor costs than all the methods of academic and alternative historians. This is shown in detail in the Egyptian cycle.

**Russian:**

История церкви длиннее светской; в истоке и все государства с его институтами, и виды искусств вышли из церкви (за неимением другого термина использую этот, хотя суть его со времен доистории сильно изменилась).

**English:**

The history of the church is longer than secular history. All states and their institutions and arts begin in the church (for lack of another term I use 'church', although its essence has changed a lot since prehistory).

**Russian:**

Подлинная история с подложной предыдущей начиналась в разных регионах в разное время: в Европе в основном примерно с Вестфальского мира, в России примерно с петровского рубежа, в Азии история короче. История словесная, г.о. в виде церковных текстов - немногим длинее.

**English:**

Genuine history with faked previous history began in different regions at different times: in Europe mainly from about the Peace of Westphalia, in Russia from about the time of Peter the Great, in Asia history is shorter. The verbal history, g.o. in the form of ecclesiastical texts, is a little longer.

**Russian:**

О доистрии в списке пока только один пост - "Документ о доисторической Москве".

**English:**

There is only one post about prehistory on the list so far: "[A document about prehistoric Moscow](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://911tm.livejournal.com/26786.html)".

See more about this document and its implications at [Mystery Diary](https://www.liveinternet.ru/users/4489288/) *(Russian original)*, *([English translation](https://www-liveinternet-ru.translate.goog/users/4489288/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)*.

**Russian:**

Примерно такой же объем информации, как в постах, рассыпан в комментариях в моем и других журналах, в других ресурсах. Потихоньку буду собирать их и другую информацию в посты.

**English:**

Approximately the same amount of information as there is in the posts is scattered in the comments sections of my other journals, and in other resources. Slowly I will collect them, and other information, into posts.

[Link to Google-translated English version of this file](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin-iz-b.livejournal.com/89184.html)


**Russian:**

Вынужден ставить это

**English:**

Forced to put this:

**Russian:**

*Сейчас многие блогеры собирают донаты. Воздерживался, но приходится обращаться. Кто считает, что эта работа заслуживает какой-то оплаты - буду признателен. Карта Сбербанка 4276020014219544. ЮMoney (Яндекс-кошелек) 410013946572302.

**English:**

Now a lot of bloggers are collecting donations. Refrained, but have to apply. Who thinks that this work deserves some kind of payment - would be grateful. Sberbank card 4276020014219544. YuMopeu (Yandex wallet) 410013946572302.

© All rights reserved. The original author retains ownership and rights.


