title: Inspired by "And There Was a Country Before Russia" - Part 10
date: 2013-12-22
modified: Fri 02 Apr 2021 20:09:02 BST
category:
tags: cannons; technology
slug:
authors: Vladimir Mamzerev
summary: Returning to the topic of transmitters that once covered a fairly large area. We're looking at the Tsar Cannon again.
status: published
local: Inspired by And There Was a Country Before Russia - Part 10_files
originally: По мотивам "И была Страна до России". Часть 10. - pro_vladimir — LiveJournal.html

### Translated from:

[https://pro-vladimir.livejournal.com/8801.html](https://pro-vladimir.livejournal.com/8801.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

**Russian:**

По мотивам [«И была Страна до России»](http://dmitrijan.livejournal.com/71535.html) and [«Иванами да Марьями»](http://dmitrijan.livejournal.com/91508.html)

**English:**

Based on [And there was a Country Before Russia](http://dmitrijan.livejournal.com/71535.html) and [Ivan and Mary](http://dmitrijan.livejournal.com/91508.html)

**Russian:**

Возвращаясь к теме излучателей, коими некогда была покрыта достаточно обширная территория. Снова смотрим на Царь-пушку.

**English:**

Returning to the subject of the transmitters, which once covered a fairly large area. We're looking at the Tsar Cannon again.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/h-4.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/h-4.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-4.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-4.jpg)

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/i-10.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/i-10.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/i-10.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/i-10.jpg)

**Russian:**

Установлена эта конструкция вся была в колокольне Ивана великого.

**English:**

This construction was installed in the bell tower of Ivan the Great.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Wsx_fMRTOjy6Bjd0kuk_iaXFmDLHwmuz5KD64kpw.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Wsx_fMRTOjy6Bjd0kuk_iaXFmDLHwmuz5KD64kpw.jpeg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-11.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-11.jpg)

**Russian:**

Достаточно грубо, но примерно как-то так. Как там всё это было закреплено мы конечно нынче можем только гадать, а возможно и подробности всплывут, чуть позже, как не раз это уже было. Обращает на себя внимание еще и то, что в куполах колокольни имеются подвесы. С помощью их подвешивался и менял свое положение по высоте шарик державный. Если присмотреться, к каморе казенной части Царь-пушки, то в её "дне" имеется своеобразный выступ. скорее всего исполнял роль направляющего для внутреннего кондуктора, на коем была обмотка.

**English:**

It's pretty rough, but something like that. How was it fixed there? Of course, nowadays we can only guess, and perhaps the details will come out later, just as some already have. It is also noteworthy that the domes of the bell tower have hangers. The 'ball of power' hung and changed height and position with their help. If you look closely at the breech at the "treasury" end of the Tsar Cannon, there is a peculiar projection in its "bottom". Most likely it acted as a guide for the inner conductor, which had a winding on it.

**Russian:**

На образец кондуктора может претендовать вот сей образе пищали.

**English:**

The conductor sample can be identified as a 'beeper'.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/0_7732e_7147dbbf_XL.asc)](Inspired by And There Was a Country Before Russia - Part 10_files/0_7732e_7147dbbf_XL.asc)
-->


![Image no longer available](http://img-fotki.yandex.ru/get/6101/73911542.56/0_7732e_7147dbbf_XL)


**Russian:**

Вот на этом вот наматывался провод. Конусность тоже не просто так. Да и все обмеры Царь пушки показали, что, что её ствол, что камора казенной части, имеют определенную Конусность. Ствол пищали, а он же в номинале изначально кондуктор, своим "выходным" отверстием базировался на том направляющем выступе, что имеет в казенной части Царь-пушки. Вообще, конструкция достаточно сильно напоминает сабвуфер. Тут попалось очередное стилизованное изображение работы Грааля: [http://gorojanin-iz-b.livejournal.com/8109.html](http://gorojanin-iz-b.livejournal.com/8109.html)

**English:**

This is where the wire was wrapped. Cone for a reason, too. And all the measurements of the Tsar Cannon showed that her barrel, that of the treasury chambers, have a certain connoisseurship. The barrel was squeaking, and it was originally a conductor in nominal value, and its "exit" hole was based on the guide barrel, which has the Tsar Cannon in the 'treasury'. Actually, the design is pretty much like a subwoofer. Here we have another stylized image of the Grail work: [http://gorojanin-iz-b.livejournal.com/8109.html](http://gorojanin-iz-b.livejournal.com/8109.html).


<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/0_57d84_da42857f_L.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/0_57d84_da42857f_L.jpeg)
-->

[![](http://img-fotki.yandex.ru/get/6102/131192166.8/0_57d84_da42857f_L)](http://img-fotki.yandex.ru/get/6102/131192166.8/0_57d84_da42857f_L)


**Russian:**

В правой части рисунка угадывается диодный мост.

**English:**

You can guess that on the right side of the drawing is a diode bridge.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws04-W3j4QinD3ckptOsyV09KEBWB14k6MhnXFwJA.bin)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws04-W3j4QinD3ckptOsyV09KEBWB14k6MhnXFwJA.bin)
-->

[![](http://temple_architecture.academic.ru/pictures/temple_architecture/058_0004703b.jpg#clickable)](http://temple_architecture.academic.ru/pictures/temple_architecture/058_0004703b.jpg)

**Russian:**

А тут прям оплетка, как у коаксиального кабеля.

**English:**

The frame looks like a coaxial cable.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/54789_original.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/54789_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mntc/33729660/54789/54789_original.jpg#clickable)](http://ic.pics.livejournal.com/mntc/33729660/54789/54789_original.jpg)

**Russian:**

Нам , в свете всего ранее изученного по сей тематике наиболее интересен вариант конденсатора "г" это отсюда: [http://mntc.livejournal.com/12555.html](http://mntc.livejournal.com/12555.html)

**English:**

We, in the light of previous study of this subject, are most interested in the "d" version of the capacitor discussed here: [http://mntc.livejournal.com/12555.html](http://mntc.livejournal.com/12555.html)

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws-P9J-_EnFOrl2XkMRXy-dtrUQhArpklt7Gqn-z.html)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws-P9J-_EnFOrl2XkMRXy-dtrUQhArpklt7Gqn-z.html)
-->

<!--
[![http://venividi.ru/files/img1/2782/66.jpg image no longer available](http://venividi.ru/files/img1/2782/66.jpg#clickable)](http://venividi.ru/files/img1/2782/66.jpg)
-->

[![](http://ic.pics.livejournal.com/mntc/33729660/54789/54789_original.jpg#clickable)](http://ic.pics.livejournal.com/mntc/33729660/54789/54789_original.jpg)

**Russian:**

По фасаду в изобилии выступают окончания армирующих, как принято считать, стальных связей. строители и сейчас, если требуется повысить несущую способность, армируют кладку. Но тут? как-то не повсеместно, с каким-то вроде и повторяющимся, но не везде шагом. и оканчиваются выступом с проушиной с вбитым в неё клином. это еще зачем? ну заармировал ты строение, зачем окончание арматуры выводить наружу да еще с таким клином? чтоб ржавело лучше? а может чтоб можно было запитать чего? лампочку повесить? причем, они как-то далеко не все наружу выглядывают, какие-то сопрятаны всё же в стене.

**English:**

The ends of apparent reinforcing rods protrude in abundance from the facade. It is generally assumed, they are steel anchor bonds. But here? Not everywhere, with some sort of repetitive step. And they end with a ledge with an eye into which a wedge has been hammered. Why? After all, you reinforced the structure, why bring out the end of the reinforcement, and with such a wedge? To make it rust more? To hang a light bulb on them? Or maybe to feed it with something? They don't all stick out, some of them terminate within the wall. 

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws_d4RVg7XPnytiw-EOUNLwUbCGiu4mDGpm9Tr-a.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws_d4RVg7XPnytiw-EOUNLwUbCGiu4mDGpm9Tr-a.jpeg)
-->

[![](http://www.sobory.ru/pic/00200/00214_20060116_130359.jpg#clickable)](http://www.sobory.ru/pic/00200/00214_20060116_130359.jpg)


**Russian:**

Великолепный образец. смотрим на число выступающих шин. Их аж целых шесть штук на данном фото имеется. четыре светятся на колонне схемы архивольта по периметру. Два же контакта выступают в стороне от стены. Они ниже этих двух "телевизоров".

**English:**

Magnificent specimen. Looking at the number of protruding tires. There are as many as six of them in this photo. Four shine on the perimeter column of the archivolt circuit. Two of the contacts protrude away from the wall. They're below these two "TVs."

**Russian:**

И что характерно, этот храм знают абсолютно все, как и Царь-пушку. Даже если кто и не знает, гарантированно видел его изображение, причем раз несколько на дню.

**English:**

And, characteristically, this temple is known to absolutely everyone, along with the Tsar Cannon. Even if someone does not know it, they are guaranteed to see its image once or twice a day.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws36nqNB3qmixjnQacWYP5ec867G9MP-aA9Cc4q2.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Ws36nqNB3qmixjnQacWYP5ec867G9MP-aA9Cc4q2.jpeg)
-->

[![](http://sobory.ru/pic/00200/00214_20120312_163748.jpg#clickable)](http://sobory.ru/pic/00200/00214_20120312_163748.jpg)


<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Wszl8bAHmYpcKWUeKh84XXXaewWBmyNu0CH1CNae.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 10_files/zhhCDtPbrg12ewQCVg7Wszl8bAHmYpcKWUeKh84XXXaewWBmyNu0CH1CNae.jpeg)
-->

[![](http://bankirsha.com/files/pic/1000-1997-2.jpg#clickable)](http://bankirsha.com/files/pic/1000-1997-2.jpg)

[http://sobory.ru/article/?object=00214](http://sobory.ru/article/?object=00214)

**Russian:**

Ярославль. Церковь Усекновения главы Иоанна Предтечи в Толчкове

**English:**

Yaroslavl. Church of the Beheading of John the Forerunner in Tolchkovsky

**Russian:**

Дабы не загромождать пока остановимся.

**English:**

Just so we don't overload before we stop.

Vladimir Mamzerev. 22.12.2013 г.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-111.html)

© All rights reserved. The original author retains ownership and rights.

