title: Inspired by "And there was a Country before Russia". Part 2
date: 2013-10-08
modified: Fri 02 Apr 2021 20:02:16 BST
category: pro_vladimir
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: What was it about these alleged bombards that caught the eye, or rather, attracted attention in the first place? The threaded connection of the breech to the barrel. Am I the only one who thinks that this is a bit steep for that time far away now?
status: published
local: По мотивам «И была Страна до России». Часть 2_files
originally: По мотивам «И была Страна до России». Часть 2.html

### Translated from:

[https://pro-vladimir.livejournal.com/2013/10/08/](https://pro-vladimir.livejournal.com/2013/10/08/)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html).

What was it about these alleged bombards that caught the eye, or rather, attracted attention in the first place? The threaded connection of the breech to the barrel. Am I the only one who thinks that this is a bit steep for that time far away now?

First, the accuracy of the threaded connection. The size of the gun is not a toy and you need to make enough effort to disassemble or assemble this tool. Ensure that the planes match. Any inaccuracy in the thread adds the amount of effort required to assemble the disassembly. Anyone who has dealt with threaded connections live, not in words know how tight it is sometimes necessary even with the most seemingly insignificant problems, and you have to rest hard to twist or untwist something. And if there is another defect on the thread, then you can completely tear the entire thread without achieving the result.

And this is far from an M10 thread. That is, in order for this to be twisted, it is necessary to ensure sufficient accuracy of the threaded connection. By means of an earthen form? It's very interesting to ask a machine-building company to at least estimate what they need to make such a compound right now. And look at their faces when they come together, that there is at least a need for turning. Lathes for processing a bronze block of this weight and size?

Next, attention is drawn to the execution of the slots. Now in mechanical engineering, this type of nut is called a "round nut with splines".

<!-- Local
![](По мотивам «И была Страна до России». Часть 2_files/435638046_w640_h640_klyuch-dlya-shlitsevyh.jpg#resized)
-->

[![](https://images.ru.prom.st/435638046_w640_h640_klyuch-dlya-shlitsevyh.jpg#clickable)](https://images.ru.prom.st/435638046_w640_h640_klyuch-dlya-shlitsevyh.jpg)

That is, similar keys were used for unscrewing and screwing. Both the barrel and breech have two bands of slots for these keys. That is, two held the barrel, two keys twisted the breech. A total of four keys simultaneously for servicing one trunk.

<!-- Local
![](По мотивам «И была Страна до России». Часть 2_files/mons-meg-edinburgh-castle-scottland.jpg#resized)
-->

![](http://jimcaldwell.files.wordpress.com/2012/12/mons-meg-edinburgh-castle-scottland.jpg?w=900)](http://jimcaldwell.files.wordpress.com/2012/12/mons-meg-edinburgh-castle-scottland.jpg?w=900)

With the guns of the siege of Constantinople in 1453, everything seems clear. The joint and technological elements for assembly and disassembly are clearly visible.

We look at their twin "Mons Meg" and "Mad Greta". BA, Yes, they also have elements for disassembly. Recesses in the breech of the guns make it possible to use the same keys. They are hardly made for beauty. So you can also Unscrew the breech from the barrel. The only difference is that there is room for only two keys to be installed simultaneously. One holds, the second twists.

What is the purpose of this unscrewing breech as a technical component of the barrel?

The classic gun existed for a long time as a muzzle loaded firearm before to the decision to load it via the breech. But here, some supposed bombards already have this kind of ornate technical solutions. It is quite obvious that unscrewing such a barrel for loading is complete nonsense, since, with such a large barrel, it would be easier to load the powder charge through the barrel.

Ease of transportation? It makes a bit of sense, but it's still hard to believe.

Replaceable breech parts as attachments for different applications? To select the right key for a specific task. Similar to how we adjust an organ pipe to the frequency of the desired note. In the case of the organ, the length and diameter of the pipes are changed. In the case of this type of gun, the breech was changed.

What is completely absent from this category of guns? The components of the classic bombard that enable it - by means of powder charges - to shoot anything.

The classic bombard always has components that allow it to be fixed and transported, such as all sorts of eyelets, rings, in different parts of the body. Also, bombards have, for the most part, components of a rotary mechanism for changing the elevation angle.

The barrels we are considering do not have any of this. Well, they were not intended for firing gunpowder.

And those who tried to use them for shooting cannonballs knew this very well. That's why they dug trenches next to them, so as not to kill the shooters.

Some may be outraged that the people of the past simply misunderstood the nascent power of gunpowder. But we see here they could make a screw mechanism of this size with such accuracy, and yet they could not calculate the pressure that gunpowder creates? It doesn't really fit.

Another characterisct that is typical of non-gunpowder guns is that the breech has a flat end that shows traces of abrasion. This is where it was scratched by contact with a completely flat surface, i.e. it could only have happened when the barrel was standing, just people get corns on their feet. Other traces and damage of the same scale of use are simply not found on the barrel.

Accordingly, the flat breech end was the main stop and other surfaces of the barrel, when working, did not touch anything significant. While the abrasions clearly show 'active standing', i.e. that the barrel was swaying.

And don't you think it's strange that such, we might say, perfectly executed tools do not have the same perfect transportation systems and targeting systems. That it is, in fact, a piece of pipe, skillfully executed, but without any hint of any use - except as a piece of pipe.

Or maybe they were not transported anywhere? And their use was vertical. Otherwise, where do the breech abrasions come from? And why make a flat base? And cast barrels were decorated, decorative elements were put on all surfaces with the exception of working surfaces.

<!-- Local
![](По мотивам «И была Страна до России». Часть 2_files/139.jpg#resized)
-->

[![](http://legion1812.narod.ru/3/ris/139.jpg#clickable)](http://legion1812.narod.ru/3/ris/139.jpg)

<!-- Local
![](По мотивам «И была Страна до России». Часть 2_files/142.jpg#resized)
-->

[![](http://www.viskovatov.narod.ru/T01/Ris/142.jpg#clickable)](http://www.viskovatov.narod.ru/T01/Ris/142.jpg)

Even according to historians, it was not possible to shoot them full. The diameter of the gun is large and, in order to give the shot a decent speed, they required a considerable charge of powder. 

Some say that they shot stone debris. Now imagine you're dragging a piece of pipe that weighs something like two empty cars. About. So, using travois and logs. You dig up, fill up, and sculpt the terrain so that this stub of pipe can be dragged under the walls of the besieged city. Then you risk long exposure so that you can shoot. And we remember that this gun of ours shoots, well, not very accurately. That is, you are in the vicinity of the besieged city while engaged in the installation of your "main" caliber weapon. Long and methodically setting it up with guidance and exercises, then hiding in a foxhole, and the barrel broke off after burying all the works of those who touched it.

And some of these tools suffered such a fate. Although it was more like using them in "hand-to-hand" combat. Since they stopped working like they did before. The scenario resembles hammering nails with a microscope, but after the microscope has stopped working.

Vladimir Mamzerev 08.10.2013

*Translator Note: Images used to illustrate this piece can only be seen by visiting [the original webpage.](https://pro-vladimir.livejournal.com/2013/10/08/)*

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-3.html)

© All rights reserved. The original author has asserted their ownership and rights.
