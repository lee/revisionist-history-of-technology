title: Evidence of Thermonuclear War - Sibved
date: 2021-07-23 00:02
modified: 2022-04-24 09:45:49
category: 
tags: thermonuclear war; links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md


[African Craters](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/164780.html)

[Traces of Ancient War - New Facts](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/201230.html)

[Arabian Craters](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/215129.html)

[Mexican Craters](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/216488.html)
