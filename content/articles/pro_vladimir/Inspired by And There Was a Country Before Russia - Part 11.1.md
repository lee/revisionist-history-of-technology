title: Inspired by "And There Was a Country Before Russia" - Part 11.1
date: 2013-12-27 00:00:01
modified: Fri 02 Apr 2021 20:10:23 BST
category:
tags: cannons, technology
slug:
authors: Vladimir Mamzerev
summary: The more we dig, the more we find ourselves at the root of the question: "And what is electricity?" I will tell you about the energy of the world of electricity...
status: published
from: https://pro-vladimir.livejournal.com/9239.html
local: Inspired by And There Was a Country Before Russia - Part 11.1_files
originally: По мотивам "И была Страна до России". Часть 11.1 - pro_vladimir — LiveJournal.html

### Translated from:

[https://pro-vladimir.livejournal.com/9239.html](https://pro-vladimir.livejournal.com/9239.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

**English:**

Inspired by [And there was a Country Before Russia](http://dmitrijan.livejournal.com/71535.html) and [Ivan and Mary](http://dmitrijan.livejournal.com/91508.html)

**Russian:**

Копая вопрос, всё больше сползается в корни постановки вопроса, а что есть электричество. Расскажу Вам про Энергию Мира Электричества 10032009. Автор Dmitrij. Букв много. Смысл для многих достаточно "злой". Типа "Ахтунг!!!", может быть больно. От "меня" же будет ниже небольшой "букварик" к этому. Лайт версия. Картинки в учебник. Небольшое визуальное дополнение с ответвлением.

**English:**

Digging into the question, it's more and more slipping down to the roots of the question and what's electricity. I'll tell you about the Energy of the World of Electricity. Author Dmitrij. "There are many letters. The meaning for many is "evil" enough. Like "Akhtung!", it can hurt". From "me" it's gonna be below a little "A" to that. A 'Lite' version. Pictures in the textbook. A little visual supplement with a branch...

**Russian:**

Нынче Мы собрались тут, чтобы поговорить о совсем обыденных и странных вещах. Кто-то улыбнется и скажет, что все это и так известно, кто-то скажет, что этим и так ученые занимаются, а мы подождем их решения. А кто-то скажет, что он и так это знает. Возможно. Хотя часто бывает, что в давно изученном мешке знаний, при очередной приборке, вдруг обнаруживается нечто давно забытое. И как раньше этого не заметили? А хотели ли? Нахлынут воспоминания... И Мир повернется другой гранью, совсем другой. Уселись по удобнее...

**English:**

Now we're here to talk about very ordinary and strange things. Some will smile and say "it's all known"; some will say "that's what scientists do, and we will wait for their decision". And somebody's gonna say that they already knew it. That's possible. It often happens that in a long studied area of knowledge, when it was studied using another instrument, something long forgotten is suddenly found. And how did you not notice it before? Did you want to? There are memories... And the world will turn a different face, a completely different face. Sit down comfortably...

**Russian:**

Когда-то давно. Знали Люди механику и гидравлику. Все было понятно и просто. Мир был плоский и круглый. Звезды на небе. Магия отошла, как пережиток. Наука развивалась. Вот-вот Илью пророка с неба снимут! И вот незадача, кто-то, кто-то подсунул электричество! Это оказалось некой субстанцией. Невидимой, неощутимой! Т.е. абсолютно не материальной, но вполне ощутимой! Лейденские банки ой как кусались!

**English:**

Once upon a time, people knew mechanics and hydraulics. It was clear and simple. The world was flat and round. The stars were in the sky. Magic had gone away like a relic. Science was evolving. The prophet was about to be taken out of the sky by Ilya! And here's the trouble, someone, someone encountered electricity! It turned out to be some kind of substance. Invisible, imperceptible! That is absolutely not material, but quite tangible! The Leiden jars were biting!

**Russian:**

Тогда же по аналогии с гидравликой, записали, что это некая жидкость, текущая по трубам проводников. И даже назвали молекулу жидкости - Электрон! Так невидимые энергии стаи наукой в материальном мире! Их нельзя было пощупать и увидеть, но можно было ощутить разряд от их применения и мощь воздействия. А кнут посильнее пряника впечатляет. И не беда, что никто не видел электрона! Щелчок разряда и воздействие невидимого очень даже ощутим. И никого не смущали в дальнейшем небольшие казусы, к "уж повелось", что ток течет в проводнике, ну по проводнику, для очень продвинутых вдоль проводника! А если кто скажет, что ток вообще не течет, так он не разбирается! А электрон не нашли, так найдут после! А между тем, время шло, а казусы никто не объяснял, а они копились.

**English:**

At the same time, by analogy with hydraulics, it was recorded that this is some kind of fluid flowing through the pipes of conductors. And they even named the liquid molecule: Electron! So, invisible energies became an area of science in the material world! They could not be touched and seen, but one could feel their discharge and their power of influence. And a stick is more impressive than a carrot. And it was no trouble that no one could see the electron! The clicking of the discharge and the effect of the invisible was very impressive. And nobody was embarrassed in the future by small mishaps: it "already happened" that the current flows in a conductor... well, on a conductor... in very advanced ways along a conductor! And if anyone says that the current does not flow at all, well, he does not understand! And although they didn't find the electron, they'll find it later! Meanwhile time went by and no one explained the mishaps. And they were piling up.


**Russian:**

Скажем - катушка накапливает ток. Никаких сомнений! Опыты подтверждают! Но где? Конечно внутри проводов! Разве? Если так, то больше и толще провода используем и накопления увеличатся! А на деле? Объем накопленного электричества зависит от проводников и от введенного внутрь катушки магнитного сердечника! От сердечника весьма ощутимо зависит, а лучше он в виде магнита! Получаемся магнитный выпрямитель! Такой использовался в Электрической машине Баумана и не только. Магнитные усилители советских времен делали по такому принципу. Но сердечник-то вне обмотки! Как же он так значимо влияет? Разве что что-то не так?

**English:**

Let's say the coil accumulates current. There's no doubt about it. Experiments confirm it. But where? Inside the wires of course! Is that where it is? If so, we use more and thicker wires and the accumulation will increase! And in reality? The amount of accumulated electricity depends on the wires and the magnetic core inside the coil! It is very much dependent on the core. Better yet, if the core is a magnet. We're making a magnetic rectifier. This was used in the Bauman Electric Machine and others. The magnetic amplifiers of Soviet times were made on this principle. But the core is outside of the winding! How does it have such a significant effect? Isn't something wrong?


**Russian:**

Хуже с конденсатором! Чего уж проще - два диска, а ведь какая хитрая конструкция! Говорят, что заряды накапливаются на этих дисках! Возможно. Значит чем больше такой диск, тем лучше? Так и есть. Но вот странность, чем диски ближе друг к другу, тем больше может накопить, больше емкость! Ближе, не дальше! А по теории, если электроны живут в проводниках, то расстояние вообще не влияет! Если на поверхности, то, как раз надо развести подальше и больше накопится! А тут наоборот, странно все это. Странно и другое - уже накопленный заряд возрастает, если просто эти диски раздвигать! Вот берем 2 диска, заряжаем, отключаем. Раздвигаем на приличное расстояние - если не проскочила молния между ними, то можно измерить, и выяснится, что заряд на дисках стал гораздо больше! И откуда? Аналогичная ситуация с катушкой - вводим сердечник и ее заряд возрастает! Выводим - уменьшается.

**English:**

It's worse with the capacitor. What could be easier - two disks, and what a tricky design! They say the charges accumulate on these disks! It's possible. So the bigger the disk, the better? That's the way it is. But the strange thing is, the closer the disks are to each other, the more they can accumulate, the bigger their capacity! Closer, no further! But in theory, if electrons live in conductors, the distance has no effect at all. If there are electrons on the surface, then more storage is needed! And here, on the contrary, it's weird. Weird and different: the already accumulated charge increases if you just move these disks apart! Here we take two disks, charge them, disconnect them. Separate them over a decent distance - and if lightning has not slipped between them, you can measure their charge and it will turn out that the charge on the disks has increased! But from where? It's the same situation with the coil: we inject the core and its charge increases! Pull it out, the charge decreases.

Translator's note: it's not clear which charge measurement method is being used.

**Russian:**

На этом принципе построены генераторы! Магнитный сердечник приближается к катушкам и отходит - в катушке получается ТОК! Объясняют по-разному, вроде бы успокоились на понимании явления, что так сложилось. Но это не дает ответа откуда? Интересна и другая особенность - так повелось, что катушки генераторов мотают проводом друг за другом, т.е. просто намотали и успокоились. Соответственно эффект ЭДС, что возникает при этом, т.е. сопротивления катушки перемещению сердечника, не имеет внятного объяснения, хотя всех устроило, что это некая самоиндукция сопротивляется. Так и повелось. Однако, некто, всегда есть некто, взял и намотал 2 проводами и включил их друг за другом, т.е. последовательно. По бытующей теории, ток пробегает один виток, потом еще и все по теории точно так же как намотать обычным способом таким же количеством витков. Это в теории. На практике происходит странная вещь - ЭДС самоиндукции исчезает! Т.е. входить и выходить сердечнику никакая сила не мешает! Такой генератор вращается легко и непринужденно, и сопротивление вращению не зависит от тока в нагрузке! Опс. Но не тут-то было! Это же антинаучно! Столько трудов написано! ЗРЯ?! Не может быть! Да он батенька шарлатан! Да мы лучше знаем! Да если бы такое было, возможно давно бы сделали! И т.д. А ведь так можно мотать и трансформаторы. И такая система, как и любой радиоприемник не требует затрат на потребителя от передатчика, а только задающую частоту. А ведь возможно намотать и тремя проводами.

**English:**

Generators are built on this principle. The magnetic core approaches the coils and departs - in the coil it turns out TOK! They explain it in different ways, they seem to have calmed down on the understanding of the phenomenon that it happened. But that doesn't answer the question: from where? Another interesting feature is that it happens that the generator coils are wired one after another, i.e. they are just wound up and calm down. Correspondingly, the EMF effect, which occurs in this case, i.e. the resistance of the coil to the movement of the core, has no clear explanation, although everyone is satisfied that this is some kind of self-induction resistance. That's what happened. However, someone, there is always someone, wrapped two wires and turned them on one after another, i.e. sequentially. According to the existing theory, the current runs along one spiral, then another and all the same theory in the same way as to wind the usual way the same number of turns. That's in theory. In practice, a strange thing happens: the self-induction EMF disappears! In other words, no force prevents the core from entering and exiting! Such a generator rotates easily and naturally, and the rotation resistance does not depend on the current in the load! Oops. But it couldn't be! It's unscientific! So much work has been written! WHERE?! No way! Oh, he's a quack! We know better than that! If it was true, it would probably have been done a long time ago! And so on. And that's how you wind transformers, too. And such a system, like any radio receiver, does not require the cost of the consumer from the transmitter, but only the setting frequency. But it is possible to wind one with three wires as well.

**Russian:**

Генератор на основе конденсатора так же прост - 2 диска, на них проводящие полоски, диски вращаются в разные стороны. Электрофорная машина, ее показывают в школе. Именно на ней реализован принцип расходящихся пластин конденсатора! Т.е. пластинки заряжаются, разводятся подальше, заряд снимается! Всех усилий на вращение дисков! Весьма небольшой, а вот заряд весьма ощутимый в результате. Несколько сложна машинка, но как пример годится.

**English:**

The capacitor-based generator is just as simple: two disks with conductive strips on them. The disks rotate in different directions. It's an electric machine, it's shown at school. With it, the principle of diverging capacitor plates is demonstrated. That is, the plates are charged, separated from each other, and the charge is removed. The effort required to rotate the disks is small! Very small, but the charge is very tangible as a result. A complicated machine, but a good example.

**Russian:**

Электронные лампы! Вот где кажется все признаки электронов! Но странное дело - мало того, что ток знает, где плюс, а где минус, так еще и летят, управляемые зарядом! Говорят, что летят, их никто не поймал в момент полета. Не получилось, старались, но никак. Прилет есть, отлет вроде есть, а полета не зафиксировано.

**English:**

Electronic lamps. That's where all the signs of electrons seem obvious. But the strange thing is not only that the current knows where the plus and where the minus is, but that they move - controlled by the charge. They say they're moving but nobody has caught them in the act of moving. they've tried to but they are never successful. There is a departure, there is an arrival, but the flight is not determined.

**Russian:**

Некто Гейзенберг в 20-х выдвинул принцип неопределенности - где суть, подтвержденная на практике, такова - выпускаем 1 электрон - приемник получает 1 электрон. Выпускаем 2 - регистрируется 2. Ставим преграду, делаем отверстие - 2 улетело, 2 прилетело. Делаем 2 отверстия. 2 электрона улетело, а вот прилетело от 0 до 7! в зависимости от положения преграды перед источником! Опс. Откуда лишние? Принцип легко реализуется на лампах или статисторах. Очень просто достигается эффект и ведь при затратах в 2 единицы на выходе возможно получить 7! Опыт проверен и перепроверен, но по теории такого быть не может, но оно есть.

**English:**

Some Heisenberg in the 1920's put forward the uncertainty principle, the essence of which, confirmed in practice, is this:

- we release one electron 

- the receiver receives one electron. 

- we release two 

- the receiver receives two 

We put an obstacle between transmitter and receiver; we make a hole in it:

- two electrons flew away, two arrive

We make 2 holes: 

- two electrons are released

- but any number from zero to seven arrived, depending on the position of the barrier in front of the source!

Oops! Where'd the extra ones come from? The principle is easily realized on lamps or accessories. It is very easy to achieve the effect: that at the cost of two units at the output you can get seven! This experience is tested and double-checked, though in theory it can not be so. However, it is so.

**Russian:**

Странности в поведении катушек, конденсаторов, да и не пойманных электронов можно считать казусом и проходить мимо. Можно оставить потомкам и успокоится. А можно задуматься.

**English:**

Strange behavior of coils, capacitors, and uncaught electrons can be considered a mishap and something to ignore. You can leave it to your descendants and calm down. Or you can think about it.

**Russian:**

Начнем с простого - статики. Как получить заряд? Потереть что-то о что-то! Говорят, что они бывают положительные и отрицательные. Но странное дело - разноименные всегда притягиваются. Хотя есть ли они? Как установили - потерли, прикоснулись к волоскам - они стараются слипнутся - слипаются - несомненно, но вот что с чем? Заряд, который мы получили с чем? А был ли другой заряд? А не было! Именно заряд притягивается к его отсутствию! Интересно другое - если зарядов набрать много, то создается статический ветер! Он идет вдоль источника таких зарядов и напоминает вихрь. Понятное дело, что материя с 0 зарядом стремится к заряженному месту, как мы к Солнцу погреться, а подлетев, отлетает! Именно поэтому, соприкоснувшись зарядом, к еще одной пластине 2 вихря стремятся разбежаться. Берем заряженную палочку - подносим к бумажке - притягивается. Подносим еще одну бумажку - они притягиваются и тут же отлетают! Говорят, что заряды стали одноименными. Но вы когда-нибудь видели, чтобы на воде 2 вихря воронки притягивались, схлопывались? А смерча? И не увидите! Прикоснувшись 2 бумажками, мы создаем подобие сбалансированной Вселенной, где 2 вихря вращаются и стремятся расширить вселенную в разные стороны! Именно так, как притягиваются 2 бумажки, античастицы стремятся встретится, а встретившись аннигилируют и создают Шар! А шар имеет свойство раздуваться, вот эти бумажки, как часть поверхности шара и отталкивает подальше.

**English:**

Let's start with the simple statics. How do we get a charge? From something from something! Charge is said to be positive and negative. But the strange thing is, the different charges are always attracted to each other. But are they? 

As established, rubbed, touched hairs try to stick together. Stick together? Yes, surely, but what is with what? The charge that we got with what? Was there another charge? There wasn't! It's the charge that attracts us to its absence! 

Another interesting thing is that if you collect a lot of charge, you get a static wind. It goes along with the source of the charge and resembles a vortex. It is clear that matter with zero charge is attracted towards a charged place, as we are to the Sun to warm up. For this reason, having touched the charge to one more plate of two plates, the vortices try to disperse. For example, we take a charged wand and bring it to a piece of paper and the paper is attracted to it. We bring another piece of paper and the two pieces of paper are attracted and immediately drop away. They say that the charges have become the same polarity (balanced). But have you ever seen two vortexes of whirlpools attracted to each other in water? And a whirlwind? You won't see it! Touching two papers, we create the likeness of a balanced universe, where two vortices spin and seek to expand the universe in different directions! Just as two papers are attracted, anti-particles tend to meet, and when they meet, they annihilate and create a sphere. And the sphere has the property of inflating, so these papers - as part of the surface of the ball - push away.

*Translator's note: The detail of what was originally being described is not very clear from the machine translation. Human translation and/or diagrams would probably clarify.*

**Russian:**

Возьмем конденсатор - увеличение ширины пластин соответствует увеличению емкости, а расстояние? Увеличиваем его, растет заряд! С точки зрения теории тока не логично. С точки зрения ток на проводнике тоже. С точки зрения ток следствие чего-то зависимость прямая, но чего? Ведь если некая среда не в проводах, а снаружи, то раздвигая, мы увеличиваем расстояние, а значит размер вихря зарядов. А больший вихрь - больше энергии! Если припомнить воду, то пластины конденсатора можно рассматривать как стенки сосуда. Увеличивая их поверхность - увеличиваем объем сосуда. Раздвигаем их - объем так же растет! Но если сосуд открытый и в глубине воды, то увеличив объем стенками, мы не добьемся увеличение заряда. Другое дело сблизить стенки как можно ближе - вся вода выйдет, продуть и закрыть сосуд, т.е. зарядить. А дальше начать раздвигать стенки! Воды внутри нет, а снаружи полно! Заряд, т.е. давление воды резко будет возрастать с увеличением расстояния! Достаточно открыть пробку и вода хлынет и совершит работу! Но ведь можно стенки раздвигать и дальше, и дальше, дальше? Материал-то не бесконечно прочный! Значит, стенки сломаются, произойдет пробой! Т.е. вода хлынет внутрь, а в случае с электричеством - сверкнет молния.

**English:**

Let's take the capacitor... Increasing the width of the plates correspondingly increases the capacity. And increasing the distance? If we increase it, the charge increases. This is not logical from the point of view of current theory. In terms of current on the conductor, it's the same. From the point of view of a current, the current is directly dependent on something, but what? Perhaps the medium it is dependent on is not the wires, but outside, so that once moving, we increase the distance, and thus the size of the vortex of charges. And a bigger vortex equals more energy! 

If you think of water, the capacitor plates can be seen as the walls of a vessel. By increasing their surface area, we increase the volume of the vessel. Expand them and the volume increases as well. But if the vessel is open and in a depth of water, then by increasing the volume of the walls, we will not increase the pressure, the charge. 

Another example would be to bring the walls as close as possible. All the water will come out. Now empty of water - ie, of charge - let's close the vessel. And then we start to move the walls apart. There is no water inside, but there is plenty of water outside. Charge - ie, the water pressure will increase sharply with increasing distance of the vessel walls from each other. It's enough to open a plug in the vessel and the water will flow in and balance the pressure. But is it possible to move the walls further and further, further? The material is not infinitely durable. So, the walls will break down, there will be a breakdown. That is, the water will flood in, and in the case of electricity, that is when lightning will sparkle.

**Russian:**

Принцип Гейзенберга точно так же легко объясняется интерференцией волн, если допустить, что электроны волны. А это дуализм, однако. Или проще посчитать, что электрон и не был никогда материальным? Ну ошиблись вначале, с кем не бывает. Чего зазря биться головой о стенку? Зачем зря городить лишнее в угоду амбиций?

**English:**

The Heisenberg principle is just as easily explained by the interference of waves, assuming that electrons are waves. But this is dualism, however. Or is it easier to think that the electron has never materially existed? Well, you were wrong at first, but who doesn't that happen to? Why bang your head against a wall for nothing? Why waste your head banging against a wall for your ambition?

**Russian:**

Интересная картина получается, все казусы электричества объясняются с позиций некого поля. Невидимого, не ощущаемого, однако вполне реально взаимодействующего. Воздух мы еще видим, воду. Но ведь заряды вот они, ощутимо кусаются? Значит и причина есть? В радиоволны верим? А если волны, то на поверхности чего?

**English:**

It turns out to be an interesting picture, all electricity's mishaps are explained from the point of view of a certain field. Invisible, not felt, but quite realistically interacting. We can still see the air, the water. But the charges here, do they bite noticeably? So there's a reason? Do we believe in radio waves? And if there are waves, then on the surface of what?

**Russian:**

Эфир... возможно. Однако, ток можно померить только взаимодействием с электромагнитным полем проводника! Никак иначе! Все приборы измерения - это вариации на тему магнитной стрелки компаса и угла ее поворота. Что такое магнит? Может это некая щепка на поверхности среды? Ведь поместив компас внутрь проводника никакого поля там не обнаружим! Ток есть, а поля нет. Экранирует, говорим. А может, тока нет? Может магнитное поле и есть среда, а заодно и то, что мы измеряем? А может электромагнитные волны, есть причина, а не следствие? Ну и что, что не видно? Тока ведь тоже не видно, как не искали! А единственная зацепка - прибор с магнитом! А магнит вне провода. Так и выходит, что может не стоит лишнее плодить, проще принять то, что есть - т.е. есть среда вокруг, не видимая и что? Не ощущаемая и что? Вон Душа или ДНК тоже не видна, но ведь их наличие отрицать сложно? А Самосознание? Его даже не пощупать, но оно есть! Так стоит ли усложнять мир, идти на поводу предрассудков прошлого, когда наследие механики и гидродинамики пытались навязать по аналогии? Ведь одно дело считать, что мы вне среды, и она течет по трубам, а другое дело, что внутри ее и она вокруг нас! И раз уж так сложилось, то проще всего ее звать Магнитной средой!

**English:**

The air... ...perhaps. 

However, the current can only be measured by interaction with the electromagnetic field of the conductor. No, it can't! All measuring devices are variations on a theme: the magnetic arrow of a compass and the angle of its rotation. What is a magnet? Maybe it is some kind of chip on the surface of the medium? After all having placed the compass inside a conductor we will not discover any field there. There is a current and there is no field. It's shielding, we say. Or maybe there's no current? Maybe the magnetic field is the medium, but is also what we measure? Or maybe electromagnetic waves are the cause, not the consequence? So what, what can't you see? You can't see the current, either. And the only clue is a device with a magnet! And the magnet is off the wire. So it turns out that maybe it's better not to worry too much, it's easier to accept what is - that is, there is an invisible environment around. It's not visible, so what? The Soul or DNA is not visible either, but their presence is difficult to deny, isn't it? And self-consciousness? You can't even touch it, but it's there! So is it necessary to complicate the world, to go on the prejudices of the past, which the heritage of mechanics and hydrodynamics tried to impose by analogy? After all, it is one thing to think that we are outside Wednesday and it flows through the pipes, and another thing to think that is inside her and she is around us! And if that's the case, the easiest way to call her the Magnetic Wednesday!

**Russian:**

Следую именно такой логике можно предположить, вопреки мнению науки, что идеальный провод это фольга! Максимальная поверхность и минимальный расход материалов. Сопротивление? Перегреется, скажет специалист? Именно для этого и нужны толстые провода, ибо с толщиной провода сопротивление уменьшается? А вот и нет! Сопротивление уменьшается с размером поверхности! Именно поэтому предпочтительней многожильный провод! Законы Вселенной, переложенные на материю, подсказывают, что волна на поверхности будет стараться сдвинуть материал проводника за поверхность, как и любая продольная волна будет толкать волнорез. Смещение материала волнореза и вызывает нагрев его! Провода еще и скручиваются, именно поэтому гудит трансформатор. Мотаем его в 2 провода, соединяем последовательно - и не гудит! Потерь нет! Но не научно.

**English:**

I follow exactly this logic, contrary to the opinion of science, that the ideal wire is foil! Maximum surface and minimum material consumption. Resistance? Overheating, the expert will say? This is exactly what thick wires are for, because with the thickness of the wire, the resistance decreases, right? Well, it doesn't. The resistance decreases with the surface area! That's why a multicore wire is preferable to solid core wire. The laws of the universe on matter suggest that the wave on the surface will try to move the conductor material beneath the surface, as any longitudinal wave will push at the breakwater. The shift of the breakwater material causes the breakwater to heat up. Wires are also twisted, which is why the transformer buzzes. Wrap it in two wires, connect it in series, and it does not hum! No loss! But that's not scientific.

**Russian:**

Китайская промышленность вслед за российскими некоторыми "неправильными" научными деятелями, наладила выпуск проводов из фольги! Такой провод выдерживает гораздо большие нагрузки, и практически не греется! Именно поэтому плавкие предохранители не делают из фольги и многожильного провода! Они не перегорят "правильно"! Но также это и подтверждает, что тока в проводе нет! Его вообще нет! Это миф! Миф, но для чего? Неужели ученые не видят очевидного? Неужели их амбиции и желание получать деньги на исследования превышают логику и чистоту научных выводов?

**English:**

Chinese industry, following the "wrong" Russian scientists, has set up the production of wires from foil. Such wire can withstand much higher loads, and barely gets warm. That is why fuses are made neither of foil nor multicore wires. They will not burn out "correctly"! But it also confirms that there is no current in the wire. There is no current in the wire at all. It's a myth! A myth, but for what? Can't scientists see the obvious? Does their ambition and desire to get money for research exceed the logic and purity of scientific conclusions?

**Russian:**

Откуда собственно берется ток? Энергия кругом! Солнце светит, ветер дует, пользуйся, не хочу. Но тока нет! Статические заряды есть. Но тока нет! Ток получается как движение этих зарядов, якобы движение! Именно с этого все и начиналось. Логично поначалу было. Опять же батарейка - вот пример получения тока, вроде бы. Все можно объяснить с точки зрения энергий элементов, да вот не все. Но можно и по-другому объяснить. Еще вариант - солнечная батарея или термопара - некий сплав, некий материл, где на границе сред возникает напряжение, подключаем источник - ток! Вроде бы все логично. Свет попадает, нагревает, выбивает, ток бежит. Если бегут электроны, то почему? Говорят, что один материал более энергонасыщен, чем другой. Возможно. Но вот села батарейка, логично предположить, что если некие электроны бежали от - к +, то на + их скопилось больше? Логично. Однако материала на + не стало больше, он не стал другим, хотя химия нам сообщает, что вся структура материала, это ядро да электроны кругом летают. Значит, если их скопилось больше, то ускорители не нужны - вот они в материале накопились! Так и ведь медь в золото легко перевести, в теории, если. А прибывшие электроны не сразу в атом вставляют, а называют это ионами! Хитрят, чувствую, хитрят. Изворачиваются, никак теорию под практику не подгонят. А на практике зачем-то ускорители строят! Говорят, что надо побыстрее разогнать электроны, чтобы ах! Странно, странно все это.

**English:**

Where does current actually come from? Energy all around. The sun is shining, the wind is blowing, and they use it. I don't want to. But there is no current. There are static charges. But there's no current. Current's supposed movement turns out to be the movement of these charges. That's where it all started. It was logical at first. Again, I think the battery is an example of how to get current. You can explain anything in terms of the energy of the cells, but not everything. But there's another way of explaining it. Another option for explaining it is a solar cell or a thermocouple. A certain alloy, a certain material, where voltage occurs at the boundary of the two mediums when a current source is connected. Everything seems to be logical. Light hits, heats, knocks out, current runs. If electrons run, then why? They say that one material is more energy-saturated than the other. It's possible. But if the battery is dead, it is logical to suppose that if some electrons ran from negative to positive, more of them were accumulated by the positive? That makes sense. However, there is no more material on the positive pole, it has not become different, although chemistry tells us that the entire structure of the material, the core and electrons move around. So, if there are more of them, then accelerators are not needed - they are accumulated in the material. So it's easy to transform copper into gold, in theory. And if the newly arrived electrons are not immediately inserted into the atom, then they call them ions! They're tricky, I can feel it, they're tricky. They're twisting, they're not gonna fit the theory into practice. And in practice, they build accelerators for some reason. They say they need to accelerate the electrons to, ah, it's weird, it's weird.

**Russian:**

Но что происходит в реальности? В реальности ток то есть, то нет. Странно это. Причем там он бежит, там перепрыгивает. Для его описания вводят мнимые числа! Это такие числа, которых нет, но подразумеваются! Причем бытующая теория тока не действует всегда, на всех схемах, она применяется, только на участках! Целую картину, рассчитывают приблизительно! Если некто скажет, что вот у него производится ток, но не из розетки или батарейки, то любая окрестная компания по продаже электричества может смело заявить, что это он у нее ворует! Нет проводов? Не страшно! Но ему взять-то его больше негде! Из воздуха? Обкрадывает радиостанцию! И это не прикол, это реальные случаи. Странно все это.

**English:**

But what happens in reality? In reality, there's current, there's no current. That's weird. And there it runs, there it jumps. To describe it, they enter imaginary numbers. They're numbers that don't exist, but they're meant to exist. And the existing theory of current does not always work, on all schemes. It can be applied, but only to sections. The whole picture is quite roughly calculated. If someone tells you that he makes current, but not from a socket or a battery, then any neighboring electricity company can safely claim that he is stealing from it! No wires? That's okay! But he can't get it anymore! Out of the air? He's stealing from a radio station! And it's not a joke, these are real cases. It's all weird.

**Russian:**

Как вот в мифологии заездили дракона о 3-х головах. А ведь, оттуда же можно узнать, что летал-то он хвостом вперед! Рыгал пламенем! А может это и не головы были, а сопла двигателей? Может так и с током?

**English:**

As in mythology, there was a three-headed dragon. And from the same place you can know when he was moving forward. He was burping with flames! Maybe those weren't heads, they were engine nozzles. Maybe it's the same with the current?


**Russian:**

Энергия, что достается миру всегда сбалансированная, в ней нет ни + ни -! Река течет, у нее есть исток и устье. Т.е. направление! У энергий всегда есть направление. Более того, они всегда движутся! Свет, огонь, вода, воздух - всегда в движении! Все естественно!

**English:**

The energy that the world gets is always balanced, there is no positive or negative. The river flows, it has its source and mouth. That is its direction. Energy always has a direction. What's more, it always moves. Light, fire, water, air - always in motion. It's natural.

**Russian:**

И тут некие заряды, ток! Да, человек может искрить, говорят, что это одежда и т.д. Но может искрить и без одежды! И в мокрой атмосфере! А много искр - молния! Что это как не часть Сути Мира Душ? Свет Добра? Но он ведь целостный!

**English:**

And there are some charges, some current. Yes, a man can emit sparks. They say it's his clothes, etc. But he can spark without clothes. And in a wet atmosphere. And a lot of sparks, like lightning, too. Perhaps that's like part of the Soul World? The Light of Kindness? But it's holistic!

**Russian:**

Как получается ток? Разделением энергии посредством движения или напрямую! Воду и воздух делят посредством движения. Солнце, огонь - когда движением, когда напрямую. Термопара - волнорез - набегающий Огонь Сути делит на части. Далее делаем перемычку, и эта Суть течет - опять целостна становится. То же самое Солнце и фотоэлемент! Батарейка так вообще работает на разложении материи - окисление.

**English:**

How does current work? By separating energy through motion or directly. Water and air are separated by motion. Sun, fire when in motion, when directly. The thermocouple, the breakwater, the raging Fire of Suti ('Сути' - possibly 'Sufi') divides into parts. Then we make a connector, and this essence flows. It becomes integral again. Same with the sun and the photocell. The battery works like this on oxidisation - the decomposition of matter.

**Russian:**

Обратный процесс - Свет! Говорят, что нагрев провода к нему приводит, не совсем. Ведь свет излучает и ртутные лампы, и твердые диоды. А то и органика - OLed, нынче такой модный! И уже свет его не перебивается Солнцем - если TFT экраны на Солнце на улице почти не видны, то OLed очень даже! Выходит, такой Свет больше созвучен Сути Добра, что поступает, как Свет Солнца и не противоречит ему.

**English:**

Reverse the process... light! They say that heating a wire leads to it, but not quite. Because both mercury lamps and solid diodes emit light. Or even organics OLEDs. They are so fashionable nowadays! And its light is not interfered with by the sun. For example, a TFT screen's low light output makes it almost almost unreadable out in the sun, while an OLED is very visible! So, this Light is more in tune with the Suti ('Сути' - possibly 'Sufi') of Good, which acts as the Light of the Sun and does not contradict it.

**Russian:**

Представим себе океан, море, лучше озеро! Это будет поверхность магнитной среды. Проведем поперек него стену, рядом еще одну. Это будет провод. Бросим щепку, и по ней будем замерять высоту волн. Пока волны маленькие - стенам ничего не угрожает. Возьмем цистерну воды и опрокинем ее в воду! Поднимется волна, станет биться о стену. Щепка наша начнет прыгать - ток появился? Мы видим волну вдоль стенки - значит, нечто воздействует на стену! Что такое цистерна как не батарейка?

**English:**

Let's imagine an ocean, a sea, a better lake! That would be the surface of a magnetic medium. We'll run a wall across it, another one next to it. It'll be a wire. We'll throw a pebble and measure the height of the waves on it. As long as the waves are small, the walls are in no danger. We take a water tank and dump it in the water. The wave will rise, they will hit the wall. Will our splinters start jumping? Is there a current? We can see the wave along the wall, so something's affecting the wall. How is a tank not like a battery?

**Russian:**

Возьмем некий вентилятор, направим поток воздуха на поверхность - сила воздуха начнет выдумать воду - с одной стороны ее станет меньше по уровню! Соединим перешейком 2 стены - вода потечет на другую сторону! Так работает термопара и фотоэлемент! Возьмем лопатку и начнем толкать воду вдоль стенки - вода вздыбится и волна пойдет вдоль. Как вариант - возьмем насос и с одной стороны, будем перекачивать воду на другую - это и есть работа генератора постоянного тока! Возьмем лопатку и начнем шлепать по воде, так чтобы поверхность заколыхалась - принцип генератора переменного тока! Волны, что бьются о стену, осуществляют работу, давят на стену, поднимают щепку? Несомненно! Какие затраты нужны для создания волны? Только сила по раскачиванию волны в одном месте! А по перекачиванию? Только затраты на подъем воды. А сколько потребителей можно подключить к волнам? Да сколько угодно, пока волны не закончатся! Если один потребитель волны и не будет качаться, то волне ничего не будет! Разве что в этом месте волны не будет. Есть нюанс, если ширина канала будет не такой широкой, то волны перестанут проходить тут, они пойдет в обход. Разве что надо несколько подобрать длину канала, чтобы волна, что дошла до конца, обратно не отбилась, ну это вопрос технический. Хотя именно для этого используют второй провод!

**English:**

Let's take a certain fan, direct the air flow to the surface - the force of the air will begin to invent water - on the one hand it will become less in level! Connect the isthmus of 2 walls - water will flow to the other side! This is how a thermocouple and photocell works! Let's take a blade and start pushing water along the wall - the water will swell and the wave will go along. As an option - we will take a pump and on one side we will pump water to the other - this is the work of the generator of direct current! Let's take a spatula and start to smack on water, so that the surface will stir up - the principle of the alternator! Waves that hit the wall, carry out the work, press the wall, lift the splinter? Absolutely! What cost does it take to create a wave? Only the power to swing the wave in one place! And the pumping power? Only the cost of lifting the water. And how many consumers can you connect to the waves? As many as you want, until the waves are over! If one consumer waves and will not swing, the wave will be nothing! Unless there are no waves in this place. There is a nuance, if the width of the channel is not so wide, then the waves will stop passing through here, they will go around. Unless you need to select the length of the channel a bit so that the wave that has reached the end will not fight back, well, it is a technical issue. Although this is what the second wire is used for!

**Russian:**

И нет причин и препятствий сделать вечный источник! Ибо потребителей может быть множество, а вот такой возмутитель 1 и ему надо всего-то только на свое содержание. Именно так работают радиостанции, впрочем, мы ловим продольные волны, а они, как круги на воде, затухают, а есть и поперечные! Вот они не имеют дальности.

**English:**

And there is no reason or obstacle to make an eternal source! For there can be many consumers, but such an outrage 1 and he needs only for his contents. This is how radio stations work, however, we catch longitudinal waves, and they are like circles on the water, fade away, and there are transverse waves! Here they have no range.


**Russian:**

Кстати скорость тока, как и Света бесконечна. Измерили? А что собственно? Измерили скорость реакции электронного прибора на пределе его точности! Ведь будь именно такой скорость тока, а для нынешних протяженных проводов, это уже секунды задержки, то включение тока отзывалось бы волной его прохождения, а этого нет! После замыкания выключателя электричество всегда во всех узлах схемы, разве что накопители, ну там идет набор Сути.

**English:**

By the way, the speed of current, as well as the light, is infinite. Have you measured it? What is it, exactly? We measured the reaction speed of the electronic device at the limit of its accuracy! After all, if such speed of the current, and for the current long wires, it is already seconds of delay, then the inclusion of the current would be recalled by a wave of its passage, and this is not! After closing the circuit breaker electricity is always in all nodes of the circuit, except for the drives, well, there is a set of Suti ('Сути' - possibly 'Sufi').


**Russian:**

А если есть Магнитная среда, то там должна быть и живность? Есть, конечно, есть! Мало того, что Души взаимодействуют с ним напрямую, так еще и облака все плывут и плывут, а ведь это Сущности 5D мира, живность! Именно поэтому сверкают молнии - Сущности магнитного мира обмениваются Сутью! Дождик идет, а им все нипочем. Странно, не правда ли? Да и располагаются они слоями над головами людей, даже могут менять высоту и перемещаться, хотя считается, что это их ветер гонит! Такие же Сущности есть и на Юпитере, вполне разумные.

**English:**

And if there's a Magnetic Wednesday, should there be a living thing? There is, of course there is! Not only do Souls interact with it directly, but also all the clouds are floating and floating, and this is the Essences of the 5D world, the living! That's why lightning sparkles - the Essences of the magnetic world exchange Essence! It's raining, and they don't care. It's strange, isn't it? And they are placed in layers above the heads of people, even can change the height and move, although it is believed that the wind drives them! The same Essences are on Jupiter, quite reasonable.


**Russian:**

Вся эта суета в мире электричества не прошла даром, цивилизация компьютеров вполне процветает. Хотя после уже описанных в другой писульке событий, в этом мире не появился главный компьютер, а значит именно тому зигзагу, они обязаны нынешней демократией у себя.

**English:**

All this fuss in the world of electricity is not in vain, the civilization of computers is quite prosperous. Although after the events already described in another psalm, there is no main computer in this world, which means exactly that zigzag, they owe their present democracy.


**Russian:**

Но все-таки, почему ученые так настойчиво упорствуют в своем желании считать ток существующим? Возможно, это желание видеть мир материальным и не признавать, что мир больше видимого? А сны, и остальное - бред?

**English:**

Still, why are scientists so persistent in their desire to consider current to exist? Perhaps it is a desire to see the world as material and not recognize that the world is more visible? And dreams and the rest are delusional?

**Russian:**

Наука зародилась в средние века пот патронажем Церкви, неверно думать, что Церковь с ней боролась, это не выгодно. Однако наука разделила мир на части, части стала изучать, развивать мир на кусочки и ушли, ушли далеко от целого. Что вам прок знать, сколько химических элементов в салате? в каше? Вы хотите ее съесть, и чтобы было вкусно! Наука позволила создать искусственные химические вина, порошки ля разведения в воде, но это не решило проблем с питанием! И она все дальше и дальше от целого! Да, каждое направление продвинулось далеко, но что толку? Круша мир, круша целое, можно понять, как он устроен? Или только увидеть его части? Части можно собрать обратно, но будет ли это целым? Можно склеить, но будет ли это прежним?

**English:**

Science was born in the Middle Ages by the patronage of the Church, it is wrong to think that the Church fought with it, it is not profitable. However, science has divided the world into parts, parts began to study, develop the world into pieces and went away, far from the whole. What do you need to know, how many chemical elements in the salad? in the porridge? You want to eat it and make it tasty! Science made it possible to create artificial chemical wines, powders in water, but it did not solve the problem with nutrition! And it's getting further and further away from the whole! Yes, each direction has gone far, but what good is it? Destroying the world, destroying the whole, can you understand how it works? Or just to see its parts? Parts can be assembled back, but will it be whole? You can glue it back together, but will it be the same?


**Russian:**

Суть электричества еще и в том, что разделяя, для получения, поверхность Магнитной среды вскипает, ломается, разбивается. А в ней плавают и Души. Частички их засасывает в водоворот этого мира. Душа есть круговорот, Движение, собственная вселенная, на движении Сути Добра. И как водоворот, смывает частички Души. Крупнее водоворот, сильнее их уносит. Именно эта Суть и необходима для получения мощного источника. Разрушение материи дает не так много энергии. Суть Мира Душ - Душа и ДНК отличный, долговременный источник электричества. Может именно поэтому, его подсунули втихую? Может именно поэтому роятся НЛОшки и ждут эвакуации Людей, чтобы помочь, но не бесплатно же? Может именно поэтому, организованны центры сбора Добра в виде приходов и массовых религиозные центров? Все может быть. Из Душ неплохие батарейки выходят. Отличные источники для автомобилей, а вы генераторы, знаем, знаем. Наука подтвердила. Возможно. Время покажет.

**English:**

The essence of electricity is also that dividing, to obtain, the surface of the Magnetic Medium boils, breaks down, crashes. And in it, the Souls float. Particles are sucked into the vortex of this world. The Soul is a cycle, a Movement, its own universe, on the movement of the Souty ('Сути' - possibly 'Sufi') of Good. And like a vortex, it washes away the particles of the Soul. Bigger vortex, takes them away more. That's the essence that's needed to produce a powerful source. The destruction of matter doesn't produce much energy. The Essence of the World of Souls is the Soul and DNA is an excellent, long-term source of electricity. Maybe that's why they slipped it quietly. Maybe that's why UFOs are digging and waiting to evacuate people to help, but not for free? Maybe that's why, organized collection centers of good in the form of parishes and mass religious centers? Anything can be. From showers not bad batteries come out. Excellent sources for cars, and you generators, we know, we know. Science confirmed it. It's possible. Time will tell.


**Russian:**

А Тушка не так проста, как кажется, ДНК, материя, а ведь есть еще и вирусы!

**English:**

And Carcass is not as simple as it seems, DNA, matter, and there are viruses too!


**Russian:**

Грипп, вроде вирус, вроде. А что такое вирус? А это некое биологическое образование, которое не нуждается ни в еде, ни в воздухе, а жить может долго. И притом считается, что он способен инфицировать живые клетки! Причем целенаправленно! Причем изменяется! Т.е. все признаки некой самостоятельной системы присутствуют. Разве, что размножаются, говорят, в живых клетках. И что интересно - откуда пришли и куда уходят, а главное почему, никаких объяснений. Борьба с ним? Вакцина - сознательное заражение и пусть организм сам выкручивается! Выжил - молодец, привит. Помер - был ослаблен, так и запишем. Нет, есть и другие методы якобы борьбы, но гарантий они не дают, т.е. помогли или нет, не понятно. А вот совокупные меры для организма очень даже приносят результаты. Еще были методы вибрационного воздействия - звон колоколов, музыка. Помогает, причем неплохо, но это же, не материальные способы? Значит не научно!

**English:**

Influenza, kind of a virus, kind of. What's a virus? It's some kind of biological entity that doesn't need food or air, but can live for a long time. And it's also believed to be able to infect living cells! And purposefully! It's also changing! That is, all the signs of a certain independent system are present. Unless they're multiplying, they say, in living cells. And what's interesting is where they come from and where they go, and most importantly why, no explanation. Fighting it? The vaccine is a deliberate infection and let the body twist itself out! Survivor - well done, grafted. He was weakened, so let's write it down. No, there are other methods of alleged struggle, but they do not give guarantees, ie, helped or not, it is not clear. But the combined measures for the body are very effective. There were also methods of vibratory action - bells ringing, music. It helps, and not bad, but it is not material methods? So it's not scientific!


**Russian:**

И так Грипп, можно конечно списать на злого вируса, но Коны Вселенной едины. Вирус не что иное, как отражение Сути Мира Души, по осям вихрям которого собирается материя, которую мы и видим. Т.е. это даже не ДНК! Это простейшие Сути Мира Душ. Этакие простейшие ДНК. Как можно представить, им не надо ничего материального. А вот сами они могут быть употреблены ДНК, освоены, по материальному - съедены! После съедения начинается переваривание. А дальше как пойдет. А переваривание идет с отвлечением сил ДНК, да смотря, что съели - если более горячее, как спирт с водой соединяются, будет разогрев. Если энергетически холодное - то упадок сил. Бороться можно - добавив стабильной Сути Мира Душ - Солнечный Свет, Кислород воздуха, спиртовые настойки, да и травы. Химия тоже может помочь, но по-разному. Вот пенициллин вообще из плесени. Живность пришлого мира - ДНК очень неплохо осваивается.

**English:**

And so the flu can be attributed to an evil virus, of course, but the Cones of the Universe are one. The virus is nothing more than a reflection of the Soul World's essence, whose vortexes gather matter along the axes that we see. So it's not even DNA! It's the simplest Soul World Sentiments. These are the simplest DNA. As you can imagine, they don't need anything material. But they themselves can be used DNA, mastered, eaten materially! After eating, the digestion begins. And then, as it goes. And digestion comes with a distraction of DNA, and it depends on what you have eaten - if hotter, like alcohol and water are combined, it will be heated. If it's energy cold, it's a loss of strength. It is possible to fight - adding a stable Soul World - Sunlight, Oxygen, alcoholic tinctures, and herbs. Chemistry can also help, but in different ways. Here's penicillin from mold. The life of the coming world - DNA is very well mastered.


**Russian:**

Эти элементарные ДНК носит по бесконечности Мира Душ по Конам Мира. Т.е. они так же как и планктон в океане, плещутся на поверхности, на волнах, впитывают Свет Сути Мира, а там это Благо, осваивают себе подобных и подрастают. Плещутся они на гребнях волн Мира Душ. Эти волны в астрологии принято обозначать символами. Делятся на сектора. По сути это 12 Волн. Волны Силы, Гребни Волн Сути Мира Душ. Впрочем, такое есть и в океане Блага.

**English:**

These elementary DNA are worn across the infinity of the World of Souls by the Konam of the World. I.e. they, as well as plankton in the ocean, splash on the surface, on the waves, absorb the Light of the Souty ('Сути' - possibly 'Sufi') World, and there it is the Good, learn the likes and grow up. They splash on the crest of the waves of the Soul World. These waves are symbolic in astrology. They're divided into sectors. They're basically 12 waves. The Waves of the Force, the Ridges of the Waves of the Soul World. However, there are also in the Ocean of Good.


**Russian:**

Круг разделен на 12 Волн. Волна делится на 30 меток. 15 одного склона и 15 другого склона. Вот на гребне и качается этакий планктон. Пролетая поперек такой Волны, Мир пересекает такие потоки планктона элементарных ДНК. Более высокоемкие ДНК и Души не прочь подкрепиться. А после закуски послеобеденных отдых. В зависимости от энергосилы ДНК и Души все проходит по-разному. Учитывая, что Мир, Вселенная, Система движутся, а элементарные ДНК тоже развиваются, греются Сутью Мира, то в зависимости на какую Волну попадает мир, тот планктон и достается. В целом - чем дальше в лес, тем толще партизаны.

**English:**

The circle is divided into 12 waves. The wave is divided into 30 labels. 15 of one slope and 15 of another slope. Here on the ridge and swinging this plankton. Flying across such a wave, the world crosses such flows of plankton elementary DNA. Higher-density DNA and the soul wouldn't mind a little refreshment. And after an afternoon snack, rest. Depending on the power of the DNA and the Soul, things go differently. Taking into account that the World, the Universe, the System are moving, and elementary DNA are also developing, warming up to the Essence of the World, then depending on what Wave the world gets, that plankton and gets. In general - the further into the forest, the thicker the partisans.


**Russian:**

Осваивание таких элементарных ДНК не проходит бесследно, ДНК, как и любой наевшийся организм, перестраивается. Разгоняется, нагревается. Если материя Тушки к этому не подготовится заранее, то происходит выгорание, увядание, варение материи, распад, причем изнутри! А вы как думали происходит увеличение ДНК и Души? Как и везде - притоком Сути! Пока Суть отличается - видно как она пристроила ДНК цепочку, говорят - мутация. Если ДНК Тушки оказалась сильнее и Тушка не сломалась, то цепочка вируса распадается и становится частью ДНК Тушки, она становится ярче!

**English:**

Mastering such elementary DNA does not pass without a trace, DNA, like any full-blooded organism, is rebuilt. It accelerates, it heats up. If the matter of the Carcass does not prepare for this in advance, there is a burnout, withering, boiling matter, decay, and from within! What did you think the DNA and Soul increase? Like everywhere else, a stream of Souty ('Сути' - possibly 'Sufi')! As long as the Essence is different - you can see how it has attached the DNA chain, they say - a mutation. If the DNA of the Carcass is stronger and the Carcass is not broken, then the virus chain breaks down and becomes part of the DNA of the Carcass, it becomes brighter!


**Russian:**

Собственно аминокислоты, почему кислоты? Свойства кислот! Есть нейтральные материальные вещества, Кислоты - окислители и Щелочи - восстановители! Базовые элементарные 3 кирпичика Мира: Водород - восстанавливает материю, Кислород - окисляет, расщепляет, активизирует. Гелий - нейтральная Суть. Остальное - усложненные производные. ДНК - Суть Мира Душ - Добро - Свет - высвечивает, Освещает, дробит страхи, освещает темные углы, Мир становится более светлым! Именно так и поступают кислоты - расщепляют материю Светом Сути для создания лучше организованной структуры, более насыщенной. Собственно они и есть Суть, Добро! Окисление - Горение - Согревание - Освещение Сутью Мира Душ. Именно поэтому Люди так любят огонь - это щелочка в Мир Душ. А тлен от распадающейся на элементы в этом Свете материи и есть чад, уголь, копоть. Схождение Святого огня - Свет Добра запускает процесс окисления. Открывается щелочка, причем у православных более "правильный" слоган для ее открытия, и Свет освещает, активирует фитильки, да и не только, всю материю, растительные фитильки вспыхивают и светятся некоторое время Добром, а потом, постепенно частота Добра падает и огонь становится материальным.

**English:**

The amino acids themselves, why acids? The properties of acids! There are neutral materials, acids are oxidizing agents and alkalis are reducing agents! The basic elementary 3 bricks of the World: Hydrogen - restores matter, Oxygen - oxidizes, splits, activates. Helium is a neutral Essence. The rest is complex derivatives. DNA - Essence of the World of Souls - Good - Light - illuminates, illuminates, crushes fears, illuminates dark corners, the World becomes brighter! This is exactly what acids do - they break down matter with Light of Sense to create a better organized structure, more saturated. In fact, they are the Essence, the Good! Oxidation - Burning - Warming - Lighting the Essence of the World of Souls. That's why People love fire so much - it's an alkali in the World of Souls. And the corpse from the decaying matter in this Light is the Chad, coal, soot. The descent of the Holy Fire - The Light of Good triggers the oxidation process. An alkali opens, and Orthodox have a more "correct" slogan for its opening, and Light illuminates, activates wicks, and not only, all matter, plant wicks flash and shine for some time Kind, and then, gradually the frequency of Kind falls and fire becomes material.


**Russian:**

Точно так же объясняется и самовозгорание! Когда выгорает только материя Тушки, точнее она распадается. Ибо Душа начинает освещать ее Добром слишком большой силы, такое СВЧ выжигает, точнее переводит ДНК Тушки в новое состояние, а материя не так быстро может меняться. Процесс окисления, распада, видимый огонь, ну еще дым и только одежда остается. Причем не тронутая. Так иногда горят на проповедях или молящиеся, чрезмерно. Требуют Добра себе и побольше - оно сходит, но как и деньги это надо уметь держать, и прямо на глазах окружающих выгорают - т.е. распадаются на элементы материи! Кто не до конца сгорел, бывают случаи - у того все внутри выжжено сильно. Все симптомы СВЧ внутри.

**English:**

It also explains the spontaneous ignition! When only Carcass matter burns out, it disintegrates. For the Soul begins to illuminate it with too much good power, such microwave burns out, more precisely transfers the Carcass DNA to a new state, and the matter can not change so quickly. The process of oxidation, decay, visible fire, well, more smoke and only clothes remain. And it's untouched. So sometimes it burns at sermons or praying too much. Require goodness to themselves and more - it goes, but as well as money it must be able to hold, and right in front of others burn out - ie fall apart into elements of matter! Who is not completely burned, there are cases - everything inside is burned out strongly. All symptoms of microwave inside.


**Russian:**

С Деньгами интересней, они вызывают внутренний огонь желания. Вот он и жжет. Причем Желания не столько обладателя, сколько завистливых и вожделеющих Тушек. Именно их огонь и жжет все сильнее и сильнее. Постепенно накопление Денег - постепенное обучение к этому огню и правилам обращения с ним. Быстрое получение - вспышка огня окружающих и Люди выгорают постепенно изнутри! Тушка чахнет.

**English:**

With Money more interesting, it causes an inner fire of desire. So it burns. And Desires are not so much the owner as the envious and lustful carcasses. It's their fire that burns harder and harder. Gradually accumulation of Money - a gradual training to this fire and the rules of handling it. Quick receipt - a flash of fire of others and people gradually burn out from the inside! The carcass is withering.


**Russian:**

Водород же, Суть мира материи, восстанавливает это и структуру возвращает в Мир материи. А вместе - Водород и Кислород - Энергия! Соединение их - больше Водорода - более материальная структура - вода пример, хотя это не химическая связь, энергетическая. И странности ее поведения хорошо об этом говорят. Самая тяжелая вода при +5 градусах. Водород, т.е материя становится абсолютной. Добро на материю влияет по минимуму, процессы жизни - движения Добра замирают. Именно поэтому для хранения продуктов, сохранения Тушек при медитации, в пещерах, где нет тлена - разложения, разделения, движения материи под Светом Добра - оптимальная температура +5.

**English:**

Hydrogen, the Essence of the World of Matter, restores this and returns the structure to the World of Matter. And together - Hydrogen and Oxygen - Energy! Connecting them - more Hydrogen - more material structure - water is an example, though it is not a chemical bond, energy. And the strangeness of its behavior speaks well of this. It's the heaviest water at +5 degrees. Hydrogen, i.e. matter becomes absolute. Goodness affects matter to a minimum, the processes of life - the movements of Good stop. That's why for storage of products, preservation of Carcasses during meditation, in caves where there is no aphids - decomposition, separation, movements of matter under the Light of Good - optimal temperature is +5.


**Russian:**

Да и вода не просто так - она запоминает вибрации Мира, строится по ним. Именно поэтому писать вилами по воде, толочь воду, не просто пустые занятия.

**English:**

And water for a reason - it remembers the vibrations of the world, built on them. That's why writing with forks on water, sharpening water, not just empty classes.


**Russian:**

Это отлично знали Алхимики, АлХимия и была полной наукой о материи на основе волновых процессов! И философский камень сделали, но у него были некоторые ограничения. Именно поэтому и повелось прикалываться над Алхимиками, что они все философский камень ищут. А они знали одно ограничение его использования, и были ответственными, а вот обычная наука уже не считала Душу чем-то ценным и ее траты на работу философского камня ограничением. Да только где те, кто им рискнул воспользоваться? Еще в Римской империи знали, как медь переделать в Золото - было множество Медных рудников, и были места получения Золота! Но пришло время и строжайшим указом все записи об этом были ликвидированы еще до нашей эры. Записи-то остались и у Восточных мудрецов, да обычный человек не понимает таких слов. Зато развитие, наука! А развитие - это расщепление веревочки на составляющие, которые уже не веревочка.

**English:**

This was well known by Alchemists, Alchemists and was the complete science of matter based on wave processes! And the Philosopher's Stone was made, but it had some limitations. That's why they made fun of Alchemists, that's why they were all looking for the Philosopher's Stone. And they knew one limitation of its use, and were responsible, but ordinary science no longer considered the Soul something valuable and its spending on the work of the Philosopher's Stone limitation. But where are those who risked using it? Back in the Roman Empire, they knew how to turn copper into gold - there were many Copper Mines, and there were places to get Gold! But the time has come and by the strictest decree all records about it were liquidated even before our era. The records are left and the Eastern sages, but the average person does not understand such words. But development, science! And development is the splitting of the rope into components that are no longer rope.


**Russian:**

Магия была несколько иной направленности - это уже была практика, наука о применении знаний! Причем тоже на основе волновых процессов. Если Алхимия изучала, то Магия применяла! И умели Словом менять мир! "Абракадабра" - не что иное, как подслушанная универсальная формула. Идеальное слово. Вперед и назад читается одинаково, не имеет смысла для материального люда. Да и ученики Магов ленились, запоминали только технологии Магии, Суть потерялась, магия стала забываться, а тут еще и Земля в тень пошла.

**English:**

Magic was a slightly different direction - it was already practice, the science of applying knowledge! And it was also based on wave processes. If Alchemy was studying, Magic was applying! And they knew how to change the world by the Word! "Abracadabra" was nothing more than an overheard universal formula. It's the perfect word. Forwards and backwards, it reads the same, makes no sense to the material man. And the students of Magic were lazy, they remembered only the technologies of Magic, the essence was lost, magic began to be forgotten, and then the Earth went into the shadows.


**Russian:**

А правильное оно такое - "А-б-р-а-д-а-К-а-д-а-р-б-А" - вот и 12 слойный Мир. И тот самый Ка, и Ра, и Да, и аД, и бА. Знакомые слоганы, да помним ли мы откуда взялись? К и есть центр - 13, Черта-К - Предел, Чертоги, Портал, Дверь говорят нынче.

**English:**

And the right one - "A-b-r-a-d-a-K-a-d-a-r-b-A" - is the 12-layer World. Yes, and that Ka, and Ra, and Ad, and Ba. Familiar slogans, do we remember where we came from? K is the center - 13, Devil's-K - Limit, Devil's, Portal, Door talk nowadays.


**Russian:**

Вот отсюда и 12+1 - 12 Апостолов или Координаторов и 1, т.е. 13 - Христос или Спаситель, Черта-К. От края А до центра К - 7 меток. Это струны, ряд, радуга Волны. Собственно координатор тоже центр своего Мира и там такая же "АбрадаКадарбА". Это слово сбалансировано, целостно, и именно поэтому оно имеет влияние на Мир, как его матрица и Суть. Делая акценты на том или ином элементе можно выбрать нужную часть и изменить Мир. Каждому символу соответствует свой неповторимый набор Музыки. Их Совокупность и есть Музыка Высших Сфер. Играющий на струнах Мира эту Музыку, строит Мир вокруг по своему усмотрению. Вот этой игрой и занималась Магия. Нынче бы сказали - волновой генетикой. Именно поэтому Музыка может менять, усиливать, разгонять и подавлять ДНК, а так же останавливать, т.е. убивать Тушку.

**English:**

This is where 12+1 - 12 Apostles or Coordinators and 1, i.e. 13 - Christ or Savior, Devil's K. From edge A to center K, there are 7 marks. These are strings, row, wave rainbow. The coordinator is also the center of his world and there is the same AbradaKadarbA. This word is balanced, integral, and that is why it has an influence on the World as its matrix and Essence. Making accents on this or that element, you can choose the necessary part and change the World. Each symbol has its own unique set of Music. Their Complex is the Music of the Higher Spheres. Playing this Music on the strings of the World, builds the World around at your discretion. That's what Magic was doing. They'd say wave genetics nowadays. That's why Music can change, amplify, disperse and suppress DNA, as well as stop, ie kill the Carcass.


**Russian:**

Отсюда же, от Черта-К и появилось то самое - Черт, а работники ЧертаК поменьше рангом - Черти. Обычные сотрудники Портала. А хвосты и все остальное им потом дорисовал возбужденный Ум Людей. Да и какой аД? Обычное отмывание от наносной материальности. Как отмывают? Да по пониманию людей в тазах, греют их на огне! Когда в Христианстве был постулат оборота Душ, тогда это было нормально и естественно, что каждого вернувшегося из материального мира надо отмыть от страстей мирских, смывалось наносное, оставалось наработанное. Это и осуществлялось по прохождению Черта-К, К принимает на Черте, приходящий отчитывается, показывает накопленное, отмывается от наносного и обратно. Помогали естественно Черти, сотрудники Черты. Постулат кругооборота, когда убрали, то сразу очищение, отмывание превратилось в вечное варение, ибо дальше то пути нет! Вот так сотрудники приемного пункта реабилитации стали Чертями нынешними. А 13 как и была Чертой, так и стала нынешней Чертовой, да при дюжине.

**English:**

From here, from HellK, the same thing came up - Hell, and the workers of HellK are less ranked - Hell. Ordinary employees of the Portal. And the tails and everything else, they'd then draw the excited Mind of Men. And what's the Hell? The usual money laundering. How do they launder? You know, people in their pelvis warm them up on fire! When in Christianity there was a postulate of the circulation of souls, then it was normal and natural that everyone who came back from the material world should be washed away from the passions of the worldly, washed away all sorts of things, there was still work to be done. This was done after the passage of the Hell - K, K receives on the Hell, coming to report, shows the accumulated, washed away from the miserable and back. The Devils, the Devil's employees, naturally helped. The postulate of the circle, when removed, then immediately purification, washing turned into eternal jam, for there is no further way! That's how the rehab staff became the Devils of today. And 13 was a Hell, and it became a Hell, yes, a dozen.


**Russian:**

Еще Антихристом пугают, придет де и все порушит. Верно - придет и скажет лишнее. АнтиХрист - это всего лишь по Не Христианству, но Православие и христианство не одно и тоже! Так что АнтиХрист - это все что противоречит и с чем не согласна Христианская Церковь, не более. А вот то что это у верующих христиан выбьет мир под ногами их веры, ну так жизнь такая.

**English:**

The Antichrist is also frightened, he will come de and ruin everything. That's right, he'll come and say the wrong thing. Antichrist is not Christianity, but Orthodoxy and Christianity are not the same! So Anti-Christ is all that contradicts and disagrees with what the Christian Church, no more. And that it at believing Christians will beat out the world under feet of their belief, well such life is.


**Russian:**

Волны Слов, есть не что иное, как общение, обмен Волнами Сути. Некоторые свою волну считают более правильной и тогда пытаются поразить других объемом и числом волн, за которыми теряется Суть. С другой стороны, создавая Волну, постепенно множество окружающих подстраиваются под нее, синхронизируются и вот уже Волны Добра от внимательных слушателей возвращаются оратору и возносят его. И Он скользит по синхронным Волнам Добра Внимания Согласных Слушателей.

**English:**

Waves of Words, is nothing more than communication, exchange of Waves of Suthi. Some consider their wave is more correct and then try to hit others with the volume and number of waves, behind which the essence is lost. On the other hand, creating a wave, gradually a lot of others are adjusted to it, synchronized and now the waves of good from attentive listeners return to the speaker and raise it. And He slides on the synchronous Waves of Good Attention of attentive listeners.


**Russian:**

Опять же Монастыри, Церкви и другие организации собрания людей. Люди собираются, молятся, Веру меняют на Снискание Добра. Волны Сути Мира Душ перемещаются. Опять же удобный пункт сбора Добра от прихожан. Есть и мобильные пункты сбора - обереги, Иконы и т.д. Воздавая мольбу, Добро Души уходит в направлении оберега или образа Иконы, часть возвращается, а Душа тает.

**English:**

Again, there are monasteries, churches and other gathering organizations. People gather, pray, change their faith for the Legacy of Good. The waves of Souty ('Сути' - possibly 'Sufi') World Souls are moving. Again, a convenient gathering point for the good of the parishioners. There are also mobile collection points - amulets, icons, etc. When the prayer is answered, the Good Soul goes in the direction of the amulet or the icon, a part returns and the Soul dissolves.

**Russian:**

Амулеты, Зеркала мира, и другие заговоренные вещи сильны Добром Душ, именно его они перерабатывают, на нем и работают, а вы думали на чем? Вот жемчуг или драгоценности - в отсутствии носки теряют блеск, лоск, умирают - дайте им, покормите их Добром Души и они засияют.

**English:**

Amulets, Mirrors of the world, and other conspired things are strong Good Souls, it's what they recycle, it's what they work on, and you thought about what? Here are pearls or jewelry - in the absence of socks lose their luster, gloss, die - give them, feed them the Good Soul and they will shine.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-112.html)

© All rights reserved. The original author retains ownership and rights.

