title: Seattle - Consequences of a cataclysm, war or...
date: 2019-04-25
modified: Fri 19 Mar 2021 18:23:31 GMT
category:
tags: Seattle; catastrophe; thermonuclear war
slug:
authors: digitall-angell
from: https://digitall-angell.livejournal.com/973473.html
originally: Seattle - Consequences of a cataclysm war or.html
local: Seattle - Consequences of a cataclysm war or_files/
summary: The claim is that Seattle townsfolk organized the terraforming of the entire city area, but somehow I don't really believe such stories given the tools shown and the cyclopean size of the task.
status: published

#### Translated from:

[https://digitall-angell.livejournal.com/973473.html](https://digitall-angell.livejournal.com/973473.html)

**Russian:**

Очень странные фотографии Сиэтла начала 1900-х годов.

**English:**

Very strange photos of Seattle in the early 1900s.

**Russian:**

[Источник](https://mashable.com/2015/08/18/building-seattle/?fbclid=IwAR070Pv_Ly7A4THPFyuFuUrW6X0TrzyrX3DxnTu0l8yjUBJMc44HZiOigE4#Fahkj_Oviqqn) утверждает, что горожанами были организованы работы по терраформированию всей площади города, но как-то не очень верится в такие рассказы с учетом показанных инструментов и циклопических размеров задачи.

**English:**

[Source](https://mashable.com/2015/08/18/building-seattle/?fbclid=IwAR070Pv_Ly7A4THPFyuFuUrW6X0TrzyrX3DxnTu0l8yjUBJMc44HZiOigE4#Fahkj_Oviqqn) claims the terraforming of the entire city area was organized by the townspeople, but somehow I don't really believe such stories, given the tools shown and the cyclopean size of the task.

**Russian:**

Как думаете, что тут произошло?

**English:**

What do you think happened here?

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4757991_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4757991_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4757991/4757991_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4757991/4757991_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4758508_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4758508_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4758508/4758508_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4758508/4758508_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4758028_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4758028_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4758028/4758028_1000.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4758028/4758028_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4758767_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4758767_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4758767/4758767_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4758767/4758767_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4758817_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4758817_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4758817/4758817_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4758817/4758817_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4759291_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4759291_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4759291/4759291_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4759291/4759291_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4759504_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4759504_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4759504/4759504_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4759504/4759504_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4759621_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4759621_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4759621/4759621_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4759621/4759621_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4759962_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4759962_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4759962/4759962_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4759962/4759962_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4760135_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4760135_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4760135/4760135_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4760135/4760135_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4760494_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4760494_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4760494/4760494_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4760494/4760494_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4760819_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4760819_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4760819/4760819_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4760819/4760819_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4760935_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4760935_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4760935/4760935_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4760935/4760935_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4761101_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4761101_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4761101/4761101_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4761101/4761101_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4761557_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4761557_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4761557/4761557_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4761557/4761557_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4761746_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4761746_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4761746/4761746_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4761746/4761746_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4761913_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4761913_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4761913/4761913_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4761913/4761913_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4762159_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4762159_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4762159/4762159_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4762159/4762159_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4762563_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4762563_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4762563/4762563_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4762563/4762563_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4762849_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4762849_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4762849/4762849_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4762849/4762849_original.jpg)

<!-- Local
[![](Seattle - Consequences of a cataclysm war or_files/4762990_1000.jpg#clickable)](Seattle - Consequences of a cataclysm war or_files/4762990_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4762990/4762990_original.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4762990/4762990_original.jpg)

**Russian:**

По теме:

**English:**

More on the topic (in the original Russian site):

**Russian:**

- [ГОСТы предков. Как выглядят засыпанные этажи допотопных мегалитов?](https://digitall-angell.livejournal.com/969102.html)

- [Индустриально развитая цивилизация существует на Земле [десятки] [тысяч] лет](https://metaisskra.com/blog/industrialno-razvitaya-civilizaciya/)

- [Античная империя: жилища гигантов](https://metaisskra.com/blog/antichnaya-imperiya-zhilishcha-gigantov/)

- [Гиганты, люди и переходный генотип. Три расы до разделения](https://metaisskra.com/kniga-pamyati/glava-28-tri-rasy-giganty/)

- [Краткая история Атлантиды на примере Петербурга](https://metaisskra.com/blog/kratkaya-istoriya-atlantidy-na-primere-peterburga/)

- [Почему некоторые здания погрузились под землю и захват древних оборонных комплексов](https://metaisskra.com/blog/6071/)

- [Отголоски глобальной цивилизации](https://metaisskra.com/blog/otgoloski-globalnoj-civilizacii/)

- [Гиганты со всего мира до уплотнения реальности](https://metaisskra.com/blog/giganty-so-vsego-mira-do-uplotneniya-realnosti/)

- [Потоп](https://metaisskra.com/tag/potop/)

[Заброшенные города предков. Слияние и разделение веток реальности 300 лет назад](https://metaisskra.com/blog/6522/):

[Необъяснимые факты о древних цивилизациях и катаклизмах](https://metaisskra.com/blog/neobyasnimye-fakty-o-drevnih-civilizaciyah-i-kataklizmah/):


**English:**

- [Ancestral GHOSTS. What do the buried floors of prehistoric megaliths look like?](https://digitall-angell.livejournal.com/969102.html)

- [An industrially advanced civilization has existed on Earth for tens of thousands of years](https://metaisskra.com/blog/industrialno-razvitaya-civilizaciya/). Read RoHT's [English translation of this piece here](./quarried-earth-an-industrially-developed-civilization-has-existed-on-earth-for-tens-of-thousands-of-years.html)

- [Ancient Empire: Dwellings of Giants](https://metaisskra.com/blog/antichnaya-imperiya-zhilishcha-gigantov/)

- [Giants, humans, and the transitional genotype. Three races before division](https://metaisskra.com/kniga-pamyati/glava-28-tri-rasy-giganty/)

- [A brief history of Atlantis by the example of St. Petersburg](https://metaisskra.com/blog/kratkaya-istoriya-atlantidy-na-primere-peterburga/)

- [Why some buildings have sunk underground and the takeover of ancient defense complexes](https://metaisskra.com/blog/6071/)

- [Echoes of global civilization](https://metaisskra.com/blog/otgoloski-globalnoj-civilizacii/)

- [Giants from around the world before reality compacted](https://metaisskra.com/blog/giganty-so-vsego-mira-do-uplotneniya-realnosti/)

- [Flood](https://metaisskra.com/tag/potop/)

<!-- Local
![](Seattle - Consequences of a cataclysm war or_files/4608463_1000.jpg#resized)
-->

![][(https://ic.pics.livejournal.com/digitall_angell/38000872/4608463/4608463_1000.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4608463/4608463_original.jpg)

*[Abandoned Ancestral Cities. The merging and splitting of branches of reality 300 years ago](https://metaisskra.com/blog/6522/)*

<!-- Local
![](Seattle - Consequences of a cataclysm war or_files/20170511-02968-001.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2017/05/20170511-02968-001.jpg#resized)

*[Unexplained facts about ancient civilizations and cataclysms](https://metaisskra.com/blog/neobyasnimye-fakty-o-drevnih-civilizaciyah-i-kataklizmah/)*

**Russian:**

Реальность многомерна, мнения о ней многогранны. Здесь показана лишь одна или несколько граней, каждая из которых должна рассматриваться как        частный случай. Не стоит принимать этот текст за истину в последней инстанции, ибо истина безгранична. Тренируем свои барометры, учимся добывать,    анализировать и дополнять информацию по внутреннему резонансу

**English:**

Reality is multidimensional, opinions about it are multifaceted. Only one or more facets are shown here, each of which must be treated as a special case. Do not take this text as the ultimate truth, for the truth is infinite. Train your barometers, learn to extract, analyze, and complete information by internal resonance 

© All rights reserved. The original author retains ownership and rights.

