title: Media of the Past - Part 4
date: 2014-04-02 00:00:02
modified: Thu 14 Jan 2021 12:25:06 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/29387.html
originally: Media of the Past - Part 4.html
local: Media of the Past - Part 4_files/
summary: It is suggested that liquid was poured into these vessels. It then evaporated and created the conditions for arc arc to burn somewhere between those two prongs. A kind of mercury discharge lamp, with a spectrum similar to that of the sun.
status: published

### Translated from:

[https://pro-vladimir.livejournal.com/29387.html](https://pro-vladimir.livejournal.com/29387.html)

**Russian:**

Носители информации прошлого. Часть 3.
[http://pro-vladimir.livejournal.com/29127.html](http://pro-vladimir.livejournal.com/29127.html)

**English:**

Information Media of the Past. Part 3. [http://pro-vladimir.livejournal.com/29127.html](http://pro-vladimir.livejournal.com/29127.html)

**Russian:**

отсюда: [http://nnmuz.com/interesting/other/34486-suschestvuet-tehnologiya-s-pomoschyu-kotoroy-dvd-mogut-hranit-do-72-terabayt-informacii.html](http://nnmuz.com/interesting/other/34486-suschestvuet-tehnologiya-s-pomoschyu-kotoroy-dvd-mogut-hranit-do-72-terabayt-informacii.html)

**English:**

From here: [http://nnmuz.com/interesting/other/34486-suschestvuet-tehnologiya-s-pomoschyu-kotoroy-dvd-mogut-hranit-do-72-terabayt-informacii.html](http://nnmuz.com/interesting/other/34486-suschestvuet-tehnologiya-s-pomoschyu-kotoroy-dvd-mogut-hranit-do-72-terabayt-informacii.html)

**Russian:**

> Существует технология, с помощью которой DVD могут хранить до 7,2 терабайт информации
>
> Всё дело в технологии. Обычные DVD содержат информацию, записанную на манер виниловой пластинки, на двумерной дорожке. Коротковолновый лазер плотнее «упаковывает» информацию на Blu-ray, но принцип тот же. Разработчики пытаются делать «многослойные» диски, чтобы ещё плотнее «утрамбовать» на диск содержимое.
>
> Исследователь из Мельбурна Джеймс Чон разработал совершенно иную технологию. Он предлагает производителям более сложную лазерную систему, основанную на использовании набор различных вол и поляризаций лазерного луча. Эта технология позволит во много раз увеличить объём для хранения данных на дисках.
>
> Сама по себе эта система не сложна. Трудности возникли с выбором подходящего материала для производства. Авторы пришли к выводу, что лучше всего с задачей справятся золотые наностержни разных размеров и разной ориентации. При записи лазер «подплавляет» только те стержни, направление которых такое же, как и направление поляризации, остальные стержни остаются нетронутыми. Таким образом можно на одном и том же участке записать несколько разных наборов данных.

**English:**

> There is a technology with which DVDs can store up to 7.2 terabytes of information
> 
> It's all about technology. Conventional DVDs contain information recorded in the manner of a vinyl record, on a two-dimensional track. The shortwave laser "packs" the information more densely on Blu-ray, but the principle is the same. Developers are trying to make "multi-layered" discs to "cram" even more densely packed content onto the disc.
>
> Melbourne researcher James Chon has developed a completely different technology. He offers manufacturers a more sophisticated laser system based on the use of a set of different wavelengths and polarizations of the laser beam. This technology will allow for many times the storage capacity of disk drives.
>
> The system itself is not complicated. Difficulties arose with the choice of a suitable material for production. The authors concluded that gold nanorods of different sizes and orientations would do the job best. During recording, the laser "fuses" only those rods whose direction is the same as the polarization direction, the other rods remain untouched. Thus it is possible to record several different data sets in the same area.

<!-- Local
[![](Media of the Past - Part 4_files/7bFJKCAdUmdsP_3raMH6mSVenpk4JwFSyw_h0yRdKMzjjskHzU0D7nofpI9.html)](Media of the Past - Part 4_files/1334688186_dvd.jpg)
-->

[![Image no longer available](http://nnmuz.com/uploads/posts/2012-04/thumbs/1334688186_dvd.jpg#clickable)](http://nnmuz.com/uploads/posts/2012-04/thumbs/1334688186_dvd.jpg)

**Russian:**

о как. Еще немного, еще чуть чуть. Давеча поглядывал в сторону молекулярного состава бронзы. Фантазировал на предмет, а как, собственно, мог бы быть "изъеден" и стурктурирован сам информационный слой. Решётку там уложили особым способом, али как компоненты бронзы заставили разместится. Ведь в широком понимании бронза того "времени", это по большей части медь+олово (хотя в этом не уверен). Могли и этот состав как-то упорядочить. В том плане, что чередованием меди-олова формировать запись.

Или поры выжечь лишние.

**English:**

Oh, boy. A little more, a little more. Recently I was looking at the molecular composition of bronze. I was fantasizing about how the information layer could have been "eaten away" (etched) and structured. The lattice was placed in a special way or it was made to fit the bronze components. Bronze of that time is widely regarded as mostly copper + tin (I am not so sure about this, though). They could have streamlined this composition somehow. In the sense that by alternating copper-tin to form the record. Or burn out the pores.

**Russian:**

что лучше всего с задачей справятся золотые наностержни разных размеров и разной ориентации. При записи лазер «подплавляет» только те стержни, направление которых такое же, как и направление поляризации, остальные стержни остаются нетронутыми. Таким образом можно на одном и том же участке записать несколько разных наборов данных.

**English:**

The best way to do this is to use gold nanorods of different sizes and orientations. When recording, the laser "fuses" only those rods whose direction is the same as the polarization direction, the other rods remain untouched. Thus it is possible to record several different data sets in the same area.

**Russian:**

Что нам мешает нанострежни разной ориентации заменить на нанопоры разной ориентации?

**English:**

Whats to stop us from replacing nanorods of different orientation with nanopores of different orientation?

**Russian:**

Еще пробел с шатанием чем читали и писали. У нас это сделано мы знаем как. А тогда? Набор кристаллов? Так оно же как-то должно иметь форму и содержание обособленное. А у нас всё с приставкой "драг" раз по несколько разламывалось для ювелирных потребностей.

Ювелир-нивелир. Сплошной велир.

Dmitrij:

> ЛиРа - структурированное излучение, когерентное по-нашему.

> Ра - излучение? Ли - структура. Ли-тература. Ли-ра - балалайка такая. Литьё, лимузин, лихач и прочее. Даже лишнее - выбранное ненужное, т.е. тот же структурированный подход, выбор.
>
> Лишения - потеря чего-то нужного.
>
> Изготовителей ювелирки звали Ю-Де.

**English:**

More wobble space than read and write. We have done it, we know how. And then? A set of crystals? So it must somehow have a detached form and content. And we had everything with the prefix "drag" (droog? Friend?) broken down several times for jewelry needs.

A jeweler's jeweler. Solid velier.

Translator's note: In the passage above, pro_vladimir quotes Dmitrij_an as Dmitrij_an points out that one or more of the syllables of various Russian words which express 'added structure', such as 'cast' (as in foundry work) will likely be 'Li'. Similarly, in Russian but not English, words that express removal or selective removal also often have a 'Li' syllable.

**Dmitrij:**

> LiRa is structured radiation. We would say 'coherent radiation'.
> 
> Ra - radiation? Li - structure. Li - literature. Li - ra is a balalaika like that. Casting, limousine, dashing, and so forth. 
>
> Even superfluous - meaning 'assessed as unnecessary' contains the same structured approach and expresses choice.
> 
> Deprivation is the loss of something that is necessary.
>
> The makers of the jewelry were called U-De.

<!-- Local
[![](Media of the Past - Part 4_files/IMG_5692_zpsb92037f2.html)](Media of the Past - Part 4_files/IMG_5692_zpsb92037f2.jpg)
-->

[![](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5692_zpsb92037f2.jpg#clickable)](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5692_zpsb92037f2.jpg)

<!-- Local
[![](Media of the Past - Part 4_files/IMG_5277_zps7b8d522a.html)](Media of the Past - Part 4_files/IMG_5277_zps7b8d522a.jpg)
-->

[![](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5277_zps7b8d522a.jpg#clickable)](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5277_zps7b8d522a.jpg)

<!-- Local
[![](Media of the Past - Part 4_files/IMG_5267_zps7790ea25.html)](Media of the Past - Part 4_files/IMG_5267_zps7790ea25.jpg)
-->

[![](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5267_zps7790ea25.jpg#clickable)](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5267_zps7790ea25.jpg)

<!-- Local
[![](Media of the Past - Part 4_files/IMG_5681_zps9b381436.html)](Media of the Past - Part 4_files/IMG_5681_zps9b381436.jpg)
-->

[![](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5681_zps9b381436.jpg#clickable)](http://i1364.photobucket.com/albums/r737/tiulyapa/2nd/IMG_5681_zps9b381436.jpg)

**Russian:**

*Газоразрядные лампы*

**English:**

*Gas discharge lamps*

**Russian:**

Подсказывают, что в данные сосуды наливалась жидкость. Которая испарялась и создавала условия горения дуги. Вон где-то меж тех двух штырей. Некое подобие газоразрядной ртутной лампы. Со спектром излучения близким к солнечному. Оными и светили при "просмотре" на "зеркала".

**English:**

It is suggested that liquid was poured into these vessels. It then evaporated and created the conditions for arc arc to burn somewhere between those two prongs. A kind of mercury discharge lamp, with a spectrum similar to that of the sun. These were used to shine the light on the "mirrors".

© All rights reserved. The original author retains ownership and rights.

