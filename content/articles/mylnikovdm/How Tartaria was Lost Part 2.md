title: How Tartaria Died - Part 2 - Existing Theories
date: 2014-09-06 08:17
modified: Thu 14 Jan 2021 07:22:36 GMT
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: I am well aware that most people, under the influence of constant propaganda in the education system and the media, find it very difficult to believe that such a gigantic catastrophe could have happened 200 years ago. In the beginning, it was hard for me to believe it, too.
status: published
originally: Как погибла Тартария, ч2.zip
local: How Tartaria was Lost Part 2_files/

### Translated from:

Как погибла Тартария, ч2.zip, [https://mylnikovdm.livejournal.com/950.html](https://mylnikovdm.livejournal.com/950.html) and [http://mylnikovdm.livejournal.com/1088.html](http://mylnikovdm.livejournal.com/1088.html)

**Russian:**

После опубликования первой части было получено некоторое количество комментариев, в том числе на тему того, что происхождение данных следов давно объяснено другими теориями. Поэтому в начале второй части я хочу сделать некоторые свои замечания к этим комментариям и теориям.

**English:**

After the publication of the first part, some comments were received, including the fact that the origin of these traces had long been explained by other theories. Therefore, at the beginning of the second part, I want to make some comments on these comments and theories.

**Russian:**

Некто belyaefff сделал следующий комментарий:

> "Господи, ну и бред. Все эти гривы - это следы от пресного моря, которое образовалось в результате закупорки ледником Северного Ледовитого океана. Великие Сибирские реки, упершись в ледник, образовали гигантский пресный водоём, который нёс свои воды на юго-запад, к Аралу и Каспию. Потоки воды пробурили равнину и кое-где засыпали песком огромные узкие полости. Потом на этих песках выросли сосны. 

**English:**

Someone made the following comment:

> "God, this is crazy. All these 'manes' are traces of the fresh sea, which was formed by the blockage of the Arctic Ocean by the glacier. Great Siberian rivers, leaning on the glacier, formed a giant fresh water body, which carried its waters to the south-west, to the Aral Sea and the Caspian Sea. The streams of water drilled the plain and somewhere covered with sand huge narrow cavities. Then pines grew on these sands.

**Russian:**

> Вот, почитайте: 

[http://www.poteplenie.ru/doc/karn-golfst5.htm](http://www.poteplenie.ru/doc/karn-golfst5.htm)

**English:**

> Here, read this:

[http://www.poteplenie.ru/doc/karn-golfst5.htm](http://www.poteplenie.ru/doc/karn-golfst5.htm)

**Russian:**

Теория образования «Сибирского пресного моря» из-за ледника, который перекрывал стоки сибирских рек в Северный Ледовитый океан, несомненно, заслуживает внимания, вот только к рассматриваемым следам она не имеет никакого отношения.

**English:**

The theory of the formation of the "Siberian Fresh Sea" because of the glacier that blocked the flow of Siberian rivers into the Arctic Ocean is undoubtedly worthy of attention, but it has nothing to do with the traces under consideration.

**Russian:**

Во-первых, она не объясняет, почему следы идут практически параллельно и под углами близкими к 66 градусам, то есть углу наклона земной оси к плоскости эклиптики?

**English:**

Firstly, it does not explain why the traces go almost parallel and at angles close to 66 degrees, i.e. the angle of inclination of the Earth's axis to the ecliptic plane?

**Russian:**

Во-вторых, непонятно почему эти потоки, как и в случае с теорией следов от ледника, игнорируют имеющийся рельеф местности. Особенно если учесть, что наши следы как раз пересекают линию водораздела между реками Иртыш и Обь.

**English:**

Secondly, it is not clear why these flows, as in the case with the theory of glacier tracks, ignore the existing terrain. Especially if we take into account that our tracks just cross the watershed line between the rivers Irtysh and Ob.

**Russian:**

В-третьих, эта теория не объясняет, почему на протяжении более 200 км. Следы имеют практически одинаковую ширину в 5 км, а потом почему-то резко начинают разливаться. Причём на снимках очень хорошо видно, что следы №1 и №2 начинаются от реки Обь и фактически заканчиваются у реки Иртыш. А как же эти воды дальше текли в Арал и Каспий? Почему мы не видим аналогичных траншей на территории Казахстана и Оренбургской области? Если у нас действительно было пресное море, воды которого должны были стекать в Арал и Каспий, то узкие промоины должны были образоваться только в районе хребта водораздела между реками. При этом правая часть должна была быть покрыта водой, а значит там течения были подводные. Но чем дальше от хребта, тем след должен быть более широким с обеих сторон, напоминая своей формой песочные часы. У нас же форма следа совсем другая, след расширяется только со стороны «вытекания». Кроме того, ниже я также покажу на конкретных примерах, что форма следа ну никак не соответствует руслу, которое могло быть промыто рекой или течением.

**English:**

Third, this theory does not explain why over 200 km. The traces are almost the same width of 5 km, and then for some reason they begin to spill sharply. And the images clearly show that tracks #1 and #2 start from the Ob River and actually end at the Irtysh River. And how did these waters flow further to Aral and Caspian Sea? Why can't we see similar trenches in Kazakhstan and Orenburg region? If we really had a fresh sea, the waters of which should have flowed into the Aral and the Caspian Sea, then narrow trenches should have formed only in the area of the watershed ridge between the rivers. At the same time, the right part should have been covered with water, so there were underwater currents. But the farther from the ridge, the wider the trail should be on both sides, reminiscent of the shape of the hourglass. The shape of the trail is quite different, the trail widens only from the "outflow" side. Besides, below I will also show with concrete examples that the shape of the footprint does not correspond to the channel that could have been washed by a river or current.

**Russian:**

И, наконец, в четвёртых, эта теория никак не объясняет наличие множества более мелких параллельных вытянутых следов, а также большое количество круглых озёр метеоритного происхождения на западе Курганской и Юго-востоке Челябинской областей. Каким образом образовались данные объекты, если следовать теории сброса воды в Арал и Каспий?

**English:**

And finally, in the fourth, this theory does not explain the presence of many smaller parallel elongated traces, as well as a large number of round meteorite lakes in the west of the Kurgan and south-eastern Chelyabinsk regions. How did these objects were formed, if we follow the theory of water discharge into the Aral and Caspian Sea?

**Russian:**

Второй контраргумент, который привели сразу несколько человек, что данные метеориты, если они были ледяные, не должны были долететь до поверхности Земли и взорваться в воздухе, как Тунгусский метеорит, либо должны были оставить оплавленные следы, картеры и отвалы вокруг них, если это были каменные или металлические метеориты. В связи с этим я решил сделать некоторое отступление от основной темы и разобрать этот вопрос подробнее, тем более, что понимание этих моментов потребуется для дальнейшего объяснения.

**English:**

The second counter-argument, which led several people at once, was that these meteorites, if they were icy, should not have flown to the surface of the Earth and exploded in the air like the Tunguska meteorite, or should have left melted tracks, 'sumps'? and dumps around them if they were stone or metal meteorites. I therefore decided to make some deviation from the main subject and to look at the matter in more detail, especially since understanding these points would be necessary for further explanation.

**Russian:**

### Как падают метеориты?

**English:**

### How do meteorites fall?

**Russian:**

Общая картина падения метеоритов особых разногласий не вызывает. Некий объект из камня, льда или их смеси на большой скорости влетает в атмосферу Земли, где происходит его торможение. При этом объект очень интенсивно нагревается об атмосферу Земли, а также испытывает различные сильные нагрузки из-за давления плотных слоёв атмосферы и быстрого неравномерного нагрева (спереди нагревается сильнее и быстрее, чем сзади). Некоторые из метеоритов полностью разрушаются и сгорают в плотных слоях атмосферы, вообще не долетая до земли. Некоторые взрываются, разрушаясь на множество мелких частей, которые могут упасть на поверхность Земли. А наиболее крупные и прочные могут долететь до поверхности Земли и, ударившись в неё, оставить характерный кратер в месте падения.

**English:**

The overall picture of the meteorite fall is not very controversial. An object made of stone, ice or a mixture of them flies into the Earth's atmosphere at high speed, where it slows down. At the same time, the object heats up very intensely against the Earth's atmosphere, and also experiences various heavy loads due to the pressure of dense layers of the atmosphere and rapid uneven heating (the front heats up more strongly and faster than the back). Some of the meteorites are completely destroyed and burned in the dense layers of the atmosphere without reaching the ground at all. Some explode, crumbling into many small parts that can fall to the surface. And the largest and strongest can fly to the surface of the Earth and leave a characteristic crater at the point of impact.

**Russian:**

Но у этого процесса есть масса особенностей, о которых, увы, не рассказывают ни в школе, ни даже в большинстве ВУЗов.

**English:**

But this process has a lot of features that, alas, are not told about either at school or even in most universities.

**Russian:**

Во-первых, большое заблуждение, что все метеориты, пролетая через плотные слои атмосферы, будут разогреваться до больших температур и светиться. Тут нужно вспоминать курс физики из средней школы, касающийся процесса изменения фазовых состояний воды, то есть, переход из твердого состояния в жидкое, и далее в газообразное. Особенность этого процесса в том, что вы не сможете нагреть лёд до температуры выше температуры плавления, а получившуюся жидкость выше температуры кипения. При этом пока лёд плавиться или жидкость выкипает, они будут потреблять тепловую энергию, но их нагрева происходить не будет, поступающая энергия будет уходить на изменение фазового состояния вещества. К этому нужно добавить, что теплопроводность водяного льда достаточно низкая, поэтому лёд вполне может таять на поверхности айсберга, в тоже время оставаясь достаточно холодным внутри. Именно благодаря этому свойству ледяные айсберги, отколовшись от ледяного панциря Антарктиды, могут проплывать тысячи морских миль и спокойно пересекать линию экватора.

**English:**

First, it is a big misconception that all meteorites flying through the dense layers of the atmosphere will warm up to high temperatures and glow. Here it is necessary to recollect the course of physics from high school, concerning the process of changing the phase states of water, that is, the transition from solid to liquid state, and then to gaseous. The peculiarity of this process is that you can not heat the ice to a temperature above the melting point, and the resulting liquid above boiling point. While the ice is melting or the liquid is boiling, they will consume heat energy, but they will not be heated, the incoming energy will be used to change the phase state of the substance. To this we should add that the thermal conductivity of the water ice is quite low, so the ice can melt on the surface of the iceberg, while remaining cold enough inside. It is thanks to this property ice icebergs, breaking away from the ice shell of Antarctica, can swim thousands of nautical miles and quietly cross the equator line.

**Russian:**

Когда метеорит представляет собой большой кусок водяного льда, то при его прохождении через плотные слои атмосферы будут работать те же самые законы, что и для ледяного айсберга в водах экватора. Да, он будет нагреваться об атмосферу, да, перед ним будет создаваться зона повышенного давления и температуры из-за сжатия воздуха быстро двигающимся телом. Но вот его поверхность не будет нагреваться выше температуры плавления льда, а на поверхности будет тонкая плёнка растаявшей воды, которая будет тут же испаряться и уноситься от поверхности метеорита набегающим воздушным потоком, расходуя на это энергию нагретого воздуха и охлаждая его. При этом до более высоких температур может нагреться не сам метеорит, а окружающий его воздух. Я даже допускаю, что окружающий воздух может нагреться до температур, когда начнётся ионизация и свечение газа, но это свечение будет не очень сильным, больше похожим на северное сияние, а не на яркую ослепляющую вспышку, как от каменного или металлического болида (типа Челябинского в 2013 году). Это связано с тем, что нашу земную атмосферу в основном составляют газы, которые при ионизации не дают интенсивного свечения.

**English:**

When a meteorite is a large piece of water ice, the same laws that apply to an ice iceberg in the equator will work when it passes through dense layers of atmosphere. Yes, it will heat up against the atmosphere, yes, a zone of high pressure and temperature will be created in front of it due to air compression by a rapidly moving body. But its surface will not be heated above the melting point of the ice, and on the surface will be a thin film of melted water, which will immediately evaporate and be carried away from the surface of the meteorite by the raging air stream, using the energy of heated air and cooling it. It is not the meteorite itself that can be heated to higher temperatures, but the air surrounding it. I even assume that the surrounding air can be heated to temperatures when ionization and gas glow begins, but this glow will not be very strong, more like a northern glow, rather than a bright blinding flash like a stone or metal bolide (like Chelyabinsk in 2013). This is due to the fact that our Earth's atmosphere is mainly composed of gases, which do not give an intense glow when ionized.

**Russian:**

Существует зависимость температуры плавления и температуры кипения от давления окружающей среды. Но при этом зависимость температуры плавления от давления очень низкая. Для увеличения температуры плавления водяного льда на 1 градус Цельсия необходимо увеличить давление среды более чем на 107 Н/м2. Зависимость температуры кипения от давления более ярко выражена, но и тут рост не такой значительный, как кажется. При увеличении давления до 100 атмосфер температура плавления составит всего 309.5 градусов Цельсия. 

[http://www.sci.aha.ru/ALL/b17.htm](http://www.sci.aha.ru/ALL/b17.htm)

**English:**

There is a dependence of melting and boiling point on ambient pressure. However, the dependence of melting point on pressure is very low. To increase the melting point of water ice by 1 degree Celsius, it is necessary to increase the ambient pressure by more than 107 N/m2. The dependence of the boiling point on pressure is more pronounced, but the increase is not as significant as it seems. If the pressure increases to 100 atmospheres, the melting point will only be 309.5 degrees Celsius.

[http://www.sci.aha.ru/ALL/b17.htm](http://www.sci.aha.ru/ALL/b17.htm)

**Russian:**

Поскольку мы имеем дело с незамкнутым объёмом, то давление атмосферы перед метеоритом не сможет достичь величин порядка 100 атмосфер, тем более, что разогревание воздуха будет компенсироваться таянием льда и испарением воды на поверхности метеорита.

**English:**

Since we are dealing with an open volume, the pressure of the atmosphere before the meteorite will not be able to reach values of about 100 atmospheres, especially since the heating of the air will be compensated by the melting of ice and evaporation of water on the surface of the meteorite.

**Russian:**

Другими словами, поверхность нашего метеорита не может нагреться до нескольких тысяч градусов, а значит нет и предпосылок для его взрыва. Если ледяной метеорит недостаточно крупный, то он просто растает в атмосфере, если же он достаточно крупных размеров, то он спокойно долетит до поверхности Земли, а дальше всё уже зависит от угла, под которым он ударится в поверхность. Если угол достаточно крутой, то будет удар с образованием кратера. Если траектория будет идти под очень пологим углом, как в нашем случае, то мы получим длинный вытянутый след. При этом в процессе прорезания следа, метеорит будет продолжать таять, превратившись в конечном итоге в волну селевого потока, в котором вода из метеорита будет смешана с срезанным с поверхности грунтом, причём вся эта селевая масса будет продолжать двигаться вдоль траектории падения метеорита, при этом растекаясь в ширь, пока окончательно не растеряет кинетическую энергию, что мы и наблюдаем на снимках.

**English:**

In other words, the surface of our meteorite cannot be heated to several thousand degrees, which means there are no prerequisites for its explosion. If the ice meteorite is not large enough, it just melts in the atmosphere, but if it is large enough, it will quietly reach the surface of the Earth, and then everything depends on the angle at which it hits the surface. If the angle is steep enough, it will strike with a crater. If the trajectory goes at a very gentle angle, as in our case, we will get a long elongated trajectory. In the process of cutting the trail, the meteorite will continue to melt, eventually becoming a wave of debris flow in which the water from the meteorite will be mixed with the ground cut from the surface, and all this debris will continue to move along the trajectory of the meteorite fall, while melting into a wider area until it finally loses its kinetic energy, as we see in the images.

**Russian:**

В каких случаях может произойти взрыв подобного метеорита? Только в тех случаях, когда метеорит неоднороден и в нём имеются вкрапления твёрдых минералов или достаточно крупные и глубокие трещины и полости. Твердые минералы в большинстве своём имеют лучшую теплопроводность, а также могут нагреться до более высоких температур, чем лёд. В результате через эти вкрапления и их нагрев тепло будет попадать внутрь метеорита, где лёд также начнёт интенсивно таять, а вода испаряться, создавая давление перегретого пара внутри метеорита, который и должен в конечном итоге его разорвать на части.

**English:**

In what cases could a meteorite like this explode? Only when the meteorite is heterogeneous and has hard mineral inclusions or sufficiently large and deep cracks and cavities. Hard minerals, for the most part, have better thermal conductivity and can also be heated to higher temperatures than ice. As a result, through these impurities and their heating, heat will enter the interior of the meteorite, where the ice will also begin to melt intensively, and water will evaporate, creating pressure from superheated steam inside the meteorite, which should eventually tear it apart.

**Russian:**

Теоретически возможен взрыв метеорита, который состоит не только из водяного льда, а имеет крупные вкрапления заледеневшего газа или жидкости, который имеет другую температуру плавления. В этом случае данный газ может растаять раньше, образовав полости, которые и приведут к разрушению метеорита. Но я сильно сомневаюсь, что подобные объекты могут возникать в естественных условиях, разве что их кто-то создаст искусственно.

**English:**

In theory, a meteorite explosion is theoretically possible, which consists not only of water ice, but also has a large build-up of frozen gas or liquid, which has a different melting point. In this case, the gas may melt earlier, forming cavities that will destroy the meteorite. But I highly doubt that such objects can occur in natural conditions, unless someone creates them artificially.

**Russian:**

Не всё так просто и с каменными или металлическими метеоритами. При падении в атмосферу земли с большой скоростью они будут разогреваться до очень высоких температур в тысячи градусов. При этом маленькие объекты будут полностью расплавляться и «сгорать» в атмосфере, а очень большие долетать до поверхности Земли и оставлять на ней весьма заметные следы с массой катастрофических последствий, начиная от гигантских наводнений и кончая извержениями супервулканов в местах пробоя земной коры.

**English:**

It's not that simple with stone or metal meteorites. When the earth falls into the atmosphere at high speed, they will warm up to very high temperatures of thousands of degrees. In this case, small objects will melt completely and "burn" in the atmosphere, and very large objects will fly to the surface of the Earth and leave very visible traces on it with a mass of catastrophic consequences, ranging from giant floods and ending with super volcanic eruptions in places where the Earth's crust breaks down.

**Russian:**

Но самое интересное происходит со средними метеоритами. Метеориты размерами близкими к Челябинскому-2013 или чуть большие будут не просто взрываться в атмосфере или долетать до её поверхности и оставлять на ней кратер. При достижении критических значений температуры и давления в них будет запускаться цепная ядерная реакция разрушения ядер вещества, аналогичная той, что происходит в ядерной бомбе. В результате мы будем получать воздушный ядерный взрыв достаточно большой мощности. Наблюдаемые на космических снимках характерные воронки диаметрами до 13 км говорят о мощности взрывов сравнимых с термоядерным бомбам мощностью от 100 до 200 мегатонн в тротиловом эквиваленте.

**English:**

But the most interesting thing happens with average meteorites. Meteorites the size close to Chelyabinsk-2013 or almost large will not just explode in the atmosphere or fly to its surface and leave a crater on it. When the critical values of temperature and pressure are reached, they will trigger a chain nuclear reaction of destruction of the substance's nuclei, similar to what happens in a nuclear bomb. As a result, we will get an airborne nuclear explosion of sufficient power. The characteristic funnels with diameters up to 13 km observed on space images show the explosive power comparable to thermonuclear bombs with power from 100 to 200 megatons in TNT equivalent.

**Russian:**

Благодаря невежеству и пропаганде большинство людей думает, что ядерную бомбу можно сделать только из ядерных радиоактивных материалов, типа урана или плутония. Причём весьма многие, как оказалось, считают, что если вы соберёте критическую массу урана или плутония, то Вы сразу же получите ядерный взрыв.

**English:**

Because of ignorance and propaganda, most people think that a nuclear bomb can only be made from nuclear radioactive materials like uranium or plutonium. And a lot of people, as it turned out, believe that if you collect a critical mass of uranium or plutonium, you will immediately get a nuclear explosion.

**Russian:**

Уран или плутоний мы используем только потому, что для запуска цепной реакции, приводящей к ядерному взрыву, их нужно очень небольшое количество, которое достаточно легко можно доставить к выбранной нами цели. При этом вовсе не достаточно просто соединить два куска урана или плутония с докритической массой, чтобы получить взрыв. Когда у вас имеется критическая масса урана или плутония цепная реакция запускается, он начинает очень интенсивно нагреваться и плавиться, но, увы, ядерного взрыва при этом не происходит. Чтобы произошёл взрыв, необходимо резко изменить скорость течения цепной реакции распада ядер радиоактивного вещества. Радиоактивные части ядерного заряда располагаются в специальной капсуле в виде секторов сферы. Когда нам требуется подорвать ядерный заряд, то происходит специально рассчитанный объёмный взрыв обычной взрывчатки, который толкает все части в центр сферы, где они соединяются при резко возросших за счёт обычного взрыва температуре и давлении, и вот только тогда мы получаем ядерный взрыв. Именно в умении получать подобный объёмный взрыв только в нужном нам месте и только в нужное нам время и состоит вся колоссальная сложность создания ядерной бомбы, для чего требуется произвести огромное количество расчётов. Так что накопить необходимое количество урана или плутония не самая сложная часть в создании ядерной бомбы.

**English:**

We only use uranium or plutonium because we need a very small amount of it to trigger a chain reaction leading to a nuclear explosion, which can be easily delivered to the target we choose. It is not enough just to connect two pieces of uranium or plutonium to the pre-critical mass to make an explosion. When you have a critical mass of uranium or plutonium, the chain reaction starts to heat up and melt very intensely, but alas, no nuclear explosion occurs. In order for an explosion to occur, you must drastically change the rate of flow of the chain reaction of decay of the radioactive nuclei. Radioactive parts of the nuclear charge are placed in a special capsule as sectors of a sphere. When we need to detonate a nuclear charge, there is a specially calculated volume explosion of ordinary explosives, which pushes all the parts into the center of the sphere, where they join together at a temperature and pressure that have risen sharply due to the usual explosion, and only then we get a nuclear explosion. It is in the ability to obtain such a volumetric explosion only in the right place and at the right time that we have the enormous difficulty of creating a nuclear bomb, which requires a huge number of calculations. So accumulating the right amount of uranium or plutonium is not the hardest part in building a nuclear bomb.

**Russian:**

Когда мы имеем дело с каменным или металлическим метеоритом среднего размера, то за счёт его разогрева до очень высоких температур и возникающего вследствие этого высокого давления, в нем могут создаться условия, которые также приведут к запуску цепной реакции распада ядер вещества. Мы не используем этот способ получения ядерных взрывов только потому, что наши технологии не позволяют нам перемещать в нужное место с нужной скоростью каменные глыбы весом несколько миллионов тонн. Сам метеорит при этом практически полностью разрушается, то есть, на месте падения такого метеорита и его взрыва мы будем наблюдать только классическую воронку от ядерного взрыва, но не будем видеть кратеров или других следов от как от обычных метеоритов.

**English:**

When we deal with a medium-sized stone or metal meteorite, by heating it to very high temperatures and the resulting high pressure, conditions can be created that will also trigger a chain reaction of the decay of the substance's nuclei. We do not use this method to produce nuclear explosions simply because our technology does not allow us to move several million tons of stone blocks at the right speed to the right place. The meteorite itself is almost completely destroyed, i.e., we will only see a classic nuclear explosion crater, but we will not see craters or other tracks like conventional meteorites at the site where such a meteorite falls and explodes.

**Russian:**

Хочу ещё раз подчеркнуть, что для того, чтобы при падении метеорита получился ядерный взрыв, он должен лететь с нужной скоростью и иметь определённую массу. То есть, какой попало метеорит не будет давать подобного эффекта. Если масса или скорость метеорита недостаточны, либо он влетает под очень крутым углом, а значит идёт по короткой траектории через атмосферу к поверхности Земли, то мы получим удар о поверхность и классический кратер. Если метеорит слишком крупный, то из-за соотношения площади поверхности к объёму вещества он также не сможет достичь критических параметров температуры и давления для инициации ядерного взрыва.

**English:**

I want to emphasize once again that in order for a meteorite to have a nuclear explosion, it must fly at the right speed and have a certain mass. I mean, any meteorite wouldn't have that effect. If the mass or speed of the meteorite is insufficient, or if it flies at a very steep angle, and thus follows a short trajectory through the atmosphere to the surface of the Earth, we will get a blow to the surface and a classic crater. If the meteorite is too large, because of the ratio of surface area to volume, it will also not be able to reach the critical parameters of temperature and pressure to initiate a nuclear explosion.

**Russian:**

### Миф о последствиях ядерных взрывов.

**English:**

### Myth about the consequences of nuclear explosions.

**Russian:**

Прежде чем перейти к одной из главных тем, касающихся датировок этих катастрофических событий, я хочу коснуться ещё одной важной темы, которая так же прозвучала в нескольких комментариях. Если опустить эмоции, то суть этих комментариев в том, что большинство людей не верит в то, что 200 лет назад могла произойти массированная ядерная бомбардировка, последствия которой мы сейчас не ощущаем и не фиксируем. Особенно в части радиации.

**English:**

Before turning to one of the main topics concerning the dating of these catastrophic events, I would like to touch upon another important topic, which was also mentioned in several comments. Omitting emotion, the essence of these comments is that most people do not believe that 200 years ago there could have been a massive nuclear bombing, the consequences of which we do not now feel or record. Especially with regard to radiation.

**Russian:**

Первый миф состоит в том, что радиационное заражение после ядерной бомбардировки будет держаться очень долго. На самом деле это не так. В момент ядерного взрыва, действительно, образуется мощный поток альфа-частиц и нейтронов, то есть, проникающей радиации, облучение которым смертельно опасно. При наземном ядерном взрыве у нас также образуется воронка с кратером из оплавленного вещества земной коры, поверхность которого также может достаточно долгое время оставаться радиоактивной, поскольку все металлы и минералы имеют свойство «накапливать» радиацию, то есть, от проникающей радиации, образовавшейся в момент взрыва, в них образуются радиоактивные изотопы, которые сами начинают «фонить». От людей, которые участвовали в ликвидации последствий аварии в Чернобыле я знаю, что они первым делом стремились избавиться от любых металлических предметов, включая золотые зубные протезы, именно по этой причине. А вот органические вещества или почва очень быстро теряют остаточную радиоактивность.

**English:**

The first myth is that the radiation contamination after the nuclear bombing will last a very long time. Actually, it won't. The moment of a nuclear explosion, indeed, a powerful flow of alpha particles and neutrons is formed, that is, penetrating radiation, irradiation of which is deadly dangerous. In a ground nuclear explosion, we also have a crater funnel of molten matter from the Earth's crust, the surface of which may also remain radioactive for quite a long time, because all metals and minerals have the property of "accumulating" radiation, that is, from the penetrating radiation formed at the time of the explosion, they form radioactive isotopes, which themselves begin to "background". I know from the people who participated in the Chernobyl accident that the first thing they tried to do was to get rid of any metal objects, including gold dentures, for this reason. But organic matter or soil loses its residual radioactivity very quickly.

**Russian:**

Когда мы имеем дело с воздушными ядерными взрывами, то никаких оплавленных воронок от них не образуется и радиоактивное заражение территории от них минимально.

**English:**

When we deal with airborne nuclear explosions, there are no melted funnels from them and the radioactive contamination of the territory from them is minimal.

**Russian:**

Высокий радиоактивный фон и очень длительные последствия радиоактивного заражения в зоне аварии Чернобыльской АЭС вызваны тем, что там был не ядерный взрыв, а обычный, вследствие которого радиоактивное вещество из реактора было выброшено из реакторной зоны и рассеяно в атмосфере, а потом выпало на землю. Причем количество радиоактивного вещества в ядерном реакторе во много раз больше, чем в ядерной бомбе. При ядерном взрыве происходит совсем другой процесс.

**English:**

The high radioactive background and the very long consequences of radioactive contamination in the Chernobyl accident zone are due to the fact that there was not a nuclear explosion, but a conventional one, as a result of which radioactive material from the reactor was released from the reactor zone and dispersed in the atmosphere and then dropped to the ground. The amount of radioactive material in a nuclear reactor is many times greater than in a nuclear bomb. In a nuclear explosion, there is a very different process.

**Russian:**

В качестве примера можно также привести тот факт, что на территориях городов Хиросимы и Нагасаки в Японии, которые подверглись ядерной бомбардировке со стороны США в 1945 году, в настоящее время следы радиоактивного заражения минимальны, эти города густо заселены, о ядерных взрывах напоминают только мемориальные комплексы. А ведь прошло не 200, а всего 70 лет.

**English:**

As an example, we can also mention the fact that in the territories of Hiroshima and Nagasaki cities in Japan, which were bombed by the U.S. in 1945, at present the traces of radioactive contamination are minimal, these cities are densely populated, only memorial complexes remind of nuclear explosions. But it has not been 200 years, but only 70.

**Russian:**

Те, кто ещё не знаком со статьёй о термоядерном сносе зданий Всемирного торгового центра в Нью-Йорке 11 сентября 2001 года, может ознакомиться со следующей статьёй 

**English:**

Those who are not yet familiar with the article on the thermonuclear demolition of the World Trade Center in New York on September 11, 2001, may read the following article:

[http://anonymouse.org/cgi-bin/anon-www.cgi/http://trueinform.ru/3808.html](http://anonymouse.org/cgi-bin/anon-www.cgi/http://trueinform.ru/3808.html)

**Russian:**

Открывать её следует через анонимайзер, как сделано у меня в ссылке, поскольку сайт [http://www.911-truth.net](http://www.911-truth.net), где автор разместил свои иллюстрации, оказался внесённым в реестр запрещённых сайтов (это к вопросу о «свободе слова» и к тому, что США, якобы, не контролирует власть в России...).


**English:**

You should open it through an anonymizer, like I did in the link above, because the actual site link - [http://www.911-truth.net](http://www.911-truth.net) - where the author placed his illustrations, appeared on the register of forbidden sites (this refers to the question of "freedom of speech" and to the fact that the USA, allegedly, does not control power in Russia...).

**Russian:**

В данной статье автор достаточно убедительно, с массой фактов, доказывает, что в центре Нью-Йорка для сноса небоскрёбов были применены три подземных термоядерных заряда. Для нас же важен тот факт, что если мы сейчас пройдёмся по этой территории, то мы обнаружим только весьма незначительное превышение уровня радиации над естественным фоном.

**English:**

In this article the author proves convincingly enough, with a mass of facts, that three underground thermonuclear charges were used to demolish skyscrapers in the center of New York. What is important to us, however, is that if we walk through this area now, we will find only a very small excess of radiation over the natural background.

**Russian:**

При ядерной бомбардировке, несомненно, кроме радиоактивного заражения должны быть и другие последствия, в том числе климатические и экологические. На отсутствие этих последствий также указывают некоторые комментаторы. Но весь фокус в том, что на самом деле эти последствия были, но по определённым причинам мы сейчас о них ничего не знаем, хотя есть масса фактов, которые на эти последствия указывают. Подробнее все эти факты я буду разбирать ниже, сейчас же только скажу, что на рубеже 18 и 19 веков имеет место очень существенный климатический сдвиг, который можно охарактеризовать как начало малого ледникового периода.

**English:**

In a nuclear bombardment, there must of course be consequences other than radioactive contamination, including climatic and environmental ones. The absence of these consequences is also pointed out by some commentators. But the whole trick is that there were, in fact, these consequences, but for certain reasons we do not know anything about them now, although there are many facts that point to these consequences. I will look at all of these facts in more detail below, but now I will only say that at the turn of the 18th and 19th centuries there was a very significant climatic shift, which can be described as the beginning of a small ice age.

**Russian:**

### Когда же произошла катастрофа?

**English:**

### When did the accident happen?

**Russian:**

Я прекрасно понимаю, что большинству людей, под влиянием постоянной пропаганды в системе образования и СМИ очень сложно поверить, что подобная гигантская катастрофа могла произойти 200 лет назад. В начале мне тоже было сложно в это поверить. Есть ведь, якобы, масса свидетельств о том, как заселялась Сибирь в 17 и 18 веке, как строились крепости. Например, в Челябинской области были построены в 1736 году Кызылташская, Миасская (в районе села Миасское, Красноармейский район, а не города Миасс), Чебаркульская, Челябинская крепости, в 1737 Еткульская крепость. В 1742 году Уйская. Об этом имеется достаточно подробная статья 

**English:**

I am well aware that most people, under the influence of constant propaganda in the education system and the media, find it very difficult to believe that such a gigantic catastrophe could have happened 200 years ago. In the beginning, it was hard for me to believe it, too. There is supposedly a lot of evidence about how Siberia was settled in the 17th and 18th centuries, how fortresses were built. For example, in the Chelyabinsk region were built in 1736 Kyzyltash Fortress, Miass Fortress (in the village Miass, Krasnoarmeysky district, not the city of Miass), Chebarkul Fortress, Chelyabinsk, in 1737 Etkul Fortress. In 1742 Uyskaya fortress. There is a rather detailed article about it.

[http://resources.chelreglib.ru:6005/el_izdan/kalend2011/kreposti.htm](http://resources.chelreglib.ru:6005/el_izdan/kalend2011/kreposti.htm) 

**Russian:**

в которой есть очень интересные иллюстрации.

**English:**

which has very interesting illustrations.

**Russian:**

Если смотреть на сохранившиеся планы крепостей, то мы видим, что это именно крепости, построенные по всем канонам передовой фортификационной науки того времени, форты вынесены наружу за линию стен, чтобы можно было обстреливать нападающих под стенами, вокруг земляной вал и ров. Только стены построены из дерева, а не из камня.

**English:**

If we look at the preserved plans of the fortresses, we can see that these are exactly the fortresses, built according to all the canons of advanced fortification science of the time, the forts are taken outside the line of walls, so that it was possible to bombard the attackers under the walls, around the earth wall and the moat. Only walls are built of wood, not stone.

**Russian:**

В другой статье можно прочитать историю Усть-Уйской крепости 

**English:**

In another article you can read the history of Ust-Ui fortress.

[http://www.kurgangen.org/History%20of%20Kurga/Tselinny%20region/Ust-Uyskoe%20selo/UstUyskoe%20krepost/](http://www.kurgangen.org/History%20of%20Kurga/Tselinny%20region/Ust-Uyskoe%20selo/UstUyskoe%20krepost/)

**Russian:**

которая располагалась на территории современной Курганской области. Там особенно интересен следующий фрагмент: 

**English:**

which was located on the territory of modern Kurgan region. The following fragment is especially interesting there:

**Russian:**

> «В 1805 году казаков 7 крепостей Исетской провинции (Челябинской, Миасской, Чебаркульской, Еткульской, Еманжелинской, Кичигинской, Коельской), переселили в укрепления Оренбургской линии, в крепости: Таналыцкая, Уртазымская, Кизильская, Магнитная, Усть-Уйская и редуты: Калпацкий, Тереклинский, Орловский, Березовский, Грязнушинский, Сыртииский, Верхнекизильский, Спасский, Подгорный, Саларский и другие. Количество переселенных составляло 1181 человек, в основном это были казаки и малолетки. Капралы, урядники и зауряд-офицеры меняли место службы с меньшим энтузиазмом.»

**English:**

> "In 1805, Cossacks moved 7 fortresses of the Iset province (Chelyabinsk, Miass, Chebarkul, Yetkul, Emanzhelin, Kichigin, Koel) to fortifications of the Orenburg line: Tanalitskaya, Urtazymskaya, Kizilskaya, Magnitnaya, Ust-Uiskaya and redoubts: Kalpatskiy, Tereklinskiy, Orlovskiy, Berezovskiy, Gryaznushinskiy, Syrtiiskiy, Verhnekizilskiy, Spasskiy, Podgorny, Salarskiy and others. The number of displaced persons was 1,181, mainly Cossacks and young children. Corporals, uriadniks and ordinary officers changed the place of duty with less enthusiasm".

**Russian:**

Всё это хорошо, ситуация поменялась, казаков решили переселить, крепости потеряли своё военное значение, вроде бы стали и не нужны. Вот только фокус в том, что подобные сооружения не могут исчезать совершенно бесследно, особенно когда речь идёт о населённых пунктах. После того, как построена крепость, она оказывает влияние на всю планировку остального поселения, которое возникает вокруг крепости. Причём оказывает это влияние даже после того, как крепость уже перестала существовать. Могло быть принято решение снести крепостные стены, может быть даже срыть земляные валы и засыпать рвы, вот только никто не будет заново прокладывать дороги и сносить уже построенные дома. При этом со временем старые дома могут заменяться новыми, но общая структура улиц и центральных проездов сохранится. При этом центральные проезды и улицы будут идти к воротам крепости, поскольку именно по ним изначально будут двигаться войска и обозы в крепость и из крепости.

**English:**

All this is good, the situation has changed, the Cossacks have decided to move, the fortresses have lost their military significance, they seem to have become and are not needed. The only trick is that such constructions cannot disappear completely without a trace, especially when it comes to populated areas. Once the fortress has been built, it has an impact on the entire layout of the rest of the settlement, which appears around the fortress. Moreover, it has an impact even after the castle has ceased to exist. It may have been decided to demolish the fortress walls, maybe even to dig earth ramparts and fill up the ditches, but nobody will ever rebuild the roads and demolish the houses already built. Over time, old houses may be replaced by new ones, but the general structure of streets and central passages will remain. In this case, the central passages and streets will go to the gates of the fortress, because it will initially move the troops and carts to and from the fortress.

**Russian:**

Если мы посмотрим на города в европейской части России, то там мы увидим именно такую картину. Московский, Нижегородский, Казанский кремли намертво определили структуру старого центра города. При этом везде основные магистрали ведут к крепостным воротам. Аналогичную картину мы наблюдаем и в тех городах, где крепости до наших дней не сохранились.

**English:**

If we look at cities in the European part of Russia, we will see exactly this picture there. The Moscow, Nizhny Novgorod and Kazan Kremlin have deliberately defined the structure of the old city centre. And everywhere the main highways lead to the fortress gates. We see a similar picture in those cities where fortresses have not survived to this day.

**Russian:**

Например, вот план тоже не сохранившейся крепости в городе Воронеж, который наложен на современную топографическую карту. Очень хорошо видно, что структура улиц, ведущих к воротам, а также центральная площадь сохранились до наших дней.

**English:**

For example, here is the plan of the not preserved fortress in the city of Voronezh, which is superimposed on a modern topographic map. It is very well seen that the structure of the streets leading to the gate, as well as the central square have been preserved to this day.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/4591_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/4591_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/4591/4591_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/4591/4591_original.jpg)

**Russian:**

На современном снимке со спутника эта структура также очень хорошо видна.

**English:**

This structure is also very well visible on a modern satellite image.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/4771_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/4771_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/4771/4771_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/4771/4771_original.jpg)

**Russian:**

При этом хочу обратить внимание, что улицы идут под сходящимися углами к центру, которым являлась крепость, хотя это и неудобно для строительства домов, особенно каменных. Но никто существующую структуру улиц ради удобства строительства менять не стал. Старые дома снесли, но новые вписали в те же самые улицы.

**English:**

At the same time, I would like to draw attention to the fact that the streets are at convergent angles to the center, which was the fortress, although it is inconvenient for the construction of houses, especially stone ones. But nobody changed the existing street structure for the convenience of construction. The old houses were demolished, but the new ones fit into the same streets.

**Russian:**

Город Смоленск, от крепости остались фрагменты стен. Сама крепость, кстати, разрушена во время войны 1812 года. Вот план 1898 года, а также современный вид со спутника. Вся структура улиц практически полностью сохранилась до наших дней.

**English:**

The city of Smolensk, fragments of the walls remained from the fortress. The fortress itself, by the way, was destroyed during the war of 1812. Here is the plan of 1898 as well as the modern view from a satellite. The entire structure of the streets is almost completely preserved to this day.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/5074_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/5074_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/5074/5074_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/5074/5074_original.jpg)

<!-- Local
[![](How Tartaria Was Lost Part 2_files/5238_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/5238_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/5238/5238_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/5238/5238_original.jpg)

**Russian:**

Иркутск, где строительство деревянного кремля было завершено в 1670 году. Имеется план 1784 года, когда кремль ещё существовал. На плане его территория залита тёмно-серым цветом (два квартала на самом берегу реки).

**English:**

Irkutsk, where the wooden Kremlin was completed in 1670. There is a plan for 1784, when the Kremlin still existed. On the plan its territory is covered with dark grey color (two blocks on the river bank).

<!-- Local
[![](How Tartaria Was Lost Part 2_files/5489_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/5489_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/5489/5489_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/5489/5489_original.jpg)

**Russian:**

А вот современный вид со спутника.

**English:**

And here's the modern satellite view.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/5835_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/5835_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/5835/5835_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/5835/5835_original.jpg)

**Russian:**

Очень хорошо видно, что старые здания снесены целыми кварталами, построено много новых современных зданий, но вся структура улиц осталась неизменной. Улицы до сих пор сходятся к воротам уже давно не существующего кремля.

**English:**

It is very clear that old buildings have been demolished in whole blocks, many new modern buildings have been built, but the entire street structure has remained unchanged. The streets still converge on the gates of the long-defunct Kremlin.

**Russian:**

Теперь посмотрим, что же мы видим на Южном Урале.

**English:**

Now let's see what we see in the South Urals.

**Russian:**

Сохранилось несколько схем Чебаркульской крепости, а также её описаний, из которых можно установить, что крепость находилась на мысу озера Чебаркуль, который расположен на северо-восточном берегу.

**English:**

Several schemes of the Chebarkul fortress and its descriptions have been preserved, from which it can be established that the fortress was located on the Cape of Lake Chebarkul, which is located on the north-eastern shore.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/7297_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/7297_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/7297/7297_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/7297/7297_original.jpg)

**Russian:**

Если смотреть на современный спутниковый снимок, то, во-первых, мы видим, что конфигурация берега озера заметно изменилась. Похожий мыс на северо-восточном берегу озера Чебаркуль есть, но форма его совсем другая. Также интересно, что мы видим другую топологическую структуру водоёмов, которая сегодня отсутствует, поскольку никаких малых озёр, которые изображены снизу от крепости на первом плане или справа от неё на втором, мы сегодня не наблюдаем. И если расхождение формы объектов ещё как-то можно списать на мастерство картографов, которые составляли данные планы и не смогли точно отобразить форму объектов, то изображение несуществующих объектов на подобных планах этим объяснить нельзя.

**English:**

If we look at the modern satellite image, firstly, we can see that the configuration of the lake shore has noticeably changed. There is a similar cape on the north-eastern shore of Lake Chebarkul, but its shape is quite different. It is also interesting that we see another topological structure of reservoirs, which is absent today, because we do not observe any small lakes, which are depicted from the bottom of the fortress in the foreground or from the right of the fortress in the second. And if the discrepancy in the shape of objects can somehow be attributed to the skill of mappers who made these plans and could not accurately display the shape of objects, the image of non-existent objects on such plans can not be explained by this.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/6661_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/6661_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/6661/6661_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/6661/6661_original.jpg)

**Russian:**

Но самое главное состоит в том, что сам город Чебаркуль напрочь игнорирует тот факт, что на данном месте когда-то была крепость! Центр города расположен совсем в другом месте, основные проезды ведут к современному центру города, не обращая никакого внимания на старую крепость. Мало того, к тому месту, где когда-то располагалась крепость, вообще нет старых дорог!

**English:**

But the most important thing is that the city of Chebarkul ignores the fact that this place was once a fortress! The city centre is located in a completely different place, the main passages lead to the modern city centre, without paying any attention to the old fortress. Not only that, there are no old roads to the place where the fortress was once located!

**Russian:**

Если внимательно посмотреть, то на правом, более детальном плане крепости, очень хорошо виден ров с водой вокруг стен, а также канал, идущий от середины верхней стороны к озеру Чебаркуль. На этом плане также показаны два ручья или небольших речки, которые впадают в место соединения канала со рвом. Чтобы там находилась вода, глубина рва должна была быть ниже уровня озера Чебаркуль, а это уже весьма ощутимая глубина и достаточно большой объём земляных работ. При этом землю, которую извлекали из рва, обычно насыпали в виде земляного вала по периметру будущей стены. Следы от подобного фортификационного сооружения должны были остаться на местности до наших дней, особенно если учесть, что на месте, где когда-то была крепость, никаких серьёзных строительных работ не велось, а значит, никто бы там заниматься выравниванием рельефа не стал.

**English:**

If you look closely, on the right, more detailed plan of the fortress, you can see very clearly the ditch with water around the walls, as well as the channel running from the middle of the upper side to Lake Chebarkul. This plan also shows two streams or small rivers that flow into the junction of the canal with the moat. In order for the water to be there, the ditch must be below the level of Lake Chebarkul, which is already a considerable depth and a rather large volume of excavation work. At the same time, the earth extracted from the ditch was usually poured as an earth rampart around the perimeter of the future wall. The footprints of such a fortification had to remain on the ground until today, especially taking into account that no serious construction works were carried out on the site where the fortress once stood, which means that no one would have been engaged in leveling the relief there.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/7152_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/7152_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/7152/7152_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/7152/7152_original.jpg)

**Russian:**

На данном месте сейчас расположена частная застройка, которую начали строить относительно недавно. Тогда же и проложили туда дороги. Никаких следов от земляных валов, рвов или канала от озера, которые являлись частью конструкции крепости, мы также не наблюдаем. Зато прямо над тем местом, где когда-то располагалась крепость, мы видим хорошо различимый круглый след диаметром порядка 430 метров!

**English:**

This place is now home to a private development, which began construction relatively recently. That's when they built the roads there. We also do not observe any traces of earth walls, ditches or canals from the lake, which were part of the construction of the fortress. On the other hand, right above the place where the fortress was once located, we can see a clearly visible circular trail with a diameter of about 430 meters!

**Russian:**

Вывод из всего этого следует только один. Если Чебаркульская крепость когда-то и существовала, то она была уничтожена, вместе с поселением, которое существовало вокруг неё. А тот город Чебаркуль, который мы видим сейчас, был построен заново в этом же месте, но уже с чистого листа, в начале 19 века, то есть уже после катастрофы. Поэтому уничтоженные постройки, включая крепость, не оказали никакого влияния на его планировку, на расположение его центра и на направление основных проездов, которые ведут к новому центру, а не к уничтоженной крепости.

**English:**

There's only one conclusion to all this. If the fortress of Chebarkul once existed, it was destroyed, along with the settlement that existed around it. And the city of Chebarkul that we see now was built anew in the same place, but from scratch at the beginning of the 19th century, that is, after the catastrophe. Therefore, the destroyed buildings, including the fortress, had no impact on its layout, on the location of its center and on the direction of the main passages that lead to the new center, not to the destroyed fortress.

**Russian:**

Далее переходим к Челябинской крепости. В интернете удалось найти следующий небольшой фрагмент с планом крепости, который повторяется практически во всех публикациях на эту тему, а также публикуется в нескольких книгах по истории Южного Урала и Челябинска, которые мне удалось найти.

**English:**

Then we move on to the Chelyabinsk fortress. On the Internet it was possible to find the following small fragment with the plan of a fortress which is repeated practically in all publications on this theme, and also is published in several books on history of Southern Ural Mountains and Chelyabinsk which I managed to find.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/7645_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/7645_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/7645/7645_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/7645/7645_original.jpg)

**Russian:**

Во-первых, настораживает тот факт, что фрагмент этот является вырезанным куском из какого-то документа, имеющего больший размер. Почему было не опубликовать весь документ целиком? Зачем понадобилось вырезать только этот кусок?

**English:**

First of all, it is alarming that this fragment is a cut out of some document of a larger size. Why wasn't the whole document published? Why did you have to cut out only this piece?

**Russian:**

Во-вторых, направление течения реки Миасс почему-то указано в другую сторону, поскольку на плане оно указано справа налево, в то время как на современных картах река Миасс в Челябинске течёт слева направо. Это означает, что на данном плане Юг наверху, а Север внизу. В принципе, многие средневековые карты на самом деле имеют именно такое, перевёрнутое вверх ногами, расположение сторон света, что само по себе является весьма интересным фактом. В нашем же случае возникает вопрос, должны ли мы в этом случае переворачивать и остальные планы крепостей, которые, очевидно, составлялись в одно время, или только план Челябинской крепости сделан в перевёрнутой системе? Но на приведённом ниже плане Миасской крепости направление течения указано также, как оно есть сегодня, да и на озере Чебаркуль с юго-западной стороны нет мест похожих на то, что изображено на плане (если попытаться его перевернуть).

**English:**

Secondly, the direction of the Miass River flow is somehow indicated in the other direction, as it is indicated from right to left on the plan, while on modern maps the Miass River in Chelyabinsk flows from left to right. This means that on this plan the South is at the top and the North is at the bottom. In principle, many medieval maps actually have this upside-down arrangement of the sides of the world, which in itself is a very interesting fact. In our case, the question arises: should we turn over the other plans of the fortresses, which, obviously, were made at the same time, or only the plan of the Chelyabinsk fortress is made in an upside-down system? But the following plan of the Miass fortress shows the direction of the current as it is today, and on the lake Chebarkul on the southwest side there are no places similar to what is depicted on the plan (if we try to turn it over).

**Russian:**

В итоге либо мы должны перевернуть только план Челябинской крепости, как показано ниже, либо место, которое показано на плане, не имеет никакого отношения к центру Челябинска, где якобы при раскопках найдены остатки старой крепости.

**English:**

As a result, either we have to turn over only the plan of the Chelyabinsk fortress, as shown below, or the place shown in the plan has nothing to do with the center of Chelyabinsk, where the excavations allegedly found the remains of the old fortress.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/7806_original.jpg#clickable)](How Tartaria Was Lost Part 2_files/7806_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/7806/7806_original.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/7806/7806_original.jpg)

**Russian:**

Отдельный вопрос, почему крепость в Челябинске расположена на правом южном берегу? Ведь согласно официальной легенде, крепости строились для защиты от набегов местных башкирских племён и киргиз-кайсаков с территории современного Казахстана, которые шли именно с южной стороны. При этом освоение шло с севера на юг. Считается, что Екатеринбург был основан в 1723 году, то есть на 13 лет раньше, чем наши крепости. Но крепость расположена таким образом, как будто враг находится на севере, а не на юге.

**English:**

A separate question, why is the fortress in Chelyabinsk located on the right south bank? After all according to an official legend, fortresses were under construction for protection against attacks of local Bashkir tribes and Kirghiz-Kaisakov from territory of modern Kazakhstan which went from the southern side. Thus development went from the north to the south. It is believed that Yekaterinburg was founded in 1723, that is 13 years earlier than our fortresses. But the fortress is located as if the enemy is in the north, not in the south.

**Russian:**

Имеется описание Челябинской крепости, которое сделал немецкий путешественник И. Г. Гмелин в 1742 году: 

> «Эта крепость также находится на реке Миясс, на южном берегу, она похожа на Миясскую, но побольше и окружена только деревянными стенами из лежащих бревен. Каждая стена имеет примерно 60 саженей. Она заложена вскоре после Миясской крепости, а имя получила от ближайшего к ней, находящегося выше на южной стороне реки леса, по-башкирски Челябе-Карагай».

**English:**

There is a description of the Chelyabinsk Fortress, which was made by a German traveler I. G. Gmelin in 1742:

> "This fortress is also located on the Miyass River, on the south bank, it is similar to Miyass, but larger and surrounded only by wooden walls of lying logs. Each wall has approximately 60 sazhens. It was laid soon after the Miyasskaya fortress, and got its name from the nearest to it, located higher on the southern side of the river forest, in Bashkir Chelyabe-Karagai style".

**Russian:**

Во-первых, это описание позволяет примерно определить размер стены в 120 метров (одна сажень равна 2.1336 метра).

**English:**

Firstly, this description allows us to estimate the size of the wall at 120 meters (one fathom is 2.1336 meters).

**Russian:**

Во-вторых, сделанное Гмелиным описание отличается от имеющегося плана крепости, на котором явно присутствует ров, соединявшийся с рекой Миасс, а также мощное внутреннее укрепление с фортами, выступающими на линии стен. Он упоминает только наружную стену, так называемые «надолбы», которые на плане обозначены буквой Н и закрывают крепость с трёх сторон (с четвёртой стороны крепость прикрывает река Миасс). Но с момента основания крепости в 1736 году прошло всего-то 6 лет! За это время крепость уже успели разобрать, а ров и валы заровнять? Или за 6 лет построили только внешнюю стену, а строить саму крепость, с валами, рвами и стенами ещё не приступали?  Теперь посмотрим на план города Челябинска 1910 года.

**English:**

Secondly, Gmelin's description differs from the existing plan of the fortress, which clearly includes a ditch connected to the Miass River, as well as a powerful internal fortification with forts protruding on the line of walls. He mentions only the outer wall, the so-called "nadolby", which on the plan is marked with the letter H and covers the fortress on three sides (on the fourth side the fortress covers the Miass River). But only 6 years have passed since the foundation of the fortress in 1736! During this time the fortress has already been dismantled, and the moat and ramparts have been leveled? Or have you built only the outer wall in 6 years, and you have not started to build the fortress itself, with the ramparts, moats and walls yet?  Now let's look at the plan of the city of Chelyabinsk in 1910.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/8015_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/8015_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/8015/8015_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/8015/8015_original.jpg)

**Russian:**

Река в центре Челябинска имеет совсем другую конфигурацию, чем на плане крепости. Как и в случае с Чебаркулём, планировка Челябинска напрочь игнорирует тот факт, что ещё 100 лет назад где-то возле моста была хорошо укреплённая крепость. Мы видим хорошо спланированное регулярное поселение, которое удобно строить на ровном месте, с нуля, но никак не план поселения, которое постепенно развивается начиная от укреплённого центра в виде крепости.

**English:**

The river in the centre of Chelyabinsk has a completely different configuration from the plan of the fortress. As in the case of Chebarkul, the layout of Chelyabinsk ignores the fact that just 100 years ago somewhere near the bridge was a well fortified fortress. We see a well-planned regular settlement, which is convenient to build on a flat spot, from scratch, but not a settlement plan, which gradually develops from the fortified center in the form of a fortress.

**Russian:**

Я взял размеры внешней стены, приведённые в описании Гмелина, равные 120 метрам, и исходя из предположения, что существующий мост через Миасс на улице Кирова находится в том же самом месте, где когда-то находился крепостной мост, сделал следующую схему на современном спутниковом снимке с Гугл-мап.

**English:**

I took the size of the outer wall, as described in Gmelin's description, equal to 120 meters, and based on the assumption that the existing bridge over Miass on Kirov Street is in the same place where the fortress bridge once stood, I made the following diagram in a modern satellite image from Google Map.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/8267_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/8267_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/8267/8267_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/8267/8267_original.jpg)

**Russian:**

Оранжевая линия, это наша внешняя деревянная стена с трёх сторон, рамзером 120х120 метров. Голубая линия — ров с водой вокруг стен. Жёлтый полупрозрачный прямоугольник — это основное укрепление. При этом красным цветом показана область, где велись археологические раскопки при строительстве нового краеведческого музея и якобы найдены останки старой крепости.

**English:**

Orange line, this is our external wooden wall on three sides, razmer 120x120 meters. The blue line is a moat with water around the walls. Yellow translucent rectangle is the main reinforcement. The red color shows the area where archaeological excavations were conducted during the construction of a new local history museum and allegedly found the remains of an old fortress.

**Russian:**

Общая планировка старой части города, структура улиц, а также многие здания остались неизменными с 1910 года, что очень хорошо видно при сравнении приведённого выше плана с данным снимком. При этом крепость никак не вписывается в данную планировку. Положение моста в данном месте объясняется тем, что тут самое узкое в центре города место на реке Миасс. Если же попробовать сместить крепость влево, чтобы её часть попадала в зону раскопок, показанную красным, то тогда мы должны сместить и мост тоже, поскольку на плане чётко видно, что у крепости имелось двое ворот с башнями по центру северной и южной стен, при этом мост находился с ними практически на одной линии. Но в этом случае либо мост был не в самом узком месте, что противоречит здравому смыслу, либо русло реки Миасс серьёзно изменилось с момента строительства крепости. Опять же, при смещении крепости вправо мы должны будем сместить и центральный проезд, который вёл к воротам крепости, что должно поменять всю планировку центра города..

**English:**

The general layout of the old part of the city, the street structure as well as many buildings have remained unchanged since 1910, which can be clearly seen when comparing the above plan with this image. At the same time the fortress does not fit into this layout in any way. The position of the bridge in this place is explained by the fact that it is the narrowest place in the city center on the Miass River. If we try to move the fortress to the left, so that its part gets into the excavation area, shown in red, then we must move the bridge too, because the plan clearly shows that the fortress had two gates with towers in the center of the northern and southern walls, and the bridge was almost in line with them. But in this case, either the bridge was not in the narrowest spot, which is contrary to common sense, or the Miass riverbed had changed significantly since the construction of the fortress. Again, if the fortress was moved to the right, we would have to shift the central passage leading to the gate of the fortress, which would have to change the entire layout of the city center...

**Russian:**

Переходим к крепости в селе Миасском. Удалось найти два разных изображения данной крепости.

**English:**

We go to the fortress in Miassky village. We managed to find two different images of this fortress.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/9703_original.jpg#clickable)](How Tartaria Was Lost Part 2_files/9703_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/9703/9703_original.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/9703/9703_original.jpg)

**Russian:**

Первый план нам интересен тем, что на нём обозначено направление на север, исходя из которого необходимо повернуть план примерно на 20 градусов по часовой стрелке.

**English:**

The first plan is interesting to us because it indicates the direction to the north, from which it is necessary to turn the plan by about 20 degrees clockwise.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/8951_original.jpg#clickable)](How Tartaria Was Lost Part 2_files/8951_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/8951/8951_original.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/8951/8951_original.jpg)

**Russian:**

Ещё более интересен второй план, который я повернул так, чтобы направление на север стало вертикальным. На нём изображена не только сама крепость, но ещё и посёлок рядом с крепостью, а также ров, который я залил голубым цветом вместе с рекой. Причём ров этот окружает не только крепость, но ещё и охватывает с двух сторон посёлок, соединяясь с рекой Миасс. Из этого, опять же, следует, что глубина рва должна была быть ниже уровня воды в реке Миасс. Также хорошо виден мост, который ведёт к воротам в центре стены.

**English:**

Even more interesting is the second plan, which I turned so that the direction to the north becomes vertical. It shows not only the fortress itself, but also the village next to the fortress, as well as the moat that I poured blue along with the river. This moat surrounds not only the fortress, but also covers both sides of the village, connecting with the Miass River. Again, it follows that the depth of the moat was to be below the water level in the Miass River. The bridge that leads to the gate in the centre of the wall is also clearly visible.

**Russian:**

А теперь посмотрим на современный план села Миасское. Если нанести то направление, которое должны были иметь стены крепости согласно первому плану, которое я показал красной линией, то мы увидим, что оно весьма сильно отличается от направлений улиц в сегодняшнем селе. Хорошо, предположим, что составитель первого плана ошибся и неточно указал направление на север. Я обрисовал фрагмент реки и каналы со второго плана, обозначил на нём положение моста, после чего вставил размеры исходя из того, что Миасская крепость была чуть меньше Челябинской. Размер Челябинской И.Г.Гмелин определил в 60 саженей, то бишь 120 метров, поэтому я взял для Миасской 100 метров.

**English:**

Now let's take a look at the modern plan for the village of Miasskoye. If we draw the direction that the walls of the fortress should have according to the first plan, which I showed with a red line, we will see that it is very different from the direction of the streets in today's village. Okay, let's assume that the compiler of the first plan made a mistake and inaccurately indicated the direction to the north. I drew a fragment of the river and channels from the second plan, marked on it the position of the bridge, and then inserted the dimensions based on the fact that the Miass fortress was slightly smaller than the Chelyabinsk one. The size of Chelyabinsk I.G.Gmelin has defined in 60 sazhens, that is 120 meters, therefore I have taken 100 meters for Miass fortress.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/9206_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/9206_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/9206/9206_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/9206/9206_original.jpg)

**Russian:**

Полученное изображение я для начала совместил с существующим положением моста через реку Миасс, поскольку он также задаёт направление центральных улиц села Миасское, а значит и всю структуру планировки посёлка. Я много раз бывал в селе Миасском и достаточно неплохо знаю эту территорию. Левый северный берег реки Миасс в этом месте достаточно крутой, центр посёлка фактически стоит на вершине холма. На снимке мы в этом месте не видим никаких построек как раз из-за крутого склона. Так что у меня очень большие сомнения, что там когда-то была Миасская крепость, да ещё и с глубоким рвом вокруг.

**English:**

I initially combined the image with the existing position of the bridge over the Miass River, as it also sets the direction of the central streets of Miasskoye village, and thus the entire structure of the layout of the village. I have visited Miasskoye many times and know this area quite well. The left north bank of the Miass River is quite steep in this place, the center of the village actually stands on the top of a hill. On the image we do not see any buildings in this place due to the steep slope. So I have very big doubts that there used to be a Miass fortress there, with a deep moat around it.

**Russian:**

Пока я проводил свои изыскания, то обратил внимание на остров справа от моста. Когда я развернул свою схему каналов по направлению с первого плана и совместил с этим островом, то получил новую схему, которая, возможно, как раз и указывает на то место, где когда-то находилась Миасская крепость.

**English:**

While I was doing my research, I noticed the island to the right of the bridge. When I turned my channel plan in the direction in the foreground and combined it with the island, I got a new plan which probably indicates where Miass Fortress used to be.

<!-- Local
[![](How Tartaria Was Lost Part 2_files/9388_900.jpg#clickable)](How Tartaria Was Lost Part 2_files/9388_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/9388/9388_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/9388/9388_original.jpg)

**Russian:**

Да, форма протоков не точно соответствует схеме каналов, но с учётом постоянных половодий реки и направления течения каналы и русло могли несколько изменить свою форму, поскольку вода ищет себе более удобный путь, чем течение с поворотами под прямым или острым углом. При этом направление верхней части протоки точно совпало с направлением канала, полученного с первого плана, которое на данной схеме я показал красной линией. Вряд ли это случайность.

**English:**

Yes, the shape of the ducts does not exactly correspond to the scheme of the canals, but taking into account the constant floods of the river and the direction of flow the canals and the channel could slightly change its shape, because the water is looking for a more convenient way than the flow with turns at right or sharp angles. Thus the direction of the top part of a duct has precisely coincided with the direction of the channel received from the foreground which on the given scheme I have shown a red line. It was hardly an accident.

**Russian:**

В целом же с селом Миасским мы имеем ровно ту же картину, что и в двух предыдущих случаях. Существующая планировка села полностью игнорирует тот факт, что на этом месте когда-то была крепость, которая должна была стать основой для всей структуры будущего поселения. Если пытаться совместить схему крепости с существующей структурой посёлка, то мы не наблюдаем каких-либо намёков на то, что там когда-то была крепость со рвом, окружавшим ещё и часть посёлка, поскольку попадаем на крутой склон холма. Если же рассматривать место, которое, возможно, является местом расположения крепости, то оно никак не согласуется с существующей структурой поселения.

**English:**

In general, with the village of Miassky, we have exactly the same picture as in the two previous cases. The existing layout of the village completely ignores the fact that this was once a fortress, which should have been the basis for the entire structure of the future settlement. If we try to combine the layout of the fortress with the existing structure of the village, we do not see any hints that there once was a fortress with a moat surrounding a part of the village, because we get to the steep hillside. If we consider the place, which is probably the location of the fortress, then it does not agree with the existing structure of the settlement.

**Russian:**

Либо крепости не было вообще, либо она была разрушена вместе со своим старым поселением, а позже на этом месте построили новое село с тем же названием, но уже «с чистого листа».

**English:**

Either there was no fortress at all, or it was destroyed along with its old settlement, and later on this place they built a new village with the same name, but already "from scratch".

**Russian:**

Исследуя эту проблему я неожиданно для себя обнаружил, что если речь идёт о населённых пунктах Челябинской области, то не существует никаких доказательств, что именно эти населённые пункты существуют тут больше 200 лет. Ни в одном городе Челябинской области нет ни одного здания или сооружения, которое можно было бы гарантированно датировать старше начала 19 века. Официальный миф на эту тему гласит, что то ли Пётр I, то ли его дочь Елизавета, то ли Екатерина II издали указ, запрещающий строить каменные здания где либо кроме столицы Санкт-Петербурга. Сделано это было для того, чтобы знать активнее застраивала Санкт-Петербург. От разных людей я слышал все три варианта неоднократно, в том числе во время экскурсий по Санкт-Петербургу. А раз был такой запрет, то в 18 веке все здания во вновь закладываемых городах строились только деревянные. Потому-то, дескать, здания 18 века в городах на Южном Урале и не сохранились.

**English:**

Studying this problem I suddenly found out for myself that if we are talking about settlements of Chelyabinsk region, there is no evidence that these settlements exist here more than 200 years. No city in the Chelyabinsk region has a single building or structure that can be guaranteed to date older than the early 19th century. The official myth on this topic states that either Peter I, his daughter Elizabeth, or Catherine II issued a decree prohibiting the construction of stone buildings anywhere except the capital city of St. Petersburg. This was done in order to know more actively built up St. Petersburg. From different people I have heard all three variants repeatedly, including during excursions in St. Petersburg. And since there was such a ban, in the 18th century all buildings in newly built cities were built only of wood. That is why, they say, the buildings of the 18th century in cities in the South Urals have not been preserved.

**Russian:**

На самом деле этот аргумент напрочь опровергается тем фактом, что в европейской части России полно и деревянных, и каменных зданий 18 века, не смотря ни на какие запреты.

**English:**

In fact, this argument is refuted by the fact that the European part of Russia is full of both wooden and stone buildings of the 18th century, in spite of any prohibitions.

**Russian:**

В тоже время, когда проводятся археологические раскопки, например при новом строительстве, то находятся следы существовавших когда-то зданий и сооружений, что пытаются выдать как доказательство того, что тот или иной город на этом месте существует уже давно. Но если произошла какая-то масштабная катастрофа, которая уничтожила поселение, а потом на этом месте оно было отстроено заново, ты мы будем видеть ровно ту же самую картину.

**English:**

At the same time, when archaeological excavations are carried out, for example, in new construction, there are traces of buildings and structures that once existed, which they try to issue as evidence that a particular city on this site has long existed. But if there is a major disaster that destroyed a settlement and then it was rebuilt on this place, you will see exactly the same picture.

**Russian:**

Я осмотрел несколько кладбищ, которые считаются очень старыми, но нигде мне не удалось найти захоронений ранее 1834 года. В тоже время при строительстве новых микрорайонов в том же Челябинске строители регулярно натыкаются на старые захоронения, которых вроде как в этом месте быть не должно. Одно время про это активно пытались писать местные газеты, но потом, когда счёт подобных случаев пошёл уже на десятки, перестали обращать на это внимание. В данном случае, как и с раскопками останков зданий, отсутствие старых точно датированных захоронений ещё не означает, что их там нет, но и не опровергает того факта, что катастрофа, уничтожившая поселения, могла произойти.

**English:**

I have visited several cemeteries, which are considered very old, but nowhere have I been able to find burials before 1834. At the same time, during the construction of new neighborhoods in the same Chelyabinsk builders regularly come across old burials, which seemingly should not be in this place. At one time local newspapers actively tried to write about it, but then, when the account of such cases has gone on dozens, they stopped paying attention to it. In this case, as with the excavation of the remains of buildings, the lack of old precisely dated burials does not mean that they are not there, but neither does it deny the fact that the disaster that destroyed the settlements could have occurred.

**Russian:**

То, что мы имеем документ, в котором сказано об основании поселения в 1736 году, а также тот факт, что сейчас на этом месте существует поселение или город с таким же или похожим названием ещё не означает, что в промежутке между этими событиями, например в 1812 или 1815 году не было катастрофы, уничтожившей существовавшее в то время поселение.

**English:**

The fact that we have a document that says that a settlement was founded in 1736 and that there is now a settlement or town with the same or similar name on the site does not mean that there was no disaster in between, for example in 1812 or 1815, that destroyed the settlement that existed at that time.

**Russian:**

Это только часть фактов, которые говорят о том, что масштабная катастрофа на территории Сибири могла произойти 200 лет назад. Про остальные в следующей части.

**English:**

This is only part of the facts that suggest that a massive catastrophe in Siberia could have occurred 200 years ago. The rest is in the next part.

**Russian:**
*Продолжение следует...*

**English:**
*To be continued...*

**Dmitry Mylnikov**

© All rights reserved. The original author retains ownership and rights.

