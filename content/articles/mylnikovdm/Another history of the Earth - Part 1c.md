title: Another history of the Earth - Part 1c
date: 2017-03-14 00:00
modified: 2022-12-14 16:04:08
category:
tags: catastrophe; Tartaria
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/220008.html
originally: Another history of the Earth. Part 1c. - Dmitry Mylnikov - LiveJournal.html
local: Another history of the Earth - Part 1c_files
summary: ...the Pacific Ocean's waters had to free themselves from a volume of space with a diameter of about 500 km. At the same time, as I wrote earlier, part of the water should have turned into steam almost instantly. As a result of which, the lateral pressure on the water at the point of passage of the object should have increased sharply and given an additional impulse to the movement of sea water in all directions from the epicenter of the impact site.
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/220008.html](https://mylnikovdm.livejournal.com/220008.html)

[First article in series (Russian)](http://mylnikovdm.livejournal.com/219459.html)

[First article in series (English)](./another-history-of-the-earth-part-1a.html)

Now let's return to the Pacific Ocean, where a giant tsunami formed at the moment the object entered. Moreover, in this case it will not be just a powerful wave, as would be the case with earthquakes, because in this case a several kilometres thick mass of water from the surface to the bottom of the sea will have to move. At the moment of the passage of the object the Pacific Ocean's waters had to free themselves from a volume of space with a diameter of about 500 km. At the same time, as I wrote earlier, part of the water should have turned into steam almost instantly. As a result of which, the lateral pressure on the water at the point of passage of the object should have increased sharply and given an additional impulse to the movement of sea water in all directions from the epicenter of the impact site.

I will illustrate the scenario with an example to make it clearer. If we fill a bowl to the brim with water and tap sharply on the side of the plate, it will be like  an ordinary earthquake-derived tsunami. In this case, a wave forms and part of the water will splash out of the plate on the opposite side from the impact - the tap. Now, to simulate our bolide case; if we abruptly put into the same bowl a round glass that has been filled to the brim, the wave will spread in all directions outward from the glass and will splash out of all sides of the bowl. In this case, the amount of water that will splash on the table will be much larger than in the first case.

Roughly the same thing happens after the fall of the object into the Pacific Ocean. After it, a giant wave several kilometers high begins to spread in all directions from the place of fall. At the same time, at each place the wave passes, the pressure on the earth's crust will increase in proportion to the ratio of the total height of the wave to the usual ocean depth at that place. That is, if your depth was 4 km and the wave height is 4 km above the previous sea level, then the pressure increase will be only twice normal pressure. But where the previous depth was insignificant, for example, 200 meters, the pressure will increase 20 times. Therefore, exactly where the ocean floor begins to rise and turn into the continental shelf, faults and downward pressure on the ocean floor should appear.

But in addition to the movement of water on the surface, after the passing of the object and the breakdown of the Earth's crust, there is also the movement of magma inside the Earth as it follows the passing object. And this movement of magma will create a suction effect, due to which the Pacific plate should move downward even more.

The pictures below show schematically what, in my opinion, should be happening.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/162449_900.png#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/162449/162449_900.png#resized)

*red text: внешний разлом = External fault;
green text: внутренние разломы = Internal fault;
black text: выдавленая поверхность в зоне точки выхода образовала Гималаи = At the exit point the extruded surface formed the Himalayas
brown text: разлом и глубоководный желоб = Fault and deep water trough
green text: водяной вал = Floodwater (tsunami)
brown text: течение магмы = Magma flow*

Due to the pressure on impact, and then the effect of "suction" from the movement of magma, the Pacific plate should move downward and partially lose its curvature. At the same time, external faults should have formed along the edges of the slab. These should be clearly visible from above. Internal faults should form in the middle of the slab closer to the center of the deflection. These will not be visible as faults from above but a little later I will show that they do exist.

In the next diagram, I tried to show the so-called "subduction", that is, the place where the oceanic plate "dives" under the continental plate, ostensibly due to plate movement caused by the flow of magma in the inner layers. In popular literature, this is usually portrayed as follows:

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/162628_600.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/162628/162628_600.jpg#resized)

*Text says: Subduction zone*

But these "subduction zones" have an interesting oddity. For some reason, they are observed in large quantities only along the perimeter of the Pacific Ocean. And for some reason we do not observe anything like this in the Indian Ocean or the Atlantic Ocean. The Atlantic Ocean has a mid-oceanic rift and ridge, similar to the Pacific mid-rift and ridge. However, the Atlantic Ocean does not have a coast lined with faults combined with deep-sea trenches, which are - allegedly - a consequence of "subduction". Why is subduction observed at the Pacific coast of both North and South America, but not at their Atlantic coasts?

Because these formations have a completely different origin. Upon impact and subsequent decrease in the curvature of the Pacific plate due to the effect of "suction" from the magma flow along the trajectory of the object, the edges of the plate begin to expand to the sides and tension arises. And when a giant wave several kilometers high reaches the shallow water near the coast, then due to a sharp increase in pressure from the huge mass of water, the edge of the plate is pressed down and slips under the continental plate, where it remains for some time. Over time, due to the fact that the internal flow of magma will gradually return to its original state, the matter inside the Earth will begin to redistribute back to the equilibrium state and the Pacific plate will gradually restore its curvature. At the same time, it will pull its edges back from under the mainland plates,

The lower diagram also shows how, due to the movement of magma inside the Earth along the trajectory of the object, a flow is formed, which, on the one hand, leads to a decrease in the level and a change in the curvature of the Pacific plate, and on the other hand, around the exit site, squeezes the earth's crust up, forming the Himalayan mountain system. By the way, if you examine how this mountain system looks, then its structure is also very interesting. This is how it looks on a satellite image. The point with an arrow is Everest, the highest peak on Earth.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/162846_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/162846/162846_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/162846/162846_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/162846/162846_original.jpg)

And this is how the same place looks on a topographical map.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/163075_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/163075/163075_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/163075/163075_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/163075/163075_original.jpg)

Note that the highest ridge of the Himalayas follows the southernmost edge of the mountain system. At the same time, further south, there is a very abrupt transition to a vast flat and low plain. It seems as if a stone tsunami rolled south onto the plain (from the north) and at some point suddenly froze. Further north from the highest ridge is a huge gray area, where there are vast plateaus as well as mountain ranges. The average height of this area is about 4000 meters above sea level.

Also, when I look at all these pictures and images, I personally can't get rid of the feeling that all this was flowing and moving quite recently.

Now let's see how the shores of the Pacific Ocean look. Upon closer inspection, it turns out that they do not look at all like the shores of all other oceans.

<!-- Local
[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/163363/163363_600.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/163363/163363_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/163363/163363_600.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/163363/163363_original.jpg)

*Deformation diagram*

We see a network of faults along almost all the Pacific Ocean's coastal edges. Along these edges there are zones of seismic activity, and many volcanoes that form the so-called "Pacific Volcanic Ring" or, as it is also called, the "Pacific Ring of Fire".

What's interesting about the Pacific Ring of Fire? First of all, the fact that of all earthquakes that have ever occurred in the world, over 90% happened here. Moreover, about 80% are among the strongest in history. This is exactly how it should be if the Pacific plate was deformed during the impact disaster, following which the edges of the Pacific Ocean are gradually regaining their original shape.

Also in this zone are 328 of the 540 active ground volcanoes known on Earth. In total, there are 452 volcanoes here, which is more than 75% of the active and extinct volcanoes in the world.

But in fact, this diagram is incomplete, because, firstly, only the currently active volcanoes are marked here, and secondly, only those faults that we can observe from the outside - from above - are shown. But if the proposed hypothesis of the processes taking place is correct, then we must also have internal crustal faults, which I also promised to show you. To find them, it is necessary to study the origin of the islands in the Pacific Ocean. It turns out that very many of the islands are of volcanic origin. However, many of the groups of islands in the Pacific Ocean also have an interesting feature. They are stretched out in lines which are sometimes near-perfect, sometimes with slight deviations.

For example, this is how the Hawaiian Islands look on a satellite image.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/163872_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/163872/163872_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/163872/163872_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/163872/163872_original.jpg)

Note that the line along which the Hawaiian Islands themselves run is very clearly visible. But so is another. From the left of the Hawaiian chain upwards, almost exactly to the North, there is an even chain of underground mountains of exactly the same volcanic origin as the Hawaiian Islands, but which couldn't quite grow to reach the surface of the ocean and become islands.

And there are a lot of such chains at the bottom of the Pacific Ocean. Here is another fragment to the south, where a linear chain of islands also sits above a fault.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/164210_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164210/164210_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/164210/164210_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164210/164210_original.jpg)

How did these islands, or rather these old inactive volcanoes, become lined up? The answer is: because they run along the faults that are not located on the outer face of the Earth's crust, but on the inner face of it. That is, along those internal crustal faults that were formed during the described catastrophe.

To make the overall picture clearer, I decided to make my own scheme, on which to plot the external faults which were shown on the diagram above as well as internal faults. The faults shown in orange on the map coincide with the deep sea trenches. The yellow ones are faults that do not coincide with deep-sea trenches. Separately, I highlighted the deepest tench - the Mariana Trench - where the red dot marks the location of the so-called "Challenger Abyss". It is the deepest place on Earth that we know of today, with a depth of about 10,994 meters.

With green lines, I have shown chains of islands. I assume their positions indicate internal faults in the Earth's crust. Green patches show where seamounts or volcanic islands are found in an area of seabed, rather than on a clear line.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/164445_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164445/164445_original.png)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/164445/164445_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164445/164445_original.png)

The resulting diagram clearly shows that most of these lines are not scattered randomly, but form a certain structure. The main lines run almost along parallel with the centre-line of the Pacific plate. They also have a general orientation toward the Tamu massif. On the right side of the diagram, the lines seem to form an arc, but these are distortions caused by the elliptical surface of the Earth being projected on to a plane. If you look at the image from the Google Earth program, you can clearly see that the islands run in almost straight lines.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201c.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/164847_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164847/164847_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/164847/164847_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/164847/164847_original.jpg)

[*Continuation (Russian)*](http://mylnikovdm.livejournal.com/220823.html)

[Next Part (English)](./another-history-of-the-earth-part-1d.html)


© All rights reserved. The original author retains ownership and rights.

