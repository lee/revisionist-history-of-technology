title: Another history of the Earth - Part 1b
date: 2017-03-12 00:01
modified: 2022-12-14 16:03:56
category:
tags: catastrophe; Tartaria
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/219687.html
originally: Another history of the Earth. Part 1b. - Dmitry Mylnikov - LiveJournal.html
local: Another history of the Earth_files - Part 1b_files
summary: I assume that all this sand was formed as a result of this catastrophe at the moment the object exploded, and that it is the same as the substance of this object. At the same time, small inclusions of other rocks, which we observe as drops of molten matter along the flight path, may well be of terrestrial origin.
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/219687.html](https://mylnikovdm.livejournal.com/219687.html)

[First part of series (Original Russian)](http://mylnikovdm.livejournal.com/219459.html)

[First part of series (English)](./another-history-of-the-earth-part-1a.html)

##### First phase

A huge object with a diameter of about 500 km first flies through the dense layers of the atmosphere. In this case, the surface of the object is heated to very high temperatures, forming a plasma cloud around the object. Due to the large size of the object and its movement at a very high speed, a very powerful shock wave must form in the atmosphere. At the same time, at the place of passage of the object, its speed will be commensurate with the speed of the object, but as it moves away, it will gradually fall to the speed of sound. But the object itself during the passage of the atmosphere will practically not lose speed, since due to the gigantic dimensions, the ratio of the object's cross-section to its mass will be very small, therefore, there will be virtually no aerodynamic braking. In addition, the height of the dense layers of the Earth's atmosphere is about 20 km, while the estimated size of the object is about 500 km.

##### Second phase

The object reaches the surface of the ocean and flies through the multi-kilometer water column. Now the depth of the ocean in the area of the Tamu massif is up to 6 km deep. But how deep it was at the time of the catastrophe is an open question, since after the catastrophe it should have become deeper. Even if we assume that it was 6 km back then, then it is slightly more than 1.2% of the diameter of an object of 500 km. That is, the object practically did not notice the presence of the ocean. Let us add to this that during its flight through the atmosphere the surface of the object is heated and the object is enveloped in a cloud of plasma. Accordingly, upon contact with the ocean, huge masses of water instantly turn into steam, as a result of which we get a powerful steam explosion and a hydro-steam shock. Where can the generated steam go? Above it, a falling object with a huge mass is pressing down and below is the seabed. Consequently, as the object hits the seabed, a huge amount of steam will rush in all directions from the center of the fall. And since the object eventually reaches the bottom, breaks through the earth's crust and continues inward, this means that a huge amount of water in a 500 km diameter area with a thickness of the depth of the ocean - several kilometers - will be pushed out in all directions from the impact area. Thus, we have a super tsunami with a wave height of several kilometers. But this tsunami will continue towards the coast for some time, and our object is moving at a cosmic speed. I can't estimate its speed at the entrance site. To do this, it is necessary to build complex mathematical models. But on its exit from the body of the Earth, the object's velocity had declined from its initial cosmic velocity, as I will show below.

##### Third phase

The object reaches the ocean floor and breaks through the Earth's crust. Due to the enormous mass and speed, the earth's crust cannot provide any serious resistance and in this case it simply breaks like an egg shell. The fragments of the Earth's crust are pressed by the moving object into the molten magma. Some melts or even evaporates, turned by the impact into into plasma. Further, the object maintains its trajectory while it passes through the layer of molten magma.

It goes without saying that serious changes are taking place with the object itself. It heats up; the minerals that make it up start to melt. But here it is necessary to take into account the fact that the thermal conductivity of a substance is finite and for most minerals is relatively low - noticeably lower than that of metals. Therefore, the surface of an object may already be heated to ultra-high temperatures at which its substance begins not only to melt but also to turn into plasma. At the same time, the substance inside the object will still be cold. In this case, the process of sublimation of minerals on the surface to the state of plasma itself will consume a huge amount of energy, acting as a kind of cooling system. Nevertheless, we proceed from the assumption that the size of the formations almost corresponds to the size of the object.

What else happens when an object moves through molten magma at high speed? In this case, the magma will behave exactly like any liquid. That is, a shock wave will arise in it, since it must somehow free up space for the object to move through it. Apparently, due to the tremendous speed and mass involved, some part of the magma should also turn into plasma. In fact, it should instantly transform into a gaseous state. In other words, a massive object that can pierce the body of the Earth at such a high speed, as it were, burns its way through, converting matter into plasma on its way and partially burning itself. Regardless of whether the object itself starts to melt, first a powerful shock wave should form in the magma, and then the magma should flow and will move along in the trajectory of the object.

Since the object moves at high speed, then, as I wrote above, it actually burns a channel through the inside of the Earth's body. It starts with a diameter of about 500 km and is diminishes to 350 km at the end while still inside the body of the Earth. Moreover, the whole process takes place very quickly. The length of the path through the body of the Earth from the point of entry to the point of exit is 6108 km. If we assume that the initial speed of the object was the same as that of the 2013 Chelyabinsk bolide, that is, about 20 km/s with a final speed in the region of 4-5 km/s, then the object will cover this distance in about 15-20 minutes. (It is difficult to calculate the numbers because we do not know how an object decelerates during such a process, I personally believe that it will hardly be a uniformly deccelerated motion). Thus, the formation of a giant channel inside the Earth will occur almost instantly. But this channel cannot just remain empty. It should start filling up with magma again, therefore a powerful magma flow should form in the inner layers and will begin to fill the formed cavity. All these processes inside magma will lead to serious changes on the surface, which we will talk about in more detail later. Moreover, this wave, like the two previous waves, which propagate in the atmosphere and in the ocean, will also move at a much lower speed than the object itself.

##### Fourth phase

The object, having passed through the molten layers of magma inside the Earth, reaches the surface at the point of exit. At this point, a fragment of the surface is simply burned, and a giant plasma ball about 350 km in diameter escapes from the bowels of the Earth. At the start, it flies back into space. But because of its low speed, it can no longer fly out of the Earth's gravitational field. Having flown on a ballistic trajectory of about 4,600 km, the object again returns to Earth and explodes in the Red Sea region.

The process at the exit point will also be similar to a giant explosion, at the moment of which a small "sun" flies out of the earth's body. The surface of the ejected object will be heated up to thousands of degrees and enveloped in a cloud of plasma consisting of part of the magma through which the object passed and part of the substance of the object itself.

We will have a hole for some time at the exit point of the object, from which plasma will escape at high speed. This plasma is the substance - in gaseous state - of which the magma and the body of the passing object consisted. At the initial moment immediately after the passage of the object, the entire channel pierced by the body will be filled with high-temperature plasma under strong pressure. For some time, this will keep the channel from filling with molten magma. This channel will begin to close from the point of entry of the object, so the plasma will exit from the channel at the point of exit of the object. This will also be facilitated by the momentum that the former contents of the channel received from being in contact with a rapidly moving object, regardless of the state of aggregation of this material (magmatic or plasma) after contact.

At the moment of initial breakthrough, the plasma's exit speed will be commensurate with the speed of the object itself, and it is possible that it will even exceed it, gradually decreasing as pressure is relieved in the channel from which it is escaping. As a result, a sufficiently large amount of matter in a plasma state will be ejected into near-earth space. This plasma will very quickly cool and crystallize, but due to the fact that after leaving the channel, its density should also significantly decrease, it will form not a monolithic stone, but small crystals - that is, sand or small particles of similar silicates. Over time, all these particles will have to return back to Earth.

Now let's examine the Earth's surface along the trajectory where the object flew during the process of exiting the Earth's body. It is logical to assume that there should be very noticeable traces in this area.

This is how the Earth's surface looks along the trajectory of the object's flight from space (the trajectory is indicated by a yellow line). At the beginning we see the scorched mountains of the Iranian Highlands, and then the scorched desert of the Arabian Peninsula. At the same time, if you take a closer look at the structure of the mountains of the Iranian Highlands, you get the impression that these rocks were melting and flowing down. Considering that the object passed over this place at a fairly low altitude, and its surface was heated to several thousand degrees, then most likely it was flowing down.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/161043_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161043/161043_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/161043/161043_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161043/161043_original.jpg)

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/161517_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161517/161517_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/161517/161517_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161517/161517_original.jpg)

The territory of the Arabian Peninsula and the eastern coast of the Red Sea in Egypt are also scorched. At the same time, from people who have been to these places, I have heard more than once that burnt and even melted rocks and stones are very common in these places.  There are no scorched areas further out because the object exploded and its remains mainly formed a huge cloud of sand. It is possible that during the explosion some of the fragments may have received an impulse of sufficient strength to overcome Earth's gravity and go into some kind of low-Earth orbit or even leaving the Earth's gravitational field altogether and go into orbit around the Sun. A diagram of the process should look something like this, if you look at it from the side.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/161645_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161645/161645_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/161645/161645_600.jpg)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161645/161645_original.jpg)


<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/161913_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161913/161913_original.png)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/161913/161913_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/161913/161913_original.png)

*Text says (left to right): Estimated point of explosion of the object; Exit Point; Entry Point*

Now let's look at another image and correct the mistakes that I had in the article ["How the Sahara Desert came to be. Working hypothesis"](http://mylnikovdm.livejournal.com/2051.html).

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/162250_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/162250/162250_original.png)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/162250/162250_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/162250/162250_original.png)

*Text says (left to right): Yellow text: (Greenwich) Meridian; Tropic of Cancer; Equator; Red text: Estimated point of explosion of the object; Blue text: Earth's direction of rotation*

I created this diagram in the Google Earth program specially to avoid accusations that I did not take into account distortions due to the spherical surface of the Earth. The red arrow shows the flight path of an object after it leaves the body of the Earth. At the same time, I deliberately turned on the coordinate grid so that it was clear that the strip of sand in North Africa runs exactly along the line of the Tropic of Cancer. That is, its origin is unambiguously associated with the rotation of the Earth around its axis.

By the way, pay attention to the fact that the width of the sand-covered strip is almost the same along its entire length, while the distance from the territory occupied by the desert to the ocean coast varies greatly. This means that the formation of a giant desert in this area is in no way simply connected with the climate and lack of moisture. It has some other cause.

I assume that all this sand was formed as a result of this catastrophe at the moment the object exploded, and that it is the same as the substance of this object. At the same time, small inclusions of other rocks, which we observe as drops of molten matter along the flight path, may well be of terrestrial origin. That is, these are fragments of the earth's crust that were punched out and picked up by the object as left the body of the Earth. Similar traces are found in abundance on the Arabian Peninsula and in Africa. I have already provided some images of such objects in the above-mentioned [article about the origin of the Sahara](http://mylnikovdm.livejournal.com/2051.html).

The numbers used in the below explanation refer to the diagram below and the same diagram used [in the first part of this series](http://mylnikovdm.livejournal.com/219459.html), and in [in the original "Origin of the Sahara article](http://mylnikovdm.livejournal.com/2051.html).

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/26098_1000.jpg#clickable)](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/ 26098_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/26098/26098_1000.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/26098/26098_original.jpg)

Formation at point 1. The impression is that someone threw lumps of mud on top although in reality these are rocks which official science calls ancient volcanoes.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/28763_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/28763/28763_900.jpg#resized)

Another formation at point 2. The picture is similar. Drops of the molten mass fell from above and solidified.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/29051_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/29051/29051_900.jpg#resized)

A whole group of formations at point 4.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/29801_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/29801/29801_900.jpg#resized)


And this is how the central spot at point 4 looks in enlarged form. The diameters of the "circles" are from 200 meters to 1 km.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/30421_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/30421/30421_900.jpg#resized)


Another similar spot with many "drops" at point 5.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201b.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/31134_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/31134/31134_900.jpg#resized)


Interestingly, points 4 and 5 are located almost exactly along the trajectory along which the object flew, and points 1 to 3 to the left of it. This is explained quite simply. The fragments that formed points 4 and 5 at the time of the explosion were thrown along the trajectory of the object, but the fragments we observe at points 1 to 3 were thrown up and to the sides. That is, they flew along a ballistic trajectory and fell on the surface of the Earth later, after the Earth rotated somewhat due to its normal diurnal rotation.

The daily rotation also explains the strip of sand that runs exactly along the line of the Tropic of Cancer. Sand particles fall to the Earth's surface both immediately and gradually because the atmosphere exerts a strong inhibitory effect on them. Therefore, sand falls out of the sand cloud somewhat slowly - somewhere within 6-7 hours. During this time the Earth has time to turn and the falling sand forms a strip along the line of latitude under the point of the explosion. At the same time, the movement of the atmosphere has an insignificant effect on this process, since the explosion took place outside the dense layers of the Earth's atmosphere.

[*Continuation*](http://mylnikovdm.livejournal.com/220008.html)

[Next Part (English)](./another-history-of-the-earth-part-1c.html)


© All rights reserved. The original author retains ownership and rights.

