title: Occasionally Asked Questions
date: 2020-12-23 15:25
modified: Wed 13 Jan 2021 07:28:08 GMT
category:
tags: faq
slug: 
authors: RoHT
summary: Occasionally Asked Questions
status: draft

0. I don't quite get whether you are a history site or a technology site.

A. Don't beat yourself up about it. We're interested in both. We grew out of a need for history and technology researchers to find and share foreign-language material.

1. Are you specialists in history or technology?

A. We have a lot of expertise in technology and technical editing.

1. Why are the translations rough?

A. Because they are machine translations reviewed by people with varying knowledge of the source language.

2. Do you update the translations?

A. Yes. As and when we identify faults and fix/improve them.

3. Can I archive my material with you?

Q. Yes.

4. Do you want new material in a specific format?

A. We prefer material in markdown format with all content justified to the left margin. We prefer image links (clickable) on separate lines, also justified to the left margin. This helps our editing and translation code separate out text from links.

5. Do you accept other formats?

A. Yes. We can process all known formats. The advantage of markdown is that it takes much less time for humans to review and edit and is very predictable for machine processing translations.

6. Do you charge?

A. No

7. Do you want sponsorship?

A. If you mean 'money', yes. If you mean in 'money' in exchange for editorial influence, brand promotion, adverts, etc: no.

8. What would you do with donations or financial sponsorship?

A. Pay for assistants to review and edit articles. That would speed up processing of our archive of unpublished material.

9. How can I donate?

A. You can't. We think [Ko-fi](https://ko-fi.com/) or something like that fits best with our values. We have some half developed code to handle Ko-fi payments but we tend to focus on processing articles.

10. How can I contact you?

A. We check [this link](https://pad.publiclab.org/p/yH8k20sfGhf18ZIYjX0J) for comments most days. Note, all comments are publicly visible to, and deletable by, all users.

11. Do you take on specific editing work?

A. Yes. If our skill-set, code and processes can help move it along.

12. Do you take on specific translating work?

A. Our machine translation code does. We run it and will 

13. Why no proper site name?

A. Resilience against Internet technical problems. Demonstration sometimes being more effective than explanation, compare:

    - StolenHistory.org: [https://172.81.117.190/](https://172.81.117.190/)

    - StolenHistory.net: [https://136.144.41.142](https://136.144.41.142)


