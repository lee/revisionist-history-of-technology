title: Why do archaeologists only excavate temples and altars?
date: 2020-11-16
modified: Mon 05 Jul 2021 18:58:35 BST
category: 
tags: quarried Earth
slug: 
authors: sibved
summary: Has anyone checked these sites for mines beneath them? Could they be ore dumps? At one time at the opening of the Olympics in London in 2012 there   was a historical hint about this (the moment when the workers come out of the hill and start industrialization).
status: published 
local: Why do archaeologists only find altars and temples_files/
from: https://zen.yandex.ru/media/sibved24/pochemu-arheologi-raskapyvaiut-tolko-hramy-i-altari-5fb28d2870f5da1bda8e84ba
originally: Why do archaeologists only find altars and temples.md

#### Translated from: 

[https://zen.yandex.ru/media/sibved24/pochemu-arheologi-raskapyvaiut-tolko-hramy-i-altari-5fb28d2870f5da1bda8e84ba](https://zen.yandex.ru/media/sibved24/pochemu-arheologi-raskapyvaiut-tolko-hramy-i-altari-5fb28d2870f5da1bda8e84ba)

**Russian:**

Почему археологи раскапывают только храмы и алтари?

**English:**

Why do archaeologists only excavate temples and altars?

**Russian:**

Свежий пример раскопок китайских археологов. Предлагаю взглянуть на него так, как-будто мы не читаем новостных заметок, не знакомы с заключениями археологов. Перед нами только фотографии:

**English:**

A fresh example of excavations by Chinese archaeologists. I suggest we look at it as if we don't read the news reports and are not familiar with the conclusions of archaeologists. We have only pictures in front of us:

![](./images/Why do archaeologists only find altars and temples_files/1.jpeg#resized)

**Russian:**

На севере Китая, в районе Внутренняя Монголия китайские археологи с 2019 г. проводят раскопки. Объект по форме – срезанный конус с насыпями по периметру в виде колец. Внутренний диаметр 13,5 м, внешний – 32,5 м.

**English:**

In northern China's Inner Mongolia region, Chinese archaeologists have been excavating since 2019. The object is shaped like a cut cone with ring-shaped embankments around the perimeter. The inner diameter is 13.5 m and the outer diameter is 32.5 m.

![](./images/Why do archaeologists only find altars and temples_files/2.jpeg#resized)

**Russian:**

Что видим? В центре холма углубились внутрь, а вокруг раскопали сегменты якобы рвов. Оставили прямоугольные столбы из глины для подсчета грунтовых работ. Из-за формы объекта и находок археологи сделали вывод, что это алтарь для молитв императора возрастом 1500 лет. А в рвах приносили в жертву животных. Вроде даже нашли исторические упоминания про это место. Ссылка: https://news.myseldon.com/ru/news/index/239907043

**English:**

What do we see? In the center of the hill they dug deeper, and around it they excavated segments of supposedly ditches. They left rectangular clay posts to count the soil work. Because of the shape of the site and the findings, archaeologists concluded that it was a 1500-year-old altar for the emperor's prayers. And animals were sacrificed in the moats. I think they even found historical references to this place. Link: https://news.myseldon.com/ru/news/index/239907043

**Russian:**

Оценить размер объекта можно в коротком ролике:

**English:**

You can estimate the size of the site in the short video:

![]](https://vk.com/video_ext.php?oid=-98443963&id=456245272&hash=f3e2efba771b9ce3)
video

**Russian:**

Внутри много разбитых сосудов, черепков и керамики. Это больше напоминает остатки от склада продуктов (кувшины с семенами или маслом). Либо вообще холодильник, ледник.

**English:**

Inside there are many broken vessels, shards, and pottery. It looks more like leftovers from a food warehouse (jars of seeds or oil). Or in general a refrigerator, an icehouse.

![](./images/Why do archaeologists only find altars and temples_files/3.jpeg#resized)

**Russian:**

Вообще, версий можно выдвинуть множество. Это и курган и городище и загон для скота с шатровой крышей и хранилище продуктов. Хотя, по находкам зернохранилищ есть и исключения и в Китае:

**English:**

In general, many versions can be put forward. This is a barrow, and settlement, and corral for cattle with a hipped roof and storehouse products. Though, there are some exceptions in China as well according to the finds of granaries:

![](./images/Why do archaeologists only find altars and temples_files/4.jpeg#resized)
Source: https://www.vsyako.net/archives/1489030

**Russian:**

Тоже район Внутренней Монголии в Китае. Возраст объекта, который присвоили археологи – 2000 лет. Но это уже зернохранилище. Видимо, не нашли ничего того, что могло указать на ритуальность этого объекта.

**English:**

Also an area of Inner Mongolia in China. The object is archaeologically dated at 2000 years old. But it is already a granary. Apparently nothing was found that could indicate that this object was ritual.

**Russian:**


> Про Древнюю Грецию и Древний Рим вообще сказка – там что ни раскопки, то храм или захоронения.

**English:**

> It is a fairy tale about Ancient Greece and Ancient Rome - every excavation there is a temple or a burial place.

**Russian:**

Подобной формы объект в Китае с круговыми рвами – не единственный и не уникальный. Вот примеры из Англии:

**English:**

The similarly shaped object in China with circular ditches is not the only and not unique. Here are examples from England:

![](./images/Why do archaeologists only find altars and temples_files/5.jpeg#resized)

![](./images/Why do archaeologists only find altars and temples_files/6.jpeg#resized)

![](./images/Why do archaeologists only find altars and temples_files/7.jpeg#resized)

**Russian:**

Кто-нибудь проверял эти объекты на наличие шахт под ними? Может это отвалы от добычи руды? В свое время на открытии Олимпиады в Лондоне в 2012 г. об этом был исторический намек (момент, когда из холма выходят работники и начинают индустриализацию).

**English:**

Has anyone checked these sites for mines beneath them? Could they be ore dumps? At one time at the opening of the Olympics in London in 2012 there was a historical hint about this (the moment when the workers come out of the hill and start industrialization).

**Russian:**

На территории современной России подобные объекты со рвами называют городища. А на территории современной Украины – майданы:

**English:**

On the territory of modern Russia such sites with ditches are called townships. And on the territory of modern Ukraine - maidans:

![](./images/Why do archaeologists only find altars and temples_files/8.jpeg#resized)

**Russian:**

Для чего нужно было копать, перемещать и сооружать подобное даже для целей алтарей – трудно понять. Мы не знаем о жизни строителей тех эпох практически ничего. Но если это возводилось вручную, то мотивация в любом случае была. Современная история эту мотивацию сдвигают в религиозный и мистический смысл на уровне страха у работников. А если это делали техникой? Пусть примитивной, работающей на паровой тяге, но техникой? Тогда история меняется. Стоит лишь пересмотреть время появление паровых машин или сдвинуть возраст этих археологических объектов по-ближе к нам.

**English:**

Why it was necessary to dig, move and build such even for the purpose of altars is difficult to understand. We know almost nothing about the lives of the builders of those eras. But if it was erected by hand, there was a motivation in any case. Modern history shifts this motivation to a religious and mystical meaning at the level of fear in the workers. And if it was done by machine? Even a primitive, steam-powered machine, but a machine? Then the story changes. It is only necessary to revise the time of steam engines appearance or to shift the age of these archaeological sites closer to us.

**Russian:**

Но о назначении подобных мест можно говорить много. Хотя, в объяснения про алтари и храмы верит все меньше интересующихся темой археологических раскопок и находок. И как всегда одна важная деталь – все под толстыми слоями глины с тонким слоем перегноя сверху. И внутренний объект, холм и рвы снаружи. Хотя за 1500 лет этот слой должен быть как минимум метровым.

**English:**

But we can talk a lot about the purpose of such places. Although, fewer and fewer people interested in the topic of archaeological excavations and finds believe the explanations about altars and temples. And as always one important detail - everything is under thick layers of clay with a thin layer of humus on top. And the inner object, the hill and the ditches on the outside. Although for 1500 years this layer should be at least a meter.

**Russian:**

Картинки и фотографии взяты из открытых источников: сервиса Яндекс.Картинки

**English:**

Pictures and photos taken from public sources: service Yandex.kartinki

**Russian:**

Кому интересны мои публикации - > Подписывайтесь на канал и добавляйте его в закладки браузера (Ctrl+D).

**English:**

If you're interested in my publications - > subscribe to the channel and add it to your browser bookmarks (Ctrl+D).

**Russian:**

Использование материалов журнала для youtube-каналов – только с разрешения и согласования с автором (с).

**English:**

Using materials from the magazine for youtube-channels - only with permission and coordination with the author (c).

© All rights reserved. The original author retains ownership and rights.
