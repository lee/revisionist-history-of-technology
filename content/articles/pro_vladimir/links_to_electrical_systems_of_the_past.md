title: Links to pro-vladimir's Electrical Equipment of the Past articles
date: 2014-06-30 00:00:02
modified: 2021-10-21 20:43:22
category: 
tags: technology; links
slug:
authors: Vladimir Mamzerev
summary: Links to pro-Vladimir's Google-translated 'Electrical Equipment of the Past' articles
status: published
local: 
originally: links_to_electrical_systems_of_the_past


### Translated from:

[https://pro-vladimir.livejournal.com/106270.html](https://pro-vladimir.livejournal.com/106270.html) Index page in Russian

Links to articles, Google-translated into English:

[http://pro-vladimir.livejournal.com/2998.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/2998.html) Fabrications about the gaze.

[http://pro-vladimir.livejournal.com/3405.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/3405.html) Electrical equipment of the past

[http://pro-vladimir.livejournal.com/4416.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/4416.html) Who is the Volt? or Electrical equipment of the past. Part 2

[http://pro-vladimir.livejournal.com/5472.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/5472.html) Jewelry or spare parts?

[http://pro-vladimir.livejournal.com/5781.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/5781.html) ARCHI-VOLT

[http://pro-vladimir.livejournal.com/40121.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/40121.html) Electrical equipment of the past. Part 3.

[http://pro-vladimir.livejournal.com/51165.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/51165.html) What a microwave oven is capable of! High voltage

[http://pro-vladimir.livejournal.com/54023.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/54023.html) Electrical equipment of the past. Part 4.

<!--
[http://pro-vladimir.livejournal.com/53690.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/53690.html) Papal Cross? Or is it not a cross at all?
-->

[http://pro-vladimir.livejournal.com/54721.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/54721.html) Electrical equipment of the past. Part 5.

[http://pro-vladimir.livejournal.com/56021.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/56021.html) Electrical equipment of the past. Part 6.

[http://pro-vladimir.livejournal.com/58553.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/58553.html) Points.

[http://pro-vladimir.livejournal.com/97018.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/97018.html) Electric equipment of the past. Part 7.

[http://pro-vladimir.livejournal.com/99272.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/99272.html) Electrical equipment of the past. Part 8.

[http://pro-vladimir.livejournal.com/105902.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://pro-vladimir.livejournal.com/105902.html) Electrical equipment of the past. Part 9


© All rights reserved. The original author retains ownership and rights.

