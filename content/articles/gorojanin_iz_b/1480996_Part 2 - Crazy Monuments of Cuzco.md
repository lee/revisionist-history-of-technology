title: Mysterious Mesoamerica - Part 2 Crazy Monuments of Cuzco - Is this real or fake?
date: 2018-10-28 04:51
modified: 2022-09-17 12:28:34
category:
tags: forged history; Cuzco
slug:
authors: Anton Sizykh
from: https://eto-fake.livejournal.com/1480996.html
originally: 1480996.html
local: 1480996_files/
summary: The Peruvian-Bolivian antiquities are not exactly Mesoamerica, and historians have scattered them to different cultures and times, but in fact they  were made at approximately the same time as the Central American antiquities. They were made using the same technology, by the same performer - a workshop of history makers.
status: published

#### Translated from:

[https://eto-fake.livejournal.com/1480996.html](https://eto-fake.livejournal.com/1480996.html)

and from original at:

[Mysterious Mesoamerica. Mysteries and clues. Part 2. Crazy monuments of Cuzco](https://911tm.livejournal.com/27392.html)

**Russian:**

В продолжение первой части Таинственная Мезоамерика. Загадки и разгадки. Часть 1. Непостижимый Саксайуаман Отсюда: 1 , 2 , 3 Э тот памятник Виракоче, создателю сущего, построенный на..."

**English:**

In continuation of the [first part of the Mysterious Mesoamerica. Mysteries and riddles. Part 1. The inscrutable Sacsayhuaman From here: 1](https://gorojanin-iz-b.livejournal.com/81937.html) and [2](https://sibved.livejournal.com/280920.html).

Отсюда: [1](https://gorojanin-iz-b.livejournal.com/81937.html?thread=17602065#t17602065), [2](https://sibved.livejournal.com/280920.html?thread=21554264#t21554264), [3](https://sibved.livejournal.com/280920.html?thread=21553752#t21553752)

**English:**

From here: [1](https://gorojanin-iz-b.livejournal.com/81937.html?thread=17602065#t17602065), [2](https://sibved.livejournal.com/280920.html?thread=21554264#t21554264), [3](https://sibved.livejournal.com/280920.html?thread=21553752#t21553752)

<!-- Local
![](1480996_files/2vJSVL6.jpg#resized)
-->

![](https://i.imgur.com/2vJSVL6.jpg#resized)

**Russian:**

> Э тот памятник Виракоче, создателю сущего, построенный на частные средства возле Исторического музея Куско, [Министерство культуры Перу недавно распорядилось убрать](https://codigooculto.com/2018/03/ministerio-de-cultura-de-peru-exige-retiro-de-monumento-a-viracocha-en-cusco/), назвав "галюциногенным", "шокирующим" и другими нелестными словами. Нарушает традиции. Жаль. Если вдуматься и всмотреться в архитектурные памятники, составляющие упомянутые традиции, то вполне соответствует, просто построен на век-полтора позже - вся и вина этого колоритного Виракочи.

**English:**

> ...and that monument to Viracocha, creator of things, built with private funds near the Historical Museum of Cuzco, [the Peruvian Ministry of Culture recently ordered its removal](https://codigooculto.com/2018/03/ministerio-de-cultura-de-peru-exige-retiro-de-monumento-a-viracocha-en-cusco/), calling it "hallucinogenic", "shocking" and other unflattering words. Breaks with tradition. Pity. If you think about it and take a closer look at the architectural monuments that make up these traditions, it's quite consistent... It's just built a century and a half later. It's all the fault of that colorful Viracocha.

**Russian:**

Хотя перуанско-боливийские древности - не совсем Мезоамерика, и историки растащили их по разным культурам и временам, но фактически они делались  примерно в одно время с центрально-американскими, по одним технологиям, одним исполнителем - цехом создателей истории. Поэтому и их ставлю сюда же. Продолжаем цикл.

**English:**

The Peruvian-Bolivian antiquities are not exactly Mesoamerica, and historians have scattered them to different cultures and times, but in fact they were made at approximately the same time as the Central American antiquities. They were made using the same technology, by the same performer - a workshop of history makers. So I put them here as well. We continue the cycle.

**Russian:**

Храм Кориканчо в Куско, он же музей, якобы главное здание Империи Инков, 1438 года постройки. Бут и армирование строители истории используют везде, естественно, и в Кориканчо. Технически невозможно отлить такие дуры, не делая мусорную основу для залития.

**English:**

The temple of Coricancho in Cusco, aka the museum, supposedly the main building of the Inca Empire, built in 1438. Butte and reinforcement history builders use everywhere, naturally, and in Coricancho as well. It is technically impossible to cast such fools without making a rubbish base for pouring.

**Russian:**

За Храмом Кориканчо следят, подмазывают, вылизывают для туристов, но за таким огромным объектом априори нельзя уследить, где-нибудь все равно проявляется... И неизбежно попадает на съемки. Железяки и посторонние камни из основы и бута вполне видны на хорошей съемке.

**English:**

The Temple of Coricancho is monitored, oiled, licked for tourists, but such a huge object is a priori impossible to keep track of, it still shows up somewhere... And inevitably it gets caught on film. Tin-plates and foreign stones from the base and the butt are quite visible on a good photograph.

<!-- Local
![](1480996_files/sQvewvC.jpg#resized)
-->

![](https://i.imgur.com/sQvewvC.jpg#resized)

<!-- Local
![](1480996_files/zTZhf8M.jpg#resized)
-->

![](https://i.imgur.com/zTZhf8M.jpg#resized)

<!-- Local
[![](1480996_files/HdTZ6T1.jpg#resized)](1480996_files/HdTZ6T1.jpg)
-->

[![](https://i.imgur.com/HdTZ6T1.jpg#clickable)](https://i.imgur.com/HdTZ6T1.jpg)

**Russian:**

Совсем откровенная железяка с куском тряпки, который пропитался-произвестковался насквозь при стекании воды из мегалита еще в опалубке.

**English:**

Quite frankly a piece of iron with a piece of rag, which was soaked-sanded through when the water flowed out of the megalith still in the formwork.

**Russian:**

С такими тряпочками и сеточками, якобы из мрамора, кстати, носятся зомбоящеры дурнета, постящие для домохозяек великие працивилизации и загадочные технологии непостижимых скульптур забытых цивилизаций средневековья.

**English:**

With such rags and grids, ostensibly from marble, by the way, zombie-people of the durnet carry, posting for housewives great civilisations and mysterious technologies of incomprehensible sculptures of the forgotten civilisations of the Middle Ages.

<!-- Local
![](1480996_files/K1wxuyT.jpg#resized)
-->

![](https://i.imgur.com/K1wxuyT.jpg#resized)

<!-- Local
[![](1480996_files/D6a2P4u.jpg#resized)](1480996_files/D6a2P4u.jpg)
-->

[![](https://i.imgur.com/D6a2P4u.jpg#clickable)](https://i.imgur.com/D6a2P4u.jpg)

**Russian:**

Хороший объектив - хорошее дело. Вряд ли строители памятника рассчитывали на такую технику у туристов.

**English:**

A good lens is a good thing. It is unlikely that the builders of the monument counted on such a tool being available to tourists.

<!-- Local
![](1480996_files/kr9BQW3.jpg#resized)
-->

![](https://i.imgur.com/kr9BQW3.jpg#resized)

<!-- Local
[![](1480996_files/yRyUGCb.jpg#resized)](1480996_files/yRyUGCb.jpg)
-->

[![](https://i.imgur.com/yRyUGCb.jpg#clickable)](https://i.imgur.com/yRyUGCb.jpg)

**Russian:**

Глядя на эти пазы и дырки от труб, вынутые по застыванию, кто-то возьмется утверждать, что это вытачивалось в граните? Среди разумных зрителей - вряд ли. очень не любят показывать эту часть Храма Кориканчо в туристских проспектах и конспирологических трудах.

**English:**

Looking at these grooves and holes from pipes, taken out by solidification, will someone assert that it was milled from granite? Among reasonable spectators, that is unlikely. They are very loath to show this part of the Temple of Coricancho in tourist brochures and (ancient architecture) conspiracy writings.

**Russian:**

Это, разумеется, не дверь и не ниша. В пристенке планировалась гранитная конструкция, назначние которой мы не узнаем - цех не публикует проектов и планов. Безобразие должно было спрятаться этой конструкцией. Но вышло как вышло, не успели, сделали, но плохо... оставили как есть.

**English:**

It is not a door or a niche, of course. There was a granite construction planned in the wall, the purpose of which we do not know - the workshop does not publish projects or plans. The mess was supposed to be hidden by this construction. But it turned out as it turned out, there was no time, it was done, but badly... and left as it is.

<!-- Local
[![](1480996_files/H37QUFf.jpg#resized)](1480996_files/H37QUFf.jpg)
-->

[![](https://i.imgur.com/H37QUFf.jpg#clickable)](https://i.imgur.com/H37QUFf.jpg)

<!-- Local
[![](1480996_files/gWCaWlm.jpg#resized)](1480996_files/gWCaWlm.jpg)
-->

[![](https://i.imgur.com/gWCaWlm.jpg#clickable)](https://i.imgur.com/gWCaWlm.jpg)

**Russian:**

И чтобы не говорили про реставраторов - есть и фото до реставраторов. Неважнецкие, но уж какие есть. Главное: пазы и дырки от труб видны.

**English:**

And whatever they say about the restorers, there are pictures before the restorers. They are not very good, but they are what they are. The main thing: the grooves and holes from the pipes are visible.

<!-- Local
[![](1480996_files/cozHPEw.jpg#resized)](1480996_files/cozHPEw.jpg)
-->

[![](https://i.imgur.com/cozHPEw.jpg#clickable)](https://i.imgur.com/cozHPEw.jpg)

<!-- Local
![](1480996_files/BEhn1zW.jpg#resized)
-->

![](https://i.imgur.com/BEhn1zW.jpg#resized)

**Russian:**

До ученых часть Храма с этим нише-проемом по обычаю была завалена. Наверняка и части неудачной конструкции за строителями убирали, и трубы выдергивали археологи, которые этот Храм откапывали.

**English:**

Before scientists a part of the Temple with this niche-open was traditionally rubble. Probably the parts of unsuccessful construction were removed after the builders, and the pipes were pulled out by archaeologists, who dug out this Temple.

<!-- Local
![](1480996_files/wd4RO9Q.jpg#resized)
-->

![](https://i.imgur.com/wd4RO9Q.jpg#resized)

**Russian:**

Туристам демонстрируют убранные части на стеллажиках, просто так.

**English:**

Tourists are shown the stowed parts on racks, just for fun.

<!-- Local
[![](1480996_files/QkEHVKf.jpg#resized)](1480996_files/QkEHVKf.jpg)
-->

[![](https://i.imgur.com/QkEHVKf.jpg#clickable)](https://i.imgur.com/QkEHVKf.jpg)

<!-- Local
[![](1480996_files/GOFlxtr.jpg#resized)](1480996_files/GOFlxtr.jpg)
-->

[![](https://i.imgur.com/GOFlxtr.jpg#clickable)](https://i.imgur.com/GOFlxtr.jpg)

<!-- Local
[![](1480996_files/EFpy20t.jpg#resized)](1480996_files/EFpy20t.jpg)
-->

[![](https://i.imgur.com/EFpy20t.jpg#clickable)](https://i.imgur.com/EFpy20t.jpg)

**Russian:**

Наверняка труба из того же комплекта от того же поставщика видна в гранитной толще в дырке, при нужном ракурсе. В средней.

**English:**

Surely a pipe from the same kit from the same supplier can be seen in the granite thickness in the hole, at the right angle. In the middle one.

<!-- Local
![](1480996_files/NcEeYnG.jpg#resized)
-->

![](https://i.imgur.com/NcEeYnG.jpg#resized)

<!-- Local
[![](1480996_files/TaIp3kK.jpg#resized)](1480996_files/TaIp3kK.jpg)
-->

[![](https://i.imgur.com/TaIp3kK.jpg#clickable)](https://i.imgur.com/TaIp3kK.jpg)

**Russian:**

Топ-памятники окрестностей Куско, творчество начала 20 века, не поддерживаются так тщательно, как дворец. Поэтому лезет гораздо больше. ЛАИсты молодцы - снимали эти ракурсы, рискуя свернуть башку. Это виды не для туристов. Еще бы не писали всякую дурь и свои собственные съемки внимательней смотрели.

**English:**

The top monuments around Cusco, an early 20th century creation, are not maintained as carefully as the palace. That's why there's so much more climbing. The LAists did well - shooting these angles at the risk of twisting their heads. These views are not for tourists. I wish they would not write such rubbish and watch their own photos more attentively.

<!-- Local
[![](1480996_files/jXOZNE4.jpg#resized)](1480996_files/jXOZNE4.jpg)
-->

[![](https://i.imgur.com/jXOZNE4.jpg#clickable)](https://i.imgur.com/jXOZNE4.jpg)

**Russian:**

Посторонние предметы, бревно и труба, видны только с такого ракурса на этих знаменитых окошках.

**English:**

Foreign objects, a log and a pipe, are only visible from this angle on these famous windows.

<!-- Local
![](1480996_files/FxwXaXA.jpg#resized)
-->

![](https://i.imgur.com/FxwXaXA.jpg#resized)

**Russian:**

Часть опалубной формы, тонколистый пластичный металл - осталась в объекте.

**English:**

Part of the formwork - thin sheet plastic metal - was left in the facility.

<!-- Local
![](1480996_files/biNkQc7.jpg#resized)
-->

![](https://i.imgur.com/biNkQc7.jpg#resized)

**Russian:**

Здесь товарищ конкретно рисковал башкой. Воспользовался "инкскими лестницами". Нельзя этого делать! Они не для лазанья, а только для зрителя.

**English:**

Here the comrade risked his head. He took advantage of the ladders of the Incas. You can't do that! They are not for climbing, but only for the spectator.

<!-- Local
[![](1480996_files/vKjdbPy.jpg#resized)](1480996_files/vKjdbPy.jpg)
-->

[![](https://i.imgur.com/vKjdbPy.jpg#clickable)](https://i.imgur.com/vKjdbPy.jpg)

**Russian:**

Но рисковал не зря. Увидел постороннюю трубу в основании этого дуро-памятника.

**English:**

But I didn't risk it for nothing. I saw an extraneous pipe at the base of this fool's monument.

<!-- Local
![](1480996_files/ybWSwJ9.jpg#resized)
-->

![](https://i.imgur.com/ybWSwJ9.jpg#resized)

**Russian:**

И более внятно снял составную основу из мусорных блоков-кирпичей, недозалитую гранитом.

**English:**

And more distinctly removed the composite base of junk block bricks underfilled with granite.

<!-- Local
![](1480996_files/JH6RYdG.jpg#resized)
-->

![](https://i.imgur.com/JH6RYdG.jpg#resized)

**Russian:**

Вот фото выше всех похвал. Не для проспектов! Мусорная основа, залитая оболочкой.

**English:**

Here's a picture above all praise. Not for the avenues! Garbage base, filled with shell.

<!-- Local
[![](1480996_files/pLWGDW2.jpg#resized)](1480996_files/pLWGDW2.jpg)
-->

[![](https://i.imgur.com/pLWGDW2.jpg#clickable)](https://i.imgur.com/pLWGDW2.jpg)

<!-- Local
![](1480996_files/hvykcmT.jpg#resized)
-->

![](https://i.imgur.com/hvykcmT.jpg#resized)

**Russian:**

В великих памятниках вроде Ольянтайтамбо всегда интересна выбраковка строителей истории. Тем, что ее не реставрируют. Но держат на периферии туристских троп.

**English:**

In great monuments like Ollantaytambo it is always interesting to cull the builders of history. In that it is not restored. But kept on the periphery of hiking trails.

**Russian:**

Проволок, ясен пень, в тесаниях инков и анунахов быть не должно. Только в творчестве цеха создателей истории, вариаций для официозников и альтернатистов. У них один автор - повторю в десятый раз.

**English:**

The wires, it is clear, in theses Incas and anunnahs should not be. Only in the works of the history makers, variations for the officious and alternativeists. They have one author - I will repeat for the tenth time.

<!-- Local
![](1480996_files/WmME6Xb.jpg#resized)
-->

![](https://i.imgur.com/WmME6Xb.jpg#resized)

<!-- Local
[![](1480996_files/XmR1z4c.jpg#resized)](1480996_files/XmR1z4c.jpg)
-->

[![](https://i.imgur.com/XmR1z4c.jpg#clickable)](https://i.imgur.com/XmR1z4c.jpg)

**Russian:**

К этой стене туристам вплотную не подойти, любуются издали. Потекла стена от бурного дождя, как и в Саксайуамане. Как и в любом ландшафтном литье, пристроенном к природным поверхностям, полно посторонностей.

**English:**

Tourists cannot come close to this wall, they admire it from afar. The wall is leaking from the stormy rain, as well as in Saksayhuaman. As in any landscape casting, attached to natural surfaces, is full of extraneous things.

<!-- Local
![](1480996_files/xkj1B5v.jpg#resized)
-->

![](https://i.imgur.com/xkj1B5v.jpg#resized)

<!-- Local
![](1480996_files/JIkOv8O.jpg#resized)
-->

![](https://i.imgur.com/JIkOv8O.jpg#resized)

<!-- Local
![](1480996_files/ARDYBJB.jpg#resized)
-->

![](https://i.imgur.com/ARDYBJB.jpg#resized)

<!-- Local
![](1480996_files/bWMqJOS.jpg#resized)
-->

![](https://i.imgur.com/bWMqJOS.jpg#resized)

<!-- Local
[![](1480996_files/qvYpYAG.jpg#resized)](1480996_files/qvYpYAG.jpg)
-->

[![](https://i.imgur.com/qvYpYAG.jpg#clickable)](https://i.imgur.com/qvYpYAG.jpg)

**Russian:**

Посторонние палки, провода, пластины - всё как всегда.

**English:**

Extraneous sticks, wires, plates, the usual.

<!-- Local
![](1480996_files/oh8zg8S.jpg#resized)
-->

![](https://i.imgur.com/oh8zg8S.jpg#resized)

<!-- Local
[![](1480996_files/O0P3L4w.jpg#resized)](1480996_files/O0P3L4w.jpg)
-->

[![](https://i.imgur.com/O0P3L4w.jpg#clickable)](https://i.imgur.com/O0P3L4w.jpg)

**Russian:**

Это же место с другого ракурса.

**English:**

It's the same place from a different angle.

<!-- Local
![](1480996_files/4AsgKvf.jpg#resized)
-->

![](https://i.imgur.com/4AsgKvf.jpg#resized)

<!-- Local
[![](1480996_files/7IfX9i3.jpg#resized)](1480996_files/7IfX9i3.jpg)
-->

[![](https://i.imgur.com/7IfX9i3.jpg#clickable)](https://i.imgur.com/7IfX9i3.jpg)

**Russian:**

Еще съемка Ольянтайтамбо.

**English:**

More shooting of Ollantaytambo.

**Russian:**

Больше всего это похоже на пуговицу. Обычно сметают и топят в бут любой мусор, но прежде всего тот, который с очевидностью оставлен строителями.

**English:**

Most of all it looks like a button. Usually any rubbish is swept away and drowned in the boot, but above all that which is obviously left behind by the builders.

<!-- Local
![](1480996_files/un2zdLa.jpg#resized)
-->

![](https://i.imgur.com/un2zdLa.jpg#resized)

**Russian:**

Посторонняя пластина. Те же опалубные листы из тонкого пластичного металла, что использовались во всех [**египтах**](https://gorojanin-iz-b.livejournal.com/80593.html) и питерах.

**English:**

Foreign plate. The same thin ductile metal shuttering plates used in all [**Egypt**](https://gorojanin-iz-b.livejournal.com/80593.html) and peters.

<!-- Local
![](1480996_files/NSyFVrk.jpg#resized)
-->

![](https://i.imgur.com/NSyFVrk.jpg#resized)

<!-- Local
[![](1480996_files/PvX1exJ.jpg#resized)](1480996_files/PvX1exJ.jpg)
-->

[![](https://i.imgur.com/PvX1exJ.jpg#clickable)](https://i.imgur.com/PvX1exJ.jpg)

**Russian:**

Здесь два посторонних предмета. Один похож на подшипник или другую втулкообразную деталюшку, второй, по самому распространенному обычаю всех древнегреков, древнепальмирцев и прочих древнеегиптян - проволока.

**English:**

There are two foreign objects here. One looks like a bearing or some other bushing-like detail, the second, according to the most common custom of all the Ancient Greeks, Ancient Palmirians and other Ancient Egyptians, is a wire.

<!-- Local
![](1480996_files/gP8p1pX.jpg#resized)
-->

![](https://i.imgur.com/gP8p1pX.jpg#resized)

<!-- Local
[![](1480996_files/V9rWk1b.jpg#resized)](1480996_files/V9rWk1b.jpg)
-->

[![](https://i.imgur.com/V9rWk1b.jpg#clickable)](https://i.imgur.com/V9rWk1b.jpg)

**Russian:**

Посторонний чер- и цветмет.

**English:**

Foreign ferrous and non-ferrous metal.

<!-- Local
![](1480996_files/ySLSJkb.jpg#resized)
-->

![](https://i.imgur.com/ySLSJkb.jpg#resized)

<!-- Local
[![](1480996_files/yvDhRrr.jpg#resized)](1480996_files/yvDhRrr.jpg)
-->

[![](https://i.imgur.com/yvDhRrr.jpg#clickable)](https://i.imgur.com/yvDhRrr.jpg)

**Russian:**

В этой тесанине тоже полно постороннего чермета.

**English:**

There's a lot of extraneous scrap metal in this tezanina, too.

<!-- Local
[![](1480996_files/abZGtHP.jpg#resized)](1480996_files/abZGtHP.jpg)
-->

[![](https://i.imgur.com/abZGtHP.jpg#clickable)](https://i.imgur.com/abZGtHP.jpg)

<!-- Local
![](1480996_files/0AQUPg9.jpg#resized)
-->

![](https://i.imgur.com/0AQUPg9.jpg#resized)

<!-- Local
![](1480996_files/vAoJBy8.jpg#resized)
-->

![](https://i.imgur.com/vAoJBy8.jpg#resized)

<!-- Local
![](1480996_files/zmlG5iO.jpg#resized)
-->

![](https://i.imgur.com/zmlG5iO.jpg#resized)

**Russian:**

О толщине слоя скажет вот это живучее деревце суровых андских гор.

**English:**

This hardy tree of the Andes Mountains will tell you how thick the layer is.

**Russian:**

Не надо отливать горы. В ландшафтном литье самое большее - метры.

**English:**

You don't have to cast mountains. In landscape casting, the most you can do is meters.

<!-- Local
![](1480996_files/KflTYBy.jpg#resized)
-->

![](https://i.imgur.com/KflTYBy.jpg#resized)

<!-- Local
[![](1480996_files/IhQwsHX.jpg#resized)](1480996_files/IhQwsHX.jpg)
-->

[![](https://i.imgur.com/IhQwsHX.jpg#clickable)](https://i.imgur.com/IhQwsHX.jpg)

**Russian:**

Здесь альтернатисты любуются таинственными насечками.

**English:**

This is where alternativeists admire the mysterious notches.

**Russian:**

Тайна сия зело велика - палки положили строители, пока сырое, да прошлись.

**English:**

This mystery is great - the builders put sticks while it was wet, but they went over.

<!-- Local
[![](1480996_files/wQ00HLt.jpg#resized)](1480996_files/wQ00HLt.jpg)
-->

[![](https://i.imgur.com/wQ00HLt.jpg#clickable)](https://i.imgur.com/wQ00HLt.jpg)

**Russian:**

За насечками не видят пластину, открывшуюся там, где доходился строитель, отломил край.

**English:**

Behind the notches you can't see the plate, opened up where the builder reached, broke off the edge.

<!-- Local
![](1480996_files/TmGh1yT.jpg#resized)
-->

![](https://i.imgur.com/TmGh1yT.jpg#resized)

**Russian:**

И опалубный лист, торчащий в стене. Ближе его снять невозможно, к сожалению. Делалась эта опалубная форма с помощью лесов, разобранных перед сдачей.

**English:**

And the formwork sheet sticking out in the wall. Unfortunately, it is impossible to take a closer picture. This formwork was made with the help of scaffolding dismantled before handing it over.

<!-- Local
![](1480996_files/hsZYeZz.jpg#resized)
-->

![](https://i.imgur.com/hsZYeZz.jpg#resized)

**Russian:**

Любуемся дальше. Палка, часть заливавшегося каркасного элемента.

**English:**

We're admiring the rest of it. The stick, part of the cast frame element.

<!-- Local
![](1480996_files/PoDi2bP.jpg#resized)
-->

![](https://i.imgur.com/PoDi2bP.jpg#resized)

<!-- Local
[![](1480996_files/Dnp17nE.jpg#resized)](1480996_files/Dnp17nE.jpg)
-->

[![](https://i.imgur.com/Dnp17nE.jpg#clickable)](https://i.imgur.com/Dnp17nE.jpg)

**Russian:**

Здесь интересен дальний план. Забросили краном подальше от глаз многочисленный брак недозалития блоков. С дорожек для туристов его не видать. А на дальнем плане - вполне. Присмотритесь, там его полно.

**English:**

Here the distant plan is interesting. Thrown by the crane out of sight of the numerous marriage of underfilling blocks. You can't see it from the paths for tourists. It's quite visible in the background. Take a closer look, there's plenty of it.

<!-- Local
![](1480996_files/IGV0uzV.jpg#resized)
-->

![](https://i.imgur.com/IGV0uzV.jpg#resized)

<!-- Local
[![](1480996_files/HoJf4c5.jpg#resized)](1480996_files/HoJf4c5.jpg)
-->

[![](https://i.imgur.com/HoJf4c5.jpg#clickable)](https://i.imgur.com/HoJf4c5.jpg)

**Russian:**

Вот, наконец - слава ЛАИ и Кецалькоатлю! - нормальная съемка мегалитических сосков невероятных памятников Куско.

**English:**

Here at last - glory to LAI and Quetzalcoatl! - normal shooting of megalithic nipples of incredible monuments of Cuzco.

**Russian:**

Соски, как не раз говорил, это распорки формы при залитии. Бракованные продолговатые балки-блоки из того же состава, что и основное тело мегалитов. Крупные мегалиты подсыхали и ужимались, распорки выперли над гранями, поскольку материал их просох и ужался раньше.

**English:**

The nipples, as has been said more than once, are the spacers of the mold during pouring. The spacers are oblong beams-blocks of the same composition as the main body of the megaliths. The large megaliths were drying and shrinking, the spacers were sticking out over the edges, because their material dried out and shrunk earlier.

<!-- Local
[![](1480996_files/VV87puj.jpg#resized)](1480996_files/VV87puj.jpg)
-->

[![](https://i.imgur.com/VV87puj.jpg#clickable)](https://i.imgur.com/VV87puj.jpg)

**Russian:**

Расщелился древний мегалит в месте неравномерого процесса усыхания-ужимания, посторонние предметы открылись.

**English:**

The ancient megalith has split in the place of unequal shrinkage-shrinkage process, foreign objects have opened.

<!-- Local
![](1480996_files/ItdHDN9.jpg#resized)
-->

![](https://i.imgur.com/ItdHDN9.jpg#resized)

**Russian:**

Правый предмет более всего похож на гайку. Но даже если и не гайка, то в любом случае не гранит.

**English:**

The right object looks most like a nut. But even if it is not a nut, it is not granite in any case.

<!-- Local
![](1480996_files/UBIrXjz.jpg#resized)
-->

![](https://i.imgur.com/UBIrXjz.jpg#resized)

**Russian:**

Тут вообще какая-то часть электроприбора.

**English:**

There's some kind of electrical appliance in here.

<!-- Local
![](1480996_files/O0xH6w6.jpg#resized)
-->

![](https://i.imgur.com/O0xH6w6.jpg#resized)

**Russian:**

Вот роскошная загадка инкской/анунахской цивилизации! Больше всего это похоже на ведро. Но

**English:**

Here's a gorgeous riddle of the Inca/Anunah civilization! It looks more like a bucket than anything else. But

**Russian:**

ученые ничего не скажут про это, ученым, академистам и альтернатистам, про эти вещи в великих памятниках инков/анунахов говорить нельзя. Мы про них сами должны думать.

**English:**

scientists will not say anything about it, scientists, academicians and alternativeists, about these things in the great monuments of Inca/Anuns cannot say. We have to think about them ourselves.

<!-- Local
![](1480996_files/JYkjyrB.jpg#resized)
-->

![](https://i.imgur.com/JYkjyrB.jpg#resized)

**Russian:**

Ну и классика мегалитов там же - провода-палки обыкновенные, цивилизации земной.

**English:**

Well and the classic megaliths are there as well - wires and sticks of the common, civilization of the earth.

<!-- Local
![](1480996_files/u5k60ML.jpg#resized)
-->

![](https://i.imgur.com/u5k60ML.jpg#resized)

<!-- Local
[![](1480996_files/7BWdX9y.jpg#resized)](1480996_files/7BWdX9y.jpg)
-->

[![](https://i.imgur.com/7BWdX9y.jpg#clickable)](https://i.imgur.com/7BWdX9y.jpg)

**Russian:**

И напоследок самый выдающийся объект инкского творчества на всем континенте - батарея "мегалитов из риолита".

**English:**

And lastly, the most prominent Inca site on the entire continent is a battery of "rhyolite megaliths".

**Russian:**

Край как не люблю эти памятники инков. От них шиза так и прет. Полное ощущение, что курировал все стройки великих инков в начале 20 века в Боливиии и Перу один цеховик, не без художественных талантов, но больной на всю голову, а "риолиты" - самое шизовое произведение из всех.

**English:**

I hate these Incan monuments. They're so crazy. There is a feeling that all the great Inca construction sites in Bolivia and Peru in the early 20th century were overseen by one shopkeeper, not without artistic talents, but sick in the head, and "rhyolites" is the most schizophrenic work of all.

**Russian:**

Это такой же "риолит", как и тамошний "гранит", хотя с фасада для толпы зрителей, с проспектов для туристов и со съемок для ученых смотрится внушительнее всех других форм Куско и окрестностей, за счет своей величины. Но это такое же дуро-литье на мусорном основании, под штукатурку, которая облазит, типичное оболочное, под опалубными щитами. В состав для литья оболочки включен розовый порошок, вполне может быть и риолитом. Здесь видно, как пропечатались отдельные каменюки основания в "мегалите". Вся и тайна "самых великих мегалитов" Куско.

**English:**

It is the same "rhyolite" as the "granite" from there, although from the façade for the crowds of spectators, from the avenues for tourists and from the filming for scientists it looks more imposing than all other forms of Cusco and its surroundings, due to its size. But it's the same duro-casting on a rubbish base, under plaster that's peeling, typical shell casting, under formwork shields. The shell casting compound includes pink powder, could well be rhyolite. Here you can see how the individual base stones are imprinted in the "megalite". All and mystery of the "greatest megaliths" of Cusco.

<!-- Local
![](1480996_files/K0w3FQi.jpg#resized)
-->

![](https://i.imgur.com/K0w3FQi.jpg#resized)

<!-- Local
[![](1480996_files/W39og4l.jpg#resized)](1480996_files/W39og4l.jpg)
-->

[![](https://i.imgur.com/W39og4l.jpg#clickable)](https://i.imgur.com/W39og4l.jpg)

**Russian:**

[Часть 3 здесь](https://eto-fake.livejournal.com/1481274.html)

**English:**

[Part 3 here](https://eto-fake.livejournal.com/1481274.html)

**Russian:**

[Другие материалы по теме строительства истории

**English:**

Other materials on the topic of building history

**Russian:**

[Таинственная Мезоамерика. Загадки и разгадки. Часть 1. Непостижимый Саксайуаман](https://gorojanin-iz-b.livejournal.com/83925.html)

**English:**

[The mysterious Mesoamerica. Riddles and clues. Part 1. The inscrutable Sacsayhuaman](https://gorojanin-iz-b.livejournal.com/83925.html)

**Russian:**

[Таинственная Мезоамерика. Загадки и разгадки. Часть 2. Безумные памятники Куско](https://gorojanin-iz-b.livejournal.com/84097.html)

**English:**

[The mysterious Mesoamerica. Riddles and clues. Part 2. Crazy Monuments of Cuzco](https://gorojanin-iz-b.livejournal.com/84097.html)

**Russian:**

[Таинственная Мезоамерика. Загадки и разгадки. Часть 3. Делирий архитектора Тиуанако](https://gorojanin-iz-b.livejournal.com/84385.html)

**English:**

[The mysterious Mesoamerica. Riddles and clues. Part 3. Delirium of the Architect of Tiwanaku](https://gorojanin-iz-b.livejournal.com/84385.html)

**Russian:**

[Таинственная Мезоамерика. Загадки и разгадки. Часть 4. Древние города майя, говорите...](https://gorojanin-iz-b.livejournal.com/84666.html)

**English:**

[The mysterious Mesoamerica. Riddles and clues. Part 4. Ancient cities of Maya, say...](https://gorojanin-iz-b.livejournal.com/84666.html)

**Russian:**

[Документ о доисторической Москве, уцелевший по недосмотру](https://911tm.livejournal.com/26786.html)

**English:**

[Document about prehistoric Moscow, survived by oversight](https://911tm.livejournal.com/26786.html)

**Russian:**

[Наделение историей. Сказки Александрийского столпа](http://gorojanin-iz-b.livejournal.com/80878.html)

**English:**

[Empowering History. Tales of the Pillar of Alexandria](http://gorojanin-iz-b.livejournal.com/80878.html)

**Russian:**

[Египетский морок. Часть 1. Строительство колоссов, Сфинкса, пирамид на заре эры фотографий в 19 веке](http://gorojanin-iz-b.livejournal.com/80010.html)

**English:**

[The Egyptian morass. Part 1. Construction of the Colossi, Sphinx, Pyramids at the dawn of the photo era in the 19th century](http://gorojanin-iz-b.livejournal.com/80010.html)

**Russian:**

[Египетский морок. Часть 2. Строительство пирамид на заре эры фотографий в 19 веке. ПОЛНАЯ ВЕРСИЯ.](https://alterhistory.livejournal.com/201086.html)

**English:**

[The Egyptian morass. Part 2. Construction of the pyramids at the dawn of the photo era in the 19th century. FULL VERSION.](https://alterhistory.livejournal.com/201086.html)

**Russian:**

[Египетский морок. Часть 3. Египтологи на защите древнегипетских реликвий. Асуанский обелиск](http://gorojanin-iz-b.livejournal.com/80593.html)

**English:**

[The Egyptian morass. Part 3. Egyptologists on the protection of ancient Egyptian relics. Aswan obelisk](http://gorojanin-iz-b.livejournal.com/80593.html)

**Russian:**

[Египетский морок. Часть 4. В египетскую кампанию Наполеона Великие пирамиды еще строились](https://gorojanin-iz-b.livejournal.com/83036.html)

**English:**

[The Egyptian morass. Part 4. In Napoleon's Egyptian campaign the Great Pyramids were still under construction](https://gorojanin-iz-b.livejournal.com/83036.html)

**Russian:**

[Эллинистический Гиппос, строительство Пальмиры, немного «древнего» Египта](https://gorojanin-iz-b.livejournal.com/81937.html)

**English:**

[Hellenistic Hippos, construction of Palmyra, a bit of "ancient" Egypt](https://gorojanin-iz-b.livejournal.com/81937.html)

**Russian:**

[Липовые лики богов на нью-йоркском ломе](https://gorojanin-iz-b.livejournal.com/83490.html)

**English:**

[Fake Faces of the Gods on New York Scrap](https://gorojanin-iz-b.livejournal.com/83490.html)

**Russian:**

[Идиоты из труппы ИГИЛ опять рушат «официальную» историю](https://gorojanin-iz-b.livejournal.com/75511.html)

**English:**

[ISIS troupe idiots ruin the "official" story again](https://gorojanin-iz-b.livejournal.com/75511.html)

**Russian:**

[Клинические идиоты из труппы ИГИЛ продолжают рушить официальную историю](https://gorojanin-iz-b.livejournal.com/75580.html)

**English:**

[The clinical idiots in the ISIS troupe continue to ruin the official story](https://gorojanin-iz-b.livejournal.com/75580.html)

**Russian:**

[Японский средневековый замок оказался пустой «древностью»](https://elemental1111.livejournal.com/44911.html)

**English:**

[Japanese medieval castle turned out to be an empty "antiquity"](https://elemental1111.livejournal.com/44911.html)

**Russian:**

[Античный Линдос, затопленный Долихисте и другие постройки создателей истории](https://gorojanin-iz-b.livejournal.com/82785.html)

**English:**

[Antique Lindos, the flooded Dolihiste and other constructions of history makers](https://gorojanin-iz-b.livejournal.com/82785.html)

**Russian:**

[Строительство Стоунхенджа с нулевого цикла. Кинo- и фотодокументы 1949-58 годов](http://gorojanin-iz-b.livejournal.com/64046.html)]]

**English:**

[Construction of Stonehenge from ground zero. Film and photographic documents 1949-58](http://gorojanin-iz-b.livejournal.com/64046.html)]

**Russian:**

Вынужден поставить и это.

**English:**

Forced to put that in as well.

**Russian:**

Сейчас многие блогеры, стримеры собирают донаты, теперь это в порядке вещей. До сих пор воздерживался. Мешало советское прошлое)) Но очень сильно подрубило возможности выживать и что-то делать в сети решение об отъеме пенсий у населения 1959 и последующих годов рождения. Не знаю, как доскребаться до нового срока. Нынче у нас и в 50 люди нигде не нужны, а под 60 - тем более. Поэтому кто считает, что эта работа заслуживает какой-то оплаты - буду признателен.

**English:**

A lot of bloggers, streamers are collecting donations now; it's the order of the day. Until now, I refrained. The Soviet past got in the way :). But the decision to take pensions away from people born in 1959 and later has really screwed up my ability to survive and do something online. I do not know how to scrape by until the new term. Nowadays we do not need people who are even 50 years old -  and under 60, even less so. So whoever thinks that this work deserves some kind of payment - I would be grateful.

**Russian:**

Номер карты Сбербанка 4276020014219544. Яндекс-кошелек 410013946572302.]]

**English:**

Sberbank card number 4276020014219544. Яндекс-кошелек 410013946572302.]]

© All rights reserved. The original author retains ownership and rights.

