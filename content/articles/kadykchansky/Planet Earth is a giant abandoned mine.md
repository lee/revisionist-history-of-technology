title: Planet Earth is a giant abandoned mine
date: 2014-07-25
modified: Sat 01 May 2021 17:17:48 BST
category:
tags: quarried Earth
slug:
authors: Andrey Viktorovich Kadykchansky
from: https://kadykchanskiy.livejournal.com/264787.html
originally: Planet Earth is a giant abandoned mine. - Zapiski kolymchanin - LiveJournal.html
local: Planet Earth is a giant abandoned mine_files/
summary: Tens of millions more tons of precious metals were mined from the Dead Sea. Several tons of precious metals per day were mined in Baalbek, evaporating the largest river in ancient Lebanon, the Leonte. But they could have extracted the most from the Marianas Trench.
status: published

#### Translated from:

[https://kadykchanskiy.livejournal.com/264787.html](https://kadykchanskiy.livejournal.com/264787.html)

For  А.Ю. Кушелева, check:
[http://prometheus.al.ru/phisik/kushelev.htm](http://prometheus.al.ru/phisik/kushelev.htm)
[http://sandywest.narod.ru/yastreb.html](http://sandywest.narod.ru/yastreb.html)
[https://www.youtube.com/channel/UCsx422KjIzwTWTBPPpwBlXQ](https://www.youtube.com/channel/UCsx422KjIzwTWTBPPpwBlXQ)

Article by A.Yu. Kusheleva is another confirmation that most of the objects of disputable (natural or man-made) origin are a by-product of the mining industry, a civilization unknown to us. The nature of their origin is akin to waste heaps near mines and dumps in gold mining locations. It is here that the answers to the questions of skeptics who study, for example, the Kolyma megaliths, or Gornaya Shoria, are hiding:

> "And where are the traces of economic activity, tools, dishes, other household items, necessarily accompanying places of long-term residence of a person?" "Where are the necessary door and window openings"? "Where are the stairs?" etc.

The answer is simple as always.

<!-- Local
![](Planet Earth is a giant abandoned mine_files/1276884507_vertolet-samyjj-vysokijj-terrikon.jpg)	
-->

[![](https://kartavtseff.files.wordpress.com/2016/03/1276884507_vertolet-samyjj-vysokijj-terrikon.jpg#clickable)](https://kartavtseff.files.wordpress.com/2016/03/1276884507_vertolet-samyjj-vysokijj-terrikon.jpg)

These are waste heaps in the Donbass. There are no windows or doors. There is nothing familiar, characteristic of traces of human activity, but no one claims that these are "freaks of nature"?

**Nanomir Laboratory**

*When Reality reveals secrets, miracles fade into the shadows...*

Dmitrovsky Val through the eyes of aliens from Easter Island (material for the newspaper *Dmitrovskie Izvestia*)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/001.jpg#clickable)](http://img-fotki.yandex.ru/get/17/nanoworld.79/0_c86a_9ef2c34d_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/17/nanoworld.79/0_c86a_9ef2c34d_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/17/nanoworld.79/0_c86a_9ef2c34d_orig.jpg)

*[Photo by Nikolay Podshibyakin](http://fotki.yandex.ru/users/nanoworld/album/34833/)*

*Top center: Trees in the spring area. Center: Ledge/shaft. Trees in the spring area. Bottom center: Run-off?*

What might link Dmitrovsky Val with Easter Island?

<!-- Local
![](Planet Earth is a giant abandoned mine_files/99.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/3006/nanoworld.bd/0_1fef5_428159e8_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/3006/nanoworld.bd/0_1fef5_428159e8_orig.jpg)

At first glance, nothing. There is a defensive wall in Dmitrov, and a volcano on Easter Island. The Dmitrov rampart was created with shovels, the volcano is a natural formation. But imagine for a moment that you are Sherlock Holmes, who is tasked with investigating an unprecedented crime by human standards. You have been approached by a citizen who claims that millions of tons of gold have been stolen here and there! If he went to the police, he would be sent to a psychiatric hospital, but it is not the first time Sherlock Holmes has solved incredible riddles, so he is in no hurry to call the psychiatric hospital. Instead, he listens to an incredible story...

An eyewitness account. I walked along Dmitrovsky Val and admired the surroundings.

<!-- Local
![](Planet Earth is a giant abandoned mine_files/071.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/23/nanoworld.f/0_a05f_15be4ca_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/23/nanoworld.f/0_a05f_15be4ca_orig.jpg)

Suddenly I noticed that the rim was not round, but elongated along the course of the Yakhroma River. It turned out that this shape is characteristic of these rims...

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/065.jpg#clickable)](http://img-fotki.yandex.ru/get/16/nanoworld.f/0_a05a_d86dc5bc_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/16/nanoworld.f/0_a05a_d86dc5bc_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/16/nanoworld.f/0_a05a_d86dc5bc_orig.jpg)

And when I reached the pointed end, i.e. downstream of the river, I found that the rampart had been washed out by overflowing water.

<!-- Local
![](Planet Earth is a giant abandoned mine_files/011.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/3209/nanoworld.ed/0_25de1_ae65b033_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/3209/nanoworld.ed/0_25de1_ae65b033_orig.jpg)

Dmitrovsky rim. Reconstruction by A.M. Vasnetsov

How so? If there was water inside, then could our ancestors build houses under water? It turns out that the rampart existed even before the foundation of the city and by the beginning of the development it was already washed out by the Yakhroma River, and the water had already receded, i.e. it became dry inside the rampart and it became possible to build houses... Then where did the Dmitrovsky rampart come from?

**In this place, the author posted a photo, but at the time of publication it was not available for viewing.**

I decided to measure its height and found that in different parts it differs more than twice! This means that the rampart could not be poured as a defensive structure. After all, pouring extra 7-8 meters means doing 70-80% of the extra work. People wouldn't do 5 times more than they need to. By the way, the mass of the Dmitrovsky rim turned out to be almost a million tons! It's easy to calculate. The rim is 960 meters long, the maximum height is 14 meters, and the width reaches 60 meters. The density of the soil is about 2 tons per cubic meter. Problem for middle school students. Archaeologists and historians understand that it was impossible to fill up the rim quickly, because there were few people and the shovels were made of wood. Therefore, they decided that the rim was poured gradually, which is reflected in the reconstruction, where its height is commensurate with the growth of a person. But we already know that the rim was fully formed and even washed out even before the start of construction, i.e. it could not be poured after the foundation of Dmitrov.

**In this place, the author posted a photo, but at the time of publication it was not available for viewing.**

It is curious that other (Zmiev's) rims have the same parameters (pay attention to the size and shape of the gaps), i.e. up to 15 meters in height, up to 60 meters in width, and their total length exceeds... 2000 km! This means that their mass is over a billion tons! Even the entire population of the Globe 2000 years ago could not have built the Zmievy Shafts, to which the Dmitrovsky Shaft belongs in all respects.

**In this place, the author posted a photo, but at the time of publication it was not available for viewing.**

Excavations of the Serpent Shafts have shown that they contain nothing but waste rock. Wooden fortifications are extremely rare. One gets the impression that the already finished rampart in these places was adapted to new (defensive) tasks. But the steep bank of the river was also adapted to defensive tasks. This does not mean that the river was built by people for defensive purposes? :) By the way, many rims stretch along river banks.

**In this place, the author posted a photo, but at the time of publication it was not available for viewing.**

Dmitrovsky hill. Photo by Nikolay Podshibyakin. Reconstruction by Alexander Kushelev

There is a reservoir inside the Dmitrovsky rim. I decided to find out the composition of this water and passed a sample for mass spectral analysis.

> "In your water, the concentration of a rare and scattered precious metal (gallium) is 1000 times higher than the concentration in seawater," the specialist who carried out the analysis told me.

What is it... So in this place a precious metal comes out from the bowels of the Earth, which cannot evaporate with water and gradually accumulates. How much of it could have accumulated in the Dmitrovsky rim zone? If its content was the same as the platinum content in the Nizhne-Tagil deposit, i.e. 800 grams per ton of rock, in the rock forming the Dmitrovsky rim there could be 10 thousand tons of gallium.

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/006.jpg#clickable)](http://img-fotki.yandex.ru/get/17/nanoworld.7e/0_cc90_4fc11f0e_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/17/nanoworld.7e/0_cc90_4fc11f0e_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/17/nanoworld.7e/0_cc90_4fc11f0e_orig.jpg)

Exploring the zones designated by numbers and letters, I came to the conclusion that the Dmitrovsky rim was filled with the help of a giant mechanism.

I noticed that the height difference from the inside of the rim is several meters less than from the outside, i.e. it seems that the rampart is surrounded by a hill washed out by the river. Then its elongated shape along the river becomes clear, resembling a cut chicken egg in projection. According to my estimate, the mass of the hill reached 4 million tons. This means that the first time someone could have mined 40,000 tons of gallium here, and the second time, i.e. when the rampart was poured around the eroded hill, another 10,000 tons of gallium could be mined. 50,000 tons of gallium at the prices I found on the internet costs the same as half a million tons of gold! Someone took away from here a thousand (or several thousand) times more of Russia's strategic gold reserve!

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/001(1).jpg#clickable)](http://img-fotki.yandex.ru/get/3212/nanoworld.ec/0_25c7f_5d1cb5ae_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/3212/nanoworld.ec/0_25c7f_5d1cb5ae_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/3212/nanoworld.ec/0_25c7f_5d1cb5ae_orig.jpg)

Further investigations revealed a connection between the embankment of the swell with a small spring, where, according to my estimate, an additional 10 tons of gallium was mined.

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/004.jpg#clickable)](http://img-fotki.yandex.ru/get/3211/nanoworld.ec/0_25c90_79786e46_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/3211/nanoworld.ec/0_25c90_79786e46_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/3211/nanoworld.ec/0_25c90_79786e46_orig.jpg)

*Center-right: A final chokepoint? Bottom-center: Finish line*

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/008.jpg#clickable)](http://img-fotki.yandex.ru/get/3112/nanoworld.ec/0_25c9c_3dc64ccb_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/3112/nanoworld.ec/0_25c9c_3dc64ccb_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/3112/nanoworld.ec/0_25c9c_3dc64ccb_orig.jpg)

Montage by Alexander Kushelev. The image of the "flying saucer" is taken from the [Larousse encyclopedia](http://www.nanoworld.org.ru/data/01/data/images/encyclop/larosse/index.htm)

For this, the "flying saucer" after the main production had to return from the launch pad and fill up the rim protrusion. "Tiny" - thought Dr. Watson...

Well, things are clear with the Dmitrovsky rim, but what does Easter Island have to do with it? There is no such rim there...

The rim is not there, but compared to what happened on Easter Island, Dmitrovsky rim is just a prank. And it was like this. I am sitting at home, reading the AIF. And there, Ernst Muldashev tells how he went to Easter Island.

<!-- Local
![](Planet Earth is a giant abandoned mine_files/140.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/3306/nanoworld.bd/0_1ff12_887d64a4_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/3306/nanoworld.bd/0_1ff12_887d64a4_orig.jpg)

Geometric examination by Alexander Kushelev

It turns out that so much was cut off from this Rano Raraku volcano that it was possible to make not 1000, but a million idols weighing 10 tons each! In floods, I thought. Need to check. I go to the Internet, to Google Maps, and see:

<!-- Local
![](Planet Earth is a giant abandoned mine_files/018.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/51/nanoworld.8a/0_e7ae_e25290a4_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/51/nanoworld.8a/0_e7ae_e25290a4_orig.jpg)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/015.gif#clickable)](http://img-fotki.yandex.ru/get/51/nanoworld.89/0_e6e5_596ebd15_orig.gif)
-->

[![](http://img-fotki.yandex.ru/get/51/nanoworld.89/0_e6e5_596ebd15_orig.gif#clickable)](http://img-fotki.yandex.ru/get/51/nanoworld.89/0_e6e5_596ebd15_orig.gif)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/103.jpg#clickable)](http://img-fotki.yandex.ru/get/2710/nanoworld.bd/0_1ff04_9d960407_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/2710/nanoworld.bd/0_1ff04_9d960407_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/2710/nanoworld.bd/0_1ff04_9d960407_orig.jpg)

<!-- Local
![](Planet Earth is a giant abandoned mine_files/si3HVXekl4FgpRXlQi-kNej0VTGCHuI4JaOrDvtC9zDXaR-QvaAnQp56t1aQrl8Lv6niYkkT8poYyyPAReoiJ0BZuTf4CiJdopMjN9PmJhA)
-->

![](http://img-fotki.yandex.ru/get/51/nanoworld.89/0_e6e5_596ebd15_orig.gif#resized)

[Rano Raraku volcano. Reconstruction by Alexander Kushelev](http://img-fotki.yandex.ru/get/51/nanoworld.89/0_e6e5_596ebd15_orig.gif)

Rano Raraku volcano, which looks like a round cake, from which a piece has been neatly cut. The piece is about 800 meters long and over 100 meters high. This is seven times higher than the Dmitrovsky rim... In short, someone cut off 10 million tons from the volcano. This is more than the mass of the entire population of Russia :)

<!-- Local
![](Planet Earth is a giant abandoned mine_files/si3HVXekl4FgpRXlQi-kNWE6YcAJC5vBv6Z3uXOQdffuVOn_BEIOqYUyD0DwyNnWr0AhcIVN6RkAkm7NMAuD709wMRQCSSpRwYFa06CDyTYILRKs_LAw08fB9MHhbsK5)
-->

![Image no longer available]()

Physical and geometric examination of Alexander Kushelev

Why would anyone want a million 10-ton idols? To play toy soldiers? And what, they are conducting military maneuvers, shooting at wooden (and not only!) targets. Why not shoot at 10-ton (and there are also 300-ton soldiers on Easter Island!) soldiers?

<!-- Local
![](Planet Earth is a giant abandoned mine_files/924.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/32/nanoworld.94/0_1293e_7dbfd34a_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/32/nanoworld.94/0_1293e_7dbfd34a_orig.jpg)

Illustration from the book of Thor Heyerdahl

The purpose of the maneuvers is known - to maintain shape in the event of an attack. It remains to understand what they fought for? And I decided to look at Easter Island from the height of the satellite, i.e. "through the eyes of aliens"...

<!-- Local
![](Planet Earth is a giant abandoned mine_files/018_129592.jpg#resized)
-->

[![](https://img-fotki.yandex.ru/get/3005/nanoworld.b9/0_1fa38_f60fdc8f_orig.jpg#clickable)](https://img-fotki.yandex.ru/get/3005/nanoworld.b9/0_1fa38_f60fdc8f_orig.jpg)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/158.gif#clickable)](http://img-fotki.yandex.ru/get/3000/nanoworld.be/0_200a6_3ec36a50_orig.gif)
-->

[![](http://img-fotki.yandex.ru/get/3000/nanoworld.be/0_200a6_3ec36a50_orig.gif#clickable)](http://img-fotki.yandex.ru/get/3000/nanoworld.be/0_200a6_3ec36a50_orig.gif)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/fantom.gif#clickable)](http://img-fotki.yandex.ru/get/3006/nanoworld.bf/0_2020c_b23496b3_orig.gif)
-->

[![](http://img-fotki.yandex.ru/get/3006/nanoworld.bf/0_2020c_b23496b3_orig.gif#clickable)](http://img-fotki.yandex.ru/get/3006/nanoworld.bf/0_2020c_b23496b3_orig.gif)

[Maunga Parehe. Alexander Kushelev's reconstruction](http://fotki.yandex.ru/users/nanoworld/view/129693?page=6)

Who poured three mounds on Easter Island?

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/1234_big.jpg#clickable)](http://img-fotki.yandex.ru/get/12/nanoworld.0/0_5a7b_52416733_orig.jpg)
-->

[![](http://img-fotki.yandex.ru/get/12/nanoworld.0/0_5a7b_52416733_orig.jpg#clickable)](http://img-fotki.yandex.ru/get/12/nanoworld.0/0_5a7b_52416733_orig.jpg)

They are so huge that people thought they were volcanic domes...

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/005.gif#clickable)](http://img-fotki.yandex.ru/get/30/nanoworld.93/0_123b6_47f786da_orig.gif)
-->

[![](http://img-fotki.yandex.ru/get/30/nanoworld.93/0_123b6_47f786da_orig.gif#clickable)](http://img-fotki.yandex.ru/get/30/nanoworld.93/0_123b6_47f786da_orig.gif)

*The grid shows the slope surface. The light right of mound N3 does not appear below the grid. It is the light rock is above the sloping surface.*

[Maunga Parehe, maunga Tea-Tea, maunga Vai-Heva](http://fotki.yandex.ru/users/nanoworld/album/37688/?p=6)

But Sherlock Holmes conducts his own investigation and makes sure that the lighter material of the "mounds" does not fall below the surface of the slope. And this means that they were poured from above... Later it turns out that there are a dozen more mounds on Easter Island, and the total mass of the megalithic complex exceeds 60 million tons. This is more than the mass of the Rano Raraku volcano... The proposition that only a handful of people created such a thing is like proposing ants might build a tank. In my estimation, the aliens mined several million tons of precious metals on Easter Island. It can be compared to mining in Egypt. There, other aliens heated the nummulite deposits with dielectric reflectors in the shape of pyramids. Tens of millions more tons of precious metals were mined from the Dead Sea. Several tons of precious metals per day were mined in Baalbek, evaporating the largest river in ancient Lebanon, the Leonte. But they could have extracted the most from the Marianas Trench. According to my estimates, as much as one cubic kilometer of gold could be extracted from sea water there. Do you need a cubic kilometer of gold? ;)

Aliens take away from under our noses millions of tons and even cubic kilometers of precious metals... Is it really that bad?

Why? As the famous chess player used to say, one lost game of chess is worth ten won... If we learn to extract precious metals as efficiently as aliens, then we will be able to extract millions of times more efficiently on our planet, and "take revenge" on other planets...

What, the aliens will let us take revenge?

Let's apply the famous deduction method. Those aliens who mined precious metals and poured the Dmitrovsky rim have long created a "self-assembled tablecloth" and they are no longer interested in mines. And the aborigines, on whose planets we can take revenge, do not yet know that there are "flying saucers" with aliens :)

Let's admit. And who will tell us about the technology of extraction of precious metals by aliens?

Well, okay, listen to the third story... It was the summer of 7515 from the Creation of the World or 2007 in the new style. I was returning with mushrooms from the forest. Before reaching my summer cottage, I noticed a circle of crushed grass. The blades of grass were laid very neatly, not broken, but bent at the knees... Examination under a microscope showed that there were burns on the knees, as in case of strong radiation. The blades of grass were twisted into spirals, as if charged particles were flying in a strong magnetic field under the influence of an electric field... Then I decided to check if there were precious metals in this place? Mass spectral analysis showed that everything in the soil is normal. In plants too. However, analysis of spring water showed that the concentration of gallium in it is approximately 700 times higher than in seawater. Water brings gallium to the surface but it is practically absent in irradiated plants and soil. Where did he go? It was then that I imagined charged gallium ions flying in spirals... And who charged them? And then I remembered about ionizing radiation. Maybe it left burns on the knees of the plants? But why did the radiation ionize only gallium? I decided to figure it out and found out that there is characteristic radiation that is absorbed only by certain chemical compounds or elements, for example, gallium... This is how the technology of mining precious metals became clear in the first approximation. At midnight, when the Sun does not interfere with photographing the Earth's surface with flares that have a special spectrum, the aliens find spots with an increased concentration of precious metals. Then they irradiate these areas with characteristic radiation, ionizing only what they need. And you can extract ionized precious metals with an ordinary electric field. And we can do that when we have cheap electricity. After all, the power of the characteristic X-ray radiation should be several megawatts, and in a couple of hours a ton of gallium can be pulled out of a small swamp...

Well it is clear. And where to get money for the creation of new energy, new mining industry?

For a start, you can organize an expedition to Easter Island, prove to mankind that apart from volcanoes on the island there are a dozen bulk hills, the mass of which is close to the mass of the Rano Raraku volcano. When it becomes obvious to many that the aliens have mined tens of megatons of precious metals there, then there will be people who also want to extract millions of tons of precious metals. In the meantime, there are such people, you can organize a museum right under the fill mound, making a tunnel under it. After all, a volcano must have a vent along which lava rose from the ground, while a bulk mound has no vent. Each visitor to the Museum "Under the Alien Roof" will be able to make sure that there is no vent, which means it is under the "Alien Roof"...

If every visitor to the museum cashes in a dollar, then very soon tourists will be able to fly over Easter Island on home-made flying bracelets, like bird-people from Easter Island;)

<!-- Local
[![](Planet Earth is a giant abandoned mine_files/042.gif#clickable)](http://img-fotki.yandex.ru/get/3108/nanoworld.ed/0_25ddc_8631857f_orig.gif)
-->

[![](http://img-fotki.yandex.ru/get/3108/nanoworld.ed/0_25ddc_8631857f_orig.gif#clickable)](http://img-fotki.yandex.ru/get/3108/nanoworld.ed/0_25ddc_8631857f_orig.gif)

*Illustration from the book of Thor Heyerdahl*

Image text from top left:

*Tangata-ika - The Seal Man. This is one of the rarest sculptures on Easter Island (height 32 cm)*

*Moai-kawakawa, rear view. You should pay attention not only to the very strange image of the spine, but also to its anomaly.*

Image text from top right:

*Moai-kawakawa, front view. There is nothing Polynesian in the style of his image (height 51 cm)*

*Bird-man (height 24 cm). This is a very rare sculpture from Toromiro.*

------------------------------------------------------------------------

[Discuss on forum](http://www.nanoworld.org.ru/forum/viewforum.php?id=2)

**Investment in scientific projects**

We invite investors and sponsors.

How to get huge profits?

- Invest
in research and development.

The latest types of energy sources, high technologies.

[Commercial finished products](http://redirect.subscribe.ru/science.news.nanoworldnews,48798/20070426112217/n/m0/-/science.news.nanoworldnews,48798/20070415001242/n/m0/-/science.news.nanoworldnews,48798/20070413144059/n/m0/-/www.nanoworld.org.ru/data/01/data/products/index.htm)

[Projects](http://redirect.subscribe.ru/science.news.nanoworldnews,48798/20070426112217/n/m0/-/science.news.nanoworldnews,48798/20070415001242/n/m0/-/science.news.nanoworldnews,48798/%2020070413144059/n/m0/-/www.nanoworld.org.ru/data/01/data/projects/index.htm)

Alexander Yurievich Kushelev. Laboratory "Nanomir"

Moscow city

Phones:
8-926-510-1703 Megafon
8-903-200-3424 Beeline
8-916-826-5031 MTS

e-mail: nanoworld2003 # narod.ru

Website: [http://nanoworld.narod.ru](http://redirect.subscribe.ru/science.news.nanoworldnews,48798/20070426112217/n/m0/-/science.news.nanoworldnews,48798/20070415001242/n/m0/-/science.news.nanoworldnews,48798/20070413144059/n/m0/-/nanoworld.narod.ru/)

Yaroslav Starukhin (chief manager of lab.Nanomir)

Phone: 8-926-2109383

[Permanent posting address of the article.](http://nanoworld.narod.ru/151.htm)

Supplement as of 17:15, 25-07-2014. Bonus: A related film: [https://youtu.be/8j5qKnavubI](https://youtu.be/8j5qKnavubI)

##### Comments

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 08:33 am (UTC)][[]]
Horses and sobies with halos.

In addition:
Horses and sobs with halos.
[http://svr.su/content/item/3431/](http://svr.su/content/item/3431/)

[http://www.liveinternet.ru/users/3296663/post257387081/](http://www.liveinternet.ru/users/3296663/post257387081/)

This is certainly no secret, well, except that it denigrates the opinion of modern believers about their own holiness and the role of people in business gods.
[http://dmitrijan.livejournal.com/71535.html?thread=1185135#t1185135](http://dmitrijan.livejournal.com/71535.html?thread=1185135#t1185135)

PS: So non-humans with the muzzles of horses, dogs and other living creatures are quite real creatures that, until recently, fly so 300 back could be found.

Naturally, after the "pictures" were smeared.

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Jul. 25th, 2014 10:49 am (UTC)]
Re: Horses and sobs with halos.

Thank you! A very nice addition.

[**Alexey Kucherenko**](https://ext-481695.livejournal.com/)
[Jul. 25th, 2014 05:52 pm (UTC)]
Yes, there was a case

The Pseglawites lived in northern Africa and were very angry. For their aggressiveness they were demolished. Cats (people with cat genes) remained. And centaurs or polkans are already a product of genetic technology. Something like that.

There are plans to restore the lost race of magicians. They don't seem to be going to restore the Pseglavians.

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 06:19 pm (UTC)]
Re: Yes, there was a case

It is always great when there are those who are initiated into the affairs of higher powers, with whom these powers consult, ask for advice and share their plans.

[**Alexey Kucherenko**](https://ext-481695.livejournal.com/)
[Jul. 25th, 2014 06:37 pm (UTC)][[]]
Re: Yes, there was a case

The higher powers are very unhappy with the inertia of the majority and are going to remove 98% of such a population from the planet.

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 07:07 pm (UTC)][[]]
Re: Yes, there was a case

And dill is an example of this.
Yes, but why do higher powers need the remaining 2%? To answer their questions or not to get bored?

[**Alexey Kucherenko**](https://ext-481695.livejournal.com/)
[Jul. 25th, 2014 07:17 pm (UTC)][[]]
Re: Yes, there was a case

They contain a particle of the Absolute and they are capable of self-development.

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 07:57 pm (UTC)][[]]
Re: Yes, there was a case

It turns out that people are bred like tomatoes in the beds?

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Jul. 25th, 2014 08:07 pm (UTC)][[]]
Re: Yes, there was a case

What? Is this the first time you have such a thought?

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 08:38 pm (UTC)][[]]
Re: Yes, there was a case

Judging by the dill, the harvest will be rich:
Kindergarten with a pomegranate
<http://vvv-ig.livejournal.com/768201.html>

Sect Ukraine
<http://loboff.livejournal.com/460872.html>

[**dmitrijan**](https://dmitrijan.livejournal.com/)
[Jul. 25th, 2014 08:48 pm (UTC)][[]]
Re: Yes, there was a case

As if somehow like this, some 2007:

[Farm Earth: TxT_093_22102007 Text 093](http://dmitrijan.livejournal.com/27346.html) in which: The purpose of people in the divine world is considered from quite practical and natural points of the MiRa. For every creature and creature in this world is intended and has its own role. And even if it is not visible to them and to us, this does not at all mean that this is not in the essence, which is provided for for people by Nature, MiRom, the World, Divine Providence, the Creator, the Creator or the One Being, and quite possibly the Great Cosmic Something. Which, naturally, have their own plans and opinions about the presence of people in our world, the Plan of Worlds, on planet Earth. And these opinions, as well as destinations with a purpose, can radically differ from the desires of the people themselves with their own conceit. Or they may not be different.
[![Farm Land](https://ic.pics.livejournal.com/dmitrijan/42576892/26049/26049_original.jpg#clickable)](http://dmitrijan.livejournal.com/27346.html)

*Lone sheep: That man and the dog are definitely working together*

*Sheep in flock: For God's sake Hank, you're back with your conspiracy theories...*

Possible coincidences with real events are accidental.
[http://dmitrijan.livejournal.com/27346.html](http://dmitrijan.livejournal.com/27346.html)

Will it go? And a funny year - 2007. Almost 7 years have passed, no?

[Re: Horses and sobs with halos.](https://kadykchanskiy.livejournal.com/264787.html?thread=21452115#t21452115) [- ] [[**Nikolay Pestov**](https://ext-2765698.livejournal.com/)][ - ][Nov. ][26th, 2015 02:52 pm (UTC) ][[- ]][[Expand](https://kadykchanskiy.livejournal.com/264787.html?thread=21452115#t21452115)][(https://kadykchanskiy.livejournal.com/264787.html?thread=21452115#t21452115)]

[**izofatov**](https://izofatov.livejournal.com/)
[Jul. 25th, 2014 08:57 am (UTC)][[]]
I HAVE ALWAYS STATED THAT KUSHELEV --- HEAD --- ONLY HE DOWN THE SCALE - IN MY OBSERVATIONS, THEY AND THE TERRITORY OF RUSSIA HAVE GOTHED ALWAYS AND CROSSED - SEE MY PHOTOALVELOMBOMA - LOOK AT ME THE PHOTOALVEBOMA ALL THEIR WORK --- ALL OF THIS Pulls ON AN ARTICLE OF THE CRIMINAL CODE ---- ALIEN PLANET OF THE EARTH

[**makha0na**](https://makha0na.livejournal.com/)
[Jul. 25th, 2014 09:41 am (UTC)][[]]
What is this? Maybe the Earth is their territory, but we are simply allowed to live on it, like ants near a mine. Is it a pity?

[**Stas Sushkov**](https://ext-1626110.livejournal.com/)
[Jul. 25th, 2014 10:42 pm (UTC)][[]]
I agree with you. I observed the same thing recently in Georgia. There are "mountains" along the rivers, but inside there is empty rock. Very often there are embankments along the rivers on ONE side. A very interesting topic.

[**yamaha3**](https://yamaha3.livejournal.com/)
[Aug. 1st, 2014 07:05 pm (UTC)][[]]
On GoogleMaps I found a place in the Siberian taiga, where the taiga is planted like beds, hundreds of square kilometers in the wilderness

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Aug. 3rd, 2014 08:48 am (UTC)][[]]
Alexander, hello! Glad to see you in health! Coordinates please...

[**yamaha3**](https://yamaha3.livejournal.com/)
[Aug. 3rd, 2014 01:44 pm (UTC)][[]]
Hey!
With the past dr!

I am looking, while I was looking for a landing, I found it here, it looks like a Nazca plateau, but here:
w 62 ° 52'7.59 "N
D 121 ° 40'18.98" E

[**yamaha3**](https://yamaha3.livejournal.com/)
[Aug. 3rd, 2014 01:59 pm (UTC)][[]]
stripes

61 ° 52'50.73 "N
126 ° 5'34.67"

<!--
![one](./Planet%20Earth%20is%20a%20giant%20abandoned%20mine.%20-%20Zapiski%20kolymchanin%20-%20LiveJournal_files/26583_900.jpg "one")
-->
![one](https://ic.pics.livejournal.com/yamaha3/68860910/26583/26583_900.jpg#resized)
<!--
![22](./Planet%20Earth%20is%20a%20giant%20abandoned%20mine.%20-%20Zapiski%20kolymchanin%20-%20LiveJournal_files/26777_900.jpg "22")
-->
![22](https://ic.pics.livejournal.com/yamaha3/68860910/26777/26777_900.jpg#resized)

[*Edited at [2014-08-03 02:06 pm (UTC)]*]
[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Aug. 4th, 2014 08:02 am (UTC)][[]]
Re: stripes

AND!!! I also have a mark in this place. I've already seen it.

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Aug. 4th, 2014 08:07 am (UTC)][[]]
Thank you! This is all beauty!

[**anakem**](https://anakem.livejournal.com/)
[Jul. 25th, 2014 09:08 am (UTC)][[]]
Can you tell me why he writes to me that my data is stored on the FSB server until payday and that you are their favorite friend?

[**izofatov**](https://izofatov.livejournal.com/)
[Jul. 25th, 2014 09:10 am (UTC)][[]]
YES AND THERE IS A TERM - ABANDONED MINE - READY TO ARGUE - MY OBSERVATIONS AND ANALYSIS OF CIRCLES IN THE FIELDS SAYS THAT THEY SEND PROBES SYSTEMATICALLY TO CHECK THEIR MINES - AND WE LOOK IN KHO BOKOLYAH === THEIR BLOCKS

[**frozen_herring**](https://frozen-herring.livejournal.com/)
[Jul. 27th, 2014 12:56 am (UTC)][[]]
Excuse me, are your caps stuck?

[**valerytim**](https://valerytim.livejournal.com/)
[Jul. 25th, 2014 09:18 am (UTC)][[]]
But I guessed it! Somehow, they are landing in a wrong way... (c)

[**makha0na**](https://makha0na.livejournal.com/)
[Jul. 25th, 2014 09:40 am (UTC)][[]]
This is all insanely interesting, but I would also like to understand why they need such a lot of precious metals. Do they eat them or what?
I would understand, if they pumped oil, it is at least good for fuel.

It remains to be assumed that gold and other metals can also be used as fuel.

[**izofatov**](https://izofatov.livejournal.com/)
[Jul. 25th, 2014 09:46 am (UTC)][[]]
THERE IS A LOT OF IRON IN SPACE - BUT YOU WILL NOT MAKE AN AX WITHOUT RARE EARTH METALS --- AND THEIR SHIPS WITH SIZES OF 300-500 KILOMETERS - HERE AND ESTIMATE THE VOLUME
[**vbelk**](https://vbelk.livejournal.com/)
[Jul. 25th, 2014 09:59 am (UTC)][[]]
Mining is cool

And nothing that matter is, in fact, most likely just streams of ether (medium, primordial substance, etc.) organized in the form of a vortex, torus. For the advanced it is somehow too small - to squeeze out the ready-made from the natives. If the earthlings are beginning to finish and make little by little devices a la tablecloth, then these flyers are simply chronic morons.

[**AS**](https://ext-683468.livejournal.com/)
[Jul. 25th, 2014 10:06 am (UTC)][[]]
Pure iron doesn't even rust. There is an ancient iron pillar in India with a purity that is impossible even by modern standards.

[**russ_79**](https://russ-79.livejournal.com/)
[Jul. 25th, 2014 12:21 pm (UTC)][[]]
they have ships 360 kilometers (!!) in diameter (seen in the Earth's atmosphere from satellites),

the sun - in general, ships of 2000-3000 km in size are spinning.

according to some sources, the moon is also an artificial object - a hollow titanium sphere.

so consider what mountains of raw materials they need for all this.

[**makha0na**](https://makha0na.livejournal.com/)
[Jul. 25th, 2014 12:34 pm (UTC)][[]]
Let's admit. Then what kind of "stealing" of resources are we talking about?
Do you ask the ants for permission when you pick mushrooms near their nest?

[**russ_79**](https://russ-79.livejournal.com/)
[Jul. 25th, 2014 12:44 pm (UTC)][[]]
did I personally write somewhere about "theft"? ))

no.
whoever can get it.
take a look at the film at the bottom of the comments, which I posted,
there, on the basis of FACTS and logic, it is shown and proved
how aliens are still extracting resources on Earth with might and main.

[**makha0na**](https://makha0na.livejournal.com/)
[Jul. 25th, 2014 12:49 pm (UTC)][[]]
And I and not to you personally. In a post somewhere it flashed. Like it was stolen from us.
Aha ha.

I'll watch the film.

[**russ_79**](https://russ-79.livejournal.com/)
[Jul. 25th, 2014 12:55 pm (UTC)][[]]
here it is more likely not "stolen",
but, since the Earth is a living organism, it has been milked out of it for millions of years.
good "milk"! ))

[**makha0na**](https://makha0na.livejournal.com/)
[Jul. 25th, 2014 01:26 pm (UTC)][[]]
Well milked. The cow usually doesn't mind being milked. We ourselves are not averse to sucking oil - black milk.

[**AS**](https://ext-683468.livejournal.com/)
[Jul. 25th, 2014 10:03 am (UTC)][[]]
Chinese

China is practically a monopoly in the extraction of rare earth elements. Fuck they let anyone break their monopoly.

[**irinamedvedeva8**](https://irinamedvedeva8.livejournal.com/)
[Jul. 25th, 2014 10:44 am (UTC)][[]]
Very interesting stuff. Thank you very much, Andrey.

Usually, after reading the post, I ponder it, periodically returning to the text, and when questions and assumptions are formed, it seems to be too late to comment on... to receive from you and readers additional materials, new ideas, explanations of people - experts in certain fields... I am writing about what came to mind in the process of reading.

1. From the point of view of the existence / absence of God the creator, the aliens, even if they created artificially the first people, are simply an additional link in the chain: instead of the question: "Who created man?", We ask: "Who created the aliens who created man?"

2. If Kushelev's version of the artificial (as a side effect of industrial development) formation of the hill is correct, then why are there aliens? Biblical stories about giants, supposedly hidden remains of giants, the use of salt by humans and animals, which allows balancing external and intracellular pressure, etc., allow us to put forward hypotheses about a different atmospheric pressure, a different gravity on the ancient Earth, giants - predecessors of modern man (ancestors or creators ?). Under different conditions of the terrestrial environment, the giants could well have built the Serpent's Ramparts, megaliths, as modern man does now: waste heaps, road and other embankments, tunnels cut in the rocks...

3. The possibility of the existence of another highly developed civilization of the Earth in antiquity is very versatile considered by NF Zhirov in the book "Atlantis. The main problems of Atlantology". It is written in con. 50s, early. 60s last century, but I "did not get my hands on" to see what our contemporaries-scientists say about the origin of the Earth's oceans and land: if anyone knows, please enlighten...

4. There is an interesting version that aliens and earthlings are the essence is one: people moved from the dying Phaeton to the Earth, "phaetonoforming" it... Ivan Katyukhin in "Who are we? Where are we from?" gives diagrams of how this resettlement, in his opinion, was carried out. I don't know anything about the author, I am a layman, it is difficult to understand whether there is a rational kernel in his hypothesis?

5. "PLANET EARTH IS A GIANT ABANDONED MINE" - if this is true, WHY was it abandoned? Perhaps scientific and technological progress is a dead-end path for the development of EVERY ORDINARY CIVILIZATION? Perhaps a person should improve himself - HIS SOUL, MIND, BODY (in a natural way - hardening, physical education, etc.), and not "conquer" the Earth, nature, destroying the Ideal created not by him? And what if the living Earth simply gets rid of the "presumptuous" parasites every time with the help of cataclysms? ..

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Jul. 25th, 2014 01:05 pm (UTC)][[]]
I don't understand the words "alien" at all. "alien", etc. I can't hear or see them. For me there is the concept of "others". Who are they? Yes, it does not matter. Neighbors. Maybe even our creators, or maybe we have different creators with them.

It is wrong to call this the Kushelev version. Many people have come to similar conclusions, including myself. But there is also an earlier statement on the same topic. Look on YouTube for a whole cycle of videos.

Maybe this is not an abandoned mine, but a mothballed one. Maybe our task is to get things useful for THEM without costing them, but at our expense instead. There are a lot of versions.

[*(no subject)*](https://kadykchanskiy.livejournal.com/264787.html?thread=12059475#t12059475) [ - ] [[**irinamedvedeva8**](https://irinamedvedeva8.livejournal.com/)][ - ][Jul. ][26th, 2014 06:59 am (UTC) ][[- ]][[Expand](https://kadykchanskiy.livejournal.com/264787.html?thread=12059475#t12059475)](https://irinamedvedeva8.livejournal.com/profile)[](https://irinamedvedeva8.livejournal.com/)] [] [][ [] [](https://kadykchanskiy.livejournal.com/264787.html?thread=12059475#t12059475)]

[*(no subject)*](https://kadykchanskiy.livejournal.com/264787.html?thread=12104275#t12104275) [ - ] [[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)][ - ][Jul. ][27th, 2014 11:22 am (UTC) ][[- ]][[Expand](https://kadykchanskiy.livejournal.com/264787.html?thread=12104275#t12104275)][[![](./Planet%20Earth%20is%20a%20giant%20abandoned%20mine.%20-%20Zapiski%20kolymchanin%20-%20LiveJournal_files/userinfo_v8.svg#clickable)](https://kadykchanskiy.livejournal.com/profile)[](https://kadykchanskiy.livejournal.com/)] [] [][ [] [](https://kadykchanskiy.livejournal.com/264787.html?thread=12104275#t12104275)]

[Everything that has been accumulated is kept in the head, but is based on the sample](https://kadykchanskiy.livejournal.com/264787.html?thread=12106067#t12106067) [- ] [[**irinamedvedeva8**](https://irinamedvedeva8.livejournal.com/)][ - ][Jul. ][27th, 2014 11:51 am (UTC) ][[- ]][[Expand](https://kadykchanskiy.livejournal.com/264787.html?thread=12106067#t12106067)][[![](./Planet%20Earth%20is%20a%20giant%20abandoned%20mine.%20-%20Zapiski%20kolymchanin%20-%20LiveJournal_files/userinfo_v8.svg#clickable)](https://irinamedvedeva8.livejournal.com/profile)[](https://irinamedvedeva8.livejournal.com/)] [] [][ [] [](https://kadykchanskiy.livejournal.com/264787.html?thread=12106067#t12106067)]

[**sherp96**](https://sherp96.livejournal.com/)
[Jul. 25th, 2014 10:51 am (UTC)][[]]
Andrey, welcome! I wanted to write a comment, but after reading it, I realized that my point of view had already been stated. Very interesting post!

[**russ_79**](https://russ-79.livejournal.com/)
[Jul. 25th, 2014 12:23 pm (UTC)][[]]
>>> 50,000 tons of gallium at prices that I found on the internet cost the same as half a million tons of gold! >>>

at prices - WILD LIES.
for production - no.

[**russ_79**](https://russ-79.livejournal.com/)
[Jul. 25th, 2014 12:25 pm (UTC)][[]]
mega-movie on the extraction of raw materials by aliens

[**kadykchanskiy**](https://kadykchanskiy.livejournal.com/)
[Jul. 25th, 2014 01:13 pm (UTC)][[]]
Re: alien mining mega movie

Good movie. I have it somewhere in the magazine. I will take it out again, to the end of the article.

[**milli_si**](https://milli-si.livejournal.com/)
[Jul. 25th, 2014 12:40 pm (UTC)][[]]
interesting, thanks!

[**full_sid**](https://full-sid.livejournal.com/)
[Jul. 25th, 2014 01:30 pm (UTC)][[]]
Yes, on Google-Earth there are a lot of such heaps in the north.

[**tusia62**](https://tusia62.livejournal.com/)
[Jul. 25th, 2014 02:14 pm (UTC)][[]]
I don't know about aliens or who they are, but it is possible that this is the work of proto-civilizations... after all, we practically do not know anything about them... neither how they lived, nor what they did, nor the basics of their technologies...

[**Elena Lena**](https://ext-2290123.livejournal.com/)
[Jul. 25th, 2014 03:29 pm (UTC)][[]]
I also believe that the traces that are now being found are the work of the humanity that was destroyed about a thousand years ago. There was an invasion of some other race, which reshaped everything for itself.
Now it is unclear who lives on Earth, with unclear what animal laws.

[**stra1k**](https://stra1k.livejournal.com/)
[Jul. 25th, 2014 02:40 pm (UTC)][[]]
An interesting version, only the water pouring out from the inside of the rim, in my opinion, is rather a consequence of the flood that destroyed Tartary 300-400 years ago.

© All rights reserved. The original author retains ownership and rights.


