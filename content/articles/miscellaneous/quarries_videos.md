title: Videos: Quarried Earth, Terraformed Earth
date: 2021-02-01
modified: 2022-08-20 09:48:21
category: 
tags: quarried Earth; terraformed Earth; videos
slug: 
authors: Various
summary: This page collects together videos on grand-scale quarrying and terraforming, with links to subtitles. Videos are from various Russian sources, primarily by Alexei Kungurov and from Oleg Nestyagin's 'Dark Side' YouTube channel. This page was a test of a third-party video           translation service. It didn't work as well as hoped but the videos may still be interesting, especially if you speak the native language.
status: published

<!--
#### Formatting from:

[http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/](http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/)
-->

<!--
https://www.youtube.com/c/ТемнаясторонапланетыЗемля Oleg Nestyagin's channel
https://www.youtube.com/channel/UCk6UxHjd-UgRRhZn7lWTn7w Alexei Kungurov's channel
-->

<iframe width="100%" height="358" frameborder="0" allowfullscreen
    src="https://www.youtube.com/embed/LEM7jO-n6-4?autoplay=0&t=0s">
</iframe>

Earth, The Giant Ancient Mine, Fake Volcanoes, the Bosnian Pyramid and Petra. [Source](https://www.youtube.com/watch?v=LEM7jO-n6-4). English voiceover.


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/xBluZ5p8-CQ/0.jpg)](https://youtu.be/xBluZ5p8-CQ)

[Earth is a giant mine of past civilizations](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/zemlya-gigantskij-rudnik-civilizacij-proshlogo/). A review of [WakeUpHuman's Quarried Earth article](https://wakeuphuman.livejournal.com/1342.html), which is [available in English here](./quarried-earth-an-industrially-developed-civilization-has-existed-on-earth-for-tens-of-thousands-of-years.html). Reviewer follows WakeUpHuman's Google search instructions to show more photographs of deserted Eurasia and craters.

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/xBluZ5p8-CQ)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/Pxr-ShRCLMQ/0.jpg)](https://youtu.be/Pxr-ShRCLMQ)

[Planet Earth is a Giant Mined Out Quarry](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/planeta-zemlya-gigantskij-vyrabotannyj-karer/). Another review of WakeUpHuman's Quarried Earth video.

- [Subtitles .srt files](https://downsub.com/?url=https%3A%2F%2Fyoutu.be%2FPxr-ShRCLMQ)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/8H4HFXq07W4/0.jpg)](https://youtu.be/8H4HFXq07W4)

[The Giant Quarry of Planet Earth; The Crimean Trail](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/giganskij-karer-planety-zemlya-krymskij-sled/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/8H4HFXq07W4)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/bWYhrQoYS8Y/0.jpg)](https://youtu.be/bWYhrQoYS8Y)

[15 Meters of Clay. Taman Crimean Bridge](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/15-metrov-gliny-taman-krymskij-most/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/bWYhrQoYS8Y)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/rXV88_nofJE/0.jpg)](https://youtu.be/rXV88_nofJE)

[Artificial Mountains. Selected observations. Parsing the evidence](https://youtu.be/rXV88_nofJE)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/rXV88_nofJE)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/6AI-iPkIDKs/0.jpg)](https://youtu.be/6AI-iPkIDKs)

[Artificial dumps in Bashkiria. Evidence of Terraforming](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/iskusstvennye-otvaly-bashkirii-dokazatelstva-terraformacii/)
    
- [English subtitle .srt file (RoHT translation)](../../files/Искусственные отвалы Башкирии.  Доказательства Терраформации.-6AI-iPkIDKs.srt)

Translator's note: This subtitles file is significantly better than the English subtitles available from [Downsub.com](https://downsub.com/) used for the other subtitles on this page. You may want to watch this video before turning to the poorer quality subtitles.


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/qYq3i42p5cs/0.jpg)](https://youtu.be/qYq3i42p5cs)

[How to create a false history of planet Earth](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/kak-sozdayut-falshivuyu-istoriyu-planety-zemlya/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/qYq3i42p5cs)

<!--
- [English subtitle .srt file (RoHT translation)](../../files/)
-->

[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/4FWR4dxaQmM/0.jpg)](https://youtu.be/4FWR4dxaQmM)

[Giant maze and quarries in Pakistan](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/gigantskij-labirint-i-karery-v-pakistane/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/4FWR4dxaQmM)


[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/t0OkmKiGiX0/0.jpg)](https://youtu.be/t0OkmKiGiX0)

[Giant black boulder. The second largest in the world](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/gigantskij-chernyj-valun-vtoroj-po-velichine-v-mire/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/t0OkmKiGiX0)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/X9NpbGSbehE/0.jpg)](https://youtu.be/X9NpbGSbehE)

[Ancient quarries, technology and geology of the Crimea are amazing](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/drevnie-karery-texnologii-i-geologiya-kryma-porazhayut-voobrazhenie/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/X9NpbGSbehE)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/uOV71dWTrr0/0.jpg)](https://youtu.be/uOV71dWTrr0)

[Utah's Ancient Quarries](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/drevnie-karery-yuty/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/uOV71dWTrr0)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/NpPo5KWXjuk/0.jpg)](https://youtu.be/NpPo5KWXjuk)

[Quarries of previous civilizations. Ambuk Mountain, Yergaki](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/karery-predydushhix-civilizacij-gora-ambuk-ergaki/)i. Alexei Kungurov shows how some natural-looking rock-face features were created artificially.

- [Subtitles .srt files](https://downsub.com/?url=://youtu.be/NpPo5KWXjuk)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/TQYkuw_-Rk4/0.jpg)](https://youtu.be/TQYkuw_-Rk4)

[Artifacts of previous civilizations. Two types of quarries](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/artefakty-predydushhix-civilizacij-karery-dvux-tipov/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/TQYkuw_-Rk4)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/x9FlDkSngy8/0.jpg)](https://youtu.be/x9FlDkSngy8)

[THE INVISIBLE IN PERU - Quarries of an unknown civilization](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/neveroyatnoe-v-peru-karery-neizvestnoj-civilizacii/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/x9FlDkSngy8)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/eWt4o6hif3M/0.jpg)](https://youtu.be/eWt4o6hif3M)

[The most unusual abandoned quarries in Russia](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/samye-neobychnye-zabroshennye-karery-v-rossii/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/eWt4o6hif3M)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/8j5qKnavubI/0.jpg)](https://youtu.be/8j5qKnavubI)

From: [Planet Earth is a giant abandoned mine](http://kadykchanskiy.livejournal.com/264787.html)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/8j5qKnavubI)


<!--
Alexei Kungurov videos from [](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/category/kungurov/)

- [https://youtu.be/irl7AMoNjQc](https://youtu.be/irl7AMoNjQc)
    - Subtitles from [https://downsub.com/?url=https://youtu.be/irl7AMoNjQc](https://downsub.com/?url=https://youtu.be/irl7AMoNjQc)


- [https://youtu.be/Ip-Z7-DdIc0](https://youtu.be/Ip-Z7-DdIc0) 
    - Subtitles: [https://downsub.com/?url=https://youtu.be/Ip-Z7-DdIc0](https://downsub.com/?url=https://youtu.be/Ip-Z7-DdIc0)
    - [English subtitles as file](../../files/[English] Повод довести информацию. Знания Храма Света. [DownSub.com].srt)


- [013 About the Siege of Leningrad](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/013-voprosy-vov-o-blokade-leningrada/)
    - [https://youtu.be/ILzCEVrPRT8](https://youtu.be/ILzCEVrPRT8)
    - [Subtitles](https://downsub.com/?url=https://youtu.be/ILzCEVrPRT8)


- [016 The Scale of the Evacuation](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/016-voprosy-vov-masshtaby-evakuacii/)
    - [https://youtu.be/dWiiw496S5Q](https://youtu.be/dWiiw496S5Q)
    - [Subtitles](https://downsub.com/?url=https://youtu.be/dWiiw496S5Q)
-->

