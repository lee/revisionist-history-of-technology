title: How Tartaria Died Part 5 - Backfilled cities
date: 2014-10-23
modified: 2021-08-11 06:35:08
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: Here's the weird thing: buildings built before the 1820s are mostly buried. But those buildings that were built after the 1820s have no backfill.
status: published
originally: Как погибла Тартария, ч5.zip
local: How Tartaria was Lost Part 5_files/

### Translated from: 
Как погибла Тартария, ч5.zip, [https://mylnikovdm.livejournal.com/3759.html](https://mylnikovdm.livejournal.com/3759.html) and [https://mylnikovdm.livejournal.com/3986.html](https://mylnikovdm.livejournal.com/3986.html)

**Russian:**

Во врем поездок по разным городам России я обратил внимание на то, чтомногие дома стоят буквально окнами к земле, то есть засыпаны на высоту от метра до двух. Позже выяснилось, что на этот факт обращают внимание имножество других людей, но только не официальная наука. Официальное объяснение этого факта состоит в том, что дома постепенно погружаются вземлю, поскольку "растёт культурный слой" за счёт постепенного накопления пыли, грязи и мусора. При этом в некоторых источниках дажеуказывается, что скорость роста культурного слоя может достигать 1 метра за 100 лет или в среднем по сантиметру в год.

**English:**

During my travels in different cities of Russia, I noticed that many houses are literally windows to the ground, that is, filled to a height of one meter to two. Later it turned out that a lot of other people pay attention to this fact, but not official science. The official explanation for this fact is that the houses are gradually sinking into the ground, or because the "cultural layer" is growing due to the gradual accumulation of dust, dirt and garbage. At the same time, some sources even point out that the growth rate of the cultural layer can reach 1 meter in 100 years, or an average of one centimeter per year.

**Russian:**

Но вот что странно. Засыпанными в основном оказываются здания исооружения построенные до 1820-х годов. А вот у тех зданий, которые построены после 1820-го года никакой засыпки не наблюдается. То есть,получается, что после 1820 года культурный слой стал вдруг расти значительно медленнее? Ведь прошло уже почти 200 лет, так что пусть еслине 2 метра, то хотя бы 20 см должно было образоваться.

**English:**

But here's the weird thing: buildings built before the 1820s are mostly buried. But those buildings that were built after the 1820s have no backfill. So, it turns out that after 1820 the cultural layer suddenly began to grow much slower? After all, almost 200 years have passed, so if only 2 meters, then at least 20 cm of backfill should have formed.

**Russian:**

Второй важный момент состоит в том, что в тех случаях, когда ведутся строительные или реставрационные работы и этот "культурный слой"раскапывается, то выясняется, что состоит он из глины или суглинка и практически не содержит мусора, пыли и прочих составляющих, которыедолжны быть в "культурном слое".

**English:**

The second important point is that when construction or restoration work is carried out and this "cultural layer" is excavated, it turns out that it consists of clay or loam and contains practically no debris, dust and other components that should be in the "cultural layer".

**Russian:**

Например, вот как выглядит срез грунта в Санкт-Петербурге во время производства ремонтных работ на дороге вдоль одного из каналов.

**English:**

For example, here is what a cut of soil looks like in St. Petersburg during repair work on the road along one of the canals.

<!-- Local
[![](How Tartaria was Lost Part 5_files/37232_900.jpg#clickable)](How Tartaria was Lost Part 5_files/37232_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/37232/37232_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/37232/37232_original.jpg)

**Russian:**

Под слоем асфальта хорошо видна первая мостовая, далее слой суглинка, под которым отчётлива видна вторая мостовая! Причём толщина слоя порядка30-40 см. Если следовать официальной версии про "культурный слой", то получается, что после строительства нижней мостовой её вообще никто неубирал и не чистил? А как же дожди и регулярные наводнения? Опять же, это центр города, где большая часть дорог и проездов была замощена, тоесть, такому количеству глины было просто неоткуда взяться на дороге. То, что могли завести телеги и кареты с просёлочных грунтовых дорог,должно было уже облететь намного раньше. А если это всё было принесено тем или иным способом из дворов домов, то тогда там должны былиобразоваться приличные ямы, поскольку если где-то прибыло, то значит оно откуда-то убыло. Мне много раз приходилось бывать в Санкт-Петербурге, втом числе срезать путь по дворам-колодцам в старых кварталах, но я ни разу не видел, чтобы уровень земли во дворах был бы заметно ниже, чемуровень улиц.

**English:**

Under the asphalt layer you can clearly see the first pavement, then the loam layer, under which you can clearly see the second pavement! And the thickness of the layer is about 30-40 cm. If you follow the official version about the "cultural layer", it turns out that after the construction of the lower pavement, it was not cleaned or removed at all? And what about rains and regular floods? Again, it is the center of the city, where most of the roads and passages were frozen, that is, so much clay was just nowhere to get on the road. What could have been started by carts and carriages from the countryside dirt roads should have flown much earlier. And if all this was brought by one way or another from the yards of houses, then there should have been decent pits, because if it arrived somewhere, it means that it has left somewhere. I've been to St. Petersburg many times, including cutting the way through yards and wells in old neighborhoods, but I've never seen the level of land in the yards noticeably lower than the level of the streets.


**Russian:**

Но подобный слой толщиной от одного до двух метров наблюдается во всему городу, а не только в каком-то одно месте, причём даже там, где простоне могло образоваться толстого культурного слоя. Слоистая структура, аналогичную той, что мы видим на фотографии, обнаружилась и на ДворцовойПлощади во время её реконструкции. Нас хотят уверить, что центральную площадь государства не убирали в течении ста лет?

**English:**

But such a layer of thickness from one to two meters is observed in the whole city, not only in one place, and even in places where a thick cultural layer could form. The layered structure, similar to the one we see in the photo, was also found on Palace Square during its reconstruction. Are we to be assured that the central square of the state has not been cleaned for a hundred years?

<!-- Local
[![](How Tartaria was Lost Part 5_files/37417_900.jpg#clickable)](How Tartaria was Lost Part 5_files/37417_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/37417/37417_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/37417/37417_original.jpg)

**Russian:**

На фотографии, сделанной в 2002 году на северо-западном углу Дворцовой площади, красной стрелкой отмечена чёрная полоса, которуюпо TV объявили "дневной поверхностью времён Екатерины". От синей стрелки начинается асфальтовый слой. Между ними слои суглинка и песка. И этоточно не "культурный слой", поскольку он должен выглядеть именно так, как выглядит тонкая чёрная полоска.

**English:**

A photograph taken in 2002 on the northwest corner of Palace Square shows a black stripe with a red arrow, which TV called "the daily surface of Catherine's time". The blue arrow marks the beginning of the asphalt layer. There are layers of loam and sand between them. And this is not a "cultural layer", because it should look exactly as a thin black strip looks.


**Russian:**

А вот целая серия фотографий из Петропавловской крепости, присланная мнеодним из читателей, где также наблюдается весьма приличный слой грунта, который никак нельзя объяснить "культурным слоем", поскольку натерритории крепости, куда доступ был ограничен, он просто не мог образоваться в таком количестве.

**English:**

And here is a whole series of photos from the Peter and Paul Fortress, sent to me by one of the readers, where there is also a very decent layer of soil, which can not be explained by the "cultural layer," because on the territory of the fortress, where access was limited, it simply could not be formed in such quantity.


**Russian:**

Вот так, например, выглядят сегодня стены Невской крутины изнутриПетропавловской крепости, откопанные при реставрации.

**English:**

For example, this is how the walls of Nevskaya Krutina look today from within the Peter and Paul Fortress, excavated during restoration.

<!-- Local
[![](How Tartaria was Lost Part 5_files/37660_900.jpg#clickable)](How Tartaria was Lost Part 5_files/37660_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/37660/37660_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/37660/37660_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/37988_900.jpg#clickable)](How Tartaria was Lost Part 5_files/37988_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/37988/37988_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/37988/37988_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/38243_900.jpg#clickable)](How Tartaria was Lost Part 5_files/38243_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/38243/38243_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/38243/38243_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/38564_900.jpg#clickable)](How Tartaria was Lost Part 5_files/38564_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/38564/38564_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/38564/38564_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/38767_900.jpg#clickable)](How Tartaria was Lost Part 5_files/38767_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/38767/38767_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/38767/38767_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/39012_900.jpg#clickable)](How Tartaria was Lost Part 5_files/39012_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/39012/39012_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/39012/39012_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/39192_900.jpg#clickable)](How Tartaria was Lost Part 5_files/39192_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/39192/39192_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/39192/39192_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/39595_900.jpg#clickable)](How Tartaria was Lost Part 5_files/39595_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/39595/39595_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/39595/39595_original.jpg)

**Russian:**

Строить крепостные стены, зарывая их в землю, не имеет никакого смысла, наоборот, стены крепости стараются сделать повыше. Очевидно, что слойгрунта появился внутри крепости уже после строительства стен. И это точно не "культурный слой".

**English:**

Building the fortress walls, burying them in the ground, makes no sense. On the contrary, the walls of the fortress are meant to be higher. Obviously, the layer of soil appeared inside the fortress after the walls were built. And this is definitely not a "cultural layer".

**Russian:**

Это Невские ворота, внутри которых в стенах две двери, одна из которыхсейчас заложена. Примечательно то, что уровень пола у помещений, а значит и дверей, сделан на полтора метра ниже современного уровня земли.

**English:**

This is the Nevsky Gate, inside of which there are two doors, one of which is now laid out (removed?). It is noteworthy that the floor level at the premises, and thus the doors, is one and a half meters below the modern ground level.

<!-- Local
[![](How Tartaria was Lost Part 5_files/39832_900.jpg#clickable)](How Tartaria was Lost Part 5_files/39832_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/39832/39832_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/39832/39832_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/40010_900.jpg#clickable)](How Tartaria was Lost Part 5_files/40010_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/40010/40010_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/40010/40010_original.jpg)

**Russian:**

Вот только у помещений стараются не делать пол ниже уровня земли, чтобыне иметь проблем с подтоплением во время дождей. Может быть это просто вход в подвал? Ничего подобного. Многие здания и сооружения вПетропавловской крепости стоят окнами в землю, а это значит, что уровень пола там будет ниже современного уровня земли.

Нарышкин бастион, за пушками окна идут почти по самой земле.

**English:**

They try not to position the inside floor below ground level so as to avoid problems with flooding during rainfall. Maybe it's just the entrance to the basement. No, it's not. Many buildings and structures in the Peter and Paul Fortress have windows below ground level, which means that the floor level there will be below the modern ground level.

Naryshkin bastion, behind the guns, the windows start almost at ground level.

<!-- Local
[![](How Tartaria was Lost Part 5_files/40440_900.jpg#clickable)](How Tartaria was Lost Part 5_files/40440_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/40440/40440_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/40440/40440_original.jpg)

**Russian:**

Никольские ворота, справа и слева от них окна идут по самой земле. Справа у окна стоит девушка в жёлтой футболке, подоконникниже колена. Если вы стоите в помещении, то подоконник у вас будет на уровне пояса. Это означает, что уровень пола где-то минимум на 60 смниже современного уровня земли, а должен быть минимум на полметра, а то и на метр выше. Получаем высоту лишнего грунта от метра до полутора метров.

**English:**

The gates of Nicole, the windows to the right and left of them are on the ground itself. On the right side of the window is a girl in a yellow T-shirt, under the window sill of her knee. If you stand in the room, the window sill will be at waist level. This means that the floor level is at least 60 cm below the modern ground level, and should be at least half a meter, or even a meter higher. We get the height of the extra soil from one meter to one and a half meters.

<!-- Local
![](./How Tartaria was Lost Part 5_files/Pictures/100000000000032000000218AB3B1B03.jpg#resized)
Is image 40627 below
-->

<!-- Local
[![](How Tartaria was Lost Part 5_files/40627_900.jpg#clickable)](How Tartaria was Lost Part 5_files/40627_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/40627/40627_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/40627/40627_original.jpg)

<!-- Local
![](How Tartaria was Lost Part 5_files/10000000000002BC000001D2928D7526.jpg#resized)
-->

<!-- Local
![](./How Tartaria was Lost Part 5_files/Pictures/10000000000002BC000001D2928D7526.jpg#resized)
-->

**Russian:**

Это Петровские ворота и Петровская крутина, окна опятьу самой земли.

**English:**

This is the Petrovskie Vorota and Petrovskaya Krutina, the windows again in the earth itself.

<!-- Local
[![](How Tartaria was Lost Part 5_files/41368_900.jpg#clickable)](How Tartaria was Lost Part 5_files/41368_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/41368/41368_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/41368/41368_original.jpg)

**Russian:**

А это вид сзади, снова окна у самой земли.

**English:**

And this is the view from the back, the windows at ground level again. 

<!-- Local
[![](How Tartaria was Lost Part 5_files/40960_900.jpg#clickable)](How Tartaria was Lost Part 5_files/40960_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/40960/40960_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/40960/40960_original.jpg)

**Russian:**

При этом подтретьим окном справа мы видим засыпанный канал, аналогичный которому можно увидеть откопанным выше на фото Невской крутины. Такие каналы встенах обычно делают для спуска лишней воды наружу во время паводка. Делать же делать данный канал и засыпать его потом землёй нет вообщеникакого смысла.

**English:**

By the window on the right, we can see a backfilled canal similar to the one we found above in the photo of the Nevskaya slope. Such canals are usually made to drain excess water aware during a flood. But there is no commonsense in making this canal and then covering it with earth.

<!-- Local
[![](How Tartaria was Lost Part 5_files/42718_900.jpg#clickable)](How Tartaria was Lost Part 5_files/42718_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/42718/42718_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/42718/42718_original.jpg)

**Russian:**

Дворец Меньшикова на Университетской набережной Васильевского острова.Фундамент значительно ниже современного уровня земли, окна фактически в уровень земли, пол ниже уровня окон ещё минимум на метр, в итогеполучаем примерно те же полтора метра лишней земли.

**English:**

Menshikov Palace on Universitetskaya Embankment on Vasilyevsky Island. The foundation is much lower than the modern ground level, the windows are actually in the ground level, the floor below the window level is still at least one and a half meters below ground.

<!-- Local
[![](How Tartaria was Lost Part 5_files/41595_900.jpg#clickable)](How Tartaria was Lost Part 5_files/41595_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/41595/41595_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/41595/41595_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/41965_900.jpg#clickable)](How Tartaria was Lost Part 5_files/41965_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/41965/41965_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/41965/41965_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/42235_900.jpg#clickable)](How Tartaria was Lost Part 5_files/42235_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/42235/42235_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/42235/42235_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/42424_900.jpg#clickable)](How Tartaria was Lost Part 5_files/42424_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/42424/42424_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/42424/42424_original.jpg)

**Russian:**

И подобных примеров по Санкт-Петербургу можно найти ещё много. Вот так, например, выглядит здание Зоологического музея на Васильевском острове,причём и сегодня, и в конце 19 века.

**English:**

And there are many more such examples in St. Petersburg. For example, the building of the Zoological Museum on Vasilyevsky Island looks like this, and even today, as well as in the late 19th century.

<!-- Local
[![](How Tartaria was Lost Part 5_files/42764_900.jpg#clickable)](How Tartaria was Lost Part 5_files/42764_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/42764/42764_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/42764/42764_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/43136_900.jpg#clickable)](How Tartaria was Lost Part 5_files/43136_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/43136/43136_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/43136/43136_original.jpg)

**Russian:**

Глядя на это здание можно сделать только два вывода, либо архитектор был пьян и потому запроектировал такой низкий первый этаж и несуразный вход,либо нам явно что-то недоговаривают. Особенно интересны окна внизу по правой стороне, которые практически врыты в землю. А когда вы входите вэто здание, то после двери сразу идут ступеньки вниз, поскольку уровень пола ниже уровня входа больше чем на метр.

**English:**

Looking at this building we can only draw two conclusions: either the architect was drunk and that's why he designed such a low ground floor and ridiculous entrance, or we clearly something is not being said. Especially interesting are the windows at the bottom right side, which are practically buried in the ground. And when you enter this building, after the door are the steps down - because the floor level is more than a meter below the entrance.

**Russian:**

А это подборка фотографий разных зданий с улиц Питера, часть из которыхя позаимствовал в журнале ЕнотоЖЖ [http://lone-ringtail.livejournal.com/](http://lone-ringtail.livejournal.com/), часть подобрал на просторах интернета из разных источников.

**English:**

And this is a collection of photographs of different buildings from the streets of Peter, some of which were borrowed from the Lone Ringtail magazine [http://lone-ringtail.livejournal.com/](http://lone-ringtail.livejournal.com/), some picked up on the Internet from different sources.

<!-- Local
[![](How Tartaria was Lost Part 5_files/43311_900.jpg#clickable)](How Tartaria was Lost Part 5_files/43311_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/43311/43311_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/43311/43311_original.jpg)

**Russian:**

Это дом Троекурова на Васильевском острове. Обратите внимание на входы вздание, которые ниже современного уровня земли на метр, да и весь первый этаж смотрится совершенно негармонично. Ни один вменяемый архитектортакого запроектировать не мог. При этом если почитать официальное описание тут ([http://walkspb.ru/zd/troekurov.html](http://walkspb.ru/zd/troekurov.html)), то там мы можемпрочитать вот такой шедевр 

> "В 1723-1730 годах здесь для него было построено каменное. 

**одноэтажное** здание с мезонином"!!!

**English:**

This is Troekurov's house on Vasilyevsky Island. Pay attention to the entrances of the building, which is one metre below the modern level of land, and the whole ground floor looks completely disharmonious. No sane architect could have designed this building. If you read the official description here ([http://walkspb.ru/zd/troekurov.html](http://walkspb.ru/zd/troekurov.html)), we read there such a masterpiece as: 

> "In 1723-1730, a stone building was built here for him."

A one-story building with a mezzanine!!!

<!-- Local
[![](How Tartaria was Lost Part 5_files/43311_900.jpg#clickable)](How Tartaria was Lost Part 5_files/43311_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/43311/43311_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/43311/43311_original.jpg)

**Russian:**

Автор этого шедевра видимо не в курсе, что в жилых домах вход в здание, особенно парадный, через подвальное помещение неделается. То есть, здание явно двухэтажное!

**English:**

The author of this masterpiece probably does not know that in apartment houses the entrance to the building, especially the front one, is not made through the basement. That is, the building is clearly a two-story building!

**Russian:**

В Питере, кстати, есть множество зданий построенных в 18 веке, которые в 19 были реконструированы с переносом парадных входов с перового этажа,ставшего полуподвальным, на второй. А во многих случаях, как можно увидеть на фотографиях ниже, у входов в здания нет крыльца. Точнее, оноесть, но находится под землёй и в некоторых местах мы видим только верхнюю плиту крыльца в уровень тротуара.

**English:**

In St. Petersburg, by the way, there are many buildings built in the 18th Century, which were then rebuilt in the 19th Century. During this reconstruction, the front entrances were transferred from the ground floor, which became semi-basement, to the second floor. And in many cases, as can be seen in the photographs below, the entrances to the buildings have no porch. To be more precise, the porch is still underground and in some places we see only the upper porch slab at the sidewalk level.

**Russian:**

Кожевенная линия Васильевского острова, окна в уровень земли, вход безкрыльца.

**English:**

Leather line of Vasilyevsky Island, windows to the ground level, roofless entrance.

<!-- Local
[![](How Tartaria was Lost Part 5_files/43747_900.jpg#clickable)](How Tartaria was Lost Part 5_files/43747_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/43747/43747_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/43747/43747_original.jpg)

**Russian:**

Малый проспект Васильевского острова, дом 3, окна в землю, входы безкрыльца.

**English:**

Small Prospect of Vasilyevsky Island, 3, windows at ground level, the entrances bezkryltsa (?).

<!-- Local
[![](How Tartaria was Lost Part 5_files/43833_900.jpg#clickable)](How Tartaria was Lost Part 5_files/43833_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/43833/43833_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/43833/43833_original.jpg)

**Russian:**

Угол Лиговского проспекта и ул. Растанной. Окна первого этажа занижены,входы без крыльца.

**English:**

Corner of Ligovsky Prospekt and Rastanna Street. The windows of the first floor are submerged, the entrances are without porch.

<!-- Local
[![](How Tartaria was Lost Part 5_files/44265_900.jpg#clickable)](How Tartaria was Lost Part 5_files/44265_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/44265/44265_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/44265/44265_original.jpg)

**Russian:**

Три фотографии с улицы Репина на Васильевском острове, опять вход безкрыльца, а окна практически в уровень тротуара.

**English:**

Three photos from Repina Street on Vasilyevsky Island, again the entrance to the bezkryltsa, and the windows almost at the sidewalk level.

<!-- Local
[![](How Tartaria was Lost Part 5_files/44316_900.jpg#clickable)](How Tartaria was Lost Part 5_files/44316_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/44316/44316_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/44316/44316_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/44546_900.jpg#clickable)](How Tartaria was Lost Part 5_files/44546_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/44546/44546_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/44546/44546_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/45001_900.jpg#clickable)](How Tartaria was Lost Part 5_files/45001_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/45001/45001_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/45001/45001_original.jpg)

**Russian:**

16-17 линия Васильевского острова. Окна почти у земли, вход без крыльца(справа за деревом).

**English:**

16-17 line of Vasilyevsky Island. Windows almost on the ground, entrance without porch (right behind the tree).

<!-- Local
[![](How Tartaria was Lost Part 5_files/45195_900.jpg#clickable)](How Tartaria was Lost Part 5_files/45195_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/45195/45195_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/45195/45195_original.jpg)

**Russian:**

Андреевский собор на Васильевском острове. Глядя на него создаётсяощущение, что архитектор забыл сделать фундамент и поставил собор прямо... на землю.

**English:**

St. Andrew's Cathedral on Vasilyevsky Island. Looking at it, the architect forgot to make the foundation and put the cathedral directly... on the ground.

<!-- Local
[![](How Tartaria was Lost Part 5_files/45488_900.jpg#clickable)](How Tartaria was Lost Part 5_files/45488_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/45488/45488_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/45488/45488_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/45650_900.jpg#clickable)](How Tartaria was Lost Part 5_files/45650_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/45650/45650_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/45650/45650_original.jpg)

**Russian:**

Подобных примеров в Санкт-Петербурге можно найтивеликое множество. Фактически у всех зданий построенных до начала 19 века первые этажи занижены, а уровень пола находится практически насовременном уровне земли или даже ниже её. Этому есть официальное объяснение, которое гласит, что в Санкт-Петербурге было запрещеностроить здания выше, чем Зимний дворец. Поэтому, дескать, хозяева зданий, чтобы максимально использовать площадь земельного участка,начали уходить вниз, создавая функциональные полуподвальные и подвальные помещения. Но это объяснение рассыпается о тот же дом Третьякова,который был построен задолго до постройки Зимнего дворца, да и высота его всё равно ниже. Кроме того, заниженные первые этажи наблюдатся и утех зданий, которые изначально строились заведомо ниже Зимнего дворца, а поэтому там не было необходимости зарываться в землю.

**English:**

There are many such examples in St. Petersburg. In fact, all buildings built before the beginning of the 19th century have lower ground floors and the floor level is almost level with the modern ground level or below it. There is an official explanation for this, which states that in St. Petersburg it was forbidden to build buildings higher than the Winter Palace. Therefore, the owners of the buildings, in order to make the most of the land area, started going down, creating functional semi-basement and basement rooms. But this explanation is dismissed by the Tretyakov house, which was built long before the construction of the Winter Palace, and yet still has a lower height. In addition, the lower ground floors are observed and the comfort of buildings that were originally built deliberately below the Winter Palace, and therefore there was no need to burrow into the ground.

Translator's note: not sure how to deal with that last sentence. Throughout this piece is a risk of 'first floor' being used in its US sense of 'ground floor' and/or in its European sense of 'first floor above ground floor'.

**Russian:**

Не стоит также забывать о такой важной особенности Санкт-Петербурга, какпостоянные наводнения, с которыми его строители и жители познакомились очень давно. Из-за наводнений уровень первого этажа старались поднятьповыше, хотя бы метра на полтора, чтобы в случае небольших наводнений вода оказывалась ниже. Когда город засыпало и уровень земли поднялся, тодома как раз и оказались без фундаментов с уровнем пола первого этажа вровень с новым уровнем земли. Там же, где первый этаж не поднимали, илиземли насыпало чуть больше, уровень пола и входы в здания оказались ниже уровня земли, как у дома Троекурова, а также у многих других зданий натом же Невском проспекте, где при входе в некоторые магазины или кафе вам приходится не подниматься на крыльцо, а спускаться вниз, вполуподвальное помещение, причём иногда совсем немного, на одну две ступени, которые могут быть не только снаружи, но и внутри помещения. Тоесть, порог двери на уровне тротуара, а пол первого этажа ниже где-то на полметра. Это значит, что уже после засыпания сделали реконструкцию,дверь немного приподняли до уровня тротуара, а ступени для спуска на уровень пола перенесли внутрь здания, поскольку так удобнее, особеннозимой.

**English:**

We should also not forget about such an important feature of St. Petersburg, as the constant floods, which its builders and residents became acquainted with a long time ago. Due to floods, the ground floor level was raised by at least one and a half meters, so that in case of small floods the flood water would still be beneath floor level. When the city fell asleep and the ground level rose, the tombs were just without foundations with the formerly raised ground floor level now flush with the new ground level. And if the ground floor level was not raised in response to this - or if the outside ground level rose a little more, the floor level and the entrances to the buildings were now below the ground level, as at the house of Troekurov (Tretyakov house?), as well as many other buildings on the same Nevsky Prospekt. At the entrance to some shops or cafes, you do not have to go up to the porch, but go down into the basement room, sometimes by very little - one or two steps - which may be outside or inside. That is, the threshold of the door is at the level of the sidewalk, and the floor of the ground floor is about half a meter lower. This means that after falling asleep, and being rebuilt, the door was raised slightly to the level of the sidewalk, and the steps to descend to the floor level moved inside the building, because it is more convenient, especially in winter.


**Russian:**

Может быть этот грунт был намыт во время постоянных наводнений? Но когда происходили наводнения и вода шла из Финского залива вверх по Неве,затапливая Санкт-Петербург, воду гнал ветер с моря, а не естественное течение реки. Ветер создаёт поверхностное течение, которое практическине переносит осадочных пород, да и тот ил и песок, который могло принести со дна Финского залива, должен отличаться по составу от тогогрунта, что мы видим в местах раскопок. Подобный грунт если и могло принести, то только потоком с материка.

**English:**

Maybe this soil was washed up during the constant floods? But when there were floods and the water was coming from the Gulf of Finland up the Neva, flooding St. Petersburg, the water was driven by wind from the sea, not the natural flow of the river. The wind creates a surface current that carries practically no sediment. Also, the mud and sand that could have been brought from the bottom of the Gulf of Finland should be different in composition from that soil that we see in the excavation sites. Such soil could only be brought by a flow from the mainland.

**Russian:**

Ещё одним аргументом против того, что до полутора метров грунта былопринесено обычными наводнениями из Финского залива, является то, что засыпание зданий наблюдается не только в Санкт-Петербурге, но и вомногих других городах России, где нет Невы и ветров с Балтики, поэтому подобных регулярных наводнений просто не бывает.

**English:**

Another argument against the fact that up to one and a half meters of soil was brought by ordinary floods from the Gulf of Finland, is that the backfilling of buildings is observed not only in St. Petersburg, but also in many other cities in Russia, where there is no Neva and no Baltic winds, so such regular flooding simply does not happen.

**Russian:**

Засыпанные здания, например, наблюдаются в Ярославле, где я был летом2014 года. Доходный дом, находящийся недалеко от площади Богоявления с памятником Ярославу Мудрому, был построен в конце 18 - начале 19 века.Опять видим окна практически на уровне тротуара и очень низкое крыльцо. 

**English:**

Backfilled buildings, for example, are observed in Yaroslavl, where I was in the summer of 2014. A profitable? house, located near Epiphany Square with the monument to Yaroslav the Wise, was built in the late 18th/early 19th century. Again we see the windows almost at pavement level and a very low porch.

<!-- Local
[![](How Tartaria was Lost Part 5_files/46045_900.jpg#clickable)](How Tartaria was Lost Part 5_files/46045_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/46045/46045_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/46045/46045_original.jpg)

**Russian:**

Это дом на противоположной стороне улицы (на заднем плане площадьБогоявления и памятник Ярославу). Снова окна у тротуара, при этом вход, а значит и уровень пола первого этажа, на полметра ниже.

**English:**

This is a house on the opposite side of the street (in the background, Epiphany Square and the monument to Yaroslav). Again the windows are at sidewalk level, with the entrance, and thus the floor level of the first floor, half a meter lower.

<!-- Local
[![](How Tartaria was Lost Part 5_files/46130_900.jpg#clickable)](How Tartaria was Lost Part 5_files/46130_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/46130/46130_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/46130/46130_original.jpg)

**Russian:**

Это бывший дом вице-губернатора, построенный в 1780-е годы, первый этаж явно занижен.

**English:**

This is a former Lieutenant Governor's house built in the 1780s, the ground floor is clearly submerged.

<!-- Local
[![](How Tartaria was Lost Part 5_files/46461_900.jpg#clickable)](How Tartaria was Lost Part 5_files/46461_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/46461/46461_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/46461/46461_original.jpg)

**Russian:**

А это вид на основной фасад и главный вход, где снова "отсутствует" крыльцо. Видимо вице-губернатор очень не любил ступеньки перед входом.

**English:**

And this is the view of the main facade and the main entrance, where the porch is again "missing". Apparently the Lieutenant Governor really didn't like steps in front of the entrance.

<!-- Local
[![](How Tartaria was Lost Part 5_files/47350_original.jpg#clickable)](How Tartaria was Lost Part 5_files/47350_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/47350/47350_original.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/47350/47350_original.jpg)

**Russian:**

Для сравнения один из домов, который был построен после 1820-го года и уже выглядит нормально. Высокое крыльцо, высокий первый этаж, нормальныйфундамент. И, что интересно, никакого "культурного слоя" со времени строительства почти не накопилось.

**English:**

For comparison, one of the houses that was built after 1820 and which already looks normal. High porch, high ground floor, normal foundation. And, interestingly, almost no "cultural layer" has been accumulated since its construction.

<!-- Local
[![](How Tartaria was Lost Part 5_files/46674_900.jpg#clickable)](How Tartaria was Lost Part 5_files/46674_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/46674/46674_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/46674/46674_original.jpg)

**Russian:**

Далее подборка фотографий из Спасо-Преображенского монастыря, окружённого кремлём, который расположен в самом центреЯрославля и одной стеной выходит на площадь Богоявления.

**English:**

Next, a selection of photos from the Spaso-Preobrazhensky Monastery, surrounded by the Kremlin, which is located in the heart of Yaroslavl and where one wall overlooks the Epiphany Square.

**Russian:**

Два рядом стоящих храма, собор Спаса Преображения (1505-1516) и церковь Ярославских Чудотворцев (1827-1831) (приводится официальная датировка изсправочника, [http://sobory.ru/article/?object=05743](http://sobory.ru/article/?object=05743), которая может отличаться от реальной).

**English:**

Two nearby churches, the Cathedral of the Savior of the Transfiguration (1505-1516) and the Church of Yaroslavl Miracle-workers (1827-1831) (the official date from the reference book is given - [http://sobory.ru/article/?object=05743](http://sobory.ru/article/?object=05743) - which may differ from the real one).

<!-- Local
[![](How Tartaria was Lost Part 5_files/47535_900.jpg#clickable)](How Tartaria was Lost Part 5_files/47535_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/47535/47535_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/47535/47535_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/47700_900.jpg#clickable)](How Tartaria was Lost Part 5_files/47700_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/47700/47700_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/47700/47700_original.jpg)

**Russian:**

Очень хорошо видно, что храмы стоят на разной высоте. При этом в старомхраме уровень пола галереи первого этажа почти на полметра ниже, чем современный уровень земли.

**English:**

It is very clear that the churches stand at different heights. However, in the old church, the floor level of the ground floor gallery is almost half a meter lower than the modern ground level.

<!-- Local
[![](How Tartaria was Lost Part 5_files/48041_900.jpg#clickable)](How Tartaria was Lost Part 5_files/48041_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/48041/48041_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/48041/48041_original.jpg)

**Russian:**

А это вид на "новую" церковь, по которому можно оценить высоту фундамента, на которую подняли церковь. Опасались, что высотагрунта может расти и дальше?

**English:**

And this is a view of the "new" church, from which you can assess the height of the foundation on which the church was raised. Were they afraid that the height of the ground might rise further?

<!-- Local
[![](How Tartaria was Lost Part 5_files/48334_900.jpg#clickable)](How Tartaria was Lost Part 5_files/48334_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/48334/48334_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/48334/48334_original.jpg)

**Russian:**

Церковь воскресения Христова (1520), которая сейчасстоит без куполов и крестов, была откопана во время благоустройства территории монастыря, как и сооружения Петропавловской крепости. Если жесмотреть сбоку, то хорошо видно, что нижние окна находятся на современном уровне земли.

**English:**

The Church of the Resurrection (1520), which now stands without domes and crosses, was dug out during the improvement of the monastery, as well as the construction of the Peter and Paul Fortress. If you look at the side, you can clearly see that the lower windows are at the modern ground level.

<!-- Local
[![](How Tartaria was Lost Part 5_files/48388_900.jpg#clickable)](How Tartaria was Lost Part 5_files/48388_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/48388/48388_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/48388/48388_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/48806_900.jpg#clickable)](How Tartaria was Lost Part 5_files/48806_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/48806/48806_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/48806/48806_original.jpg)

**Russian:**

Звонница с церковью Печёрской иконы Божей Матери тожестоит "без фундамента", крыльцо у входа отсутствует.

**English:**

The bell ringer with the Church of the Pechersk Icon of the Mother of God also stands "without foundation" and there is no porch at the entrance.

<!-- Local
[![](How Tartaria was Lost Part 5_files/48974_900.jpg#clickable)](How Tartaria was Lost Part 5_files/48974_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/48974/48974_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/48974/48974_original.jpg)

**Russian:**

Но это здание на самом деле интересно не только тем, что у него присыпан фундамент и первый этаж. Самое интересное у этого зданиянаходится на верху. На самом деле это чудом сохранившаяся символика настоящего Православного храма язычников солнцепоклонников, о чём ясобираюсь чуть позже написать отдельную статью, а пока фотография "для затравки".

**English:**

But this building is really interesting and not only because it has a foundation and ground floor. The most interesting thing about this building is upstairs. In fact, it is a miraculously preserved symbolism of the real Orthodox Church of the Pagan sun worshipers, about which I will a little later write a separate article. So this photo is "for seed".

<!-- Local
[![](How Tartaria was Lost Part 5_files/49270_900.jpg#clickable)](How Tartaria was Lost Part 5_files/49270_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/49270/49270_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/49270/49270_original.jpg)

**Russian:**

Келейный корпус, который откопали во время последних работ поблагоустройству территории, уложив вдоль самого корпуса асфальт. При этом по следам на стенах, которые ещё не ремонтировали, хорошо видно гдекогда-то был уровень земли. Также интересно, что вход в здание когда-то был на первом этаже, а когда его завалило, то вход сделали через второйэтаж, пристроив деревянное крыльцо, а двери на первом этаже заложили.

**English:**

Glue building, which was dug out during the last works to beautify the territory, laying asphalt beside the building itself. At the same time, the traces on the walls that have not yet been repaired are clearly visible around ground level. It is also interesting that the entrance to the building was once on the ground floor, and when it fell down, the entrance was re-made through the second floor, adding a wooden porch, and the doors on the ground floor were removed.

<!-- Local
[![](How Tartaria was Lost Part 5_files/49486_900.jpg#clickable)](How Tartaria was Lost Part 5_files/49486_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/49486/49486_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/49486/49486_original.jpg)

**Russian:**

Одна заложенная арка от двери видна под лестницей, приэтом хорошо видно насколько ниже должен быть исходный уровень земли. Ещё две арки дальше по зданию также заложены, их видно внизу стены междустоек нижней части деревянного крыльца. При этом ближняя арка точно такая же, как под крыльцом, а дальняя явно выше и шире, больше похожаяна грузовые ворота склада.

**English:**

One laid arch from the door can be seen under the stairs, but you can clearly see how much lower the original ground level should be. Two more arches further down the building are also laid, they can be seen at the bottom of the wall beneath the lower part of the wooden porch. The nearby arch is exactly the same as beneath the porch, and the furthest arch is clearly higher and wider, more similar to the cargo gate of a warehouse.

<!-- Local
[![](How Tartaria was Lost Part 5_files/49842_900.jpg#clickable)](How Tartaria was Lost Part 5_files/49842_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/49842/49842_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/49842/49842_original.jpg)

**Russian:**

Старая Русса, где собираются строить новый курортный корпус, а потому,как это и положено по закону, проводят так называемые "спасательные" археологические раскопки. Полностью с материалами по этим раскопкамможно ознакомится по ссылкам

**English:**

Old Russa, where they are going to build a new spa building, and therefore, as is required by law, they conduct so-called "rescue" archaeological excavations. All the materials on these excavations can be found here:

[http://sibved.livejournal.com/117908.html](http://sibved.livejournal.com/117908.html)


[http://sibved.livejournal.com/135613.html](http://sibved.livejournal.com/135613.html)

**Russian:**

В последней статье внизу есть ссылки на первоисточник информации по Старой Руссе. Тут я приведу только самые интересные фотографии из этихстатей.

**English:**

In the last article below there are links to the source of information on Old Russe. Here I will only present to you the most interesting photos from these articles.

**Russian:**

Общий вид раскопа в Старой Руссе.

**English:**

General view of excavation in Old Russ.

<!-- Local
[![](How Tartaria was Lost Part 5_files/50167_900.jpg#clickable)](How Tartaria was Lost Part 5_files/50167_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/50167/50167_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/50167/50167_original.jpg)

**Russian:**

Съёмка так называемой "стратиграфии" слоёв грунта.Сверху небольшой слой современной почвы, потом более толстый слой глины и суглинка, потом снова вклинивается слой с вкраплениями чернозёма,потом снова слой глинястых почв, потом слой чернозёма толщиной больше полуметра, после которого начинается уже более менее стандартный набор слоёв почвы.

**English:**

Shooting of so called "stratigraphy" of soil layers. Small layer of modern soil, then thicker clay layer and loamy clay layer on top, then black soil layer with inclusions of black soil again, then clay soil layer again, then black soil layer more than half meter thick, after which a less standard set of soil layers begins.

<!-- Local
[![](How Tartaria was Lost Part 5_files/50257_900.jpg#clickable)](How Tartaria was Lost Part 5_files/50257_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/50257/50257_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/50257/50257_original.jpg)

**Russian:**

Единственное объяснение того, как глина и суглинокмогли оказаться сверху слоя чернозёма --- глину насыпали сверху. Под слоём глины чернозём образоваться не мог никак.

**English:**

The only explanation for how clay and loam could have been poured on top of the black soil layer is: clay was poured on top. There was no way black soil could have formed beneath the clay layer.

**Russian:**

А вот следующие две фотографии нам будут важны, когда мы будем делатьвыводы из всего увиденного. Из них следует, что слой грунта был именно насыпан сверху, а не принесён сюда водным потоком, как любят объяснятьсторонники теории смены полюсов и гигантской океанической волны, которая по их мнению прокатилась по нашей территории и оставила массу осадочныхпород. На первой фотографии следы пахоты, которые остались под слоем глины, на второй деревянная мостовая, которая также обнаружилась внизу.

**English:**

But the next two photos will be important to us when we make conclusions from what we've seen. From them it follows that the layer of soil was poured from above and was not brought here by a water flow. The supporters of the pole shift theory and the subsequent giant ocean wave hold the opinion that the tsunami rolled over our territory and left a lot of sedimentary rock. In the first picture, we see traces of arable land that remains under a layer of clay; in the second, a wooden bridge that was also found beneath it.

<!-- Local
[![](How Tartaria was Lost Part 5_files/50571_900.jpg#clickable)](How Tartaria was Lost Part 5_files/50571_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/50571/50571_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/50571/50571_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 5_files/50927_900.jpg#clickable)](How Tartaria was Lost Part 5_files/50927_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/50927/50927_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/50927/50927_original.jpg)

**Russian:**

Две этих находки напрочь отметают происхождение глиняныхслоёв за счёт наводнения. Водный поток должен был размыть и верхний слой чернозема, не оставив следов пахоты, и как минимум оторвать и разбросатьдоски деревянной мостовой.

**English:**

Two of these discoveries offset the theory of the origin of the clay layers due to flooding. The water flow should have also eroded the top layer of black soil, leaving no traces of ploughing, and should have at least torn off and scattered the wooden bridge boards.

<!-- Local
[![](How Tartaria was Lost Part 5_files/51197_900.jpg#clickable)](How Tartaria was Lost Part 5_files/51197_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/51197/51197_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/51197/51197_original.jpg)

**Russian:**

И очень важная находка, свинцовая печать с датой 1820год, которая была найдена при раскопках под слоем глины, что означает, что катаклизм, который привёл к образованию глиняных наносов, должен былпроизойти после 1820 года!

**English:**

And a very important find, a lead seal with the date 1820, which was found during excavations under a layer of clay, which means that the cataclysm that led to the formation of clay sediment must have occurred after 1820!

© All rights reserved. The original author retains ownership and rights.
