title: Global cataclysm, memory-erasure and branch-merging 150 years ago... Where do people come from?
date: 2020-01-31
modified: 2022-04-09 20:43:08
category:
tags: catastrophe; forged history
slug:
authors: digitall-angell
from: https://digitall-angell.livejournal.com/1004206.html
originally: Global cataclysm, memory mashing and branch merging 150 years ago - Where do people come from.html
local: Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/
summary: Versions and cause-and-effect relationships between events sometimes contradict each other so much that one cannot help wondering if we all live in the same reality.
status: published

#### Translated from:

[https://digitall-angell.livejournal.com/1004206.html](https://digitall-angell.livejournal.com/1004206.html)

**Russian:**

Уже давно на этом и и многих других ресурсах идут разговоры о том, что официальная история не имеет ничего общего с реальными событиями даже 100-200 летней давности, не говоря о тысячах лет, слишком много явных нестыковок, манипуляций и перетягивания сов на глобус победителями.

**English:**

There has long been talk on this and many other resources that the official history has nothing to do with the real events of even 100-200 years ago, not to mention thousands of years ago. Too many obvious inconsistencies, manipulation and inept fact-fudging by the 'winners' who write history.

**Russian:**

И это уже давно не версия, а совершенно ясный факт, ярым противникам которого достаточно будет посмотреть на сегодняшнее медийное пространство, в котором одинаковые события в реальном времени освещаются и интерпретируются совершенно по-разному на разных каналах, разными партиями и в разных государствах.

**English:**

And this is no longer theory, but a very clear fact, whose ardent opponents would only need to look at the current media space, in which the same events are covered and interpreted completely differently in real time on different channels, by different parties and in different states.

**Russian:**

Версии и причинно-следственные связи событий иногда настолько противоречат друг другу, что невольно закрадывается мысль - а в одной ли на всех реальности мы живем?

**English:**

Versions and cause-and-effect relationships between events sometimes contradict each other so much that one cannot help wondering if we all live in the same reality.

**Russian:**

Если в одной, то нам явно лгут о многих её явлениях и событиях.

**English:**

If we all live in one reality, we are clearly being lied to about many of its phenomena and events.

**Russian:**

Если в разных, то... на этом моменте у "нормального" человека возникает слишком много вопросов, поэтому не будем забегать вперед.**

**English:**

If we live in different realities, then... at this point a "normal" person has too many questions, so let's not get ahead of ourselves.

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4865262_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4865262/4865262_1000.jpg#resized)

**Russian:**

Для начала предлагаю вашему вниманию серию клипов о задокументированных странностях не столь давней эпохи.

**English:**

For starters, here's a series of clips of documented oddities from a not-so-distant era.

**Russian:**

Два коротких ролика о событиях в США 19-го и начала 20-го века:

**English:**

Two short clips about events in the 19th and early 20th century USA:

Translator's note: if there were video clips at this point in the article, they are no longer available. However 'clips' may refer to the examples that follow.

**Russian:**

По всей стране загадочным образом сгорают почти все крупные города, но их относят к бытовым пожарам. Например [Сиэтл пожар **1889**](https://digitall-angell.livejournal.com/973473.html):

**English:**

Across the country, almost all major cities are mysteriously burning down, but they are being categorized as domestic fires. For example [Seattle fire **1889**](https://digitall-angell.livejournal.com/973473.html):

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4762990_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4762990/4762990_1000.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4757991_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4757991/4757991_1000.jpg#resized)

**Russian:**

При этом здания восстанавливаются в темпах, которым позавидует любой профессиональный строить сегодняшних дней.

**English:**

At the same time, the buildings are being restored at a pace that would be the envy of any professional builder of today.

**Russian:**

Например, в Сиэтле за 18 месяцев было построено 5, 525 зданий. Это не считая, разобранных завалов и такой мелочи, как 40 с чем-то миль железнодорожных путей. Легкий подсчет в уме, дает нам 10 зданий в сутки, без сна и отдыха. И знаете, что? Тогда еще не было панельного строительства и строили они не бараки для погорельцев.

**English:**

In Seattle, for example, 5,525 buildings were built in 18 months. That's not counting debris removal and little things like 40-something miles of railroad tracks. Easy mental arithmetic tell us that is ten buildings a day, no sleep or rest. And you know what? They didn't have wood panel construction back then, and they weren't building mass barracks for the fire victims either.

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/51%252017.jpg#resized)
-->

![](https://cont.ws/uploads/pic/2019/2/51%20%2817%29.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/52%252014.jpg#resized)
-->

![](https://cont.ws/uploads/pic/2019/2/52%20%2814%29.jpg#resized)

**Russian:**

На фотографиях видны люди, поголовно одетые в черное, осматривающие тлеющие руины не как свои бывшие дома - с горем и отчаянием в глазах, - а как туристы, глазеющие на невиданную диковинку или чью-то чужую беду, но уж точно не остатки их родного города. При этом среди них не видно детей и стариков, все люди зрелого и рабочего возраста.

**English:**

The photographs show people, all dressed in black, examining the smouldering ruins not as their former homes - with grief and despair in their eyes - but as tourists gazing at an unseen curiosity or someone else's misfortuner. Certainly not the remains of their home town. At the same time, among them, there are no children and elderly people. They are all working age adults.

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4864388_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4864388/4864388_1000.jpg#resized)

**Russian:**

Балтимора пожар 1904

**English:**

Baltimore Fire 1904

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4863628_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4863628/4863628_1000.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/48%252024.jpg#resized)
-->

![](https://cont.ws/uploads/pic/2019/2/48%20%2824%29.jpg#resized)

**Russian:**

Сан-Франциско пожар 1906 года

**English:**

San Francisco fire of 1906

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4863940_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4863940/4863940_1000.jpg#resized)

**Russian:**

Чикаго пожар 1871 года

**English:**

Chicago fire of 1871

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/5%2520371.jpg#resized)
-->

![](https://cont.ws/uploads/pic/2019/2/5%20%28371%29.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4864708_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/digitall_angell/38000872/4864708/4864708_1000.jpg#resized)

**Russian:**

Великий Бостонский пожар 1872

**English:**

The Great Boston Fire of 1872

**Russian:**

Великие пожары 19 века (кликабельно):

**English:**

The great fires of the 19th century (images are clickable):

<!-- Local
[![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4864889_1000.jpg#clickable)](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/4864889_original.jpg)
-->

[![](https://ic.pics.livejournal.com/digitall_angell/38000872/4864889/4864889_1000.jpg#clickable)](https://ic.pics.livejournal.com/digitall_angell/38000872/4864889/4864889_original.jpg)

Auto-translation:

- 1830: Great Fire of Mobile, Alabama. 4 separate fires on September 29 - October 29 from Athens to Bankruptcy
- 1840: Great Fire of Louisville, Kentucky
- 1842: Great Fire of Tronkaim, (additional fires of 1841, 1842 + 3 more)
- 1842: Great Fire of Hamburg, Germany
- 1843: Great Fire of Kingston, Jamaica
- 1843: The Great Fire of Tallahassee, Florida
- 1843: Great Fall River Fire, Massachusetts
- 1844: The Great Fire of Boston, Massachusetts
- 1844: Great Fire of Gravesend, United Kingdom
- 1845: Great Fire of Bridgeport,
- 1845: Great Fire of New York, New York
- 1845: Great Sag Harbor Fire, New York (possibly also in 1815)
- 1843: Great Fire of Pittsburgh, Pennsylvania Destroyed
- 1843: La Playa (de Ponce, port city of Ponce, Puerto Rico), most of the color
- 1846: The Great Fire of St. John's, Canada
- 1846: Great Fire of Nantucket, Massachusetts
- 1847: Great Fire in Bucharest Romania
- 1848: Great Fire of Albany, New York
- 1848: Fire of Medina, Ohio, all rabo
- 1849: Great Saint Louis Fire, MO
- 1849: First great fire in Toronto
- 1850: the first great fire in the
- 1850: Wildfire in Frederickton
- 1850: The Great Fire of San Francisco
- 1851: The Great Fire of San Francisco
- 1852: The Great Fire of Montreal, California
- 1853: great fire in Oswego
- 1854: big fire
- 1855: Great fire on Bankside, London
- 1856: City of Philadelphia
- 1857: the first great fire in the
- 1858: the great fire of Christiania (Translator's note: Unclear if this is Copenhagen or elsewhere)
- 1861: Charlton Fire, Carolina
- 1862: Troy, New York, 671 buildings destroyed
- 1863: Great Fire of Denver, CO
., 4
- 1864: Great Fire of Brisbane. Cain- Australia: more than 50 homes were destroyed in the tech for four blocks of aging
- 1864: Atlanta, Georgia, burned after the time allotted for the evacuation of citizens by order of Tecumseh Sherman
- 1865: fire of the city of Richmond Virginia, by retreating Confederates
- 1865: Columbia, South Carolina, burned down while occupied by troops under the command of William Tecumseh Sherman
- 1864: Atlanta, Georgia, burned after the time allotted for the evacuation of citizens by order of William Tecumseh Sherman
- 1865: fire protection of the city of Richmond, Virginia, is burned by the confederates otstupnoye
- 1865: Columbia, South Carolina, was burned during occupied by troops under the command of William Tecumseh Sherman
- 1866: Great Fire of Portland, Maine
- 1858: Auerbach in der Oberpfalz, Bavaria. As a result of arson, 107 houses and 146 other buildings were destroyed, 4 were killed
- 1870: The Great Fire of Constantinople, Turkey
- 1870: Fire in Medina, Ohio, consumes everything but two blocks from the business district, nearly destroys the entire city
- 1871: Great Fire in Chicago, Illinois
- 1871: Great Fire Case In Peshtigo, Wisconsin
- 1871: The Great Urbana Fire, oh
- 1872: The Great Boston Fire of 1872, destroyed 776 buildings and killed at least 20 people.
- 1873: The Great Fire of Portland, or
- 1874: fire in the city of Chicago, destroyed 612 buildings and killed 20 people.
- 1877: fire in St. John, New Brunswick, destroys 1,600 buildings
- 1878: The Great Fire of Hong Kong, destroyed 350 to 400 buildings in more than 10 acres of central Hong Kong
- 1879: Hakodate fire, Hakodate, Hokkaido, Japan, 67 people, 20,000 homeless
- 1881: Thumb fire in Michigan
- 1881: Ringtheater Fire in Vienna, Austria
- 1886: Great Vancouver Fire, Vancouver, British Columbia
- 1807: Cannon Falls Fires,
- 1889: Great Fire in Spokane, Washington
- 1889: Great Bakersfield Fire - destroyed 196 homes and killed 1 person
- 1889: Seattle, Washington Fire
- 1889: The First Great Lynn Fire, Lynn, Massachusetts destroyed about 100 buildings
- 1891: The Great Fire of Syracuse
- 1892: The Great Fire of St. John's, Newfoundland, CA
- 1893: fire in Clarksville, Virginia.
- 1894: Great Hinckley
- 1894: Great Fire in Shanghai, destroyed more than 1,000 buildings
- 1896: Great Fire in Paris, Texas, destroyed most of the city
- 1897: Great Fire of Windsor, Nova Scotia, Canada, destroyed in 80% of the cities
- 1898: The Great Fire of New Westminster, British Columbia
- 1898: Great Fire in Park City, Utah
- 1906: Great SF Fire and earthquake
- 1914: Great Salem Fire, Ma - 1,376 buildings
- 1918: Cloquet Duluth and noe Lake

**Russian:**

Примерно в это же время по всей стране и многим городам Европы и [России](https://digitall-angell.livejournal.com/1003509.html) проводятся "[всемирные выставки](https://digitall-angell.livejournal.com/1003967.html)", на которые, как заявлено, съезжаются миллионы людей. Фотографии с этих выставок также практически не показывают детей и стариков, а количество самих посетителей близится к половине населению всей страны. И это в то время, как страна официально не имеет развернутой транспортной системы и находится в кризисе.

**English:**

At roughly the same time, [World Exhibitions](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/1003967.html) were being held across the country and in many cities in Europe and [Russia](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/1003509.html), which are claimed to have attracted millions of people. Also, photographs from these exhibitions show almost no children and elderly, yet the number of visitors is close to half the population of the entire country. And this at a time when the country officially has no deployed transportation system and is in crisis.

**Russian:**

Заявлено 27 МИЛЛИОНОВ посетителей за 1 год, в то время как в США проживает около 50 миллионов. Из Европы столько приехать не могло, тк транспортные компании обеспечивают не более 500 тысяч перевозок в год. Создается впечатление, что люди прибыли в новую для себя реальность и осваиваются в её декорациях, идет ознакомление с миром, который совсем недавно принадлежал другим.

**English:**

27 **MILLION** visitors in 1 year are declared, while there were about 50 million people in the USA. So many people could not have come from Europe, because transport companies provided no more than 500 thousand transports per year. It seems that people had arrived in a new reality and were getting used to its scenery, getting acquainted with the world, which quite recently belonged to others.

**Russian:**

Параллельно с этим в США возникает партия ["ничего не знаю" ("know nothing")](https://ru.wikipedia.org/wiki/Know_Nothing), название которой официально говорит о секретности её членов ("ничего не знаю" - их ответ на любой вопрос о партии). Партия приравнена к террористическому движению, тк яростно противостоит распространению религий, вплоть до поджогов церквей. Между тем, автор выдвигает версию, что название говорит о другом - в эту партию входили люди, которые не знали, КТО ОНИ, и требовали о управляющих это рассказать. Люди, потерявшие память, на обломках былой цивилизации, сожженной силами глобальных масштабов.**

**English:**

At the same time, a ['know nothing'](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.wikipedia.org/wiki/Know_Nothing) party emerges in the USA, whose name officially indicates the secrecy of its members ("I know nothing" being their answer to any question about the party). This party is equated with a terrorist movement, since it violently opposes the propagation of religion, up to and including the burning of churches. Meanwhile, the author puts forward a version that the name says something else: the party consisted of people who did not know WHO THEY ARE, and demanded to be told about them. People who had lost their memory, on the wreckage of the former civilization, burned by the forces of global proportions.


[![](https://img.youtube.com/vi/FddETR9dTms/0.jpg)](https://youtu.be/FddETR9dTms)

[caption] [Subtitles files for different languages](https://downsub.com/?url=https%3A%2F%2Fyoutu.be%2FFddETR9dTms)

[![](https://img.youtube.com/vi/pizITR5ddmo/0.jpg)](https://youtu.be/pizITR5ddmo)

[caption] [Subtitles files for many languages](https://downsub.com/?url=https%3A%2F%2Fyoutu.be%2FpizITR5ddmo)

**Russian:**

Клип о Франции 19-го века и диких племенах, появляющихся из ниоткуда на территории одной из сверхдержав того времени, якобы захватившей полмира, но неспособной навести порядок в своих собственных границах. О странностях кампаний Наполеона и захвата Парижа армией Бисмарка.

**English:**

A clip about 19th century France and the savage tribes appearing out of nowhere in the territory of one of the superpowers of the time, supposedly conquering half the world, but unable to bring order to its own borders. About the strangeness of Napoleon's campaigns and the capture of Paris by Bismarck's army.

[![](https://img.youtube.com/vi/qci14ZWWHDs/0.jpg)](https://youtu.be/qci14ZWWHDs)

[caption] No subtitles currently available

**Russian:**

Перезагрузка 19 века. Франция дикие племена

**English:**

19th century reboot. France wild tribes

**Russian:**

Небольшой бонус: Наградные медали за две войны

С Наполеоном 1812-1814 гг.** **и Турецкая 1829 г.

Почему одежда у русичей как у римских легионеров? Кто там реально сражался и кто кого победил?

**English:**

A little bonus: two war medals

With Napoleon 1812-1814. And the Turkish 1829.

Why do Russians have clothes like Roman legionaries? Who really fought there and who defeated whom?

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-7.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-7.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-8.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-8.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-9.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-9.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-10.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-10.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-11.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-11.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-13.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-13.jpg#resized)

<!-- local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-1.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-1.jpg#resized)

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-3.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-3.jpg#resized)

**Russian:**

**Неудобные артефакты войны и медали 1812 года**

**English:**

*From: [Inconvenient artifacts of the war and medals of 1812](https://metaisskra.com/tag/1812/)*

<!-- Local
![](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-4.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-4.jpg#resized)

**Russian:**

**Новые маски на старых лицах войны 1812 года. Медали 1812 и 1829 годов**

**English:**

*From: [New masks on the old faces of the War of 1812. Medals of 1812 and 1829](https://metaisskra.com/tag/1812/)*

<!-- Local
![Неудобные артефакты войны и медали 1812 года](Global cataclysm memory mashing and branch merging 150 years ago - Where do people come from_files/medali-1812-5.jpg#resized)
-->

![](https://metaisskra.com/wp-content/uploads/2014/02/medali-1812-5.jpg#resized)

**Russian:**

**Неудобные артефакты войны и медали 1812 года**

**English:**

From: [Inconvenient artifacts of the war and medals of 1812](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/tag/1812/)

**Russian:**

Думаю, постоянные читатели блога догадались, куда я веду с этими клипами.

**English:**

I think regular blog readers have guessed where I'm going with these clips.

**Russian:**

Подтверждают ли они версию о крупном слиянии многих веток реальности с параллельной зачисткой, [сменой полюсов](https://www.youtube.com/watch?v=VGNl99rIYS8&t=1s) и, как следствие, [затиранием памяти](https://www.youtube.com/watch?v=AREA29_1vdo&) и смены декораций голограммы каких-то 100-200 лет* назад?

**English:**

Do they support the version of a major merger of many branches of reality with a parallel sweep, [pole shift](https://www.youtube.com/watch?v=VGNl99rIYS8&t=1s) and resulting [memory mashing](https://www.youtube.com/watch?v=AREA29_1vdo&) and hologram set-change some 100-200 years ago?

**Russian:**

речь о [нашей] временной шкале, тк на разных ветках были разные события, фрактально распространяющиеся по спирали времени

**English:**

Speak of [our] timeline, as different branches had different events fractally propagating along the time spiral.

**Russian:**

Подтверждают ли они, что [война 1812](https://metaisskra.com/tag/1812/) года была лишь [ширмой](https://metaisskra.com/kniga-pamyati/glava-25-kak-strazhi-moskvy-morochili-golovu-napoleonu/) для событий совершенно иного порядка, нежели нам рассказывают историки?

**English:**

Do they confirm that the [War of 1812](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/tag/1812/) was only a [screen](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/kniga-pamyati/glava-25-kak-strazhi-moskvy-morochili-golovu-napoleonu/) for events of an entirely different order from what historians tell us?

**Russian:**

И продолжаются ли эти события по сей день...?

**English:**

And do these events continue to this day...?

**Russian:**

Если вы не знакомы с выдвинутой версией слияния веток  квантовой реальности и её прямыми и косвенными подтверждениями, читаем по теме:

**English:**

If you are not familiar with the version of quantum reality branch fusion put forward - and its direct and indirect confirmations, read on the topic:

**Russian:**

- [Квантовый мир. Слияние и разделение веток реальности](https://metaisskra.com/kniga-pamyati/ch-12-kvantovyj-mir-sliyanie-i-razdelenie-vetok-realnosti-soderzhanie/)

- [Первая мировая война: откуда оружие и технологии?](https://metaisskra.com/blog/pmv-tehnologii)

- [Загадки блокады Ленинграда](https://metaisskra.com/blog/zagadki-blokady-leningrada/)

- [1914г. Вторая отечественная война и глобальная цивилизация](https://metaisskra.com/blog/1914g-vtoraya-otechestvennaya-vojna-i-globalnaya-civilizaciya/)

- [Отголоски глобальной цивилизации](https://metaisskra.com/blog/otgoloski-globalnoj-civilizacii/)

- [Кто строил мегалиты Питера по официальной версии? Маразм и ложь историков](https://metaisskra.com/blog/kto-stroil-megality-pitera-po-oficialnoj-versii-marazm-i-lozh-istorikov/)

- [Смерть исторической науки. Ложь историков](https://metaisskra.com/blog/smert-istoricheskoj-nauki/)

- [Неудобные артефакты войны и медали 1812 года](https://metaisskra.com/blog/neudobnye-artefakty-vojny-1812-goda/)

- [Заброшенные города предков. Слияние и разделение веток реальности 300 лет назад](https://metaisskra.com/blog/6522/)

- [Оборонные технологии храмовых комплексов и зачистка цивилизации 300 лет назад](https://metaisskra.com/blog/oboronnye-tehnologii/)

- [Война миров 1812г. Ляпы официальной истории](https://metaisskra.com/blog/vojna-mirov-1812g-lyapy-oficialnoj-istorii/)

- [Индустриально развитая цивилизация существует на Земле десятки тысяч лет](https://metaisskra.com/blog/industrialno-razvitaya-civilizaciya/)

- [Оплавленные мегалиты древних цивилизаций, Земля рудник](https://metaisskra.com/tag/oplavlennye-megality/)

- [Всемирные промышленные выставки. Технологии прежней цивилизации](https://digitall-angell.livejournal.com/1003967.html)

- [Виртуальная реальность. Как понять, что ты уже в ней?](https://metaisskra.com/blog/12088/)

**English:**

- [Quantum World. Merging and separating branches of reality](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/kniga-pamyati/ch-12-kvantovyj-mir-sliyanie-i-razdelenie-vetok-realnosti-soderzhanie/)

- [World War I: Where do weapons and technology come from?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/pmv-tehnologii)

- [Mysteries of the Leningrad blockade](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/zagadki-blokady-leningrada/)

- [1914: World War II and global civilization](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/1914g-vtoraya-otechestvennaya-vojna-i-globalnaya-civilizaciya/)

- [Echoes of Global Civilization](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/otgoloski-globalnoj-civilizacii/)

- [Who built the megaliths of Peter according to the official version? Short-sightedness and lies of historians](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/kto-stroil-megality-pitera-po-oficialnoj-versii-marazm-i-lozh-istorikov/)

- [The death of historical science. Lies of historians](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/smert-istoricheskoj-nauki/)

- [Inconvenient war artifacts and medals of 1812](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/neudobnye-artefakty-vojny-1812-goda/)

- [Abandoned Ancestral Cities. Merging and splitting branches of reality 300 years ago](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/6522/)

- [Defence technology of temple complexes and the sweep of civilisation 300 years ago](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/oboronnye-tehnologii/)

- [War of the Worlds 1812. Official history bloopers](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/vojna-mirov-1812g-lyapy-oficialnoj-istorii/)

- [Industrialized civilization has existed on Earth for tens of thousands of years](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/industrialno-razvitaya-civilizaciya/)

- [Melted megaliths of ancient civilizations, quarried Earth](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/tag/oplavlennye-megality/)

- [World Industrial Exhibitions. Technologies of the former civilization](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/1003967.html)

- [Virtual reality. How do you know if you are already in it?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/12088/)

**Russian:**

Реальность многомерна, взгляды на неё многогранны. Здесь показана лишь одна или несколько граней, каждая из которых должна рассматриваться как частный случай. Частный случай подразумевает также и частное мнение, которое не обязано совпадать с другими мнениями, ожиданиями и "прописными истинами", ибо истина безгранична, а реальность постоянно меняется. Берем свое и оставляем чужое по принципу внутреннего резонанса

**English:**

Reality is multidimensional, views on it are multifaceted. Here only one or several facets are shown, each of which should be treated as a special case. A particular case also implies a private opinion, which does not have to coincide with other opinions, expectations and "commonplace truths", because the truth is boundless and reality is constantly changing. Taking one's own and leaving another's according to the principle of internal resonance

© All rights reserved. The original author retains ownership and rights.

