title: Inspired by "And there was a Country before Russia". Part 5
date: 2013-10-17
modified: Fri 02 Apr 2021 20:05:34 BST
category: 
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: At the base, we have the barrel from the Tsar cannon. It is schematically shown how it radiates something in the form of diverging circles originating at the "base" of the bowl, in our case, at the "crown" of the "Tsar-bell".
status: published
originally: По мотивам «И была Страна до России». Часть 5.html

### Translated from:
[https://pro-vladimir.livejournal.com/1813.html](https://pro-vladimir.livejournal.com/1813.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html)

And they dug a net in the collection of grains that lay in a fat layer in the porridge of ordinary people and their self-soothing points of view. There is a sort of "ordinary man in the street" person who has a simple answer for everything.  They have "Money" = "stolen", and everything that is not according to Wikipedia must be due to the lack of education of the doubters.

What? What are these resonating springs? This is the most common crown of an Orthodox bell. They are not even afraid to send to read the network, see smart pictures there. Of course, the spring is one-in-one executed as a rigid link. How can there be any doubts? So what if it's curved? Isn't it similar? No? Well, anyone who wants to will find the difference. Those who understand everything may not read our fabrications at all. And we will continue a little bit.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/budva-10.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/budva-10.jpg)
-->

[![](https://lh4.googleusercontent.com/-aTsNc7Q_iT0/UCKYgX-JrEI/AAAAAAAASCI/mghZvwLxuME/s838/budva-10.jpg#clickable)](https://lh4.googleusercontent.com/-aTsNc7Q_iT0/UCKYgX-JrEI/AAAAAAAASCI/mghZvwLxuME/s838/budva-10.jpg)

Of course, in order to hang it, it was necessary to "disfigure" the eye in this way.

It seems to me that the current methods of making bell crowns are a simple transformation of the original version. Bell makers have long forgotten what was there originally and why it was there. They do the same thing nowadays, but it is to hang the bell. But what was the original "mount" that held the bell up?

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/budva-10.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/budva-10.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-3.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-3.jpg)

We see again an image depicting the item known as the 'Grail'.

At the foot, we have a barrel from the Tsar cannon. It is schematically shown how it radiates something in the form of diverging circles originating at the "base" of the bowl, in our case at the "crown" of the "Tsar-bell". In the upper part of the Grail, there is a circular swirl, which also gives some radiation, but it already diverges in intermittent uneven circles.

What else do we have left of these artifacts? The 'ball of power' that now decorates the top of the Tsar bell. As it is there, seen hanging and is currently suspended in a sling.

Continue. What's missing? In the form that we put together the components available to us, using a 'shporgalka' with a working image of the Grail, this is still a set of large-sized pieces of iron. Difficult to execute. Presumably, in the working position, it should emit something. By what means? What gave the system a boost? Yes, and how? Today's bell ringers swing huge clappers with ropes to delight the ears of believers. But back then?

In ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html), the use of electricity is mentioned. Static electricity. But then there must be wires that put it all together as a circuit and allowed it to function? Where are they?

On the territory of the Kremlin, they don't seem to be there. But if you look around a little...

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/13.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/13.jpg)
-->

[![](http://centant.spbu.ru/centrum/publik/books/kul/13.jpg#clickable)](http://centant.spbu.ru/centrum/publik/books/kul/13.jpg)

*"Golden tripod mounted on a bronze column in the form of a three-headed snake"*

Dense people. All they see are snakes twining around.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/98286458_Kaducey.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/98286458_Kaducey.jpg)
-->

[![](http://img0.liveinternet.ru/images/attach/c/7/98/286/98286458_Kaducey.jpg#clickable)](http://img0.liveinternet.ru/images/attach/c/7/98/286/98286458_Kaducey.jpg)

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/731576_original.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/731576_original.jpg)
-->

![](http://ic.pics.livejournal.com/galeneastro/32190196/731576/731576_original.jpg#resized)

Perhaps the caduceus and the symbol that medics use are the same image? Part of a Grail system with a different number of wire windings? Where is one used, where are two used, and where are three used?

What about the insulation? There's a hint that silk was used for insulation. Everything was wrapped in silk. And wires wrapped in silk were often mistaken for a snake. Although the snake is more a symbol of defeat. Although defeat of whom and with what medicinal poison? Considered in a different way, you can see it was possible for this snake to "bite".

The symbolism of wires is available in abundance.

Time and vision redrawing artists have left their mark. The same caduceus, in many images, has become somewhat distorted. Usually there is a staff. But in some there is not and these will be shown ending with bends like that of a bell. And perhaps the wings that are also shown in Grail imagery are stylised radiation?

What else is there about electricity? Oh, yes, a certain weapon of archers: the halberd or a Berdysh, or an axe.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 5._files/888.jpg#clickable)](По мотивам «И была Страна до России». Часть 5._files/888.jpg)
-->

[![](http://spiculo.ru/wp-content/uploads/2013/07/888.jpg#clickable)](http://spiculo.ru/wp-content/uploads/2013/07/888.jpg)

Just imagine, you are an army. They look, and see the weapons that are designed to cut them, put up in this way, and the enemy is supposed to see this with horror and flee?

But if you imagine that instead of ropes, these are electrified wires with considerable charge on them? Then it turns out that the barrier can electrocute. And the number of simultaneously stretched wires only provides some strength and capacity for this electric fence.

You can certainly try to come up with a different uses for these rings. Try to dream up why these holes are needed there, why try to rest on this element if it does not have any practical use. And not in a single product, but massively for the entire army of that time.

<!-- Local
![](По мотивам «И была Страна до России». Часть 5._files/1342808317_bochka.jpg#resized)
-->

[![](http://cyrillitsa.ru/uploads/posts/2012-07/1342808317_bochka.jpg#clickable)](http://cyrillitsa.ru/uploads/posts/2012-07/1342808317_bochka.jpg)

What was transported in carts? A galvanic cell? 

Vladimir Mamzerim 17.10.2013 g.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-6.html)

© All rights reserved. The original author has asserted their ownership and rights.

Comments:

A galvanic cell is unlikely, I think. But the barrier of Berdysh amused. This is exactly came up with the academician of the room, which is anything heavier than a pencil in his hand never kept, never been in a fight and especially in the army are not served. A clumsy attempt to explain why you need rings and holes on cleavers. Wires aren't an explanation either. They are only needed by parasitic electricity suppliers. If electricity was used, and I have little doubt about it, it was completely different, without generators and power lines. The way Tesla tried to implement it.


dmitrijan
2013-10-18 08:22 am (UTC)
Naturally, electric fences are not used now, as well as live wire. Archaism ...


Kundalini
Alexey En In
2013-12-04 12:02 pm (UTC)
It is very similar to the schematic image of the Kundalini rising. Why do you think about wires and electricity, there are no other energies?

Re: Kundalini
dmitrijan
2013-12-04 12: 32 pm (UTC)
What would they not be, is.

about fabrications
Ilya Shapiro
2013-10-17 10:27 am (UTC)
thank you for the smart images ))) read look think)))


man_oleg
2013-10-23 05:13 am (UTC)
Here you go!
Gorgeous images.
The head of the serpent - it clamps! crocodile type, for quick cable connection.
And the barrier is a typical mobile overhead power line.
We use the same solution as part of a high-voltage machine, only we have ordinary sticks.


dmitrijan
2013-10-23 09:29 am (UTC)
This "stick" used to be called Vseles-rtsy-Stolb. Ve-R-St

