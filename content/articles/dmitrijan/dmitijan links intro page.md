title: dmitrijan's Livejournal intro and links
date: 2012-12-21
modified: Thu 14 Jan 2021 07:23:06 GMT
category: 
tags: links
slug: 
authors: Dmitry Anatolyevich
summary: Specific esotericism without falling into the astral and meditations
status: published
originally: Едины во всём - Едины всегда. — LiveJournal.html

### Translated from:

[https://dmitrijan.livejournal.com/](https://dmitrijan.livejournal.com/) 

In order not to make it difficult to read, we offer a set of typed and ideologically verified answers to these texts by different readers:

> 1. "Many books" - the author is engaged in letter-writing to satisfy his own ambitions and desires of worthlessness. It is necessary to write shorter and easier, and it is better not to write at all.

> 2. "Complete stupidity!" - the author is not familiar with true masterpieces and does not quote from them: "Bible", "Koran", "history of the CPSU", "Capital", "Vedas", "What to do?" and other great works. And you can immediately see that the author is engaged in admiring himself, because he does not admire such works in every line. No mention of you-know-who, either! And there is no delightful reference to his work.

> 3. "Complete nonsense!" - the author contradicts modern scientific, technical, world, religious, alternative knowledge! The author does not understand trends and clearly did not study well! He didn't read the smartest textbook of so-and-so. And without it, no one has ever been able to write something good.

> 4. "Heresy!" - the Author does not give figures, graphs, Tries to shake the foundations of the world!

> 5. "the Devil!" - When reading, the thoughts of readers suck energy, you feel empty, there is clearly something in them.

> 6. "Lazy to read".

> 7. "There is something in this"-the author could have written easier, in a couple of words to state everything, Letterman, you know.

> 8. "I haven't read it, but I condemn it"

And also:

Publication of books [The Impossible is Possible](http://dmitrijan.livejournal.com/10325.html) and others on:

- [RussoLit](http://russolit.ru/profile/DmitrijAN/created/books/)

- [BooksMarket](http://booksmarket.org/search.html?q=Dmitrij)

- [SamLib](http://samlib.ru/editors/d/dmitrij_an/pip_000_20111207_newozmozhnoewozmozhno_ch1.shtml)

To [download published books](http://yadi.sk/d/-sBvllhhERDLY) in various electronic formats.

Articles on: 

- website [Proza](http://www.proza.ru/avtor/dmitrijan)

- on the website of the magazine [Samizdat (SamLib)](http://samlib.ru/editors/d/dmitrij_an). 

- All sorts of [different poems](http://www.stihi.ru/avtor/dmitrijan), 

- forum on the [site Pi9](http://pi9.3dn.ru/forum).

Articles: 

- [from 2009](http://yadi.sk/d/KZ7h_y7-BTFBH)

- [from 2010](http://yadi.sk/d/H7RLHAReBTGJ3)

- [from 2011](http://yadi.sk/d/bbySV0HtBTHXP)

- [from 2012-14](http://yadi.sk/d/fIZIY4Z8FFcZj)

- [PoM](https://yadi.sk/d/16SWr76HFHv8Y). As well [as drawings for texts](http://yadi.sk/d/SCRqDrqUFFc3b) in different resolutions.

The book *The Impossible is Possible*:

- [Part 1](http://dmitrijan.livejournal.com/10325.html)

- [Part 2](http://dmitrijan.livejournal.com/10516.html)

- [Part 3 (Chapter 4)](http://dmitrijan.livejournal.com/11326.html)

- [Part 3 (Chapter 5)](http://dmitrijan.livejournal.com/11633.html)

- [Part 4](http://dmitrijan.livejournal.com/12839.html)

- [Part 5 (Chapter 7 H1)](http://dmitrijan.livejournal.com/13453.html)

- [Part 5 (Chapter 7 H2)](http://dmitrijan.livejournal.com/13805.html)

- [Part 5 (Chapter 8)](http://dmitrijan.livejournal.com/168930.html)

- [Part 6 (Chapter 9-10)](http://dmitrijan.livejournal.com/204993.html)

- [Part 6 (Chapter 11)](http://dmitrijan.livejournal.com/205641.html)

- [Part 6 (Chapter 12-13)](http://dmitrijan.livejournal.com/206367.html), "Part 7", "Part 8", "Part 9", "Part 10", "Part 11", "part 12"

The book *SN (SN), Pilot's Notes. Contact symbol – Open Door*: [Chapter 33](http://tipa-dream.livejournal.com/37471.html)

Publication of the book [*Pilot's Notes, Contact Symbol - Open Door*](http://tipa-dream.livejournal.com/?skip=30&tag=%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D0%BA%D0%B8%20%D0%9F%D0%B8%D0%BB%D0%BE%D1%82%D0%B0) on: [RussoLit](http://russolit.ru/books/item/zapiski-pilota_-simvol-kontakta-otkrytaja-dver.html)

- [BooksMarket](http://booksmarket.org/book/Sono-May_Zapiski-pilota-Simvol-kontakta---otkrytaya-Dver.html)

- [Bookscriptor](https://bookscriptor.ru/books/zapiski-pilota-simvol-kontakta-otkrytaya-dver-23/)

- [Lulu](http://www.lulu.com/shop/sono-may/zapiski-pilota-simvol-kontakta-otkrytaya-dver/paperback/product-22788945.html).


ViVa:

- [Oh, gold and gold](http://dmitrijan.livejournal.com/139780.html)

- [And the world was split in half](http://dmitrijan.livejournal.com/140680.html)

- [Oh, Mother Russia](http://dmitrijan.livejournal.com/145857.html)

- [What is the power? Is it money?](http://dmitrijan.livejournal.com/146577.html)

- [Nature doesn't have bad weather](http://dmitrijan.livejournal.com/152947.html)

- [Kitty, don't slap your ears on your cheeks!](http://dmitrijan.livejournal.com/148081.html)

- [They Shot (C)](http://dmitrijan.livejournal.com/167430.html)

- [We retreated in silence for a long time](http://dmitrijan.livejournal.com/169939.html)

- [European officials 5th column of Europe?](http://dmitrijan.livejournal.com/175885.html)


Txt Pro:

- [067 About Oil](http://dmitrijan.livejournal.com/88883.html)

- [068 About Current](https://dmitrijan.livejournal.com/90034.html)

- [070 About Weather](http://dmitrijan.livejournal.com/162395.html)

- [079 About Sun](http://dmitrijan.livejournal.com/148904.html)

- [087 About Meteor](http://dmitrijan.livejournal.com/97134.html)

- [090 Angular Number System](http://dmitrijan.livejournal.com/3119.html)

- [093 Farm Earth](http://dmitrijan.livejournal.com/27346.html)

- [098 About Statelets](http://dmitrijan.livejournal.com/104006.html)

- [099 About Aether and our World](http://dmitrijan.livejournal.com/68502.html)

- [100 About Magnetic Grids](http://dmitrijan.livejournal.com/152436.html)

- [111 About Vegetarianism](http://dmitrijan.livejournal.com/118792.html)

- [116 About ABC](http://dmitrijan.livejournal.com/105662.html)

- [118 About Azbogu](http://dmitrijan.livejournal.com/105798.html)

- [121 Pro Dream](http://dmitrijan.livejournal.com/176841.html)

- [129 Pro Merakaba](http://dmitrijan.livejournal.com/106163.html)

- [133 Pro People 5 Race](http://dmitrijan.livejournal.com/65910.html)

- [Txt_150_10032009_About Time](http://dmitrijan.livejournal.com/202500.html)

- [152 About The World of Man](http://dmitrijan.livejournal.com/185229.html)

- [156 Thoughts-Word-Deed](http://dmitrijan.livejournal.com/124477.html)

- [182 About the Planet](http://dmitrijan.livejournal.com/26076.html)

Txt PiP: 

- [289 No Stability](http://dmitrijan.livejournal.com/207475.html)

- [290 And We Have Something to show?](http://dmitrijan.livejournal.com/203450.html)

- [300 Reflectors, Mirrors and Mirror](http://dmitrijan.livejournal.com/1090.html)

- [303 Romans Real and Imaginary](http://dmitrijan.livejournal.com/12377.html)

- [305 Our Bias](http://dmitrijan.livejournal.com/15098.html)

- [310 Sprout of Life](http://dmitrijan.livejournal.com/16976.html)

- [311 A Fundamental Principle of Life](http://dmitrijan.livejournal.com/17394.html)

- [312 Spiritual Life](http://dmitrijan.livejournal.com/17872.html)

- [319 Why do not people fly like birds](http://dmitrijan.livejournal.com/38941.html)

- [320 quality thinking](http://dmitrijan.livejournal.com/41849.html)

- [321 malachite chronicle](http://dmitrijan.livejournal.com/61801.html)

- [322 And was the Country to Russia](http://dmitrijan.livejournal.com/71535.html)

- [323 We did IT](http://dmitrijan.livejournal.com/132233.html)

- [324 Polyushko pole](http://dmitrijan.livejournal.com/157962.html)

- [325 Worlds of our dreams](http://dmitrijan.livejournal.com/161692.html)

- [326 Look & Eyes](http://dmitrijan.livejournal.com/190536.html)

- [327 life as a Zubr-Zubr](http://dmitrijan.livejournal.com/195137.html)

- [328 Egregori, such agregory](http://dmitrijan.livejournal.com/200620.html)

- [329 Planet of Planet](http://dmitrijan.livejournal.com/203616.html)

PoM:

- [004 Telegraph of former Russia](http://dmitrijan.livejournal.com/40416.html)

- [006 funny places of Pskov](http://dmitrijan.livejournal.com/46506.html)

- [008 topic of doleviks and fiorts in discussion with assucareira](http://dmitrijan.livejournal.com/52723.html)

- [012 on works of Alexandra of Rome](http://dmitrijan.livejournal.com/49985.html)

- [013 Lord of the great Navi grad](http://dmitrijan.livejournal.com/50318.html)

- [024 immortality elixir](http://dmitrijan.livejournal.com/204194.html)

- [025 Ivan and Mary](http://dmitrijan.livejournal.com/91508.html)

- [029 alpha & omega](http://dmitrijan.livejournal.com/102086.html)

- [030 zhytie we e](http://dmitrijan.livejournal.com/106525.html)

- [032 eh Troya](http://dmitrijan.livejournal.com/113831.html)

- [038 and it was Slovo](http://dmitrijan.livejournal.com/149600.html)

- [035 When the lords fight](http://dmitrijan.livejournal.com/136586.html)

- [039 lighter than light](http://dmitrijan.livejournal.com/154008.html)

- [040 scepter and power are just decorations?](http://dmitrijan.livejournal.com/172835.html)

- [041 from the life of kakla](http://dmitrijan.livejournal.com/174805.html)

- [042 Dill, such dill](http://dmitrijan.livejournal.com/184551.html)

- [043 Devices are different, green and red](http://dmitrijan.livejournal.com/186550.html)

- [044 Our brains](http://dmitrijan.livejournal.com/188958.html)

- [045 Star and now translates as a hole](http://dmitrijan.livejournal.com/194810.html)

- [046 lost gods](http://dmitrijan.livejournal.com/195903.html)

- [047 These mysterious Runes](http://dmitrijan.livejournal.com/197961.html)

- [048 Lies, such lies](http://dmitrijan.livejournal.com/198430.html)

- [049 Measures, such measures](http://dmitrijan.livejournal.com/200032.html)

- [051 eh, Pyramid pyramids](http://dmitrijan.livejournal.com/211759.html)

- [052 What do people do all their lives? Eat and poop!](http://pro-vladimir.livejournal.com/331260.html)

- [053 scientists - light and ignorance - a little light - on work](http://dmitrijan.livejournal.com/218350.html)

- [054 the relevance of the effect of the 100th monkey for the people?](http://dmitrijan.livejournal.com/228226.html) [055 Metropolitan or the Metropolitan of Russia](http://dmitrijan.livejournal.com/228666.html)

PiPs: [Imaginary and real Romans](http://dmitrijan.livejournal.com/9955.html)

- [Cut down you fly, son.](http://dmitrijan.livejournal.com/158888.html)

OnLine: [030 the divine Cosmos](http://dmitrijan.livejournal.com/5472.html)

- [035 Toddlers' happiness,](http://dmitrijan.livejournal.com/11133.html)

- [040 did you Know that the Pskov](http://dmitrijan.livejournal.com/19135.html)

Dreams:

- [20110415 After landing UFO](http://dmitrijan.livejournal.com/177609.html)

- [And they awakened Mind](http://dmitrijan.livejournal.com/77610.html)

- [So soft in this hard world](http://dmitrijan.livejournal.com/170136.html)

- [Cherub](http://dmitrijan.livejournal.com/178311.html)

- [Strange area](http://dmitrijan.livejournal.com/186104.html)

- [Dream Castle and cunning old man](http://dmitrijan.livejournal.com/165390.html)

- [the Frozen lake](http://dmitrijan.livejournal.com/198912.html)

- [Rainbow stairs](http://dmitrijan.livejournal.com/200883.html)

- [Stiny World](http://dmitrijan.livejournal.com/203790.html)

- [20161129 Strange room](http://dmitrijan.livejournal.com/209046.html)

PS

> Longer life, excitement, strength - is that all we need?

> No, you still need distance, parting and sadness!

> The sadness of sudden revelations and hot decisions.

© All rights reserved. The original author retains ownership and rights.
