title: How Tartaria Died Part 6 - Destroyed Mountains
date: 2014-11-04
modified: 2021-08-10 10:08:59
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: When I first heard about the nuclear war on Earth 200 years ago, I took it as crazy nonsense, but on the Internet there were Alexei Kungurov films which did not simply make empty statements but cited many concrete facts.
status: published
originally: Как погибла Тартария, ч6.zip
local: How Tartaria was Lost Part 6_files/

### Translated from: 

Как погибла Тартария, ч6.zip, [https://mylnikovdm.livejournal.com/4645.html](https://mylnikovdm.livejournal.com/4645.html) and [https://mylnikovdm.livejournal.com/5102.html](https://mylnikovdm.livejournal.com/5102.html)

**Russian:**

В начале очередное дополнение к предыдущей части, где я рассказывал про"засыпанные города". После того, как она была опубликована в моём блоге и на портале [http://www.kramola.info](http://www.kramola.info/),поступило несколько десятков комментариев и писем по электронной почте, где люди пишут о том, что в их городах также имеются здания с сильнозасыпанными первыми этажами зданий. Причём подобные случаи фиксируются и в Сибири, и в европейской части России, и даже на Украине и Белоруссии.

**English:**

To start with, another addition to the previous article, where I talked about "buried cities". After it was published in my blog and on the portal [http://www.kramola.info](http://www.kramola.info/), there were several dozen comments and emails where people wrote that their cities also have buildings with heavily backfilled ground floors. And similar cases are recorded in Siberia, the European part of Russia, and even in Ukraine and Belarus.

**Russian:**

В частности, прислали вот такую ссылку с множеством интересных примеров [http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html](http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html). Изучая её я обнаружил, что был не прав по поводу того, что засыпка зданий ненаблюдается в Царском селе, поскольку там была приведена следующая фотография.

**English:**

In particular, I was sent this link with many interesting examples [http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html](http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html). While studying it, I found out that I was wrong about the fact that the filling of buildings is not observed in Tsarskoye Selo. Wrong because the following picture was provided there:

<!-- Local
[![](How Tartaria was Lost Part 6_files/59642_900.jpg#clickable)](How Tartaria was Lost Part 6_files/59642_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/59642/59642_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/59642/59642_original.jpg)

**Russian:**

Это последствия разрушений во время Великой Отечественной войны. При этом очень хорошо видны окна, которые сегодняаккуратно засыпаны земляным бруствером.

**English:**

These are the consequences of destruction during the Great Patriotic War. At the same time, you can see very well the windows, which are now carefully covered with earth.

<!-- Local
[![](How Tartaria was Lost Part 6_files/59691_900.jpg#clickable)](How Tartaria was Lost Part 6_files/59691_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/59691/59691_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/59691/59691_original.jpg)

**Russian:**

А вот ещё несколько интересных изображений Большого Екатерининскогодворца в Царском селе. Первое, это картина, которую написал Фридрих Гартман Баризьен. Большой Царскосельский дворец императрицы ЕлизаветыПетровны 1760-1761.

**English:**

And here are some more interesting pictures of the Great Catherine Palace in Tsarskoye Selo. The first one is a painting by Friedrich Gartman Barizen. The Grand Palace of Tsarskoye Selo of Empress Elizabeth 1760-1761.

<!-- Local
[![](How Tartaria was Lost Part 6_files/59932_900.jpg#clickable)](How Tartaria was Lost Part 6_files/59932_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/59932/59932_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/59932/59932_original.jpg)

**Russian:**

На двух следующих вид на церковь и северный флигель сейчас.

**English:**

The next two views of the church and the north wing now.

<!-- Local
[![](How Tartaria was Lost Part 6_files/60318_900.jpg#clickable)](How Tartaria was Lost Part 6_files/60318_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/60318/60318_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/60318/60318_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/60611_900.jpg#clickable)](How Tartaria was Lost Part 6_files/60611_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/60611/60611_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/60611/60611_original.jpg)

**Russian:**

На изображении середины 18 века нижний этаж у церквизасыпан, но потом, видимо когда строили здание лицея, лишний грунт решили снять и первый этаж по этой стене восстановили. На современныхфото у этого здания отлично видно именно полноценный первый этаж. А вот у остального 300 метрового здания первый этаж откапывать не стали, а приреставрации после ВОВ вообще, видимо, решили аккуратно присыпать землёй, чтобы лишних вопросов не возникало. Когда я поглубже копнул информациюпро Царское село и Екатерининский дворец, который на самом деле стоило бы называть Елизаветинский, обнаружилось много интересных фактов, покоторым я скорее всего напишу чуть позже отдельную статью.

**English:**

In the image from the middle of the 18th century, the ground floor near the church was backfilled, but then, apparently, when the lyceum building was built, the extra soil was removed and the ground floor was restored along this wall. On modern photos of this building you can clearly see the full ground floor. But in the rest of the 300-meter building the ground floor was not dug up, and in the restoration after the Second World War in general, it seems to have been decided to carefully arrange the ground so that there were no unnecessary questions. When I dig deeper into the information about Tsarskoye Selo and Catherine's Palace, which should actually be called Elizabeth's Palace, I found a lot of interesting facts, on which I will most likely write a separate article later.

**Russian:**

По зданиям, засыпанным грунтом, 1 ноября 2014 года в ЖЖ Сибведа появилась ещё одна интересная статья [http://sibved.livejournal.com/144844.html](http://sibved.livejournal.com/144844.html), с которой я предлагаю ознакомиться всем желающим.

**English:**

For buildings backfilled with soil, there was another intersting article on November 1, 2014 in Sibveda Zhizn - [http://sibved.livejournal.com/144844.html](http://sibved.livejournal.com/144844.html) - that I'd like everyone to read.

**Russian:**

В целом же напрашивается ещё одно большое дополнение к пятой части сподборкой и анализом новых фактов, в данной же части я хочу дать лишь основные выводы из того, что я увидел.

**English:**

In general terms, there is one more big addition to that fifth part of this assembly and analysis of new facts, in this part I want to give only basic conclusions based on what I have seen.

**Russian:**

Во-первых, уже очевидно, что засыпание зданий грунтом не было разовымявлением. Это было несколько событий, которые происходили по всему миру на протяжении как минимум последних 300, а может быть даже и 500 лет. Покрайней мере в Москве есть следы засыпки зданий и сооружений, в том числе старых крепостных стен Кремля и Китай-города, но это произошлоявно раньше, чем засыпание зданий в Санкт-Петербурге или в то же Казани, поскольку как следует из фотографий приведённых в первой ссылке [http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html](http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html) у зданий конца 18 и начала 19 веков в Москве не наблюдается такого уровня лишнегогрунта. Пример с Царским селом, который я привёл в начале, тоже говорит о том, что засыпание дворца, если верить официальной датировке егопостройки, произошло в середине 18 века. Но там обнаруживается масса нестыковок по датам и тому, кто и что в этом дворце строил иперестраивал. В частности, утверждается, что в 1752-1756 годах окончательную перестройку дворца завершил Растрелли, при этом онпостроил многоэтажные боковые флигели. Но на картине, которая датируется 1760-1761 годами, флигели изображены ещё одноэтажными. При этом еслипосмотреть на пристроенные позже флигели, то очень хорошо видно, что они несколько отличаются по стилю от остального дворца, что характерно какраз для элементов зданий, которые достраиваются позже.

**English:**

First of all, it is already obvious that covering the buildings with soil was not a single event. These were several events that took place around the world over at least the last 300 and even 500 years. At least in Moscow there are traces of backfilling of buildings and constructions, including old fortress walls of the Kremlin and Kitay-gorod. But it happened before that there was backfilling of buildings in St. Petersburg or in the same Kazan. It is clear from photos presented in the first reference - [http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html](http://atlantida-pravda-i-vimisel.blogspot.ru/2013/11/16.html) - of buildings of the end of 18 and the beginning of 19 centuries that in Moscow such raised ground level is not observed. The example of Tsarskoye Selo, which I gave in the beginning, also shows that the hibernation? of the palace, according to the official date of its construction, occurred in the mid-18th century. But there are a lot of inconsistencies in terms of dates and who and what built and rebuilt what in this palace. In particular, it is alleged that in 1752-1756 Rastrelli completed the final reconstruction of the palace, and he built multi-storey side wings. But in a painting, which dates back to 1760-1761, the wings are depicted as single-storey. If you look at the wings attached later, you can clearly see that they are somewhat different in style from the rest of the palace, and are characteristic of the elements of buildings that are completed later.

**Russian:**

Во-вторых, по тем описаниям, которые приводят люди из разных мест, можно сделать вывод, что сам процесс образования лишнего грунта может иметьразную природу. В каких-то случаях это, действительно, больше похоже на последствия наводнения и селевого потока, поскольку более высокийуровень лишнего грунта соответствует понижению рельефа местности. В других случаях, как в Казанском кремле или Петропавловской крепости,больше похоже на то, что лишний грунт сыпался или стекал сверху.

**English:**

Secondly, it can be concluded from the descriptions given by people from different places that the very process of formation of excess soil may be of a different nature. In some cases, this is indeed more likely to be the consequence of flooding and debris flow, since a higher level of excess soil corresponds to a lower terrain. In other cases, as in the Kazan Kremlin or the Peter and Paul Fortress, it is more likely that excess soil has fallen or flowed down from above.

**Russian:**

В третьих, как сообщают некоторые из читателей, в Омске, Кургане и Новосибирске есть здания, которые построены до войны или сразу послевойны, у которых также наблюдается подобная засыпка нижнего этажа! Эту информацию ещё нужно проверять, наблюдается ли этот эффект у единичныхзданий или это характерно для всех близлежащих зданий этого периода, поскольку в первом случае мы скорее всего имеем дело с просадкойфундаментов из-за ошибок при проектировании или строительстве. Но, поскольку такая информация поступила из нескольких городов, я всё такипредполагаю, что мы имеем дело с тем же самым процессом.

**English:**

Thirdly, as some of the readers report, in Omsk, Kurgan and Novosibirsk there are buildings that were built before the war or immediately after the war, which also have a similar filling of the ground floor! This information still needs to be checked to see if this effect is observed in single buildings or if it is typical of all nearby buildings of this period, because in the first case we are likely to be dealing with subsidence due to design or construction errors. But since this information came from several cities, I still believe that we are dealing with the same process.

**Russian:**

Последняя информация о том, что подобные события происходили практически до начала 1960-х годов, натолкнула меня на мысль, что эти события могутбыть напрямую связаны с проведением испытания ядерного оружия, в ходе которых всеми странами, по официальным данным, было взорвано 609открытых ядерных взрывов разной мощности на земле, в атмосфере и в океане (общее количество взрывов, включая подземные, более 2000). Из них214 открытых взрывов было произведено в СССР. Подробности по взрывам в СССР тут [http://wsyachina.narod.ru/history/nuclear_testing_3.html](http://wsyachina.narod.ru/history/nuclear_testing_3.html).

**English:**

This new information that such events occurred almost up to the early 1960s led me to believe that these events could be directly related to the nuclear weapons tests, in which all countries, according to official data, detonated 609 open nuclear explosions of various capacities on the ground, in the atmosphere and in the ocean. (The total number of explosions, including underground explosions, is over 2000.) Of these, 214 open explosions were carried out in the USSR. Details of the explosions in the USSR are here: [http://wsyachina.narod.ru/history/nuclear_testing_3.html](http://wsyachina.narod.ru/history/nuclear_testing_3.html).

**Russian:**

На самом деле это очень много, хватило бы на приличную ядерную войну. Носамое главное, что при наземных и воздушных надземных ядерных взрывах в верхние слои атмосферы с поверхности земли поднимается большоеколичество грунта и пыли. Именно они и формируют тот самый ядерный гриб, который все мы помним по художественным и документальным фильмам.

**English:**

Actually, that's a lot, enough for a decent nuclear war. The main thing is that during ground and above-ground nuclear explosions, a large amount of soil and dust rises from the surface of the earth to the upper atmosphere. It is this that form the nuclear mushroom that we all remember from the films and documentaries.

**Russian:**

Желающие увидеть, как это происходит в действительности, могутпосмотреть подборку документальных съёмок с испытаний ядерного оружия [https://youtu.be/ZDgC2V8Av14](http://www.youtube.com/watch?v=ZDgC2V8Av14)

**English:**

Those wishing to see how this actually happens can view a collection of documentary footage from nuclear weapons tests.

[https://youtu.be/ZDgC2V8Av14](http://www.youtube.com/watch?v=ZDgC2V8Av14)

<!-- Local
[![](How Tartaria was Lost Part 6_files/60909_900.jpg#clickable)](How Tartaria was Lost Part 6_files/60909_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/60909/60909_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/60909/60909_original.jpg)

**Russian:**

А это американская фотография взрыва над долиной Нагасаки. Атомная бомба "Толстяк" (Fat Man), сброшенная с американского бомбардировщика B-29,взорвалась на высоте 300 метров. "Атомный гриб" взрыва --- столб дыма, раскаленных частиц, пыли и обломков --- поднялся на высоту 20километров.

**English:**

And this is an American picture of the explosion over Nagasaki Valley. The Fat Man atomic bomb, dropped from an American B-29 bomber, exploded at an altitude of 300 meters. The "Atomic Mushroom" explosion - a column of smoke, hot particles, dust and debris - rose to a height of 20 kilometers.

**Russian:**

Очень вероятно, что о реальных последствиях ядреного взрыва нам рассказывают далеко не всё. Если вся эта пыль была поднята в верхниеслои атмосферы, то она рано или поздно должна была где-то упасть обратно на землю. Так что наличие зданий построенных до 1960-х годов, у которыхнаблюдается засыпка грунтом, причём в городах, которые как раз расположены достаточно близко к Семипалатинскому ядерному полигону,может быть одним из последствий этих испытаний. Всё, что было связано с ядерными испытаниями и их последствиями, было в СССР засекречено и неподлежало огласке. В СМИ про это писать было запрещено и поэтому большинству населения об этих явлениях ничего не известно.

**English:**

It is very likely that not everything is told us about the real consequences of a nuclear explosion. If all this dust was lifted into the upper atmosphere, it had to fall back to the ground somewhere sooner or later. So the presence of buildings built before the 1960s, which have been backfilled with soil, and in cities that are close enough to the Semipalatinsk nuclear test site, may be one of the consequences of these tests. Everything connected with nuclear tests and their consequences was classified in the USSR and not publicized. It was forbidden to write about it in the mass media and therefore the majority of the population did not know anything about these phenomena.

**Russian:**

Добавьте сюда ещё тот факт, что основные страны, которые вели испытанияядерного оружия, в 1963 году вдруг решили отказаться от открытых ядерных взрывов и стали использовать для испытания ядерного оружия толькоподземные взрывы, которые не будут иметь серьёзных последствий для атмосферы и экологии, а в том числе не будет и переноса и высыпаниябольшого количества грунта и пыли.

**English:**

Add to this the fact that the main countries that tested nuclear weapons in 1963 suddenly decided to abandon open nuclear explosions and began to use only underground explosions to test nuclear weapons, which will not have serious consequences for the atmosphere and the environment, including the transfer and raising of large amounts of soil and dust.

**Russian:**

Если этот грунт выпал как следствие проведения ядерных испытаний, то у него, несомненно, должен быть явный признак в виде повышенного уровнярадиации. Но тут есть одно но. На самом деле мы не знаем, насколько этот уровень должен быть выше нормы. Это зависит от множества факторов,включая состав вещества в самом грунте. Кроме того, это будет не первичная радиация, как в случае с радиоактивными материалами, авторичная, вызванная облучением этого вещества радиацией во время самого взрыва. Из той информации, что мне удалось найти, в этом случаеобразуются в основном коротко живущие изотопы, которые очень быстро распадаются, поэтому повышенный фон регистрируется только очень короткоевремя непосредственно после взрыва. Это означает, что сейчас уровень радиации этого грунта не будет сильно выделяться из общего фона.

**English:**

If this soil has fallen out as a result of nuclear testing, it should definitely leave a clear sign of increased radiation. But there's one thing: the truth is, we don't know how much higher this level would have to be. It depends on many factors, including the composition of the ground itself. Besides, it wouldn't be primary radiation, as in the case of radioactive materials, primary radiation caused by exposure of this substance to radiation during the explosion itself. From the information that I have been able to find, mainly short-lived isotopes are formed in this case, which decay very quickly, so that the increased background is only recorded for a very short time immediately after the explosion. This means that now the radiation level of this soil will not stand out much from the general background.

**Russian:**

В общем, этот вопрос, несомненно, требует дополнительных исследований,которые уже выходят за рамки энтузиастов-одиночек.

**English:**

In general, this issue undoubtedly requires more research, which already goes beyond single enthusiasts.

**Russian:**

В тоже время, если в 20-ом веке причиной появления лишнего грунта в городах могли быть ядерные взрывы, то вполне вероятно, что эта жепричина может быть и у того лишнего грунта, который выпадал сверху в 18 и 19 веках? Правда, тут возникает проблема с тем, что первую ядернуюбомбу человечество изобрело и изготовило только в 1940-х годах. Но из чего следует, что те бомбы должны были быть изготовлены именно нами? Итот факт, что мы пока не обнаружили во Вселенной другие Цивилизации, вовсе не означает, что их там нет. Особенно если они очень не хотят,чтобы мы знали об их существовании.

**English:**

At the same time, if in the 20th century the cause of excess soil in cities could have been nuclear explosions, is it likely that the same cause could account for the excess soil that fell from above in the 18th and 19th centuries? It is true that there is a problem with the fact that the first nuclear bomb was invented and manufactured by mankind only in the 1940s. But from what does it follow that those bombs have only been made by us? The fact that we haven't yet discovered other civilisations in the universe does not mean they're not there. Especially if they really don't want us to know they exist.

**Russian:**

Когда я в первый раз услышал о том, что ядерная война на Земле произошла 200 лет назад, я воспринял это как глупую шутку или бред сумасшедшего.Но потом в интернете появились фильмы с Алексеем Кунгуровым, в которых были не просто пустые заявления, но также приводилось множествоконкретных фактов, из которых следовало, что официальная версия истории очень многого не договаривает или даже сознательно искажает. И этозаставило меня по другому взглянуть на многие вещи, которые мы видим вокруг.

**English:**

When I first heard about the nuclear war on Earth 200 years ago, I took it as a stupid joke or crazy nonsense, but then there were films on the Internet with Alexei Kungurov, which did not simply make empty statements but also cited many concrete facts. Facts with which the official version of the story very much does not agree or even deliberately distorts. And it made me take a different look at many things that we see around us.

**Russian:**

Первую хорошо читаемую воронку, которая по многим критериям подходит наслед от высотного ядерного взрыва, обнаружил мой брат, после того, как я дал ему ссылки на фильмы Алексея Кунгурова. Оказалось, что она находитсявсего в 40 километрах от Челябинска, где мы живём, в районе города Еманжелинск. Я уже приводил это изображение, но повторим его ещё раз.

**English:**

The first easily visible crater, which by many criteria fits the profile of the result of a high-altitude nuclear explosion, was discovered by my brother after I gave him links to Alexei Kungurov's films. It turned out that it is only 40 kilometers from Chelyabinsk, where we live, in the area of the city of Emanzhelinsk. I've already given you this image, but let's go over it again.

<!-- Local
[![](How Tartaria was Lost Part 6_files/61122_900.jpg#clickable)](How Tartaria was Lost Part 6_files/61122_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/61122/61122_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/61122/61122_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/61255_900.jpg#clickable)](How Tartaria was Lost Part 6_files/61255_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/61255/61255_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/61255/61255_original.jpg)

**Russian:**

Диаметр воронки составляет 13 км, в центре хорошо выделяется область так называемой "зоны подсоса". След идеально круглойформы таких размеров остаётся только от воздушного ядерного взрыва. От удара метеорита, во-первых, остаётся кратер с бортами, а во-вторых,метеориты очень редко падают под прямым углом к поверхности, а только в таком случае кратер от удара метеорита будет круглым, а не вытянутымвдоль траектории падения.

**English:**

The diameter of the crater is 13 km. In its center, the so-called "suction zone" stands out well. A perfectly rounded shape of this size is left only by an airborne nuclear explosion. From the impact of a meteorite, firstly, there remains a crater with sides, and secondly, meteorites very rarely fall at right angles to the surface, but only then the crater from the impact of a meteorite will be round, not stretched along the trajectory of the fall.

**Russian:**

Позже обнаружилась ещё одна хорошо читаемая воронка в районе Чебаркульской крепости, которая заметно меньше Еманжелинской. Либо силавзрыва в Чебаркуле была слабее, либо он был намного ниже, а возможно, что и то, и другое.

**English:**

Later, another easily readable crater was found in the area of Chebarkul Fortress, which is noticeably smaller than the Emanzhelin Fortress. Either the reaction in Chebarkul was weaker, or it was much lower, and possibly both.

<!-- Local
[![](How Tartaria was Lost Part 6_files/61680_900.jpg#clickable)](How Tartaria was Lost Part 6_files/61680_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/61680/61680_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/61680/61680_original.jpg)

**Russian:**

Но есть и совсем другие следы, которые не так легко распознать, как воронки на равнинах, поскольку это следы ядерной бомбардировки нашихУральских гор. Причём следы эти видят десятки и сотни тысяч людей, которые путешествуют по нашим горам, просто они не понимают, что же онивидят на самом деле.

**English:**

But there are very different traces that are not as easy to recognize as the craters in the plains, because these are the traces of the nuclear bombing of our Urals mountains. And these traces are seen by tens and hundreds of thousands of people who are traveling through our mountains. They just don't understand what they are really seeing.

**Russian:**

Дело в том, что очень многие горы и даже целые горные хребты на Урале разрушены! Очень многие склоны покрыты так называемыми "каменнымиреками", огромными россыпями крупного колотого камня. А если вы начнёте внимательно изучать торчащие из земли скальные останцы, то большинствоиз них покрыто трещинами. Причём расколоты эти скалы и камни совсем недавно, поскольку на них нет следов водной и ветровой эрозии, которыедолжны быть на камнях и скалах лежащих под ветром и водой уже тысячи лет. Все эти камни фрагменты скал имеют острые кромки и свежие сколы,которые за тысячи лет должны были сгладиться и стать округлыми. А это означает, что разломаны они были не так уж давно, не больше 300 летназад.

**English:**

The fact is that many mountains and even whole mountain ranges in the Urals are destroyed! Very many slopes are covered with so-called "stone rivers", huge scatterings of large broken stone. And if you start to study carefully the rock remains sticking out of the ground, most of them are covered with cracks. And these rocks and stones were cracked quite recently, because there are no traces of water and wind erosion on them. That should not be so on rocks and rocks that were lying under the wind and water for thousands of years. All these rock fragments have sharp edges and fresh chips that should have been smoothed out and rounded through thousands of years of erosion. And that means that they were broken not so long ago, not more than 300 years ago.

**Russian:**

Но есть и другие следы, которые говорят о том, что в Уральских применялось именно ядерное оружие. Это схема района, где мне самомунеоднократно доводилось бывать в горах, последний раз летом 2014 года.

**English:**

But there are other traces that suggest that nuclear weapons were used in the Urals. This is a diagram of the area where I have been to the mountains myself on several occasions, last time in the summer of 2014.

<!-- Local
[![](How Tartaria was Lost Part 6_files/61716_900.jpg#clickable)](How Tartaria was Lost Part 6_files/61716_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/61716/61716_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/61716/61716_original.jpg)

**Russian:**

На схеме хорошо видна жёлтая линия федеральнойавтотрассы М5, идущей из Москвы через Уральские горы на восток (Челябинск, Курган, Омск и т.д.).

**English:**

The yellow line of the M5 federal motorway running from Moscow through the Ural Mountains to the east (Chelyabinsk, Kurgan, Omsk, etc.) can be clearly seen on the diagram.


**Russian:**

На ней я выделил три вершины, о которых пойдёт речь. Яман-Тау - самаявысокая гора Южного Урала, гора Иремель --- излюбленное место туристов, а также гора Большой Шелом, являющейся самой высокой точкой хребтаЗигальга, идущего вдоль левого края долины реки Юрюзань, берущей начало у подножия Яман-Тау. А вот так выглядит гора Большой Шелом на крупномасштабном снимке соспутника.

**English:**

On it, I've identified three peaks that we're going to talk about. Yaman Tau - the highest mountain in the South Urals, Mount Iremel - a favorite place for tourists, as well as Mount Big Shelom, which is the highest point of the Zigalga ridge, which runs along the left edge of the valley of the Yuriuzan River, and which originates at the foot of Yaman Tau. And this is what Big Shelom Mountain looks like in a large-scale satellite image.

<!-- Local
[![](How Tartaria was Lost Part 6_files/62054_900.jpg#clickable)](How Tartaria was Lost Part 6_files/62054_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/62054/62054_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/62054/62054_original.jpg)

**Russian:**

Вместо остроконечной вершины мы видим плоскую площадкупрактически прямоугольной формы, а по середине нижней грани небольшое возвышение, которое является наиболее высокой точкой и считаетсявершиной горы Большой Шелом. Если посмотреть на Большой Шелом снизу, то он выглядит примерно вот так.

**English:**

Instead of a pointed peak, we see a flat platform of rectangular shape and a small elevation in the middle of the lower edge, which is the highest point and is considered to be the top of Mount Big Shelom. If you look at Big Shelom from below, it looks like this.

<!-- Local
[![](How Tartaria was Lost Part 6_files/62432_900.jpg#clickable)](How Tartaria was Lost Part 6_files/62432_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/62432/62432_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/62432/62432_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/62717_900.jpg#clickable)](How Tartaria was Lost Part 6_files/62717_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/62717/62717_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/62717/62717_original.jpg)

**Russian:**

То есть, собственно вершина представляет собой холмикправильной конической формы, насыпанный из битого камня. При этом снять его именно как вершину, неприступно возвышающуюся над окрестными горамипрактически невозможно, поскольку этот небольшой холмик, что хорошо видно на снимке со спутника, стоит посредине ровного плато. Поэтому есливы будете искать в интернете фотографию горы Большой Шелом, то скорее всего найдёте либо что-то похожее на следующее фото, на котором на самомделе мы видим вовсе не вершину, а подъём на плато со стороны Малого Шелома, поскольку саму вершину с этой точки, как и со многих других,увидеть просто невозможно.

**English:**

What I mean is this: the summit itself is a conical shaped hillock, sprinkled with broken stone. But unfortunately it is impossible to photograph it as a peak rising impregnably above the surrounding mountains, because this small hill, which is clearly visible on a satellite image, stands in the middle of a flat plateau. As a result, if you search the Internet for a picture of Mount Big Shelom, you will most likely find something similar to the next photo, where in reality we do not see Big Shelom's summit because from the plateau to the side of Little Shelom, the summit itself is simply impossible to see.

<!-- Local
![](How Tartaria was Lost Part 6_files/62901_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/62901/62901_900.jpg#resized)

**Russian:**

Либо вам вообще покажут живописные фотографии гор, нотолько это будет не Большой Шелом, а Малый Шелом или ещё чаще 3 и 4 Шеломы, которые находятся на хребте Зигальга рядом и уже выглядят какгоры, а не как непонятная, хотя и очень большая куча щебня, на которую больше похож Большой Шелом.

**English:**

You'll be shown pictures of mountains, except they won't be of Big Shel, but Little Shel or - more often - Sheloms 3 and 4, which are sited on the Zigalga Ridge next to each other and look much like mountains. They are more engaging than a rather incomprehensible, though very large pile of rubble, which is what Big Shel looks like.

<!-- Local
[![](How Tartaria was Lost Part 6_files/63092_900.jpg#clickable)](How Tartaria was Lost Part 6_files/63092_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/63092/63092_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/63092/63092_original.jpg)

**Russian:**

Само название "Шелом" происходит от более известного большинству слова шлем. Но я предполагаю, что это название в началеполучила гора Малый Шелом поскольку именно она издалека своей округлой формой с выступающей над ней пипкой вершины как раз похожа на старинныйшлем русских витязей. А вот как изначально называлась вершина Большого Шелома, это очень интересный вопрос. На следующей фотографии слева мывидим Большой Шелом, правее Малый Шелом в виде шлема, и ещё правее вершина 3-го Шелома.

**English:**

The very name "Shelom" comes from the word helmet, better known to most. But I suppose that this name was first received for the Little Shelom mountain because, from a distance, its rounded shape with the peak protruding above it, looks just like the oldest Russian knights' helmet. But what was the peak of Big Shelom originally called? This is a very interesting question. In the next picture on the left we see Big Shel, on the right Little Shel in the form of a helmet, and on the right, the top of the 3rd Shelom.

<!-- Local
[![](How Tartaria was Lost Part 6_files/63251_900.jpg#clickable)](How Tartaria was Lost Part 6_files/63251_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/63251/63251_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/63251/63251_original.jpg)

**Russian:**

Почему же гора Большой Шелом выглядит так странно? И откуда взялось такое количество битого камня вокруг, называемого "курумы"? Всё этопоследствия воздушного ядерного взрыва, который произошёл непосредственно над вершиной Большого Шелома. Мощная ударная волна,которая образуется во время взрыва, распространяется от эпицентра и разрушает кристаллическую структуру вещества на определённом расстоянии,которое зависит от мощности заряда. Именно на это расстояние от эпицентра "срезало" вершину горы, образовав вместо неё ровное плато. Заэтой границы кристаллическая структура вещества уже не разрушается полностью, а просто происходит дробление камня. Причём это дроблениепроисходит на протяжении всего хода ударной волны в радиусе от нескольких десятков, до полутора сотен километров в зависимости от силывзрыва. В итоге все ближайшие горы на несколько десятков километров оказываются засыпаны битым камнем, курумами. Местами эти курумырассыпаются по долинам, где раньше текли реки и ручьи, в результате чего и получаются так называемые "каменные реки". Называют их так в том числепотому, что когда вы идёте по этим камням, то внизу отчётливо слышно, как под камнями течёт вода.

**English:**

Why does Mount Big Shelom look so weird? And where did so much of the broken stone around it - called "kurum" - come from? It's all a consequence of an airborne nuclear explosion that took place just above the top of Big Shelom. The powerful shock wave generated by a nuclear explosion spreads from the epicenter and destroys the crystalline structure of the material up to a distance which depends on the power of the charge. It is close to the epicenter that we see the top of the mountain is 'cut', forming a flat plateau. Beyond this boundary of this cut, the crystal structure of matter is no longer completely destroyed, but is simply crushed stone. And this crushing takes place during the passge of the shock wave up to a radius from several tens to 150? kilometers depending on the force of explosion. As a result, all the nearby mountains up to several tens of kilometers away are covered with broken stone, kurum. In some places these kurumyrs are scattered in the valleys where rivers and streams used to flow, resulting in so-called "stone rivers". They are also called so because when you walk on these rocks, you can clearly hear water flowing under the rocks.

**Russian:**

А тот конический холмик, который сегодня считается вершиной БольшогоШелома, как раз отмечает точно положение эпицентра взрыва, поскольку является следствием действия так называемой "зоны подсоса", когдараскалённый ядерным взрывом воздух начинается стремительно подниматься вверх, закручиваясь в ядерный гриб, к центру устремляются ураганныеветра. Мелкая пыль поднимается вверх, а более крупные осколки и камни остаются внизу, формируя холм правильной конической формы, который мы инаблюдаем.

**English:**

And that conical hill, which is now considered to be the top of Big Shelom, records exactly the location of the epicenter of the explosion, because it is a consequence of the so-called "suction zone", when the air compressed by a nuclear explosion begins to rise up rapidly, twisting into a nuclear mushroom, and accompanied by hurricane winds rushing to the center. Fine dust rises up, while larger fragments and rocks remain at the bottom, forming a hill of regular conical shape, which we observe.


**Russian:**

Также не секретом является тот факт, что все эти гранитные россыпи имеют повышенный радиоактивный фон, поскольку согласно официальной версии, всегранитные выходы на Урале обладают повышенным радиоактивным фоном. Но, по имеющимся у меня сведениям, в районе Большого Шелома есть достаточносильно загрязнённые радиацией участки, поскольку было несколько случаев, когда после походов по хребту Зигальга у некоторых из людейфиксировались странные болезни, по симптомам очень похожие на лучевую болезнь. Одна из женщин даже умерла в начале 1990-х годов после такогопохода, при этом официально врачами был поставлен другой диагноз, но в неформальной беседе один из врачей сказал родственникам, что по всемсимптомам это была именно лучевая болезнь. После этого среди туристов Челябинска поползли упорные слухи, что где-то в районе Большого Шеломаимеются нелегальные захоронения радиоактивных материалов, которые тогда связывали с расположенной рядом запретной зоной, которая сегодняизвестна как "город Трёхгорный". Причём всё это было на волне только что рассекреченных сведений по аварии на заводе Маяк в 1957 году, что лишьдобавляло аргументов сторонникам этой версии. Сейчас же уже можно однозначно сказать, что зоны повышенной радиации в районе Зигальги никакне могут быть связаны с производством в городе Трёхгорный, поскольку оно не имеет никакого отношения к производству или использованию каких-либорадиоактивных веществ.

**English:**

It is also not a secret that all these granite placers have raised background radiation levels. That is, according to the official version, all granite areas in the Urals have elevated background radiation. But, according to my information, there are areas quite heavily contaminated with radiation in the area of Bolshoi Shelom. There were several cases where, after hiking on the Sigalga Ridge, people developed develped strange diseases, with symptoms very similar to radiation sickness. One woman even died in the early 1990s after such a walk. The doctors made a different diagnosis officially, but in an informal conversation one of the doctors told relatives that, with those symptoms, it was radiation sickness. After that, there were persistent rumors among tourists in Chelyabinsk that radioactive material had been illegally dumped somewhere in the vicinity of Bolshoi Shelomaisk. At that time the area was associated with a nearby restricted area, now known as the "City of Tryokhorny". And all this was on a wave of recently declassified information about the accident at the Mayak factory in 1957, which gave the supporters of this version additional evidence. Now it is impossible to say unequivocally that raised radioactivity in area of Zigalga is in any way connected with manufacture in the city of Tryohgorny because it has no relation to manufacture or use of any lab or radioactive substances. 

**Russian:**

Теперь давайте посмотрим поближе, как выглядят все эти скалы и курумы сейчас. Фотографии, публикуемые ниже, в основном из моего личногофотоархива. Как я уже писал выше, мне много раз доводилось ходить по окрестным горам, последний раз в июе 2014 года, когда мы предпринялиочередную попытку подняться на Большой Шелом. Попытка, к сожалению, оказалась неудачной, поскольку саму гору затянуло облаками и пошёлсильный дождь. Скакать по мокрым камням не самое лучшее удовольствие, а с учётом облаков и тумана, полностью закрывших вершину, смыслаподниматься на верх всё равно не было, поскольку снять на фото и видео мы бы всё равно ничего толком не смогли. Тем не менее, было сделаномножество интересных снимков во время подхода к горе, которые я публикую ниже с краткими пояснениями.

**English:**

Now let's take a closer look at what all these rocks and kurums look like today. The photos published below are mostly from my personal photo archive. As I wrote above, I have walked around the surrounding mountains many times, most recently in July 2014, when we made our next attempt to climb the Big Shel. Unfortunately, the attempt was unsuccessful because the mountain itself was covered in clouds and it rained heavily. Walking on wet rocks is not the greatest fun, and given the clouds and fog that completely covered the summit, there was no point in climbing anyway because we couldn't photograph or film anything. However, we took a lot of interesting photos during the approach to the mountain, which I publish below with brief explanations.

**Russian:**

Во первых, когда начинаешь подниматься на хребет Зигальга отрасположенной в долине реки Юрюзань деревни Александровка, то в лесу постоянно попадаются россыпи камней. При этом многие из них не являютсяместными скальными выходами, поскольку лежат по верх грунта. Подобная картина наблюдается возле каменных карьеров, где добычу камня ведутвзрывным способом, при котором некоторые из камней разбрасываются взрывом по близлежащим лесам. Вот только взрывных работ в этой местностиникто не вёл. Выглядит это примерно так.  Камни встречаются как относительно маленькие

**English:**

First of all, when you start climbing the Zigalga Ridge in the valley of the Yuryuzan river in the village of Alexandrovka, you constantly find scattered stones in the forest. Many of them are not local rock outcrops, as they lie on top of the ground. A similar pattern is observed near stone quarries, where stone mining is carried out in an explosive manner, where some of the stones are scattered by explosion into the nearby forests. Except there were no blasting operations in this area. The stones are found as relatively small. Looks something like this. 

<!-- Local
[![](How Tartaria was Lost Part 6_files/63622_900.jpg#clickable)](How Tartaria was Lost Part 6_files/63622_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/63622/63622_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/63622/63622_original.jpg)

**Russian:**

достаточно большие

**English:**

Large enough...

<!-- Local
[![](How Tartaria was Lost Part 6_files/63759_900.jpg#clickable)](How Tartaria was Lost Part 6_files/63759_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/63759/63759_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/63759/63759_original.jpg)

**Russian:**

а также средние по размеру. Сомневаюсь, чтобы кому-то могло понадобиться привозить и сваливать эти камни в лесу.

**English:**

as well as of average size. I doubt anyone would need to bring and dump these rocks in the woods.


<!-- Local
![](How Tartaria was Lost Part 6_files/100000000000120000000D80F903E3F7.jpg#resized)
-->

![](./images/How Tartaria was Lost Part 6_files/100000000000120000000D80F903E3F7.jpg#resized)

<!-- Local
[![](How Tartaria was Lost Part 6_files/64080_900.jpg#clickable)](How Tartaria was Lost Part 6_files/64080_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/64080/64080_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/64080/64080_original.jpg)

**Russian:**

Последний камень мы решили подкопать, чтобы убедиться, что он лежит сверху грунта, а не является частью скалы, торчащей изземли (подкапывали с дальней стороны относительно предыдущего снимка).

**English:**

We decided to dig up the last stone to make sure it was lying on top of the ground and not part of a rock protruding from the ground (digging from the far side of the previous image).

<!-- Local
[![](How Tartaria was Lost Part 6_files/64449_900.jpg#clickable)](How Tartaria was Lost Part 6_files/64449_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/64449/64449_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/64449/64449_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/64674_900.jpg#clickable)](How Tartaria was Lost Part 6_files/64674_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/64674/64674_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/64674/64674_original.jpg)


**Russian:**

 А вот так выглядят склоны хребта Зигальга, большей частью засыпанные колотым камнем.

**English:**

And this is what the slopes of the Zigalga Ridge look like - mostly covered with chipped stone.

<!-- Local
[![](How Tartaria was Lost Part 6_files/64987_900.jpg#clickable)](How Tartaria was Lost Part 6_files/64987_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/64987/64987_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/64987/64987_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/65206_900.jpg#clickable)](How Tartaria was Lost Part 6_files/65206_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/65206/65206_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/65206/65206_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/65403_900.jpg#clickable)](How Tartaria was Lost Part 6_files/65403_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/65403/65403_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/65403/65403_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/65786_900.jpg#clickable)](How Tartaria was Lost Part 6_files/65786_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/65786/65786_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/65786/65786_original.jpg)

**Russian:**

Во-первых, обратите внимание на то, что большая частькамней имеют примерно одинаковый размер, причём дальше будет видно, что размер этот достаточно крупный, от полуметра до несколько метров. Сучётом средней плотности гранита 2.6 тонны на кв. метр масса этих "камешков" от тонны до десяти тонн.

**English:**

First, note that most of the stones are about the same size, and further you will see that this is quite a large size, from half a meter to several meters. Taking into account the average density of granite of 2.6 tons per square meter, most of these "pebbles" weigh from a ton to ten tons.

**Russian:**

Во-вторых, склоны гор в этом месте не очень высокие и крутые, чтобы поним могли сходить каменные лавины. Им тут просто негде разогнаться для набора необходимой скорости и энергии, чтобы так дробить камни.

**English:**

Secondly, the slopes of the mountains in this place are not so very high and steep that avalanches of stone can fall. They simply provide nowhere to accelerate to gain the necessary speed and energy to crush the stones like this.

**Russian:**

Другими словами, не существует естественных причин, которые могли быобъяснить появление данных "курумов" в таких количествах в этих местах. Ни землетрясения, ни естественная эрозия за счёт ветра, воды и перепадовтемператруы не могут дать такое количество битого камня подобных размеров, да ещё и так раскидать их по склонам. Тем более, что данныйрайон не является сейсмически активным.

**English:**

In other words, there are no natural reasons that could explain the appearance of these "kurums" in such quantities in these places. Neither earthquakes nor natural erosion due to wind, water and temperature fluctuations can create such a quantity of broken stone of such sizes, and also spread them across slopes. All the more so because this area is not seismically active.

**Russian:**

Вот так, например, выглядит гора "Антенная" на хребте Зигальга, которую назвали так из-за антенны пассивного отражателя, которая виднеетсясправа за горой.

**English:**

For example, the mountain "Antenna" on the ridge of Sigalga looks like this, which was named so because of the passive reflector antenna, which can be seen right behind the mountain.

<!-- Local
[![](How Tartaria was Lost Part 6_files/65867_900.jpg#clickable)](How Tartaria was Lost Part 6_files/65867_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/65867/65867_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/65867/65867_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/66284_900.jpg#clickable)](How Tartaria was Lost Part 6_files/66284_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/66284/66284_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/66284/66284_original.jpg)

**Russian:**

Обратите внимание, что вся скала в трещинах, и все кромки острые. Следы эрозии ветра и воды, которые должны быть на старыхскалах, отсутствуют.

**English:**

Note that the whole rock is cracked, and all the edges are sharp. There are no traces of the wind and water erosion that should be found on the old rocks.

**Russian:**

Это ещё одна скала дальше по склону хребта в сторону Большого Шелома, которая выглядит точно так же. Снова скала вся в трещинах, а камни, чторассыпаны вокруг, являются её обломками.

**English:**

This is another rock further down the ridge towards Big Shelom, which looks exactly the same. Once again, the rock is covered in cracks, and the rocks that are scattered around are its wreckage.

<!-- Local
[![](How Tartaria was Lost Part 6_files/66411_900.jpg#clickable)](How Tartaria was Lost Part 6_files/66411_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/66411/66411_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/66411/66411_original.jpg)

<!-- Local
![](How Tartaria was Lost Part 6_files/1000000000000640000004B069DBF7C1.jpg#resized)
-->

![](./images/How Tartaria was Lost Part 6_files/1000000000000640000004B069DBF7C1.jpg#resized)

<!-- Local
[![](How Tartaria was Lost Part 6_files/66598_900.jpg#clickable)](How Tartaria was Lost Part 6_files/66598_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/66598/66598_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/66598/66598_original.jpg)


**Russian:**

А вот так эти "курумы" выглядят вблизи. Обратите внимание на острые кромки камней, которые не успели сгладиться под действием ветра и воды.Это говорит о том, что наломаны эти камни относительно недавно. Где-то лет 200 или 300 назад, но никак не тысячи или даже сотни тысяч лет, какговорит официальная версия, поскольку утверждается, что Уральские горы являются очень старыми, а потому, якобы, уже давно разрушаются поддействием воды и ветра.

**English:**

And this is how these "kurums" look close up. Note the sharp edges of the stones, which have not been smoothed out by the wind and water, indicating that these stones were broken relatively recently. About 200 or 300 years ago, but not thousands or even hundreds of thousands of years ago, as the official version will say. The official version claims that the Ural Mountains are very old, and therefore, allegedly, its rocks have long been eroded by the impact of water and wind.

<!-- Local
[![](How Tartaria was Lost Part 6_files/66897_900.jpg#clickable)](How Tartaria was Lost Part 6_files/66897_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/66897/66897_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/66897/66897_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/67099_900.jpg#clickable)](How Tartaria was Lost Part 6_files/67099_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/67099/67099_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/67099/67099_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/67458_900.jpg#clickable)](How Tartaria was Lost Part 6_files/67458_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/67458/67458_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/67458/67458_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/67619_900.jpg#clickable)](How Tartaria was Lost Part 6_files/67619_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/67619/67619_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/67619/67619_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/67846_900.jpg#clickable)](How Tartaria was Lost Part 6_files/67846_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/67846/67846_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/67846/67846_original.jpg)

**Russian:**

На следующем снимке объём "камешка" на переднем планебольше 2 кубических метров, соответственно масса его больше 5 тонн.

**English:**

On the next image the volume of the "pebble" in the foreground is more than 2 cubic meters. Respectively, it weighs more than 5 tons.

<!-- Local
[![](How Tartaria was Lost Part 6_files/68274_900.jpg#clickable)](How Tartaria was Lost Part 6_files/68274_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/68274/68274_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/68274/68274_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/68444_900.jpg#clickable)](How Tartaria was Lost Part 6_files/68444_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/68444/68444_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/68444/68444_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/68761_900.jpg#clickable)](How Tartaria was Lost Part 6_files/68761_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/68761/68761_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/68761/68761_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/68909_900.jpg#clickable)](How Tartaria was Lost Part 6_files/68909_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/68909/68909_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/68909/68909_original.jpg)

**Russian:**

Острые кромки у камней видны на всех снимках. При этом мхом камни заросли тоже не очень сильно. Сплошного покрытия нетпрактически нигде.

**English:**

The sharp edges of the rocks are visible in all the pictures. The stones aren't overgrown with moss either. It's not practical to cover all this up everywhere.

<!-- Local
![](How Tartaria was Lost Part 6_files/1000000000000640000004B084D0A482.jpg#resized)
-->
<!--
![](./images/How Tartaria was Lost Part 6_files/1000000000000640000004B084D0A482.jpg#resized)
This is a repeat of https://ic.pics.livejournal.com/mylnikovdm/24488457/68761/68761_900.jpg
-->

<!-- Local
![](How Tartaria was Lost Part 6_files/1000000000000640000004B04487B861.jpg#resized)
-->

<!--
![](./images/How Tartaria was Lost Part 6_files/1000000000000640000004B04487B861.jpg#resized)
This is a repeat of 86909.900.jpg above
-->

<!-- Translator's note: the above short paragraph, along with two photographs, are in the zip file odt doc but not the web page
-->

**Russian:**

Отдельная тема, это наличие камней с ребристой поверхностью, которая напоминает стиральную доску недалёкого прошлого. Высказывалосьпредположение, что это могут быть следы оплавления камня высокой температурой во время взрыва. Но мы внимательно осмотрели обнаруженныеобразцы, поверхность не имеет следов плавления и соответствуют поверхности раскола. При этом совершенно непонятно при каких условияхгранит при раскалывании будет образовывать подобную структуру, но есть большие сомнения, что это следствие раскалывания из-за естественнойэрозии под воздействием воды и перепада температур. Камней с такой поверхностью встречается достаточно много. Мы не ставили перед собойцелью специально их искать, просто шли по маршруту, но даже при этом видели их больше десятка. Находили мы подобные камни и на склоне горыИремель, о котором я расскажу ниже.

**English:**

A separate theme is the presence of stones with a ribbed surface, which resemble the washing boards of the not so distant past. It has been suggested that these may be traces of high temperature melting of the stone during an explosion. But we looked closely at the samples and found the surface has no melting marks and matches the surface of the split. It is not clear under what conditions splitting will form such a structure, but there is great doubt that this is a consequence of the fracturing caused by natural erosion under the influence of water and temperature differences (thermal expansion/contraction). There are quite a few stones with such a surface. We did not set ourselves the goal to look for them specifically, we just walked the route, but even so we saw more than a dozen of them. We also found similar stones on the slope of Mount Iremel, which I will tell you about below.

<!-- Local
[![](How Tartaria was Lost Part 6_files/69261_900.jpg#clickable)](How Tartaria was Lost Part 6_files/69261_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/69261/69261_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/69261/69261_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/69515_900.jpg#clickable)](How Tartaria was Lost Part 6_files/69515_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/69515/69515_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/69515/69515_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/69645_900.jpg#clickable)](How Tartaria was Lost Part 6_files/69645_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/69645/69645_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/69645/69645_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/70068_900.jpg#clickable)](How Tartaria was Lost Part 6_files/70068_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/70068/70068_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/70068/70068_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/70304_900.jpg#clickable)](How Tartaria was Lost Part 6_files/70304_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/70304/70304_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/70304/70304_original.jpg)

**Russian:**

А так вблизи выглядят разрушенные скалы, покрытые сетью трещин и имеющиена изломах острые кромки.

**English:**

And so look, ruined rocks covered with a network of cracks that have sharp edges with fractures.

<!-- Local
[![](How Tartaria was Lost Part 6_files/70545_900.jpg#clickable)](How Tartaria was Lost Part 6_files/70545_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/70545/70545_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/70545/70545_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/70805_900.jpg#clickable)](How Tartaria was Lost Part 6_files/70805_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/70805/70805_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/70805/70805_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/71141_900.jpg#clickable)](How Tartaria was Lost Part 6_files/71141_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/71141/71141_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/71141/71141_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/71328_900.jpg#clickable)](How Tartaria was Lost Part 6_files/71328_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/71328/71328_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/71328/71328_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/71491_900.jpg#clickable)](How Tartaria was Lost Part 6_files/71491_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/71491/71491_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/71491/71491_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/71772_900.jpg#clickable)](How Tartaria was Lost Part 6_files/71772_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/71772/71772_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/71772/71772_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/72057_900.jpg#clickable)](How Tartaria was Lost Part 6_files/72057_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/72057/72057_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/72057/72057_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/72193_900.jpg#clickable)](How Tartaria was Lost Part 6_files/72193_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/72193/72193_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/72193/72193_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/72475_900.jpg#clickable)](How Tartaria was Lost Part 6_files/72475_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/72475/72475_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/72475/72475_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/72836_900.jpg#clickable)](How Tartaria was Lost Part 6_files/72836_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/72836/72836_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/72836/72836_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/72999_900.jpg#clickable)](How Tartaria was Lost Part 6_files/72999_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/72999/72999_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/72999/72999_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/73378_900.jpg#clickable)](How Tartaria was Lost Part 6_files/73378_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/73378/73378_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/73378/73378_original.jpg)

[http://www.youtube.com/watch?v=tutnlMZl2II](http://www.youtube.com/watch?v=tutnlMZl2II)

**Russian:**

Подобная картина наблюдается не только на хребте Зигальга. В 32 км от горы Большой Шелом находится ещё одна весьма популярная у туристов гора Иремель, являющейся второй по высоте на Южном Урале после Яман-Тау.

**English:**

Such a picture is not only observed on the Zigalg Ridge. 32 km from Big Shelom Mountain is another mountain Eremel, which is the second highest in the South Urals after Yaman Tau and which is very popular with tourists.

**Russian:**

Сам Иремель и все его склоны сплошь усыпаны курумами, которые выглядят точно так же, как и на Зигальге.

**English:**

Eremel itself and all its slopes are covered with kurums and fragmented boulders, which look exactly like those on Zigalg.

**Russian:**

Вот так выглядит подъём на Иремель.

**English:**

This is what it looks like to climb Eremel.

<!-- Local
[![](How Tartaria was Lost Part 6_files/73710_900.jpg#clickable)](How Tartaria was Lost Part 6_files/73710_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/73710/73710_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/73710/73710_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/73945_900.jpg#clickable)](How Tartaria was Lost Part 6_files/73945_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/73945/73945_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/73945/73945_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/74190_900.jpg#clickable)](How Tartaria was Lost Part 6_files/74190_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/74190/74190_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/74190/74190_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/74491_900.jpg#clickable)](How Tartaria was Lost Part 6_files/74491_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/74491/74491_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/74491/74491_original.jpg)

**Russian:**

Размеры камней, наличие острых кромок, уровеньзарастания мхом, практически в точности повторяют то, что мы видели на Зигальге. Обратите внимание на последний снимок, где весь склон, которыймы видим на заднем плане, засыпан такими же камнями, какие мы видим на переднем плане. Каким образом тут образовалось такое количество битогокамня? Ведь тут, как и в случае с Зигальгой и Большим Шеломом, высота и крутизна склонов не позволяет сходить каменным лавинам, которые смоглибы его наколоть.

**English:**

The size of the stones, the sharp edges, the level of moss growth, they are almost exactly the same as what we saw on Zigalga. Note the last picture, where the entire slope we see in the background is covered with the same stones as we see in the foreground. How did such a large number of boulders come into being here? After all, here, as in the case of Zigalga and Big Shelom, the height and steepness of the slopes does not enable the avalanching of stone such that it might fracture like this.

**Russian:**

Мало того, мы не видим возвышающихся скал, от которых могли бы под действием ветра, воды и температуры откалываться какие-либо части, хотькрупные, хоть мелкие. Поднявшись на вершину вы увидите лишь небольшие остатки скал, которые все покрыты трещинами, как будто по ним кто-тоударил гигантским молотом. На первых трёх фото очень хорошо видно, что слои вскалах идут под наклоном, как будто кто-то сильно ударил по горе сбоку. И именно с той стороны находится гора Большой Шелом, над которой былэпицентр взрыва. То, что мы наблюдаем, это последствия мощной ударной волны, которая пришла как раз с той стороны.

**English:**

Not only that, we do not see towering rocks from which any parts, whether large or small, could break off under the influence of wind, water and temperature. When you climb to the top you will see only small remnants of rocks, all of which are covered with cracks as if someone had hit them with a giant hammer. The first three photos show very clearly that the layers of broken rocks slope as if someone had hit the mountain hard from the side. And this is the side that faces Big Shel mountain, above which was the epicenter of the explosion. What we're seeing are the consequences of a powerful shock wave that came from that direction.

<!-- Local
[![](How Tartaria was Lost Part 6_files/74727_900.jpg#clickable)](How Tartaria was Lost Part 6_files/74727_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/74727/74727_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/74727/74727_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/74769_900.jpg#clickable)](How Tartaria was Lost Part 6_files/74769_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/74769/74769_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/74769/74769_original.jpg)

<!-- Local
![](How Tartaria was Lost Part 6_files/1000000000000640000004B0C308B217.jpg#resized)
-->

![](./images/How Tartaria was Lost Part 6_files/1000000000000640000004B0C308B217.jpg#resized)

<!-- Local
[![](How Tartaria was Lost Part 6_files/75355_900.jpg#clickable)](How Tartaria was Lost Part 6_files/75355_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/75355/75355_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/75355/75355_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/75147_900.jpg#clickable)](How Tartaria was Lost Part 6_files/75147_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/75147/75147_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/75147/75147_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/75742_900.jpg#clickable)](How Tartaria was Lost Part 6_files/75742_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/75742/75742_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/75742/75742_original.jpg)

**Russian:**

Как я уже говорил, если вы решите совершить восхождение на Иремель, то сочень большой вероятностью сможете по пути обнаружить камни, поверхность которых похожа на старые стиральные доски. Сделать это не сложно,поскольку таких камней на склоне Иремеля встречается достаточно много.

**English:**

As I said before, if you decide to climb Eremel, it is very likely that you will be able to find stones on your way that look like old washing boards. It's not difficult to do that because there are quite a few of these stones on its slopes.

<!-- Local
[![](How Tartaria was Lost Part 6_files/75939_900.jpg#clickable)](How Tartaria was Lost Part 6_files/75939_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/75939/75939_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/75939/75939_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/76162_900.jpg#clickable)](How Tartaria was Lost Part 6_files/76162_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/76162/76162_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/76162/76162_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/76446_900.jpg#clickable)](How Tartaria was Lost Part 6_files/76446_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/76446/76446_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/76446/76446_original.jpg)

**Russian:**

Непосредственно рядом с Иремелем есть место, где оченьхорошо видны последствия прихода мощной ударной волны со стороны Большого Шелома. Это небольшой пологий склон, который примыкает кдолине, где большинство туристов любят вставать на ночлег. Честно скажу, когда мы ходили к Иремелью в 2010 году, никто из нас ещё и понятия неимел, что же мы такое на самом деле видим, но уже тогда было очевидно, что это нечто очень необычное. Дело в том, что из всего этого склона,как иголки из дикобраза, торчат маленькие и большие камни. При этом часть из них, как на следующих двух фото, с этого же склона, но какая-тосила вздыбила их почти вертикально.

**English:**

Directly next to Eremel, there is a place where the consequences of a powerful shock wave from Big Shelom can be clearly seen. It is a small gentle slope that adjoins the Cdoline, where most tourists like to spend the night. To be honest, when we went to Eremel in 2010, none of us had any idea what we would really see, but it was already clear that this was something very unusual. The fact is that small and large rocks stick out all over this slope like porcupine needles. At the same time, some of them, as in the next two photos, are from the same slope, but some kind of force has moved them almost to the vertical.

<!-- Local
[![](How Tartaria was Lost Part 6_files/76782_900.jpg#clickable)](How Tartaria was Lost Part 6_files/76782_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/76782/76782_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/76782/76782_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/76925_900.jpg#clickable)](How Tartaria was Lost Part 6_files/76925_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/76925/76925_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/76925/76925_original.jpg)

**Russian:**

И таких странных скальных выходов на этом склоне множество. Вот ещё один.

**English:**

And there are so many strange rocky outcropes on this slope. Here's another one alone.

<!-- Local
[![](How Tartaria was Lost Part 6_files/77280_900.jpg#clickable)](How Tartaria was Lost Part 6_files/77280_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/77280/77280_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/77280/77280_original.jpg)

**Russian:**

Но кроме больших камней, на этом склоне наблюдается также очень много средних и маленьких, причём часть из них, как мывыяснили, не с этого склона, поскольку они воткнуты в грунт, как нож в масло. Но тогда мы ещё не могли объяснить, какая же сила могла этосделать. Склон справа на заднем плане, это начало подъёма не Иремель, наш лагерь стоит в ложбинке в лесу, а мы пошли на этот склон, чтобыполюбоваться на закат Солнца.

**English:**

But apart from the large rocks, there are also many medium and small rocks on this slope, and some of them, we discovered, are not from this slope, because they are stuck in the ground like a knife in butter. But at that time we could not yet explain what kind of force could have done this. By the way, the slope on the right in the background is not Eremel; our camp was in a hollow in the woods, and we went to this slope to see the sunset.

<!-- Local
[![](How Tartaria was Lost Part 6_files/77435_900.jpg#clickable)](How Tartaria was Lost Part 6_files/77435_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/77435/77435_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/77435/77435_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/77671_900.jpg#clickable)](How Tartaria was Lost Part 6_files/77671_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/77671/77671_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/77671/77671_original.jpg)

**Russian:**

На этом снимке хорошо видно, что слои у всех камней расположены под разным углом. То есть, это явно не естественное их положение. Но,поскольку мы стоим практически на вершине склона, то падать этим камням тоже неоткуда. То есть, естественных причин, которые могли бы заставитьих принять подобное положение, не существует.

**English:**

This image clearly shows that the layers of all the stones are at different angles. That is, it is clearly not their natural position. But since we're standing almost at the top of the slope, these stones have nowhere to fall either. That is, there are no natural reasons that would force them to accept such a position.

<!-- Local
![](How Tartaria was Lost Part 6_files/1000000000000640000004B0C239738E.jpg#resized)
-->

![](./images/How Tartaria was Lost Part 6_files/1000000000000640000004B0C239738E.jpg#resized)

<!-- alt link remains in webpage text - Respect author's half removal of this image: -->
<!-- Local
[![](How Tartaria was Lost Part 6_files/77863_900.jpg#clickable)](How Tartaria was Lost Part 6_files/77863_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/77863/77863_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/77863/77863_original.jpg)

**Russian:**

А вот это один из воткнутых камней, который сюда прилетел. Он не с этогосклона. Обратите внимание, что камень левее его лежит вполне себе горизонтально, а не торчит параллельно воткнутому. Мы осмотрелинесколько подобных камней, они именно воткнуты в грунт.

**English:**

And this is one of the stones that flew in here. It's not from this slope. Notice that the stone to the left of it lies quite horizontally, not sticking out parallel to the one that's stuck. We've looked at a number of such stones, they're just stuck in the ground.

<!-- Local
[![](How Tartaria was Lost Part 6_files/78265_900.jpg#clickable)](How Tartaria was Lost Part 6_files/78265_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/78265/78265_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/78265/78265_original.jpg)

**Russian:**

Вот ещё один образец камня, который воткнут в щель между другим камнями.И подобных камней дальше по склону виднеется ещё множество.

**English:**

Here's another sample of a stone that's stuck in the gap between the other stones. And there's a lot of similar stones further down the slope.

<!-- Local
[![](How Tartaria was Lost Part 6_files/78438_900.jpg#clickable)](How Tartaria was Lost Part 6_files/78438_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/78438/78438_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/78438/78438_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/78677_900.jpg#clickable)](How Tartaria was Lost Part 6_files/78677_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/78677/78677_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/78677/78677_original.jpg)

**Russian:**

При этом ещё раз хочу обратить внимание, что углы у этих торчащих камней разные, что говорит о том, что они не моглиобразоваться из слоёв гранита, который находится в основании данного склона. Иначе бы все они торчали под одни и тем же углом,соответствующим углу наклона слоистой структуры минерала.

**English:**

At the same time, I would like to draw attention once again to the fact that the angles of these protruding stones are different, which suggests that they could not be formed from the layers of granite at the base of this slope. Otherwise they would all stick out at the same angle, corresponding to the angle of slope of the layered structure of the granite.

**Russian:**

Солнце садиться прямо над горой Большой Шелом. Возможно, что примерно так же выглядела с этого места вспышка термоядерного взрыва двести летназад. Я привел это фото, чтобы было понятно, что со склона, на котором мы наблюдаем вздыбленные и воткнутые камни, была прямая видимость наэпицентр взрыва. А те камни, которые воткнуты в склон, скорее всего принесло ударной волной с того склона, который чернеет непосредственноперед нами, расстояние до него около полутора километров.

**English:**

The sun is setting right above Mount Big Shel. It's possible that a fusion of a thermonuclear explosion two hundred years ago might have looked the same from this place. I brought this photo to make it clear that there was direct visibility of the epicenter of the explosion from the slope where we see the stones sticking out of the ground. And those stones that were stuck into the slope were most likely brought by a shock wave from the shadowed slope directly in front of us in this photograph - a distance of about one and a half kilometres.

<!-- Local
[![](How Tartaria was Lost Part 6_files/78936_900.jpg#clickable)](How Tartaria was Lost Part 6_files/78936_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/78936/78936_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/78936/78936_original.jpg)

**Russian:**

Следующее фото тоже очень показательно. Солнце светитточно со стороны эпицентра взрыва и точно освещает поверхности вздыбленных камней, которые повёрнуты в его сторону. Если вы вернётесь ипосмотрите предыдущие снимки ещё раз, то по положению света и тени на камнях легко увидите, откуда пришла сила, которая их вздыбила.

**English:**

The next photo is also very revealing. The sun is shining from the epicenter of the explosion and accurately illuminates the surfaces of the blasted rocks that are turned in its direction. If you go back and look at the previous pictures again, you can easily see from the position of light and shadow on the stones from where came the force that caused them to shatter.

<!-- Local
[![](How Tartaria was Lost Part 6_files/79228_900.jpg#clickable)](How Tartaria was Lost Part 6_files/79228_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/79228/79228_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/79228/79228_original.jpg)

**Russian:**

Причём проризошло это не очень давно, поскольку на всех этих вздыбленных и воткнутых камнях отлично видны острые грани. Чтобынесведущим людям было понятно, как должны выглядеть камни и горы, которые давно подвергаются действию ветра, воды и перепадов температуры,ещё несколько снимков, на которых это отлично видно.

**English:**

And it did not happen very long ago, because all these tumbled and stuck-in-th-ground stones have perfectly visible sharp edges. To make people aware of what stones and mountains that have long been exposed to the action of wind, water and temperature changes, *should* look like, here are a few more pictures where this is perfectly visible.

<!-- Local
[![](How Tartaria was Lost Part 6_files/79946_900.jpg#clickable)](How Tartaria was Lost Part 6_files/79946_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/79946/79946_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/79946/79946_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/80295_900.jpg#clickable)](How Tartaria was Lost Part 6_files/80295_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/80295/80295_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/80295/80295_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/80614_900.jpg#clickable)](How Tartaria was Lost Part 6_files/80614_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/80614/80614_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/80614/80614_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/80648_900.jpg#clickable)](How Tartaria was Lost Part 6_files/80648_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/80648/80648_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/80648/80648_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 6_files/81091_900.jpg#clickable)](How Tartaria was Lost Part 6_files/81091_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/81091/81091_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/81091/81091_original.jpg)

[http://www.youtube.com/watch?v=tutnlMZl2II](http://www.youtube.com/watch?v=tutnlMZl2II)

**Russian:**

Именно так должны выглядеть старые скалы и камни, укоторых нет никаких острых кромок. Все они сглажены ветром и водой. Согласитесь, что это очень сильно отличается от того, что мы видели и наЗигальге, и в окрестностях Иремеля. 

Я подробно описал именно это место, поскольку сам бывал там много раз.


**English:**

That's what old rocks and rocks with no sharp edges should look like. They're all smoothed out by wind and water. You must agree that this is very different from what we have seen both at Zigalge and in the vicinity of Eremel.

I have described this place in detail because I have been there many times myself.

**Russian:**

Но подобные курумы, причём курмы именно молодые, с камнями имеющимиострые грани, встречаются на Урале повсеместно. Аналогичные россыпи есть в районе Златоуста в национальном парке Таганай. Срезанная плоскаявершина у самой высокой горы Южного Урала --- Яман-Тау, окрестности которой тоже усыпаны битым канем, аналогичным наблюдаемому на Зигальге.Много подобных россыпей на и на Среднем, и на Северном Урале. А это означает, что одним только взрывом над Большим Шеломом дело неограничилось. Уральские горы бомбили нещадно во многих местах, и следов тому на самом деле множество, нужно только понимать, что искать,поскольку круглых воронок, которые возникают на равнинах, в горах не образуется из-за очень пересечённого рельефа, который сильно искажаетударную волну. Но вот недавно наколотый камень в гигантских количествах и разрушенные взрывами скалы никуда не спрячешь.

**English:**

But such kurums - and with stones having sharp edges, they are young kurums - are everywhere in the Urals. Similar placers are found in the Zlatoust area in the Taganai National Park. A cut, flat-lying placer can be found near the highest mountain in the South Urals - Yaman-Tau, the surroundings of which are also buried with a broken rocks similar to the ones observed in Zigalga. There are many similar placers in both the Middle and North Urals. Which means that the bombing was not limited to Big Shelom alone. The Ural Mountains were mercilessly bombed in many places, and there are many traces of this. You just have to understand what to look for. For example, the round craters that appear on the plains do not form in the mountains because the very rugged landscape relief distorts the shock waves. However, recently moved stones in gigantic quantities and rocks fractured by explosions can not hide anywhere, including mountainous land.

**Russian:**

Это последняя часть, в которой я привожу собранные факты о катастрофе,которая привела к гибели огромной империи, указанной на старых картах как Тартария. В следующей, заключительной части, я попытаюсь сделатьобщий вывод и представить своё видение тех событий, их причин, заказчиков и исполнителей.

**English:**

This is the last part where I bring you the facts about the catastrophe that led to the death of a huge empire, listed on the old maps as Tartaria. In the next and final part, I will try to make a general conclusion and present my vision of those events, their causes, their customers, and their performers.

**Russian:**

Окончание следует...Окончание следует...

**English:**

The end follows... The end follows...

© All rights reserved. The original author retains ownership and rights.

