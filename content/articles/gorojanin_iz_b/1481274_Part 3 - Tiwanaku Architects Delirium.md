title: Mysterious Mesoamerica Part 3 - Tiwanaku Architect's Delirium - Is this real or fake?
date: 2018-10-28 04:52
modified: 2022-09-17 12:28:55
category:
tags: forged history; Tiwanaku
slug:
authors: Anton Sizykh
from: https://eto-fake.livejournal.com/1481274.html
originally: 1481274.html
local: 1481274_files/
summary: In general, the details of the heritage of humanity and UNESCO are better not being filmed. The nature of these legacies is bound to come out.
status: published

#### Translated from:

[https://eto-fake.livejournal.com/1481274.html](https://eto-fake.livejournal.com/1481274.html)

<!--originally from: 
[Продолжение. Часть 2 [здесь](https://911tm.livejournal.com/27969.html)
-->

**Russian:**

Отсюда: [1](https://gorojanin-iz-b.livejournal.com/83036.html?thread=17757276#t17757276), [2](https://gorojanin-iz-b.livejournal.com/83036.html?page=4)]

**English:**

From here: [1](https://gorojanin-iz-b.livejournal.com/83036.html?thread=17757276#t17757276), [2](https://gorojanin-iz-b.livejournal.com/83036.html?page=4)]

<!-- Local
![](1481274_files/GPnGAoU.jpg#resized)
-->

![](https://i.imgur.com/GPnGAoU.jpg#resized)

<!-- Local
![](1481274_files/S7sMiOU.jpg#resized)
-->

![](https://i.imgur.com/S7sMiOU.jpg#resized)

**Russian:**

Величественный сакральный центр древних инков Тиуанако. По данным академической науки построен за 500 лет до н.э., альтернативной - за 15 000 лет до н.э.

**English:**

The majestic sacred centre of the ancient Incas of Tiwanaku. According to academic science it was built 500 years BC, alternative science says 15,000 years BC.

**Russian:**

Обыкновенный непромес, каркас, зауряднейшее литье на каркасе.

**English:**

Ordinary unprocessed frame, mediocre casting on the frame.

<!-- Local
![](1481274_files/zZwl3YE.jpg#resized)
-->

![](https://i.imgur.com/zZwl3YE.jpg#resized)

<!-- Local
![](1481274_files/cXEYSXC.jpg#resized)
-->

![](https://i.imgur.com/cXEYSXC.jpg#resized)

<!-- Local
[![](1481274_files/kAr7K80.jpg#resized)](1481274_files/kAr7K80.jpg)
-->

[![](https://i.imgur.com/kAr7K80.jpg#clickable)](https://i.imgur.com/kAr7K80.jpg)

**Russian:**

Памятник из разряда ТОП. В местах, где подрассыпались грани, есть вопросы?

**English:**

Top of the line monument. Any questions in the areas where the edges are crumbled?

<!-- Local
![](1481274_files/ka0TJFa.jpg#resized)
-->

![](https://i.imgur.com/ka0TJFa.jpg#resized)

<!-- Local
![](1481274_files/sp3K9Ie.jpg#resized)
-->

![](https://i.imgur.com/sp3K9Ie.jpg#resized)

<!-- Local
[![](1481274_files/gaCVfJH.jpg#resized)](1481274_files/gaCVfJH.jpg)
-->

[![](https://i.imgur.com/gaCVfJH.jpg#clickable)](https://i.imgur.com/gaCVfJH.jpg)

**Russian:**

То ли арматуру не завезли, то ли высокохудожественный творческий замысел такой, чтобы типа "кровь из скульптур богов шла", но это трубки, свернутые из листового материала. В этих местах и выпало. Не учли, что полая арматура - влагонакопитель. Из элемента укрепляющего стала разрушающим))

**English:**

The fittings were not delivered, or it was a highly artistic creative idea so that "the blood came out of the sculptures of the gods", but these were tubes rolled up from a sheet material. That's where it fell out. They did not take into account that the hollow reinforcement was a moisture accumulator. It turned from a reinforcing element into a destructive one. :)

<!-- Local
![](1481274_files/6A1FmEJ.jpg#resized)
-->

![](https://i.imgur.com/6A1FmEJ.jpg#resized)

<!-- Local
![](1481274_files/lCIIFA8.jpg#resized)
-->

![](https://i.imgur.com/lCIIFA8.jpg#resized)

<!-- Local
[![](1481274_files/zsXyWjW.jpg#resized)](1481274_files/zsXyWjW.jpg)
-->

[![](https://i.imgur.com/zsXyWjW.jpg#clickable)](https://i.imgur.com/zsXyWjW.jpg)

<!-- Local
![](1481274_files/FFGlML7.jpg#resized)
-->

![](https://i.imgur.com/FFGlML7.jpg#resized)

<!-- Local
[![](1481274_files/qdnLtBK.jpg#resized)](1481274_files/qdnLtBK.jpg)
-->

[![](https://i.imgur.com/qdnLtBK.jpg#clickable)](https://i.imgur.com/qdnLtBK.jpg)

**Russian:**

Еще трубчатое армирование. Думаю, что все-таки кровь из скульптур богов. С тех психопатов, которые ваяли древнеинкские памятники Тиуанако, станется...

**English:**

More tubular reinforcement. I'm thinking blood from the sculptures of the gods after all. Those psychopaths who sculpted the ancient Incan monuments of Tiwanaku would be...

<!-- Local
![](1481274_files/ZNXtZGu.jpg#resized)
-->

![](https://i.imgur.com/ZNXtZGu.jpg#resized)

<!-- Local
![](1481274_files/b2YrKBt.jpg#resized)
-->

![](https://i.imgur.com/b2YrKBt.jpg#resized)

<!-- Local
[![](1481274_files/xShHNzp.jpg#resized)](1481274_files/xShHNzp.jpg)
-->

[![](https://i.imgur.com/xShHNzp.jpg#clickable)](https://i.imgur.com/xShHNzp.jpg)

**Russian:**

Прокладывались трубочки динамично и на глазок, как и любые каркасы.

**English:**

The tubes were laid dynamically and by eye, just like any framework.

<!-- Local
[![](1481274_files/WonoRnG.jpg#resized)](1481274_files/WonoRnG.jpg)
-->

[![](https://i.imgur.com/WonoRnG.jpg#clickable)](https://i.imgur.com/WonoRnG.jpg)

<!-- Local
[![](1481274_files/YCjJeeK.jpg#resized)](1481274_files/YCjJeeK.jpg)
-->

[![](https://i.imgur.com/YCjJeeK.jpg#clickable)](https://i.imgur.com/YCjJeeK.jpg)

**Russian:**

Двужильный провод. Каркас связывали. Без каркаса такая дура долго стоять не будет. И второй фрагмент - явно тоже что-то из мира проводов.

**English:**

Double stranded wire. The frame was tied together. Without the frame, this stupid thing wouldn't stand for long. And the second fragment is obviously something from the wire world, too.

<!-- Local
![](1481274_files/tyV83ou.jpg#resized)
-->

![](https://i.imgur.com/tyV83ou.jpg#resized)

<!-- Local
![](1481274_files/cUH2xfP.jpg#resized)
-->

![](https://i.imgur.com/cUH2xfP.jpg#resized)

<!-- Local
[![](1481274_files/D4iyZcK.jpg#resized)](1481274_files/D4iyZcK.jpg)
-->

[![](https://i.imgur.com/D4iyZcK.jpg#clickable)](https://i.imgur.com/D4iyZcK.jpg)

**Russian:**

Похоже на карандаш или авторучку древнеинков, и еще на дюбель, но пластиковые в начале 20 века не выпускались.

**English:**

It looks like a pencil or ancient pen, and also a dowel, but plastic ones weren't produced in the early 20th century.

<!-- Local
![](1481274_files/6Z1ASxb.jpg#resized)
-->

![](https://i.imgur.com/6Z1ASxb.jpg#resized)

<!-- Local
[![](1481274_files/cUjkcUz.jpg#resized)](1481274_files/cUjkcUz.jpg)
-->

[![](https://i.imgur.com/cUjkcUz.jpg#clickable)](https://i.imgur.com/cUjkcUz.jpg)

**Russian:**

Здесь явная полоса постороннего материала и какой-то элемент крепежа. Другая сторона истукана.

**English:**

There is an obvious strip of foreign material and some kind of fastening element. The other side of the statue.

<!-- Local
![](1481274_files/cK3Ff0d.jpg#resized)
-->

![](https://i.imgur.com/cK3Ff0d.jpg#resized)

<!-- Local
![](1481274_files/bQlcHDH.jpg#resized)
-->

![](https://i.imgur.com/bQlcHDH.jpg#resized)

<!-- Local
[![](1481274_files/Fc0fYyT.jpg#resized)](1481274_files/Fc0fYyT.jpg)
-->

[![](https://i.imgur.com/Fc0fYyT.jpg#clickable)](https://i.imgur.com/Fc0fYyT.jpg)

**Russian:**

Строители отлично знают, что без каркаса эти дуры долго не протянут.

**English:**

Builders know very well that without framing, this dumb structure wouldn't last long.

<!-- Local
![](1481274_files/t3Aia0q.jpg#resized)
-->

![](https://i.imgur.com/t3Aia0q.jpg#resized)

<!-- Local
[![](1481274_files/DKRWNPi.jpg#resized)](1481274_files/DKRWNPi.jpg)
-->

[![](https://i.imgur.com/DKRWNPi.jpg#clickable)](https://i.imgur.com/DKRWNPi.jpg)

**Russian:**

Великое дело - качественная съемка. Спасибо группе Склярова! Посторонний винт или болт.

**English:**

A quality photograph is a great thing. Thanks to Sklyarov's group! An extraneous screw or bolt.

<!-- Local
![](1481274_files/MMBHhhq.jpg#resized)
-->

![](https://i.imgur.com/MMBHhhq.jpg#resized)

<!-- Local
[![](1481274_files/JrtZwqW.jpg#resized)](1481274_files/JrtZwqW.jpg)
-->

[![](https://i.imgur.com/JrtZwqW.jpg#clickable)](https://i.imgur.com/JrtZwqW.jpg)

**Russian:**

Не может быть, чтобы в такой дуроформе не было ничего постороннего. Посторонний винт.

**English:**

There's no way there's nothing extraneous in a duroform like this. A foreign screw.

<!-- Local
![](1481274_files/XNvfyoE.jpg#resized)
-->

![](https://i.imgur.com/XNvfyoE.jpg#resized)

<!-- Local
![](1481274_files/YM1Z4wf.jpg#resized)
-->

![](https://i.imgur.com/YM1Z4wf.jpg#resized)

<!-- Local
[![](1481274_files/ldYMwnr.jpg#resized)](1481274_files/ldYMwnr.jpg)
-->

[![](https://i.imgur.com/ldYMwnr.jpg#clickable)](https://i.imgur.com/ldYMwnr.jpg)

**Russian:**

Еще один культовый объект. Прославленные Ворота Солнца. Камушки бута, как водится, не показывают. Как и основание с петелькой каркаса.

**English:**

Another iconic site. The famous Gate of the Sun. The pebbles of the butte, as usual, do not show. Neither does the base with the frame hinge.

<!-- Local
![](1481274_files/7aQw1OV.jpg#resized)
-->

![](https://i.imgur.com/7aQw1OV.jpg#resized)

<!-- Local
![](1481274_files/NYcdg9j.jpg#resized)
-->

![](https://i.imgur.com/NYcdg9j.jpg#resized)

<!-- Local
[![](1481274_files/TB8UUxv.jpg#resized)](1481274_files/TB8UUxv.jpg)
-->

[![](https://i.imgur.com/TB8UUxv.jpg#clickable)](https://i.imgur.com/TB8UUxv.jpg)

**Russian:**

Понятно, что посторонние железяки надо искать в местах сломов. Естественно, нашлось.

**English:**

It is clear that extraneous iron should be looked for where the breaks are. Naturally, we found some.

<!-- Local
![](1481274_files/Hvf6sRz.jpg#resized)
-->

![](https://i.imgur.com/Hvf6sRz.jpg#resized)

<!-- Local
[![](1481274_files/OKM9fYw.jpg#resized)](1481274_files/OKM9fYw.jpg)
-->

[![](https://i.imgur.com/OKM9fYw.jpg#clickable)](https://i.imgur.com/OKM9fYw.jpg)

**Russian:**

Ну каким анунахам или инкам придет в голову высекать такую дуру из гранита, которую и использовать-то незнамо как... Разумеется, каркас, естественно, местами торчит.

**English:**

What kind of Anunnakis or Incas would carve such a dumb thing out of granite, which they don't know how to use... Of course, the reinforcement sticks out in places.

<!-- Local
![](1481274_files/j5hpmq1.jpg#resized)
-->

![](https://i.imgur.com/j5hpmq1.jpg#resized)

<!-- Local
![](1481274_files/IEUJ3Tq.jpg#resized)
-->

![](https://i.imgur.com/IEUJ3Tq.jpg#resized)

<!-- Local
![](1481274_files/0pOcNHC.jpg#resized)
-->

![](https://i.imgur.com/0pOcNHC.jpg#resized)

<!-- Local
[![](1481274_files/ZY2ATjX.jpg#resized)](1481274_files/ZY2ATjX.jpg)
-->

[![](https://i.imgur.com/ZY2ATjX.jpg#clickable)](https://i.imgur.com/ZY2ATjX.jpg)

**Russian:**

Выточить, вырубить и даже отлить на месте это колесо невозможно. Это лепка на каркас. Оно и видно местами.

**English:**

It is impossible to chisel, carve or even cast this wheel in place. It's a molding on a wire-frame. You can see it in places.

<!-- Local
![](1481274_files/Fkquvdd.jpg#resized)
-->

![](https://i.imgur.com/Fkquvdd.jpg#resized)

<!-- Local
![](1481274_files/R7d7DJX.jpg#resized)
-->

![](https://i.imgur.com/R7d7DJX.jpg#resized)

<!-- Local
![](1481274_files/OzWIQCR.jpg#resized)
-->

![](https://i.imgur.com/OzWIQCR.jpg#resized)

<!-- Local
![](1481274_files/RNbTbSr.jpg#resized)
-->

![](https://i.imgur.com/RNbTbSr.jpg#resized)

<!-- Local
[![](1481274_files/rRosd8w.jpg#resized)](1481274_files/rRosd8w.jpg)
-->

[![](https://i.imgur.com/rRosd8w.jpg#clickable)](https://i.imgur.com/rRosd8w.jpg)

**Russian:**

Самое интересное за колесом, но оно не заинтересовало снимальщиков.

**English:**

The most interesting thing is behind the wheel, but it didn't interest the filmmakers.

<!-- Local
![](1481274_files/UEKvamr.jpg#resized)
-->

![](https://i.imgur.com/UEKvamr.jpg#resized)

**Russian:**

Чтобы сделать вывод о всей партии однотипных изделий из одной формы, достаточно увидеть посторонний предмет в одной. А он тут на самом видном месте.

**English:**

To conclude about an entire batch of the same type of products from the same mold, it's enough to see a foreign object in one. And it's in the most prominent place here.

<!-- Local
![](1481274_files/XEIWJyR.jpg#resized)
-->

![](https://i.imgur.com/XEIWJyR.jpg#resized)

<!-- Local
[![](1481274_files/Xhw12LV.jpg#resized)](1481274_files/Xhw12LV.jpg)
-->

[![](https://i.imgur.com/Xhw12LV.jpg#clickable)](https://i.imgur.com/Xhw12LV.jpg)

**Russian:**

Больше всего это похоже на патрон -

**English:**

It looks more like a cartridge than anything else.

<!-- Local
![](1481274_files/LiuJl0J.jpg#resized)
-->

![](https://i.imgur.com/LiuJl0J.jpg#resized)

**Russian:**

а это - на ключ от английского замка, номерок гардероба... только край какой-то подрубленный.

**English:**

And this is an English lock key, a wardrobe number... only the edge is kind of chopped off.

<!-- Local
![](1481274_files/PvVlSme.jpg#resized)
-->

![](https://i.imgur.com/PvVlSme.jpg#resized)

<!-- Local
[![](1481274_files/UqozSLT.jpg#resized)](1481274_files/UqozSLT.jpg)
-->

[![](https://i.imgur.com/UqozSLT.jpg#clickable)](https://i.imgur.com/UqozSLT.jpg)

**Russian:**

Вот это четко опознаваемый предмет - пуговица, военного образца.

**English:**

This is a clearly identifiable item: a military issue button.

<!-- Local
![](1481274_files/qisOzz4.jpg#resized)
-->

![](https://i.imgur.com/qisOzz4.jpg#resized)

<!-- Local
[![](1481274_files/0M8UWOo.jpg#resized)](1481274_files/0M8UWOo.jpg)
-->

[![](https://i.imgur.com/0M8UWOo.jpg#clickable)](https://i.imgur.com/0M8UWOo.jpg)

**Russian:**

Какие тут сомнения, и в чем? В древнеинкском армировании нет сомнений. Наверху петелька, внизу штырь.

**English:**

What doubts are there, and about what? There's no doubt about the ancient Incan reinforcement. There's a hinge on top, a pin on the bottom.

<!-- Local
![](1481274_files/mUXEQGE.jpg#resized)
-->

![](https://i.imgur.com/mUXEQGE.jpg#resized)

<!-- Local
![](1481274_files/N9pPwMc.jpg#resized)
-->

![](https://i.imgur.com/N9pPwMc.jpg#resized)

<!-- Local
[![](1481274_files/jU07wRe.jpg#resized)](1481274_files/jU07wRe.jpg)
-->

[![](https://i.imgur.com/jU07wRe.jpg#clickable)](https://i.imgur.com/jU07wRe.jpg)

**Russian:**

Поближе эта часть. Славный гранит!

**English:**

Get a closer look at this part. Nice granite!

<!-- Local
![](1481274_files/CKc66d2.jpg#resized)
-->

![](https://i.imgur.com/CKc66d2.jpg#resized)

<!-- Local
![](1481274_files/X2etTAN.jpg#resized)
-->

![](https://i.imgur.com/X2etTAN.jpg#resized)

<!-- Local
[![](1481274_files/9THaHAT.jpg#resized)](1481274_files/9THaHAT.jpg)
-->

[![](https://i.imgur.com/9THaHAT.jpg#clickable)](https://i.imgur.com/9THaHAT.jpg)

**Russian:**

Отсюда красиво -

**English:**

It's beautiful from here.

<!-- Local
[![](1481274_files/c1mYRX5.jpg#resized)](1481274_files/c1mYRX5.jpg)
-->

[![](https://i.imgur.com/c1mYRX5.jpg#clickable)](https://i.imgur.com/c1mYRX5.jpg)

**Russian:**

А обратная сторона ниже критики.

**English:**

And the flip side is beyond criticism.

<!-- Local
![](1481274_files/EDjscMW.jpg#resized)
-->

![](https://i.imgur.com/EDjscMW.jpg#resized)

<!-- Local
![](1481274_files/g31N1n1.jpg#resized)
-->

![](https://i.imgur.com/g31N1n1.jpg#resized)

<!-- Local
![](1481274_files/4iSUQbK.jpg#resized)
-->

![](https://i.imgur.com/4iSUQbK.jpg#resized)

<!-- Local
[![](1481274_files/LGn00FC.jpg#resized)](1481274_files/LGn00FC.jpg)
-->

[![](https://i.imgur.com/LGn00FC.jpg#clickable)](https://i.imgur.com/LGn00FC.jpg)

**Russian:**

Изделие крепче, когда заливается сетка. Не арматура, не мусор. Это правильное технологическое решение!

**English:**

The product is stronger when the mesh is poured. Not rebar, not trash. This is the right technological solution!

<!-- Local
![](1481274_files/sh2NOor.jpg#resized)
-->

![](https://i.imgur.com/sh2NOor.jpg#resized)

<!-- Local
[![](1481274_files/cABorB9.jpg#resized)](1481274_files/cABorB9.jpg)
-->

[![](https://i.imgur.com/cABorB9.jpg#clickable)](https://i.imgur.com/cABorB9.jpg)

**Russian:**

Данное изделие любят постить альтернатисты. Как пример совершенства технологии анунахов.

**English:**

The alternative-ists love to post this product. As an example of Anunnaki technological perfection.

<!-- Local
[![](1481274_files/twAo7Gs.jpg#resized)](1481274_files/twAo7Gs.jpg)
-->

[![](https://i.imgur.com/twAo7Gs.jpg#clickable)](https://i.imgur.com/twAo7Gs.jpg)

**Russian:**

Стесняются показывать след и дырки от планки эпохи штампованных стальных изделий, ну и по мелочи кое-что.

**English:**

Shy to show trace and holes from the bar from the era of stamped steel products, well, and some minor things.

<!-- Local
![](1481274_files/jMuhlrV.jpg#resized)
-->

![](https://i.imgur.com/jMuhlrV.jpg#resized)

<!-- Local
![](1481274_files/uylFAqF.jpg#resized)
-->

![](https://i.imgur.com/uylFAqF.jpg#resized)

<!-- Local
![](1481274_files/085TSMU.jpg#resized)
-->

![](https://i.imgur.com/085TSMU.jpg#resized)

**Russian:**

Очень хорошая ануначья штамповка! Только с земными железяками.

**English:**

A very good Anunnaki stampede! Only with earthly iron.

<!-- Local
![](1481274_files/t2H3yFr.jpg#resized)
-->

![](https://i.imgur.com/t2H3yFr.jpg#resized)

<!-- Local
[![](1481274_files/HtR5mZL.jpg#resized)](1481274_files/HtR5mZL.jpg)
-->

[![](https://i.imgur.com/HtR5mZL.jpg#clickable)](https://i.imgur.com/HtR5mZL.jpg)

<!-- Local
[![](1481274_files/j1boOuZ.jpg#resized)](1481274_files/j1boOuZ.jpg)
-->

[![](https://i.imgur.com/j1boOuZ.jpg#clickable)](https://i.imgur.com/j1boOuZ.jpg)

**Russian:**

Главное для наследия ЮНЕСКО и человечества - оно должно быть сделано на совесть и крепким. Вот этому армированному изделию даже атомная война не страшна!

**English:**

The main thing for the heritage of UNESCO and humanity - it must be made with care and durability. Even an atomic war is not a problem for this reinforced product!

<!-- Local
![](1481274_files/OHNnUtJ.jpg#resized)
-->

![](https://i.imgur.com/OHNnUtJ.jpg#resized)

<!-- Local
[![](1481274_files/hP6KexM.jpg#resized)](1481274_files/hP6KexM.jpg)
-->

[![](https://i.imgur.com/hP6KexM.jpg#clickable)](https://i.imgur.com/hP6KexM.jpg)

**Russian:**

Тому не надо за тайнами працивилизаций ходить, у кого эти тайны под ногами. Гвоздь обыкновенный, ржавый, 20 век.

**English:**

One does not need to go for the secrets of the pre-civilizations if they have these secrets under their feet. The nail is ordinary, rusty, 20th century.

<!-- Local
![](1481274_files/lLeMsYn.jpg#resized)
-->

![](https://i.imgur.com/lLeMsYn.jpg#resized)

<!-- Local
[![](1481274_files/ZzYZWPU.jpg#resized)](1481274_files/ZzYZWPU.jpg)
-->

[![](https://i.imgur.com/ZzYZWPU.jpg#clickable)](https://i.imgur.com/ZzYZWPU.jpg)

**Russian:**

Видать, очень сильно портачили строители, раз учеными решено сделать только экспозицию раскопок. Но и тут, если присмотреться, металл не убран.

**English:**

It seems that the builders messed it up pretty badly, so scientists decided to make an exposition showing only the excavations. But even here, if you look closely, the metal has not been removed.

<!-- Local
![](1481274_files/FgCW3fv.jpg#resized)
-->

![](https://i.imgur.com/FgCW3fv.jpg#resized)

<!-- Local
[![](1481274_files/fRLVK1R.jpg#resized)](1481274_files/fRLVK1R.jpg)
-->

[![](https://i.imgur.com/fRLVK1R.jpg#clickable)](https://i.imgur.com/fRLVK1R.jpg)

**Russian:**

Прикопать бы надо было этот инкский мегалит в той же экспозиции раскопок. Типа ученые еще недовыкопали. А то вон чего...

**English:**

We should have buried this Inca megalith in the same excavation exposition. Like scientists haven't dug enough yet. And here we are...

<!-- Local
![](1481274_files/kCucclY.jpg#resized)
-->

![](https://i.imgur.com/kCucclY.jpg#resized)

<!-- Local
[![](1481274_files/tcNmV2a.jpg#resized)](1481274_files/tcNmV2a.jpg)
-->

[![](https://i.imgur.com/tcNmV2a.jpg#clickable)](https://i.imgur.com/tcNmV2a.jpg)

**Russian:**

Подойдем к истуканам ненадолго. В этом две железячки торчат.

**English:**

Let's go over to the statues for a while. There are two pieces of iron sticking out of this one.

<!-- Local
![](1481274_files/XXb8cpP.jpg#resized)
-->

![](https://i.imgur.com/XXb8cpP.jpg#resized)

<!-- Local
[![](1481274_files/mRqik3v.jpg#resized)](1481274_files/mRqik3v.jpg)
-->

[![](https://i.imgur.com/mRqik3v.jpg#clickable)](https://i.imgur.com/mRqik3v.jpg)

**Russian:**

Пластиночка каркасной основы с проволочной скруткой.

**English:**

A plate frame base with a wire twist.

<!-- Local
![](1481274_files/Rv5c0AT.jpg#resized)
-->

![](https://i.imgur.com/Rv5c0AT.jpg#resized)

<!-- Local
[![](1481274_files/JJ5pPn1.jpg#resized)](1481274_files/JJ5pPn1.jpg)
-->

[![](https://i.imgur.com/JJ5pPn1.jpg#clickable)](https://i.imgur.com/JJ5pPn1.jpg)

**Russian:**

Обычный хлам и посторонности в другом истукане.

**English:**

The usual junk and extraneous bits in another statue.

<!-- Local
![](1481274_files/UMJkXv1.jpg#resized)
-->

![](https://i.imgur.com/UMJkXv1.jpg#resized)

<!-- Local
[![](1481274_files/Ji9lJC0.jpg#resized)](1481274_files/Ji9lJC0.jpg)
-->

[![](https://i.imgur.com/Ji9lJC0.jpg#clickable)](https://i.imgur.com/Ji9lJC0.jpg)

**Russian:**

Давно пора штукатурить инкских истуканов. Пока мелочь всякая лезет, но кто его знает, что под следующим вышелушиванием откроется...

**English:**

It's high time we plastered the Incan icons. So far all sorts of little things are coming in, but who knows what will be revealed in the next exhumation...

<!-- Local
![](1481274_files/mIOkyC8.jpg#resized)
-->

![](https://i.imgur.com/mIOkyC8.jpg#resized)

<!-- Local
![](1481274_files/ELn3qsX.jpg#resized)
-->

![](https://i.imgur.com/ELn3qsX.jpg#resized)

<!-- Local
[![](1481274_files/TyWYHme.jpg#resized)](1481274_files/TyWYHme.jpg)
-->

[![](https://i.imgur.com/TyWYHme.jpg#clickable)](https://i.imgur.com/TyWYHme.jpg)

<!-- Local
![](1481274_files/y1ni05H.jpg#resized)
-->

![](https://i.imgur.com/y1ni05H.jpg#resized)

<!-- Local
[![](1481274_files/O34vVyF.jpg#resized)](1481274_files/O34vVyF.jpg)
-->

[![](https://i.imgur.com/O34vVyF.jpg#clickable)](https://i.imgur.com/O34vVyF.jpg)

**Russian:**

Больная фантазия мастера-худрука цеха строителей истории породила эти изделия древних инков. Строители отлили их для них. Скрутка каркаса, загнувшаяся в ходе заполнения формы слишком близко к поверхности, сдала и мастера, и строителей.

**English:**

The sick imagination of the master craftsman of the history builders' workshop gave birth to these ancient Inca pieces. The builders cast them for them. The curvature of the frame, bent in the course of filling the mould too close to the surface, gave away both the master and the builders.

<!-- Local
![](1481274_files/94qgnWh.jpg#resized)
-->

![](https://i.imgur.com/94qgnWh.jpg#resized)

<!-- Local
[![](1481274_files/OWXUino.jpg#resized)](1481274_files/OWXUino.jpg)
-->

[![](https://i.imgur.com/OWXUino.jpg#clickable)](https://i.imgur.com/OWXUino.jpg)

**Russian:**

Зампотех начальника этой цеховой стройки очень хорошо справился, хорошего качества литье. Но не безупречно. Не могут быть безупречны такие объемы.

**English:**

The deputy foreman of this shop building has produced very good quality casting. But it is not flawless. Such volumes cannot be flawless.

<!-- Local
![](1481274_files/nHuN7ed.jpg#resized)
-->

![](https://i.imgur.com/nHuN7ed.jpg#resized)

<!-- Local
![](1481274_files/1A2vIuW.jpg#resized)
-->

![](https://i.imgur.com/1A2vIuW.jpg#resized)

**Russian:**

А это опять на двужильный провод похоже.

**English:**

This looks like a double wire again.

<!-- Local
[![](1481274_files/lYMH28E.jpg#resized)](1481274_files/lYMH28E.jpg)
-->

[![](https://i.imgur.com/lYMH28E.jpg#clickable)](https://i.imgur.com/lYMH28E.jpg)

<!-- Local
[![](1481274_files/woR6792.jpg#resized)](1481274_files/woR6792.jpg)
-->

[![](https://i.imgur.com/woR6792.jpg#clickable)](https://i.imgur.com/woR6792.jpg)

**Russian:**

Здесь отмечаем постороннюю полоску и более ясно - выходы стальной проволоки каркаса.

**English:**

Here we mark the extraneous strip and more clearly the outputs of the steel wire frame.

<!-- Local
![](1481274_files/Ii9OKZt.jpg#resized)
-->

![](https://i.imgur.com/Ii9OKZt.jpg#resized)

<!-- Local
![](1481274_files/AXjVdOw.jpg#resized)
-->

![](https://i.imgur.com/AXjVdOw.jpg#resized)

<!-- Local
![](1481274_files/TzWIAzo.jpg#resized)
-->

![](https://i.imgur.com/TzWIAzo.jpg#resized)

<!-- Local
[![](1481274_files/PWEUNBf.jpg#resized)](1481274_files/PWEUNBf.jpg)
-->

[![](https://i.imgur.com/PWEUNBf.jpg#clickable)](https://i.imgur.com/PWEUNBf.jpg)

**Russian:**

Эти дела еще в гранитном литье питерских фортов глаза намозолили.

**English:**

These cases are still in the granite casting of the St. Petersburg forts.

<!-- Local
![](1481274_files/1Ocfb54.jpg#resized)
-->

![](https://i.imgur.com/1Ocfb54.jpg#resized)

<!-- Local
[![](1481274_files/fBxPYwi.jpg#resized)](1481274_files/fBxPYwi.jpg)
-->

[![](https://i.imgur.com/fBxPYwi.jpg#clickable)](https://i.imgur.com/fBxPYwi.jpg)

**Russian:**

По-моему, архитектор, придумавший это для цеха строителей истории, издевался над учеными. Никаким альтернатистам не придумать, зачем тесать такую фигню инкам и анунахам.

**English:**

In my opinion, the architect who thought it up for the history builders' shop was making a mockery of scientists. No alternative-ists can come up with an explanation why you would attribute such stuff to the Incas and Anunnakis.

<!-- Local
[![](1481274_files/1Y97n2O.jpg#resized)](1481274_files/1Y97n2O.jpg)
-->

[![](https://i.imgur.com/1Y97n2O.jpg#clickable)](https://i.imgur.com/1Y97n2O.jpg)

**Russian:**

Волны заливочные неминуемо гуляют как прибой при многослойном залитии таких площадей.

**English:**

Casting waves inevitably walk like the surf in a multilayer cast of such areas.

<!-- Local
![](1481274_files/eStCx7K.jpg#resized)
-->

![](https://i.imgur.com/eStCx7K.jpg#resized)

**Russian:**

Как водится, в бут ушел всякий брак, в том числе армированный.

**English:**

As usual, all sorts of defects, including reinforced, went into the booth.

<!-- Local
![](1481274_files/5D7iaz9.jpg#resized)
-->

![](https://i.imgur.com/5D7iaz9.jpg#resized)

**Russian:**

И что обидно: такого огромного труда и объема изделие, а какая-нибудь мятая полосочка спускает их все в трубу. Это опалубная полосочка. Тот же металл, смесь на основе дюраля, есть и в **[Древнеегипте](https://gorojanin-iz-b.livejournal.com/80593.html)**, и в Питерских фортах, и много где...

**English:**

And what a shame: such an enormous amount of work and volume of product, and some crumpled strip flushes them all down the tube. It's a formwork strip. The same metal, a dural-based compound, is in [Ancient Egypt](https://gorojanin-iz-b.livejournal.com/80593.html), and in St Petersburg's forts, and in many places...

<!-- Local
![](1481274_files/4xJapIb.jpg#resized)
-->

![](https://i.imgur.com/4xJapIb.jpg#resized)

<!-- Local
[![](1481274_files/sy6E1tC.jpg#resized)](1481274_files/sy6E1tC.jpg)
-->

[![](https://i.imgur.com/sy6E1tC.jpg#clickable)](https://i.imgur.com/sy6E1tC.jpg)

**Russian:**

Не туда показывает отважный исследователь великих працивилизаций. Тут несколько мест, куда надо показать и получше отснять.

**English:**

That's not where the brave explorer of great civilizations is pointing. There are several places to point and take better pictures.

<!-- Local
[![](1481274_files/QCdGEnM.jpg#resized)](1481274_files/QCdGEnM.jpg)
-->

[![](https://i.imgur.com/QCdGEnM.jpg#clickable)](https://i.imgur.com/QCdGEnM.jpg)

**Russian:**

Здесь есть армирование, обычное и привычное.

**English:**

There is reinforcement here, conventional and familiar.

<!-- Local
![](1481274_files/6lPI7JO.jpg#resized)
-->

![](https://i.imgur.com/6lPI7JO.jpg#resized)

**Russian:**

Обычная проволока.

**English:**

It's a regular wire.

<!-- Local
![](1481274_files/GAYI2ZR.jpg#resized)
-->

![](https://i.imgur.com/GAYI2ZR.jpg#resized)

**Russian:**

Провод какой-то... Фигня по сравнению с працивилизациями!

**English:**

Some kind of wire... Bullshit compared to pre-civilisations!

<!-- Local
![](1481274_files/ibiXMeV.jpg#resized)
-->

![](https://i.imgur.com/ibiXMeV.jpg#resized)

**Russian:**

Забавный предмет. Уж не часы ли настольные, механические, с рукояткой завода? Вернее, остатки их.

**English:**

That's a funny-looking object. Wouldn't that be a desk clock, mechanical, with a winding handle? Or rather, the remnants of it.

<!-- Local
![](1481274_files/Cc1KhiH.jpg#resized)
-->

![](https://i.imgur.com/Cc1KhiH.jpg#resized)

<!-- Local
[![](1481274_files/WjONBId.jpg#resized)](1481274_files/WjONBId.jpg)
-->

[![](https://i.imgur.com/WjONBId.jpg#clickable)](https://i.imgur.com/WjONBId.jpg)

**Russian:**

Совсем уж запоротое литье. Не совестно такое выставлять? Стыд и срам цеху.

**English:**

It's a really screwed-up casting. Don't you have any shame in showing it off? Shame on the shop.

<!-- Local
![](1481274_files/sJN96qj.jpg#resized)
-->

![](https://i.imgur.com/sJN96qj.jpg#resized)

<!-- Local
[![](1481274_files/yLS1tbZ.jpg#resized)](1481274_files/yLS1tbZ.jpg)
-->

[![](https://i.imgur.com/yLS1tbZ.jpg#clickable)](https://i.imgur.com/yLS1tbZ.jpg)

**Russian:**

Почему-то не дали нормальный снимок этого места альтернатисты. Осталось загадкой: это жесть из обивки тарного ящика или чугун?

**English:**

For some reason, the alternative-ists didn't provide a normal picture of this place. It remains a mystery: is it tin from the upholstery of the tare box or cast iron?

<!-- Local
![](1481274_files/HMZGHQD.jpg#resized)
-->

![](https://i.imgur.com/HMZGHQD.jpg#resized)

<!-- Local
[![](1481274_files/FTWNiOH.jpg#resized)](1481274_files/FTWNiOH.jpg)
-->

[![](https://i.imgur.com/FTWNiOH.jpg#clickable)](https://i.imgur.com/FTWNiOH.jpg)

**Russian:**

Все-таки не смогли бы работать в легальном строительстве цеховые строители истории. Ни советский, ни нынешний приемщик не примет подобную бетонную стену с такими включениями...

**English:**

Still, the shop builders of history could not work in legal construction. Neither a Soviet, nor a present-day receiver would accept such a concrete wall with such inclusions...

<!-- Local
![](1481274_files/DAq71re.jpg#resized)
-->

![](https://i.imgur.com/DAq71re.jpg#resized)

<!-- Local
[![](1481274_files/J7CmpkY.jpg#resized)](1481274_files/J7CmpkY.jpg)
-->

[![](https://i.imgur.com/J7CmpkY.jpg#clickable)](https://i.imgur.com/J7CmpkY.jpg)

**Russian:**

Огромный и небессмысленный труд многих портит и обессмысливает невнимательность некоторых, на финише техпроцесса.

**English:**

The tremendous and unintelligent work of many is being spoiled and discouraged by the inattention of some, at the finish line of the tech process.

<!-- Local
![](1481274_files/x03md2M.jpg#resized)
-->

![](https://i.imgur.com/x03md2M.jpg#resized)

<!-- Local
[![](1481274_files/efF4ea8.jpg#resized)](1481274_files/efF4ea8.jpg)
-->

[![](https://i.imgur.com/efF4ea8.jpg#clickable)](https://i.imgur.com/efF4ea8.jpg)

**Russian:**

Титанические труды и крохотный недосмотр, спускающий их в каналюгу... (недосмотров конечно больше, но формула хороша! не буду другие показывать)

**English:**

Titanic labors and a tiny oversight flushing them down the drain... (there are more under-observations of course, but the formula is good! I won't show you others)

<!-- Local
![](1481274_files/KXW7Wkn.jpg#resized)
-->

![](https://i.imgur.com/KXW7Wkn.jpg#resized)

<!-- Local
[![](1481274_files/OxIjNE7.jpg#resized)](1481274_files/OxIjNE7.jpg)
-->

[![](https://i.imgur.com/OxIjNE7.jpg#clickable)](https://i.imgur.com/OxIjNE7.jpg)

**Russian:**

Тут все любуются таинственными пропилами лазеров анунахов в толще гранита, хотя вот эти обрывки от сеточки поинтереснее будут.

**English:**

Here everybody is admiring mysterious laser-cuts of Anunnakis in the thickness of granite, though these mesh scraps will be more interesting.

<!-- Local
![](1481274_files/1sQ2LNd.jpg#resized)
-->

![](https://i.imgur.com/1sQ2LNd.jpg#resized)

<!-- Local
[![](1481274_files/EU3JOKA.jpg#resized)](1481274_files/EU3JOKA.jpg)
-->

[![](https://i.imgur.com/EU3JOKA.jpg#clickable)](https://i.imgur.com/EU3JOKA.jpg)

**Russian:**

Хороши железяки! Между прочим, нерабочий конец стального инструмента, вставляющийся в деревянную ручку.

**English:**

Nice hardware! By the way, the non-working end of a steel tool inserted into a wooden handle.

<!-- Local
![](1481274_files/6E2jvKt.jpg#resized)
-->

![](https://i.imgur.com/6E2jvKt.jpg#resized)

<!-- Local
[![](1481274_files/xXddP5Q.jpg#resized)](1481274_files/xXddP5Q.jpg)
-->

[![](https://i.imgur.com/xXddP5Q.jpg#clickable)](https://i.imgur.com/xXddP5Q.jpg)

**Russian:**

Табурет главного анунаха - прародителя индейцев у альтернативных историков. Табурет крепкий. Железом напихали на совесть. Главанунах еще и ануначиху на коленках держал.

**English:**

The stool of the chief Anunnaki, the progenitor of the Indians according to alternative historians. The stool is sturdy. It was packed with iron for good measure. The Anunnaki was also holding the head of the stool on his knees.

<!-- Local
![](1481274_files/XwgA72R.jpg#resized)
-->

![](https://i.imgur.com/XwgA72R.jpg#resized)

<!-- Local
[![](1481274_files/X3cPilv.jpg#resized)](1481274_files/X3cPilv.jpg)
-->

[![](https://i.imgur.com/X3cPilv.jpg#clickable)](https://i.imgur.com/X3cPilv.jpg)

**Russian:**

Замечательное крашеное пигментом в растворе изделие. Рисунок насыпался крошками, наподобие нынешней песочной анимации... Но главное - не рисунок, а скобяное изделие древнеинков-анунахов. И штопор какой-то в кадр немножко попал. Жаль, не сняли ЛАИсты его как следует.

**English:**

A wonderful product painted with pigment in mortar. The drawing was poured with crumbs, like today's sand animation... But the main thing is not the drawing, but the hardware of ancient Ancient Anunnakis. And some corkscrew got in the frame a little bit. It is a pity that LAI did not shot it properly.

<!-- Local
[![](1481274_files/NUPDRRS.jpg#resized)](1481274_files/NUPDRRS.jpg)
-->

[![](https://i.imgur.com/NUPDRRS.jpg#clickable)](https://i.imgur.com/NUPDRRS.jpg)

<!-- Local
[![](1481274_files/chlz8eQ.jpg#resized)](1481274_files/chlz8eQ.jpg)
-->

[![](https://i.imgur.com/chlz8eQ.jpg#clickable)](https://i.imgur.com/chlz8eQ.jpg)

<!-- Local
![](1481274_files/UONabhL.jpg#resized)
-->

![](https://i.imgur.com/UONabhL.jpg#resized)

<!-- Local
![](1481274_files/G5Mxlyi.jpg#resized)
-->

![](https://i.imgur.com/G5Mxlyi.jpg#resized)

**Russian:**

Снова раскопки - вскрытие учеными продукта труда строителей. Прекрасный продукт! И опалубные листы еще не вынуты, и посторонние предметы не убраны, и роскошное стальное изделие не спрятано...

**English:**

Excavations again - dissecting by scientists the product of the builders' labor. A beautiful product! And the formwork sheets have not been removed yet, and foreign objects have not been removed, and the luxurious steelwork has not been hidden...

<!-- Local
[![](1481274_files/BRTyXpM.jpg#resized)](1481274_files/BRTyXpM.jpg)
-->

[![](https://i.imgur.com/BRTyXpM.jpg#clickable)](https://i.imgur.com/BRTyXpM.jpg)

<!-- Local
![](1481274_files/FiNVOFI.jpg#resized)
-->

![](https://i.imgur.com/FiNVOFI.jpg#resized)

<!-- Local
![](1481274_files/eCLM0ec.jpg#resized)
-->

![](https://i.imgur.com/eCLM0ec.jpg#resized)

<!-- Local
![](1481274_files/hxBxqFT.jpg#resized)
-->

![](https://i.imgur.com/hxBxqFT.jpg#resized)

<!-- Local
![](1481274_files/nrwWjco.jpg#resized)
-->

![](https://i.imgur.com/nrwWjco.jpg#resized)

**Russian:**

Ну а это эль-классико Тиуанако. Выставлено просвещающимся массам давно и любимо ими.

**English:**

Well, this is the El Classico of Tiwanaku. Exposed to the enlightened masses long ago and loved by them.

<!-- Local
[![](1481274_files/rG4mLtM.jpg#resized)](1481274_files/rG4mLtM.jpg)
-->

[![](https://i.imgur.com/rG4mLtM.jpg#clickable)](https://i.imgur.com/rG4mLtM.jpg)

**Russian:**

Сделано на совесть. Только грани все время рассыпаются. Не успевают их реставрировать. Там и найдем самое интересное.

**English:**

It's very well made. Only the edges keep falling apart. They can't be restored in time. That's where we'll find the best part.

<!-- Local
![](1481274_files/blwIGxn.jpg#resized)
-->

![](https://i.imgur.com/blwIGxn.jpg#resized)

<!-- Local
![](1481274_files/qF6Nq1i.jpg#resized)
-->

![](https://i.imgur.com/qF6Nq1i.jpg#resized)

<!-- Local
![](1481274_files/QzGbdOc.jpg#resized)
-->

![](https://i.imgur.com/QzGbdOc.jpg#resized)

**Russian:**

Еще эль-классико Тиуанако. Прямо под ногами.

**English:**

Also the el clasico of Tiwanaku. Right underfoot.

<!-- Local
![](1481274_files/QecHa7l.jpg#resized)
-->

![](https://i.imgur.com/QecHa7l.jpg#resized)

<!-- Local
[![](1481274_files/9hbLlYR.jpg#resized)](1481274_files/9hbLlYR.jpg)
-->

[![](https://i.imgur.com/9hbLlYR.jpg#clickable)](https://i.imgur.com/9hbLlYR.jpg)

**Russian:**

В общем, подробностей наследия человечества и ЮНЕСКО лучше не снимать. Обязательно вылезет природа этих наследий.

**English:**

In general, the details of the heritage of humanity and UNESCO are better not being filmed. The nature of these legacies is bound to come out.

<!-- Local
![](1481274_files/T1q0UHR.jpg#resized)
-->

![](https://i.imgur.com/T1q0UHR.jpg#resized)

<!-- Local
[![](1481274_files/Sz1FKvA.jpg#resized)](1481274_files/Sz1FKvA.jpg)
-->

[![](https://i.imgur.com/Sz1FKvA.jpg#clickable)](https://i.imgur.com/Sz1FKvA.jpg)

<!-- Local
![](1481274_files/Pek4Hlk.jpg#resized)
-->

![](https://i.imgur.com/Pek4Hlk.jpg#resized)

<!-- Local
![](1481274_files/QmwwECO.jpg#resized)
-->

![](https://i.imgur.com/QmwwECO.jpg#resized)

<!-- Local
[![](1481274_files/Qyre68l.jpg#resized)](1481274_files/Qyre68l.jpg)
-->

[![](https://i.imgur.com/Qyre68l.jpg#clickable)](https://i.imgur.com/Qyre68l.jpg)

<!-- Local
![](1481274_files/We0iIie.jpg#resized)
-->

![](https://i.imgur.com/We0iIie.jpg#resized)

<!-- Local
![](1481274_files/Aq62Ion.jpg#resized)
-->

![](https://i.imgur.com/Aq62Ion.jpg#resized)

<!-- Local
[![](1481274_files/1eEZxLX.jpg#resized)](1481274_files/1eEZxLX.jpg)
-->

[![](https://i.imgur.com/1eEZxLX.jpg#clickable)](https://i.imgur.com/1eEZxLX.jpg)

**Russian:**

А вот это заслуживает отдельного показа. Очень интересный предмет. Вилка. Дешевая дюралевая штамповка.

**English:**

Now, this deserves a separate show. It's a very interesting item. Fork. Cheap dural stamping.

**Russian:**

У меня в базе это третья древневилка. Одна нашлась в толще мега-колонны Карнакского храма на не ретушированном снимке Феликса Бонфилса 1860-х годов, вторая, вместе с ложкой - в толще мальтийского храмового мегалитического комплекса Джгантия, 3600 лет до н.э.

**English:**

This is the third antique fork in my database. One was found in the thickness of a mega-column of the Karnak temple on an unretouched photo by Felix Bonfils in the 1860s, the second, together with the spoon, in the thickness of the Maltese temple megalithic complex Jgantija, 3600 years BC.

<!-- Local
![](1481274_files/mg8i3KF.jpg#resized)
-->

![](https://i.imgur.com/mg8i3KF.jpg#resized)

<!-- Local
![](1481274_files/816ZoV2.jpg#resized)
-->

![](https://i.imgur.com/816ZoV2.jpg#resized)

<!-- Local
[![](1481274_files/A9TNLQh.jpg#resized)](1481274_files/A9TNLQh.jpg)
-->

[![](https://i.imgur.com/A9TNLQh.jpg#clickable)](https://i.imgur.com/A9TNLQh.jpg)

**Russian:**

И для настроения - еще пара забавных сопоставлений в жанре "было-стало" из съемок времен открытия и наших дней великого памятника и наследия человечества - древнего храма в Тиуанако.

**English:**

And for the mood - a couple of funny comparisons in the genre of "once upon a time" from the time of discovery and the present day of the great monument and heritage of mankind - the ancient temple in Tiwanaku.

**Russian:**

Добра!

**English:**

Goodness!

<!-- Local
[![](1481274_files/Y79yo3v.jpg#resized)](1481274_files/Y79yo3v.jpg)
-->

[![](https://i.imgur.com/Y79yo3v.jpg#clickable)](https://i.imgur.com/Y79yo3v.jpg)

<!-- Local
[![](1481274_files/75ZxAoH.jpg#resized)](1481274_files/75ZxAoH.jpg)
-->

[![](https://i.imgur.com/75ZxAoH.jpg#clickable)](https://i.imgur.com/75ZxAoH.jpg)

<!-- Local
[![](1481274_files/G4VvWya.jpg#resized)](1481274_files/G4VvWya.jpg)
-->

[![](https://i.imgur.com/G4VvWya.jpg#clickable)](https://i.imgur.com/G4VvWya.jpg)

<!-- Local
[![](1481274_files/oKJuvaQ.jpg#resized)](1481274_files/oKJuvaQ.jpg)
-->

[![](https://i.imgur.com/oKJuvaQ.jpg#clickable)](https://i.imgur.com/oKJuvaQ.jpg)

**Russian:**

Часть 4 [здесь](https://eto-fake.livejournal.com/1481666.html)]

**English:**

Part 4 [here](https://eto-fake.livejournal.com/1481666.html)]

© All rights reserved. The original author retains ownership and rights.

