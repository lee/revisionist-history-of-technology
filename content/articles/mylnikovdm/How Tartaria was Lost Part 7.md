title: How Tartaria Died Part 7 - Conclusions
date: 2014-11-24
modified: Thu 14 Jan 2021 07:22:41 GMT
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: However, strangely enough, there is no mention of glorious victories in history in a Great War, without which a huge state simply could not be destroyed.
status: published
originally: Как погибла Тартария? Часть 7. Заключительная. - Дмитрий Мыльников — LiveJournal.html; Как погибла Тартария? Часть 7 окончательная. - Дмитрий Мыльников — LiveJournal.html; Как погибла Тартария? Часть 7б. Заключительная. - Дмитрий Мыльников — LiveJournal.html; Как погибла Тартария? Часть 7в - Дмитрий Мыльников — LiveJournal.html
local: How Tartaria was Lost Part 7_files/

### Translated from: 

[https://mylnikovdm.livejournal.com/6335.html](https://mylnikovdm.livejournal.com/6335.html), [https://mylnikovdm.livejournal.com/6639.html](https://mylnikovdm.livejournal.com/6639.html), [https://mylnikovdm.livejournal.com/6658.html](https://mylnikovdm.livejournal.com/6658.html) and [https://mylnikovdm.livejournal.com/7086.html](https://mylnikovdm.livejournal.com/7086.html)


Translator's note: Mylnikov's conjectures may be easier to conceive if readers have already read [Vladimir Rubtsov's 2009 *The Tunguska Mystery*](https://www.springer.com/gp/book/9780387765730). The book collates various researchers' attempts to untangle anomalies in the Tunguska event. Research into Tunguska's genetic effects is also described in depth in [Z. K. Silagadze's *Tunguska genetic anomaly and electrophonic meteors*](https://cds.cern.ch/record/684520/files/0311337.pdf). A brief summary of the core problem is at [https://www.science-frontiers.com/sf100/sf100g10.htm](https://www.science-frontiers.com/sf100/sf100g10.htm).

**Russian:**

Когда я начинал писать первую часть статьи, то предполагал, что управлюсь со всем за две недели. Но стоило только потянуть за ниточку, как на меня посыпалось огромное количество фактов, о существовании которых до начала этой работы я понятия не имел. Множество людей стали присылать письма по электронной почте и оставлять комментарии с множеством ссылок, фотографий или с рассказами о собственных наблюдениях. В итоге работа растянулась на несколько месяцев. За это время я собрал уже столько дополнительных фактов, что содержание всех частей можно было бы увеличить в два, а то и три раза. Но в конце концов я решил, что даже тех фактов, что уже изложены в первых шести частях достаточно, чтобы сделать определённые выводы, которые я собираюсь изложить в этой части.

**English:**

When I started writing the first part of the article, I assumed that I would manage everything in two weeks. But as soon as I pulled the strings, a huge number of facts fell on me, about which I had no idea before this work began. A lot of people started sending e-mails and leaving comments with lots of links, pictures or stories about their own observations. In the end, the work lasted for several months. During this time I gathered so many additional facts that the content of all parts could be increased by two or even three times. In the end, I decided that the facts already presented in the first six parts were enough to draw certain conclusions which I was going to present in this part.

**Russian:**

Прежде чем перейти к выводам, я хочу кратко ещё раз изложить основные ключевые моменты, которые описаны в предыдущих шести частях.

**English:**

Before turning to the conclusions, I want to briefly reiterate the main key points described in the previous six parts.

**Russian:**

Огромное государство, которое на старых картах и в книгах 16-18 веков называется «Великая Тартария», бесследно исчезло в начале 19 века. Его перестали обозначать на картах, перестали упоминать в книгах и учебниках истории и географии. При этом, что странно, в истории нет упоминания о славных победах в большой войне, без которой огромное государство просто не могло быть уничтожено.

**English:**

The huge state, which on old maps and in books of the 16th-18th centuries is called "Great Tartaria", disappeared without a trace in the early 19th century. It was no longer marked on maps, and was no longer mentioned in history and geography books and textbooks. However, strangely enough, there is no mention of glorious victories in history in a Great War, without which a huge state simply could not be destroyed.

<!-- Local
![](How Tartaria was Lost Part 7_files/767_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/767/767_original.jpg#resized)

**Russian:**

В первой части](http://mylnikovdm.livejournal.com/603.html) я рассказал о том, что на спутниковых снимках территории Западной Сибири очень хорошо читаются множество следов от падения крупных и средних метеоритов. В частности, подобными следами являются так называемые ленточные боры, расположенные на территории Новосибирской области, Алтайского края и Казахстана. Ширина этих полос от 5 до 8 километров, длина самых длинных более 240 километров.

**English:**

[In the first part](http://mylnikovdm.livejournal.com/603.html) I talked about the fact that in satellite images of the territory of Western Siberia are very easily readable traces of the fall of large and medium sized meteorites. In particular, such traces are the so-called ribbon scars, located in Novosibirsk Region, Altai Territory and Kazakhstan. The width of these bands is from 5 to 8 kilometers, the longest is more than 240 kilometers.

<!-- Local
[![](How Tartaria was Lost Part 7_files/1607_original.jpg#clickable)](How Tartaria was Lost Part 7_files/1607_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1607/1607_original.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/1607/1607_original.jpg)

**Russian:**

На то, что данные следы оставлены объектами, упавшими из космоса, указывает тот факт, что в начальной части своей траектории их угол соответствует углу наклона оси вращения Земли к плоскости Эклиптики, в которой движутся почти все планеты и астероиды в Солнечной системе.

**English:**

The fact that these traces were left by objects that fell from space is indicated by the fact that in the initial part of their trajectory, their angle corresponds to the angle of inclination of the Earth's axis of rotation to the plane of the ecliptic, in which almost all the planets and asteroids in the solar system are moving.

**Russian:**

Альтернативные теории, которые выдвигаются для объяснения происхождения данных следов, указывают в качестве возможных причин их образования сход ледника после последнего оледенения, мощные потоки воды, вызванные закупоркой стока сибирских рек в Ледовитый океан ледником, либо даже гигантскую волну океанской воды, вызванную поворотом земного шара и смещением оси вращения Земли вследствие удара гигантского метеорита.

**English:**

Alternative theories put forward to explain the origin of these footprints indicate a glacier descending after the last glaciation, powerful water flows caused by blockage of the flow of Siberian rivers into the Arctic Ocean by this glacier, or even a giant wave of ocean water caused by the rotation of the globe and the shift of the Earth's rotation axis due to the impact of a giant meteorite.

**Russian:**

Но все эти теории не объясняют, почему следы располагаются именно под углом, который соответствует углу наклона оси вращения. Кроме того, данные следы не похожи на те, которые оставляют мощные потоки воды, особенно с учётом рельефа местности и того факта, что следы пересекают хребет водораздела между речными системами Оби и Иртыша. Против теории гигантской волны при смещении оси вращения земного шара говорит также тот факт, что следу идут узкими ровными полосами, при этом мы не наблюдаем на данной местности гор, которые могли бы сформировать подобные потоки.

**English:**

But none of these theories explain why the traces are located at an angle that precisely corresponds to the axis of the angle of rotation. Besides, these footprints are not similar to those that powerful flows of water leave, especially taking into account the terrain and the fact that the footprints cross the watershed ridge between the Ob and Irtysh river systems. The fact that the trails run in narrow, flat strips contradicts the theory of a giant wave - created when the globe's axis of rotation shifts.

**Russian:**

Я предполагаю, что данные следы оставлены крупными метеоритами, размером более 5 километров в диаметре, которые летели с высокой скоростью практически по касательной к поверхности Земли. При этом то, что в конце следов мы не наблюдаем ни кратеров, ни множества каменных обломков, ни гор высотой в несколько километров, но зато можем наблюдать характерные следы разлива большого количества воды, как в устьях рек, позволяет предположить, что данные метеориты представляли собой гигантские куски обычного водяного льда. Этот лёд в конце концов растаял, образовав потоки воды, которые образовали размывы в конце следов. В ходе обсуждения высказывались возражения, что ледяные метеориты не смогли бы долететь до поверхности Земли, разрушившись при прохождении атмосферы, но каких либо конкретных расчётов, которые бы обосновывали данное предположение, представлено не было. С учётом гигантских размеров объектов выделившейся энергии при прохождении атмосферы было бы недостаточно, чтобы их полностью испарить. Это означает, что масса вещества всё равно должны была достигнуть поверхности Земли. А поскольку мы не наблюдаем разлетающиеся следы от обломков, логично предположить, что основная масса льда достигла Земли одним большим куском.

**English:**

I assume that these tracks were left by large meteorites - over 5 kilometres in diameter - which flew at high speed almost tangentially to the surface of the Earth. The fact that at the end of the footprints we do not see any craters, or a lot of rock debris, or mountains several kilometers high, but we can see the characteristic traces of a large spill of water, as in the mouths of rivers, suggests that these meteorites were giant pieces of ordinary water ice. This ice eventually melted, forming streams of water that formed a washout at the end of the tracks. During the discussion, there were objections that the ice meteorites would not have been able to reach the surface of the Earth because atmospheric heating would have destroyed them, but no specific calculations were presented to support this assumption. Given the giant size of the objects, the energy released during the passage of the atmosphere would not be enough to completely vaporize them. This means that the mass of matter would still have reached the Earth's surface. And since we don't see any spreading debris traces, it's logical to assume that the bulk of each ice meteorite reached the Earth in one big piece.

**Russian:**

Кроме нескольких крупных следов наблюдается большое количество средних и мелких следов, например, в районе озера Чаны.

**English:**

In addition to a few large traces, there are a large number of medium and small traces, for example, in the area of Lake Chany.

<!-- Local
[![](How Tartaria was Lost Part 7_files/1991_900.jpg#clickable)](How Tartaria was Lost Part 7_files/1991_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1991/1991_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/1991/1991_original.jpg)

**Russian:**

В районе города Омск.

**English:**

Near the city of Omsk.

<!-- Local
[![](How Tartaria was Lost Part 7_files/2718_900.jpg#clickable)](How Tartaria was Lost Part 7_files/2718_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2718/2718_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/2718/2718_original.jpg)

**Russian:**

Севернее Ишима, почти до Тобольска.

**English:**

North of Ishim, almost at Tobolsk.

<!-- Local
[![](How Tartaria was Lost Part 7_files/2960_900.jpg#clickable)](How Tartaria was Lost Part 7_files/2960_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2960/2960_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/2960/2960_original.jpg)

**Russian:**

И даже на территории северного Казахстана в районе города Рудный.

**English:**

And even on the territory of northern Kazakhstan near the town of Rudny.

<!-- Local
[![](How Tartaria was Lost Part 7_files/3713_900.jpg#clickable)](How Tartaria was Lost Part 7_files/3713_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/3713/3713_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/3713/3713_original.jpg)


**Russian:**

Размеры объектов, которые оставили данные следы, от 2 километров до нескольких сот метров. При этом тот факт, что все данные следы идут параллельно, говорит о том, что упали все эти объекты в один и тот же момент. На то, что следы появились в одно время, указывает и то, что выглядят они примерно одинаково, в то время как следы, которые образовались в разное время, будут выглядеть по разному из-за их постепенного зарастания и сглаживания за счёт воды и ветра.

**English:**

The size of the objects that left these traces ranges from 2 kilometres to several hundred metres. The fact that all these footprints are parallel suggests that all these objects fell at the same time. The fact that the tracks look roughly the same indicates that they were created at the same time, while those that appear at different times will look different due to their gradual overgrowth and smoothing by water and wind.

**Russian:**

В целом же площадь поражения, на которой данные следы хорошо читаются на спутниковых снимках, составляет порядка 1.5 млн. квадратных километров.

**English:**

In general, the area of impact, on which these traces can be read well on satellite images, is about 1.5 million square kilometers.

<!-- Local
[![](How Tartaria was Lost Part 7_files/4251_900.jpg#clickable)](How Tartaria was Lost Part 7_files/4251_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/4251/4251_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/4251/4251_original.jpg)

**Russian:**

Так как данные следы не образуют веера с достаточно близким центром, чтобы взрыв мог произойти в атмосфере Земли при контакте с ней крупного астероида, можно говорить о том, что это было астероидное поле с шириной фронта порядка 800 километров. Но это поле не было однородным, а включало в себя несколько более плотных областей, поскольку следы на поверхности имеют несколько зон с большим количеством следов, между которыми следов либо нет вообще, либо имеются только одиночные образования подобного рода.

**English:**

Since these traces do not form a fan radiating from a center close enough to a ground zero that could indicate where an asteroidal explosion occurred in the Earth's atmosphere, we can say that it was an asteroid field with a front width of about 800 kilometers. The field was not homogeneous, but included several denser areas. This we know because the surface tracks have several zones with many traces, between which there are no traces at all or only single formations of this kind.

**Russian:**

При этом в конце зоны падения, на западе Курганской и Юго-востоке Челябинской областей характер следов меняется с сильно смазанных на овальные и круглые кратеры, которые сейчас заполнены водой и являются озёрами. Это говорит о том, что данные объекты достигли Земли позже, когда земной шар уже повернулся за счёт суточного вращения, поэтому падали они уже не по касательной к поверхности, как первые объекты, а под более крутым углом и практически вертикально в самом конце.

**English:**

At the same time, at the end of the fall zone, in the west of the Kurgan and south-east of the Chelyabinsk regions, the character of the traces changes from heavily lubricated skid troughs to oval and circular craters, which are now water-filled lakes. This suggests that these objects reached the Earth later, when the globe had already turned due to its diurnal rotation. Instead of falling tangentially to the Earth's surface, like the earlier objects, they fell at a steeper angle and were almost vertically at the very end.

**Russian:**

Падение такого огромного количества объектов, а судя по следам средних и мелких метеоритов было несколько десятков тысяч, должно было привести к катастрофе планетарного масштаба. Во первых, в самой зоне поражения выжить было возможно только в глубоких подземных убежищах. Всё, что находилось на поверхности в этих местах, было убито или разрушено практически сразу. При этом был повреждён поверхностный слой почвы и уничтожена растительность на достаточно большой территории непосредственного контакта с объектами.

**English:**

The fall of such a huge number of objects - and judging by the traces of medium and small meteorites it was in the several tens of thousands - should have led to a planetary scale catastrophe. In the first place, it was possible to survive in the affected area itself only in deep underground shelters. Everything on the surface in these places was killed or destroyed almost immediately. In the rather large area of direct contact with the objects, the surface layer of soil was damaged and vegetation was destroyed.

**Russian:**

Во время падения и взрыва метеорита в феврале 2013 года в Челябинске, размер которого оценивается всего 15 метров, мощная взрывная волна, которая вызывала разрушения, наблюдалась на расстоянии более 100 километров от эпицентра взрыва. Теперь представим, что подобных взрывов при падении плотного метеоритного потока происходят сотни. Причём размеры объектов, судя по следам тех объектов которые долетали до поверхности, а значит и мощность взрывов явно больше, чем мы наблюдали в Челябинске. Это означает, что должна была наблюдаться мощная ударная волна в радиусе до 150-200 км от границы зон прямого поражения. В этой зоне леса могли быть повалены, а имевшиеся здания и сооружения разрушены.

**English:**

During the fall and explosion of a meteorite in February 2013 in Chelyabinsk, which is estimated to have been only 15 meters diameter, a powerful destructive blast wave was observed more than 100 kilometers from the epicenter of the explosion. Now let's imagine that hundreds of such explosions occur when a dense meteorite stream falls. And the size of objects, judging by the traces of those objects that reached the surface, and thus the power of their explosions is clearly greater than we observed in Chelyabinsk. This means that there should have been a powerful shock wave within a radius of 150-200 km from the edge of direct impact zones. In this zone, forests and existing buildings and structures could have been destroyed.

**Russian:**

При прохождении подобного количества объектов атмосфера должна была разогреться. Часть объектов, скорее всего, взрывалась в верхних слоях атмосферы, аналогично тому, как взорвался Челябинский метеорит в феврале 2013 года, остальные теряли часть своего вещества при торможении и нагреве в верхних слоях атмосферы. Это означает, что в атмосфере образовалось большое количество пыли, которая должна была оказать влияние на климат, заслонив поверхность Земли от Солнечного света. Последствия этого аналогичны так называемой «ядерной зиме», когда атмосфера загрязняется пылью и пеплом, которые поднимают в атмосферу ядерные взрывы.

**English:**

When such a large number of objects passed through it, the atmosphere should have warmed up. Some objects most likely exploded in the upper atmosphere, just as the Chelyabinsk meteorite exploded in February 2013, while others lost some of their substance while braking and heating up in the upper atmosphere. This means that a large amount of dust was formed in the atmosphere, which should have influenced the climate by shielding the Earth's surface from sunlight. The consequences are similar to the so-called "nuclear winter", where the atmosphere is polluted with dust and ashes that nuclear explosions bring into the atmosphere.

**Russian:**

Всё это в конечном итоге вызвало экологическую катастрофу не только в зоне падения, но и на огромной прилегающей к ней территории. Какое-то время вода рек была загрязнена. С одной стороны ледяные метеориты растаяв, образовали много воды. При этом мы не можем точно сказать, каков был её химический состав, была ли эта вода пресной или солёной и какие соли в ней содержались. С другой стороны, умершие живые организмы начали разлагаться, а нарушенный верхний слой почвы активно размывался как водой от метеоритов, так и просто атмосферными осадками. И все эти продукты разложения и осадочные породы в конечном итоге смывались водой вниз по течению рек.

**English:**

All of this eventually caused an environmental disaster not only in the fall zone, but in a huge adjacent area. For a while, the river water was contaminated. On the one hand, ice meteorites melted and formed a lot of water. At the same time, we can not say exactly what its chemical composition was; whether the water was fresh or salty and what salts it contained. On the other hand, dead organisms began to decompose, and the disturbed topsoil was actively eroded by both meteorite water and simple precipitation. And all these decomposition products and sedimentary rocks were eventually washed away downstream into the rivers.

**Russian:**

На тех территориях, которые оказались затронуты катастрофой, очень много сельскохозяйственных земель, которые используются для выращивания продовольствия. Это означает, что данная катастрофа должна была также вызвать экономическую катастрофу и голод в Великой Тартарии.

**English:**

In the areas affected by the disaster, there was a lot of agricultural land that was used for food cultivation. This means that this catastrophe should also have caused an economic disaster and famine in Great Tartaria.

**Russian:**

[В четвёртой части](http://mylnikovdm.livejournal.com/2593.html) я рассказывал, что в начале 19 века имеется множество свидетельств о том, что наблюдалось локальное похолодание. Аномальное похолодание фиксировалось с 1815 по 1818 годы. 1816 год вообще во многих странах называли «год без лета». Согласно официальной версии причиной этого похолодания было извержение вулкана Тамбора на индонезийском острове Суматра. Но если посмотреть планетарную на схему ветров и течений, то мы увидим, что воздушные массы из южного полушария практически не смешиваются с воздушными массами северного полушария (на схеме преобладающие ветра показаны серыми стрелками). В учебнике по гражданской обороне при описании последствий ядерного взрыва также сообщается, что радиоактивные осадки обычно выпадают в том же полушарии, где происходит ядерный взрыв.

**English:**

[In the fourth part](http://mylnikovdm.livejournal.com/2593.html) I told you that there was a lot of evidence of local cooling at the beginning of the 19th century. Anomalous cooling was recorded from 1815 to 1818. The year 1816 was generally referred to in many countries as the "year without summer". According to the official version, the reason for this cooling was the eruption of the Tamboro volcano on the Indonesian island of Sumatra. But if we look at the planetary scheme of winds and currents, we will see that the air masses from the southern hemisphere barely mix with the air masses of the northern hemisphere (on the plan, prevailing winds are shown by grey arrows). The civil defense textbook, in its description of the consequences of a nuclear explosion, also reports that radioactive fallout usually falls in the same hemisphere where the nuclear explosion occurs.

**Russian:**

08 схема ветров и течений

**English:**

08 wind and current pattern

<!-- Local
[![](How Tartaria was Lost Part 7_files/87094_900.jpg#clickable)](How Tartaria was Lost Part 7_files/87094_900.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/87094/87094_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/87094/87094_900.jpg)

**Russian:**

Это означает, что извержение вулкана Тамбора тут не причём, а вот масштабная катастрофа в Западной Сибири как раз могла быть причиной столь резкого изменения климата. При этом, как я указывал в 4-ой части, если на территории Северной Америки и большей части Западной и Средней Европы климат постепенно восстановился, то на территории России произошёл так называемый климатический сдвиг, так как уничтожение растительности и лесов на большой территории Западной Сибири данным катаклизмом привело к нарушению температурного баланса, поскольку леса, особенно хвойные, выполняют роль температурного стабилизатора, не позволяя почве зимой сильно промерзать, а летом перегреваться и высыхать. В итоге граница так называемой «вечной мерзлоты» должна была существенно сдвинутся на юг, что вызвало на территории Евразии своеобразный малый ледниковый период. Сейчас же мы можем наблюдать постепенное восстановление климата, поскольку за последние 20 лет граница вечной мерзлоты в Западной Сибири сдвинулась на север более чем на 200 километров. Также за последнее время существенно уменьшилась и ледяная шапка в Арктике. В 2012 году площадь льдов была самой минимальной с начала спутникового наблюдения в 1979 году. При этом по данным некоторых исследователей общая масса арктического льда составляла в 2012 году всего 30% от его массы в начале 80-х годов. [http://www.bbc.co.uk/russian/science/2012/08/120827_arctic_ice_record_low.shtml](http://www.bbc.co.uk/russian/science/2012/08/120827_arctic_ice_record_low.shtml)

**English:**

This means that the eruption of the Tamboro volcano had nothing to do with it, but a massive catastrophe in Western Siberia could have caused such a dramatic climate change. At the same time, as I pointed out in the fourth part, if on the territory of North America and the greater part of Western and Central Europe the climate gradually recovered, then on the territory of Russia a so-called climatic shift took place. This because the destruction of vegetation and forests on the large territory of Western Siberia disrupted the temperature balance. Forests, especially coniferous forests, are temperature stabilizers, preventing the soil from freezing heavily in winter and from overheating and drying out in summer. As a result, the boundary of the so-called "permafrost" shifted significantly to the south, which caused a kind of small glacial period in Eurasia. Now we can observe a gradual recovery of the climate, because over the past 20 years the permafrost border in Western Siberia has shifted to the north by more than 200 kilometers. The ice cap in the Arctic has also significantly decreased in recent years. In 2012, the ice area was the lowest since satellite observation began in 1979. According to some researchers, the total mass of Arctic ice in 2012 was only 30% of its mass in the early 80s. [http://www.bbc.co.uk/russian/science/2012/08/120827_arctic_ice_record_low.shtml](http://www.bbc.co.uk/russian/science/2012/08/120827_arctic_ice_record_low.shtml)

**Russian:**

Если бы мы наблюдали только следы от падения метеоритов, то всё это можно было бы попытаться списать на некоторое катастрофическое, но природное событие, которое произошло само собой, по неким естественным причинам. Летают же в космосе кометы, да и одиночные метеориты регулярно падают по всей планете. Но на территории Урала, Зауралья и Поволжья мы также наблюдаем и другие объекты, которые больше всего похожи на следы, которые остаются после применения ядерного оружия разной мощности.

**English:**

If we saw only the tracks of the meteorite fall, we could try to attribute it all to some catastrophic, but natural event that happened by itself, for natural reasons. Comets fly in space, and single meteorites regularly fall anywhere the planet. But on the territory of the Urals, Trans-Urals and Volga region, we also observe other objects, which are most similar to the traces that remain after the use of nuclear weapons of varying yields.

**Russian:**

09 Еманжелинск воронка

**English:**

09 Emanzhelinsk crater

<!-- Local
[![](How Tartaria was Lost Part 7_files/87535_900.jpg#clickable)](How Tartaria was Lost Part 7_files/87535_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/87535/87535_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/87535/87535_original.jpg)

**Russian:**

При этом [во второй части](http://mylnikovdm.livejournal.com/950.html) я рассказывал, что вопреки общепринятому мнению о том, что большая часть поселений на территории Зауралья и Западной Сибири были основаны в 17 и 18 веках, мы не можем утверждать о том, что существующие сейчас города и посёлки являются теми же самыми, поскольку при сопоставлении имеющихся старых схем и описаний с планами тех же самых поселений конца 19 начала 20 века наблюдаются заметные расхождения в их планировке. Также в большинстве этих поселений отсутствуют постройки старше середины 19-го века. Другими словами, если бы данные поселения были разрушены в начале 19 века, а потом в этих же местах построены заново с теми же названиями, то мы бы наблюдали именно такую картину, как наблюдаем. То, что многие новые города часто стоят на месте старых поселений, на самом деле вполне закономерный процесс, поскольку большая часть критериев, по которым определяется наиболее подходящее место для поселения, остаются неизменными на протяжении многих веков. Кроме того, дополнительным фактором для выбора может служить наличие дорог, которые остались со времён существования предыдущего поселения и ведут именно к этому месту, поскольку накатанные дороги будут зарастать намного медленнее, чем остальная территория. В лесах старые лесные дороги достаточно хорошо читаются даже через десятки лет после того, как ими перестают активно пользоваться.

**English:**

At the same time [in the second part](http://mylnikovdm.livejournal.com/950.html) I said that contrary to the common opinion that the majority of settlements in the territory of Trans-Urals and Western Siberia were founded in the 17th and 18th centuries, we cannot assert that the same cities and settlements exist now. This is because when we compare the remaining old schemes and descriptions with the plans of the same settlements in the late 19th and early 20th centuries, we find noticeable differences in their layout. Also in most of these settlements, there are no buildings older than the mid-19th century. In other words, if these settlements had been destroyed at the beginning of the 19th century and then rebuilt with the same names in the same places, we would see exactly the same picture as we do see. The fact that many new cities often stand on the site of old settlements is in fact a natural process, since most of the criteria by which the most suitable place for settlement is determined have remained unchanged for centuries. In addition, a factor for choosing a settlement site is the presence of roads that remain from the time of the previous settlement and lead to the place. Prepared road surfaces will overgrow with vegetation much slower than the rest of the territory. In forests, old forest roads are readable even decades after they are no longer actively used.

**Russian:**

Серьёзные разрушения, которые также могут быть последствием применения ядерного оружия, наблюдаются в Уральских горах, о чём я рассказывал [в шестой части](http://mylnikovdm.livejournal.com/4645.html). Причём подобные разрушения наблюдаются не только на Южном Урале, а практически по всему Уральскому хребту. Единственным весомым аргументом, который высказывался множеством оппонентов, является тот факт, что в начале 19 века наша человеческая цивилизация ещё не обладала ни ядерным оружием, ни средствами его доставки на большие расстояния (слишком безумные версии наиболее упёртых альтернативщиков я не рассматриваю в виду отсутствия бесспорных фактов подтверждающих их версии). Поэтому я перехожу к изложении своей версии того, что происходило и происходит на нашей планете. Данная версия основывается на той информации и фактах, которые я собираю уже больше 20-ти лет, но так как всю накопленную информацию, увы, невозможно подробно изложить в короткой статье, большую часть из них придётся опустить, оставив только выводы.

**English:**

Serious destruction, which may also be a consequence of the use of nuclear weapons, is observed in the Ural Mountains, as I described [in the sixth part](http://mylnikovdm.livejournal.com/4645.html). And such destruction is observed not only in the South Urals, but practically all over the Ural mountain range. The only weighty counter argument, which was expressed by many opponents, is the fact that in the beginning of the 19th century our human civilization did not yet possess either nuclear weapons or the means for their delivery over long distances (I leave out here crazy versions and the most stubborn alternative views because they lack indisputable facts to confirm their versions). So I move on to my version of what has happened - and is happening - on our planet. This version is based on the information and the facts which I have been collecting for more than 20 years. However, since all the accumulated information, alas, is impossible to describe in detail in a short article, most of it will have to be omitted, leaving only conclusions.

**Russian:**

Хотя когда-то нам пытались внушить, что именно Земля является центром Вселенной, а все остальные тела, включая Солнце, вращаются вокруг неё, сегодня мы знаем, что это далеко не так. Мало того, что наша планета вращается вокруг звезды, которую мы называем Солнце, так ещё и вся Солнечная система вращается вокруг центра нашей Галактики, называемой «Млечный путь». При этом по галактическим меркам наша Солнечная система является дальней провинцией, которая находится не только далеко от центра, но и в стороне от центрального галактического диска. Я полагаю, что данная удалённость нашей системы от остальных миров и является одной из основных причин того, что Солнечная система подверглась нападению некой внешней агрессивной цивилизации паразитического типа.

**English:**

Although we were once convinced that the Earth is the center of the universe and all other bodies, including the Sun, revolve around it, today we know that this is far from being the case. Not only does our planet revolve around a star that we call the Sun, but the entire solar system revolves around the center of our Galaxy, called the Milky Way. By galactic measures, our solar system is a distant province, which is not only far from the center, but also away from the central galactic disk. I believe that this distance of our system from other worlds is one of the main reasons that the solar system was attacked by some external aggressive civilization of a parasitic nature.

**Russian:**

Цивилизация наших предков была биогенной, в то время как напавшая на нас цивилизация являлась техногенной. Фундаментальные различия между данными двумя подходами к развитию и взаимодействию с материей и энергией я пытаюсь по мере способностей изложить ещё в одной своей статье "[Прекрасный мир, который мы потеряли](http://mylnikovdm.livejournal.com/4158.html)". В частности, один из выводов в этой статье состоит в том, что техногенная цивилизация может развиваться только постоянно захватывая всё новые и новые миры, поскольку в основе её способов взаимодействия с материей и получения энергии лежит разрушение и экстенсивное использование захваченных ресурсов за короткий промежуток времени, много меньший, чем требуется для их синтеза.

**English:**

The civilization of our ancestors was biogenic, while the civilization that attacked us was anthropogenic. 

In my article [The Beautiful World We Lost](http://mylnikovdm.livejournal.com/4158.html), I try to describe as far as I can the fundamental differences between these two approaches to development and interaction with matter and energy. In particular, one of the conclusions in that article is that a technogenic civilization can only develop by constantly capturing more and more worlds, because the basis of its ways of interacting with matter and obtaining energy is the destruction and extensive use of captured resources in a short period of time. That is, in much less time than is required for their synthesis.

**Russian:**

В тоже время уровень развития нашей биогенной цивилизации на момент нападения был намного выше, чем у нападавших. Поэтому один из вопросов, на который у меня нет ответа, состоит в том, почему они не смогли себя защитить и отразить данное нападение. Возможно, что сила первой атаки оказалась намного мощнее того, на что рассчитывали наши предки.

**English:**

At the same time, the level of development of our biogenic civilization at the time of the attack was much higher than that of the attackers. Therefore, one of the questions I have no answer to is why they could not defend themselves and repel the attack. It is possible that the strength of the first attack was much more powerful than our ancestors had anticipated.

**Russian:**

Инопланетная раса захватчиков обладала возможностью вступать в телепатический контакт с живущими на Земле людьми. При этом они себя выдавали за некую очень могущественную сущность, за Господа, который сотворил небо и землю. Из текстов Библии и Торы известно, что перед всемирным потопом Господь предупреждал всех людей, но никто, кроме Ноя, ему не поверил. Другими словами, многие из людей слышали представителей инопланетной расы, но большинство отказывались с ними сотрудничать.

**English:**

The alien race of invaders had the ability to enter into telepathic contact with people living on Earth. In doing so, they pretended to be a very powerful entity, the Lord, who created the heavens and the earth. It is known from the texts of the Bible and Torah that before the world flood the Lord warned all people, but no one except Noah believed him. In other words, many people heard the representatives of the alien race, but most refused to cooperate with them.

**Russian:**

Когда Ной построил свой «Ковчег», произошёл очень мощный планетарный катаклизм, который сегодня известен как «Великий потоп». Если эта инопланетная цивилизация могла путешествовать между Звёздными системами, то они должны были уметь управлять гравитацией, поскольку остальные известные нам способы передвижения в космическом пространстве непригодны для дальних межзвёздных перелётов. На это же указывают и многочисленные наблюдения за различными НЛО, которые, правда, упорно не признаются официальной наукой и властями. Обладая подобными знаниями и технологиями захватчики смогли изменить траекторию движения крупного небесного тела и вызвать его столкновение в Землёй. В результате этого столкновения и происходит мощный катаклизм, результатом которого является наклон оси вращения Земли и мощная инерционная волна, которая прокатывается по суше, смывая всё на своём пути.

**English:**

After Noah built his "Ark", there was a very powerful planetary cataclysm, which today is known as the "Great Flood". If this alien civilization could travel between start systems, then they would have to be able to manipulate gravity - the other ways we know of traveling in outer space are not suitable for long interstellar flights. This is also indicated by the numerous observations of various UFOs, which are, however, persistently unrecognized by official science and authorities. With this knowledge and technology, the invaders were able to change the trajectory of a large celestial body and cause it to collide with the Earth. The result of this collision was a powerful cataclysm, two results of which are the inclination of the Earth's rotation axis and a powerful inertial wave that rolled over land, washing away everything in its path.

**Russian:**

Точка удара, вероятнее всего, находилась в районе Индийского океана. На это указывает искажение формы гравитационного поля Земли, которое вызвано неравномерностью распределения массы вещества планеты.

**English:**

The point of impact was most likely in the Indian Ocean region. This is indicated by the distortion of the shape of the Earth's gravitational field, which is caused by the uneven mass distribution of the planet's matter.

**Russian:**

10 гравитационная карта Земли

**English:**

10 gravitational chart of the Earth

<!-- Local
[![](How Tartaria was Lost Part 7_files/87588_900.jpg#clickable)](How Tartaria was Lost Part 7_files/87588_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/87588/87588_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/87588/87588_original.jpg)

**Russian:**

На ней хорошо видно, что в районе Индийского океана наблюдается очень сильная гравитационная аномалия, из-за чего уровень Индийского океана более чем на 100 метров ниже, чем правильная геометрическая поверхность идеального эллипсоида. При этом мы также наблюдаем две аномалии в Атлантическом океане и в Тихом океане в районе Папуа-Новая Гвинея, где превышение достигает более 60 метров, что в случае с Атлантическим океаном особенно странно. Данные гравитационные аномалии, скорее всего, вызваны именно смещением внутренних масс вещества планеты вследствие удара в синюю область.

**English:**

It clearly shows that there is a very strong gravitational anomaly in the Indian Ocean area, so the level of the Indian Ocean is more than 100 meters lower than the correct geometric surface of the ideal ellipsoid. However, we also observe two anomalies in the Atlantic Ocean and in the Pacific Ocean in the area of Papua New Guinea, where the exceedance reaches more than 60 meters, which is particularly strange in the case of the Atlantic Ocean. These gravity anomalies are most likely caused by a shift in the internal masses of the planet's matter as a result of the impact in the blue region.

**Russian:**

В следующем ролике можно посмотреть, как это выглядит на трёхмерной модели. При этом необходимо учитывать, что вертикальный масштаб величины отклонения гравитационного поля сильно увеличен, чтобы изображение было более наглядным.

[https://youtu.be/Lq7pXrAjfo4](https://youtu.be/Lq7pXrAjfo4)

**English:**

In the next video, you can see how this looks in a three-dimensional model. Keep in mind that the vertical scale of magnitude of a deviation of the gravitational field has been greatly increased so that the deviation is more obvious.

[https://youtu.be/Lq7pXrAjfo4](https://youtu.be/Lq7pXrAjfo4)

**Russian:**

Для тех, кто не хочет или не имеет возможности посмотреть данный ролик скриншот, на котором наглядно видна «яма» в Индийском океане и выпуклости в Северной Атлантике и в районе Папуа-Новой Гвинеи.

11 равитационное поле Земли шар

**English:**

For those who do not want to or do not have the opportunity to see this video, here is a  screenshot, which clearly shows the "hole" in the Indian Ocean and the swell in the North Atlantic and Papua New Guinea.

11 balloon earth's gravitational field

<!-- Local
[![](How Tartaria was Lost Part 7_files/88219_900.jpg#clickable)](How Tartaria was Lost Part 7_files/88219_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/88219/88219_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/88219/88219_original.jpg)


**Russian:**

Возможность устанавливать телепатический контакт с некоторыми из людей объясняет феномен «пророков», которые описаны в Библии и других «священных текстах». При этом если посмотреть, о чём предупреждают данные «пророки», то в большинстве случаев они, как и Ной в своё время, предупреждают о неких глобальных катаклизмах, которые на самом деле устраиваются теми, кто шлёт им эти послания.

**English:**

The ability to establish telepathic contact with some people explains the phenomenon of "prophets", which are described in the Bible and other "sacred texts". If you look at what these "prophets" warn about, then in most cases they, like Noah in his time, warn about some global cataclysms, which are actually created by those who send them these messages.

**Russian:**

Подобных катаклизмов в той же Библии описано великое множество.

**English:**

A great many similar cataclysms are described in the same Bible:

**Russian:**

> 24. И пролил Господь на Содом и Гоморру дождем серу и огонь от Господа с неба,
> 25. и ниспроверг города сии, и всю окрестность сию, и всех жителей городов сих, и произрастания земли.
> 26. Жена же Лотова оглянулась позади его, и стала соляным столпом.
> 27. И встал Авраам рано утром и пошел на место, где стоял пред лицем Господа,
> 28. и посмотрел к Содому и Гоморре и на все пространство окрестности и увидел: вот, дым поднимается с земли, как дым из печи. 

*(Книга Бытие 19:24-28)*

**English:**

> 24. And the Lord shed rain on Sodom and Gomorrah with sulphur and fire from the Lord from heaven,
> 25. and overthrew this city, and all this vicinity, and all the inhabitants of this city, and the growths of the earth.
> 26. And Lot's wife looked behind him, and became a pillar of salt.
> 27. And Abraham rose up early in the morning, and went to the place where he stood before the face of the Lord;
> 28. And looked to Sodom and Gomorrah, and to the whole of the countryside, and saw, Behold, smoke rises from the earth like the smoke of a furnace. 

*(Book of Genesis 19:24-28)*


**Russian:**

В данном месте для меня лично не совсем понятно, что произошло в строфе 26, где сообщается, что жена Лотова стала соляным столпом, оглянувшись на происходящее в Содоме и Гоморре. В остальном же мы имеем описание планетарной бомбардировки из космоса, на что явно указывается «огонь от Господа с неба».

**English:**

In this place, for me personally, it is not quite clear what happened in verse 26, which says that Lotov's wife became a pillar of salt, looking back at what was happening in Sodom and Gomorrah. Otherwise, we have a description of the planetary bombardment from space, which is clearly indicated by "fire from the Lord from heaven".


**Russian:**

> 10. Господь привел их в смятение при виде Израильтян, и они поразили их в Гаваоне сильным поражением, и преследовали их по дороге к возвышенности Вефорона, и поражали их до Азека и до Македа.
> 11. Когда же они бежали от Израильтян по скату горы Вефоронской, Господь бросал на них с небес большие камни до самого Азека, и они умирали; больше было тех, которые умерли от камней града, нежели тех, которых умертвили сыны Израилевы мечом.

*(Книга Иисуса Навина 10:10,11)*

**English:**

> 10. The Lord disturbed them at the sight of the Israelites, and they smote them in Havana, and persecuted them on their way to the hilltop of Bethoron, and smote them to Azek and to Macedonia.
> 11. And it came to pass, as they fled from the Israelites upon the sting of Mount Bethoron, that the Lord threw great stones upon them from heaven, as far as Azek, and they died; there were more of those who died of the stones of hail than of those whom the children of Israel killed with the sword. 

*(Joshua 10:10,11).*


**Russian:**

Опять описана метеоритная бомбардировка поверхности планеты «бросал камни с небес».

**English:**

The meteoriter-bombing of the planet's surface "threw stones from the sky" is described again.

**Russian:**

> 18. И будет в тот день, когда Гог придет на землю Израилеву, говорит Господь Бог, гнев Мой воспылает в ярости Моей.
> 19. И в ревности Моей, в огне негодования Моего Я сказал: истинно в тот день произойдет великое потрясение на земле Израилевой.
> 20. И вострепещут от лица Моего рыбы морские и птицы небесные, и звери полевые и все пресмыкающееся, ползающее по земле, и все люди, которые на лице земли, и обрушатся горы, и упадут утесы, и все стены падут на землю.
> 21. И по всем горам Моим призову меч против него, говорит Господь Бог; меч каждого человека будет против брата его.
> 22. И буду судиться с ним моровою язвою и кровопролитием, и пролью на него и на полки его и на многие народы, которые с ним, всепотопляющий дождь и каменный град, огонь и серу...

*(Иезекииль 38:18-22)*

> 4. Господь во святом храме Своем, Господь, — престол Его на небесах, очи Его зрят; вежды Его испытывают сынов человеческих.
> 5. Господь испытывает праведного, а нечестивого и любящего насилие ненавидит душа Его.
> 6. Дождем прольет Он на нечестивых горящие угли, огонь и серу; и палящий ветер — их доля из чаши… 

*(Псалтирь 10:4-6)*

**English:**

> 18. And it shall be on that day, when Gog shall come to the land of Israel, saith the Lord God, My wrath shall blaze in My wrath.
> 19. And in my jealousy, in the fire of my indignation, I said, Verily in that day a great shock will come upon the land of Israel.
> 20. And the sea, and the birds of the sky, and the beasts of the field, and all the reptiles that crawl upon the earth, and all the people that are upon the face of the earth, and the mountains that fall, and the cliffs that fall, and all the walls that fall upon the earth, shall come down.
> 21. And upon all my mountains I will call a sword against it, saith the Lord God; every man's sword shall be against his brother.
> 22. And I will sue him with the pestilence of the sea and with bloodshed, and I will shed on him and on his shelves, and on many nations that are with him, and the rain and the hail of stone, and the fire and the sulfur...

*(Ezekiel 38:18-22)*

> 4. The Lord in His holy temple, the Lord, is His throne in heaven, His eyes are seen; His winds are tested by the sons of men.
> 5. The Lord tests the righteous, but the wicked and loving violence hates His soul.
> 6. He will spill burning coals, fire and sulfur on the wicked; and the scorching wind is their share of the cup...

**Russian:**

В данном фрагменте имеется прямое указание на то, что «престол Его на небесах», то есть где-то в космосе, не на Земле. Также интересно указание на «палящий ветер», что похоже на высокотемпературную ударную волну.

**English:**

In this fragment there is a direct indication that "His throne is in heaven", that is, somewhere in space, not on Earth. Also interesting is the indication of a "scorching wind", which looks like a high-temperature shock wave.

**Russian:**

> 8. Потряслась и всколебалась земля, дрогнули и подвиглись основания гор, ибо разгневался Бог; 
> 9. поднялся дым от гнева Его и из уст Его огонь поядающий; горячие угли сыпались от Него. 
> 10. Наклонил Он небеса и сошел, — и мрак под ногами Его.
> 11. И воссел на Херувимов и полетел, и понесся на крыльях ветра.
> 12. И мрак сделал покровом Своим, сению вокруг Себя мрак вод, облаков воздушных.
> 13. От блистания пред Ним бежали облака Его, град и угли огненные.
> 14. Возгремел на небесах Господь, и Всевышний дал глас Свой, град и угли огненные.
> 15. Пустил стрелы Свои и рассеял их, множество молний, и рассыпал их.

*(Псалтирь 17:8-15)*

> 6. Господь Саваоф посетит тебя громом и землетрясением, и сильным гласом (сильный шум, звук), бурею и вихрем, и пламенем всепожирающего огня. 

*(Исаия 29:6)*

**English:**

> 8. The earth trembled and shook, and the foundations of the mountains trembled and moved, for God was wrathful; 
> 9. the smoke of His wrath rose up, and there was fire from His mouth; hot coals fell from Him. 
> 10. He bowed down the heavens and came down, and the darkness under His feet. 
> 11 And He sat down on the Cherubim and flew, and carried on the wings of the wind. 
> 12. And the darkness made His cover, the darkness of the waters, the clouds of the air around Himself. 
> 13. From his shining before him ran his clouds, hail and embers of fire. 
> 14. The Lord warmed up in heaven, and the Most High gave His voice, hail and coals of fire. 
> 15. And He fired His arrows and scattered them, and scattered them, and scattered them. 

*(Psalter 17:8-15.)*

> 6. The Lord of hosts shall visit you with thunder and earthquake, and with a strong voice (strong noise, sound), storm and whirlwind, and the flame of all-expanding fire. 

*(Isaiah 29:6)*

**Russian:**

Интересно, что данное описание подходит как под падение крупного метеорита, который на большой скорости ударяется в поверхность Земли и взрывается, при этом выделяется очень много энергии, что по своим последствиям будет похоже на ядерный взрыв. Но это также похоже и на описание ядерного или термоядерного взрыва, которые тоже в то время могут быть описаны термином «пламя всепожирающего огня».

**English:**

Interestingly, this is an appropriate description of the fall of a large meteorite, which hits the surface of the Earth at high speed and explodes, while emitting a lot of energy, and which in its effects will be similar to a nuclear explosion. But it is also similar to the description of a nuclear or thermonuclear explosion, which can also be described by the term "flame of an all-expanding fire" at that time.

**Russian:**

Много схожих описаний и в откровениях Иоанна Богослова, которые известны также как «Апокалипсис». При этом я, как и некоторые другие исследователи, считаю, что в Апокалипсисе описаны не будущие, а уже произошедшие события, поскольку описание это очень подробное и детальное. Но мы к нему ещё вернёмся чуть позже. Пока же из всех этих «священных» текстов следует, что это именно «Господь» насылает с небес все эти катаклизмы, при этом они не являются случайными, поскольку в большинстве случаев «пророки» заранее получают от Господа предупреждение о том, что в том или ином месте должна произойти катастрофа, как это было с Ноем, Лотом или Иисусом Навиным.

**English:**

There are many similar descriptions in John the Theologian's revelations, also known as the "Apocalypse". However, I, like some other researchers, believe that the Apocalypse does not describe future events, but events that have already occurred, because the description is very detailed. However, we will return to it a little later. In the meantime, it follows from all these "sacred" texts that it is the "Lord" who sends all these cataclysms from heaven, and they are not accidental, because in most cases the "prophets" receive a warning from the Lord in advance that a catastrophe will happen in a certain place, as it was with Noah, Lot and Joshua.

**Russian:**

Как следует из книги «Бытие», в конечном итоге между инопланетной расой, выдающей себя за «Господа» и иудеями был заключён договор. Иудеи признают «Господа» в качестве Бога, то бишь верховной власти, и обязуются исполнять все его требования, а за это племя иудеев становится проводником их воли на Земле и получает с их помощью власть над всеми странами и народами.

**English:**

As follows from Genesis, in the end, a treaty was made between the alien race posing as "the Lord" and some of Earth's people. These people recognized the "Lord" as God, i.e. as the supreme authority, and undertook to fulfill all his demands, and for this reason their tribe becomes the conductor of the parasite's will on Earth. With their help the parasites gain power over all countries and peoples.

**Russian:**

Вот как об этом сказано в Библии:

**English:**

This is what the Bible says:

**Russian:**

> 7. и поставлю завет Мой между Мною и тобою и между потомками твоими после тебя в роды их, завет вечный в том, что Я буду Богом твоим и потомков твоих после тебя; 
> 8. и дам тебе и потомкам твоим после тебя землю, по которой ты странствуешь, всю землю Ханаанскую, во владение вечное; и буду им Богом. 

*(Книга Бытие 17:7,8)*

**English:**

> 7. And I will put my covenant between me and thee, and between thy descendants after thee into their generations, an everlasting covenant in that I will be thy God and thy descendants after thee; 
> 8. And I will give thee and thy descendants after thee the land on which thou wandest, all the land of Canaan, into everlasting possession; and I will be their God.

*(Book of Genesis 17:7,8)*

**Russian:**

При этом я полагаю, что племя иудеев было выбрано не случайно. Видимо с ними у захватчиков лучше всего получалось устанавливать телепатическую связь и они лучше, чем остальные, поддавались контролю с их стороны. То есть, как и говорится в Библии и Торе, иудеи являются народом, избранным «Господом», вот только истинная причина этой избранности в их хорошей управляемости.

**English:**

However, I believe that the tribe was not chosen by chance. Apparently, the invaders were best able to establish telepathic communication with them, and they were better controlled than the others. That is, as the Bible and Torah say, they are a people chosen by "the Lord", but the true reason for this choice is their good controllability.

**Russian:**

Почему захватчики не уничтожили все людей, чтобы спокойно использовать ресурсы планеты? Ответ на этот вопрос также можно найти в Библии.

**English:**

Why didn't the invaders destroy all humans to use the planet's resources in peace? The answer to this question can also be found in the Bible.

**Russian:**

> 24. И остался Иаков один. И боролся Некто с ним до появления зари; 
> 25. и, увидев, что не одолевает его, коснулся состава бедра его и повредил состав бедра у Иакова, когда он боролся с Ним. 
> 26. И сказал: отпусти Меня, ибо взошла заря. Иаков сказал: не отпущу Тебя, пока не благословишь меня. 
> 27. И сказал: как имя твое? Он сказал: Иаков. 
> 28. И сказал: отныне имя тебе будет не Иаков, а Израиль, ибо ты боролся с Богом, и человеков одолевать будешь. 
> 29. Спросил и Иаков, говоря: скажи имя Твое. И Он сказал: на что ты спрашиваешь о имени Моем? И благословил его там. 
> 30. И нарек Иаков имя месту тому: Пенуэл; ибо, говорил он, я видел Бога лицем к лицу, и сохранилась душа моя. 
> 31. И взошло солнце, когда он проходил Пенуэл; и хромал он на бедро свое. 

*(Книга Бытие 32:24-31)*

**English:**

> 24. And Jacob was left alone. And Jacob struggled with him until the dawn appeared; 
> 25. And when he saw that he could not overcome him, he touched the composition of his thigh and damaged the composition of Jacob's thigh when he struggled with him. 
> 26. And he said, "Let me go, for the dawn has risen. James said, "I will not let you go until you have blessed me. 
> 27. And he said, How is your name? He said, "Jacob. 
> 28. And he said, "From now on your name will not be Jacob, but Israel, for you have struggled with God, and you will defeat men. 
> 29. Jacob also asked, saying, "Say your name. And he said, "What do you ask of my name? And he blessed him there. 
> 30. And Jacob called his name to the place: Penuel; for, he said, I saw God face to face, and my soul was preserved. 
> 31. And the sun rose as he passed Penuel; and he limped on his thigh. 

*(Book of Genesis 32:24-31)*

**Russian:**

В данном фрагменте очевидно, что так называемый «Некто», с которым боролся Иаков, является одним из «ангелов», представителем того самого «Господа». При этом он явно боится Солнечного света. Если покопаться в мифологии и сказаниях разных народов, то можно узнать, что у этих тварей солнечный свет вызывает сильные ожоги внешних кожных покровов, поэтому они пытаются избегать солнечного света.

**English:**

In this fragment it is evident that the so-called "One", with whom James struggled, is one of the "angels", the representative of the "Lord". It is obviously afraid of sunlight. If you dig through the mythology and legends of different peoples, you can learn that sunlight causes these creatures severe burns on the outer skin, so they try to avoid sunlight.

**Russian:**

Другими словами, расе инопланетных захватчиков не подходит свет нашей Звезды, поэтому они стараются его избегать. Видимо поэтому они решили, что им выгоднее создать рабов и надсмотрщиков из местного населения. Вторая вероятная причина может состоять в том, что самих захватчиков сравнительно мало, а миров, которые они пытаются использовать в качестве источников ресурсов, они захватили уже много. Поэтому использование местного населения, скорее всего несколько генетически модифицированного, в качестве рабочей силы на поверхности планеты является у них стандартной практикой использования захваченных миров.

**English:**

In other words, the light of our star does not suit the race of alien invaders so they try to avoid it. Apparently, that's why they decided it was more profitable for them to create slaves and wardens from the local population. The second probable reason could be that the invaders themselves are relatively few, and they have already captured many worlds they are trying to use as sources of resources. Therefore, the use of local populations, most likely a few genetically modified, as manpower on the planet's surface is their standard practice of using invaded worlds.

**Russian:**

При этом большая их часть остаётся на космическом корабле или космической станции, то есть за пределами Земли. Поэтому мы и встречаем фразу «престол Господа на небесах» и периодические указания на то, что «Господь с небес наблюдает за грешниками».

**English:**

In this case, most of them remain on the spacecraft or space station, that is, outside the Earth. That is why we find the phrase "the throne of the Lord in heaven" and periodic indications that "the Lord from heaven watches over sinners".

**Russian:**

Из-за того, что захватчикам не подходит свет Солнца, они вынуждены плотно закрывать своё тело от Солнечных лучей, поэтому находясь на поверхности днём, они вынуждены будут всегда использовать одежду, в том числе прикрывающую голову и лицо. Например такую, какую используют монахи-католики.

**English:**

Due to the fact that the sunlight does not suit the invaders, they are forced to tightly cover their bodies from the rays of the sun, so being on the surface during the day, they will always have to use clothing, covering their head and face. Like the clothes used by Catholic monks.

**Russian:**

12 монах

**English:**

12 Monk

<!-- Local
![](How Tartaria was Lost Part 7_files/88325_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/88325/88325_original.jpg#resized)

**Russian:**

А чтобы не сильно отличаться от остальных, «Господь» вводит запрет на обнажённое тело, объявляя наготу грехом.

**English:**

And so as not to be too different from the others, "the Lord" imposes a ban on the naked body, declaring nudity a sin.

**Russian:**

Также следует отметить, что у большинства народов, включая Православных, в мифологии именно слуги Дьявола, или «нечистая сила», боятся Солнечного света. При этом Дьявол со своими слугами выступают против Бога и пытаются подчинить себе Души людей. Те, кто соглашается продать свою Душу дьяволу и присягает ему на верность, становится его слугой и попадает в число избранных, элиты, которая управляет данным миром.

**English:**

It should also be noted that in the mythology of the majority of peoples, including Orthodox, it is the Devil's servants, or "unclean power", that are afraid of sunlight. Thus the Devil with the servants act against the God and tries to subordinate to itself people's souls. Those who agree to sell their souls to the devil and swear allegiance to him, become his servant and join the elect, the elite who rule this world.

**Russian:**

На то, что «Господь» иудеев и христиан, которому они поклоняются, на самом деле и является тем «Дьяволом», нечистой силой, захватившей Землю, указывают также те деяния, которые во множестве описаны в Библии. Защищая иудеев, которые присягнули ему на верность, а также помогая им занять господствующее положение на Земле, «Господь» в основном совершает только всякие гадости и мерзости. Отравляет воду, убивает невинных младенцев в Египте, устраивает множество глобальных и локальных катаклизмов с помощью метеоритных и/или ядерных бомбардировок поверхности планеты, которые приводят к гибели всего живого, включая людей. То есть, всё это совершенно не похоже на деяния настоящего Бога, сотворившего всю Вселенную, нашу Солнечную систему и нашу планету, и который, как учит в Новом завете Иисус, есть Любовь.

**English:**

The fact that the "Lord" worshipped by the Abrahamic religions is, in fact, the "Devil", the unclean force that has taken over the earth, is also indicated by the acts described in the bible. By defending the people who swore allegiance to him and helping them to dominate the earth, "the Lord" basically produces nothing but abomination and vice. Poisons water, kills innocent babies in Egypt, arranges many global and local cataclysms by means of meteor and/or nuclear bombardments of the planet's surface, which leads to the death of anything alive, including people. That is, all this is absolutely not like the acts of the real God, who created the whole Universe, our solar system and our planet, and who, as Jesus teaches in the New Testament, is Love.

<!--
**Russian:**

### Часть 7б: Как погибла Тартария? Часть 7б. Заключительная

**English:**

### Part 7b: How did Tartaria die? Part 7b. Conclusion.

Translated from:

[https://mylnikovdm.livejournal.com/6639.html](https://mylnikovdm.livejournal.com/6639.html)


28 November, 2014
-->

**Russian:**

Выводы о том, что по Земле был нанесён метеоритно-ядерный удар некоторые назовут бредом, поскольку им сложно принять эту информацию. Слишком уж она не вписывается в ту картину Мира, которая сложилась в их восприятии. Многие из таких людей будут упираться до последнего, цепляясь за свои стереотипы восприятия, даже не смотря на то, что подобной информации начинает появляться всё больше и больше.

**English:**

Some will call the conclusion that a meteorite nuclear strike has been delivered on Earth delusional because it is difficult for them to accept this information. Too much of it doesn't fit into the picture of the world as they see it. Many of these people will cling to the last, clinging to their perception stereotypes, even though more and more of this information is beginning to appear.

**Russian:**

Вот совсем свежая новость, которая появилась на портале РИА Новости 26 ноября 2014 года:

**English:**

Here is the latest news, which appeared on RIA Novosti portal on November 26, 2014:


**Russian:**

> Древняя жизнь на Марсе могла быть уничтожена из-за использования ядерного оружия инопланетными цивилизациями, полагает профессор физики плазменных частиц, экс-сотрудник Командования воздушно-космической обороны США (Space Missile Defense) американский исследователь Джон Бранденбург. Издание International Business Insider приводит выдержки из научной работы исследователя, в которой он приходит к заключению, что в околоземном космическом пространстве могли существовать силы или создания, "враждебные к молодым цивилизациям, вроде земной", и обладавшие ядерным потенциалом, в результате использования которого, предположительно, и была уничтожена древняя жизнь на Марсе. 

> Бранденбург подчеркнул, что есть явные доказательства наличия внушительных ядерных взрывов на Марсе, уничтоживших древние поселения в Сидонии и Утопии — двух частях Марса. По его мнению, взрывы были спланированы и проведены высокоразвитыми существами, вероятно, инопланетянами. Свидетельством же наличия фактов взрыва является цвет поверхности планеты — красный", — приводит газета аргументацию, содержащуюся в научной работе. Недавно, отмечает IBT, орбитальный корабль космической одиссеи НАСА (NASA) смог передать данные о наличии высокой концентрации изотопов ксенона-129 в атмосфере Марса. Сходная концентрация изотопов была обнаружена и на Земле после инцидентов, вроде Чернобыльской ядерной катастрофы. 

> Принимая во внимание число изотопов в атмосфере Марса, сходное с теми, что фиксировались после ядерных катаклизмов на Земле, Марс может являться примером цивилизации, стертой с поверхности в результате ядерной атаки из космоса", — говорится в исследовании Бранденбурга."

[Первоисточник: http://ria.ru/world/20141126/1035237448.html](http://ria.ru/world/20141126/1035237448.html)

[То же на английском языке: http://au.ibtimes.com/articles/573878/20141125/nuclear-weapons-attack-explosions-aliens-life-mars.htm](http://au.ibtimes.com/articles/573878/20141125/nuclear-weapons-attack-explosions-aliens-life-mars.htm)

**English:**

> Ancient life on Mars could have been destroyed because of the use of nuclear weapons by alien civilizations, believes John Brandenburg, professor of plasma particle physics, a former employee of the U.S. Space Missile Defense Command. 

This edition of International Business Insider quotes excerpts from the scientific work of the researcher, in which he concludes that there may have been forces or creatures in near-Earth space, "hostile to young civilizations, like Earth", and possessing nuclear potential, which supposedly resulted in the destruction of ancient life on Mars. 

> Brandenburg stressed that there is clear evidence of impressive nuclear explosions on Mars, which destroyed the ancient settlements in Sidonia and Utopia, two parts of Mars. In his view, the explosions were planned and carried out by highly developed creatures, probably aliens. The evidence of the facts of the explosion is the color of the planet's surface - red - the newspaper argues in the scientific work. Recently, according to IBT, the orbiting spacecraft Odyssey NASA (NASA) was able to transmit data on the presence of high concentrations of xenon-129 isotopes in the atmosphere of Mars. Similar concentrations of isotopes were found on Earth after incidents like the Chernobyl nuclear disaster. 

> Given the number of isotopes in the Mars atmosphere similar to those recorded after nuclear disasters on Earth, Mars may be an example of a civilization wiped from the surface by a nuclear attack from space,"

the Brandenburg study says.

[Source: http://ria.ru/world/20141126/1035237448.html](http://ria.ru/world/20141126/1035237448.html)

The same article in English: [http://au.ibtimes.com/articles/573878/20141125/nuclear-weapons-attack-explosions-aliens-life-mars.htm](http://au.ibtimes.com/articles/573878/20141125/nuclear-weapons-attack-explosions-aliens-life-mars.htm)

**Russian:**

Бывший сотрудник Командования воздушно-космической обороны США тоже бредит, как я или Алексей Кунгуров? Или может быть далеко не всё, что нам рассказывали в школе, институте или с экранов телевизоров является правдой?

Тоже самое скептики говорят и по поводу телепатии. Это не может быть, потому что не может быть никогда. А вы не задумывались, для чего нам так усиленно внушают мысль, что телепатии не существует? Может быть это на самом деле кому-то выгодно?

**English:**

Is a former member of the U.S. Aerospace Defense Command as delusional as me and Alexei Kungurov? Or maybe not everything we've been told at school, university, or on TV is true?

The same skeptics say about telepathy: it can't be, because it can't ever be. Have you ever wondered why we are so intensely focused on the idea that telepathy does not exist? Maybe it's really beneficial for someone.

**Russian:**

Да и так ли уж она невозможна в принципе?

13 нейроинтерфейс

**English:**

And is it really impossible in principle?

13 Neurointerface

<!-- Local
![](How Tartaria was Lost Part 7_files/88823_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/88823/88823_original.jpg#resized)

**Russian:**

В настоящее время активно ведутся работы по созданию нейроинтерфейсов для установления прямой связи между мозгом и компьютером. Ещё в 2008 году появилась статья о том, что японские учёные смогли считать изображение непосредственно с коры головного мозга [http://www.membrana.ru/particle/1937](http://www.membrana.ru/particle/1937).

В 2013 году появилась новость о том, что был разработан усовершенствованный алгоритм декодирования подобных изображений [http://habrahabr.ru/post/175627/](http://habrahabr.ru/post/175627/).

**English:**

At present, work is actively being done to create neuro-interfaces to establish a direct link between the brain and the computer. Back in 2008, there was an article about how Japanese scientists were able to count images directly from the cortex: [http://www.membrana.ru/particle/1937](http://www.membrana.ru/particle/1937)

In 2013, there was news that an improved algorithm for decoding such images was developed: [http://habrahabr.ru/post/175627/](http://habrahabr.ru/post/175627/).

**Russian:**

В сентябре этого года испанские учёные провели эксперимент, который мировые СМИ сразу же назвали не иначе как первым примером телепатии [http://www.gazeta.ru/science/2014/09/08_a_6206081.shtml](http://www.gazeta.ru/science/2014/09/08_a_6206081.shtml) с использованием всё тех же нейроинтерфесов, компьютера и глобальной сети интернет для передачи информации. При этом в данном эксперименте интернет вполне мог быть заменён на систему радиопередатчик-радиоприёмник.

**English:**

In September of this year, Spanish scientists conducted an experiment that the world's media immediately called the first example of telepathy - [http://www.gazeta.ru/science/2014/09/08_a_6206081.shtml](http://www.gazeta.ru/science/2014/09/08_a_6206081.shtml) - using the same neuro-interfaces, computer and global Internet network to transmit information. In this experiment, the Internet could just as easily have been a radio transmitter-receiver system.

**Russian:**

Да, пока первые результаты весьма примитивны, но вспомните про то, какого качества были первые телевизионные сигналы, которые пытались передавать. Причём первым это сделал наш русский учёный Борис Розинг 9 мая 1911 года, и передавал он тоже изображения простейших фигур. И посмотрите на качество изображение, которого мы достигли за каких-то 103 года.

**English:**

Yes, so far the first results are quite primitive, but remember the quality of the first television signals that we tried to transmit. And our Russian scientist Boris Rosing did it first on May 9, 1911. He also transmitted images of the simplest figures. And now look at the quality of the image that we achieved after some 103 years.

**Russian:**

Поэтому, на мой взгляд, вполне логично предположить, что цивилизация, которая находится дальше на по уровню техногенного развития, может обладать гораздо более совершенными подобными устройствами.

**English:**

That is why, in my opinion, it is quite logical to suppose that a civilization which is further on the level of technogenic development can possess much more perfect such devices.

**Russian:**

Отдельно хочу заметить, что в данном случае мы рассматриваем нейроинтерфейс между компьютером и мозгом человека, которые являются принципиально разными структурами с точки зрения своего построения и обработки информации. Если же взаимодействуют две однородных системы, то есть два мозга, то установление связи должно быть проще, поскольку не требуется преобразовывать сигналы из одной формы записи, передачи и обработки в другую.

**English:**

Separately, I would like to note that in this case we consider the neuro-interface between the human computer and brain, which are fundamentally different structures in terms of their construction and processing of information. If two homogeneous systems, i.e. two brains, interact, then it should be easier to establish connection, as it is not necessary to convert signals from one form of recording, transmission and processing into another.

**Russian:**

Почему вас не удивляет, что два компьютерных устройства могут обмениваться информацией на прямую друг с другом без проводов, но при этом вы не допускаете мысли, что подобная возможность есть у такой гораздо более совершенной системы, как человеческий мозг?

**English:**

Why aren't you not surprised that two computer devices can exchange information directly with each other without wires, but you don't admit that such a possibility exists in a much more perfect system such as the human brain?

**Russian:**

Ещё одну большую группу вопросов, которые регулярно встречаются в комментариях, можно свести к одному: «почему ты так уверен, что это не природное событие, а злой умысел?».

**English:**

Another large group of questions that regularly appear in the comments to my articles can be summarized to one: "Why are you so sure that this is not a natural event, but an evil intention?

**Russian:**

Например потому, что мы об этом событии ничего не знаем. На первый взгляд это может показаться странным, но именно тот факт, что кто-то постарался стереть из памяти людей и из нашей истории упоминания о грандиозной катастрофе, которая произошла всего 200 лет назад, сам по себе указывает на то, что это не было случайным событием. Причём это не могло быть сделано без участия правящей элиты, поскольку именно она контролировала и контролирует распространение информации в обществе. Та же цензура в Романовской Российской Империи была сильнее, чем во времена СССР, о чём есть масса свидетельств.

**English:**

For instance, because we don't know anything about this event. It may seem strange at first glance, but the fact that someone has tried to erase from people's memory and from our history references to the grand catastrophe that occurred just 200 years ago, in itself indicates that it was not a random event. And it could not have been done without the participation of the ruling elite, because it was they who controlled and supervised the spread of information in society. The same censorship in the Romanov Russian Empire was stronger than in the Soviet times, and there is a lot of evidence of this.

**Russian:**

Сама катастрофа происходит очень во время, но это если рассматривать происходящие события не с точки зрения официального исторического мифа, который начинает трещать по швам под грузом тех фактов, которые уже вскрыты. Только по 19 веку мы имеем массу искажений. Бородинская битва, судя по последним изысканиям, произошла в 1867 году, а в 1812 году Романовская Империя со столицей в захваченном ими Санкт-Петербурге ведёт войну с Московской Тартарией, являющейся европейской частью Великой Тартарии, о чём я уже писал в своей заметке [Примерная реконструкция событий 18-19 века. Рабочий вариант](http://mylnikovdm.livejournal.com/3300.html), поэтому не буду здесь повторятся ещё раз.

**English:**

The catastrophe itself took place at a very opportune time, but that is if we consider the events taking place not in terms of the official historical myth, which is starting to crack at the seams under the load of the facts that have already been revealed. Only in the 19th century do we have a lot of distortions. The Battle of Borodino, judging by the latest research, took place in 1867, and in 1812 the Romanov Empire, with its capital in the captured St. Petersburg, is at war with the Moscow Tartaria, which is the European part of Great Tartaria, as I have already written about in my note: [An approximate reconstruction of the events of the 18th-19th century. Working version](http://mylnikovdm.livejournal.com/3300.html), so I will not repeat it here.

**Russian:**

Если мы на самом деле толком не знаем, что происходило 200 лет назад, то что говорить о более ранних временах? Один из базовых принципов научного метода исследований гласит, что если находится факт, который противоречит вашей теории, то это означает, что ваша теория неверна или неполна и нуждается в доработке. Но этот принцип работает везде, кроме истории. Сталкиваясь с неудобными фактами, они историками просто отбрасываются. При этом часто в качестве фактов нам выдают то, что на самом деле фактами не является, например, подложные документы или новоделы, такие как «Колизей» в Риме, построенный в 18 веке и достраиваемый до сих пор, или Стоунхэндж в Великобритании, который вообще построили в 50-х годах 20-го века, и которые пытаются выдать за древние сооружения.

**English:**

If we don't really know what was going on 200 years ago, what about earlier times? One of the basic principles of the scientific research method is that if there is a fact that contradicts your theory, it means that your theory is wrong or incomplete and needs to be refined. This principle works everywhere except in history. When faced with uncomfortable facts, historians simply throw them away. They often give us facts that are not actually facts, such as fake documents or innovations, such as the Colosseum in Rome, built in the 18th century and still being completed, or Stonehenge in Britain, which was generally built in the 50s of the 20th century, and which they are trying to pass off as ancient buildings.

**Russian:**

В тоже время у нас на Земле полно сооружений, происхождение которых официальный исторический миф объяснить не в состоянии, но это «учёных»-историков совершенно не смущает.

**English:**

At the same time we have a lot of constructions on the Earth, the origin of which the official historical myth can't explain, but this doesn't confuse "scientists"-historians at all.

**Russian:**

Вот, например, две статьи о развалинах города Пинара в Турции, куда не водят туристов. [Часть 1](http://kadykchanskiy.livejournal.com/313482.html), [часть 2](http://kadykchanskiy.livejournal.com/313833.html).

Там много интересных фотографий, но я приведу тут только одну.

14 Амфитеатр Турция

**English:**

Here are, for example, two articles about the ruins of the city of Pinara in Turkey, where no tourists are taken. [Part 1](http://kadykchanskiy.livejournal.com/313482.html), [Part 2](http://kadykchanskiy.livejournal.com/313833.html).

There's a lot of interesting pictures, but I'll only bring one.

14 Amphitheatre Turkey

<!-- Local
![](How Tartaria was Lost Part 7_files/89058_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/89058/89058_original.jpg#resized)

**Russian:**

Особенность данного сооружения в том, что все эти ступени являются монолитом, а сама чаша выдолблена в скале из базальта! Для справки, базальт является достаточно твёрдой горной породой вулканического происхождения, фактически застывшей вулканической лавой. Для обработки базальта требуется специальный инструмент. Современная высоколегированная инструментальная сталь его не берёт, не говоря уже о древних железе или бронзе. Официальный исторический миф относит данное сооружение к временам Римской Империи, но при этом ничего не объясняется про то, каким образом в то время могло быть изготовлено подобное сооружение. Ну, согнали побольше рабов, делов-то!

**English:**

The peculiarity of this construction is that all these steps are monolithic, and the bowl itself was hollowed out of basalt rock! For reference, basalt is a quite hard rock of volcanic origin - it is actually frozen volcanic lava. To process basalt, a special tool is required. Modern high-alloy tool steel does not cut it, not to mention ancient iron or bronze. The official historical myth refers to the times of the Roman Empire, but there is no explanation for how such a structure could have been made at that time. Well, more slaves were working away, the standard business!

**Russian:**

Вот ещё один объект, который плохо вписывается в официальный исторический миф. Каменные развалины огромного города, известного как «Южная Пальмира». Как известно, Санкт-Петербург также часто называют «Северная Пальмира» (или быть может правильнее северные пол мира?). Подробнее об этом объекте можно почитать и посмотреть массу фотографий по ссылке [http://kadykchanskiy.livejournal.com/305585.html](http://kadykchanskiy.livejournal.com/305585.html), здесь же я приведу только одну фотографию с общим видом.

**English:**

Here's another object that doesn't fit the official historical myth. The stone ruins of a huge city known as "South Palmyra." As you know, St. Petersburg is also often called the "Northern Palmyra" (or maybe more correctly the northern half of the world?). You can read more about this site and see a lot of photos at the link [http://kadykchanskiy.livejournal.com/305585.html](http://kadykchanskiy.livejournal.com/305585.html), here I will bring only one photo with a general view.

**Russian:**

14 Южная Пальмира

**English:**

14 South Palmyra

<!-- Local
[![](How Tartaria was Lost Part 7_files/89168_900.jpg#clickable)](How Tartaria was Lost Part 7_files/89168_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/89168/89168_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/89168/89168_original.jpg)

**Russian:**

Огромный каменный город, который кто-то умудрился построить посредине пустыни? Или когда его строили, то пустыни там не было? Но официальный исторический миф ничего вразумительного об этом не сообщает. Оазис в пустыне на пути торговых караванов. То, что с экономической точки зрения постройка подобного города в пустыне в древние века была невозможной, если только город не стоял на россыпях золота, никого из историков, видимо, не смущает. Они ведь не экономисты и в таких тонкостях разбираться не обязаны.

**English:**

A huge stone city that someone managed to build in the middle of the desert? Or perhaps the desert wasn't there when it was built? But the official historical myth doesn't make any sense out of this. An oasis in the desert on the road of trade caravans. The fact that, from an economic point of view, building such a city in the desert in the ancient ages was impossible, unless the city was standing on gold placers, does not seem to confuse any historian. They are not economists and do not have to understand such subtleties.

**Russian:**

Но если про данные объекты официальный исторический миф пытается придумать хоть какие-то отговорки, то благодаря всевидящему оку Гугла сейчас стало возможно отыскать такие объекты, которые на официальном историческом мифе ставят большой и жирный крест. Например, на дне Средиземного моря.

**English:**

But if the official historical myth is trying to come up with some excuses about these objects, then thanks to the all-seeing eye of Google it is now possible to find objects that put a big and fat cross on the official history myth. For example, at the bottom of the Mediterranean Sea:

**Russian:**

15 Общая схема Средиземное море

**English:**

15 Overall view of the Mediterranean Sea

<!-- Local
[![](How Tartaria was Lost Part 7_files/89530_900.jpg#clickable)](How Tartaria was Lost Part 7_files/89530_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/89530/89530_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/89530/89530_original.jpg)

**Russian:**

На данной схеме я красной пунктирной линией обозначил положение объекта, а прямоугольниками с номерами выделил фрагменты, которые показаны в увеличенном масштабе ниже. Зелёной пунктирной линией показан второй подобный объект, отходящий от первого. Сам объект представляет из себя девять параллельных «борозд», которые идут по дну Средиземного моря.

**English:**

In this diagram, I have marked the position of the object with a red dotted line, and I have highlighted the fragments with numbers in rectangles, which are shown in an enlarged scale below. A green dotted line shows a second similar object, separated from the first. The object itself consists of nine parallel "furrows", which run along the bottom of the Mediterranean Sea.


**Russian:**

15 Фрагмент 01

**English:**

15 Fragment 01

<!-- Local
[![](How Tartaria was Lost Part 7_files/89855_900.jpg#clickable)](How Tartaria was Lost Part 7_files/89855_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/89855/89855_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/89855/89855_original.jpg)

**Russian:**

15 Фрагмент 01-1

**English:**

15 Fragment 01-1

<!-- Local
[![](How Tartaria was Lost Part 7_files/90025_900.jpg#clickable)](How Tartaria was Lost Part 7_files/90025_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/90025/90025_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/90025/90025_original.jpg)

**Russian:**

Ширина данных полос около 6 км, ширина одной полосы около 660 метров. При этом в некоторых местах, где деформировано дно, вместе с ним деформированы и данные «борозды», их структура нарушена. От пролива Гибралтар на восток линии тянутся на расстояние около 1200 км, а на запад, по дну Атлантического океана, до острова Сан-Мигел на расстояние 1800 км. При этом в районе данного острова имеется целый узел из подобных образований, поскольку от него подобные структуры расходятся в несколько сторон. В промежутке между городами Аннаба и Эль-Кала возле северного побережья Африки следы объекта теряются, упираясь в возвышенность дна. Но если продолжить линию на восток, то через 660 км, когда возвышенность кончается, мы снова видим похожее образование, которое разделяется на два направления. Причём верхний объект делает два поворота под достаточно острыми углами.

**English:**

The total width of these bands is about 6 km, the width of any individual band is about 660 meters. At the same time, in some places where the bottom is deformed, these "furrows" are deformed together with it, their structure is broken. From the Strait of Gibraltar to the east, the lines extend for about 1,200 km; and to the west, along the Atlantic Ocean floor, to St. Miguel's Island they extend for 1,800 km. There is a whole node of such formations in the area of the island, as such structures diverge in several directions from it. Between the towns of Annaba and El-Kala off the northern coast of Africa, traces of the site are lost, relying on the elevated seabed. But if we continue the line to the east, then after 660 km, when the elevation ends, we see again a similar formation, which is divided into two directions. And the upper object makes two turns at fairly sharp angles.


**Russian:**

Вот как эти следы выглядят у побережья Алжира (фрагмент 2).

**English:**

This is what these tracks look like off the coast of Algeria (fragment 2).

**Russian:**

15 Фрагмент 02

**English:**

15 Fragment 02

<!-- Local
[![](How Tartaria was Lost Part 7_files/90250_900.jpg#clickable)](How Tartaria was Lost Part 7_files/90250_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/90250/90250_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/90250/90250_original.jpg)

**Russian:**

А так они выглядят в Атлантическом океане, сверху справа берег Португалии.

**English:**

And this is what they look like in the Atlantic Ocean, on the top right side of the Portuguese coast.

**Russian:**

15 Фрагмент 03

**English:**

15 Fragment 03

<!-- Local
[![](How Tartaria was Lost Part 7_files/90541_900.jpg#clickable)](How Tartaria was Lost Part 7_files/90541_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/90541/90541_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/90541/90541_original.jpg)

**Russian:**

Причём в Атлантическом океане структура объекта меняется. Там уже скорее одна полоса на каком-то основании. Но ширина основания составляет всё те же 6 км, а ширина самой полосы около 660 метров.

**English:**

And in the Atlantic Ocean, the structure of the object changes. It's more of a single strip on some base. But the width of the overall base is still 6 km, and the width of the strip itself is about 660 meters.

**Russian:**

Надеюсь, что никто не будет спорить, что наблюдаемые объекты являются неким артефактом, то есть, имеют не естественное природное происхождение, а являются следом деятельности некой разумной силы.

**English:**

I hope that no one will argue that the observed objects are some kind of artifact, that is, they are not of natural origin, but are a trace of some reasonable force.

**Russian:**

По моему совершенно очевидно, что эти следы оставлены не нашей цивилизацией. Мы не обладаем технологиями, которые позволяют строить подобные гигантские сооружения на дне морей и океанов. Также очевидно, что это не могли построить ни римляне, ни египтяне с их бронзовым инструментом.

**English:**

It is clear to me that these traces were not left by our civilization. We do not have the technology to build such giant structures at the bottom of seas and oceans. It's also obvious that neither the Romans nor the Egyptians could have built these with their bronze tools.

**Russian:**

Мало того, судя по деформации объектов, они были сооружены до того, как начали происходить геологические процессы, которые привели к поднятию материковых плит и образованию возвышений, вызвавших деформацию объектов. Я в этих вопросах не специалист, поэтому затрудняюсь сказать какой это может быть геологический период, но факт, что это было очень давно. Можно даже предположить, что их строили не на дне морей и океанов, то есть, что в момент их строительства эта территория не была дном Средиземного моря и Атлантического океана.

**English:**

Moreover, judging by the deformation of the objects, they were built before the geological processes that led to the rise of continental plates and the formation of elevations that caused the deformation of objects. I am not an expert in these issues, so it is difficult to say what geological period it may be, but the fact is that it was a very long time ago. We can even assume that they were not built on the seabed and ocean floor, that is, at the time of their construction this territory was not the bottom of the Mediterranean Sea and the Atlantic Ocean.

**Russian:**

И что может нам рассказать официальный исторический миф про эти объекты? Откуда они взялись на дне Средиземного моря и Атлантического океана? Первобытные люди это построили каменными топорами, кутаясь в набедренные повязки? Или может это следы строительной деятельности неандертальцев?

**English:**

And what can the official historical myth about these objects tell us? Where did they come from at the bottom of the Mediterranean Sea and the Atlantic Ocean? Did the primitive people build these objects with stone axes, while dressed in loincloths? Or maybe they are traces of Neanderthal construction activity?

**Russian:**

Честно скажу, что я не знаю, что это за объекты и каково их назначение, но я отчётливо вижу, что они имеют место быть и не являются природными образованиями. Разве что Гугл решил над нами подшутить и посадил своих специалистов рисовать все эти линии на дне морей и океанов, чтобы мы с вами тут ломали головы. Но пока, насколько мне известно, «всемогущий» Гугл уличали только в том, что они не добавляли, а удаляли с карт те объекты, которые плохо вписываются в официальный исторический миф. Так что вполне вероятно, что скоро могут затереть и эти следы. Так что, спешите посмотреть своими глазами, пока есть такая возможность.

**English:**

To be honest, I do not know what these objects are nor what their purpose is, but I clearly see that they exist and are not natural formations. Unless Google decided to pull a prank on us and put its specialists in charge of drawing all these lines at the bottom of the seas and oceans so that you and I could break our heads here... But so far, as far as I know, the "almighty" Google has only been accused of not adding, but removing from maps those objects that do not fit into the official historical myth. So it's very likely that these traces will soon be erased. Hurry up and see them for yourself while you can.

**Russian:**

Ссылка для выхода на фрагмент 1: [https://www.google.ru/maps/@36.593294,-1.5194892,140578m/data=!3m1!1e3?hl=ru](https://www.google.ru/maps/@36.593294,-1.5194892,140578m/data=!3m1!1e3?hl=ru)

**English:**

The reference for the exit to fragment 1: [https://www.google.ru/maps/@36.593294,-1.5194892,140578m/data=!3m1!1e3?hl=ru](https://www.google.ru/maps/@36.593294,-1.5194892,140578m/data=!3m1!1e3?hl=ru)

**Russian:**

Но вернёмся к катастрофе в Западной Сибири, которая произошла 200 лет назад. Отсутствие прямых свидетельств объясняется тем, что вряд ли мог выжить кто-то из тех, кто видел эту катастрофу своими глазами. А косвенных свидетельств у нас имеется множество, часть из них я уже приводил в предыдущих частях статьи.

**English:**

But back to the catastrophe in Western Siberia that happened 200 years ago. The lack of direct evidence is explained by the fact that hardly anyone who saw this catastrophe with their own eyes could have survived. And we have a lot of indirect evidence, some of which I have already mentioned in the previous parts of this article.

**Russian:**

Но я также считаю, что у нас есть прямое свидетельство о данной катастрофе непосредственного очевидца событий, который мог видеть его своими глазами. Причём этот документ известен ну очень многим людям, поскольку он входит в состав книги под названием Библия и называется «Откровения Ионна Богослова» или «Апокалипсис». Правда, нас сегодня уверяют, что это описание неких событий, которые должны произойти во время второго пришествия Иисуса Христа, но судя по тем фактам, которые я приведу ниже, как минимум часть этих событий уже произошли, причём именно в Западной Сибири и Средней Азии.

В самом начале своего повествования Иоанн сообщает следующее:

**English:**

But I also believe that we have direct evidence of this catastrophe from a direct eyewitness who did see it with his own eyes. And this document is known to many people, because it is part of a book called the Bible and is called "Revelations of John the Theologian" or "The Apocalypse". It is true that today we are assured that this is a description of some events that should take place at the second coming of Jesus Christ, but judging by the facts that I will give below, at least some of these events have already happened, and they happened in Western Siberia and Central Asia.

At the very beginning of his narrative, John reports this:

**Russian:**

> 9. Я, Иоанн, брат ваш и соучастник в скорби и в царствии и в терпении Иисуса Христа, был на острове, называемом Патмос, за слово Божие и за свидетельство Иисуса Христа. 

*(Откровение Иоанна Богослова 1:9)*

**English:**

> 9. I, John, your brother and partner in tribulation and in the kingdom and patience of Jesus Christ, was on an island called Patmos for the word of God and for the testimony of Jesus Christ. 

*(Revelation 1:9)*

**Russian:**

Согласно официальной легенде, в данном случае имеется в виду остров Патмос, который находится в Средиземном море. Именно туда, якобы, был сослан Иоанн Богослов и именно там, в одной из пещер имел откровение, составившее содержание Апокалипсиса.

**English:**

According to official legend, in this case we mean the island of Patmos, which is located in the Mediterranean Sea. It was there that John the Theologian was exiled, and it was there, in one of the caves, that he had the revelation which constituted the content of the Apocalypse.

**Russian:**

Но оказывается, что существует и другой [остров Патмос, который находится на реке Катунь в Алтайском крае!](https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D1%82%D0%BC%D0%BE%D1%81)

**English:**

But it turns out that there is another [Patmos Island, which is on the Katun River in the Altai Territory!](https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D1%82%D0%BC%D0%BE%D1%81).

**Russian:**

Если мы посмотрим на схему ниже, то увидим, что находясь на этом острове вы, во-первых, будете находиться вне основной зоны поражения, а во-вторых, вам будет прекрасно видно небосвод над той территорией, где происходит катастрофа и падение крупных метеоритов.

**English:**

If we look at the diagram below, we will see that being on this island, firstly, you will be outside the main zone of destruction, and secondly, you will be able to see the sky above the territory where the disaster and the fall of large meteorites.

**Russian:**

16 Остров Патмос

**English:**

16 Patmos Island

<!-- Local
![](How Tartaria was Lost Part 7_files/90780_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/90780/90780_original.jpg#resized)


**Russian:**

При этом сам Иоанн чуть раньше указывает, что он находится именно в Азии, а не в Средиземном море, поскольку Асия есть старое название Азии:

**English:**

At the same time, John himself indicated a little earlier that he was in Asia and not in the Mediterranean Sea, since Asia is the old name of Asia:

**Russian:**

> 4. Иоанн семи церквам, находящимся в Асии: благодать вам и мир от Того, Который есть и был и грядет, и от семи духов, находящихся перед престолом Его... 

*(Откровение Иоанна Богослова 1:4)*

**English:**

> 4. John to the Seven Churches in Asia: Grace to you, and peace from Him who is and was, and from the seven Spirits before His Throne... 

*(Revelation 1:4)*

**Russian:**

Дальше Иоанн приводит описание событий, происходивших во время катклизма.

**English:**

John goes on to describe the events that took place during the cataclysm.

**Russian:**

> 7. Первый Ангел вострубил, и сделались град и огонь, смешанные с кровью, и пали на землю; и третья часть дерев сгорела, и вся трава зеленая сгорела. 

*(Откровение Иоанна Богослова 8:7)*

**English:**

> 7. The first Angel sounded, and hail and fire came, mixed with blood, and fell to the ground; and a third of the trees burned, and all the grass was green. 

*(Revelation 8:7)*

**Russian:**

То есть, нам опять сообщают о метеоритной бомбардировке территории, которая привела к уничтожению лесов и травы.

**English:**

That is, we are once again informed of the meteorite bombing of the territory, which led to the destruction of forests and grass.

**Russian:**

> 8. Второй Ангел вострубил, и как бы большая гора, пылающая огнем, низверглась в море; и третья часть моря сделалась кровью. 
> 9. и умерла третья часть одушевленных тварей, живущих в море, и третья часть судов погибла. 

*(Откровение Иоанна Богослова 8:8,9)*

**English:**

> 8. The second angel sounded, and it was as if a large mountain burning with fire had plunged into the sea; and a third part of the sea became blood. 
> 9. And a third part of the living creatures that dwelt in the sea died, and a third of the ships died. 

*(Revelation 8:8,9)*

**Russian:**

А вот этот фрагмент интересен тем, что сообщает нам о падении очень крупного метеорита в море. Правда, тут не совсем понятно, о каком море идёт речь. Возможно, что в данном случае имеется в виду большое внутреннее Каспийское море, которое можно найти на некоторых старых картах, от которого в итоге остались намного меньшее Каспийское море и Аральское море. На старых картах 16-18 веков это выглядит так.

Конфигурация моря совсем другая, хотя это можно попытаться списать на умение картографа. Но на карте отчётливо прорисованы крупные реки, которые впадают в Каспий на восточном и южном берегу. Сейчас этих рек не наблюдается.

**English:**

But this fragment is interesting because it tells us that a very large meteorite fell into the sea. True, it's not quite clear which sea we're talking about. It is possible that in this case we mean a large inland Caspian Sea, which can be found on some old maps, from which there is now a much smaller Caspian Sea and Aral Sea. On old maps from the 16-18 centuries it looks like this.

The configuration of the sea is very different, although you can try to write it off as cartographer's skill. But the map clearly shows the large rivers that flow into the Caspian Sea on the eastern and southern banks. Now these rivers are not observed.

**Russian:**

17 Средняя азия старая карта 01

**English:**

17 Central Asia old map 01

<!-- Local
[![](How Tartaria was Lost Part 7_files/90933_900.jpg#clickable)](How Tartaria was Lost Part 7_files/90933_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/90933/90933_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/90933/90933_original.jpg)

**Russian:**

На данной карте конфигурация всех остальных морей и заливов более менее совпадает с правильными, кроме Каспийского моря. И опять мы видим по правому и южному берегу реки. Причём река на границе изображена как достаточно крупная.

**English:**

On this map, the configuration of all other seas and bays is less consistent with the correct ones except for the Caspian Sea, which, again, we can see is accurate from the right and southern banks of the river. And the river at the border is depicted as quite large.

**Russian:**

17 Средняя азия старая карта 02

**English:**

17 Central Asia old map 02

<!-- Local
[![](How Tartaria was Lost Part 7_files/91324_900.jpg#clickable)](How Tartaria was Lost Part 7_files/91324_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/91324/91324_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/91324/91324_original.jpg)


**Russian:**

Следующая карта вообще уникальна, поскольку это карта именно Каспийского моря, причём карта мореходная, поскольку на ней обозначены промеры глубины, а также даны вставки по заливам и устьям крупных рек. При этом реки Krudosel, которая изображена на 6 и 7 рисунках, я найти не смог.

**English:**

The following map is unique in general because it is a map of the Caspian Sea, and the map is navigable because it shows depth measurements, as well as inserts for the bays and mouths of large rivers. At the same time, I could not find the river Krudosel, which is depicted in figures 6 and 7.

**Russian:**

17 Средняя азия старая карта 03

**English:**

17 Central Asia old map 03

<!-- Local
[![](How Tartaria was Lost Part 7_files/91448_900.jpg#clickable)](How Tartaria was Lost Part 7_files/91448_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/91448/91448_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/91448/91448_original.jpg)

**Russian:**

Правда, эту карту нужно повернуть на 90 градусов, чтобы она соответствовала современному привычному нам способу отображения.

**English:**

However, this map must be rotated 90 degrees to match the modern way we are used to seeing it displayed.

**Russian:**

17 Средняя азия старая карта 03а

**English:**

17 Central Asia old map 03a

<!-- Local
[![](How Tartaria was Lost Part 7_files/91669_900.jpg#clickable)](How Tartaria was Lost Part 7_files/91669_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/91669/91669_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/91669/91669_original.jpg)


**Russian:**

На этой карте подписаны большинство рек и обозначено множество городов. Но при этом города и реки, которые расположены на западном и северном берегах, находятся легко, а вот те, что расположены на южном и восточном, нет. Каким образом получилось, что в одной части топонимы сохранились до наших дней, а в другой полностью изменились? Что по этому поводу нам может сообщить официальный исторический миф? И куда исчезли острова в верхней части Каспия? Они не только тщательно прорисованы, но даже обозначена их принадлежность разным государствам. Для мореходной карты подобные вольности в изображении несуществующих островов недопустимы.

**English:**

On this map, most rivers are labelled and many cities are marked. However, cities and rivers that are located on the western and northern banks are easy to find, but those located on the southern and eastern banks are not. How is it that in one part the toponyms have survived to this day, and in the other part they have completely changed? What can the official historical myth tell us about it? And where did the islands in the upper part of the Caspian Sea disappear to? They are not only painstakingly drawn, but even their belonging to different states is marked. For a nautical map such liberties in the image of non-existent islands would be unacceptable.

**Russian:**

Кстати, на этой карте обозначен город Schamachia, который по русски будет звучать как Шамахия, что подтверждает её подлинность.

**English:**

By the way, this map shows the city of Schamachia, which in Russian would sound like Shamakhiya, which confirms its authenticity.

**Russian:**

17 Средняя азия старая карта 03б

**English:**

17 Central Asia old map 03b

<!-- Local
[![](How Tartaria was Lost Part 7_files/92067_900.jpg#clickable)](How Tartaria was Lost Part 7_files/92067_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/92067/92067_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/92067/92067_original.jpg)

**Russian:**

Помните, сказку «О золотом петушке» Александра Сергеевича Пушкина?

**English:**

Do you remember the fairy tale "About the Golden Rooster" by Alexander Sergeyevich Pushkin?

**Russian:**

«Подари ж ты мне девицу, Шамаханскую царицу»

**English:**

"Give me a girl, the Shamakhan queen."

**Russian:**

Получается, что город Шамахия существовал в действительности, причём сравнительно недавно, раз о нём писал Пушкин, который узнал о нём из рассказов своей няни Арины Родионовны. Вот только сейчас вы его на карте не найдёте. Как не найдёте и других городов, расположенных дальше по побережью: Kesker, Farabath/Ferebath, Esterabad/Aftrabath и т. д.

**English:**

It turns out that the city of Shamakhiya existed in reality, and relatively recently, Pushkin wrote about it, who learned about it from the stories of his nanny Arina Rodionovna. Only now you will not find it on the map. Nor will you find other cities located further along the coast: Kesker, Farabath/Ferebath, Esterabad/Aftrabath, etc.

**Russian:**

Ещё одна карта, на которой хорошо видны реки, впадающие с восточной стороны.

**English:**

Another map that clearly shows the rivers flowing in from the east side.

**Russian:**

17 Средняя азия старая карта 04

**English:**

17 Central Asia old map 04

<!-- Local
[![](How Tartaria was Lost Part 7_files/92287_900.jpg#clickable)](How Tartaria was Lost Part 7_files/92287_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/92287/92287_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/92287/92287_original.jpg)


**Russian:**

А это снова карта Каспийского моря, которая лежит на боку.

**English:**

And this is again a map of the Caspian Sea, showing it rotated on its side.

**Russian:**

17 Средняя азия старая карта 05

**English:**

17 Central Asia old map 05

<!-- Local
[![](How Tartaria was Lost Part 7_files/92605_900.jpg#clickable)](How Tartaria was Lost Part 7_files/92605_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/92605/92605_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/92605/92605_original.jpg)

**Russian:**

Её нужно развернуть, чтобы было удобнее сравнивать с современным видом.

**English:**

It needs to be turned around to be more comfortably compared with the modern look.

**Russian:**

17 Средняя азия старая карта 05а

**English:**

17 Central Asia old map 05a

<!-- Local
[![](How Tartaria was Lost Part 7_files/92700_900.jpg#clickable)](How Tartaria was Lost Part 7_files/92700_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/92700/92700_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/92700/92700_original.jpg)

**Russian:**

Мы в который уже раз видим множество рек по южному и восточному берегу Каспия, которые сегодня отсутствуют. Но на этой карте также показано, что город Самарканд (обозначен зелёным кружком, подпись подчёркнута зелёной линией) стоит на реке, по которой мы можем попасть в Каспийское море! Этот путь я пометил синим пунктиром. Река, на которой стоит Самарканд, впадает в крупную реку, которая на одной из карт обозначена как Kajak, и которую мы можем наблюдать практически на всех рассмотренных ранее картах, то есть, это не ошибка составителей, такая река в действительности была в те времена.

**English:**

Once again we see many rivers on the southern and eastern shores of the Caspian Sea, which are absent today. But this map also shows that the city of Samarkand (marked with a green circle, the label is underlined in green) stands on a river, through which we can get to the Caspian Sea! I marked this route with a blue dotted line. The river on which Samarkand stands flows into a large river, which on one of the maps is marked as Kajak, and which we can observe on almost all the maps considered earlier. That is, it is not a mistake of the compilers, such a river did in fact exist at that time.

**Russian:**

А теперь посмотрим как это место выглядит сейчас.

**English:**

Now let's see what this place looks like now.

**Russian:**

17 Средняя азия сейчас

**English:**

17 Central Asia now

<!-- Local
[![](How Tartaria was Lost Part 7_files/93123_900.jpg#clickable)](How Tartaria was Lost Part 7_files/93123_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/93123/93123_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/93123/93123_original.jpg)

**Russian:**

Где река, по которой из Самарканда можно попасть в Каспийское море? Где вообще все эти реки и города по южному и восточному берегу Каспийского моря? Что может нам по этому поводу сообщить официальный исторический миф?

**English:**

Where is the river that leads from Samarkand to the Caspian Sea? Where are all these rivers and towns on the southern and eastern shores of the Caspian Sea? What can the official historical myth tell us about it?

**Russian:**

А вот Иоанн Богослов нам об этом сообщает, что всё это было уничтожено во время падения гигантского метеорита, когда «как бы большая гора, пылающая огнем, низверглась в море». Почему я думаю, что речь идёт именно об этом месте? Потому что падение крупного объекта в это место теоретически можно было видеть с острова Патмос в Алтайском крае, а вот падение во все другие моря — нет.

**English:**

But John the Theologian tells us that all this was destroyed during the fall of a giant meteorite, when "as if a large mountain burning with fire fell into the sea". Why do I think this is the place we're talking about? Because the fall of a large object in this place could theoretically be seen from the island of Patmos in the Altai Territory, but not an fall in all other seas - no.

**Russian:**

Что же ещё может нам сообщить Иоанн Богослов о той катастрофе?

Читаем дальше:

**English:**

What else can John the Theologian tell us about that disaster?

Let's keep reading:

**Russian:**

> 10. Третий ангел вострубил, и упала с неба большая звезда, горящая подобно светильнику, и пала на третью часть рек и на источники вод. 
> 11. Имя сей звезде «полынь»; и третья часть вод сделалась полынью, и многие из людей умерли от вод, потому что они стали горьки. 

*(Откровение Иоанна Богослова 8:10,11)*

**English:**

> 10. The third angel sounded, and a big star fell from heaven, burning like a lamp, and fell on the third part of the rivers and on the water springs. 
> 11 The name of this star is wormwood; and the third part of the waters became wormwood, and many people died of the waters because they became bitter. 

*(Revelation 8:10,11)*

**Russian:**

А вот это уже идёт описание падения тех объектов, которые оставили следы в Западной Сибири. Почему воды стали горьки? А потому, что упавшие ледяные метеориты были не пресными, а солёными. У нас и в Челябинской, и в Курганской областях встречается множество солёных озёр, причём не просто солёных, а очень солёных. Причём эти солёные озёра расположены случайным образом среди других таких же, но не солёных. Если бы эти солёные озёра остались после волны гигантского цунами, то их солёность должна была быть примерно одинаковой и по составу соответствовать морской воде или быть слабее. Но по факту это не так.

**English:**

And this is already a description of the fall of those objects that left traces in Western Siberia. Why did the waters become bitter? Because the fallen ice meteorites were salty, not fresh. In both Chelyabinsk and Kurgan regions we have a lot of salty lakes, and not just salty, but very salty. And these salty lakes are randomly located among other similar lakes, which are not salty. If these salt lakes had been left behind by a giant tsunami, their salinity would have been about the same and the same composition as or weaker than sea water. But in fact, they're not the same.

**Russian:**

> 12. Четвертый Ангел вострубил, и поражена была третья часть солнца и третья часть луны и третья часть звезд, так что затмилась третья часть их, и третья часть дня не светла была — так, как и ночи. 

*(Откровение Иоанна Богослова 8:12)*

**English:**

> 12. The fourth angel sounded, and the third part of the sun and the third part of the moon and the third part of the stars were struck, so that the third part of them was eclipsed, and the third part of the day was not shining - as well as the night. 

*(Revelation 8:12)*

**Russian:**

О том, что при падении большого количества метеоритов часть их разрушается в воздухе, а остальные теряют часть вещества при торможении в плотных слоях атмосферы, что приводит к загрязнению атмосферы, я уже писал выше. И Иоанн Богослов своими наблюдениями это подтверждает.

Дальше будет большой фрагмент, но весьма любопытный.

> 1. Пятый Ангел вострубил, и я увидел звезду, падшую с неба на землю, и дан был ей ключ от кладязя бездны. 
> 2. Она отворила кладязь бездны, и вышел дым из кладязя, как дым из большой печи; и помрачилось солнце и воздух от дыма из кладязя. 
> 3. И из дыма вышла саранча на землю, и дана была ей власть, какую имеют земные скорпионы. 
> 4. И сказано было ей, чтобы не делала вреда траве земной, и никакой зелени, и никакому дереву, а только одним людям, которые не имеют печати Божией на челах своих. 
> 5. И дано ей не убивать их, а только мучить пять месяцев; и мучение от нее подобно мучению от скорпиона, когда ужалит человека. 
> 6. В те дни люди будут искать смерти, но не найдут ее; пожелают умереть, но смерть убежит от них. 
> 7. По виду своему саранча была подобна коням, приготовленным на войну; и на головах у ней как бы венцы, похожие на золотые, лица же ее — как лица человеческие; 
> 8. и волосы у ней — как волосы у женщин, а зубы у ней были, как у львов. 
> 9. На ней были брони, как бы брони железные, а шум от крыльев ее — как стук от колесниц, когда множество коней бежит на войну; 
> 10. у ней были хвосты, как у скорпионов, и в хвостах ее были жала; власть же ее была — вредить людям пять месяцев. 
> 11. Царем над собою она имела ангела бездны; имя ему по-еврейски Аваддон, а по-гречески Аполлион. 

*(Откровение Иоанна Богослова 9:1-11)*

**English:**

I have already written above about the fact that when a large number of meteorites fall, some of them are destroyed in the air, while others lose some of the substance while decelerating in dense layers of the atmosphere, which leads to pollution of the atmosphere. And John the theologian confirms it with his observations.

What follows is a large passage but it is very curious.

> 1. The fifth angel sounded, and I saw a star falling from heaven to earth, and the key to the treasure of the abyss was given to it. 
> 2. And she opened the treasure of the abyss, and the smoke came out of the treasure like the smoke of a large furnace; and the sun and the air of the smoke of the treasure darkened. 
> 3. And locusts came out of the smoke unto the earth, and power was given to it, as the scorpions of the earth have. 
> 4. And it was told to her not to do harm to the grass of the earth, and no greens, and no tree, but only to people who have no God's seal on their foreheads. 
> 5. And it was given her not to kill them, but to torment them for five months; and her torment was like that of a scorpion, when she has stung a man. 
> 6. In those days men shall seek death, but shall not find it; they shall wish to die, but death shall flee from them. 
> 7. The locust looked like horses prepared for war; and her heads were like crowns, like gold, and her faces like human faces. 
> 8. And her hair was like the hair of women, and her teeth were like the hair of lions. 
> 9. She had armour like iron, and the noise of her wings like a knock of chariots, when many horses ran to war; 
> 10. She had tails like scorpions, and her tails were stinging; but her power was to harm men for five months. 
> 11. She had an angel of the abyss above her king; his name was Avaddon in Hebrew, and Apollo in Greek. 

*(Revelation of John the Theologian 9:1-11)*

**Russian:**

В этом месте Иоанн Богослов описывает нам посадку космического корабля и высадку десанта на летательных аппаратах, которые по описанию похожи на боевые вертолёты. Лица как человеческие — это пилоты в кабинах, венцы как бы золотые — это шлемы, про броню и так всё понятно, шум от крыльев и двигателя вертолёта вполне можно принять за стук от колесниц, когда множество коней бежит на войну.

**English:**

In this place, John the Theologian describes to us the landing of a spaceship and the landing of aircraft, which are described as similar to combat helicopters. Faces as human are pilots in cockpits, crowns as if gold are helmets, about armor and so everything is clear, the noise from the wings and engine of the helicopter can be taken for a rattle of chariots, when many horses are running to war.

**Russian:**

Другими словами, на поверхности Земли были высажены подразделения карателей, которые летали на аппаратах, похожих на вертолёты, и добивали выживших.

**English:**

In other words, units of chasteners were landed on the surface of the Earth, flying in helicopters-like vehicles and killing the survivors.

**Russian:**

> 13. Шестой Ангел вострубил, и я услышал один голос от четырех рогов золотого жертвенника, стоящего пред Богом. 
> 14. говоривший шестому Ангелу, имевшему трубу: освободи четырех Ангелов, связанных при великой реке Евфрате. 
> 15. И освобождены были четыре Ангела, приготовленные на час и день, и месяц и год, для того, чтобы умертвить третью часть людей. 
> 16. Число конного войска было две тьмы тем; и я слышал число его. 
> 17. Так видел я в видении коней и на них всадников, которые имели на себе брони огненные, гиацинтовые и серные; головы у коней — как головы у львов, и изо рта их выходил огонь, дым и сера. 
> 18. От этих трех язв, от огня, дыма и серы, выходящих изо рта их, умерла третья часть людей; 
> 19. ибо сила коней заключалась во рту их и в хвостах их; а хвосты их были подобны змеям, и имели головы, и ими они вредили. 

*(Откровение Иоанна Богослова 9:13-19)*

**English:**

> 13. The sixth angel sounded, and I heard one voice from the four horns of the golden altar, standing before God. 
> 14. Saying to the sixth angel who had the trumpet, release the four angels bound at the great river Euphrates. 
> 15. And four Angels were set free, prepared for an hour and a day, and a month and a year, in order to kill a third of the people. 
> 16. The number of the mounted army was two of those darkness; and I heard his number. 
> 17. So I saw in the vision of the horses, and the riders on them, who had armour of fire, hyacinth and sulphur; the heads of the horses were like the heads of lions, and fire, smoke and sulphur came out of their mouths. 
> 18. From these three plagues, from the fire, smoke, and sulfur that came out of their mouths, a third of the people died;
> 19. For the power of the horses was in their mouths and in their tails; and their tails were like serpents, and had heads, and they harmed them. 

*(Revelation 9:13-19)*

**Russian:**

Больше всего это описание похоже на всадников, которые едут на огнедышащих драконах из легенд средневековой Европы. Но если остальные описания совпадают с имеющимися у нас фактами, то может и эта часть тоже не является выдумкой, как и европейские легенды? Может быть действительно карательные отряды пришельцев использовали каких-то неизвестных нам своих животных, которые похожи на драконов, для истребления людей?

**English:**

This description is most similar to the horsemen who ride fire-breathing dragons from medieval European legends. But if these other descriptions coincide with the facts we have, then maybe these European legends were not fiction either? Maybe it's true that the punitive alien squads used some unknown dragon-like animals of their own to exterminate people?

**Russian:**

С другой стороны, присутствие в описании огня дыма и серы может указывать на техногенную природу этих «коней с головами льва». При этом люди, которые не понимали, что могут существовать подобные машины, принимали их за живых существ, то есть за Драконов.

**English:**

On the other hand, the presence of smoke and sulfur in the description of fire may indicate the technogenic nature of these "horses with lion heads". At the same time, people who did not understand that such machines could exist, took them for living beings, that is, for dragons.

**Russian:**

Остальной текст «Откровений Иоанна Богослова» я не будут цитировать и разбирать в данной статье, желающие могут его найти в интернете и почитать самостоятельно. Но из его откровений также следует, что «Господь» христиан сволочь ещё та и устраивать массовый геноцид населения планеты, которая отказалась подчинятся «воле Господа», для этого «Господа» дело привычное.

**English:**

I will not parse the rest of the text of "Revelations of John the Theologian" in this article. Anyone wishing to find it on the Internet can read for themselves. But it also follows from his revelations that the "Lord" of Christians is a real bastard and it was customary for this "Lord" to organize mass genocide of the population of the planet if they refused to obey the "will of the Lord".

**Russian:**

Но на самом деле не всё так уж плохо, поскольку некоторые факты указывают на то, что захватчики не настолько круты и могущественны, какими пытаются себя представить.

**English:**

But it's not really that bad, because some facts indicate that the invaders are not as cool and powerful as they try to imagine.

<!--
**Russian:**

### Как погибла Тартария? Часть 7в

**English:**

### How did Tartaria die? Part 7c.

(https://mylnikovdm.livejournal.com/6658.html)


1 December 2014
-->

**Russian:**

После опубликования первых фрагментов заключительной части пришло много комментариев и вопросов на которые необходимо ответить, прежде чем мы продолжим.

**English:**

After the publication of the first snippets of the final part, there were a lot of comments and questions to answer before we continue.

**Russian:**

Во-первых, многие указали, что следы на Гугл-мап могут иметь совсем другое объяснение. Изображение рельефа дна морей и океанов было получено не фотографированием со спутников, а построено каким-то аналитическим методом по различным наборам данных. По этом причине разрешение картинки в разных местах разное. То, что я принял за следы шириной 6 км является просто более чёткой картинкой, которая получилась в результате сканирования эхолокатором с борта океанографического судна, при этом сам след является курсом, по которому это судно шло по время проведения съёмки.

**English:**

First, many pointed out that Google Map footprints may have a very different explanation. The topography of the seabed and ocean floor was not derived from satellite imagery, but was built using some kind of analytical method on different data sets. For this reason, the resolution of the image is different in different places. What I took for traces 6 km wide is just a clearer picture, which was obtained as a result of sonar scanning from an oceanographic vessel, and the trail itself is the course along which this vessel was going during the survey.


**Russian:**

Вполне возможно, тем более, что я и сам обратил внимание, что в том же Тихом океане имеются подобные полосы, но никаких линий там при этом не видно. С дугой стороны, в Средиземном море достаточно чётко читается система параллельных линий, так что возможен и другой вариант, что делали более детальную съёмку именно этих объектов на дне, а само судно шло вдоль них, прокладывая курс по данным сканирования дна.

**English:**

It is quite possible, especially as I myself noticed that in the Pacific Ocean there are similar strips, but no lines can be seen there. On the other hand, in the Mediterranean Sea there is a clear enough system of parallel lines, so it is also possible that a more detailed survey of these very objects on the bottom was made and the ship itself followed them, laying the course according to the data of the bottom scanning.

**Russian:**

Как бы там ни было, но получить точный ответ, есть ли там что-то на дне, сидя дома за компьютером невозможно. А организация подобных экспедиций выходит а рамки моих скромных возможностей. Так что этот фрагмент можно пока вынести «за скобки» со знаком вопроса, тем более, что это принципиально не влияет на все остальные наблюдения и выводы.

**English:**

Anyway, it is impossible to get an exact answer if there is something at the bottom by sitting at home on the computer. And the organization of such expeditions goes beyond my modest possibilities. So this fragment can be put out "in brackets" with a question mark, especially since it does not affect all the other observations and conclusions.

**Russian:**

Во-вторых, был задан очень справедливый вопрос по поводу того, что согласно официальной версии «Откровения Иоанна Богослова» были написаны одним из учеников Иисуса Христа, Апостолом Иоанном. То есть, либо он никак не мог быть свидетелем катаклизма в Сибири 200 лет назад, либо это означает ну очень сильное изменение сложившейся хронологии, которая выходит даже за те построения, которые делают Носовский и Фоменко в своей «Новой Хронологии».

**English:**

Secondly, a very fair question was asked about the fact that according to the official version, "Revelation of John the Theologian" was written by one of the disciples of Jesus Christ, the Apostle John. That is, he could not be the witness of cataclysm in Siberia 200 years ago, because that would mean, well, a very strong change for the developed chronology which would be a departure even for those constructions which Nosovsky and Fomenko create in the "New Chronology".

**Russian:**

Но теже Носовский и Фоменко в своих исследованиях упомниают Апокалипсис [http://www.chronologia.org/xpon5/dop2.html](http://www.chronologia.org/xpon5/dop2.html), при этом дают совершенно другие датировки его написания. По их мнению, «Апокалипсис» был написан не ранее 1486 года. Кроме того, в самом «Апокалипсисе» есть указание на то, что его текст корректировался после написания. Вот, что они пишут по этому вопросу в своём исследовании:

**English:**

But both Nosovsky and Fomenko recall the Apocalypse in their studies [http://www.chronologia.org/xpon5/dop2.html](http://www.chronologia.org/xpon5/dop2.html), while giving completely different dates for its writing. In their opinion, the "Apocalypse" was not written before 1486. Besides, there is an indication in the Apocalypse itself that its text was corrected after it was written. This is what they write on the matter in their research:

**Russian:**

Мы имеем в виду известный сюжет "о съедении книги". От лица Иоанна Богослова, считаемого автором, в Апокалипсис включен следующий рассказ. 

> "И видел я другого Ангела сильного... В РУКЕ У НЕГО БЫЛА КНИЖКА РАСКРЫТАЯ... И воскликнул громким голосом, как рыкает лев; и когда он воскликнул, тогда семь громов проговорили голосами своими... Я ХОТЕЛ БЫЛО ПИСАТЬ; но услышал голос с неба, говорящий мне: СКРОЙ, ЧТО ГОВОРИЛИ СЕМЬ ГРОМОВ, И НЕ ПИШИ СЕГО... И голос, который я слышал с неба... сказал: ПОЙДИ, ВОЗЬМИ РАСКРЫТУЮ КНИЖКУ ИЗ РУКИ АНГЕЛА... И я пошел к Ангелу и сказал ему: ДАЙ МНЕ КНИЖКУ. Он сказал мне: ВОЗЬМИ И СЪЕШЬ ЕЕ; она будет горька во чреве твоем, но в устах твоих будет сладка, как мед. И взял я книжку из рук Ангела, И СЪЕЛ ЕЕ... И сказал он мне: ТЕБЕ НАДЛЕЖИТ ОПЯТЬ ПРОРОЧЕСТВОВАТЬ О НАРОДАХ И ПЛЕМЕНАХ, И ЯЗЫКАХ, И ЦАРЯХ МНОГИХ" (Ап. 10:1-4, 10:8-11). И далее Ангел сказал Иоанну: "Не запечатывай слов пророчества книги сей... И также свидетельствую всякому слышащему слова пророчества КНИГИ СЕЙ" 

*(Ап. 22:10, 22:19).*

**English:**

> We're talking about the famous "book-eating" story. On behalf of John the Theologian, considered to be the author, the Apocalypse includes the following story.

> "And I saw another strong angel... IN HIS HAND WAS AN OPEN BOOK... And he cried out with a loud voice, as a lion roars; and when he cried out, then seven thunders spoke with their voices... And I was about to write, but I heard a voice from heaven saying to me, SCREAM what the seven thunders said, and DO NOT WRITE THIS... And the voice I heard from heaven said: GO, TAKE THE OPEN BOOK FROM THE ANGEL'S HAND... And I went to the angel and said unto him, GIVE ME THE BOOK. And he said unto me, Take it, and eat it; it shall be bitter in thine heart, but in thy mouth it shall be as sweet as honey. And I took the book out of the angel's hands, and I ate it. And he said unto me, Thou shalt prophesy again of peoples and nations, and tongues, and kings of many peoples. (Ap. 10:1-4, 10:8-11). And then the angel said to John, "Do not seal up the words of the prophecy of this book... And I also testify to everyone who hears the words of the prophecy of this book.

*(Ap. 22:10, 22:19)*

**Russian:**

Что мы узнали из слегка путаного, но тем не менее достаточно ясного рассказа? Сначала Ангел показал Иоанну некую книгу с описанием происходящих событий, но потребовал не разглашать ее содержания: "СКРОЙ... И НЕ ПИШИ СЕГО" (Ап. 10:4). Более того, Ангел потребовал, чтобы Иоанн "съел первую книгу". Что и было исполнено. Тем самым, содержание первой книги оказалось полностью утраченным. Взамен Ангел приказал Иоанну написать новую, вторую книгу, причем ОБНАРОДОВАТЬ ЕЕ: "не запечатывай слов пророчества книги сей" (Ап. 22:19). Что и было сделано. Иоанн написал тот вариант Апокалипсиса, который, надо полагать, и дошел до нас. Таким образом, нам сообщили, что на самом деле были две книги. Некая первая, уничтоженная, "съеденная", и следовательно, расценивавшаяся как "неправильная". И вторая, "правильная", написанная заново, под диктовку Ангела. Именно так и воспринимали этот сюжет средневековые иллюстраторы Апокалипсиса. Например, на известной гравюре А.Дюрера, рис.d2.15, мы видим Ангела, протягивающего Иоанну первую книгу. Иоанн "съедает" ее. А рядом с ним уже лежит вторая книга, написанная Иоанном. На земле, около второй книги, мы видим письменные принадлежности: пузырек с чернилами и т.п. Совершенно недвусмысленно показан процесс создания второй книги на основе "съеденной" первой. То обстоятельство, что они обе одновременно изображены на гравюре, не должно нас смущать. Средневековые художники широко пользовались "приемом мультфильма", последовательно изображая НА ОДНОМ И ТОМ ЖЕ ЛИСТЕ несколько следующих друг за другом событий, разделенных промежутком времени. Как бы разворачивая события во времени.

**English:**

What have we learned from a slightly confusing, yet clear enough story? At first, the angel showed John a certain book describing the events that were taking place, but demanded not to disclose its contents: "WARNING... AND DO NOT WRITE THIS." (Ap. 10:4). Moreover, the angel demanded that John "eat the first book". Which was fulfilled. Thus, the content of the first book was completely lost. In return, the angel ordered John to write a new, second book, and to BELIEVE it: "Do not seal the words of this book prophecy" (Ap. 22:19). (Ap. 22: 19.) This was done. John wrote that version of the Apocalypse which, it is believed, came to us. Thus, we were told that there were actually two books. The first one, destroyed, "eaten", and therefore regarded as "wrong". And the second, "correct", written anew, at the dictation of the Angel. This is how the medieval illustrators of the Apocalypse perceived the plot. For example, on the famous engraving by A. Durer, fig.d2.15, we see an Angel extending the first book to John. John is "eating" it. And next to him, there's a second book written by John. On the ground, near the second book, we see the writing instruments: a vial of ink, etc. The process of creating the second book on the basis of the "eaten" first book is clearly shown. The fact that they are both depicted in the engraving at the same time should not embarrass us. Medieval artists widely used the "cartoon reception", consistently depicting on ONE and the same figure in several consecutive events, divided by a period of time. As if unfolding events in time.

**Russian:**

Тем самым Апокалипсис не скрывает, что первоначальная его версия была уничтожена, а потом написана заново. Именно это мы и утверждаем, говоря, что дошедшие до нас библейские книги подверглись редакции в XVII-XVIII веках.

**English:**

Thus the Apocalypse does not hide the fact that its original version was destroyed, then written anew. This is what we claim, saying that extant biblical books were edited in XVII-XVIII centuries.

**Russian:**

Не нужно, впрочем, думать, что первый вариант Апокалипсиса был действительно в буквальном смысле съеден Иоанном Богословом. Скорее всего, тут мы вновь сталкиваемся с путаницей, возникшей у позднейших редакторов Апокалипсиса при попытке прочесть и осознать фразу вроде: "Иоанн съел книгу". Речь шла, скорее всего, о том, что Иоанн ВНИМАТЕЛЬНО ПРОЧЕЛ первую книгу, а затем на ее основе создал свой вариант. Ведь до сих пор говорят, что кто-то "ПРОГЛОТИЛ КНИГУ", то есть жадно и быстро ее прочитал. Любителей чтения так и характеризуют: "глотают книги". Подобное выражение, вероятно, и стояло во второй книге Иоанна, вновь, еще раз, подвергнувшейся тенденциозному редактированию. Редактор мог не понять переносного выражения "проглотил книгу" и ошибочно (или специально) воспринял его буквально: "Иоанн съел книгу". Так и записал в новую версию Апокалипсиса. Получилась странность. А позднейшие послушные художники, вроде А.Дюрера, принялись тщательно изображать - как именно Иоанн жует и, давясь, съедает бумажную или пергаментную книгу. Даже если они и понимали бессмысленность такой "картинки", ослушаться церковного канона никак не могли. С тех пор и кочует по библейским иллюстрациям образ Святого Иоанна, буквально поедающего большую книгу, рис.d2.16.

**English:**

However, we should not think that the first version of the Apocalypse was really literally eaten by John the Theologian. Most likely, here we are again confronted with the confusion that later editors of the Apocalypse experienced when trying to read and understand a phrase like: "John ate a book". It was most likely that John had read the first book and then created his own version based on it. It is still said that someone "Eaten the BOOK", that is, has read it quickly, greedily. That's how people who like to read are characterized: "swallowing books." This expression was probably found in John's second book, which was once again subjected to tendentious editing. The editor may have misunderstood the portable expression "swallowed a book" and misunderstood (or purposefully misunderstood) it literally: "John ate the book. So he wrote down a new version of the Apocalypse. It was a strange thing. And later obedient artists, like A. Durer, began to carefully depict how John chews and, pressed, eats a paper or parchment book. Even if they understood the senselessness of such "pictures", they could not disobey the church canon. Since then the image of St. John, literally eating the big book, fig.d2.16 is found in biblical illustrations.

**Russian:**

Указанную нами путаницу слов легко понять, поскольку первый вариант Апокалипсиса, судя по цитированному выше сюжету, был действительно уничтожен. Так что вполне могли сказать, что "его съели".

**English:**

The confusion of words indicated by us is easy to understand, because the first version of the Apocalypse, judging by the above cited story, was indeed destroyed. So it could well have been said that "it was eaten."

**Russian:**

Из сказанного следует, что переработка первичного варианта Апокалипсиса была довольно значительной. Иначе вряд ли пришлось бы уничтожать первую книгу и вместо нее ЗАНОВО писать новую. Так что речь шла вовсе не об "исправлении опечаток". С Апокалипсисом проделали нечто куда более существенное. Вероятно, его рассматривали как весьма ценное и важное произведение, которое должно быть особо тщательно приведено в соответствие с новыми стандартами и целями эпохи Реформации XVII века.»

**English:**

It follows from the above that the processing of the primary version of the Apocalypse was quite significant. Otherwise it would hardly be necessary to destroy the first book and instead of it to write a new one. So it wasn't about "correcting typos" at all. The Apocalypse was about something much more significant. Probably, it was seen as a very valuable and important work, which should be particularly carefully brought into line with the new standards and objectives of the Reformation era of the XVII century.

**Russian:**

К этому можно добавить, что на самом деле тот факт, что «Апокалипсис» был написан именно Апостолом Иоанном, автором одного из Евангелий, тоже нельзя считать полностью доказанным. Кроме того, что автор несколько раз в тексте называет себя Иоанном, других явных указаний на это нет. В тоже время многие исследователи указывают на тот факт, что текст «Апокалипсиса» по своему стилю написания очень сильно отличается от текста Евангелия от Иоанна. Кроме того, «Апокалипсис» полностью построен на Ветхозаветных образах и символике, что говорит о том, что данный текст писал человек, который очень хорошо знал именно Ветхий Завет или Тору, то есть был не просто иудеем, а являлся одним из священнослужителей, поскольку имел доступ к текстам Ветхого Завета или Торы. Для простых прихожан священные тексты были недоступны практически до 18 века.

**English:**

To this we can add that the fact that the "Apocalypse" was written by the Apostle John, the author of one of the Gospels, can not also be considered fully proven. Apart from the fact that the author calls himself John several times in the text, there is no other clear indication of this. At the same time, many researchers point to the fact that the text of the "Apocalypse" in its style of writing is very different from the text of the Gospel of John. Moreover, the "Apocalypse" is entirely based on Old Testament images and symbols, which indicates that this text was written by a person who knew the Old Testament or Torah very well, i.e. he was one of the clergy who had access to the texts of the Old Testament or Torah. For ordinary parishioners the sacred texts were not practically available until the 18th century.

**Russian:**

Также необходимо отметить тот факт, что нам сейчас доступен в основном только один вариант текста «Апокалипсиса», который окончательно был сформирован только в 60-70-х годах 19-го века, когда был сделан так называемый [Синодальный перевод](https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D0%BD%D0%BE%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4) Библии на русский язык.

**English:**

Also it is necessary to note that we now have basically only one variant of the text of the "Apocalypse" which was finally generated only in the 60-70th years of the 19th century when the so-called [Synodal translation](https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D0%BD%D0%BE%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4) of the Bible into Russian language was made.

**Russian:**

При этом в статье по ссылке выше, посвящённой «Синодальнмоу переводу» есть один интересный фрагмент: «В 1826 году дело перевода и издания было приостановлено в силу общего изменения внутриполитической ситуации в России. Перевод книг Ветхого Завета, который к тому времени достиг книги Руфь, был остановлен. Доходило до сожжения хранившихся на складах непроданных экземпляров. Дальнейшая печать популярных в народе Нового Завета и Псалтыри также были запрещены, хотя репринты и продолжали осуществляться за границей и поступать в Россию.» Возникает вопрос, а зачем было запрещать Новый Завет и Псалтырь и уничтожать непроданные экземпляры? Что в них было такого важного, что их необходимо было уничтожить? И не для того ли их уничтожали, чтобы выпустить новые исправленные издания?

**English:**

At the same time, there is one interesting fragment in the article at the above link, devoted to "Synodalnmo translation": "In 1826 the case of translation and publication was suspended due to general changes in the domestic political situation in Russia. Translation of books of the Old Testament, which by that time had reached the book of Ruth, was stopped. It came to the burning of unsold copies stored in warehouses. Further printing of popular New Testament and Psalms was also prohibited, although reprints continued to be made abroad and to arrive in Russia. The question arises, why was it necessary to forbid the New Testament and the Psalter and to destroy unsold copies? What was so important in them that it was necessary to destroy them? And presumably they were destroyed in order to release new editions?

**Russian:**

В-третьих, у меня был вопрос о том, почему жена Лота превратилась в соляной столб, когда оглянулась назад на взрыв, которым были уничтожены Содом и Гоморра? Мне кажется, что я нашёл ответ на этот вопрос. Дело в том, что на иврите соль обозначается словом «мелах». Слог ме означает движение «от». Слог лах — «влажность». В целом слово мелах (соль) обозначает нечто, что уменьшает, сводит на нет влажность. Таким образом скорее всего имеет место неточность перевода, и изначально автор хотел сказать, что она превратилась не в соль, а высохла, засохла, потеряла всю влагу. Плюс к этому, слово, которое было переведено как «обернулась», также могло означать «вернулась назад».

**English:**

Third, I had a question about why Lot's wife turned into a pillar of salt when she looked back at the explosion that destroyed Sodom and Gomorrah? I think I found the answer to that question. The fact is, in Hebrew, salt is indicated by the word "chalk." The word me means "from". The syllable lah is "moisture." In general, the word chalk (salt) means something that reduces and negates moisture. Thus there is most likely a translation inaccuracy and the author originally wanted to say not that she has turned into salt, but that she dried up, ie lost all moisture. On top of that, a word that was translated as "turned around" could also mean "turned back".

**Russian:**

В-четвёртых, в процессе написания и публикования последней части появилась статья [Происхождение песка](http://chispa1707.livejournal.com/1131870.html), в которой автор задаётся вопросом, а точно ли весь песок образовался именно на Земле? А если на Земле, то из чего, благодаря каким процессам? В процессе обсуждения этого материала я вспомнил и рассказал в комментариях о Чарских песках в середине Сибирской тайги, в результате чего появилась следующая статья [Чарская пустыня. Забайкальский край](http://sibved.livejournal.com/150384.html). Одна из гипотез, которая обсуждалась в комментариях, состоит в том, а не могли ли эти пески упасть на поверхность Земли из космоса? И тут я вспомнил два факта. Первый, что когда песок поднимается ветром в воздух, то небо окрашивается в кроваво-красный цвет. Именно по тому, что небо на горизонте «наливается кровью» бедуины в пустыне узнают о том, что приближается песчаная буря. А второй факт, что в той же Библии при описании катаклизмов неоднократно упоминается о том, что «небо налилось кровью». Есть такое упоминание, кстати, и в Апокалипсисе:

**English:**

Fourthly, in the process of writing and publishing the last part, an article [Origin of Sand](http://chispa1707.livejournal.com/1131870.html) appeared, in which the author asks himself a question, was all sand was formed on Earth? And if it was on Earth, then from what, thanks to what processes? During the discussion of this material I remembered and mentioned in the comments the Charskie sands in the middle of the Siberian taiga, which resulted in the following article [Charskie desert. Transbaikal region](http://sibved.livejournal.com/150384.html). One of the hypotheses discussed in the comments is whether these sands could have fallen to the Earth's surface from space? And then I remembered two facts. The first is that when the sand rises in the air, the sky is coloured bloody red. It's because the sky on the horizon is "poured with blood" that the Bedouins in the desert know that a sandstorm is approaching. And the second fact is that in the same Bible, when describing cataclysms, it is repeatedly mentioned that "the sky is poured with blood". There is such a mention, by the way, in the Apocalypse:

**Russian:**

> 7. Первый Ангел вострубил, и сделались град и огонь, смешанные с кровью, и пали на землю; и третья часть дерев сгорела, и вся трава зеленая сгорела. 

*(Откровение Иоанна Богослова 8:7)*

**English:**

> 7. The first Angel sounded, and hail and fire came, mixed with blood, and fell to the ground; and a third of the trees burned, and all the grass was green. 

*(Revelation 8:7).

**Russian:**

Так что очень вероятно, что песок выпал на землю именно из космоса, и этот факт даже задокументирован в Библии.

**English:**

So it is very likely that the sand fell to the ground from space, and this fact is even documented in the Bible.

**Russian:**

В комментариях также был вопрос, а почему на песке «нет следов прохождения через атмосферу», то есть, почему он не был оплавлен, как те же каменные метеориты. Тут дело в том, что песчинки очень маленькие, поэтому имеют большую площадь поперечного сечения при очень маленькой массе. Поэтому при вхождении в атмосферу они очень быстро затормозятся, потеряв свою кинетическую энергию, а потом будут достаточно медленно оседать на землю. По этой же причине песок легко поднимается в воздух даже не очень сильным ветром. Так что никаких следов оплавления на песчинках быть и не должно. При этом выпадать на землю этот песок может достаточно далеко от точки вхождения в атмосферу. То есть, следы от метеоритов могут быть в одном месте, а песок может быть отнесён воздушным течением совсем в другое место.

**English:**

The comments also asked why the sand bore "no trace of passing through the atmosphere". That is why it was not melted like stone meteorites. The fact is that the grains of sand are very small and so have a large cross-sectional area with a very small mass. When they enter the atmosphere, they slow down very quickly, losing their kinetic energy, and then settle down slowly enough on the ground. For the same reason, sand is easily lifted into the air even by a weak wind. So there's no trace of melting on the grains of sand. At the same time, this sand can fall to the ground far enough from the point of entry into the atmosphere. That is, meteorite tracks can be in one place, and sand that arrived with them can be carried by air currents to a completely different place.

**Russian:**

19 Потоп

**English:**

19 Flood

<!-- Local
![](How Tartaria was Lost Part 7_files/93241_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/93241/93241_original.jpg#resized)

**Russian:**

Ещё один момент, который не давал мне покоя, связан с всемирным потопом. В Библии при его описании сказано:

**English:**

Another thing that kept me busy was the global flood. The Bible says in its description:

**Russian:**

> 17. И продолжалось на земле наводнение сорок дней, и умножилась вода, и подняла ковчег, и он возвысился над землею; 
> 18. вода же усиливалась и весьма умножалась на земле, и ковчег плавал по поверхности вод. 
> 19. И усилилась вода на земле чрезвычайно, так что покрылись все высокие горы, какие есть под всем небом; 
> 20. на пятнадцать локтей поднялась над ними вода, и покрылись горы. 
> 21. И лишилась жизни всякая плоть, движущаяся по земле, и птицы, и скоты, и звери, и все гады, ползающие по земле, и все люди; 
> 22. все, что имело дыхание духа жизни в ноздрях своих на суше, умерло. 
> 23. Истребилось всякое существо, которое было на поверхности земли; от человека до скота, и гадов, и птиц небесных, — все истребилось с земли, остался только Ной и что было с ним в ковчеге. 
> 24. Вода же усиливалась на земле сто пятьдесят дней. 

*(Книга Бытие 7:17-24)*

**English:**

> 17. And the flood continued forty days on earth, and the water multiplied, and lifted up the ark, and it rose above the earth; 
> 18. The water intensified and multiplied greatly on earth, and the ark floated on the surface of the water. 
> 19. And the water of the earth became exceedingly strong, so that all the high mountains, as they are under all the sky, were covered; 
> 20. And by fifteen cubits the water rose above them, and the mountains were covered. 
> 21. And all flesh moving on the earth, and birds, and cattle, and beasts, and all creeps crawling on the earth, and all men; 
> 22. And all that had the breath of the spirit of life in their nostrils on land died. 
> 23. Every creature that was on the surface of the earth was exterminated; from man to cattle, and crawling things, and birds of the sky, all that was exterminated from the earth, and Noah alone remained, and what was with him in the ark. 
> 24. 24. The water, on the other hand, increased on the earth one hundred and fifty days. 

*(Book of Genesis 7:17-24)*

**Russian:**

По описанию это не похоже на действие инерционной волны, которая вызвана ударом большого метеорита, который привёл к повороту Земного шара и смещению оси вращения, из-за чего северный географический полюс сместился где-то на 15 градусов. О том, что такое смещение было, говорит ориентация старых пирамид, например в той же Латинской Америке, ориентация которых сбита как раз на 15 градусов. Подобный удар должен был вызвать мощную инерционную волну, похожую на гигантское цунами, но в несколько раз сильнее. Вот только эта волна должна была пройти, возможно погулять по океанам некоторое время, но со всё затухающей амплитудой, после чего вода должна была стечь обратно в океан. Причём всё это не могло продолжаться ни 40, ни тем более 150 дней. Также из описания в Библии получается, что вода поднялась над вершинами гор не сразу, как если бы это была инерционная волна, а лишь после 40 дней. Значит должна быть какая-то другая причина, которая вызвала резкое увеличение количества воды на планете.

**English:**

According to the description this does not look like the action of an inertial wave, caused by the impact of a large meteorite, which triggered the rotation of the globe and shifted the axis of rotation by about 15 degrees from the then north geographic pole. That such a shift took place was proven by the orientation of old pyramids, for example, and the same in Latin America, where the orientation of pyramids shifted by 15 degrees. Such a blow should have caused a powerful inertial wave, similar to a giant tsunami, but several times stronger. Except that this wave would have had to pass, and then perhaps traverse the oceans for some time with ever-decreasing amplitude, after which the water had to flow back into the ocean. And all of this could not have lasted 40 days, much less 150 days. Also from the description in the Bible, it appears that the water did not rise over the tops of the mountains immediately, as it would if it were an inertial wave, but only after 40 days. So there must have been some other factor that caused the water to surge over the planet.

**Russian:**

И тут я вспомнил про ролики о теории гидридного строения Земли Ларина В.Н., которые недавно смотрел! Желающие могут посмотреть их тут [https://www.youtube.com/watch?v=jKolxRWihKc&index=2&list=PL7D3D2756926506B8](https://www.youtube.com/watch?v=jKolxRWihKc&index=2&list=PL7D3D2756926506B8), обсуждение темы на форуме ЛАИ тут [http://laiforum.ru/viewtopic.php?t=595](http://laiforum.ru/viewtopic.php?t=595)

**English:**

And then I remembered the theory of the hydride structure of the Earth - Larin VN's - which I recently watched! Those who wish can see them here: [https://www.youtube.com/watch?v=jKolxRWihKc&index=2&list=PL7D3D2756926506B8](https://www.youtube.com/watch?v=jKolxRWihKc&index=2&list=PL7D3D2756926506B8), and read discussion of the theme at the LAI forum here: [http://laiforum.ru/viewtopic.php?t=595](http://laiforum.ru/viewtopic.php?t=595).

**Russian:**

18 Гидридная Земля

**English:**

18 Hydride Earth

<!-- Local
![](How Tartaria was Lost Part 7_files/93479_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/93479/93479_original.jpg#resized)

**Russian:**

Общий смысл этой теории состоит в том, что ядро планеты состоит не из железа и никеля в чистом виде, как считалось ранее, а из их соединений у водородом — гидридов железа и никеля. Особенность этих соединений в том, что они устойчивы только при высоком давлении и сравнительно низкой температуре. При этом объём гидридов меньше, чем аналогичный объём просто металлов (в роликах даётся обоснование почему это так, что уже подтверждено экспериментально). Эта же гидридная теория очень хорошо объясняет механизмы роста Земли в размерах, что и приводит к тому, что материки расходятся друг от друга. При этом если понижается давление или повышается температура, то гидриды начинают интенсивно распадаться, выделяя много водорода. Этот водород просачивается через трещины в земной коре на поверхность, что тоже уже зафиксировано наблюдениями. Достигнув атмосферы, особенно озонового слоя, водород вступает в реакцию с кислородом и озоном, образуя воду. Эта вода и образует облака в верхних слоях атмосферы, которая постепенно опускается вниз, выпадая в конечном итоге в виде дожей на поверхность.

**English:**

The general sense of this theory is that the core of the planet does not consist of iron and nickel in pure form, as was earlier thought, but of their hydrogen compounds:  iron and nickel hydrides. The peculiarity of these compounds is that they are only stable at high pressure and relatively low temperature. In this case, the volume of hydrides is less than the similar volume of simple metals (the film gives a justification why this is so, which has already been experimentally confirmed). The same hydride theory very well explains the mechanisms of the Earth's growth in size, which leads to the continents being separated from each other. In this case, if the pressure drops or the temperature rises, the hydride begins to disintegrate intensively, releasing a lot of hydrogen. This hydrogen seeps through cracks in the earth's crust to the surface, which is also documented by observations. Once it reaches the atmosphere, especially the ozone layer, hydrogen reacts with oxygen and ozone to form water. This water forms clouds in the upper atmosphere, which gradually descends to the surface, eventually precipitating as rain.

**Russian:**

Но гидридная теория также хорошо объясняет откуда взялась лишняя вода при столкновении с крупным метеоритом! Во время столкновения происходит деформация земного шара, в том числе смещение вещества внутри Земли, что подтверждается картограммой гравитационного поля, на котором чётко видны аномалии вызванные данным столкновением. С одной стороны, часть вещества внутри во время удара резко сжимается, что приводит к повышению его температуры. С другой стороны, из-за нарушения формы Земли где-то образуются области повышенного давления, а где-то, наоборот, давление резко понизится. Оба этих процесса приведут к тому, что гидриды металлов ядра начнут интенсивно распадаться, выделяя много лишнего водорода. Кроме этого, во время столкновения неизбежно образуются повреждения земной коры в виде крупных трещин, которые будут способствовать выхожу на поверхность как вновь образовавшегося лишнего водорода, так и того, который образовывался и накапливался до этого во внутренних полостях. Это резкое повышение выделения водорода из недр Земли должно было в свою очередь вызвать образование большого количества новой воды в атмосфере. Причём, в отличие от инерционной волны, этот процесс мог продолжаться достаточно продолжительное время, отсюда и 150 дней, в течении которых прибывала вода.

**English:**

But hydride theory is also a good explanation of where the excess water comes from during a large meteorite impact! During the collision, the Earth is deformed, including the displacement of matter inside the Earth, as evidenced by the gravity field map, which clearly shows the anomalies caused by the collision. On the one hand, part of the matter inside the collision shrinks sharply during the impact, which leads to an increase in its temperature. On the other hand, because of the disturbance of the Earth's shape, areas of high pressure are formed somewhere and, on the other hand, the pressure will drop sharply somewhere. Both of these processes will cause the core metal hydrides to intensively break down, releasing a lot of excess hydrogen. In addition, the collision will inevitably cause damage to the earth's crust in the form of large cracks, which will contribute to the release of both the newly formed excess hydrogen and that which was previously formed and accumulated in the inner cavities. This sharp increase in the release of hydrogen from the Earth's interior should in turn have produced large amounts of new water in the atmosphere. Moreover, unlike the inertial wave, this process could continue for quite a long time, from here to 150 days, during which time the water arrived.

**Russian:**

После того, как после 150 дней вода перестала прибывать, её уровень начал постепенно понижаться. В целом потом, судя по описанию в Библии, продолжался десять с половиной месяцев. Вот начало потопа:

**English:**

After 150 days the water stopped arriving, its level began to fall gradually. In total then, according to the Bible description, it lasted ten and a half months. This is the beginning of the flood:

**Russian:**

> 11. В шестисотый год жизни Ноевой, во второй месяц, в семнадцатый день месяца, в сей день разверзлись все источники великой бездны, и окна небесные отворились… 

*(Книга Бытие 7:11)*

**English:**

> 11. In the six hundredth year of Noah's life, in the second month, on the seventeenth day of the month, all the springs of the great abyss opened, and the windows of heaven opened ... 

*(Book of Genesis 7:11)*

**Russian:**

А это окончание:

**English:**

And this is the end of it:

**Russian:**

> 13. Шестьсот первого года к первому дню первого месяца иссякла вода на земле; и открыл Ной кровлю ковчега и посмотрел, и вот, обсохла поверхность земли. 

*(Книга Бытие 8:13)*

**English:**

> 13. Six hundred of the first year by the first day of the first month, the water of the earth dried up; and Noah opened the roof of the ark, and looked, and beheld the surface of the earth dried up. 

*(Book of Genesis 8:13)*

**Russian:**

Соответственно, пять месяцев вода прибывала, и пять с половиной месяцев убывала, скорее всего стекала во внутренние полости Земли, которые освободились от вышедшего оттуда газа. Если вспомнить теорию абиогенного происхождения нефти, то одной из реакций синтеза углеводородов является взаимодействие воды с карбидами металлов. При этом углерод из карбидов металлов соединяется с водородом из молекул воды, образуя углеводородные цепочки, металлы частично восстанавливаются, а частично образуют оксиды, а часть кислорода при этом высвобождается и выделяется в виде газа обратно в атмосферу. Тем самым количество воды в целом должно было уменьшится, а количество кислорода, который ушёл на образование дополнительной воды, вернуться обратно в атмосферу.

**English:**

Accordingly, water arrived for five months and for five and a half months the water dwindled, most likely flowing into the interior cavities of the Earth, which were liberated from the gas previously released from them. If we remember the theory of the abiogenic origin of oil, one of the reactions of synthesis of hydrocarbons is the interaction of water with metal carbides. In this case, carbon from metal carbides is combined with hydrogen from water molecules, forming hydrocarbon chains, metals are partially recovered and partially form oxides, while part of the oxygen is released as a gas back into the atmosphere. In this way, the amount of water in general was reduced and the amount of oxygen that went to form the additional water was returned to the atmosphere.

**Russian:**

В очередной раз, вроде бы не связанные между собой вещи, тем не менее начинают как части мозаики выстраиваться в общую картину, увязывая всё между собой.

**English:**

Once again, things that are seemingly unrelated nevertheless begin to line up as parts of a mosaic in the overall picture, linking everything together.

**Russian:**

Есть ещё один момент, который я хотел рассмотреть более подробно. Некоторые из читателей задавали вопрос, каким образом можно было осуществить подобный катаклизм, сбросив на поверхность Земли не один метеорит, а целое метеоритное поле. И откуда в космосе могло оказаться столько замёрзшей воды, камней и песка.

**English:**

There's one more thing I wanted to consider in more detail. Some of the readers asked how such a cataclysm could be accomplished by dropping not one meteorite, but an entire meteor field on the surface. And how could there be so much frozen water, rocks and sand in space.

**Russian:**

Я придерживаюсь версии, что астероидное поле в нашей Солнечной системе является остатками одной из разрушенной захватчиками планет, которую называют Фаэтон и которая находилась между Марсом и Юпитером, то есть именно там, где сейчас находится пояс астероидов.

**English:**

I adhere to the view that our solar system's asteroid field is the remnants of one of the planets destroyed by the invaders, called Phaeton, and which was located between Mars and Jupiter, exactly where the asteroid belt is now.

**Russian:**

20 пояс астероидов_1

**English:**

20 asteroid belt_1

<!-- Local
[![](How Tartaria was Lost Part 7_files/93697_900.jpg#clickable)](How Tartaria was Lost Part 7_files/93697_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/93697/93697_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/93697/93697_original.jpg)

**Russian:**

Подробности об этой теории можно, например, [почитать тут](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D1%8D%D1%82%D0%BE%D0%BD_%28%D0%BF%D0%BB%D0%B0%D0%BD%D0%B5%D1%82%D0%B0%29) или поискать в Гугле по запросу «планета Фаэтон».

**English:**

Details about this theory can be found, for example, [here](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D1%8D%D1%82%D0%BE%D0%BD_%28%D0%BF%D0%BB%D0%B0%D0%BD%D0%B5%D1%82%D0%B0%29) or by searching for "planet Phaeton".

**Russian:**

Если уж на то пошло, то в нашей Солнечной системе много странностей, в результате чего она больше похожа на поле битвы. Ось вращения Земли наклонена на 23 градуса от вертикального положения, при этом плоскость орбиты вращения наклонена к солнечному экватору на 7,155° Но и у остальных планет полный разброд и шатание.

**English:**

For that matter, there are many strange things in our solar system that make it look more like a battlefield. The axis of rotation of the Earth is inclined on 23 degrees from the vertical, and the plane of orbit of rotation is inclined to the solar equator by 7,155°. But also other planets show full dispersion and wobbling.

**Russian:**

| № | Планета | Наклон оси вращения | Наклонение орбиты |
| - | - | - | -|
| 1 | Меркурий | 2,11′ | 7,00° |
| 2 | Венера | 177,36° | 3,4° |
| 3 | Земля | 23°26’ | 0 |
| 4 | Марс | 25,2° | 1,85° |
| 5 | Юпитер | 3,13° | 1,03° |
| 6 | Сатурн | 26,73° | 2,5° |
| 7 | Уран | 97,77° | 0,77° |
| 8 | Нептун | 28,32° | 1,77° |
| 9 | Плутон | 119,6° | 17,14° |


**English:**

| No. | Planet | Tilt rotation axis | Tilt orbit |
| - | - | - | -|
| 1 | Mercury | 2,11′ | 7,00° |
| 2 | Venus | 177.36° | 3.4° |
| 3 | Land | 23°26' | 0 |
| 4 | Mars | 25,2° | 1,85° |
| 5 | Jupiter | 3,13° | 1,03° |
| 6 | Saturn | 26.73° | 2.5° |
| 7 | Uranium | 97.77° | 0.77° |
| 8 | Neptune | 28.32° | 1.77° |
| 9 | Pluto | 119.6° | 17.14° |

**Russian:**

Венера перевёрнута вверх ногами, то есть, вращается в противоположную сторону, чем все остальные планеты. Уран лежит на боку, причём его ось также уже с другой стороны плоскости орбиты. Та же ситуация и с Плутоном, который не только лежит на боку и вращается в другую сторону, но ещё и крутиться вокруг Солнца в плоскости, которая наклонена на 17 градусов к плоскости орбиты Земли (плоскость эклиптики), что намного больше, чем у всех остальных планет.

**English:**

Venus is upside down, that is, it rotates in a direction opposite to all other planets. Uranus lies on its side, and its axis is also on the other side of its plane of orbit. Pluto is in the same situation. It not only lies on its side and rotates in the other direction, but also rotates around the Sun in a plane that is tilted 17 degrees to the plane of Earth's orbit (the plane of the ecliptic), which is a much larger tilt than all the other planets.

**Russian:**

Официальная теория образования планет из газо-пылевого облака говорит, что все планеты должны вращаться вокруг своей звезды примерно в одной плоскости, а оси вращения самих планет должны быть практически вертикально. Как официальная теория объясняет почему у планет солнечной системы это не так, я думаю все читающие этот цикл статей с начала угадают с первого раза — это столкновение с другими небесными телами!

**English:**

The official theory of the formation of planets from the gas-dust cloud says that all planets must rotate around their stars approximately in one plane, and the axes of rotation of the planets themselves must be virtually vertical. The official theory also explains why the planets of the solar system do not do so. I think everyone reading this series of articles from the beginning will get it on the first guess - it was a collision with other celestial bodies!

**Russian:**

Вопрос только в том, сами ли эти другие небесные тела прилетают в планеты или им кто-то специально помогает?

**English:**

The only question is, do these other celestial bodies come to the planets themselves or does someone help them on purpose?

**Russian:**

Чтобы путешествовать по Вселенной за разумное время, цивилизация должна либо научится управлять гравитацией, либо освоить технологию так называемого гиперскачка/гиперперехода/телепортации, поскольку технологии известные сегодня нам, такие как жидкостный реактивный двигатель или различные варианты ионных двигателей, это очень долго и затратно.

**English:**

In order to travel across the universe in a reasonable amount of time, civilizations must either learn how to control gravity, or master the technology of so-called hyperspace or hyperspace-teleportation, since technologies known to us today, such space travel with liquid fuel engines or various versions of ion engines, is very long and expensive.

**Russian:**

Чтобы уронить на поверхность планеты метеоритное поле состоящее из десятков тысяч объектов с шириной фронта в 800 км, следы падения которого мы можем наблюдать в Западной Сибири, также необходимо уметь управлять гравитационным полем. Все остальные способы не годятся. Взрыв в безвоздушном пространстве будет малоэффективен, даже термоядерный. Кроме того, при взрыве объекты полетят не в одном направлении, а в разные стороны от эпицентра взрыва, но и то при условии, что будет чем передать импульс движения. Это в атмосфере возникает ударная воздушная волна, а в космосе воздуха, а значит и ударной волны нет. Толкать каждый из многих тысяч объектов по отдельности? И сколько же на это уйдёт времени и энергии?

**English:**

To drop on a planet's surface a meteor field consisting of tens of thousands of objects with a frontal width of 800 km, the tracks of which we can observe in Western Siberia, you must also be able to control the gravity field. All other methods are unsuitable. An explosion in an airless space would be ineffective, even a thermonuclear explosion. Besides, in explosions objects do not fly in one single direction, but in different directions radiating from the explosion's epicenter, and on the condition that there will be something to transmit the movement impulse. It is in the atmosphere that a shock air wave occurs, and in space there is no atmosphere to carry a shock wave. Push each of the many thousands of objects separately? And how much time and energy would that take?

**Russian:**

Так что других разумных вариантов, кроме как создать возмущение гравитационного поля, чтобы придать облаку метеоритов вектор движения в нужном направлении, я не вижу.

**English:**

So I don't see any other reasonable option other than to create a gravity field disturbance to give the meteor clouds a vector of motion in the right direction.

**Russian:**

Но если в Солнечной системе действует некая цивилизация, которая умеет управлять гравитационным полем и швырять по планетам большими метеоритами или даже ронять на них их же спутники, то из этого следует ещё один интересный вывод.

**English:**

But if there is a certain civilization in the solar system, which knows how to control the gravitational field and throw large meteorites at planets, or even drop their own satellites on them, then another more interesting conclusion follows from this.

**Russian:**

В своё время мне очень понравилась новая хронология Носовского и Фоменко тем, что они разработали пуленепробиваемую методику датировки исторических событий. Она основана на том, что в описании многих событий присутствует в том или ином виде упоминание астрономических явлений, таких как затмение Солнца или Луны, восхождение того или иного знака Зодиака и т. п. Например, подобные указания имеются в Новом Завете, поскольку Иисус был распят сразу после Пасхи и в этот момент произошло Солнечное затмение. Исходя из того, что мы знаем периоды обращения планет и их спутников вокруг Солнца, которые считаем постоянными во времени, мы можем рассчитывать астрономические явления не только в будущем, но и в прошлом. На основании этого Носовский и Фоменко и сделали привязку многих исторических событий к новым датам, а уже потом начали вокруг этих дат выстраивать свою «Новую хронологию», находя всё новые и новые свидетельства того, что история нашей цивилизации была сфальсифицирована. Сразу скажу, что я не совсем согласен с той версией событий, которую пытаются предложить Носовский и Фоменко, но то, что базовые даты событий, от которых они выстраивают всю систему, определена по их методике верно, до недавнего времени у меня не вызывало сомнения.

**English:**

At one time I really liked the new chronology of Nosovsky and Fomenko because they developed a bulletproof method of dating historical events. It is based on the fact that in the description of many events there is present in one form or another a mention of astronomical phenomena such as an eclipse of the Sun or the Moon, the rise of a sign of the Zodiac, etc. For example, there are similar descriptions in the New Testament, where Jesus was crucified right after Easter and at that time there was a solar eclipse. Proceeding from the fact that we know periods of the planets and their satellites around the Sun which we consider constant in time, we can calculate astronomical phenomena not only in the future, but also in the past. On this basis Nosovsky and Fomenko bound many historical events to new dates and then began to build their "New Chronology" around these dates, finding more and more new evidence that the history of our civilization had been falsified. I'll say right away that I don't quite agree with the version of events that Nosovsky and Fomenko are trying to offer, but until recently I had no doubts that the base dates of events from which they built the whole system determined by their method was correct, 

**Russian:**

Вот только всё это работает ровно до тех пор, пока мы считаем движения планет в Солнечной системе неизменными во времени. Принимаем их за некий эталон, который и позволяет определять точные даты исторических событий.

**English:**

Except that it all works precisely only for as long as we consider the movements of the planets in the solar system to be constant in time. We take them as a reference that allows us to determine the exact dates of historical events.

**Russian:**

А теперь добавляем в эту, казалось бы непоколебимую систему, присутствие в Солнечной системе цивилизации, которая может управлять гравитацией, а, следовательно, может изменять параметры движения планет, как путём непосредственного воздействия на ту или иную планету, так и за счёт их столкновения к другими крупными объектами или их спутниками. Вся стройная система, которая казалась такой непоколебимой, тут же рассыпается как карточный домик.

**English:**

And now we add to this seemingly unshakeable system the presence of a civilization in the solar system, which can control gravity and, consequently, can change the parameters of the motion of the planets, both through direct impact on one or another planet, and through their collision with other large objects or their satellites. An entire slender system that seemed so unshakeable crumbles immediately like a house of cards.

<!--
**Russian:**

### Как погибла Тартария? Часть 7 окончательная

[https://mylnikovdm.livejournal.com/7086.html](https://mylnikovdm.livejournal.com/7086.html)

**English:**

### How did Tartaria die? Part 7 is final.

[https://mylnikovdm.livejournal.com/7086.html](https://mylnikovdm.livejournal.com/7086.html)


2 December, 2014


**Russian:**

предыдущая часть

**English:**

previous section
-->

**Russian:**

В последнее время стало всплывать очень много фактов, которые указывают на то, что история нашей земной цивилизации искажена значительно больше, чем об этом пишут в своих работах Носовский и Фоменко. Их методика состоит в том, что они ставят под сомнения даты, имена исторических персонажей и названия тех или иных объектов, которые у разных народов действительно могли называться по разному, но при этом они не ставят под сомнение сами события, описываемые в том или ином документе. Раз некое событие описано, значит, считают Носовский и Фоменко, оно действительно было, нужно только тем или иным способом правильно определить дату и место этого события, а также тех людей, о которых на самом деле идёт речь. При этом, надо отдать им должное, они проделали гигантскую работу по анализу и систематизации очень большого объёма информации, различных документов и фактов. И всё равно к той версии, которую они сформировали, остаётся масса вопросов. В том числе и потому, что она объясняет далеко не все факты, которые мы наблюдаем.

**English:**

Recently a lot of facts began to appear which indicate that the history of our earth civilization is distorted much more than Nosovsky and Fomenko write about it in their works. Their method consists in the fact that they question the dates, names of historical characters and names of certain objects, which really could be named differently by different peoples, but they do not question the very events described in this or that document. Once a certain event is described, it means, consider Nosovsky and Fomenko, that the event really was. It is necessary only in this or that way to correctly define the date and a place of this event, and also those people whom it involved. At the same time, we have to give them credit, they have done a gigantic job of analyzing and systematizing a very large amount of information, various documents and facts. And still, there are a lot of questions about the version they have created. This is because it does not explain all the facts that we observe.

**Russian:**

Например, их теория никак не наблюдает те парадоксы, которые наблюдаются в Санкт-Петербурге, а также многих других городах, где явно использовались строительные технологии, которых в то время, согласно обоих версий, быть не могло, а некоторых из этих технологий мы сейчас даже не знаем. Не объясняет «Новая хронология» и наблюдаемую повсеместно засыпку зданий и сооружений, о которой пишут многие авторы, в том числе и я в пятой части.

**English:**

For example, their theory does not observe in any way the paradoxes that can be seen in St. Petersburg, as well as many other cities where construction technologies were clearly used, which, according to both versions, could not exist at that time. Even now, we do not know some of these technologies. "New chronology" does not explain the everywhere observed backfill of buildings and constructions about which many authors write, including me in the fifth part of this series.

**Russian:**

Во время обсуждения предыдущих фрагментов читатели прислали ссылку на весьма интересную карту мира 1575 года, которая нарисована французом Francois De Belleforest. Если внимательно посмотреть на эту карту, то обнаруживается много интересного, что не вписывается ни в официальную историю, ни в [Новую хронологию](http://mylnikovdm.livejournal.com/3759.html).

**English:**

During the discussion of the previous fragments, readers sent a link to a very interesting 1575 world map drawn by the French Francois De Belleforest. If you look closely at this map, you will find many interesting things that do not fit into the official history or [New Chronology](http://mylnikovdm.livejournal.com/3759.html).

**Russian:**

21 Старая карта 1575 Francois De Belleforest

**English:**

21 Old map, 1575, Francois De Belleforest

<!-- Local
[![](How Tartaria was Lost Part 7_files/94139_900.jpg#clickable)](How Tartaria was Lost Part 7_files/94139_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/94139/94139_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/94139/94139_original.jpg)

**Russian:**

Удалось найти эту карту в достаточно высоком разрешении, что, к сожалению, возможно не для всех старых карт, на которые мне присылали ссылки. Полноразмерный вариант, на котором отлично читаются все подписи, открывается по ссылке с изображением.

**English:**

I was able to find this map in quite high resolution, which unfortunately may not be the case for all the old maps that I was sent links to. The full-size variant, where all captions are perfectly readable, is opened by a clicking on the image.

**Russian:**

На данной карте интересно то, что изображены и подписаны наиболее крупные или важные города. При этом много городов изображено на территории Африки. Также мы можем там наблюдать реки, которые на сегодняшней карте не существуют.

**English:**

What is interesting about this map is that it shows and labels the largest or most important cities. Many cities are depicted in Africa. We can also see rivers there that do not exist on today's map.

**Russian:**

Работая над циклом статей про Тартарию я просмотрел множество старых карт, и у всех них есть одна особенность. На них авторы могут совершенно неправильно изображать форму и положение рек, озёр и морей, островов и континентов, но при этом практически всегда правильно изображается топология объектов, то есть, их взаимосвязи. Ошибок в том, на какой реке стоят основные города, какая река в какую другую реку, озеро или море впадает, какие моря с какими другими морями или океанами соединяются проливами, практически нет. Объясняется это достаточно просто. Точно измерять расстояния и фиксировать форму объектов ещё не умели, но вот где какие проливы, через которые можно проплыть в те или иные страны, или по каким рекам нужно плыть, чтобы попасть в тот или иной город, было очень хорошо известно от множества путешественников и торговцев.

**English:**

While working on the series of articles about Tartaria, I looked at many old maps, and they all have one feature. They may depict the shape and position of rivers, lakes and seas, islands and continents completely incorrectly, but the topology of objects, that is, their interrelationships, is almost always correctly depicted. There is practically no mistake concerning on which river the main cities stand, which river flows into which other river, lake or sea, which straits connect which seas with which other seas or oceans. The explanation is simple enough. We were not yet able to accurately measure distances and record the shape of objects, but this is the location of the straits through which you can swim to this or that country, or on what rivers to sail to get to a city, was very well known by many travelers and traders.

**Russian:**

Кроме того, подобную конфигурацию рек на территории Северной Африки, где вообще-то должна находиться пустыня Сахара, наблюдается и на других картах вплоть до первой половины 18 века. И только после этого момента на этом месте начинают обозначать «Grand Desert Sahara», то есть великую пустыню Сахара. Выходит, что в середине 16-го века Сахары в Африке ещё не было?

**English:**

In addition, a similar configuration of rivers in North Africa, where the Sahara desert should actually be located, is observed on other maps until the first half of the 18th century. And only after this time does this place start to be called the "Grande Desert Sahara". That is, the great Sahara desert. It turns out that in the middle of the 16th century, the Sahara was not yet in Africa?

**Russian:**

Также интересно, что если названия городов в Европе, на Ближнем Востоке, на территории Индии и севере Африки более менее соответствуют тем, что мы знаем, то в той же Сибири или на территории нынешнего Китая ничего и близко нет! Причём городов в Сибири изображено на удивление много, в том числе и явно за полярным кругом: Taingim, Naiman, Turfon, Coβin, Calami, Obea. Вам эти названия о чём нибудь говорят?

**English:**

It is also interesting that if the names of cities in Europe, the Middle East, India and North Africa are less in line with what we know, there is nothing close to the same Siberia or the current China! And there are surprisingly many cities in Siberia, including obviously beyond the Arctic Circle: Taingim, Naiman, Turfon, Coβin, Calami, Obea. Do these names tell you anything about anything?

**Russian:**

С территорией современного Китая тоже непонятно что. Названия большинства городов явно не китайские. И куда подевался Пекин (Běijīng)?! А ведь считается, что Считается, что Пекин был крупнейшим городом мира в периоды [с 1425 по 1650](http://geography.about.com/library/weekly/aa011201d.htm) и [с 1710 по 1825](http://geography.about.com/library/weekly/aa011201e.htm) годы. Но мы видим на карте в этом месте множество городов, но только не самый крупный город планеты. Или китайцев ещё к нам на Землю не переселили и это произошло после 1575 года?

**English:**

With the territory of modern China, it's not clear either. The names of most cities are clearly not Chinese. And where did Běijīng go?! It is believed that Beijing was the largest city in the world in the periods [1425-1650](http://geography.about.com/library/weekly/aa011201d.htm) and [1710-1825](http://geography.about.com/library/weekly/aa011201e.htm). We see a lot of cities in this place on the map, but not the largest city on the planet. Or have the Chinese not yet been moved to Earth and they did so only after 1575?

**Russian:**

Во время обсуждения этой карты, было высказано предположение, не мог ли автор нарисовать несуществующие города «чтобы было красиво». Но если посмотреть на ту же Северную Америку, то автор там ничего не выдумает. Нет городов, значит ничего и не изображаем. Да и в Европе он ничего не придумал. Хотя, и там есть странности. Весьма интересно какие города для автора показались значимыми в Европе и на территории России. В Европе обозначены: Lisbona (Лиссабон), Sevilla, Lion, Brest, Paris, Ausburg, Wien, Danzic, Cracow, Buda, Ragura(?), Bergen, плюс не совсем разборчиво обозначен явно Константинополь. А вот чуть правее и ниже его мы видим Трою (Troia)!!! То есть, во второй половине 16 века её местонахождение не просто было прекрасно известно, но и сам город ещё существовал. А официальная версия истории утверждает, что Троя исчезла до нашей эры. Кстати, а где же Рим? Или на пиренейском полуострове места не хватила для значка и надписи?

**English:**

During the discussion of this map, it was suggested that the author could not have drawn non-existent cities "to be beautiful". But if you look at the same North America, the author would not make anything up there. No cities, so we don't depict anything. And he didn't invent anything in Europe either. Although, there are strange things there. It is very interesting what cities seemed significant for the author in Europe and in Russia. In Europe are marked: Lisbona (Lisbon), Sevilla, Lion, Brest, Paris, Ausburg, Wien, Danzic, Cracow, Buda, Ragura(?), Bergen, plus not quite legible but clearly marked Constantinople. But a little to the right and below it we see Troy (Troia)!!! That is, in the second half of the 16th century Troy's location was not only well known, but the city itself still existed. And the official version of history claims that Troy disappeared before our era. By the way, where is Rome? Or was there not enough space on the Iberian Peninsula for a badge and inscription?

**Russian:**

На территории России обозначены Москва, Вышеград, Новгород, Соловки (!!!), и некий S. Nicolas - Святой Николай(?).

**English:**

On the territory of Russia are marked Moscow, Vyshegrad, Novgorod, Solovki (!!!), and a certain S. Nicolas - St. Nicholas (?).

**Russian:**

Прямо скажем, не густо. Или это обозначены только административные центры территорий? Если так, то тогда какая же плотность населения в Северной Африке и в Сибири, если там столько административных центров?

**English:**

Let's just say it's not many. Or were they just designated as the administrative centers of the territories? If so, then what is the density of population in North Africa and Siberia, if there are so many administrative centers?

**Russian:**

Также интересно, какие европейские государства по мнению автора, оказались достойны отображения на карте: Англия, Испания, Галлия, Германия, Греция, Италия, Россия, Швеция и Норвегия. Да уж, как-то не густо. Тартария, кстати, обозначена, хотя граница показана так, что туда попадает практически всё нижнее и среднее повольже. Да и общая площадь, которую автор отнёс к Тартарии, сравнительно небольшая.

**English:**

It is also interesting to know which European countries, in the author's opinion, were worthy of being shown on the map: England, Gaul, Germany, Greece, Italy, Russia, Spain, Sweden and Norway. Yeah, it's not that many. Tartaria, by the way, is marked, although the border is shown in such a way that it gets almost all the lower and middle frequencies. And the total area, which the author attributed to Tartaria, is relatively small.

**Russian:**

При этом интересно, что сама Франция по мнению автора карты, который якобы француз, называется именно Галлия, как во времена Римской империи. Вот только Рим при этом отсутствует. Ну ладно, со всем остальным, допустим, автор мог наврать, но как его страна называется, которая согласно официальному мифу ко времени правления Людовика XI (1461—1483) фактически покончила с феодальной раздробленностью и превратилась в абсолютную монархию, он должен был знать? При этом нельзя сказать, что эту карту составлял полный неуч, поскольку многие вещи отображены и обозначены совершенно правильно. И сдаётся мне, что этому французу (или галличанину?) верить можно больше, чем официальному историческому мифу. А если это так, то в 1575 году катастрофа, которая привела к образованию пустыни Сахара, ещё не произошла. Там ещё существуют города и реки, которые после катастрофы исчезли.

**English:**

At the same time, it is interesting that France itself, according to the author of the map, which is supposedly French, is called Gaul, as in the days of the Roman Empire. Except that Rome is absent. Okay, with everything else, let's say, the author could lie. But what is his country called, which, according to the official myth, he had to know because by that time of the reign of Louis XI (1461-1483) it had actually finished with feudal fragmentation and turned into an absolute monarchy? It cannot be said, however, that this map was drawn by a complete ignoramus, because many things are depicted and marked quite correctly. And it seems to me that this frenchman (or Gallican?) believed more than the official historical myth. And if so, then in 1575 the catastrophe that led to the formation of the Sahara desert had not yet happened. There are still cities and rivers there, which disappeared after the catastrophe.

**Russian:**

Но, отложим старые карты, поскольку чем больше их изучаешь, тем больше вопросов появляется, так что к этой теме вернёмся чуть позже, за пределами цикла статей про гибель Тартарии.

**English:**

But let's postpone the old maps, because the more you study them, the more questions arise, so we'll get back to this topic a little bit later, outside the Tartaric death series.

**Russian:**

После публикации ссылок на Апокалипсис получил один весьма интересный комментарий:

> по поводу уничтоженных тираже. 
> на всякий случай - человек я не религиозный, для меня библия - не предмет поклонения, а памятник истории и культуры. 
> бабушка рассказывала, что в детстве слышала альтернативную версию "Аппокалипсиса", которую ей читали по какой-то старой библии. то, что запомнилось ей и что запомнил в ее пересказе я. 
> "и Земля будет опутана стальной паутиной, и летать будут по небу стальные птицы... все овражки и буераки распашут, а досыта не наедятся". ну и согласно того "Аппокалипсиса" все это будет уже в последние времена, после чего все будет совсем плохо. 
> попытался исследовать этот вопрос и узнал, что о подобном тексте (в пересказе) упоминают многие люди (география - от Архангельска до Урала), что для меня является аргументом - с моей бабушкой они явно не были знакомы, значит где-то объективно существовал такой источник. кстати по воспоминаниям последний раз саму книгу видел у своей родственницы и листал подросток в 50х годах прошлого века. 
> итого - была некая альтернативная версия Библии, она была издана большим тиражом - похоже, она была чуть ли не в каждой деревне, раз и через сотню лет потомки жителей тех деревень про нее помнят. логично предположить, что информация дошла в хоть каком-то виде благодаря массовости источника. ну а вспомнилась благодаря многим совпадения предсказаний с реалиями нового времени. 
> мне было бы гораздо спокойнее узнать, что в какой-то библиотеке хранится сохранившийся экземпляр той библии. но что-то подсказывает, что там где легко найти ничего уже нету.». [http://mylnikovdm.livejournal.com/6658.html?thread=407810#t407810](http://mylnikovdm.livejournal.com/6658.html?thread=407810#t407810)

**English:**

After the links to the Apocalypse were published, I received a very interesting comment:

> Just in case - I'm not a religious man, for me the Bible is not an object of worship, but a monument of history and culture. 
> my grandmother told me that as a child she heard an alternative version of the Appocalypse, which she was read in some old Bible. What she remembered and what I remembered in her paraphrase:
> "and the Earth will be entangled in a steel web, and steel birds will fly in the sky .... and all the ravines and bueraks will be plowed, and they won't get fed up." And according to the Appocalypse, all this will be in the last few days, and then it will be very bad. 
> tried to investigate this question and found out that many people mention such a text (in the paraphrase) (geography - from Arkhangelsk to the Urals), which for me is an argument - with my grandmother, they obviously did not know, so somewhere objectively there was such a source. By the way, by the way, the last time he saw the book itself was at his relatives and thumbed through a teenager in the 50s of last century. 
> It seems that it was published in almost every village, once and a hundred years later the descendants of the villagers remember it. It is logical to assume that the information has reached at least some form due to the mass of the source. 
> I'd be much safer to know that some library has a preserved copy of that Bible. But something tells me that there will be nothing left to find." [http://mylnikovdm.livejournal.com/6658.html?thread=407810#t407810](http://mylnikovdm.livejournal.com/6658.html?thread=407810#t407810)

**Russian:**

Сразу скажу, что я пока ничего подобного не слышал, но информация очень интересная. Поэтому у меня просьба к читателям, если кто-то что-то подобное тоже слышал или читал, большая просьба поделиться информацией любым удобным для вас способом. Хоть через комментарии, хоть на электронную почту [mylnikov_dm@mail.ru](http://www.vesti.ru/doc.html?id=1037057#)

**English:**

I'll tell you right away that I haven't heard anything like this yet, but the information is very interesting. So I have a request to the readers, if someone has heard or read something like this too, a big request to share the information in any way convenient for you. At least through comments, at least to e-mail [mylnikov_dm@mail.ru](mylnikov_dm@mail.ru)

**Russian:**

Ещё один интересный комментарий на сайте kramola.info:

**English:**

Another interesting comment on kramola.info:

**Russian:**

> Десант... Десант меня убил... Это сколько надо десанта, чтобы покрыть такую территорию? 1,5 млн кв.км только пораженных земель, ещё миллионов 7 кв.км ближних окрестностей, не говоря уже о дальних. Миллионы единиц техники? Несущей индивидуально защищённые, вооружённые биологические объекты, ведь речь идёт о тотальной зачистке. С другой стороны - ведь неодновременно везде... Ресурсы, которые были переброшены к планете и задействованы в подобной миссии, сложно себе представить, настолько они количественно- и энергоёмки. Цель должна оправдывать средства. А во-вторых - где выгодообладатель? В чьих интересах всё это было осуществлено? Мда...

**English:**

> Landing... The landing party killed me... How long does it take for a landing party to cover that area? 1.5 million square kilometers of affected land alone, another million 7 square kilometers in the vicinity, not to mention the distant ones. Millions of pieces of equipment? Carrying individually protected, armed biological objects, because it's a total sweep. On the other hand, it's not the same everywhere... The resources that have been transferred to the planet and involved in such a mission, it's hard to imagine, they are so quantitatively and power-consuming. The goal must justify the means. And secondly, where is the beneficial owner? In whose interest was all this carried out? Well...

**Russian:**

В данном конкретном месте я лично не вижу серьёзных проблем, поскольку в Апокалипсисе об этом дословно говорится следующее:

> 5. И дано ей не убивать их, а только мучить пять месяцев; и мучение от нее подобно мучению от скорпиона, когда ужалит человека.

*(Откровение Иоанна Богослова 9:5)*

**English:**

In this particular issue, I personally do not see any serious problems, because the Apocalypse literally says this:

> 5. And it is given her not to kill them, but to torment them for five months; and her torment is like that of a scorpion when it stings a man.

*(John the Theologian's Revelation 9:5)*

**Russian:**

По воздействию это больше похоже на применение некоего химического или бактериологического оружия, которое распылялось с воздуха. А с учётом времени в пять месяцев, это можно было сделать сравнительно небольшим количеством аппаратов.

**English:**

The effect is more like the use of some kind of chemical or bacteriological weapon, which was sprayed from the air. And given a time period of five months, it could be done with a relatively small number of machines.

**Russian:**

Но следующий фрагмент, на который также указали некоторые читатели, меня, если честно, пока ставит в тупик.

**English:**

But the next fragment, which was also pointed out by some readers, has me, to be honest, still stumped.

**Russian:**

> 15. И освобождены были четыре Ангела, приготовленные на час и день, и месяц и год, для того, чтобы умертвить третью часть людей. 
> 16. Число конного войска было две тьмы тем; и я слышал число его. 

*(Откровение Иоанна Богослова 9:15,16)*

**English:**

> 15. And four Angels were released, prepared for an hour and a day, and a month and a year, to kill a third of the people. 
> 16. The number of the mounted army was two of those darkness; and I heard his number. 

*(Revelation of John 9:15,16)*

**Russian:**

Старое русское числительное «тьма» означает десять тысяч. Исходя из этого в данном фрагменте речь идёт о численности войска в 200 млн. единиц. Это слишком больше число даже для сегодняшнего дня. А для более древних времён тем более. Привезти такое количество войск через космос, это тоже весьма не простая задача. Разве что вместо корабля использовался небольшой планетоид типа Луны, но это означает колоссальные затраты энергии. Но тогда, как верно было замечено в комментариях, ради чего такие затраты? Какова тогда реальная цель захватчиков. То есть, тут либо явное преувеличение рассказчика, либо мы чего-то не совсем понимаем о том, что происходит.

**English:**

The old Russian numeral "darkness" means ten thousand. Proceeding from this fragment we are talking about a troop count of 200 million units. It's too big a number even for today. And for older times, all the more so. Bringing so many troops through space isn't an easy task either. Unless they used a small planetoid like the moon instead of a ship, but that means a tremendous amount of energy. But then, as was rightly noted in the comments, what are the costs? Then, what is the real purpose of the invaders? So it's either an obvious exaggeration of the narrator, or we don't quite understand what's going on.

**Russian:**

И, наконец, ответ на последний комментарий, прежде чем я выйду на финишную прямую.

**English:**

And finally, the answer to the last comment before I go straight to the finish line.

**Russian:**

Уже несколько человек высказались в таком духе: «Слушайте, кроме старинных европейских карт есть хоть одно доказательство существования Тартарии?»

**English:**

Several people have already said this: "Look, apart from ancient European maps, is there any proof of the existence of Tartaria?"

**Russian:**

На самом деле лично мне не суть важно, как называлось это уничтоженное в начале 19-го века государство, Тартария или ещё как. Но по тем фактам, что уже собрано в количестве, я вижу, что в 18 веке династия Романовых-Ольденбургов при поддержке европейских правящих династий начала захват территорий Православного государства. И захват этот начался с установления контроля над Санкт-Петербургом. Была сделана попытка захвата Москвы в 1773-1775 годах, что мы знаем как «восстание крестьян под предводительством Емельяна Пугачёва» но она закончилась неудачно, хотя официальная Романовская версия истории нас сегодня уверяет в обратном. Именно поэтому потребовалась вторая война 1810-1815 годов, к которой Романовы-Ольденбурги тщательно готовились много лет, в том числе построив множество каналов соорудив три речные системы для прохода судов, чтобы обеспечить себе логистику наступательных военных операций на Москву и другие не захваченные ещё города. В 1812 году захватываются Смоленск и Москва, в 1815 году Казань. Это те города, по которым я факты уже проверил.

**English:**

It doesn't really matter to me personally whether the name of this state that was destroyed in the beginning of the 19th century was Tartaria or whatever. But from the facts that have already been collected in quantity, I see that in the 18th century the Romanov-Oldenburg dynasty, supported by European ruling dynasties, began to seize the territories of the Orthodox state. And this seizure began with the establishment of control over St. Petersburg. There was an attempt to capture Moscow in 1773-1775, that we know as "peasant uprising led by Yemelyan Pugachev" but it ended unsuccessfully, although the official Romanov version of history today assures us the opposite. That is why it took the second war of 1810-1815, for which the Romanov-Oldenburgs prepared carefully for many years, including the construction of many canals, building three river systems for the passage of ships to ensure the logistics of the offensive military operations in Moscow and other cities not yet captured. In 1812, Smolensk and Moscow were captured, and in 1815, Kazan. These are the cities on which I have already checked the facts.

**Russian:**

И в апреле 1815 года в Западной Сибири происходит масштабная планетарная катастрофа, организованная «господами», которым верно служит западная правящая элита. Эта катастрофа уничтожила тех людей, которые на этой территории жили в то время. Примерно в этих же годах, в районе 1810-1815 годов также проводится зачистка Уральских гор с применением ядерного оружия. Всё это вместе приводит к тому, что Романовы-Ольдебурги получают возможность с минимальными потерями достаточно быстро присоединить эти территории к своей новообразованной Империи. Существовавшая на территории Западной Сибири экономическая, транспортная и военная инфраструктура во многом разрушены, огромное количество людей погибло, остальные дезорганизованы и находятся на грани выживания. То есть, оказать масштабное сопротивление новым властям просто физически некому. Я кстати, не исключаю, что старое государство тоже могло называться Российской Империей, и Романовы-Ольденбурги просто прибрали это название к рукам, чтобы было проще фальсифицировать документы.

**English:**

And in April 1815 in Western Siberia there is a large-scale planetary disaster, organized by "gentlemen" who faithfully serve the Western ruling elite. This catastrophe destroyed the people who lived in this territory at the time. Around the same years, in the area of 1810-1815, the Ural Mountains were also cleaned up using nuclear weapons. All this together makes it possible for the Romanov-Oldeburgs to join these territories to their newly formed Empire fairly quickly with minimal losses. The economic, transport and military infrastructure that existed in Western Siberia has been largely destroyed, a huge number of people have died, and the rest are disorganized and on the edge of survival. In other words, there is simply no one physically able to resist the new authorities on a large scale. By the way, I don't rule out that the old state could have been called the Russian Empire, too, and the Romanov-Oldenburgers simply took that name into their hands to make it easier to falsify documents.

**Russian:**

Где-то в 20-30х годах 19-го века начинается процесс замены истинного Православия на его суррогат, который частично использует внешнюю атрибутику Православия, но по своей сути и идеологии является иудо-христианством, религией для рабов. Этим то и объясняется тот факт, что уже изданные тиражи Нового Завета и Псалтыри уничтожаются, а распространение их старых вариантов запрещается. Вместо них выпускается новый основательно исправленный вариант, который сегодня известен как «Синодальный перевод».

**English:**

Somewhere in the 20-30s of the 19th century begins the process of replacing the true Orthodoxy with its surrogate, which partially uses the external attributes of Orthodoxy, but in its essence and ideology is Judaic Christianity, a religion for slaves. This explains the fact that the already published copies of the New Testament and the Psalms are destroyed, and the distribution of their old versions is prohibited. Instead, a new, substantially revised version is issued, which is now known as the "Synodal Translation".

**Russian:**

Также интересно, что руководство переводом осуществлял митрополит Московский и Коломенский Филарет. А согласно официальному историческому мифу первую серьёзную реформу православия затеял тоже митрополит Московский Филарет, который якобы был Фёдор Никитич Романов, отец якобы первого царя из династии Романовых - Михаила. Насмотревшись вдоволь на то, как эти ребята работают, я в случайность подобных совпадений уже не верю.

**English:**

It is also interesting that the translation was managed by Metropolitan Filaret of Moscow and Kolomna. And according to the official historical myth the first serious reform of Orthodoxy was started also by Metropolitan Filaret of Moscow, who was - ostensibly - Fyodor Nikitich Romanov, father of ostensibly the first tsar of the Romanov dynasty: Michael. Having looked enough at how these guys work, I no longer believe in a coincidence of similar coincidences.

**Russian:**

Да и вообще, если говорить про Романовых, то это не династия, а вообще непонятно что. Генетическая связь между первым поколением и последним Романовым отсутствует в принципе, при этом прерывалась она и менялась на другую минимум три раза. Там только на бумаге всё хорошо, а по факту сплошные убийства и государственные перевороты.

**English:**

And in general, if we talk about the Romanovs, it's not a dynasty, and it's not clear what it was. There is no genetic connection between the first generation and the last Romanov, and it was interrupted and changed to another at least three times. Everything is fine there on paper, but the fact is that there are continuous killings and coups d'état.

**Russian:**

В этом плане гораздо интереснее изучить другой феномен, на который тоже указывали некоторые из читателей. Как не пытается Западная цивилизация захватить и подчинить себе Русь, ни черта у неё не выходит! Так или иначе, но Святая Русь всё время восстанавливается как Феникс из пепла. Прорастает сквозь новую систему и переделывает её под себя. В итоге им всё время приходится снова и снова решать проблему России. Несколько раз в 18 веке, несколько раз в 19 веке, три раза в 20-ом веке, в 1914-1917, в 1941-1945, потом вроде уже добили в 1991. Но хлоп, и опять не вышло! Ну не приживается у нас паразитизм и всё тут!

Вот это, действительно, настоящее чудо. На эту тему у меня тоже есть некоторые соображения, но это уже в другой статье.

**English:**

In this regard, it is much more interesting to study another phenomenon, which was also pointed out by some of the readers. If Western civilization isn't trying to grasp and subjugate Russia, then Hell isn't working! But each time, Sacred Russia is restored like the Phoenix from ashes. It grows through a new system and remakes itself. As a result, they have to solve the Russia problem over and over again all the time. Several times in the 18th century, several times in the 19th century, three times in the 20th century, in 1914-1917, in 1941-1945, and then it seems to have been finished in 1991. But clap, and again it did not work!

Well, we don't have parasitism and everything is here!

This is really a miracle. I have some thoughts on this too, but it's in another article.

**Russian:**

Многие задают следующий вопрос. Вот вы всё так хорошо расписали, и даже вроде убедили, что захватчики такие крутые и могущественные действительно существуют. Так и что же делать? Куда бежать, как спасаться и можно ли с ними вообще как-то справиться?

**English:**

Many people are asking the following question. You've painted everything so well, and you even seem to have convinced us that invaders so cool and powerful really do exist. So, what do we do? Where do you run, how do you save yourself, and is there any way to deal with them?

**Russian:**

Это и будет последний вопрос, ответом на который я хочу поставить точку в этом цикле статей о гибели Великого Русского Государства в Сибири, которое всё равно возродится вновь.

**English:**

This will be the last question, the answer to which I want to use to put a point at the end of this series of articles about the death of the Great Russian State in Siberia, which will be reborn yet again.

**Russian:**

Я давно уже собираю информацию и анализирую собранные факты. С одной стороны, в данный момент у меня нет сомнений, что цивилизация земли столкнулась с внешней силой, к встрече с которой оказалась по каким-то причинам не готова. Эта техногенная цивилизация паразитического типа, действие которой проявляется не только в порабощении жителей захваченных миров, но и в распространении своей модели поведения, что я образно обозначил как «распространение информационного вируса паразитизма». То есть, есть вирусы биологические, которые заражают физическое биологическое тело. Но есть также вирусы информационные, которые поражают ум и сознание человека. И бороться с такими вирусами намного сложнее, чем с биологическими, хотя последствия их воздействия могут быть не менее разрушительными и губительными для цивилизации.

**English:**

I have been collecting information and analyzing the facts for a long time. On the one hand, at the moment I have no doubt that the civilization of the earth has faced an external force, which for some reason I was not ready to meet. This technogenic civilization of parasitic type, the action of which is manifested not only in the enslavement of the inhabitants of the captured worlds, but also in the spread of its model of behavior, which I figuratively denoted as "the spread of information virus of parasitism". That is, there are biological viruses that infect the physical biological body. But there are also information viruses, which affect the mind and consciousness of man. And it is much more difficult to fight with such viruses than with biological ones, though the consequences of their influence can be no less destructive and destructive for civilization.


**Russian:**

Так что один из ответов на вопрос «что делать?» состоит в том, что каждый должен найти в себе силу Духа, чтобы не поддаться на соблазн и не позволять себе тем или иным способом паразитировать на других. Силой ли, обманом ли, хитростью, не важно. Главное оставаться Человеком, а не превращаться в сволочь.

**English:**

So one of the answers to the question "what to do?" is that everyone must find the power of the Spirit in him or herself, so that he or she does not give in to temptation or allow himself or herself to parasitize in one way or another. Whether by force, deception, or cunning is not important. The main thing is to remain human, not to turn into a bastard.

**Russian:**

Второй ответ в том, что также важно по мере возможностей не позволять другим паразитировать на себе и своих близких. Но тут пока, увы, наши возможности ограничены той системой, в которой мы вынуждены выживать. И изменение этой системы одна из наших общих совместных задач. В одиночку этого никто из нас решить не сможет.

**English:**

The second answer is that it is also important, to the extent possible, to prevent others from parasitizing themselves and their loved ones. But for now, alas, our capabilities are limited by the system in which we have to survive. And changing that system is one of our shared goals. None of us can solve it alone.

**Russian:**

Также я заметил в поведении захватчиков несколько странных вещей. Во-первых, то, что они скрываются, говорит не только о том, что они пытаются ввести в заблуждение людей Земли. Судя по старым мифам разных народов, раньше они этого не делали. Исходя из этого я пришёл к выводу, что им есть кого боятся помимо нас, именно по этой причине они на Земле всё пытаются обставить так, как будто бы это мы сами устраиваем весь этот бардак.

**English:**

I've also noticed some strange things about the invaders' behavior. First, the fact that they're hiding doesn't just mean that they're trying to mislead the people of Earth. Judging from the old myths of different peoples, they've never done it before. From this I have come to the conclusion that there is someone they are afraid of besides us, which is why they are trying to make everything on Earth look as if it is us who are making this mess.

**Russian:**

Во-вторых, в последние двести лет наблюдался взрывной рост техногенной науки и развития основанных на ней технологий. Причём нам начинают открывать и позволять пользоваться и развивать достаточно серьёзные вещи, типа того же атомного оружия, электроники или современных вооружений.

**English:**

Secondly, the last two hundred years have seen an explosive growth in technogenic science and the development of technologies based on it. And we are beginning to discover and enable the use and development of quite serious things, like the same atomic weapons, electronics or modern weapons.

**Russian:**

В итоге у меня сложилось устойчивое впечатление, что эти «господа» выстраивают тут, на Земле, необходимую им производственную базу. А это они могут делать только по двум причинам. Либо их исходный мир был уничтожен, поэтому необходимо более капитально обосноваться в новом, либо тот их отряд, который контролировал Солнечную систему, был тут блокирован подошедшими силами спасения с остальных братских миров, и они вынуждены накапливать силы, чтобы прорвать блокаду.

**English:**

As a result, I have a stable impression that these "gentlemen" are building the necessary production base here on Earth. And they can only do that for two reasons. Either their original world has been destroyed, so it is necessary to settle down for the long-term in a new world, or their detachment, which controlled the solar system, is blocked here by the coming rescue forces from other brotherly worlds, and they have to accumulate forces to break the blockade.

**Russian:**

На последний вариант указывает и тот факт, что непосредственно самих особей инопланетной расы, насколько я смог выяснить, достаточно мало. Кроме того, для меня совершенно очевидно, что большую часть открытый и технологий человечество изобрело самостоятельно, хотя и с наводящими подсказками. По этому поводу я и ещё несколько читателей, приславших свои комментарии и письма, независимо пришли к выводу, что исходным языком инопланетной расы является Латынь. Некоторые исследователи считают его мёртвым или искусственно созданным языком, поскольку в его возникновении и распространении много странностей. Но тот факт, что в так называемой «чёрной магии» все заклинания читаются на латыни, в католической церкви служба ведётся на латыни, а также практически все первые научные книги по математике, физике, химии и медицине были написаны исключительно на латыни, а сама латынь была обязательной для изучения во всех старых университетах, указывают на тот факт, что это и есть язык захватившей нас техногенной цивилизацией, на котором они и передали имевшиеся у них знания (хотя и не все), и именно поэтому они используется для общения сними при церковных обрядах (публичный режим) или во время магических ритуалов (закрытый, тайный режим).

**English:**

This last option is also indicated by the fact that, as far as I have been able to find out, there is not enough of the alien race itself. Moreover, for me it is quite obvious that most open technology was invented by mankind itself, albeit with suggestive clues. On this matter, I and several other readers who sent their comments and letters independently came to the conclusion that the original language of the alien race is Latin. Some researchers consider it a dead or artificially created language, because there are many strange things about its origin and spread. But the fact that in so-called "black magic" all spells are read in Latin, that the Catholic Church service is conducted in Latin, and also practically all the first scientific books on mathematics, physics, chemistry and medicine were written exclusively in Latin, and that Latin itself was obligatory for studying in all old universities, point to the fact that this is the language of the technogenic civilization that has captured us. That Latin is the language with which they transferred their knowledge (although not all of it), and that is why it is used for communication during church rites (public regime) or during magical rituals (closed, secret regime).

**Russian:**

То, что подошедшие силы лишь блокировали Солнечную систему, но не начинают операцию по освобождению, насколько я смог понять, связан с двумя причинами. Во-первых, он не знают, сможет ли цивилизация Земли побороть вирус паразитизма. А без этого, то есть без полного выздоровления, карантин с Земли снят не будет, даже если захватчики будут нейтрализованы. А во-вторых, захватившая нас цивилизация держит население Земли в качестве заложников. Именно для этого и была затеяна вся эта эпопея с открытием нам ядерных технологий и создания гигантского арсенала ядерного оружия, которое в конечном счёте находится под контролем правящей элиты, а значит под контролем захватчиков, которым эта элита подчиняется. В случае попыток прямого вмешательства третьей силы, они угрожают уничтожить Землю вместе со всем населением, что делает бессмысленной подобные спасательные операции.

**English:**

My belief that approaching forces were previously only blocking the solar system, but are not starting a rescue operation, as far as I can understand, is due to two reasons. First, it doesn't know if civilization on Earth can defeat the parasitism virus. And without that, that is, without full recovery, quarantine will not be lifted from Earth, even if the invaders are neutralized. And secondly, the civilization that has captured us is holding the population of the Earth as hostages. This is why the whole epic with the discovery of nuclear technology and the creation of a giant arsenal of nuclear weapons was started. These weapons are ultimately under the control of our ruling elite, and thus under the control of the invaders, to whom this elite is subordinate. If a third force tries to intervene directly, they threaten to destroy the Earth along with the entire population, making the rescue element of any such operation meaningless.

**Russian:**

В итоге все оказались в патовой ситуации. При этом, что интересно, сейчас всё зависит именно от нас, от населения Земли. От того, какой выбор сделает каждый из живущих сейчас людей. Встать на сторону захватчиков и принять паразитическую идеологию или остаться человеком, но при этом отказавшись от роскоши и власти, которой его прельщают.

**English:**

Ultimately, everyone is in a stalemate. However, interestingly enough, now everything depends on us, on the population of the Earth. On what choice each of the people living will now make. To take the side of the invaders and accept the parasitic ideology or to remain human, but at the same time giving up the luxury and power that seduces us.

**Russian:**

В том, что третья сила уже здесь, и что она играет именно на нашей стороне, лично у меня не осталось никаких сомнений после 15 февраля 2013 года, когда в небе над Челябинском инопланетный корабль сбил каменную глыбу, которую захватчики запустили, чтобы продемонстрировать свои возможности. Если бы тот метеорит не был сбит и долетел до той точки, куда его направляли, всего этого цикла статей просто бы не было.

**English:**

The fact that the third force is already here, and that it is playing on our side, personally I have no doubts after February 15, 2013, when in the sky over Chelyabinsk an alien ship shot down a stone block, which the invaders launched to demonstrate their capabilities. If that meteorite had not been shot down and flew to the point where it was sent, this whole series of articles simply would not have happened.

**Russian:**

О том, что он был именно сбит, сообщили даже по центральным каналам. Желающие в этом убедиться могут посмотреть ролике по ссылкам ниже.

**English:**

The fact that it was shot down was reported even on the central channels. Those wishing to make sure of this can watch the video on the links below.

[http://www.vesti.ru/doc.html?id=1037057#](http://www.vesti.ru/doc.html?id=1037057#) (Possibly no longer available)

[http://www.youtube.com/watch?v=R4Vm_AabHBI](http://www.youtube.com/watch?v=R4Vm_AabHBI)

[http://www.youtube.com/watch?v=7Aw1dWPvWGw](http://www.youtube.com/watch?v=7Aw1dWPvWGw) (no longer available)

**Russian:**

Так что, други, помощь уже здесь. Устроить очередную катастрофу из космоса им больше не позволят. А вот то, что будет происходить на земле, теперь всецело зависит только от нас с вами.

На этом я, наконец-то, ставлю точку в цикле статей о катастрофе в Западной Сибири.

**English:**

So, folks, help is here. They won't be allowed to create another disaster from space anymore. But what happens on earth is now entirely up to you and me.

Here I finally put an end to a series of articles about the catastrophe in Western Siberia.

**Russian:**

Но я не прощаюсь, поскольку есть ещё незаконченные статьи, плюс несколько новых проектов, включая обещанную [статью о том, почему Православие не является христианством и чем оно является на самом деле](http://mylnikovdm.livejournal.com/7256.html), включая подборку соответствующих фактов.

**English:**

But I do not say goodbye, because there are still unfinished articles, plus several new projects, including the promised [article about why Orthodoxy is not Christianity and what it really is](http://mylnikovdm.livejournal.com/7256.html), including a compilation of relevant facts.

Translator's note: Each web page's comments may be viewed at the bottom of each original web page]

© All rights reserved. The original author has asserted their ownership and rights.

