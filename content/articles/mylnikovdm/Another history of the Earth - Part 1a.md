title: Another history of the Earth - Part 1a
date: 2017-03-12 00:00
modified: 2022-12-14 16:03:47
category:
tags: catastrophe; Tartaria
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/219459.html
originally: Another history of the Earth. Part 1a. - Dmitry Mylnikov - LiveJournal.html
local: Another history of the Earth Part 1a_files
summary: Almost all agree that a global catastrophe occurred on Earth in the recent past, which official history is trying to hide for one reason or another. In the mythology of different peoples, this catastrophe is usually reflected as a "global flood", which is either not recognized by official history at all, or belongs to a very distant past, the so-called "extrahistorical time".
status: hidden

A more complete and up-to-date version of this series is available in English at: [https://178.62.117238/mylnikovdms-another-history-of-the-earth-articles-google-translated.html](./mylnikovdms-another-history-of-the-earth-articles-google-translated.html)


#### Translated from:

[https://mylnikovdm.livejournal.com/219459.html](https://mylnikovdm.livejournal.com/219459.html)

A small preface.

Two and a half years have passed since the moment I published the first part of my work "[How did Tartaria die?](http://mylnikovdm.livejournal.com/603.html)"...

After the publication, I received many comments and letters from readers, including some that contained a wide variety of facts that do not fit into the official version of history that is taught to us today in schools and universities. Some of the facts that readers wrote to me, in one way or another, confirmed or supplemented my observations and conclusions. I later mentioned many of them in the following parts or in my other works. But there were also facts that suggested that our history was distorted much more than it seemed to me at the time of writing the work "[How did Tartary die?](http://mylnikovdm.livejournal.com/603.html)" ([RoHT English translation](https://178.62.117.238/how-tartaria-died-part-1-without-a-trace.html)).  At one point, I even faced another worldview crisis, because the observed facts did not fit into the model of the world that had formed at that earlier time in my life.

On one hand, it became quite obvious to me that many of the events that the official history talks about either did not take place at all, or they happened quite differently, at a different time and for different reasons. On the other hand, a fundamental question arose: but what, then, had actually happened? What is the other history of the Earth that would correspond to both the observed facts and at least some part of the information that has come down to us? After all, it cannot be that some events happened, but no information about them remained at all. A different, entirely false story could not have been written from scratch, especially when it accounts for global events that affected all the Earth's inhabitants. It must rely on at least some of the facts to look believable, introducing distortions gradually. And if you consider that there are no guarantees, that it will be possible to destroy all written and oral evidence about real history and events that actually happened, they should be declared myths, an invention of a superstitious primitive population, and not try to destroy all of it. In this case, if some ancient text is discovered somewhere, then everyone will immediately understand that this is just another version of the alleged myth, and not a description of real events.

Ultimately, at some point, I realized that I didn't have enough information to build even some new integral foundation, which could be further supplemented and developed, putting into it both existing and newly discovered facts, and information, simultaneously separating the "wheat from the chaff". In this case, the resulting new model of the past must be, on the one hand, logical and internally consistent, and on the other hand, it must link all the observed facts into a single whole. And this is one of the fundamental points, since of the many facts that I will cite below, modern official science explains each one separately. And if each of these pieces is considered separately, then the official explanation looks quite logical and correct.

The published work is the result of collecting and analyzing information that I have carried out over the past year and a half, in order to build at least some general picture of the past for myself personally. At the same time, there are still many blank spots in this picture. That said, it seems to me the general basis is already beginning to emerge, so I decided to share it with you. It goes without saying that I do not pretend to be "the ultimate truth" and am ready to listen and discuss all constructive comments, objections and suggestions.

##### Part 1 The Flood.

Today in so-called "alternative history", there are many different directions and trends, but almost all agree that a global catastrophe occurred on Earth in the recent past, which official history is trying to hide for one reason or another. In the mythology of different peoples, this catastrophe is usually reflected as a "global flood", which is either not recognized by official history at all, or belongs to a very distant past, the so-called "extrahistorical time", or is reduced to some strong, but local event, which local residents have exaggerated to the scale of "worldwide". There is a lot of research on the topic of myths that mention the Flood. It can be easily found on the Internet so I see no reason to dwell on this issue.

If the catastrophe was really global, and even happened in the recent past, then this catastrophe should have left easy-to-read traces on the planet's surface. At the same time, thanks to modern technologies, such as the Internet and various network cartographic services, which allow access not only to conventional topographic maps, but also to satellite images, as well as a model of the Earth's surface relief, finding such traces becomes much easier and faster.

Some of these traces were pointed out to me by the readers of my first works, for which I want to express my special thanks to all of them. I later discovered the other part on my own, including in the works of other researchers into the real history of the Earth. At the same time, a lot of such traces were discovered, from which I came to the conclusion that most likely there were several disasters of different intensities. I will begin my story with, in my opinion, the largest catastrophe, which changed the face of the Earth in a very significant way. She is the first on my list of disasters. This does not mean that no such events had taken place before it, but at the moment I proceed from the fact that the consequences of this catastrophe were so significant that the traces of previous smaller disasters, even if they did occur, have been erased or distorted to the extent that it is rather difficult to recognize them without special training and additional research. But the traces of this catastrophe and those that occurred after it can easily be seen on space images and maps.

Judging by the traces observed, some time ago our planet collided with a large space object, the size of which at the impact site was about 500 km in diameter, and at its exit site was about 350 km. When this happened exactly is a separate issue that we will discuss later. Now our task is to establish the very fact of such a collision.

To do this, we need to find the actual place of the collision, and also assess whether the traces observed on the Earth's surface correspond to the traces that should have formed from such a collision.

I found the first trail at about the same time that I wrote the work "[How did Tartaria die?](http://mylnikovdm.livejournal.com/603.html)", and I decided that this was the impact site of some large object. At that time, I wrote a small note entitled "[How the Sahara Desert came to be. Working hypothesis](http://mylnikovdm.livejournal.com/2051.html)" ([Google English translation](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://mylnikovdm.livejournal.com/2051.html)).

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/26098_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/26098/26098_1000.jpg#resized)

But, having correctly guessed the relationship between deserts and the "spot", I made a mistake in determining the cause and the general course of the process, which occurred during the event. I just didn't have enough information at that time to draw the right conclusions and build a complete model of the events that took place. In fact, this was not the impact site of the object, but the exit point of this object as it continued outward from the Earth. There is another grand formation that serves as an entry point, but few people know about it, since this formation is located at the bottom of the Pacific Ocean. This formation is called the "Tamu Massif".

The Tamu massif is an extinct shield volcano in the northwestern Pacific Ocean, 1,600 km east of Japan. On the diagram, I highlighted it with a red spot. Previously, it was believed that its area is about 260 thousand square miles. [Though recent studies have shown that the real size of Tamu is 533 thousand square kilometers](http://vulkania.ru/novosti/massiv-tamu-namnogo-bolshe-chem-schitalos-ranee.html). That is, it is the largest volcano known to mankind today, not only on Earth, but also in the solar system (previously, Mount Olympus on Mars was considered the largest volcano in the solar system). Tamu is 100 times larger than the largest terrestrial volcano - Mauna Loa - which is situated nearby on the island of Hawaii. For a long time it was believed that this formation was several separate volcanoes, but in 2013 it was established that this volcanic field is not a volcanic complex, but one solid volcano.

This is how the Tamu massif looks on a satellite image.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/159906_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/159906/159906_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/159906/159906_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/159906/159906_original.jpg)

And this digital model of the seabed relief in the Tamu massif area shows how it looks. It shows only the southeastern fragment of the massif, which is in the lower left part of the image.

<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/159693_600.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/159693/159693_600.jpg#resized)

The linear dimensions of the Tamu massif are more than 1000 km in length and more than 500 km in width. The massif is about 4 km high, while it is 2 km below the ocean surface.

Many may wonder why we do not see a giant crater at the impact site, and instead, a huge flat mountain. But this volcanic mountain was formed after the impact of a rapidly flying huge object. It broke through the earth's crust at this site and pressed fragments of crust inward. Molten magma began to emerge into the huge resulting hole, forming the giant supervolcano.

Based on the size of the Tamu massif, we can assume that at the entrance, the object was about 500 km in diameter. At the same time, this object was moving at a very high speed counter to the rotation of the Earth, so it simply pierced and emerged in the location where now there is a formation measuring about 370 km by 950 km in the Himalayan mountain range, in Xinqiang Uygur Autonomous Region of China. Based on the size of the formation, we can assume that at the exit site, the object was about 350 km in diameter, having lost part of the mass when passing through the inner layers of the Earth.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/160117_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160117/160117_original.png)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/160117/160117_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160117/160117_original.png)

And this is how this place looks on a topographic map, on which it is very clearly visible how this spot differs from the entire surrounding landscape, since it has an almost flat surface without any of the peaks, ridges and mountain gorges that are characteristic of the area that surrounds it.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/160445_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160445/160445_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/160445/160445_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160445/160445_original.jpg)

Unfortunately, it is not very clear on ordinary topographic maps that these two locations are connected with each other, which is why, for quite a long time, I considered these two tracks to be different events. This is due to those geometric distortions that occur when the spherical surface of the Earth is projected on to a plane. But now we have such a wonderful tool as the Google Earth program, which allows us to view the Earth's surface almost without distortion (for private use, the program is free, you can freely [download and install it on your computer](https://www.google.com/earth/)).

So, when, in the process of processing the collected facts, I began to consider the image of the Earth in the Google Earth program, it became obvious that the two objects mentioned above are related, since the Tamu massif in the Pacific Ocean is located almost exactly on the axis of the ellipse of the spot in the Himalayan mountain massif. This is how it looks on a diagram which I made from a copy of the Google Earth screen, turning the Earth so as to look at the trajectory of the object's flight from above, so that the curvature distortion is minimal.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/160620_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160620/160620_original.png)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/160620/160620_600.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160620/160620_original.png)

*Text reads (left to right): "Entry point" and "Entry Point" but probably should read: "Exit point" and "Entry Point"*

And this is how it looks from a side view, where you can understand how deeply the object plunged into the molten core of the Earth as it passed through the planet's body.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201a.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/160875_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160875/160875_original.png)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/160875/160875_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/160875/160875_original.png)

*Text reads (left to right): "Exit point" and "Entry Point"*

Now let's build a general model of the processes that took place during the disaster and see how this is confirmed by the facts that we actually observe.

[Continuation (Original Russian)](http://mylnikovdm.livejournal.com/219687.html)

[Next Part (English)](./another-history-of-the-earth-part-1b.html)

© All rights reserved. The original author retains ownership and rights.

