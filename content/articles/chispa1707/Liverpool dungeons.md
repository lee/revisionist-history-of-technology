title: Liverpool dungeons
date: 2021-01-09
modified: Tue 09 Feb 2021 20:24:52 GMT
category:
tags: Liverpool, catastrophe
slug:
authors: Andrei Stepanenko
from: Andrei Stepanenk://chispa1707.livejournal.com/3453217.html
originally: Liverpool dungeons.html
local: Liverpool dungeons_files/
summary: It's logical to assume that this backfill was semi-liquid when it flooded the premises.  Plus, it's not limited to this tunnel: all of Liverpool stands on it. And Prague. And Moscow. And Omsk. And Kazan. And after 1824, when Liverpool Royal Infirmary was opened (the dishes are found here), there was one similar event: the silting up of part of San Francisco Bay around 1851.
status: published

#### Translated from:

[https://chispa1707.livejournal.com/3453217.html](https://chispa1707.livejournal.com/3453217.html)

**Russian:**

Под Ливерпулем расположены "тоннели" [https://earth-chronicles.ru/news/2015-09-27-84510](https://earth-chronicles.ru/news/2015-09-27-84510)

**English:**

There are "tunnels" beneath Liverpool: [https://earth-chronicles.ru/news/2015-09-27-84510](https://earth-chronicles.ru/news/2015-09-27-84510)

**Russian:**

Создателем тоннелей называют Джозефа Уилсона, начавшего проект после Наполеоновских войн. Среди находок - посуда из Ливерпульского Королевского лазарета, открытого в 1824 году. То есть, засыпаны эти тоннели были позже 1824 года. [https://en.m.wikipedia.org/wiki/Liverpool_Royal_Infirmary](https://en.m.wikipedia.org/wiki/Liverpool_Royal_Infirmary) Характерная фотография под катом.

**English:**

Joseph Wilson (translator's note: 'Wilson' should probably be 'Williamson'), who started the project after the Napoleonic Wars, is described as the creator of the tunnels. Among the finds are pieces of crockery from Liverpool Royal Infirmary, which only opened in 1824. In other words, these tunnels were backfilled after 1824. [https://en.m.wikipedia.org/wiki/Liverpool_Royal_Infirmary](https://en.m.wikipedia.org/wiki/Liverpool_Royal_Infirmary)


The characteristic photograph is under the curtain.

<!-- Local
![](Liverpool dungeons_files/1808771_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/chispa1707/24769663/1808771/1808771_original.jpg#resized)

**Russian:**

Логично предполагают, что этот грунт был полужидким, когда заливал помещения.  Плюс, дело не ограничивается этим тоннелем, весь Ливерпуль на этом стоит. И Прага. И Москва. И Омск. И Казань.  И вот после 1824 года, когда был открыт Ливерпульский Королевский лазарет (посуду которого здесь находят), было одно сходное событие: заиливание части бухты Сан-Франциско около 1851 года. Моя гипотеза: с верховьев разом смысло огромное количество красноватой пыли, регулярно выпадавшей чуть ранее - в 1842-1850 годах. Этой гелеобразной субстанцией города и затопило. Ливерпуль стоит в пойме реки Мерси, вся долина этой реки - сплошные низины с буквально сотнями и сотнями каналов, и берет эта река начало на Пеннинских горах - крупнейшем водоразделе Англии. Это хорошее место, чтобы там задерживалась пыль во время нескольких прошедших сирокко. Думаю, Ливперпуль замыло глиной синхронно с Сан-Франциско - около 1851 года.

**English:**

It's logical to assume that this backfill was semi-liquid when it flooded the premises.  Plus, mudflood backfill is not limited to this tunnel: all of Liverpool stands on it. And Prague. And Moscow. And Omsk. And Kazan.  And after 1824, when Liverpool Royal Infirmary was opened (whose dishes were found here), there was a similar event: the silting up of part of San Francisco Bay in around 1851. 

My hypothesis: a huge amount of reddish dust, which had regularly fallen a little earlier - in 1842-1850 - was washed down from the various watersheds in one go. Cities were flooded by this gel-like substance. Liverpool stands on the flood plain of the River Mersey. The whole Mersey river valley is solid lowlands with literally hundreds and hundreds of channels. This river originates in the Pennines - the largest watershed in England. It's a good place for the dust to have gathered during the past few [siroccos](https://www.lexico.com/definition/sirocco). I think Liverpool was 'clayed' (mudflooded) in sync with San Francisco - around 1851.

**Russian:**

ВАЖНЫЙ МОМЕНТ Такое событие сложно назвать Катастрофой с большой буквы "К". Это - достаточно отдаленное последствие Катастрофы. Неприятность в том, что это однократное событие носит довольно масштабный характер. Лично для меня особенно важно то, что становится объяснимым "затопление в грунте" достаточно молодых зданий - постройки 1840-1850-х годов. Нам следует отделять этот инцидент от более ранних, случившихся в действительно катастрофических обстоятельствах.

**English:**

This event can hardly be called a Catastrophe with a capital "C". It is a rather distant consequence of the Catastrophe. The unpleasant thing is that this one-time event was on a rather large scale. For me personally, it is especially important that the "sinking into the ground" of fairly young buildings - those built in the 1840s and 1850s - becomes explainable. We should separate this 1851 incident from earlier catastrophic incidents that happened in truly catastrophic circumstances.

**Russian:**

О СНИЖЕНИИ ПОЛНОВОДНОСТИ РЕК Кое-где осевшая глина создала вдоль всего течения русел многометровую "пробку", не дающую грунтовым водам пополнять реки. Эта вода есть, более того, она все так же потихоньку стекает в моря, но - под новым глиняным дном, слишком тяжелым, чтобы прорваться сквозь него вверх. Результат: приток Москва-реки, прежде имевший русло шириной в 200 метров, теперь буквально метр шириной, - а нет полноценного пополнения из водоносных слоев.

**English:**

In some places, the settled clay has created a many meters-deep "plug" along the course of the riverbeds, preventing groundwater from discharging into rivers. This water is there, moreover, it is still slowly flowing into the sea, but now flows under the new clay bed, which is too heavy to allow the water to break through it upwards. The result: the Moskva River inflow, which previously had a channel 200 meters wide, is now literally one meter wide - and there is no full replenishment from the aquifers.

**Russian:**

ЕЩЕ ОДИН ИСТОЧНИК НАНОСОВ Не исключено, что пыль оседала на высотах в два и более приема: при первом инциденте, убившем мамонтов, несколько раз потом и в финале - в 1840-1850-х. А вот сошла кое-где разом, - отсюда и такие объемы. И, возможно, сходила в разных местах в разные эпохи. В уязвимых местах сходила раньше. Поэтому может не быть единой даты заиливания бухт и городов.

**English:**

It is also possible that the dust settled on the heights in two or more events. The first incident, which killed mammoths, was followed by some period of time until the last incident in the 1840/1850s. And in this last incident the dust descended in one go - hence the large volumes. And it probably descended in different places at different times. In vulnerable places it was earlier. That's why there can be no single date for silting of bays and cities.

Translator's note: Stepanenko tags his original-language articles on this subject at [https://chispa1707.livejournal.com/tag/8200](https://chispa1707.livejournal.com/tag/8200). If translated into English, they will be tagged here as '#catastrophe' at: [https://178.62.117.238/tag/catastrophe.html](https://178.62.117.238/tag/catastrophe.html).

<!--
Sputniknews reports Liverpool sinkhole opens above Liverpool crypt/necropolis containing 80,000 bodies:
https://sputniknews.com/uk/202102091082025529-sinkhole-appears-in-former-cemetery-in-liverpool/
-->

© All rights reserved. The original author retains ownership and rights.

