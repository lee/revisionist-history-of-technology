title: How Tartaria Died - Part 3 - Relic Forests
date: 2014-09-28
modified: Thu 14 Jan 2021 07:22:37 GMT
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: One of the arguments against the fact that a large-scale catastrophe could have happened 200 years ago is the myth about "relic" forests that supposedly grow in the Urals and Western Siberia.
status: published
originally: Как погибла Тартария, ч3.zip
local: How Tartaria was Lost Part 3_files/

### Translated from: 

Как погибла Тартария, ч3.zip, [http://mylnikovdm.livejournal.com/1646.html](http://mylnikovdm.livejournal.com/1646.html) and [http://mylnikovdm.livejournal.com/1990.html](http://mylnikovdm.livejournal.com/1990.html)

**Russian:**

Одним из аргументов против того, что масштабная катастрофа моглапроизойти 200 лет назад является миф про "реликтовые" леса, которые якобы растут на Урале и в Западной Сибири.

**English:**

One of the arguments against the fact that a large-scale catastrophe could have happened 200 years ago is the myth about "relic" forests that supposedly grow in the Urals and Western Siberia.

**Russian:**

Впервые на мысль о том, что с нашими "реликтовыми" лесами что-то не так,я натолкнулся ещё лет десять назад, когда случайно обнаружил, что в "реликтовом" городском бору, во-первых, на прочь отсутствуют старыедеревья старше 150 лет, а во вторых, там очень тонкий плодородный слой, порядка 20-30 см. Это было странно, поскольку читая различные статьи поэкологии и лесоводству мне неоднократно попадалась информация о том, что за тысячу лет в лесу образуется плодородный слой порядка одного метра,то есть, по миллиметру в год. Чуть позже оказалось, что подобная картина наблюдается не только в центральном городском бору, но также и востальных сосновых борах, расположенных в Челябинске и окрестностях. Старые деревья отсутствуют, плодородный слой тонкий.

**English:**

I first came across something being wrong with our "relic" forests ten years ago, when I accidentally discovered that in the "relic" urban forest:

- firstly, there are no old trees older than 150 years, and 

- secondly, there is a very thin fertile layer, about 20-30 cm. It was strange, because reading various articles on poetics and forestry, I have repeatedly received information that a fertile layer of about one meter, that is, one millimeter per year, is formed in the forest over a thousand years. A little later it turned out that such a picture is observed not only in the central city pine forest, but also in the eastern pine forests located in Chelyabinsk and its surroundings. There are no old trees, and the fertile layer is thin.

**Russian:**

Когда я стал расспрашивать на эту тему местных специалистов, то мнестали объяснять что-то про то, что до революции боры вырубались и засаживались заново, а скорость накопления плодородного слоя в сосновыхлесах надо считать по другому, что я в этом ничего не понимаю и лучше туда не лезть. В тот момент меня это объяснение, в общем-то, устроило.

**English:**

After I started to ask local specialists about this topic, I eventually became tired of explanations along the lines of: before the revolution, burs were cut down and planted again, and the rate of accumulation of fertile layer in pine forests should be considered different, that I do not understand anything about it and it is better not to go there. At that time, I was actually satisfied with this explanation.

**Russian:**

Кроме того, выяснилось, что следует различать понятие "реликтовый лес",когда речь идёт о лесах, которые очень давно произрастают на данной территории, и понятие "реликтовые растения", то есть такие, которые сдревних времён сохранились только в данном месте. Последний термин вовсе не означает, что сами растения и леса, в которых они растут, старые,соответственно наличие большого количества реликтовых растений в лесах Урала и Сибири не доказывает того, что и сами леса растут на данномместе неизменно уже тысячи лет.

**English:**

Besides, it was found out that one should distinguish between the concept of "relic forest", when we talk about forests that have been growing in this area for a long time, and the concept of "relic plants", i.e. those that have survived only in this place since ancient times. The latter term does not mean at all that the plants and forests in which they grow are old, so the presence of a large number of relic plants in the forests of the Urals and Siberia does not prove that the forests themselves have been growing in this place for thousands of years.

**Russian:**

Когда я стал разбираться с "Ленточными борами" и собирать про них информацию, то наткнулся на следующее сообщение на одном из региональныхАлтайских форумов:

**English:**

When I started to deal with ribbon forests and collect information about them, I came across the following message in one of the regional Altai forums:

**Russian:**

> "Не даёт мне покоя один вопрос... Почему наш ленточный бор зовут реликтовым? Что в нём реликтового? Пишут, мол, своим возникновениемобязан леднику. Ледник сошёл не одну тысячу лет назад (если верить мучёным). Сосна живёт лет 400 и вырастает метров до 40 вверх. Еслиледник сошёл так давно, то где был ленточный бор всё это время? Почему в нём практически нет старых деревьев? И где умершие деревья? Почему слойземли там считанные сантиметры и сразу песок? Даже за триста лет шишки/иголки должны были дать больший слой... В общем, такое ощущение,что ленточный бор чуть старше Барнаула (если не моложе) и ледник, благодаря которому он возник, сошёл не 10000 лет назад, а куда ближе кнам по времени... Может я чего не понимаю?... "

[http://forums.drom.ru/altai/t1151485069.html](http://forums.drom.ru/altai/t1151485069.html)

Сообщение это датировано 15 ноября 2010, то есть тогда ещё не было ни роликов Алексея Кунгурова, ни каких-либо других материалов на эту тему.Получается, что независимо от меня у другого человека возникли ровно те же вопросы, что когда-то появились у меня.

**English:**

> "There's one question that's been bothering me... Why is our ribbon pine wood called relic? What's relic about it? They say it's born of a glacier. The glacier came down more than one thousand years ago. The pine has been living for 400 years and grows up to 40 meters high. If the glacier has come down so long ago, where has the ribbon pine forest been all this time? Why are there practically no old trees in it? And where are the dead trees? Why do they grow in centimeters of sand? Even after three hundred years, the debris and needles should have created a bigger layer... In general, it feels like the ribbon bur is little older than Barnaul (if not younger) and the glacier that created it did not descend 10,000 years ago, but much closer to the present... Maybe there's something I don't understand... "

[http://forums.drom.ru/altai/t1151485069.html](http://forums.drom.ru/altai/t1151485069.html)

The message is dated November 15, 2010. At that time there were no Alexei Kungurov's videos or any other materials on this subject. It turns out that independently of me, another person had exactly the same questions that I once had.

**Russian:**

При дальнейшем изучении этой темы выяснилось, что подобная картина, тоесть, отсутствие старых деревьев и очень тонкий плодородный слой, наблюдается практически во всех лесах Урала и Сибири. Однажды я случайноразговорился на эту тему с представителем одной из фирм, которые занимались обработкой данных для нашего лесного ведомства по всейтерритории страны. Он стал со мной спорить и доказывать, что я ошибаюсь, что этого не может быть, и тут же при мне позвонил человеку, который уних отвечал за статистическую обработку. И человек это подтвердил, что максимальный возраст деревьев, которые у них проходили по учёту в даннойработе, составлял 150 лет. Правда, выданная ими версия гласила, что на Урале и в Сибири хвойные деревья в основном не живут больше 150 лет,поэтому их и не учитывают.

**English:**

During further study of this topic it became clear that such a picture, i.e. absence of old trees and a very thin fertile layer, is observed in practically all forests of the Urals and Siberia. One day I accidentally talked about this topic with a representative of one of the companies, which was processing data for our forest authority throughout the country. He started arguing with me and claiming that I was wrong, that it could not be, and immediately I got a call from the person who was in charge of statistical processing. And the man confirmed that the maximum age of the trees that they had taken into account in this work was 150 years. It is true that their version said that coniferous trees in the Urals and Siberia mostly do not live more than 150 years, so they are not counted.

**Russian:**

Открываем справочник по возрасту деревьев [http://www.sci.aha.ru/ALL/e13.htm](http://www.sci.aha.ru/ALL/e13.htm) и видим, что сосна обыкновеннаяживёт 300-400 лет, в особо благоприятных условиях до 600 лет, сосна кедровая сибирская 400-500 лет, ель европейская 300-400 (500) лет, ельколючая 400-600 лет, а лиственница сибирская 500 лет в обычных условиях, и до 900 лет в особо благоприятных!

**English:**

We open the tree age guide: [http://www.sci.aha.ru/ALL/e13.htm](http://www.sci.aha.ru/ALL/e13.htm) and we see that an ordinary pine will live 300-400 years, an in especially favorable conditions up to 600 years. Siberian cedar pine 400-500 years, European spruce 300-400 (500) years, spruce 400-600 years, and Siberian larch 500 years in typical conditions, and up to 900 years in especially favorable conditions!

**Russian:**

Получается, что везде эти деревья живут не менее 300 лет, а в Сибири ина Урале не более 150?

**English:**

It turns out that these trees can live to least 300 years old everywhere, but no more than 150 years in Siberia and the Urals?

**Russian:**

Как на самом деле должны выглядеть реликтовые леса можно посмотреть вот

[http://www.kulturologia.ru/blogs/191012/17266/](http://www.kulturologia.ru/blogs/191012/17266/)

**English:**

You can see what relic forests should really look like here: [http://www.kulturologia.ru/blogs/191012/17266/](http://www.kulturologia.ru/blogs/191012/17266/)

**Russian:**

Это фотографии свырубки древних секвой в Канаде в конце 19-го начале 20-го веков, толщина стволов которых достигает до 6 метров, а возраст до 1500 лет. Нутак то Канада, а у нас, дескать, секвои не растут. Почему не растут, если климат практически одинаковый, никто из "специалистов" объяснитьтолком не смог.

**English:**

These are photographs of ancient sequoia felling in Canada in the late 19th and early 20th centuries, with trunks up to 6 meters thick and up to 1,500 years old. Well, that's Canada, but we don't say sequoias don't grow here. Why do not they grow here, if the climate is almost the same? None of the "experts" could answer.

<!-- Local
![](How Tartaria was Lost Part 3_files/lumberjacks-redwood-1.jpg#resized)
-->

![](https://static.kulturologia.ru/files/u12645/lumberjacks-redwood-1.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 3_files/lumberjacks-redwood-6.jpg#resized)
-->

![](https://static.kulturologia.ru/files/u12645/lumberjacks-redwood-6.jpg#resized)

**Russian:**

Сейчас да, сейчас не растут. Но оказывается, что подобные деревья росли и у нас. Ребята из нашего Челябинского государственного университета,которые участвовали в раскопках в районе Аркаима и "страны городов" на юге Челябинской области, рассказывали, что там, где сейчас степь, вовремена Аркаима были хвойные леса, причём местами там встречались гигантские деревья, диаметр стволов у которых был до 4 - 6 метров! Тоесть, они были соизмеримы с теми, что мы видим на фото из Канады. Версия о том, куда делись эти леса, гласит, что леса были варварски вырубленыжителями Аркаима и других созданных ими поселений, и даже делается предположение, что именно истощение лесов и послужило причиной миграцииаркаимцев. Типа, тут лес весь вырубили, пойдём вырубать в другом месте. О том, что леса можно сажать и выращивать заново, как делали повсеместноначиная как минимум с 18 века, аркаимцы, видимо, ещё не знали. Почему за 5500 лет (таким возрастом сейчас датируют Аркаим) лес в этом месте невосстановился сам, вразумительного ответа нет. Не вырос, ну и не вырос. Так сложилось.

**English:**

They're not growing now. But it turns out that similar trees have been growing in our country. Guys from our Chelyabinsk State University, who took part in excavations in the area of Arkaim and "country of cities" in the south of the Chelyabinsk region, said that where the steppe is now, in Arkaim there were coniferous forests, and in some places there were giant trees with trunk diameters up to 4 - 6 meters! That is, they were commensurate with what we see in the above photo from Canada. The official version about where these forests went says that the forests were barbarously cut down by the Arkaim and other settlements they had created, and they even make the assumption that it was the depletion of the forests that caused the migration of the Arkaimans. It's like: "the whole forest is cut down here, let's go cut it down somewhere else." The Arkaimans probably did not know that forests could be planted and re-grown as they have done everywhere since at least the 18th century. Why is it that for 5,500 years (now Arkaim is dated at such an age) the forest here has not restored itself? There is no reasonable answer. It hasn't grown, is the official answer, so there you have it. 

**Russian:**

Вот серия фотографий, которые я сделал в краеведческом музее в Ярославлеэтим летом, когда был с семьёй в отпуске.

**English:**

Here is a series of photos that I took at the local history museum in Yaroslavl in the summer when I was on vacation with my family.

<!-- Local
[![](How Tartaria was Lost Part 3_files/21549_900.jpg#clickable)](How Tartaria was Lost Part 3_files/21549_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/21549/21549_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/21549/21549_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/21889_900.jpg#clickable)](How Tartaria was Lost Part 3_files/21889_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/21889/21889_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/21889/21889_original.jpg)

**Russian:**

На первых двух фото спил сосны в возрасте 250 лет. Диаметр ствола более метра. Прямо над ней две пирамидки, которыесоставлены из спилов стволов сосен в возрасте 100 лет, правая росла на свободе, левая в смешанном лесу. В лесах, в которых мне приходилосьбывать в основном наблюдаются как раз подобные 100 летние деревья или чуть потолще.

**English:**

In the first two photos the treecutter sawed pines aged at 250 years. The diameter of the trunk is over a meter. Right above it are two pyramids made up of pine tree trunks sawn when they were 100 years old. The right one grew free-standing, the left one in mixed woods. In the woods where I have mostly observed the trees are just like 100-year-old trees or slightly thicker.

<!-- Local
[![](How Tartaria was Lost Part 3_files/22194_900.jpg#clickable)](How Tartaria was Lost Part 3_files/22194_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/22194/22194_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/22194/22194_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/22408_900.jpg#clickable)](How Tartaria was Lost Part 3_files/22408_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/22408/22408_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/22408/22408_original.jpg)


**Russian:**

На этих фото они даны крупнее. При этом разница междусосной, которая росла на свободе и в обычном лесу не сильно значительная, а разница между сосной 250 лет и 100 лет как раз где-то2.5-3 раза. Это означает, что диаметр ствола сосны в возрасте 500 лет будет порядка 3-х метров, а в возрасте 600 лет порядка 4 метров. Тоесть, найденные при раскопках гигантские пни могли остаться даже от обычной сосны возраста порядка 600 лет.

**English:**

In these pictures, they're bigger. At the same time, the difference between the pine trees, which grew free-standing and in the ordinary forest, is not very significant, and the difference between the pine trees of 250 years and 100 years just about 2.5-3 times. This means that the trunk diameter of a pine tree at the age of 500 years should be about 3 meters, and at the age of 600 years it will be about 4 meters. That is, the giant stumps found during the excavations could have been left behind even by an average 600 year old pine tree.

<!-- Local
[![](How Tartaria was Lost Part 3_files/22574_900.jpg#clickable)](How Tartaria was Lost Part 3_files/22574_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/22574/22574_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/22574/22574_original.jpg)

**Russian:**

На последнем фото спилы сосен, которые росли в глухом еловом лесу и на болоте. Но особенно меня поразил в этой витрине спилсосны в возрасте 19 лет, который справа вверху. Видимо это дерево росло на свободе, но всё равно толщина ствола просто гигантская! Сейчасдеревья с такой скоростью, хоть на свободе, хоть при искусственном выращивании с уходом и подкормкой, не растут, что ещё раз говорит о том,что на нашей Планете с климатом происходят очень странные вещи.

**English:**

The last photo shows cut pines that grew in a dense spruce forest and marsh. But I was particularly impressed by this showcase of spilose trees at the age of 19, which is on the top right. Apparently this tree grew wild, but still the thickness of the trunk is just gigantic! The trees do not grow at such a rate now, wild or in artificial cultivation with care and feeding, which once again indicates that on our planet very strange things are happening with the climate.

**Russian:**

Из приведённых фотографий следует, что как минимум сосны в возрасте 250 лет, а с учётом изготовления спила в 50-х годах 20-го века, родившиесяза 300 лет от сегодняшнего дня, в европейской части России имеют место быть, либо, как минимум, встречались там 50 лет назад. За свою жизнь япрошёл по лесам не одну сотню километров, как на Урале, так и в Сибири. Но точно таких больших сосен как на первом снимке, с толщиной стволаболее метра, не видел нигде! Ни в лесах, ни на открытых пространствах, ни в обжитых местах, ни в труднодоступных районах. Естественно, что моиличные наблюдения ещё не показатель, но это подтверждается и наблюдением множества других людей. Если кто-то из читающих сможет привести примерыдеревьев долгожителей на Урале или в Сибири, то милости прошу представить фотографии с указанием места и времени, когда они былисделаны.

**English:**

From the above photos it follows that at least pine trees are 250 years old, and taking into account the manufacture of a saw in the 50s of the 20th century, born 300 years from today, in the European part of Russia take place, or at least met there 50 years ago. During my life I have walked through forests more than one hundred kilometres, both in the Urals and Siberia. But I have never seen such big pines as on the first image, with a trunk more than a meter thick, anywhere! Neither in forests, nor in open spaces, nor in dwelling places, nor in hard-to-reach areas. Naturally, my observations are not yet an indicator, but this is also confirmed by the observations of many other people. If one of the readers can give examples of long-lived trees in the Urals or Siberia, I ask you to submit photographs with the place and time when they were taken.

**Russian:**

Если же посмотреть на имеющиеся фотографии конца 19-го начала 20-го веков, то в Сибири мы увидим очень молодые леса. Вот известные многимфотографии с места падения тунгусского метеорита, которые неоднократно публиковались в разных изданиях и статьях в интернете.

**English:**

If you look at the available photographs of the end of the 19th and beginning of the 20th centuries, you will see very young forests in Siberia. Here are the photos known to many people from the site of the fall of the Tunguska meteorite, which have been repeatedly published in various publications and articles on the Internet.

<!-- Local
![](How Tartaria was Lost Part 3_files/22922_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/22922/22922_900.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 3_files/23256_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/23256/23256_900.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 3_files/23401_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/23401/23401_900.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 3_files/23738_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/23738/23738_900.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 3_files/23965_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/23965/23965_900.jpg#resized)

**Russian:**

На всех фотографиях очень хорошо видно, что лес достаточно молодой, небольше 100 лет. Напомню, что Тунгусский метеорит упал 30 июня 1908 года. То есть, если предыдущая масштабная катастрофа, которая уничтожила лесав Сибири, произошла в 1815 году, то к 1908 году лес должен выглядеть именно так, как на фотографиях. Напомню скептикам, что данная территорияи сейчас практически не заселена, и в начале 20 века людей там практически не было. Это значит, что вырубать лес для хозяйственных илиещё каких нужд было просто некому.

**English:**

All photos show very well that the forest is quite young, less than 100 years old. Let me remind you that the Tunguska meteorite fell on June 30, 1908. That is, if the previous large-scale disaster, which destroyed the forest of Siberia, occurred in 1815, then by 1908 the forest should look exactly as in the photographs. Let me remind the skeptics that this area is now virtually uninhabited, and in the early 20th century there were virtually no people there. This means that there was simply no one to cut down the forest for household or other needs.

**Russian:**

Ещё одна интересная ссылка на статью [http://sibved.livejournal.com/73000.html](http://sibved.livejournal.com/73000.html) где автор приводит интересныеисторические фотографии со строительства Транссибирской магистрали в конце 19-го начале 20-го веков. На них мы тоже везде видим толькомолодой лес. Никаких толстых старых деревьев не наблюдается. Ещё большая подборка старых фотографий со строительства Трансиба тут: [http://murzind.livejournal.com/900232.html](http://murzind.livejournal.com/900232.html)

**English:**

Another interesting link to the article [http://sibved.livejournal.com/73000.html](http://sibved.livejournal.com/73000.html) where the author gives interesting historical photos from construction of Trans-Siberian Railway in the end of 19th - beginning of 20th centuries. We see only young forest everywhere on them too. No thick old trees are observed. Even bigger collection of old photos from Transsib construction [here http://murzind.livejournal.com/900232.html](http://murzind.livejournal.com/900232.html)

<!-- Local
[![](How Tartaria was Lost Part 3_files/24260_900.jpg#clickable)](How Tartaria was Lost Part 3_files/24260_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/24260/24260_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/24260/24260_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/24489_900.jpg#clickable)](How Tartaria was Lost Part 3_files/24489_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/24489/24489_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/24489/24489_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/24761_900.jpg#clickable)](How Tartaria was Lost Part 3_files/24761_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/24761/24761_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/24761/24761_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/24944_900.jpg#clickable)](How Tartaria was Lost Part 3_files/24944_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/24944/24944_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/24944/24944_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/25300_900.jpg#clickable)](How Tartaria was Lost Part 3_files/25300_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/25300/25300_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/25300/25300_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 3_files/25388_900.jpg#clickable)](How Tartaria was Lost Part 3_files/25388_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/25388/25388_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/25388/25388_original.jpg)

**Russian:**

Таким образом есть множество фактов и наблюдений, которые говорят о том, что на большой территории Урала и Сибири фактически отсутствуют лесастарше 200 лет. При этом я хочу сразу оговориться, что я не утверждаю, что старых лесов на территории Урала и Сибири нет вообще. Но именно втех местах, где произошла катастрофа, их нет.

**English:**

Thus, there are many facts and observations that suggest that in a large area of the Urals and Siberia there are actually no forests older than 200 years. At the same time I would like to clarify at once that I am not claiming that there are no old forests in the Urals and Siberia at all. But there are no such forests in the places where the disaster occurred.

**Russian:**

Вернёмся к вопросу о толщине почвы, о чём упоминает и автор сообщения о ленточных борах, которое я цитировал выше. Я уже упоминал о том, чтораньше мне встречалась в нескольких источниках цифра, что средняя скорость почвообразования составляет 1 метр за 1000 лет или примерно 1мм в год. Собирая информацию и материалы для данной статьи я решил выяснить, откуда вообще взялась эта цифра и насколько она соответствуетреальности.

**English:**

Let us return to the issue of soil thickness, which is also mentioned by the author of the report on ribbon forests [bores?], which I quoted above. I have already mentioned that earlier I encountered in several sources a number that the average speed of soil formation is 1 meter per 1,000 years or about 1mm per year. While collecting information and materials for this article, I decided to find out where this figure came from and how much it corresponds to reality.

**Russian:**

Почвообразование, как выяснилось, является достаточно сложным динамическим процессом, а сама почва имеет достаточно сложную структуру.Скорость почвообразования зависит от множества факторов, включая климат, рельеф, состав растительности, материал так называемой "материнскойосновы", то есть тот минеральный слой, на котором формируется почва. Таким образом цифра 1 метр за 1000 лет просто взята с потолка.

**English:**

Soil formation, as it turned out, is a rather complicated dynamic process and the soil itself has a rather complicated structure. Speed of soil formation depends on many factors including climate, relief, composition of vegetation, material of so called "mother base" i.e. mineral layer on which the soil is formed. Thus, the figure of 1 meter for 1,000 years is simply plucked out of thin air [orig: "taken from the ceiling"].

**Russian:**

В интернете удалось найти следующую статью на эту тему: [http://ecoportal.su/news.php?id=75701](http://ecoportal.su/news.php?id=75701)

> "**Предела на скорость формирования почв не существует, выяснили ученые**
> 
> Масса почвы в горах Новой Зеландии растет с рекордно высокой скоростью,которая примерно в два раза превышает максимально возможные темпы формирования новых почв, что позволяет говорить об отсутствии подобной "планки ", заявляют геологи в статье, опубликованной в журнале Science.
> 
> "Наши коллеги считали, что почвы могут формироваться с некой предельной скоростью. В нашей работе мы раскрыли отношение между темпамиформирования почвы и скоростью разрушения гор и обнаружили, что их максимальные значения заметно превышают те, которые были зафиксированыдругими геологами ", - заявил Айзек Ларсен из университета Вашингтона в Сиэтле (США).
> 
> Скорость разрушения гор была крайне неоднородной для разных участковЮжных Альп - так, в некоторых регионах горы  "стачивались " на 10 миллиметров в год или даже больше, а в других они почти не менялись.Исследователи обнаружили, что скорость накопления новых почв росла вместе с тем, насколько активно разрушались горы.
> 
> По расчетам геологов, почва в самых разрушенных регионах Южных Альпформировалась со скоростью в 2,5 миллиметра в год, что почти в два раза быстрее, чем считалось максимально возможным ранее. Данный факт, какполагают ученые, свидетельствует о наличии пробелов в нашем понимании того, как формируются почвы."

Исходя из последнего абзаца можно сделать предположение, что пресловутаяцифра 1 мм в год является той самой максимально возможной скоростью формирования почв, как считалась ранее. Но тут следует обратить вниманиена то, что в данной статье речь идёт о горных районах, где, как известно, скалы и весьма скудная растительность. Так что вполне логичнопредположить, что в лесах эта скорость по определению должна быть выше.

**English:**

We have found the following article on this subject on the Internet: [http://ecoportal.su/news.php?id=75701](http://ecoportal.su/news.php?id=75701)

> Scientists discover soil formation speed limits don't exist
> 
> The mass of soil in the mountains of New Zealand is growing at a record high speed, which is about twice the maximum possible rate of formation of new soils, which allows us to speak about the lack of such a "bar", geologists say in an article published in the journal Science.
> 
> "Our colleagues believed that soils could form at some limit speed. In our work, we've uncovered the relationship between the rate of soil formation and the rate of mountain destruction and found that their maximum values are significantly higher than those recorded by other geologists," said Isaac Larsen of the University of Washington at Seattle, USA.
> 
> The rate of destruction of the mountains was extremely heterogeneous for different parts of the South Alps - thus, in some regions the mountains were "stacked" by 10 millimeters per year or even more, while in others they hardly changed. Researchers found that the rate of accumulation of new soils increased along with how actively the mountains were destroyed.
> 
> According to geologists' calculations, the soil in the most destroyed regions of the Southern Alpes was formed at a speed of 2.5 millimeters per year, which is almost twice as fast as it was considered maximum possible earlier. This fact, scientists believe, shows that there are gaps in our understanding of how the soil is formed.

Based on the last paragraph, it can be assumed that the notorious 1 mm per year is the highest possible speed of soil formation, as previously thought. But here we should pay attention to the fact that in this article we are talking about mountainous areas, where, as we know, rocks and very scarce vegetation. So it is quite logical to suppose that in forests this speed should be higher by definition.

**Russian:**

Продолжив изыскания я натолкнулся в одной из брошюр по экологии на таблицу со скоростью почвообразования, из которой следовало, чтонаибольшая скорость почвообразования наблюдается на равнинах с благоприятным климатом и составляет порядка 0.9 мм в год. В районе тайгискорость почвообразования даётся 0.10-0.20 мм в год, то есть порядка 10-20 см за 1000 лет. В тундре менее 0.10 мм в год. Эти цифры вызвалиещё большее подозрение, чем 1 метр за 1000 лет. Ну ладно, скорость почвообразования в тундре с её вечной мерзлотой ещё как-то можно понять,но чтобы в тайге с мощной растительностью была такая медленная скорость почвообразования, ещё меньше чем наблюдается в горах Альп, в это веритсяс большим трудом. Тут явно было что-то не так.

**English:**

Continuing researches, in one of the brochures on ecology I came across a table showing rates of soil formation. It claimed the greatest rate of soil formation is observed on plains with a favorable climate and makes about 0.9 mm per year. In taiga, the rate of soil formation was given at 0.10-0.20 mm per year; that is about 10-20 cm over 1,000 years. In tundra, it is less than 0.10 mm per year. These figures have caused even more suspicion than 1 meter in 1000 years. Well, the speed of soil formation in the tundra with its permafrost can somehow be understood, but that in the taiga with a powerful vegetation was such a slow rate of soil formation, even less than observed in the Alps mountains, it is believed in a great difficulty. There was clearly something wrong with that.

**Russian:**

Позже мне попался в руки учебник по почвоведению под редакцией В.А. Кодвы и Б.Г. Розанова, изд. "Высшая школа", г. Москва, 1988 г.

[http://www.bsu.ru/content/hecadem/kovda/kovda1.pdf](http://www.bsu.ru/content/hecadem/kovda/kovda1.pdf)

[http://www.bsu.ru/content/hecadem/kovda/kovda2.pdf](http://www.bsu.ru/content/hecadem/kovda/kovda2.pdf)

**English:**

Later, a soil science textbook edited by V.A. Kodva and B.G. Rozanov, ed. *Higher School, Moscow, 1988.* came into my hands.

[http://www.bsu.ru/content/hecadem/kovda/kovda1.pdf](http://www.bsu.ru/content/hecadem/kovda/kovda1.pdf)

[http://www.bsu.ru/content/hecadem/kovda/kovda2.pdf](http://www.bsu.ru/content/hecadem/kovda/kovda2.pdf)

**Russian:**

В частности на стр. 312-313 есть такие интересные пояснения:

**English:**

Specifically pages 312-313 have such interesting explanations:

**Russian:**

> "Возраст почвенного покрова равнин северного полушария соответствуетконцу последнего материкового оледенения где-то около 10 тыс. лет назад. В пределах Русской равнины, в её северной части, возраст почвопределяется постепенным отступлением ледниковых покровов на север в конце ледникового периода, а в южной части - постепеннойКаспийско-Черноморской регрессией примерно в тоже время. Соответственно возраст черноземов Русской равнины составляет 8---10 тыс. лет, а возрастподзолов Скандинавии - 5---6 тыс. лет."

**English:**

> "The age of the soil cover of the plains of the northern hemisphere corresponds to the end of the last continental glaciation about 10 thousand years ago. Within the Russian Plain, in its northern part, the age of the soil is determined by the gradual retreat of the glacier cover to the north at the end of the glacial period, and in the southern part - by the gradual Caspian-Black Sea regression around the same time. Accordingly, the age of the chernozems of the Russian Plain is 8-10 thousand years, and the age of the Scandinavian podzols is 5-6 thousand years".

**Russian:**

> "Широко использовался метод определения возраста почвы по соотношению изотопов 14С:12С в почвенном гумусе. Принимая во внимание все оговоркипо поводу того, что возраст гумуса и возраст почвы - это разные понятия, что имеет место постоянное разложение гумуса и егоновообразование, перемещение новообразованного гумуса от поверхности в глубь почвы, что сам радиоуглеродный метод дает большую ошибку и т. д.,определенный этим методом возраст черноземов Русской равнины можно принять равным 7---8 тыс. лет. Г.В. Шарпензеель (1968) определил этимметодом возраст некоторых культурных поч в Центральной Европы порядка 1 тыс. лет, а торфяников - 8 тыс. лет. Возраст дерново-подзолистых почвТомского Приобья был определен порядка 7 тыс. лет."

**English:**

> "The method of determining the age of soil by the ratio of C14:C12 isotopes in soil humus is widely used. Taking into account all the qualifications around the age of humus and the age of the soil, and that there are different ideas about how constant is decomposition of humus and formation of new humus, as well as the movement of newly formed humus from the surface to the depths of the soil. And that the radio-carbon method itself produces large errors, etc., the age of black soil of the Russian Plain defined by this method can be taken as 7-8 thousand years. G.V.Sharpenzeel (1968) determined the age of some cultural soils in Central Europe as 1 thousand years and peatlands as 8 thousand years by this method. Age of sod-podzolic soils in Tomsk region was determined by this method as 7 thousand years".

**Russian:**

То есть, данные по скорости почвообразования в упомянутой выше таблице были получены методом от обратного. Мы имеем некую толщину почвы,например 1.2 метра, а далее, исходя из предположения, что она начала образовываться 8 тыс. лет назад, когда отсюда якобы ушёл ледник,получаем скорость почвообразования порядка 0.15 мм в год.

**English:**

That is, the data on the rate of soil formation in the above table were actually obtained by the reverse method. Ie, "We have a certain thickness of soil, for example 1.2 meters, and then, assuming that it began to form 8 thousand years ago, when a glacier supposedly left here, we get a speed of soil formation of about 0.15 mm per year."

**Russian:**

Про точность и эффективность радиоуглеродного метода, особенно на относительно "коротких" по историческим меркам периодах до 50 тыс. летуже не писал только ленивый. А если учесть, что мы предполагаем возможность применения на данных территориях ядерного оружия в той илииной форме, то там вообще уже говорить не о чем. Очевидно, что данные просто подгонялись под нужную цифру в 7-8 тыс. лет.

**English:**

Except that the lazy writers did not write about the (in)accuracy and (in)efficiency of the radiocarbon method, especially on relatively "short" - by historical standards - periods of, say, up to 50 thousand years ago. And if we take into account that we acknowledge the possibility that nuclear weapons were used in this or that form, there is nothing to talk about. Quite obviously, the data has simply been adjusted to give the required figure of 7-8 thousand years.

**Russian:**

Хорошо, решил я, пойдём другим путём. Может быть где-то есть работы понаблюдению за процессом текущего почвообразования? И оказалось, что такие работы не только есть, но и цифры в них приводятся совершенноиные, причём намного больше похожие на действительность!

**English:**

Okay, I decided, let's go the other way. Maybe there's an effort to monitor the current soil formation somewhere? And it turned out that not only are there such efforts, but the numbers in them are quite complete, and much more like reality!

**Russian:**

Вот весьма интересная работа на эту тему Ф.Н. Лисецкого и П.В. Голеусова из Белгородского государственного университета "Восстановление почв наантропогенно нарушенных поверхностях в подзоне южной тайги", 2010 г., УДК 631.48.

**English:**

Here is a very interesting work on this topic by F.N. Lisetsky and P.V. Goleusov from the Belgorod State University: *"Restoration of soils of the anthropogenic disturbed surfaces in the southern taiga subzone", 2010, UDC 631.48.*

**Russian:**

В этой работе приводится очень интересная таблица с фактическиминаблюдениями:

**English:**

This work contains a very interesting table with actual observations:

<!-- Local
![](How Tartaria was Lost Part 3_files/25645_1000.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/25645/25645_1000.jpg#resized)

**Russian:**

В данной таблице буквами A0, A1, A1A2, A2B, B, BC, Cобозначаются разные почвенные горизонты, в том числе:

- A0 - лесная подстилка, в травянистых сообществах очёс.

- А1 - перегнойный, или гумусовый горизонт, образуется при    накоплении остатков растений и животных и преобразовании их в гумус.     Окраска перегнойного горизонта тёмная. К низу он светлеет, так как    содержание гумуса в нём уменьшается.

- А2 - горизонт вымывания, или элювиальный горизонт. Он залегает под    перегнойным. Его можно определить по смене тёмной окраски на     светлую. У подзолистых почв окраска этого горизонта почти белая    из-за интенсивного вымывания частиц гумуса. В таких почвах горизонт     перегноя отсутствует или имеет небольшую мощность. Горизонты    вымывания бедны питательными веществами. Почвы, в которых эти     горизонты развиты, обладают низким плодородием.

- В - горизонт вмывания, или иллювиальный горизонт. Он наиболее     плотный, обогащённый глинистыми частицами. Окраска его различна. У    некоторых типов почв он коричневато-чёрный из-за примеси гумуса.     Если этот горизонт обогащён соединениями железа алюминия, то    становится бурым. В почвах лесостепей и степей горизонт В     мучнисто-белого цвета из-за высокого содержания соединений кальция,    часто в виде шаробразных конкреций.

- С - материнская горная порода.

(взято тут: [https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%87%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9\_%D0%B3%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%BD%D1%82](https://ru.wikipedia.org/wiki/Почвенный_горизонт))

**English:**

In this table, the letters A0, A1, A1/A2, A2/B, B, B/C, are used to define different soil horizons:

- A0 - forest litter, in the grass communities of glasses.

- A1 - humus horizon is formed by the accumulation of plant and animal residues and their transformation into humus. The colouring of a humus horizon is dark. To the bottom it is lighter as the content of humus in it decreases.

- A2 - the washout horizon, or eluvial horizon. It lies beneath the humus. You can tell by changing the dark to light. In podzol soils, the colour of this horizon is almost white due to the intense washout of humus particles. In such soils the horizon of humus is absent or has a small power. The washout horizons are nutrient-poor. The soils in which these horizons are developed have low fertility. 

- B - the washout horizon, or illuvial horizon. It is the most dense, enriched with clay particles. Its coloration is different. In some soil types, it's brownish black because of the humus admixture. If this horizon is enriched with iron compounds of aluminum, it turns brown. In forest-steppe soils and steppe soils, the horizon is powdery white because of the high content of calcium compounds, often in the form of spheroidal nodules.

- C - mother rock.

(Taken from here: [https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%87%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9\_%D0%B3%D0%BE%D1%80%D0%B8%D0%B7%D0%BE%D0%BD%D1%82](https://ru.wikipedia.org/wiki/Почвенный_горизонт))

**Russian:**

Другими словами, говоря о толщине почвы в целом нужно сложить толщину этих слоёв. При этом из таблицы хорошо видно, что ни о каких 0.2 мм вгод речи на самом деле не идёт!

**English:**

In other words, when talking about the thickness of the soil in general, it is necessary to add the thickness of these layers. At the same time it is clearly seen from the table that no-one is really talking about 0.2 mm per year!

**Russian:**

По разрезу с номером 18 и возрастом 134 года мы получаем толщину 1040 мм без колонки BC и 1734 с колонкой BC. Особенность колонки BC состоит втом, что это часть материнской породы смешанной со слоем почвы, постепенно просочившимся в неё. В рассматриваемом случае это рыхлыйпесок. Но даже если исключить этот слой, то мы получаем среднюю скорость почвообразования 7.8 мм в год!

**English:**

From the cut with the number 18 and age 134 we get a thickness of 1040 mm without column BC and 1734 with column BC. The feature of the BC column is that it is part of the mother rock mixed with a layer of soil that gradually leaked into it. In this case it is loose sand. But even if we exclude this layer, we get an average soil formation rate of 7.8mm per year!

**Russian:**

Если посчитать скорость почвообразвания, то получатся значения от 3 до30 мм, со средним значением порядка 16 мм в год. При этом из получаемых данных видно, что чем старше почва, тем скорость её роста ниже. Но какбы там ни было, при возрасте порядка 100 лет толщина почвенного слоя оказывается более метра, а при возрасте 600 лет толщина составляет от2-х до 3-х метров.

**English:**

If you calculate the speed of soil formation, you get values from 3 to 30 mm, with an average value of about 16 mm per year. Thus from the received data we can see clearly that the older the soil, the slower its rate of growth. But regardless, by the age of about 100 years, the thickness of the soil layer is more than a meter, and at the age of 600 years, the thickness ranges from 2 to 3 meters.

**Russian:**

Таким образом данные реальных наблюдений дают совершенно другие цифры скорости почвообразования, чем данные из справочников по экологии,основанные на неких предположениях и эмпирических построениях.

**English:**

Thus the data from real observations provides completely different numbers for the rate of soil formation than those from environmental directories, the latter being based on assumptions and some empirical constructions.

**Russian:**

Это, в свою очередь, означает, что очень тонкий слой почвы, который наблюдается в ленточных борах Алтая, после которого сразу следуетматеринская порода в виде песка, говорит о том, что леса эти очень молодые, им от силы 150, максимум 200 лет.молодые, им от силы 150, максимум 200 лет.

**English:**

This, in turn, means that a very thin layer of soil, which is observed in the ribbon forests of the Altai, followed immediately by the mother rock in the form of sand, indicates that these forests are very young. They are certainly 150-200 years old at best.

© All rights reserved. The original author retains ownership and rights.
