title: High-tech ancestors or...?
date: 2011-11-21
modified: Thu 21 Jan 2021 18:06:53 GMT
category: 
tags: technology
slug:
authors: ZigZag
from: http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_21.html
originally: Mysteries of history - High-tech ancestors or.html
local: Mysteries of history - High-tech ancestors or_files/
summary: All of this is reminiscent of advanced microwave technology. Stepped waveguide transitions, periodic structures of filters, resonators, matching devices...
status: published

#### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_21.html](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_21.html)

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_222fd_ef6fc7ef_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_222fd_ef6fc7ef_L.jpg)
-->

[![](http://4.bp.blogspot.com/-kwT96EXlN8E/TsnxjqMqDLI/AAAAAAAAAgU/GGT99SUIxPg/s1600/0_222fd_ef6fc7ef_L.jpg#clickable)](http://4.bp.blogspot.com/-kwT96EXlN8E/TsnxjqMqDLI/AAAAAAAAAgU/GGT99SUIxPg/s1600/0_222fd_ef6fc7ef_L.jpg)

What is this platform in the foreground? In the center is seen an energy source (triangular current resonator), the power of which is in the order of several tens of GW. The stepped structure around it is like a half of the same energy source. It looks like a second stage with even higher power capacity. The altered symmetry matches the motor. The first thought that arises is that this is a kind of "self-propelled oven", i.e. type of a roller that can level large areas. At the same time, its energy source is, as it were, matched with the surface on which this thing could crawl. Maybe she could not crawl, but in this case she could act as a source of standing electromagnetic waves. With the help of a stepped amplifier-transformer, these waves can move to a large area in the form of a kind of extended field of standing waves. In this case, the entire field turns into a heating zone by the "microwave oven". Further, a solution can be poured onto this field and evaporated.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_222fe_9810676f_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_222fe_9810676f_L.jpg)
-->

[![](http://4.bp.blogspot.com/-fKbhHveoRLg/TsnxuumgpxI/AAAAAAAAAgc/LOCkexpvU4g/s1600/0_222fe_9810676f_L.jpg#clickable)](http://4.bp.blogspot.com/-fKbhHveoRLg/TsnxuumgpxI/AAAAAAAAAgc/LOCkexpvU4g/s1600/0_222fe_9810676f_L.jpg)

All of this is reminiscent of advanced microwave technology. Stepped waveguide transitions, periodic structures of filters, resonators, matching devices...

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_222ff_c68eda82_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_222ff_c68eda82_L.jpg)
-->

[![](http://3.bp.blogspot.com/-W4KVKvKROgw/Tsnx6nL9_TI/AAAAAAAAAgk/c9zhjVkj7as/s1600/0_222ff_c68eda82_L.jpg#clickable)](http://3.bp.blogspot.com/-W4KVKvKROgw/Tsnx6nL9_TI/AAAAAAAAAgk/c9zhjVkj7as/s1600/0_222ff_c68eda82_L.jpg)

This is an element of a giant supermagnetron. The inclined edges of blind niches (from Makchu Picchu) are replaced with stepped ones. It seems that this is a higher level of microwave technology. Explicit division of signals into harmonics, separate work with harmonic components...

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22300_33473776_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22300_33473776_L.jpg)
-->

[![](http://2.bp.blogspot.com/-poi0v_-MrnI/TsnyJJxunyI/AAAAAAAAAgs/Gxg20pFgu3Y/s1600/0_22300_33473776_L.jpg#clickable)](http://2.bp.blogspot.com/-poi0v_-MrnI/TsnyJJxunyI/AAAAAAAAAgs/Gxg20pFgu3Y/s1600/0_22300_33473776_L.jpg)

A typical form of a trapezoidal resonator power source in a stepped design. The "pink zone" is a zone of active erosion associated with vaporization and the formation of pink sediment. The Makchu Picchu Saltworks have the same characteristic color. This means that the installation has been operating in this area for a long time. Competitors could destroy this production. And they could have undermined "ours" so that strangers would not get it. By and large, it is beneficial to undermine production just for "ours" if the stronger ones start to drive them out.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22301_e1fa261f_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22301_e1fa261f_L.jpg)
-->

[![](http://4.bp.blogspot.com/-Q--baPFJtUw/TsnyvbIsK9I/AAAAAAAAAg0/GwGPKezqmt8/s1600/0_22301_e1fa261f_L.jpg#clickable)](http://4.bp.blogspot.com/-Q--baPFJtUw/TsnyvbIsK9I/AAAAAAAAAg0/GwGPKezqmt8/s1600/0_22301_e1fa261f_L.jpg)

Each type of energy source has its own specifics. For example, these elements heat locally, while the trapezoids shine, i.e. are used to heat an extended layer between microwave mirrors.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22441_9ae9b30b_XL.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22441_9ae9b30b_XL.jpg)
-->

[![](http://4.bp.blogspot.com/-Vz75NlTM7SU/TsnzZO-UHhI/AAAAAAAAAg8/SLmao2YsMlE/s1600/0_22441_9ae9b30b_XL.jpg#clickable)](http://4.bp.blogspot.com/-Vz75NlTM7SU/TsnzZO-UHhI/AAAAAAAAAg8/SLmao2YsMlE/s1600/0_22441_9ae9b30b_XL.jpg)

*Image text says: electromagnetic emanation between "Inca" mirrors*

Translator's note: The above image is fake. The righthand side wall was mirrored from the lefthand wall along the central, vertical axis. The original image is visible at [https://3.404content.com/1/91/44/1968798747074496109/fullsize.jpg](https://3.404content.com/1/91/44/1968798747074496109/fullsize.jpg). The absence of any online copy of the faked image [according to Google](https://www.google.ch/search?hl=ru-CH&tbs=sbi:AMhZZiv7s4YnrAbcmkhrkU-dsoHtUe7Zv8DOhuvPWIr6e0Y8kuVbnzYk6B1iWhXI2z3j50lgCWVeYjQqbq-yd1N-OnlTLmKPrDsNQ9AOny69_1EMbQt1TEFRjaRhyt3DC1-awl04t9wXiQzzvPwEz35nx4ivwnwvphaeqUhNbdiT7J_1aR27X2zmT1AT4XSgDkHjeuzGUKiwcLRkqGvS-QOkKOcIx0w2lTaQv1FZPwOdRZ9hPfgxexkZiRr0PKjayJ51zmq0MxIchhEPaTh4tL5nz8Xnsixd9KZ7ZMkowV-Lkfl_1pDr-rvvBmVSD6GgMbYr-94hg1ar1Y9tcnhgiXeAXOLrtUPaSMNeg&ei=BrUJYKncPOOErwT16YmIBw&start=0&sa=N&ved=2ahUKEwip94f0wa3uAhVjwosKHfV0AnE4HhDy0wN6BAgJEDU&biw=1280&bih=907) as of 2021-01-21, suggests that the author of the piece is the only publisher of the 'mirrored' version of the original image.

Microwave evaporating bath. By the size of the steps, it is easy to determine the operating frequency if the dielectric constant of the material (epsilon) is known.

Steps on a different scale are a vivid demonstration of microwave step assignment. Another sign of microwave use is the absence of wear. On the steps found by archaeologists "no man's foot has stepped".

By the way, the meaning of small and seemingly strangely located holes becomes clear. These are balancing holes that compensate for material inhomogeneity. In some areas, on the contrary, it is necessary to create a smooth inhomogeneity, for example, to focus or defocus the electromagnetic flux.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22303_2f72bd36_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22303_2f72bd36_L.jpg)
-->

[![](http://2.bp.blogspot.com/-deI5-C246c8/TsnzpVZ7Q_I/AAAAAAAAAhE/PCye0V2TcYQ/s1600/0_22303_2f72bd36_L.jpg#clickable)](http://2.bp.blogspot.com/-deI5-C246c8/TsnzpVZ7Q_I/AAAAAAAAAhE/PCye0V2TcYQ/s1600/0_22303_2f72bd36_L.jpg)

Quite recognizable horn design for impedance matching.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22304_95492ad2_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22304_95492ad2_L.jpg)
-->

[![](http://3.bp.blogspot.com/-34EATdEdp4g/TsnzysPyF_I/AAAAAAAAAhM/BKCxmJiCBFo/s1600/0_22304_95492ad2_L.jpg#clickable)](http://3.bp.blogspot.com/-34EATdEdp4g/TsnzysPyF_I/AAAAAAAAAhM/BKCxmJiCBFo/s1600/0_22304_95492ad2_L.jpg)

Translator's note: the figure framed by a doorway in the background of the above image is apparently 'new work' being passed off as old work. Compare the two images at: [https://ussr.win/photo/9c0ff946-4e9c-4089-8101-9a6b49760b80.jpg](https://ussr.win/photo/9c0ff946-4e9c-4089-8101-9a6b49760b80.jpg). Faking on this site is discussed at: [https://eto-fake.livejournal.com/1481274.html](https://eto-fake.livejournal.com/1481274.html).

And this is a completely recognizable system of cascade evaporation of the solution. Several horizontal levels, a thin layer of liquid, microwave heaters around the perimeter create surface waves with an even distribution of power over the entire area.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22305_601240e4_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22305_601240e4_L.jpg)
-->

[![](http://3.bp.blogspot.com/-dbBBfRI98Ms/Tsnz8o3umhI/AAAAAAAAAhU/Rsfk_jWeP2A/s1600/0_22305_601240e4_L.jpg#clickable)](http://3.bp.blogspot.com/-dbBBfRI98Ms/Tsnz8o3umhI/AAAAAAAAAhU/Rsfk_jWeP2A/s1600/0_22305_601240e4_L.jpg)

And with these turbulent flow formers it is easy to determine the typical level of the evaporated solution. The layer thickness is commensurate with the wavelength of the "microwave" oven. "It all fits together".

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22306_1f804722_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22306_1f804722_L.jpg)
-->

[![](http://1.bp.blogspot.com/-DE637LHXtHQ/Tsn0ElgQ5WI/AAAAAAAAAhc/n6kO-V2dISo/s1600/0_22306_1f804722_L.jpg#clickable)](http://1.bp.blogspot.com/-DE637LHXtHQ/Tsn0ElgQ5WI/AAAAAAAAAhc/n6kO-V2dISo/s1600/0_22306_1f804722_L.jpg)

So it is no coincidence that these "gates" are made from one piece. This is a source of energy in the meter range, and polyphonic at that. Various harmonics are balanced. Most likely this is a triangular current generator, although it can be more cunning...

Why are there all sorts of symbols and a strange mask on this resonator? It is possible that this is some kind of imitation. For example, for protection from competitors, the complex can be equipped with dolmens and fake starships. Protecting the same nuclear power plants. Why not protect the mining plant? However, this protection is only valid up to a certain level. If the stronger ones attack, then the defense will not save. She saves only from the weaker and sometimes from the same level.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_2230e_8da4bdf6_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_2230e_8da4bdf6_L.jpg)
-->

[![](http://4.bp.blogspot.com/-xNVU56GEDv0/Tsn0SeAAftI/AAAAAAAAAhk/nByF1kz35IY/s1600/0_2230e_8da4bdf6_L.jpg#clickable)](http://4.bp.blogspot.com/-xNVU56GEDv0/Tsn0SeAAftI/AAAAAAAAAhk/nByF1kz35IY/s1600/0_2230e_8da4bdf6_L.jpg)

From here we will take the proportions for the model of an advanced monolithic energy source in HFSS. I wonder if there is a simple proportion of step sizes in the upper corners of the resonator? Or is she irrational / transcendental?
By the way, there is a three-level resonator in one coordinate and three-level in the other. This is even cooler than those that have been considered before...

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22321_d5f4ed2d_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22321_d5f4ed2d_L.jpg)
-->

[![](http://2.bp.blogspot.com/-xGMpQnmPl6w/Tsn0c8-mbwI/AAAAAAAAAhs/P_S0B5PjXz4/s1600/0_22321_d5f4ed2d_L.jpg#clickable)](http://2.bp.blogspot.com/-xGMpQnmPl6w/Tsn0c8-mbwI/AAAAAAAAAhs/P_S0B5PjXz4/s1600/0_22321_d5f4ed2d_L.jpg)

But we will start modeling in the HFSS program with a simpler energy source...

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22325_90f81e51_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22325_90f81e51_L.jpg)
-->

[![](http://4.bp.blogspot.com/-BYSUdZiW9h8/Tsn0kWGS9mI/AAAAAAAAAh0/-nRX15zwX60/s1600/0_22325_90f81e51_L.jpg#clickable)](http://4.bp.blogspot.com/-BYSUdZiW9h8/Tsn0kWGS9mI/AAAAAAAAAh0/-nRX15zwX60/s1600/0_22325_90f81e51_L.jpg)

The proportions of this energy source turned out to be "more than simple", namely:

- The Inca angle turned out to be the 60th part of the circle (6 degrees).

- The upper edge of the resonator is equal to three conventional units.

- The lower edge of the resonator is equal to four conventional units.

- The inclined face of the trapezoidal resonator is equal to 10 conventional units.

Calculation method.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22320_91a06de7_orig.gif#clickable)](Mysteries of history - High-tech ancestors or_files/0_22320_91a06de7_orig.gif)
-->

[![](http://3.bp.blogspot.com/-qQWT5bjekBQ/Tsn0y0_BatI/AAAAAAAAAh8/kY0HTlw-ZfE/s1600/0_22320_91a06de7_orig.gif#clickable)](http://3.bp.blogspot.com/-qQWT5bjekBQ/Tsn0y0_BatI/AAAAAAAAAh8/kY0HTlw-ZfE/s1600/0_22320_91a06de7_orig.gif)

Now let's deal with the wavelength...

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_2232d_bb4077e3_L.png#clickable)](Mysteries of history - High-tech ancestors or_files/0_2232d_bb4077e3_L.png)
-->

[![](http://1.bp.blogspot.com/-rTs9M-M4cJI/Tsn1DT0WXEI/AAAAAAAAAiE/82_zA4Xwbn0/s1600/0_2232d_bb4077e3_L.png#clickable)](http://1.bp.blogspot.com/-rTs9M-M4cJI/Tsn1DT0WXEI/AAAAAAAAAiE/82_zA4Xwbn0/s1600/0_2232d_bb4077e3_L.png)

One of the vibration modes for the Inca resonator has been obtained. The wavelength is 4 times the height of the niche (10 conventional units). In other words, the wavelength for a given mode is ~ 40 conventional units. 

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_2232e_cec18f7c_S.gif#clickable)](Mysteries of history - High-tech ancestors or_files/0_2232e_cec18f7c_S.gif)
-->

[![](http://2.bp.blogspot.com/-QlAZRc7MpAc/Tsn1Mv5RkYI/AAAAAAAAAiM/9ar1w-PqfpQ/s1600/0_2232e_cec18f7c_S.gif#clickable)](http://2.bp.blogspot.com/-QlAZRc7MpAc/Tsn1Mv5RkYI/AAAAAAAAAiM/9ar1w-PqfpQ/s1600/0_2232e_cec18f7c_S.gif)

Monolithic sarcophagi work approximately the same way. By the way, in order for the energy source not to "stall", you need to adjust the shunting factor. This can be done, for example, by applying a layer of alabaster several centimeters thick to the inner surface of a granite sarcophagus. If you increase the thickness of this layer, then the power transmitted by the "sarcophagus" energy source to the solution inside will decrease. If you reduce the layer of alabaster, then the power will increase, but up to a certain limit, after which a breakdown of generation will occur, and the "sarcophagus" will stop converting the internal energy of the radio ether into electromagnetic oscillations.

The microwave sarcophagus (microwave boiler) can work with both open and closed lids. In the first case, heating or evaporation of the inner contents of the sarcophagus will occur. If you close the lid, the microwave boiler will turn into a microwave pressure cooker, i.e. the process will run at an increased pressure, and the pressure can be adjusted by the weight of the lid. A small hole can be made in the lid with an emergency valve, just like in a conventional pressure cooker. Then the pressure can no longer be regulated by the mass of the cover, but by the mass of the valve. In order for the valve to close off the outlet channel well, it can be made, for example, in the form of a conical plug, like a decanter. If it is well lapped, it will make an excellent pressure regulator. Simple and reliable. Probably such a valve was installed in this sarcophagus.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22354_fca61011_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22354_fca61011_L.jpg)
-->

[![](http://4.bp.blogspot.com/-IOfiVaUQ-EU/Tsn1YBVK51I/AAAAAAAAAiU/As0yE2GFsjY/s1600/0_22354_fca61011_L.jpg#clickable)](http://4.bp.blogspot.com/-IOfiVaUQ-EU/Tsn1YBVK51I/AAAAAAAAAiU/As0yE2GFsjY/s1600/0_22354_fca61011_L.jpg)

According to various researchers, remains of bronze were found in such holes. Hence, many researchers conclude that the holes were drilled with bronze tools. In fact, bronze rods were inserted into such holes to prevent leakage of the electromagnetic field from the zone of the thermo-chemical microwave reactor.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22355_aca129eb_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22355_aca129eb_L.jpg)
-->

[![](http://4.bp.blogspot.com/-sKm54JlN4SA/Tsn1gUNq9rI/AAAAAAAAAic/9IaPnWPMVkc/s1600/0_22355_aca129eb_L.jpg#clickable)](http://4.bp.blogspot.com/-sKm54JlN4SA/Tsn1gUNq9rI/AAAAAAAAAic/9IaPnWPMVkc/s1600/0_22355_aca129eb_L.jpg)

Superposition of the results of modeling electromagnetic fields in the HFSS program on a photograph of a microwave evaporator. Now, what does the word "sanctuary" mean? Not a small firefly, but a whole luminary...

Let's try to simulate a more advanced model of the energy source.

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22429_e6f74ee0_L.jpg#clickable)](Mysteries of history - High-tech ancestors or_files/0_22429_e6f74ee0_L.jpg)
-->

[![](http://2.bp.blogspot.com/-YbvJ0G649NY/Tsn1qD0pBJI/AAAAAAAAAik/nhJr1iL3Y3Q/s1600/0_22429_e6f74ee0_L.jpg#clickable)](http://2.bp.blogspot.com/-YbvJ0G649NY/Tsn1qD0pBJI/AAAAAAAAAik/nhJr1iL3Y3Q/s1600/0_22429_e6f74ee0_L.jpg)

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_22428_9a231e1b_L.png#clickable)](Mysteries of history - High-tech ancestors or_files/0_22428_9a231e1b_L.png)
-->

[![](http://3.bp.blogspot.com/-6f59nyO16CM/Tsn149UwhiI/AAAAAAAAAis/dgngXPOME78/s1600/0_22428_9a231e1b_L.png#clickable)](http://3.bp.blogspot.com/-6f59nyO16CM/Tsn149UwhiI/AAAAAAAAAis/dgngXPOME78/s1600/0_22428_9a231e1b_L.png)

<!-- Local
[![](Mysteries of history - High-tech ancestors or_files/0_2242a_fd7c485f_S.gif#clickable)](Mysteries of history - High-tech ancestors or_files/0_2242a_fd7c485f_S.gif)
-->

[![](http://3.bp.blogspot.com/-0l_SRccH-og/Tsn1-agwLEI/AAAAAAAAAi0/z2dPVPjnr00/s1600/0_2242a_fd7c485f_S.gif#clickable)](http://3.bp.blogspot.com/-0l_SRccH-og/Tsn1-agwLEI/AAAAAAAAAi0/z2dPVPjnr00/s1600/0_2242a_fd7c485f_S.gif)

Interesting resonance... Now it is clear why the "gate" is so high. This allows the power of the converter to be increased. and the transition from the horizontal axis of symmetry of the resonance process to the vertical one made it possible to raise the frequency by one and a half times, and with it the power twice at the expense of the frequency and twice at the expense of the height. An unambiguously advanced model... And if there is also a triangular current (+ third (+ fifth) harmonic), then the power is three times higher, i.e. the energy source is an order of magnitude more powerful only due to a different shape of the stone and, accordingly, a different structure of the electromagnetic process...

[Nanomir Laboratory](http://www.nanomir-news.ru/2009/01/10-000_22.html)

Author: ZigZag

© All rights reserved. The original author retains ownership and rights.

