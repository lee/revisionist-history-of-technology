title: Why is the recent planetary catastrophe being hidden from us?
date: 2021-08-20
modified: 2022-04-09 19:56:00
category: 
tags: catastrophe; links
slug: 
authors: Alexandra Lorenz
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: https://mylnikovdm.livejournal.com/380777.html
originally: 

<!--
#### List translated from:

[https://mylnikovdm.livejournal.com/380777.html?thread=5856873#t5856873](https://mylnikovdm.livejournal.com/380777.html?thread=5856873#t5856873)
--> 

##### Prologue
[https://cont.ws/@id260637656/1797717](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1797717)

<!-- requires login:
[https://alexandrafl.livejournal.com/616912.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/616912.html)  
-->

<!-- Obscured imagery
[https://zen.yandex.ru/media/alexandrafl/pochemu-ot-nas -skryvaiut-nedavniuiu-planetarnuiu-katastrofu-prolog-5f95cb62a81c50318eab3c6a Part1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/media/alexandrafl/pochemu-ot-nas-skryvaiut-nedavniuiu-planetarnuiu-katastrofu-prolog-5f95cb62a81c50318eab3c6a).  
-->

##### Part 1. End of the World
[https://cont.ws/@id260637656/1797723](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1797723)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/617078.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/617078.html)  
-->

<!-- Obscured imagery
[https://zen.yandex.ru/media/alexandrafl/pochemu-ot-nas-skryvaiut-katastrofu-ch1 -konec-sveta-5f95dab34dcc5c613c5bacb6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/media/alexandrafl/pochemu-ot-nas-skryvaiut-katastrofu-ch1-konec-sveta-5f95dab34dcc5c613c5bacb6)  
-->

##### Part 2. Double Star

[https://cont.ws/@id260637656/1798392](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1798392)  

<!--
[https://alexandrafl.livejournal.com/617614.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/617614.html)  
-->

##### Part 3. Ether Whirlwind

[https://cont.ws/@id260637656/1799017](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1799017)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/618158.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/618158.html)  
-->

##### Part 4. Why So Much Clay?

[https://cont.ws/@id260637656/1799926](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1799926)  

<!-- Requires login:
[https://alexandrafl.livejournal.com /618681.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/618681.html)  
-->

##### Part 5. Why Such Chaos?

[https://cont.ws/@id260637656/1800781](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1800781)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/619022.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/619022.html)  
-->

##### Part 6. Elijah

[https://cont.ws/@id260637656/1802441](https://translate.google.com/website?                                  sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1802441)

<!-- Requires login:
[https://alexandrafl.livejournal.com/619756.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/619756.html)  
-->

##### Part 7. Planetary Expansion  

[https://cont.ws/@id260637656/1804632](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1804632)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/620235.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/620235.html)  
-->

##### Part 8. Axis

[https://cont.ws/@id260637656/1807573](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1807573)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/620773.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/620773.html)  
-->

##### Part 9. End Date

[https://cont.ws/@id260637656/1810137](https://translate.google.com/website?                       sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1810137)

<!-- Requires login:
[https://alexandrafl.livejournal.com/621176.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/621176.html)  
-->

##### Part 10. Silicates From The Sun

[https://cont.ws/@id260637656/1812807](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1812807)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/621583.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/621583.html)  
-->

##### Part 11. A Series of Civilizations

[https: // cont.ws/@id260637656/1816266](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cont.ws/@id260637656/1816266)  

<!-- Requires login:
[https://alexandrafl.livejournal.com/622100.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com/622100.html)
-->
