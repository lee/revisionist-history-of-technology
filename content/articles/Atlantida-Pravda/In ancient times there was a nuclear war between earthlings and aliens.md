title: In ancient times there was a nuclear war between earthlings and aliens
date: 2011-10
modified: Thu 21 Jan 2021 13:56:46 GMT
category:
tags: thermonuclear war; catastrophe
slug:
authors: ZigZag
from: http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/blog-post_9576.html
originally: Mysteries of history - In ancient times there was a nuclear war between earthlings and aliens.html
local: Mysteries of history - In ancient times there was a nuclear war between earthlings and aliens_files/
summary: In ancient times, there was a nuclear war between earthlings and aliens?! There is much confirmatory evidence for this hypothesis.
status: published

#### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/blog-post_9576.html](from: http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/blog-post_9576.html)

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

Scientists are increasingly coming to the conclusion that many thousands of years ago there was a nuclear war between the ancient inhabitants of the Earth - [the Asuras](https://www.thefreedictionary.com/Asuras) - and space aliens, which led to an ecological disaster and a change in living conditions on our planet.

In ancient times, there was a nuclear war between earthlings and aliens?!

There is much confirmatory evidence for this hypothesis. Many traces of radiation have been found on Earth. In animals and humans, mutations occur that cause cyclopism (Cyclops have one eye positioned above the bridge of their nose). From the legends of various peoples, you can learn about the existence of Cyclops, who were at war with humans.

Secondly, radiation leads to polyploidy - a doubling of the chromosome set, which causes gigantism and doubling of organs: two hearts or two rows of teeth. Scientists periodically find the remains of giant skeletons with a double row of teeth.

A third direction for radioactive mutagenesis is mongoloidism. Although this race is the most widespread on Earth, earlier there were even more mongoloids. They were found in Europe, in Sumeria, in Egypt, and even in Central Africa.

Further confirmation of radioactive mutagenesis is the birth of freaks and children with atavisms (regression to ancestral characteristics). Radiation leads to six-fingeredness, which occurs in Japanese survivors of the American nuclear bombing, as well as in newborns from Chernobyl.

More than a hundred craters with a diameter of 2-3 kilometers have been found on Earth, among which are two huge craters: in South America (40 km diameter) and in South Africa (120 km diameter). If they had formed in the Paleozoic era (350 million years ago), then nothing would have remained of them even long ago, since the thickness of the Earth's upper layer increases by about a meter every hundred years. But these craters are still intact. This suggests that the nuclear strike took place 25-35 thousand years ago. Using 100 craters with diameters of 3 km as our base, we calculate that 5,000 Mt of bombs were detonated during the war with the Asuras.

These facts confirm that there was a nuclear war. The fire blazed for "three days and three nights" (as the Mayan people's [*Codex Ríos*](https://en.wikipedia.org/wiki/Codex_R%C3%ADos) narrates). It entailed a nuclear rain - where bombs did not fall but radiation 'fell' instead. Another terrible radiation phenomenon is light burns to the body. They are explained by the fact that the shock wave propagates not only along the ground, but also upward. Reaching the stratosphere, it destroys the ozone layer that protects the Earth from harmful ultraviolet radiation. Ultraviolet light is known to burn unprotected skin. The nuclear explosions also resulted in a significant drop in atmospheric pressure and gas poisoning, killing the survivors.

The Asuras tried to escape from death in their underground cities, but downpours and earthquakes destroyed the shelters and drove the inhabitants back to the surface. Previously, scientists believed that the "pipes" - still found in our time - that run from caves to the surface, were of natural origin. In fact, they were made with a laser weapon to smoke out the Asuras hiding in the dungeons. These "pipes" have the correct rounded shape, which is unusual for tubes of natural origin (there are many of them in the caves of the Perm region, including in the vicinity of the city of Kungur).

Now it is clear why tunnels thousands of kilometers long - found in Altai, the Urals, the Tien Shan, the Caucasus, in the Sahara and Gobi deserts, in North and South America - were dug throughout the planet.

Perhaps lasers were used for more than just smoking out Asuras. As soon as the laser beam reached the molten underground layer, magma erupted, forming artificial volcanoes over time. 

Those who remained in underground shelters gradually lost their sight (everyone knows the epic story of [Svyatogor, whose father lived in the dungeon and did not come out to the surface, because he was blind)](http://stonecarving.ru/svyatogor-and-mikula.html). The descendants of the Asuras were reduced in size to dwarfs, about which there are many legends. Stunted creatures have survived to this day, and they have not only black, but also white, skin (the Menekhets of Guinea, and the Dopa and Hama peoples, a little more than a meter tall, living in Tibet).

Near Sterlitamak (a city in Bashkiria), there are two sand dunes of mineral substances. These are probably two graves for the Asuras. There are many such graves on Earth. But some Asuras have survived to this day. In the 70s, the Commission on Anomalous Phenomena received reports of meetings with giants as tall as a 40-storey building. These titans' footsteps were accompanied by a loud hum, and their feet sank deep into the ground.

As far as life underground is concerned, it is possible. According to geologists, there is more water underground than in the entire world's oceans. Underground seas, lakes and rivers have been discovered. Scientists have suggested that the waters of the oceans are associated with underground sources, and in addition to the water cycle between them, there is also an exchange of biological species. For the underground biosphere to be self-sufficient, there must be plants that emit oxygen and decompose carbon dioxide. But photosynthesis, it turns out, can also occur in complete darkness; it is enough just to pass a weak electric current of a certain frequency through the Earth. Forms of thermal life that do not need light have been discovered in places where heat emerges on the Earth's surface. Perhaps they can be both unicellular and multicellular, and even reach a high level of development.

The appearance of dinosaurs on Earth (for example, the Loch Ness monster) suggests that creatures living underground sometimes come to the surface to "graze". Many floating creatures from the time of the Asura biosphere may have found salvation underground. Reports of dinosaurs appearing in the oceans, seas and lakes are evidence of creatures that have found refuge underground infiltrating to the surface.

According to the Vedas, the Asuras were large and strong, but they were ruined by credulity and simplicity. The gods, with the help of deception, defeated the Asuras and drove them underground and to the bottom of the oceans. The pyramids scattered all over the planet (in Egypt, Mexico, Tibet, India) testify to the fact that the culture was unified, and the earthlings had no grounds for war among themselves. Those whom the Vedas call gods appeared from heaven; they are aliens from outer space, and the nuclear conflict was most likely of cosmic origin.

Author: ZigZag

© All rights reserved. The original author retains ownership and rights.


