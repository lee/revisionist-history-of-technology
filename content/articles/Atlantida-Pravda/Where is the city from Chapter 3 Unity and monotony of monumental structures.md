title: Where is the city from? Chapter 3. Unity and monotony of monumental structures scattered throughout the world
date: 2013-11-03 00:01:00
modified: 2022-04-24 10:07:54
category: 
tags: St Petersburg
slug: 
authors: ZigZag
summary: You can often see that new hieroglyphs have been applied over old ones... This is especially noticeable on Sphinx B, where, in general, the new hieroglyphs are applied over the old ones, i.e. someone was trying to change the meaning of the inscriptions...
status: published
originally: Mysteries of history. Controversial facts and speculations: Where does the city come from? Chapter 3. Unity and monotony of monumental structures scattered throughout the world.html
from: http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html
local: Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files

### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available     from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-   links.html). The translation below is kept purely for archival reasons.*

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

Trafalgar Square in London

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/400px-London_TrafalgarSquareColumn01.JPG#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/400px-London_TrafalgarSquareColumn01.JPG)
-->

[![](http://2.bp.blogspot.com/-WxaxGlMVzjE/UT2cCdmenqI/AAAAAAAACOk/hRKfyLOGZ24/s1600/400px-London_TrafalgarSquareColumn01.JPG#clickable)](http://2.bp.blogspot.com/-WxaxGlMVzjE/UT2cCdmenqI/AAAAAAAACOk/hRKfyLOGZ24/s1600/400px-London_TrafalgarSquareColumn01.JPG)

Bank of the Thames. Why is a Sphinx here?

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/img_6142.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/img_6142.jpg)
-->

[![](http://2.bp.blogspot.com/-D6qWtlQiKns/UT2dSuXQOYI/AAAAAAAACPE/Gmbsyg0H7KQ/s1600/img_6142.jpg#clickable)](http://2.bp.blogspot.com/-D6qWtlQiKns/UT2dSuXQOYI/AAAAAAAACPE/Gmbsyg0H7KQ/s1600/img_6142.jpg)

Here's Paris

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/450px-Paris_Column.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/450px-Paris_Column.jpg)
-->

[![](http://1.bp.blogspot.com/-J4UuqJfH2PE/UT2cWydA6FI/AAAAAAAACO4/l4uhU4-L-os/s1600/450px-Paris_Column.jpg#clickable)](http://1.bp.blogspot.com/-J4UuqJfH2PE/UT2cWydA6FI/AAAAAAAACO4/l4uhU4-L-os/s1600/450px-Paris_Column.jpg)

Entrance to the Louvre, the same Sphinx

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/img_3334_20634.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/img_3334_20634.jpg)
-->

[![](http://2.bp.blogspot.com/-5Cm38F5WSj0/UT2epngWtII/AAAAAAAACPQ/g-bTGBQRqpA/s1600/img_3334_20634.jpg#clickable)](http://2.bp.blogspot.com/-5Cm38F5WSj0/UT2epngWtII/AAAAAAAACPQ/g-bTGBQRqpA/s1600/img_3334_20634.jpg)

And this is Berlin

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/280px-Siegessaeule_FoL2009_01_MK.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/280px-Siegessaeule_FoL2009_01_MK.jpg)
-->

[![](http://1.bp.blogspot.com/-usHe-ymnf-4/UT2cxv5GlYI/AAAAAAAACO8/UgTuJYGhX5E/s1600/280px-Siegessaeule_FoL2009_01_(MK).jpg#clickable)](http://1.bp.blogspot.com/-usHe-ymnf-4/UT2cxv5GlYI/AAAAAAAACO8/UgTuJYGhX5E/s1600/280px-Siegessaeule_FoL2009_01_(MK).jpg)

The Sphinx again

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/0ad8bfde2b46.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/0ad8bfde2b46.jpg)
-->

[![](http://3.bp.blogspot.com/-RJj48nYHt8c/UT2fC73l0LI/AAAAAAAACPY/sH7f3Zpa1PE/s1600/0ad8bfde2b46.jpg#clickable)](http://3.bp.blogspot.com/-RJj48nYHt8c/UT2fC73l0LI/AAAAAAAACPY/sH7f3Zpa1PE/s1600/0ad8bfde2b46.jpg)

Still the same Egypt

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/4721-61.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/4721-61.jpg)
-->

[![](http://1.bp.blogspot.com/-keQBZ7wdUr0/UT2fQnOhSaI/AAAAAAAACPg/Db1HTLxxeoY/s1600/4721-6+(1).jpg#clickable)](http://1.bp.blogspot.com/-keQBZ7wdUr0/UT2fQnOhSaI/AAAAAAAACPg/Db1HTLxxeoY/s1600/4721-6+(1).jpg)

Istanbul

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/16200.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/16200.jpg)
-->

[![](http://1.bp.blogspot.com/-7NhkJyZCJqo/UT2fhIogqoI/AAAAAAAACPs/cGOoN4ur1nc/s1600/16200.jpg#clickable)](http://1.bp.blogspot.com/-7NhkJyZCJqo/UT2fhIogqoI/AAAAAAAACPs/cGOoN4ur1nc/s1600/16200.jpg)

Again the Sphinxes

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/1305326561_hattusas.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/1305326561_hattusas.jpg)
-->

[![](http://4.bp.blogspot.com/-dCqy0-Ln1Ag/UT2gOaTUKxI/AAAAAAAACP0/UK4RzO3vREU/s1600/1305326561_hattusas.jpg#clickable)](http://4.bp.blogspot.com/-dCqy0-Ln1Ag/UT2gOaTUKxI/AAAAAAAACP0/UK4RzO3vREU/s1600/1305326561_hattusas.jpg)

Rome

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/isi72a.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/isi72a.jpg)
-->

[![](http://2.bp.blogspot.com/-sQq-4dbzt1w/UT2geVqHzNI/AAAAAAAACP8/cuLKHVq7XmA/s1600/isi72a.jpg#clickable)](http://2.bp.blogspot.com/-sQq-4dbzt1w/UT2geVqHzNI/AAAAAAAACP8/cuLKHVq7XmA/s1600/isi72a.jpg)

And then the Sphinx

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/290px-Louvre-antiquites-egyptiennes-p1020361.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/290px-Louvre-antiquites-egyptiennes-p1020361.jpg)
-->

[![](http://2.bp.blogspot.com/-BTvj2mOISpo/UT2gpp8OaqI/AAAAAAAACQE/jHPO1zkCXVI/s1600/290px-Louvre-antiquites-egyptiennes-p1020361.jpg#clickable)](http://2.bp.blogspot.com/-BTvj2mOISpo/UT2gpp8OaqI/AAAAAAAACQE/jHPO1zkCXVI/s1600/290px-Louvre-antiquites-egyptiennes-p1020361.jpg)

Odessa

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/MonumentumEkaterina.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/MonumentumEkaterina.jpg)
-->

[![](http://4.bp.blogspot.com/-4e2AJVek3jU/UUwV9D-vGCI/AAAAAAAACUE/v7o1Z7e31Nw/s1600/MonumentumEkaterina.jpg#clickable)](http://4.bp.blogspot.com/-4e2AJVek3jU/UUwV9D-vGCI/AAAAAAAACUE/v7o1Z7e31Nw/s1600/MonumentumEkaterina.jpg)

And the Sphinx

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/a.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/a.jpg)
-->

[![](http://1.bp.blogspot.com/-GOlJAUOV78g/UUwaGH5XOCI/AAAAAAAACUk/vagEUnc7SYw/s1600/%D0%9E%D0%B4%D0%A1%D1%84.jpg#clickable)](http://1.bp.blogspot.com/-GOlJAUOV78g/UUwaGH5XOCI/AAAAAAAACUk/vagEUnc7SYw/s1600/%D0%9E%D0%B4%D0%A1%D1%84.jpg)

Take a closer look at the inscription on the plate.

Kiev

<!-- Local
[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/861722552.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/861722552.jpg)
-->

[![](http://3.bp.blogspot.com/--EmRQOKKQa4/UUwTrJTn0GI/AAAAAAAACTw/RQoAXDlbvKc/s1600/861722552.jpg#clickable)](http://3.bp.blogspot.com/--EmRQOKKQa4/UUwTrJTn0GI/AAAAAAAACTw/RQoAXDlbvKc/s1600/861722552.jpg)

Kiev has its own sphinx, here is a [link](http://www.segodnya.ua/oldarchive/c2256713004f33f5c2256b06003ba7b7.html) . 

Next to a stone block that resembles the Sphinx is the National Academy of Music of Ukraine building, which in many ways resembles the architecture of the famous San Carlo Theater in Naples. Yes, and here the figure of the Sphinx evokes mystical reflections. 

Translator's note: from here on, we introduced automated translation beneath the original text to help later review.

**Russian:**

И всё тот же Питер, не слишком ли много совпадений?

**English:**

Still, Peter, aren't there too many coincidences?

<!-- Local

[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/800px.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/800px.jpg)
 -->

[![](http://2.bp.blogspot.com/-UpK63qvi9WY/UT2hS1N1vgI/AAAAAAAACQM/PzYpBw2HBC8/s1600/800px.jpg#clickable)](http://2.bp.blogspot.com/-UpK63qvi9WY/UT2hS1N1vgI/AAAAAAAACQM/PzYpBw2HBC8/s1600/800px.jpg)

**Russian:**

А вот вам и вездесущий Сфинкс

**English:**

And here's the ubiquitous Sphinx.

<!-- Local

[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/__.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/__.jpg)
 -->

[![](http://1.bp.blogspot.com/-QyYe1DjabYQ/UT2hnJ3KdZI/AAAAAAAACQU/3a6FmZ_u1-w/s1600/_%D0%9F%D0%B8%D1%82%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D0%A1%D1%84%D0%B8%D0%BD%D0%BA%D1%81.jpg#clickable)](http://1.bp.blogspot.com/-QyYe1DjabYQ/UT2hnJ3KdZI/AAAAAAAACQU/3a6FmZ_u1-w/s1600/_%D0%9F%D0%B8%D1%82%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D0%A1%D1%84%D0%B8%D0%BD%D0%BA%D1%81.jpg)

**Russian:**

Или вот так

**English:**

Or like this.

<!-- Local

[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/4f6c5f81d2252.jpg#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/4f6c5f81d2252.jpg)
 -->

[![](http://2.bp.blogspot.com/-fQrLkKPSCoU/UUwYBVyeoiI/AAAAAAAACUU/3k5pgTtcavM/s1600/4f6c5f81d2252.jpg#clickable)](http://2.bp.blogspot.com/-fQrLkKPSCoU/UUwYBVyeoiI/AAAAAAAACUU/3k5pgTtcavM/s1600/4f6c5f81d2252.jpg)

**Russian:**

Возможно это не простые совпадения, это знаки - знаки приверженности чему-то, или кому-то, например: ["Мы знаем, мы помним, мы чтим"]

**English:**

Maybe it's not just coincidences, it's signs - signs of commitment to something, or to someone, for example: 

> ["We know, we remember, we honor."](http://atlantida-pravda-i-vimisel.blogspot.com/2012/01/blog-post_5943.html).

**Russian:**

Вот только что это такое? Тайна сфинксов.

В Санкт-Петербурге, на Университетской набережной, напротив здания института живописи стоят два сфинкса, изваянные из красного сиенского гранита. В мае 1832 года они были привезены из-под Фив в Петербург для украшения строившейся тогда пристани напротив Императорской Академии Художеств. 

**English:**

That's just what it is. The mystery of the sphinxes.

In St. Petersburg, on Universitetskaya Embankment, opposite the Institute of Painting, there are two sphinxes made of red Siena granite. In May 1832 they were brought from under Thebes to St. Petersburg to decorate the wharf in front of the Imperial Academy of Arts, which was then under construction.

<!-- Local

[![](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/16439_600.png#clickable)](Where is the city from Chapter 3 Unity and monotony of monumental structures scattered throughout the world_files/16439_600.png)
 -->

[![](http://4.bp.blogspot.com/-SPrzDgnnc1s/Uo9ds-LGC0I/AAAAAAAAFdM/wFB_t_VNdoU/s1600/16439_600.png#clickable)](http://4.bp.blogspot.com/-SPrzDgnnc1s/Uo9ds-LGC0I/AAAAAAAAFdM/wFB_t_VNdoU/s1600/16439_600.png)

**Russian:**

Сфинксы на берегу Невы украшены двойной короной царей Египта. На основании сфинксов высечены надписи. Сфинксов с таким богатым эпиграфическим материалом нет больше нигде в мире. Однако иероглифы нанесены очень неровно. Видно, что зачастую новые иероглифы были нанесены поверх старых!.. Особенно это заметно на сфинксе Б, где, в целом, новая надпись нанесена по старой, т. е. кто-то пытался поменять смысл надписей...

**English:**

The sphinxes on the banks of the Neva are decorated with the double crown of the Egyptian kings. The inscriptions are carved on the bases of the sphinxes. There are no sphinxes with such rich epigraphic material anywhere else in the world. However, the hieroglyphs are applied very unevenly. You can often see that new hieroglyphs have been applied over old ones... This is especially noticeable on Sphinx B, where, in general, the new hieroglyphs are applied over the old ones, i.e. someone was trying to change the meaning of the inscriptions...

**Russian:**

Прорисовка основных надписей на основании сфинксов (весьма и весьма приблизительная). [http://egyptiaca.narod.ru/articles/cherezov-sphinx.pdf](http://egyptiaca.narod.ru/articles/cherezov-sphinx.pdf)

Транслитерация.

Надпись № 1. Сфинкс "А" (со стороны берега)

Дшему нёвопокиреними бе, невоевадцциними? Нетщо "муццеамы" ддцнаво Царями? Неё(им поим. Дза)лаим Ня (и ныне чця)сьды. Имися неыядцими, тцаядшяцяе тьсим Вотца.

Надпись № 2. Сфинкс "А" (со стороны Невы) Дшему невопокиреными бе, невоевадцциними, нецасьныеми? Гнавомы нас ву Авоа Имия. Ево радца на руны аяны. Мыва не цепево дцсаими, - воли, адживи Царями! Нё(им поим. Дза)лаим (е ныне чця) нрзб. Дшаим Вотца.

Надпись № 3. Сфинкс "Б" (со стороны берега) Дшему невопокирьсьдце? Езже не вон ше маетщо, - наруд циво вуеанево царя? (Им поим, дза)лаим (и ныне чц)ед. Сими ноне тиими дц нами воля - невонами. Неё(и ныне чця)инны, дцтшивы неа. Дшаим Вотца.

Надпись № 4. Сфинкс "Б" (со стороны Невы) Дшему нёвопокиреними бе, невоевадцциними? Нетщциненыа ка воддцнесыцо ецво Царемнеё (Им поим, тьще)лаим Не (и ныне чця)лаим ныё цищави, цимицтщо вяцнядця, нова и ...нрзб. ... Вотца.

Перевод. Надпись

№ 1. Сфинкс "А" (со стороны берега) Почему мы не были покорены и завоёваны? Неужели "мучимы" снова Царями? Им поём. Желаем Им и ныне счастья и чести. Истово чтим Отца.

Надпись № 2. Сфинкс "А" (со стороны Невы) Почему были непокорными, невоинственными, несчастными? Гнали нас во имя Его. Его указ на руны эти. Мы не уз чаяли, а воли, возрождения Царями. Им поим, желаем им чести. Чаем Отца.

Надпись № 3. Сфинкс "Б" (со стороны берега) Почему непокорны? Разве не он ещё страдает, - народ потерпевшего поражение царя? Им поём, желаем и ныне чад. Этими самыми с нами воля! Ими! Они и ныне чтимы, почитаемы они. Чаем Отца.

Надпись № 4. Сфинкс "Б" (со стороны Невы) Почему мы не были покорены и завоёваны? Бесценное как вознесено естество Царей! Им поём, желаем Им, - и ныне желаем им славы, почета большего, нового... (Чаем) Отца. 

**English:**

Drawing of the main inscriptions on the basis of sphinxes (very, very approximate). [http://egyptiaca.narod.ru/articles/cherezov-sphinx.pdf](http://egyptiaca.narod.ru/articles/cherezov-sphinx.pdf)

Transliteration:

Inscription number 1. Sphinx "A" (shore side):

> What of the new kings, the non-warriors? Are there no "muzeamy" dtzanovo kings? We are the kings of the world, and we are the kings. They are not the ones who are not wanted, they are the ones who are not wanted.

Inscription number 2. Sphinx "A" (from the Neva's side):

> How can we be unmolested, unloved, unqualified? Gnavomys us vu Avoa Imija. His heart is on the fleece.   We are not cepevo dzsaimi, - will, ajivi kings! Nyo(im poim. Dza)laim (e now chzia) nrzb. Dshaim Votza.

Inscription number 3. Sphinx "B" (side of the shore):

> What's wrong with the tsar? Is it not the wont of the man, - the narudh of the king's voyeur? (Im poim, dza)laim (and now is the hour). And by these, not by these, our will is not our will. Neyo(and nowadays chia)inns, dtshivy nea. Dshaim Votza.

Inscription number 4. Sphinx "B" (from the Neva's side):

> What are the newcomers, the non-warriors? The kings of our kingdom are not (and nowadays are not) the same, they are the same, the same, the same, and the same. Wotza

Translation. Sync, corrected by an older man: 

Inscription 1. Sphinx "A" (shore side):

> Why have we not been conquered and conquered? Are we "tormented" again by Kings? To them we sing. We wish them happiness and honor even now. We honor the Father fervently.

Inscription number 2. Sphinx "A" (from the River Neva side): 

> Why were we disobedient, unwilling, unhappy? They persecuted us in His name. His decree for these runes. We did not expect, but the will, the rebirth of Kings. To them we drink, to them we desire honor. We pray to the Father.

Inscription number 3. Sphinx "B" (shore side):

> Why are we disobedient? Are they not still suffering, the people of the defeated king? We sing to them, we wish for their children now. By these very wills! By them! They are honored, they are honored, they are honored. We pray to the Father.

Inscription number 4. Sphinx "B" (from the Neva side):

> Why were we not subdued and conquered? How priceless is the exalted nature of Kings! To them we sing, we wish them, - and now we wish them glory, honor greater, new... (Chai/Tea) Father.

© All rights reserved. The original author retains ownership and rights.
