title: Links to Alexander Markhov Google-translated articles
date: 2021-07-23 00:02
modified: 2021-12-30 21:24:53
category:
tags: architecture; technology; links
slug:
authors: Alexander Markhov
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: draft
local:
from: http://slavruss.narod.ru/osnown/glawn.htm
originally: Alexander Markhov.md

#### Translated from:
[http://slavruss.narod.ru/osnown/glawn.htm](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/glawn.htm)

#### Aircraft
Merkaba alphabet
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM4.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM5.htm)
[6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM6.htm)
[7](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM7.htm)
[8](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM8.htm)
[9](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Am/AM9.htm)

[Sun Stone](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vrs/vrs1.htm "Sun Stone")

[A Space Odyssey of Mesoamerica](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm1.htm "A space odyssey of Mesoamerica")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm4.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/kosm/kosm5.htm)

[Secrets of the Grebennikov platform](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/TPG/Tayna.htm "Secrets of the Grebennikov platform")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/TPG/Tayna.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/TPG/TPG2_1.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/TPG/TPG2_2.htm)

[Yagalet](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Iag/Iag1.htm "Yagalet")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Iag/Iag1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Iag/Iag2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Iag/Iag3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Iag/Iag4.htm)

[Weapon of the gods, or how to build a UFO](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_NLO.htm "Weapon of the gods, or how to build a UFO") 

[Whirlwind - the weapon of the gods](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_vihr_OB.htm "Whirlwind - the weapon of the gods")

Whirlwind - the weapon of the gods from the download site (Seemingly broken as of 2021-08-09)

[Part1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://depositfiles.com/files/f5w08f00y)
[Part2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://depositfiles.com/files/rk11dha4x) (\*.rar, 254 + 269 Mb)

#### Secrets of the megaliths

[Megalithic Confrontation](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mp/mp1.htm "Megalithic confrontation") 
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mp/mp1.htm "Origins")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mp/mp2.htm "MesoAmerica")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mp/mp7.htm "Sumer")

[Stonehenge Mystery](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mp9.htm "Stonehenge Mystery")

[The End of the Mystery of the Egyptian Pyramids?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr1.htm "Конец тайны Египетских пирамид?")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr4.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pr/Pr5.htm)

[Labyrinths - the navigation system of the White Sea region](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/KL/KL1.htm)
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/KL/KL1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/KL/KL2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/KL/KL3.htm)

[Mystery of the Lovozero tundra](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt0.htm "Тайна Ловозёрских тундр")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt0.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt1.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt2.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt3.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt4.htm)
[6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/tlt/tlt5.htm)

[Seids - stone guardians of the gods?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Sd/Sd.htm "Seids - stone guardians of the gods?")

[The end of the Coral Castle mystery?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kz/Kz1.htm)
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kz/Kz1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kz/Kz2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kz/Kz3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kz/Kz4.htm)

[Secrets of the Maidans](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mdn/mdn1.htm)

[Mounds with "mustaches"](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mdn/mdn3.htm "Курганы с "усами"")
[](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/mdn/mdn3.htm "Barrows with "mustaches"")

[Complex of Bru-na-Boyne](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vrs/vrs5.htm "Комплекс Бру-на-Бойне")

[Glastonbury](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gl/Gl1.htm "Гластонбери")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gl/Gl1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gl/Gl2.htm)

[Costa Rica Balls](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vrs/vrs10.htm "Costa Rican balls")

[Temples of Malta](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vrs/vrs7.htm "Храмы Мальты")

[Avebury Henge](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Au/au1.htm "Эйвбери-хендж")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Au/au1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Au/au2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Au/au3.htm)

[Star Link](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zs/zsb1.htm)

[Vajra is easy !](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vdr/vdr1.htm "Vajra is easy!")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vdr/vdr1.htm "Оружие богов")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vdr/vdr4.htm "Кайлас")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vdr/vdr8.htm "Ваджра")

[The Fall of Jericho](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ier/Ier.htm "Падение Иерихона")

[Angkor - City of the Gods](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang1.htm "Ангкор - город богов")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang1.htm "Ангкор - город богов")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang2.htm "Ангкор")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang3_0.htm "Храмы Ангкора")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang4_1.htm "Если бы строили люди")
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang5.htm "Как строили боги")
[6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ang/ang6a.htm "Ангкор - город богов")

[Fatal Impact](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Uf/udf1.htm "Удар Фатума")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Uf/udf1.htm "Удар Фатума")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Uf/udf2.htm)

[Unknown pyramids](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/np/np1.htm "Unknown pyramids")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/np/np1.htm "White pyramid")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/np/np6.htm "Link of the gods")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/np/np12.htm "Spare communication channel")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/np/np10.htm "Umbrella Tamil Nadu")

##### Remainder includes traces of quarrying techniques

[Wandering stones of California](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/bk/bk1.htm "Блуждающие камни Калифорнии")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/bk/bk1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/bk/bk2.htm)

[Megaliths of Tiwanaku](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw1.htm "Мегалиты Тиуанако")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw1.htm "Мегалиты Тиуанако")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw2.htm "Мегалитический комплекс Тиуанако")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw3_1.htm "В поисках аналога")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw3_2.htm "Ла-Вента")
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw3_3_4.htm "Две станции Тиуанако")
[6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tw/tw3_5_7.htm "Почему плакали боги?")

[Two fortresses of Panticapaeum](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Krk/krk1.htm)
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Krk/krk1.htm "Две крепости")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Krk/krk2.htm "Крымская война")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Krk/krk3.htm "Западная крепость")

[Machu Picchu](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu1.htm "Machu Picchu - the key to the heart of South America")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu1.htm "Мачу-Пикчу - ключ к сердцу Южной Америки")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu2.htm "Сакральное ущелье")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu3.htm "Тайна Мачу-Пикчу")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu4.htm "Приёмники и потребители энергии")
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu5.htm "Апуримак и Чокекирао")
[6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu6.htm "Дороги инков")
[7](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu7.htm "Куско, Морай и Саксайуаман")
[8](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ma4u/mu8.htm "Планетная катастрофа")

[Chavin de Huantar](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu1.htm "Чавин-де-Уантар")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu1.htm "Чавин-де-Уантар")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu2A.htm "Рудники богов")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu3.htm "Чавин-де-Уантар - ВПП")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu4.htm "Чавин-де-Уантар - станция связи")
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/ChU/chu5.htm "Чавин-де-Уантар - станция ПВО")

[Chand Baori](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Skl/skl1.htm)
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Skl/skl1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Skl/skl2a.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Skl/skl3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Skl/skl4.htm)

[Herodium](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ird/ird.htm "Иродион")

[Caral](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kr/Kr1.htm "Караль")

[Jug](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv1.htm "Звездчатые крепости")

[Kuvshinov Valley](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kk/Kk1.htm "Долина Кувшинов")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kr/Kr1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kr/Kr2.htm)

[Eye of Ra](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gr/gr1.htm "Как строили пирамиды")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gr/gr1.htm "Египет: тайна богов")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gr/gr2.htm "Неизвестная энергия Нила")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Gr/gr3.htm "Как строили пирамиды")

[Ostia Antica](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ost/ost1.htm "Остия-Антика")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ost/ost1.htm "Остия-Антика")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ost/ost2.htm "Дороги ведут в Рим")

[Star Fortresses](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv1.htm "Звездчатые крепости")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv3.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv4.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zvk/zv5.htm)

[Roman theaters of the gods](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Rt/rtb1.htm "Roman theaters of the gods")

#### Three Arks
[Ark of Gabriel](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kg/Kg1.htm "Ковчег Гавриила")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kg/Kg1.htm "Три Ковчега")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kg/Kg2.htm "Ковчег Завета")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Kg/Kg3.htm "Ковчег Гавриила")

[Star temples of the gods](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zx/zxb1.htm)
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zx/zxb1.htm "Acropolis of Athens")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zx/zxb2.htm "Baalbek complex")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zx/zxb3.htm "Star landings of the gods")
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zx/zxb4.htm)

#### Additional
[Lower World Gold](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Znm/znm1.htm "Nether Gold")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Znm/znm1.htm "lower world")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Znm/znm2.htm "South American gold")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Znm/znm3.htm "Anunnaki gold")

[Three Empires](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tri/Tri1_ist.htm "Three empires")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tri/Tri1_ist.htm "Mesoamerican Empire")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tri/Tri3_ir.htm "Rama's empire")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Tri/Tri4_si.htm "Sumerian empire")

[Colonnaded streets of the Mediterranean](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ku/kul1.htm "Colonnaded streets of the Mediterranean")

#### My guests
1. Chulkov L.Ye. [Scientific analysis of the Third Appeal](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Chul3.rar "Научный анализ Третьего Обращения")

2. Babikov Yu.A. [Fundamentals of the physics of matter in a multidimensional universe](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Osnova_fiziki.rar "Fundamentals of the physics of matter in a multidimensional universe")

3. Babikov Yu.A. [Requiem](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Recviem.rar "Requiem")

[Everything is a lie](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.falsehood.me/ "Everything is a lie")

#### Applications

Free Energy

[Eternal Lamp](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/vrs/vrs4.htm "Eternal lamp")

[Notes on free energy](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Zam_FL.htm "Free energy notes")

[Heat generator from the Creator](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/TgT/TgT.htm "Heat generator from the Creator")


#### Contacts with other worlds 
S. Gaynulina - [Contact in Pushchino](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Kt1.htm "Contact in Pushchino")

V.Kostrykin - [Beyond the Unknown](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Kt2.htm "Beyond the unknown")

E. Borovoy - The [fourth dimension](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Kt3.htm "Fourth dimension")


#### Who are we and why on Earth? KOH 

[Third Appeal to Humanity](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Obr3.htm "Third Appeal to Humanity")

[Fourth Appeal](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Obr4.htm "Fourth Appeal")

[Urantia is a book of all times and peoples](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.urantia.ru/book/local_ver/ "Urantia - the book of all times and peoples")

Lindemann P. [Secrets of free energy of cold electricity](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Lind.htm "Secrets of the free energy of cold electricity")

Mangretten G. [Gray Patent Design Report No. 4,595,975](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Mangr.htm "Gray Patent Design Report # 4,595,975")

[Lost electricity](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv.htm "Lost electricity")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv1.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv2.htm)
[4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv3.htm)
[5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Pv/pv4.htm)

[Why does the Earth rotate?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_zemlia.htm "Why does the Earth rotate?")

[My transgenerator](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/MVG/MVG.htm "My transgenerator")
[](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/MVG/MVG.htm "My transgenerator")


Yuri Kravchuk - [Who are we?](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Kosmos.rar "Space.  Who are we in it?")
New
Sal Rachel - [Messages from the Founders](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/poslaniya_osnov_2.zip "Earth Change and Founders' Messages")

Lawrence Spencer - [Prison](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Tiurma.rar "Hell called "Earth"")

[Earth Change and Founder's Messages](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/poslaniya_osnov_2.zip "Earth Change and Founders' Messages")

["Hell" called "Earth"](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Tiurma.rar "Hell called "Earth"")

Kelder P. [Whirlwinds of](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/VD/VD1.htm "Whirlwinds of rebirth - an ancient secret of Tibetan lamas")

[Rebirth](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/VD/VD1.htm "Whirlwinds of rebirth - an ancient secret of Tibetan lamas")

[I - An Ancient Secret of Tibetan Lamas](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/VD/VD1.htm "Whirlwinds of rebirth - an ancient secret of Tibetan lamas")

Virginia Essen - [Galactic Man](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Galactic_Human.zip "Galactic man")

Michael Sharp - [The Book of Life](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Sharp_Kn_jzn.djvu "The book of life")

Melchizedek D. [Remembering Our Ancient Past](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zj/Zj1.htm "Remembering our ancient past")
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zj/Zj1.htm)
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zj/Zj2.htm)
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Zj/Zj3.htm)

[3rd Appeal to Humanity (transcript)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_logika.htm "3rd Appeal to Humanity")

Larisa Seklitova - [Man of the Golden Race](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Seklitova_Zol_rasa.rar "Man of the Golden Race")

- [Conversations about the unknown](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Seklitova_Besed_neizv.zip "Conversations about the unknown")

Ramacharaka [Merkaba - breathing practice of Tibetan lamas](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Dyhanie.htm "Меркаба - практика дыхания тибетских лам")

[The concept of a new system for assessing opinions in the field of electoral legislation](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_konzWZ.htm "The concept of a new system for assessing opinions in the field of electoral legislation")

Michael Newton - The [Journey of the Soul](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Puteshest_dushi.rar "Путешествие души")

- [The Purpose of the Soul](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Prednazn_dushi.rar "Предназначение души")

Petrosyants V.V. [Principles of Hermeticism by](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Princip.htm "Принципы герметизма")

Rhonda Bern
[Mystery](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Sekret.rar "Тайна")

K district - [New knowledge](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Kraion_znani.rar "New Knowledge")

Georgy Boreev
[1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/C_Atlantida.rar "Alien Civilizations of Atlantis")
[2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Humanoid.rar "Humanoid History")
[3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Nibiru.rar "6th Race and Nibiru")

Ratner S.G. [Bioenergy Secrets](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Sekret_bio.rar "Bioenergy Secrets")

[Dedication to Elizabeth Hach](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Elizabet_Hach.rar "Посвящение")

Patricia Corey - [Atlantis](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ltr/Atlantida.rar "Атлантида")

My Complex
[Eye of Rebirth](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ej/Ej_menu.htm)**  **[](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Ej/Ej_menu.htm)

[From the paradox in the multiplication of numbers to ...](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_paradox.htm "From the paradox in the multiplication of numbers to ...")

[Returning to the 60-ary number system](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Annot/A_sist60.htm "Returning to the 60-ary number system")

[about the author](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/autor.htm)

[Results of creativity](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Rez_tv.htm)

[Bibliography](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Biblgraf.htm)

[Feedback](mailto:slavruss@narod.ru)

[Meetings on forums](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/Forum.htm)

