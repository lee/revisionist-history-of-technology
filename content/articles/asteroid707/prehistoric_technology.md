title: Prehistoric Technology
date: 2015-07-20
modified: Mon 15 Feb 2021 14:44:03 GMT
category:
tags: technology, forged history
slug:
authors: Unknown
from: https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/dopotopnye-texnologii/
originally: c5NS3.html
local: prehistoric_technology_files
summary: But there is a "science" in which no one was allowed on the doorstep. We can clearly see that history is written to order. History is a weapon.
status: published

#### Translated from:

[https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/dopotopnye-texnologii/](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/dopotopnye-texnologii/)

<!-- Local
[![](prehistoric_technology_files/ruiny-evropa-221x300.jpeg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ruiny-evropa-2/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/ruiny-evropa-221x300.jpeg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ruiny-evropa-2/)

**Russian:**

*Руины в Европе*

**English:**

*Ruins in Europe*

**Russian:**

Дилетанты и профессионалы в науке

**English:**

Amateurs and professionals in science

**Russian:**

Науку, в основном, двигают энтузиасты и, нередко, именно  «дилетанты» добиваются наибольших успехов. Так, крупнейшие направления в науке, определившие, практически, все достижения 20 века были достигнуты именно дилетантами: превратить «магнетизм» в «электричество»  сумел «неграмотный ученик переплетчика» Майкл Фарадей, эру генетики открыл «горохвый закон австрийского монаха». Да и Чарльз Дарвин, был любителем. «Водородную» (на самом деле литиевую) бомбу и реактор на быстрых нейтронах, идею управляемого термоядерного синтеза сформулировал окончивший семь классов сержант срочной службы Олег Лаврентьев (правда сдавший экстерном за десятилетку). То есть, этот самоучка «умыл» кучу нобелевских лауреатов, по обе стороны океана! Но есть «наука», в которую кого попало и на порог не пускали. Это - история.[] Сейчас мы наглядно видим, что историю пишут на заказ. Так историю «великой Укрии» ввезли из-за океана в учбениках проплаченных фондом Сороса. История - это оружие. Понимая это, русские в Крыму, дав взятку киевским чиновникам, для своих школ выпустили другой учебник истории. В одной стране, которой на тот момент была Украина, было две истории, противоречащих одна другой! Ну а история дрвнего мира - это сплошное художественное творчество. На одну полку следует поставить подвиги Геракла и походы Юлия Цезаря или Александра Македонского.  

**English:**

Science is primarily driven by enthusiasts, and it is often "dilettantes" who achieve the greatest successes. Thus, the greatest advances in science - those that defined almost all achievements of the 20th century - were achieved by dilettantes: 

- Michael Faraday, "illiterate student of a bookbinder", managed to turn "magnetism" into "electricity"; 

- the era of genetics was introduced by the "pea law of the Austrian monk" Gregor Mendel. 

- Charles Darwin, too, was an amateur. 

- The "hydrogen" (actually lithium) bomb, fast-neutron reactor, and the idea of controlled thermonuclear fusion, were formulated by conscript Oleg Lavrent'ev, who completed seven classes (although he passed with an external examination for a decade). That is to say, this self-taught man "washed up" a bunch of Nobel laureates on both sides of the ocean!

But there is a "science" in which no one was allowed on the doorstep. We can clearly see that history is written to order. History is a weapon. Thus, the history of "the great Ukraine" was imported into schools from overseas and paid for by the Soros Foundation. Understanding this, Russians in Crimea, having bribed Kiev's officials, published another history textbook for their own schools. So in one country, which at that time was Ukraine, there were two histories contradicting each other. Similarly, the history of the old world is an artistic creation. On the same shelf should be placed the exploits of Hercules and the campaigns of Julius Caesar or Alexander the Great.

<!-- Local
[![](prehistoric_technology_files/kultura-shnurovoi-keramiki-300x177.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kultura-shnurovoi-keramiki/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kultura-shnurovoi-keramiki-300x177.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kultura-shnurovoi-keramiki/)

**Russian:**

*Культура шнуровой керамики*

**English:**

*Corded ceramic culture*

**Russian:**

И тут встает вопрос: а чем же занимаются профессиональные археологи? Ответ до банальности прост: пилят бабло, паразитируя на теме. Здесь главное - вписаться в установленные рамки, пережевывая по сто раз одну и ту же жвачку, получая при этом ставки, гранты, звания, должности, и, естественно - зарплаты. Как говорится - не ради Исуса, а ради хлеба куса. Меня всегда приводит в восторг термин «шнуровая керамика» - нет, вы вдумайтесь, по форме печного горшка целое направление в «науке»! Вот из справочника:

**English:**

And here there is a question: what do professional archaeologists do? The answer is so simple it is banal: they are minting money, parasitizing off the subject. Here's the main thing: to fit into established frameworks, chewing the same cud a hundred times, while getting subsidies, grants, titles, positions and, of course, salaries. As the saying goes: "not for the sake of Jesus, but for the sake of the bread of the cousin". I am always fascinated by the term "corded ceramics". No, think about it: there is a whole direction in "science" based on the shape of this clay stove pot! Here, from the reference book:

<!-- Local
[![](prehistoric_technology_files/kubok-s-utolshennym-dnom-263x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kubok-s-utolshennym-dnom/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kubok-s-utolshennym-dnom-263x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kubok-s-utolshennym-dnom/)

**Russian:**

*Кубок с утолщенным дном*

**English:**

*Bowl with thickened base*

**Russian:**

> Кубок с утолщенным дном (англ. protruding foot beaker, нем. Spreizfußbecher), типичная форма сосуда позднего неолита в Нидерландах. Характеризуется расширяющимся горлом, S-образным профилем и плоским утолщенным дном (рис. 73). Сосуды украшались шнуровым орнаментом, отпечатками зубчатой лопаточки или насечками (при помощи рыбных косточек), принадлежали культуре, для которой типичны одиночные захоронения в грунтовых ямах или под курганами, **использование боевых топоров**. Эти признаки говорят о том, что культура представляет собой голландскую ветвь широко распространенного комплекса шнуровой керамики - боевых топоров или культур одиночных погребений. В Голландии наблюдается также гибридизация между культурой К. у. д. и культурой **колоколовидных кубков**. Радиоуглеродный анализ позволяет датировать К. у. д. 2500 - 1900 гг. до н.э.

**English:**

> Protruding foot beaker (German: Spreizfußbecher), a typical form of Late Neolithic vessel in the Netherlands. It is characterized by an expanding neck, S-shaped profile and flat thickened base (Fig. 73). The vessels were decorated using cord, serrated spatula imprints or incisions (using fish bones), belonged to a culture typical for its solitary burials in earth pits or under barrows, **the use of battle axes** are typical. These features suggest that the culture is a Dutch branch of a widespread complex of corded pottery, battle axes or single burial cultures. Hybridization between the C.O.D. culture and the **bell cup culture** has also been observed in Holland. The C.O.D. culture can be radiocarbon dated from 2500 to 1900 BC.

**Russian:**

- **Пердставьте себе:** сколько копий сломано, сколько диссертаций защищено, скоько народу кормилось, чтоб «сложился научный консенсус» о «боевых топорах», «колоколовидных кубках» и «горшках с утолщенным дном»! А что они доказали? Что люди пять тысяч лет назад жили ничуть не хуже, чем наши отцы, максимум - деды: после войны глиняные крынки с молоком стояли в каждом деревенском доме. В лаптях люди приезжали на Выставку Достижений Народного хозяйства. Каменными топорами, в какой-нибудь амазонии, вам и сейчас запросто могут башку проломить. Да и пять ли тыщ лет этим черепкам - большой вопрос: об этом самом «радиоуглеродном анализе» уже куча анекдотов из жизни. Недавно велся » [Курортный раскоп](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://sibved.livejournal.com/135613.html) » под Старой Руссой. Вот нашли там вот такой примитив из неолита:

**English:**

Imagine: how many copies were broken, how many dissertations were defended, how many people were fed so that "there was a scientific consensus" about "battle axes", "bell-shaped cups" and "pots with a thickened bottom"! And what did they prove? That five thousand years ago people lived no worse than our fathers or, at the latest, our grandfathers. After the war, every village house had a clay jar to store milk. People used to come to the Exhibition of Achievements of the National Economy in sandals. You can still get your head bashed in with stone axes in the Amazon. And whether these crocks are really five thousand years old is a big question: there are already a lot of real-world anecdotes about this "radiocarbon analysis". Recently there was a "[resort excavation](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://sibved.livejournal.com/135613.html)" near Staraya Russa. There we found this kind of primitive artifact from the Neolithic period:

<!-- Local
[![](prehistoric_technology_files/neolit-18veka-108x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/neolit-18veka/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/neolit-18veka-108x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/neolit-18veka/)

**Russian:**

*Неолит. XIX век*

**English:**

*Neolithic. 19th century*

**Russian:**

Вот еще вещица из той же седой древности, найденной в том же месте:

**English:**

Here's another piece of the same grayish antiquity found in the same place:

<!-- Local
[![](prehistoric_technology_files/neolit-19vek-201x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/neolit-19vek/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/neolit-19vek-201x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/neolit-19vek/)

**Russian:**

*Неолит. XIX век*

**English:**

*Neolithic. 19th century*

**Russian:**

А потом - тырц!

**English:**

And then, zap!

<!-- Local
[![](prehistoric_technology_files/plomba-300x185.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kak-davno-eto-bylo/plomba/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/02/plomba-300x185.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kak-davno-eto-bylo/plomba/)

**Russian:**

*Свинцовая пломба*

**English:**

*Lead seal*

**Russian:**

- Свинцовая пломба с датой - 1820 год! Ну и еще [была монета](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://sibved.livejournal.com/135613.html) чуть постарше. «Если факты против нас - тем хуже для фактов» - сей девиз не прокатил, только потому, что вмешался интернет, который в режиме реального времени все это отслеживал. Вот фото:

**English:**

A lead seal with date - 1820! Well, and [there was a coin](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://sibved.livejournal.com/135613.html) - this kite didn't fly, only because the Internet intervened, which was tracking it all in real time. Here's a photo:

<!-- Local
[![](prehistoric_technology_files/djigity-kavkaza-300x219.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kogda-ischezla-tartariya/djigity-kavkaza/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/djigity-kavkaza-300x219.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kogda-ischezla-tartariya/djigity-kavkaza/)

**Russian:**

*Кавказские джигиты*

**English:**

*Caucasian dzhigits*

**Russian:**

Это не  реконструкторы играют в дружину княза Олега - это кавказские джигиты накануне 20 века. Нам кто-нибудь рассказывал, что еще каких-то сто лет назад, в Европе реально воевали с луками и стрелами, одеваясь в доспехи а-ля «коротка кольчужка-то» из фильмов Эйзенштейна? И вот выкапывают археологи костяные наконечники для тех стрел, и массу других полезных еще в прошлом веке вещей. Ну и ладно, бог с ними, пусть бы копали, если б не вставали бревном на пути познания, ополчаясь на тех, кто находит что-то не вписывающееся в прокрустово ложе, устроенное профессиональными историками. Это ж совсем нужно зажмуриться, что б не замечать, что «[ренессанс](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/chto-takoe-renessans/)» начался не семьсот лет тому назад, а каких-то полтораста - в середине 19-го века. А буквально до начала века двадцатого люди спокойно жили среди руин, как нас уверяют историки, лет полтыщи, дожив в таких условиях до эпохи фотодокументалистики:

**English:**

These are not battle reenactors dressing up as Prince Oleg's forces - these are Caucasian dzhigits on the eve of the 20th century. Has anyone ever told us that a hundred years ago, Europe really fought with bows and arrows, dressed in armor a la "short chain mail" from Eisenstein's films? And now archaeologists are digging out bone shanks from those arrows, and a lot of other useful things from the last century. Well, well, to Hell with them; let them dig. If only they wouldn't get in the way of knowledge, attacking those who find something which doesn't fit into the Procrustean bed provided by professional historians. You have to squeeze your eyes shut not to notice that the "[Renaissance](http://www.clumba.su/chto-takoe-renessans/)" began not seven hundred years ago, but a hundred and fifty years ago - in the middle of the 19th century. But historians assure us that people literally lived quietly among the ruins for half a millennium, living in these conditions until the era of photo-documentation at the beginning of the twentieth century.

<!-- Local
[![](prehistoric_technology_files/Italia-jilye-ruiny-228x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/chto-mne-nasheptali-ruiny/italia-jilye-ruiny/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/Italia-jilye-ruiny-228x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/chto-mne-nasheptali-ruiny/italia-jilye-ruiny/)

**Russian:**

*Италия. Жилые руины italia jilye ruiny*

**English:**

*Italy. Residential ruins*

**Russian:**

Вот как это могло выглядеть внутри сохранившихся дворцов:

**English:**

This is what it might have looked like inside the surviving palaces:

<!-- Local
[![](prehistoric_technology_files/ruiny-novye-hoziaeva-300x217.jpeg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ruiny-novye-hoziaeva/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/ruiny-novye-hoziaeva-300x217.jpeg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ruiny-novye-hoziaeva/)

**Russian:**

*Новые хозяева*

**English:**

*New hosts*

**Russian:**

А нам впаривают про средневековых рыцарей-королей, наследных принцев и их вассалов в неприступных замках. Профессионализм историков как в той басне: *«А видел ли слона?»...* *Да разве там он? «там!» ну, братец, виноват:* *Слона-то я и не приметил.* - разглядев культуры «шаровидных амфор», шнуровых черепков и прочей пузатой мелочи, которую левой пяткой можно сваять в любом сарае, посмотрите, к примеру реакцию профи на «[камни Ики](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/krupnejshee-otkrytie-arxeologii-xx-veka-bojkot/)«, они в упор не видят пирамид в Европе. Но, назло дармоедам от истории, есть артефакты, которые позволяют говорить не просто о допотопной культуре, а о цивилизации, возможно не одной, а сменяющей одна другую, которые обладали технологиями, которыми покуда не обладает цивилизация современная.

**English:**

And to us - steamed about medieval knights-kings, the crown princes and their vassals in impregnable castles - the professionalism of historians is like that fable: 

> Did you see the elephant?... There! Well, brother, it's my fault: I didn't even notice the elephant.

having discerned the culture of "spherical amphorae", shaped shards and other small stuff, which can be made in any barn with the left hand. Look, for example, at the reaction of pros to "[the stones of Ika](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/krupnejshee-otkrytie-arxeologii-xx-veka-bojkot/), in which they absentmindedly see pyramids in Europe. But, in spite of the history donors, there are artifacts which allow us to speak not only about the pre-flood culture, but about a civilization, and probably not one but many succeeding one another, which possessed technologies which the modern civilization does not possess so far.

**Russian:**

#### Культура полигональной кладки

**English:**

#### Polygonal masonry culture

**Russian:**

Полигональная кладка - искусство подгонять камни произвольного размера и формы без зазоров. Одно из таких мест, где можно видеть этот вид кладки - храм Аполлона в Дельфах в Древней Греции:

**English:**

Polygonal masonry is the art of fitting stones of arbitrary size and shape without gaps. One such place where you can see this type of masonry is the Temple of Apollo at Delphi in ancient Greece:

<!-- Local
[![](prehistoric_technology_files/poligonalnaia-kladka-grecia-300x165.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-grecia/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/poligonalnaia-kladka-grecia-300x165.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-grecia/)

**Russian:**

*Полигональная кладка. Греция*

**English:**

*Polygonal masonry, Greece*

**Russian:**

В таком случае, поступая как профи поступают с глиняными черепками, мы должны разнести границы древней Греции на все континенты и части света, где живут люди. Вот только в Австралии, вроде бы она не встречается. А так - по всему Земному шару. Вот «Древняя Греция» под Петербургом:

**English:**

In that case, doing as pros do with clay crocks, we should spread the borders of ancient Greece to all continents and inhabited parts of the world. Except it doesn't seem to be found in Australia. It's all over the globe. Here's "Ancient Greece" near St. Petersburg:

<!-- Local
[![](prehistoric_technology_files/poligonalnaia-kladka-peterburg-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-peterburg/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/poligonalnaia-kladka-peterburg-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-peterburg/)

**Russian:**

*Полигональная кладка. С-Петербург*

**English:**

*Polygonal masonry, St. Petersburg*

**Russian:**

В Петербурге и его окрестностях полигональной кладки - завались, и в разной технике исполнения. Япония:

**English:**

There is plenty of polygonal masonry in St. Petersburg and its suburbs and, with different techniques, Japan:

<!-- Local
[![](prehistoric_technology_files/pligonalnaia-kladka-japnia-300x198.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kusko/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/pligonalnaia-kladka-japnia-300x198.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/pligonalnaia-kladka-japnia/)

**Russian:**

*Полигональная кладка. Япония*

**English:**

*Polygonal masonry, Japan*

<!-- Local
[![](prehistoric_technology_files/kusko-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kusko/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kusko-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kusko/)

**Russian:**

*Америка, Перу, Куско*

**English:**

*Cusco, Peru, South America*

**Russian:**

Отличается от европейской кладки, но это нормально - это как «кубок с утолщенным» дном отличается от «колоколовидных кубков» в «шнуровой керамике». Сотни, если не тысячи специалистов по всему миру ломают головы - как такое можно сделать?! Размягчение материала или литье мало чего дают - не буду здесь вдаваться в подробности, кто заинтересуется - интернет в помощь. Замечу только насчет литья, вот пример кладки:

**English:**

It differs from the European masonry, but that is normal - just as a "cup with thickened" bottom differs from the "bell-shaped cups" in "corded ceramics". Hundreds, if not thousands of specialists around the world are racking their brains - how can this be done! I will not go into details here, but if you are interested, the Internet is your guide. I will only note about casting, here is an example of masonry:

<!-- Local
[![](prehistoric_technology_files/poligon-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligon/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/poligon-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligon/)

**Russian:**

*Полигональная кладка из натурального камня*

**English:**

*Polygonal natural stone masonry*

**Russian:**

Если это литье, то явно видно стремление к имитации натурального камня. Очень убедительая имитация. Вопрос: каковы мотивы, какова логика подобных действий? Кто в танке: для каждого камня нужна индивидуальная литейная форма, т.е., практически, невероятно что это не натуральный камень. Так что, по крайней мере, иногда строили не литейными технологиями. Мало того, как вообще подогнать не регламентированные ни каким нормативом плоскости, так, оказывается. как будто в подначку - а вам слабо - иногда они еще эти сопрягаемые плоскости делали одну желобком, а сопрягаемую с ней - выпуклой:

**English:**

If it is cast, one can clearly see the desire to imitate natural stone. It is a very convincing imitation. The question is: what are the motives, what is the logic behind such actions? For every stone in this assembly, each stone needs an individual mold, which means it is almost impossible that this is not natural stone. So at least these were sometimes built using techniques that did not involve casting stone. Not only how to fit the planes together as the stone turned out when unregulated by any standard. As if on a subtext - and you slave - sometimes they still made these conjugate planes with one groove, and joined to it convex polygonal masonry.

<!-- Local
[![](prehistoric_technology_files/poligonalnaia-kladka-01-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-01/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/poligonalnaia-kladka-01-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-01/)

**Russian:**

*Полигональная кладка*

**English:**

*Polygonal masonry*

**Russian:**

И как такая идея, а это не мысль обмотать сырой горшок веревкой, могла прийти в головы людям в разных концах света? Почему эта мысль никак ни кому не прийдет в голову в современной цивилизации? Почему мы сейчас не можем делать такого? Да потому, что мы не из той цивилизации, мы, скорее - из культуры «шнуровой керамики» - достаточно примитивно мыслящие и слишком много о себе мнящие. Вот недавно на Урале стенку, выложенную полигональной кладкой, [нашли  вот в этом овраге](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://re22ka.livejournal.com/207892.html) на берегу реки Чусовой:

**English:**

And how could such an idea, as wrapping a damp pot with a rope, emerge in the heads of people all over the world? Why does this idea not occur to anyone in modern civilization? Why can't we do that now? Well, because we are not from that civilization, we are rather - from the culture of "corded ceramics" - primitive in our way of thinking and with too much imagination. Here recently, a wall lined with polygonal masonry was [found in this ravine] (https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://re22ka.livejournal.com/207892.html) on the bank of the Chusovaya River in the Urals:

<!-- Local
[![](prehistoric_technology_files/ovrag-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ovrag/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/ovrag-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/ovrag/)

**Russian:**

*Овраг*

**English:**

*The ravine*

**Russian:**

Вот фрагмент кладки стены:

**English:**

Here's a fragment of the masonry wall:

<!-- Local
[![](prehistoric_technology_files/poligonalnaia-kladka-ural.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-ural/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/poligonalnaia-kladka-ural.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonalnaia-kladka-ural/)

**Russian:**

*Полигональная кладка. Урал*

**English:**

*Polygonal masonry, Chusovaya River, Urals*

**Russian:**

То что камни прямоугольной формы, сути дела не меняет - камни разного размера подогнать без зазоров задача не намного проще, чем подогнать криволинейные поверхности. И что? - Научная сенсация? - Где толпы журналюг, охочих на сенсацию, где те самые археологи, что так тщательно просеивают «культурные слои» в поисках поломанных пуговиц? Нет их. Да для «исторической науки» это не может быть сенсацией - она изучает [искусственно созданную мифологию](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/istoriya-kak-mif/). А что любители Атлантиды, которых в мире больше чем поклонников олимпийских игр? Им тоже не с руки - сказка кончилась. Геродот, первый описатель Атлантиды сам был историком, и выдуман историками, и этим все сказано. А искомая Атлантида она повсюду - на всех материках оставила свои неизгладимые следы - стоит только расшнуровать глаза и от шнуровой культуры чуть-чуть отвлечься. Ну и, на закуску, вот еще пример допотопной технологии, которую учудили на полигональной кладке, этого в наше время, точно, не сделать:

**English:**

The fact that the stones are rectangular does not change the matter: to fit together different sized stones without gaps is not much easier than to fit curvilinear surfaces. So? Scientific sensation? Where are the crowds of sensation-hungry journos and the archaeologists who sift through the "cultural layers" for broken buttons? There are none. Yes, for "historical science" it cannot be a sensation: it studies [artificially created mythology](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/istoriya-kak-mif/). And what about fans of Atlantis - of whom there are more fans than of the Olympic Games? They can't help it either - the tale is over. Herodotus, the first Atlantis-describer, was a historian himself, and was made up by historians, and that says it all. And the sought-after Atlantis is everywhere. On all continents it left its indelible traces. And, for starters, here is another example of prehistoric technology, which was created as polygonal masonry, which cannot be replicated today:

<!-- Local
[![](prehistoric_technology_files/poligonal-rasplavlen.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonal-rasplavlen/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/poligonal-rasplavlen.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/poligonal-rasplavlen/)

**Russian:**

*оплавленная полигональная кладка*

**English:**

*Molten polygonal masonry*

**Russian:**

Три стены, одна за другой, были размягчены неким воздействием, вдоль прямой перпендикулярной стенам...

**English:**

Three walls one after the other were softened by some kind of impact along a straight line perpendicular to the walls...

**Russian:**

#### Цивилизация пещерных  технологий

**English:**

#### Caveman civilization

**Russian:**

В середине нулевых по интернету шел вал обсуждений противоречащих науке археологических артефактов - о железных гвоздях в граните и даже о молотке в куске породы, золотой цепочке «вмороженной» в кусок каменного угля. Но кто подтвердит, что это имело место быть на самом деле. Тогда я задумался: а нет ли следов неопровержимых. Стал искать: первая мысль была Великие Пирамиды, но тему так замылили, что связываться было бесполезно, но  пирамиды вывели меня на огромный гранитный обилиск, лежащий в каменоломнях Асуана. И вот тогда, пять лет назад, я написал первую статью - «[Непредсказуемое прошлое](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/nepredskazuemoe-proshloe/)» - об альтернативном взгляде на историю. Вот фото оттуда:

**English:**

In the mid-noughties there was a flood of discussion on the Internet about archaeological artefacts that contradicted science: about iron nails in granite and even about a hammer in a piece of rock, a gold chain "frozen" in a lump of coal. But who will confirm that these finds really took place? Then I began to think: is there any irrefutable trace? I started to look: the first idea was the Great Pyramids. However, the topic was so washed out that it was useless to mess with them, but the pyramids led me to a huge granite cliff lying in the quarries of Aswan. And that's when, five years ago, I wrote my first article: [The Unpredictable Past](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/nepredskazuemoe-proshloe/) about an alternative view of history. Here's a photo from there:

<!-- Local
![](prehistoric_technology_files/obelisk.jpg#resized)
-->

![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2010/05/obelisk.jpg#resized)

**Russian:**

*Обилиск в Асуане*

**English:**

*Obilisk in Aswan*

**Russian:**

И вот, буквально сейчас, [подкатилась статья](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://vaduhan-08.livejournal.com/28067.html) , связанная с тем самым обилиском и связанными с ним технологиями. Дело в том, что по всему миру имеются искусственно созданные пещеры, такие как эта, в Китае:

**English:**

And now, literally just now, [an article has popped up](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://vaduhan-08.livejournal.com/28067.html) about the obilisk and related technology. The fact is that there are artificially created caves all over the world, such as this one in China:

<!-- Local
[![](prehistoric_technology_files/grot-longyou-300x187.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grot-longyou/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/grot-longyou-300x187.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grot-longyou/)

**Russian:**

*искусственный грот Лонгью*

**English:**

*Longue's artificial grotto*

**Russian:**

Все они имеют явные следы машинной обработки, вот фрагмент предыдущей фотографии:

**English:**

They all have obvious signs of machine processing. Here's a snippet from the previous photo:

<!-- Local
[![](prehistoric_technology_files/grot-fragment.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grot-fragment/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/grot-fragment.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grot-fragment/)

**Russian:**

*Грот. След горнопроходческой фрезы*

**English:**

*Grotto - the footprint of a mining cutter*

**Russian:**

В наши дни подобные следы оставляет вот такая горнопроходческая фреза:

**English:**

Nowadays, a quarry cutter like this leaves similar traces:

<!-- Local
[![](prehistoric_technology_files/gornoprohod-kombain-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/gornoprohod-kombain/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/gornoprohod-kombain-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/gornoprohod-kombain/)

**Russian:**

*Фреза для горной выработки*

**English:**

*Quarry cutter*

**Russian:**

Если в «глубокой древности» пользовались чем-то подобным, то можно попутно считать закрытым вопрос - откуда у «древних» строителей брался мелко искрошенный камень для отливки мегалитов - скажем того же постамента для «медного всадника» в С-Петербурге или Александровской колонны и прочего [в городе, где на каждом шагу литье](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/a-u-nas-otlivali-v-granite/). Подобные пещерные технологии можно встретить и у нас в Крыму, смотрите по ссылкам в конце статьи. Так вот, обилиск в Асуане, замечателен тем, что на нем продемонстрированы все «пещерные»  технологии в одном месте. И там есть такие штуки, которые, опять же, современного специалиста ставят в тупик. Вот фрагмент, на котором следы явно машинной обработки и следы сделанные, так, как если бы это долбили вручную зубилом:

**English:**

If something similar was used in "deep antiquity", then we can consider as incidentally closed the question: where did the "ancient" builders get finely milled stone for casting megaliths? For example, the pedestal for the "Bronze Horseman" in St. Petersburg, or St Petersburg's Alexander Column, and others [in the city, where there is casting at every step](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/a-u-nas-otlivali-v-granite/). And there are some things that, again, would puzzle a modern specialist. Here is a fragment, with traces of machining and traces made as if they were chiseled by hand:

<!-- Local
[![](prehistoric_technology_files/Granit-sledy-obrabotli-201x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-sledy-obrabotli/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/Granit-sledy-obrabotli-201x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-sledy-obrabotli/)

**Russian:**

*Сdwледы обработки гранита в Асуане*

**English:**

*Traces of granite processing in Aswan*

**Russian:**

А вот следы выработки совсем другого рода:

**English:**

But the traces of mining are of a completely different kind:

<!-- Local
[![](prehistoric_technology_files/granit-miagkii-asuan-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-miagkii-asuan/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/granit-miagkii-asuan-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-miagkii-asuan/)

**Russian:**

*Следы обработки гранита в Асуане*

**English:**

*Traces of granite processing in Aswan*

**Russian:**

Или:

**English:**

Or:

<!-- Local
[![](prehistoric_technology_files/granit-lopatoi-199x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-lopatoi/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/granit-lopatoi-199x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/granit-lopatoi/)

**Russian:**

*Следы обработки гранита*

**English:**

*Traces of granite processing*

**Russian:**

Словно это не гранит, а мокрый песок, который выгребали лопатой. Что это за технология - неизвестно. «Ученые» утверждают, что это выдолблено с помощью зубил, по которым били булыжниками. Вот такими, как в руках этого улыбчивого туриста:

**English:**

As if it was not granite, but wet sand which was shovelled out with a spade. We don't know what technology could have done this. "Scientists" claim that it was hollowed out with chisels that were hit with cobblestones. Like the one in the hands of this smiling tourist:

<!-- Local
[![](prehistoric_technology_files/turist-v-kamenolomne-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/turist-v-kamenolomne/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/turist-v-kamenolomne-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/turist-v-kamenolomne/)

**Russian:**

*Эй, ухнем*

**English:**

*Hey, whoa.*

**Russian:**

Но дело в том, что там есть такие узкие места, что если туда втиснуться, то обратно самостоятельно уже никак - только за ноги выдергивать. И никакой современный механизм там не поместится.

**English:**

But the thing is that there are such bottlenecks that if you squeeze into it, you can't get back on your own - you have to pull it out by your feet. And no modern mechanism will fit there.

<!-- Local
[![](prehistoric_technology_files/grnit-uzkoe-mesto-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grnit-uzkoe-mesto/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/grnit-uzkoe-mesto-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/grnit-uzkoe-mesto/)

**Russian:**

*Выработка в граните*

**English:**

*Excavation in granite*

**Russian:**

Как это сделать -  разумного объяснения нет. Но это сделано. Пещерными людьми. В заключение этого раздела фото из Крыма:

**English:**

There is no reasonable explanation for how it was done. But it was done. By cavemen. To conclude this section, a photo from the Crimea:

<!-- Local
[![](prehistoric_technology_files/inkerman-krym-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/inkerman-krym/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/inkerman-krym-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/inkerman-krym/)

**Russian:**

*Инкерман*

**English:**

*Inkerman*

**Russian:**

Это, говорят, монахи зубилом выбили. Ага, и миллионы тонн еще под землей... Что за прикол у тех кто долбил делать проем такой высоты? Телега, при такой высоте груза и ширине колеи, непременно перевернется на дороге. Зачем такой высокий проход? Мы никак не можем проверить сообщения вроде этого: «*Исследователи из Австралии среди разнообразных останков обнаружили окаменелый коренной зуб. Его высота составляла 6,7, а ширина - 4,2 сантиметра. Обладатель зуба такого размера имел рост как минимум 7,5 метра и вес - 370 килограммов*» - таких сообщений, иногда очень правдоподобных - масса. Но как это проверить? А вот проход для таких людей мы видим, таких примеров можно собрать массу: в том же Исаакии в Питере, несуразно большие двери, Все это, нехудо бы, как-то объяснить.

**English:**

They say monks chiselled it out. Yeah, and millions of tons more underground... What trick did those chisellers used to make an opening this high? A cart suitable for that height and width of track would surely overturn on the road. Why such a high aisle? There's no way we can verify reports like this:

> Researchers from Australia found a fossilized molar among a variety of remains. It was 6.7 inches high and 4.2 inches wide. The owner of a tooth of this size was at least 7.5 meters tall and weighed 370 kilograms.

Such reports are sometimes very plausible. But how to verify them? And here is a passage for people like this. We can gather a lot of examples like these: in the same Isaac's in St. Petersburg there are absurdly large doors. It would be good to explain all this somehow.

**Russian:**

#### Великоканальная культура

**English:**

#### Great canal culture

<!-- Local
[![](prehistoric_technology_files/kanal-v-kitae-300x209.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-v-kitae/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kanal-v-kitae-300x209.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-v-kitae/)

**Russian:**

*Великий китайский канал*

**English:**

*The Great Chinese Canal*

**Russian:**

Искусственная водная артерия - Великий Китайский канал. Протяженность 1782 километра. Во Вьетнаме большая часть территории вообще покрыта сетью каналов:

**English:**

The Great China Canal artificial waterway. It is 1,782 kilometers long. In Vietnam, most of the territory in general is covered by a network of canals:

<!-- Local
[![](prehistoric_technology_files/kanaly-vietnam-300x155.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanaly-vietnam/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kanaly-vietnam-300x155.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanaly-vietnam/)

**Russian:**

*kanaly-vietnam*

**English:**

*Vietnamese canals*

**Russian:**

kanal-vietnam

**English:**

Vietnamese canal

<!-- Local
[![](prehistoric_technology_files/kanal-vietnam.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-vietnam/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kanal-vietnam.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-vietnam/)

**Russian:**

Каналы проложены как по линеечке, Вот здесь длина прямолинейного участка достигает 45 км:

**English:**

The canals are laid out like rulers; here the length of the straight section is 45 km:

<!-- Local
[![](prehistoric_technology_files/kanal-vietnam-dlina.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-vietnam-dlina/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kanal-vietnam-dlina.png#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-vietnam-dlina/)

**Russian:**

*Каналы во Вьетнаме*

**English:**

*Canals in Vietnam*

**Russian:**

Это Вьетнам. Тут живут вьетнамцы:

**English:**

This is Vietnam. The Vietnamese live here:

<!-- Local
[![](prehistoric_technology_files/vietnamcy-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/vietnamcy/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/vietnamcy-300x200.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/vietnamcy/)

**Russian:**

*Вьетнамцы*

**English:**

*Vietnamese*

**Russian:**

Это их упорным трудом проложены тысячи километров этих уникальных каналов. Для сравнения. Сейчас Китай строит канал в Никрагуа. Длина 278 км, Строить будет около миллиона двухсот тысяч человек, из них 200 тысяч - на бульдозерах, скреперах и экскаваторах непосредственно в зоне русла канала. А вот в СССР, поставлен уникальный эксперимент: там так же, с кирками и тачками между 1931 и 1933 годами построили канал длиной 227 км менее чем за два года:

**English:**

It is their hard work that has paved thousands of kilometers of these unique canals. For comparison, China is currently building a canal in Nicaragua. The length is 278 km. About one million, two hundred thousand people are going to build it. 1,200,000 of them, with bulldozers, scrapers, and excavators working directly in the channel bed. But in the USSR, a unique experiment was carried out. Using picks and wheelbarrows between 1931 and 1933, a 227 km long canal was built in less than two years:

<!-- Local
[![](prehistoric_technology_files/belomor-300x244.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/belomor/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/belomor-300x244.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/belomor/)

**Russian:**

*На строительстве Беломор-канала*

**English:**

*Construction site of the White Sea Canal*

**Russian:**

Численность строителей не превышала 126 тысяч человек. Срваните: китайцы собираются уложиться в 5 лет - начало эксплуатции и 15 лет - полное завершение строительства. Более миллиона строителей с техникой - 15 лет, СССР почти в десять раз меньше строителей - менее двух лет! Без экскаваторов! Т.е. СССР тех лет, кмким-то образом, вписался в ту древнюю цивилизацю. И не рассказывают нам историки, что произошло с человечеством за каких-то полвека, что эти уникальные навыки и технолгии были безвозвратно утрачены!

**English:**

The number of builders did not exceed 126,000 people. Try comparing them: the Chinese are going to meet a five-year deadline for the canal to start being used and a 15-year deadline for full completion. More than one million builders with equipment require 15 years; the USSR has almost ten times fewer builders and they required less than two years! Without excavators! I.e. the USSR of those years, somehow, matched that ancient civilization. And historians do not tell us what happened to mankind that in half a century these unique skills and technologies were irrevocably lost!

<!-- Local
[![](prehistoric_technology_files/Kanal-suvorov-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-suvorov/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/Kanal-suvorov-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kanal-suvorov/)

**Russian:**

*Канал Телатайпале, вид с моста*

**English:**

*Telataipale canal, view from the bridge (possibly Suvorov Canal)*

**Russian:**

А это один из каналов, проложенных под руководством А.В.Суворова в Финляндии. Суворов за семь лет там нарыл этих каналов, столько, что теперешним китайцам с их экскаваторами и бульдозерами за сто лет не построить. В Америке уникальной сетью каналов покрыта  вся местность за косой Атлантик-Сити, все побережье залива Дэлавэр, все побережье Северной и Южной Каролины и далее на юг до Флориды:

**English:**

And this is one of the canals constructed under the leadership of A.V. Suvorov in Finland. In seven years Suvorov dug so many canals there, that nowadays the Chinese with their excavators and bulldozers could not build them in a hundred years. In America, a unique network of canals covers the entire area behind the spit of Atlantic City, the entire coast of Delaware Bay, the entire coast of North and South Carolina and further south to Florida:

<!-- Local
[![](prehistoric_technology_files/kanaly-v-amerike-300x157.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/otdavaj-ka-zemlicu-ameriku-otdavaj-ka-rodimuyu-vzad/kanaly-v-amerike/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/03/kanaly-v-amerike-300x157.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/otdavaj-ka-zemlicu-ameriku-otdavaj-ka-rodimuyu-vzad/kanaly-v-amerike/)

**Russian:**

*каналы в Америке*

**English:**

*Canals in America*

**Russian:**

Строили их в доэкскаваторную эпоху: если б они копались как при строительстве Панамского канала, то ушла бы на то не одна тысяча лет... К той же великоканальной культуре следует отнести и строительство грандиозных земляных насыпей, таких как Великая заволжская стена, которая протянулась более чем на две с половиной тысячи километров, при высоте в пять метров и ширине в 70 метров и рядом ров глубиной около 3 метров и шириной 10 метров:

**English:**

They were built in the pre-excavator era. If they were dug as canals were dug during the construction of the Panama Canal, it would take more than one thousand years to build them... To the same great canal culture should be attributed the construction of grandiose earth embankments, such as the Great Wall of the Volga, which stretched for more than two and a half thousand kilometers, with a height of five meters and a width of 70 meters and a nearby ditch about 3 meters deep, and with a width of 10 meters:

<!-- Local
[![](prehistoric_technology_files/zavljskaia-stena-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/zavljskaia-stena/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/zavljskaia-stena-225x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/zavljskaia-stena/)

**Russian:**

*Великая Заволжская стена*

**English:**

*Great Zavolzhskaya Wall*

**Russian:**

Ну и прибавьте сюда тысячи километров знаменитых Змиевых валов...

**English:**

And add in here the thousands of kilometers of the famous [Zmiev walls (Serpent Walls)...](https://www.globalsecurity.org/military/world/ukraine/serpent-walls.htm)

**Russian:**

#### Кремлевская культура

**English:**

#### Kremlin culture

**Russian:**

Каменный век - век строительства из натурального камня закончился  переходом на массовое строительство вначале из кирпича, а затем и других видов искусственного камня. Историки утверждают, что в гражданском строительстве массово кирпич начали применять только в 18-м веке: жилых и хозяйственных построек из кирпича более раннего времени попросту нет. Но вот кремли и монастыри, как утверждают историки, строили из кирпича задолго до 18-го века: Московский - 1485 - 1495 годы, Новгородский - 1484 -1490, Нижегородский - 1500 - 1512, т.е. тринадцатый век, это почти за пятьсот лет до начала  гражданского строительства из кирпича. Т.е по версии историков, в 13-м веке кому-то в башку пала идея: хватит ворочать тяжелые камни, давайте построим кремль из кирпича! Кремль - это миллионы кирпичей, это кустарщиной не сделать! Откроем завод, наберем работников, построим кремль, потом завод прикроем, работников   -  под зад коленкой - пусть с голоду дохнут! - Примерно такая картина вырисовывается, если поверить во все эти «древние» кремли. Логичной выглядит другая последовательность: вначале новый материал был опробован в бытовом строительстве, отрабатывались технологии, приемы работы, изучалась стойкость нового материала, в конце-концов нужно было знать сколлько времени займет строительство - нужно накопить опыт, короче, а потом уже строить гигантские городские и монастырские стены. В начале 60-х годов реставрировали Нижегородский кремль, при этом изучили строение и для реставрации создали чертежи, вот один из разрезов:  

**English:**

The Stone Age - the age of construction with natural stone - ended with the transition to mass construction, first with bricks and then with other types of artificial stone. Historians argue that brick started to be used in civil construction en masse only in the 18th century, and that there are simply no residential and commercial brick buildings from before that time. But the Kremlin and monasteries, according to historians, were built of brick long before the 18th century: the Moscow Kremlin in 1485-1495, Novgorod in 1484-1490, Nizhny Novgorod in 1500-1512. Ie the thirteenth century, which is almost five hundred years before the use of bricks in civil construction. So, according to historians, in the 13th century somebody had an idea: stop turning over heavy stones, let us build the Kremlin of bricks! The Kremlin will require millions of bricks, it cannot be built from hand-crafted bricks! Let's open a factory, hire employees, build the Kremlin, then shut down the factory and let them starve to death! Such a vivid picture emerges, if you believe in this "ancient" Kremlin. Another sequence looks logical: first the new material was tested by using it in domestic construction. That is: the technologies were tested, along with the working methods and the durability of the new material. In the end it was necessary to know long the construction would take by gaining experience. And then to build a giant city and monastery walls. In the early 60s the Nizhny Novgorod Kremlin was restored, the building was studied and drawings were made for the restoration. Here is one of the sections:

<!-- Local
[![](prehistoric_technology_files/kreml-chertej-274x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-chertej/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/kreml-chertej-274x300.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-chertej/)

**Russian:**

*Разрез кремлевской стены*

**English:**

*Cutting of the Kremlin wall*

**Russian:**

Строительство такого грандиозного сооружения как кремль, без чертежей немыслимо. Ну не мог же итальянский зодчий давать указания типа: копать от меня и до следующего дуба! В Новгороде эпохи строительства кремля для письма пользовались берестой. Так вот сколько возов бересты использовали те итальянские архитекоторы, что строили кремли в России?! И где хоть какие-то следы - переписка горожан на бересте сохранилась, а хотя бы один чертежик посмотреть как это на бересте будет выглядеть! Да никак: кирпичи для ответственного строительства имели заводскую маркировку - завод и год изготовления, никакая кустарщина здесь не допускалась:

**English:**

Building such a grandiose structure as the Kremlin is unthinkable without blueprints. After all, an Italian architect could not give instructions such as: "dig from me to the next oak tree!" In Novgorodi, during the Kremlin construction era, they used birch bark for writing. So how many wads of birch bark were used by those Italian architects who built the Kremlin in Russia? And where might at least some traces be preserved? For example, the correspondence with the townspeople on birch bark, or perhaps at last one drawing ion birch bark that showed how it would look! Yet there is no way even though - like any responsible construction - bricks were marked with the factory markings and the year of manufacture, and no handmade bricks were allowed:

<!-- Local
[![](prehistoric_technology_files/muzei-kirpicha-300x215.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/muzei-kirpicha/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/muzei-kirpicha-300x215.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/muzei-kirpicha/)

**Russian:**

*Музей кирпича в Санкт-Питербурге*

**English:**

*Brick Museum in St. Petersburg*

**Russian:**

Реставраторы Нижегородского кремля установили, что кирпич был маркирован 1785 годом, изготовлен Балахнинским заводом, который располагался недалеко от Нижнего, выше по течению. Таким образом: каменный век закончился в 18-м веке, кремли построили в конце 18-го, начале 19-го. Но, самое главное: все эти кремли, постройки конца 18-го века похожи один на другой как разные горшки шнуровой керамики. И местоположение этих «кубков с утолщенным дном» показывает ареал «кремлевской культуры», а по факту - границы Российской империи. Кремль строился не для украшения - это оборонительное сооружение, это - форпост и на вражеской территории строить его никто не позволит и секретами не поделится. Вот рядом две башни - одна крупнейшего в империи - московского Кремля, вторая башня кремля одной из южных провинций:

**English:**

Restorers of the Nizhny Novgorod Kremlin found bricks marked '1785', made by Balakhna factory, which was located upstream not far from Nizhny. Thus: the Stone Age ended in 18th century, the Kremlins were built in late 18th, early 19th. But, most importantly: all these kremlins, buildings of the late 18th century resemble each other like different pots of corded pottery. And the locations of those "cups with thickened bottoms" corresponds with the area of "Kremlin culture", and in fact - with the borders of the Russian Empire. The Kremlin was not built for decoration: it is a defensive construction, it is an outpost and was not built on enemy territory and share its secrets. Here are two towers next to each other: one is the largest in the empire - the Kremlin in Moscow - and the other is a tower from the Kremlin of one of the southern provinces:

<!-- Local
[![](prehistoric_technology_files/kreml-moskva-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-moskva/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/kreml-moskva-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-moskva/)

**Russian:**

*Кремлевские башни*

**English:**

*Kremlin Towers*

**Russian:**

Вот и еще один кирпичик в [ответ НТВ-шникам](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/armageddon/):

**English:**

Here's another brick in [answer to NTV people](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/armageddon/):

**Russian:**

> Русской нации не существует? Наша водка - польская, Кремль - итальянский, а мат - отборный санскрит. Так что же у нас своего?

**English:**

> The Russian nation doesn't exist? Our vodka is Polish, the Kremlin is Italian, and the language is selected Sanskrit. So what do we have of our own?

**Russian:**

А своего  у нас, как оказалось, русский в доску 

> «Алекса́ндр Васи́льевич Суво́ров (Суворов-Рымникский (с 1789), Суворов-Италийский (с 1799)) (24 ноября 1730[1] - 18 мая 1800) - великий русский полководец, военный теоретик, национальный герой России. Князь Италийский (1799), граф Рымникский (1789), граф Священной Римской империи, **принц Сардинского королевского дома»

и было то королевство Сардиния, стало быть, русской провинцией! До потопа. И так: южная граница - современная Италия, северная - Новгород Великий:

**English:**

And his own, as it turned out, Russian on the plaque:

> Alexander Vasilyevich Suvorov (Suvorov-Rymniksky (since 1789), Suvorov-Italian (since 1799)) (24 November 1730[1] - 18 May 1800) was a great Russian commander, military theorist, and national hero of Russia. Prince of Italy (1799), Count of Rymnik (1789), Count of the Holy Roman Empire, Prince of the Royal House of Sardinia 

And that kingdom of Sardinia was, therefore, a Russian province! Before the Flood. And so, Russia's southern border was modern Italy and its northernwas Novgorod the Great:

<!-- Local
[![](prehistoric_technology_files/kreml-novgorod-300x203.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-novgorod/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/kreml-novgorod-300x203.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-novgorod/)

**Russian:**

*Кремль в Новгороде*

**English:**

*The Kremlin in Novgorod*

<!-- Local
[![](prehistoric_technology_files/kreml-smolensk-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-smolensk/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/kreml-smolensk-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-smolensk/)

**Russian:**

*западная - Смоленск: Кремль в Смоленске*

**English:**

*Western: the Kremlin in Smolensk*

**Russian:**

восточная - Тобольск Кремль в Тобольске

**English:**

Eastern: the Kremlin in Tobolsk

<!-- Local
[![](prehistoric_technology_files/kreml-tobolsk-300x224.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-tobolsk/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/kreml-tobolsk-300x224.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/kreml-tobolsk/)

**Russian:**

*Везде единый «итальянский» проект!*

**English:**

*A single "Italian" project everywhere!*

**Russian:**

#### Дополнительно

**English:**

#### Extras

**Russian:**

Это современные раскопки на территории Кремля, обнаружены срубы, это один из них:

**English:**

This is a modern excavation on the territory of the Kremlin. Log cabins were found; this is one of them:

<!-- Local
[![](prehistoric_technology_files/srub-v-kremle-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/srub-v-kremle/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/srub-v-kremle-300x225.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/srub-v-kremle/)

**Russian:**

*Археологические раскопки в Кремле*

**English:**

*Archaeological excavations in the Kremlin*

**Russian:**

А вот эти срубы в альбоме Мейерберга (1622 - 1688):

**English:**

And these are the log cabins in Meyerberg's album (1622-1688):

<!-- Local
[![](prehistoric_technology_files/Sruby-v-Kremle-300x227.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/sruby-v-kremle/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/Sruby-v-Kremle-300x227.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/sruby-v-kremle/)

**Russian:**

*Срубы в Кремле не покрыты культурным слоем*

**English:**

*The log houses in the Kremlin were not covered with a cultural layer*

**Russian:**

Ну и стены, вокруг этих срубов окружала другая стена, на которой сейчас стоит [новый Кремль](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/), построенный, по крайней мере, после середины 17-го века, что  и требовалось доказать:

**English:**

Well, the walls around these log cabins were surrounded by another wall, on which now stands [the new Kremlin](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/), built later than the mid-17th century, which it was required to prove.

<!-- Local
[![](prehistoric_technology_files/Stena-pod-kremlem-300x201.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/stena-pod-kremlem/)
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/Stena-pod-kremlem-300x201.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/kulturnye-sloi-evrazii/stena-pod-kremlem/)

**Russian:**

*Раскопки Кремля*

**English:**

*Excavations of the Kremlin*

**Russian:**

### Пирамидная культура

**English:**

### Pyramid culture

<!-- Local
[![](prehistoric_technology_files/civilizacia-piramid.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/civilizacia-piramid/)  )
-->

[![](https://img.pandoraopen.ru/http://www.clumba.su/wp-content/uploads/2015/04/civilizacia-piramid.jpg#clickable)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/jexr/http://www.clumba.su/dopotopnye-texnologii/civilizacia-piramid/)  )

**Russian:**

*Пирамиды*

**English:**

*Pyramids*

© All rights reserved. The original author retains ownership and rights.

