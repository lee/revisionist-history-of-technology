title: Nuclear Wars of the Past
date: 2015-05-06
modified: Tue 23 Feb 2021 17:24:20 GMT
category:
tags: thermonuclear war; catastrophe; technology; cannons
slug:
authors: vaduhan_08
from: https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/yadernye-vojny-proshlogo/
originally: 50LG0.html
local: 50LG0_files
summary: Strange fuss over old cannonballs... evacuation of residents... they may explode... where they went, we don't know, the military took them away... It's always like this!
status: published

#### Translated from:

[https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/yadernye-vojny-proshlogo/](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/yadernye-vojny-proshlogo/)

Translator's note: vaduhan_08's Livejournal account is suspended at the time of translation so links to his account are currently dead.

**Russian:**

Когда я писал предыдущую статейку про разгром Севастополя в Крымскую войну, то больше писал про пушки и то что они могли наделать своими ядрами [http://vaduhan-08.livejournal.com/37205.html](http://vaduhan-08.livejournal.com/37205.html "http://vaduhan-08.livejournal.com/37205.html"). Однако в последующем, по мере собирания информации про ядра, которыми стреляли пушки тех времен и вообще про те разрушения, которые произошли по всему миру примерно в тоже время — средина 19 века, у меня возникли сомнения, а правильно ли мы воспринимаем ядра, которые могли использоваться для разрушения городов! В средине 19 века прошли чудовищные по своим разрушениям войны, погромы городов, странные пожары которые разрушали города так что из приходилось отстраивать практически заново! Я писал про пожар в Лондоне [http://vaduhan-08.livejournal.com/19681.html](http://vaduhan-08.livejournal.com/19681.html "http://vaduhan-08.livejournal.com/19681.html"), землетресение в Сан-Франциско и всемирные выставки после погромов [http://vaduhan-08.livejournal.com/21931.html](http://vaduhan-08.livejournal.com/21931.html "http://vaduhan-08.livejournal.com/21931.html").

**English:**

When I wrote the previous article about the defeat of Sevastopol in the Crimean War, I wrote more about the cannons and what they could do with their cannonballs [http://vaduhan-08.livejournal.com/37205.html](http://vaduhan-08.livejournal.com/37205.html "http://vaduhan-08.livejournal.com/37205.html").

However, later on, as I gathered information about the cannonballs they were using back then and in general about the destruction that occurred around the world around that time - the mid-19th century - I developed doubts about our perception of these cannonballs. After all, they could destroy cities! In the middle of the 19th century there were wars, pogroms of cities, and strange fires that destroyed cities such that they had to be rebuilt - practically from scratch! I've written about the fire in London [http://vaduhan-08.livejournal.com/19681.html](http://vaduhan-08.livejournal.com/19681.html "http://vaduhan-08.livejournal.com/19681.html"), the earthquake in San Francisco and the world exhibitions after the pogroms [http://vaduhan-08.livejournal.com/21931.html](http://vaduhan-08.livejournal.com/21931.html "http://vaduhan-08.livejournal.com/21931.html").

**Russian:**

Многие авторы жж выкладывают просто шокирующие и очень убедительные материалы про уничтожение городов и вообще разрушение «древней цивилизации» которая как бы и не древняя совсем: [http://wakeuphuman.livejournal.com/1116.html](http://wakeuphuman.livejournal.com/1116.html "http://wakeuphuman.livejournal.com/1116.html").

**English:**

Many alt-history authors put quite shocking and very convincing material about the destruction of cities and the destruction of an "ancient civilization" when this civilisation may not have been ancient at all: [http://wakeuphuman.livejournal.com/1116.html](http://wakeuphuman.livejournal.com/1116.html "http://wakeuphuman.livejournal.com/1116.html"). English translation at: [https://178.62.117.238/destruction-of-ekaterinoslav-dnepropetrovsk-by-a-thermonuclear-explosion-in-1785.html](https://178.62.117.238/destruction-of-ekaterinoslav-dnepropetrovsk-by-a-thermonuclear-explosion-in-1785.html).

**Russian:**

Причины как правило две и зачастую они объединяются вместе — потоп и ядерная война! Как говорится одно другому не мешает…. есть следы потопа и есть воронки! И есть основательно разрушенная «античная» цивилизация по всему миру: [http://vaduhan-08.livejournal.com/37002.html](http://vaduhan-08.livejournal.com/37002.html "http://vaduhan-08.livejournal.com/37002.html")

**English:**

As a rule, two reasons for this destruction are provided - often together. A flood and nuclear war. As they say, one does not rule out the other... there are traces of a flood and there are craters. And there is a thoroughly destroyed "ancient" civilization all over the world: [http://vaduhan-08.livejournal.com/37002.html](http://vaduhan-08.livejournal.com/37002.html "http://vaduhan-08.livejournal.com/37002.html")

**Russian:**

Те кто спорит и не принимает вариант применения ядерного оружия в древности имеют один не оспоримый довод — что бы создать ядерную бомбу нужно создать инфраструктуру, следов которой на земле нет, а если нет значит это не люди…. спорить трудно, с другой стороны, зачем это высокоразвитым инопланетянам нас закидывать килотонными зарядами, делать глубокие воронки и после этого скрыться, пропасть… а кто будет порабощать, кушать вкусное человеческое мясо… ну а зачем же мы им еще то нужны, высокоразвитым? С другой стороны есть удивительные артефакты которые ни какими человеческими технологиями в современном понимании не объяснить: [http://vaduhan-08.livejournal.com/28067.html](http://vaduhan-08.livejournal.com/28067.html "http://vaduhan-08.livejournal.com/28067.html")

**English:**

Those who do not accept the use of nuclear weapons in ancient times have one indisputable argument: to create a nuclear bomb we need to have created an infrastructure, traces of which do not exist on earth, and given that, then the nuclear weapons were not developed by humans... It is difficult to argue, on the other hand, why highly developed aliens [would pelt us with kiloton charges, make deep craters](https://178.62.117.238/tag/thermonuclear-war.html) and then disappear... and who would enslave and eat delicious human meat... and why would these highly developed entities need us? On the other hand, there are amazing artifacts which cannot be explained by any human technologies or modern understanding: [http://vaduhan-08.livejournal.com/28067.html](http://vaduhan-08.livejournal.com/28067.html "http://vaduhan-08.livejournal.com/28067.html")

**Russian:**

Но давайте вернемся к Севастополю, его разрушениям и ядрам!

**English:**

But let's get back to Sevastopol and its destruction - and cannonballs!

**Russian:**

Вот пушка и ядра — как бы все ясно, пушка стреляет ядром в сторону английских кораблей может даже и попадет и может даже что то навредит!

**English:**

Here's the cannon and cannonballs as if everything is straightforward. The cannon fires the cannonball towards the British ships, may even hit them and may even do some damage!

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1066635/1066635_900.jpg#resized)

**Russian:**

Вот например какой ужас бывает когда ядро попадает в дерево!

**English:**

This is how horrible it is when a cannonball hits a tree.

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1095143/1095143_900.jpg#resized)

**Russian:**

Но мы знает что в Крымской войне уже применялись не просто чугунные шары а бомбы — то есть полый шар внутрь которого засыпали дымный (другого ни чего еще не было) порох. Обратите внимание на «дырочку в шаре — там внутреняя резьба, то есть довольно сложное уже изделие!

**English:**

But we know that by the 1853-1856 Crimean War cannons were already firing not just cast-iron balls and bombs. That is, they were shooting a hollow ball filled with gunpowder (there was nothing else). Pay attention to the 'hole' in the ball. It has an internal thread, i.e. it is quite a complex product.

**Russian:**

pushechnye yadra

**English:**

Pushechnye Yadra

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1100620/1100620_900.jpg#resized)

**Russian:**

Khutorskoy (ru)

**English:**

Khutorskoy (ru)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1100285/1100285_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1099672/1099672_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1099261/1099261_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1094858/1094858_900.jpg#resized)

**Russian:**

Есть даже осколки разорвавшихся ядер...

**English:**

There are even fragments of exploded <del>nukes</del> cannonballs...

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1093617/1093617_900.jpg#resized)

**Russian:**

Вроде бы все пока укладывается в простую и ясную логику….однако мне попались фотографии на которые я уже давно много раз смотрел и всегда возникало смутное сомнение — почему все выглядит именно так...

**English:**

Seems like everything so far fits together with simple and clear logic... However, I came across pictures which I've looked at many times and I've always had vague doubts: why does everything looks exactly like this...

**Russian:**

Это разбитый Ричмонд в гражданской войне США...

**English:**

This is damaged Richmond in the U.S. Civil War...

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1083350/1083350_900.jpg#resized)

**Russian:**

А это Севастополь.

**English:**

And this is Sevastopol.

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/593757/593757_900.jpg#resized)

**Russian:**

Это опять америка, 1863 года, все усыпано ядрами, причем как то бес толку просто как бы их рассыпали!

**English:**

This is America again, 1863. Everything is studded with cannonballs, and not as if they were lying where they had been used. They look as if they have been scattered around.

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1083942/1083942_900.jpg#resized)

**Russian:**

А это знаменитая фотография Фентона — дорога смерти, тоже усыпанная ядрами. Зачем? Некоторые коменторы историки признают что Фентон мог специально рассыпать ядра что бы красивее было… вряли конечно, они все таки тяжелые!

**English:**

And this is [Roger Fenton](http://www.allworldwars.com/Crimean-War-Photographs-by-Roger-Fenton-1855.html)'s famous photo of the Road of Death. It is also studded with cannonballs. Why? Some historian commentators admit that Fenton could have scattered them on purpose to make the scene more beautiful... That's unlikely of course, cannonballs are heavy after all!

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/475828/475828_900.jpg#resized)

**Russian:**

Мне на глаза попалась одна фотография греческой крепости с очень похожим антуражем...

**English:**

I came across a picture of a Greek fortress with a very similar entourage...

**Russian:**

rov v kreposti

**English:**

Rov v Kreposti (possibly [Admiralteysky Rov and Kreposti Canal, St Petersburg](https://en.wikipedia.org/wiki/Kronverksky_Strait))

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1101303/1101303_900.jpg#resized)

**Russian:**

Правда ядра здесь каменные, но ведь считается что сначала из пушек стреляли каменными ядрами!

**English:**

True, the cannonballs here are made of stone, but it is thought that the first cannons fired stone cannonballs.

**Russian:**

Но прежде чем перейти к каменным ядрам я хочу дать фото сравнения крупного ядра и картечины!

**English:**

But before I get to stone balls, I want to show a photo comparison of a large cannonball and buckshot.

**Russian:**

ядра

**English:**

Buckshot

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1102758/1102758_900.jpg#resized)

**Russian:**

Ну а теперь перейдем к каменным ядрам…ибо это главная шизо-версия в этой статейке! И натолкнула меня на нее вот эта не замысловатая фотография — пушка и ядра! Прямо макушку бы расцеловал тому умнику, который положил эти ядра рядом с этой пушкой!!!

**English:**

Well, now let's move on to stone cannonballs... for they are the main schizo-version in this article. And I was prompted to it by this simple photo: a cannon and cannonballs. I could kiss the head of the clever guy who put these cannonballs next to this cannon.

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1094567/1094567_900.jpg#resized)

**Russian:**

И еще прекрасное фото в Казани у входа в музей — как говорится из песни слово не выкинешь!

**English:**

And another great photo at the entrance to the museum in Kazan. You couldn't make it up, as they say.

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1102510/1102510_900.jpg#resized)

**Russian:**

Ну а теперь, поехали... настоящие ядра настоящей войны!

**English:**

Now, let's go on... the real cannonballs of a real war.

**Russian:**

Это дробь рассыпали...

**English:**

It's a shotgun blast...

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1098239/1098239_900.jpg#resized)

**Russian:**

А это уже по серьезнее...

**English:**

That's a big one...

**Russian:**

rodos

**English:**

rodos (possibly Rhodes island)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1100851/1100851_900.jpg#resized)

**Russian:**

Ну а такими, да на скорости в 2-3 км в сек уже можно делать большие воронки!

**English:**

And with these, yes, at a speed of 2-3 km per second you could make really big craters!

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1092558/1092558_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1091575/1091575_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1091316/1091316_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1091025/1091025_900.jpg#resized)

![](https://img.pandoraopen.ru/http://ic.pics.livejournal.com/vaduhan_08/73269170/1089817/1089817_900.jpg#resized)

**Russian:**

Вот они Челябинские метеориты, лежат себе, ждут своего бога который их запустит...

**English:**

Here are Chelyabinsk meteorites. Just lying there waiting for their god who will launch them...

**Russian:**

P.S: когда писал про ядра эту статейку попалась такая вот странная информация...

**English:**

P.S: While writing this article about cannonballs I came across this strange information...

**Russian:**

> Неподалёку от Оренбурга, в селе Новоникитино один из местных жителей нашел старые металлические шары. Как выяснилось позже, этими шарами были настоящие боевые пушечные ядра, изготовленные в конце 18-го века.

**English:**

> Not far from Orenburg, in the village of Novonikitino, a local resident found old metal balls. It turned out later that these balls were real cannonballs made at the end of the 18th century.

**Russian:**

> По предварительным оценкам, найденные пушечные ядра могут относиться к периоду Пугачевского восстания 1773-1775 годов. Тогда на территории нынешней Оренбургской области располагалась ставка Емельяна Пугачёва, а позднее – проходила знаменитая Крестьянская война. И пушки были одним из основных используемых оружий.

**English:**

> According to preliminary estimates, these cannonballs can be attributed to the period of the [Pugachev Rebellion in 1773-1775. At that time Yemelyan Pugachev's headquarters were located in the territory of the present Orenburg region, and later the famous Peasant War took place](http://blogs.bu.edu/guidedhistory/files/2012/09/Pugachevs-Rebellion.pdf). And cannons were one of the main weapons used.

**Russian:**

> Металлические шары, внешне напоминавшие боевые пушечные ядра, житель Новоникитино нашел, когда распахивал поле. Они были обнаружены примерно на полуметровой глубине. Опасаясь, что найденные предметы действительно могут оказаться настоящими пушечными ядрами, мужчина обратился за помощью в полицию.

**English:**

> A resident of Novonikitino found metal balls that looked like cannonballs while plowing a field. They were about half a meter deep. Fearing that the objects could be real cannonballs, the man turned to the police for help.

**Russian:**

> Прибывшие правоохранители смогли установить, что обнаруженные мужчиной предметы действительно являются пушечными ядрами, принадлежащими, ориентировочно, к 18-му веку. Жители близлежащих домов были немедленно эвакуированы, а по периметру места с необычной находкой полицейские выставили оцепление. Чтобы проверить и, в случае необходимости, обезвредить боеприпасы, на место их обнаружения были вызваны сапёры. Группа сапёров должна приехать из ближайшего военного полигона, расположенного в Тоцке.

**English:**

> Law enforcers determined that the items were indeed cannonballs, tentatively dating them to the 18th century. Residents of nearby houses were immediately evacuated, and police officers set up a cordon around the perimeter of the site of the unusual find. The bomb squad was called to the scene to check and, if necessary, disarm the ammunition. A group of sappers was due to arrive from the nearest military training ground, located in Totsk.

**Russian:**

> Несмотря на свой почтенный возраст, найденные ядра действительно опасны для окружающих. Пролежав в земле более 200 лет, они не утратили своей взрывной мощи и по-прежнему могут взорваться в любой момент. Причина тому – особый дымовой порох, которым ранее «начиняли» снаряды. В отличие от современных видов пороха, его дымовой предшественник значительно дольше сохраняет свои взрывные свойства.

**English:**

> Despite their venerable age, the cannonballs are indeed dangerous to others. Having lain in the ground for more than 200 years, they have not lost their explosive power and can still explode at any time. The reason for this is the special type of gunpowder that was used to "fill" shells. Unlike modern types of gunpowder, its 'smoky' predecessor retains its explosive properties for much longer.

**Russian:**

> Дальнейшая судьба необычной оренбургской находки пока остаётся неизвестной. Если прибывшие на место сапёры подтвердят, что найденные боеприпасы опасны для людей и могут взорваться, они будут ликвидированы. Каким именно образом будут обезвреживать пушечные ядра пугачёвских времён, пока не ясно. После исследования местности, на которой были найдены эти пять ядер, специалисты примут окончательное решение в отношении необычной находки.

**English:**

> The fate of the unusual Orenburg find remains unknown. If the sappers who arrived at the site confirm that the found ammunition is dangerous for people and can explode, it will be disarmed or destroyed. The exact method of neutralizing cannonballs from the Pugachev times is not yet clear. After investigating the area where the five cannonballs were found, experts will make a final decision regarding the unusual find.

**Russian:**

> Учитывая особенности старинных боеприпасов, они могут быть утилизированы нестандартным способом, который подберут специалисты-сапёры. Если же окажется, что найденные жителем Октябрьского района снаряды не представляют серьезной угрозы, они будут переданы в городской музей. В экспозиции Музея истории Оренбурга на сегодняшний день уже есть целая коллекция пушечных ядер, диаметром от 5 до 10 сантиметров...

**English:**

> Considering the peculiarities of the antique ammunition, it may have to be disposed of in a non-standard way, which will be chosen by the sappers. If it turns out that the shells found by a resident of Oktyabrsky district do not pose a serious threat, they will be transferred to the city museum. Today the History Museum of Orenburg already has a collection of cannonballs, ranging in diameter from 5 to 10 centimeters.

**Russian:**

Странная суета вокруг старых ядер… эвакуация жителей…. взорваться могут….. куда делись не известно, военные увезли…. вот так всегда!

**English:**

Strange fuss over old <del>nukes</del> cannonballs... evacuation of residents... they may explode... where they went, we don't know, the military took them away... It's always like this!

**Russian:**

Источник: [vaduhan-08.livejournal.com](http://vaduhan-08.livejournal.com/38130.html)

**English:**

Source: [vaduhan-08.livejournal.com](http://vaduhan-08.livejournal.com/38130.html)

© All rights reserved. The original author retains ownership and rights.

