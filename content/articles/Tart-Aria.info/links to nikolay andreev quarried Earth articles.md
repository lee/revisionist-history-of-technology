title: Evidence Earth has been Quarried - Nikolay Andreev
date: 2021-07-22
modified: 2022-04-24 09:54:38
category: 
tags: quarried Earth; links
slug: 
authors: Nikolay Andreev
summary: Links to Russian language articles about quarried Earth with Google-translate function built in. Each article should render in English after a few seconds.
status: published
originally: Links to Quarried Earth articles translations.md
from: 
local: Links to Quarried Earth articles translations.md

[Temples stand on crosses. Part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ss69100.livejournal.com/4207380.html)

[Temples stand on crosses. Part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ss69100.livejournal.com/4207380.html://www-tart--aria-info.translate.goog/hramy-stojat-na-krestah-chast-2/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Unknown Anadyr](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/nevedomyj-anadyr/)

[Mysterious Glen Canyon](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/zagadochnyj-gljen-kanon-glen-canyon/)

[Chronological travels in the Crimea. Part 1. Panticapaeum](https://www-tart--aria-info.translate.goog/hronologicheskie-puteshestvija-v-krymu-1/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[Crimean bridge and a series of catastrophes of the late 15th century](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.tart-aria.info/krymskij-most/)

[Krasnodar - our ancestors did not build cities like this!](https://www-tart--aria-info.translate.goog/krasnodar-lozohodstvo/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Bosnia and Herzegovina, the past of ancient churches](https://www-tart--aria-info.translate.goog/bosnija-i-gercegovina-proshloe-starinnyh-cerkvej/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[The ancient origins of the ancient metallurgy of the village of Istie](https://www-tart--aria-info.translate.goog/drevnie-istoki-starinnoj-metallurgii-sela-iste/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[Zvenigorodsky settlement ins and outs](https://www-tart--aria-info.translate.goog/zvenigorodskoe-gorodishhe/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[Secrets of the Dzhusinsk pyrite-polymetallic deposit](https://www-tart--aria-info.translate.goog/tajny-dzhusinskogo-kolchedanno-polimetallicheskogo-mestorozhdenija/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Star forts and their orientation according to linear BGF anomalies](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://andreevn-bgf.blogspot.com/2019/04/blog-post.html)

[Once again - about the pyramids of Giza, Egypt](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://andreevn--bgf-blogspot-com.translate.goog/2019/05/blog-post.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Nikolay Andreev's LiveJournal blog](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://andreevnm.livejournal.com/)

For similar articles, see ['Quarried Earth' tag](https://178.62.117.238/tag/quarried-earth.html)



© All rights reserved. The original author retains all rights.


