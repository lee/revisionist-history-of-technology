title: Inspired by "And there was a Country before Russia". Part 4
date: 2013-10-15
modified: 2021-08-16 21:18:49
category: 
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: If this is part of a system, where is the rest? We are nudged to look at bells, too, from a different angle.
status: published
originally: По мотивам «И была Страна до России». Часть 4.html

### Translated from:
[https://pro-vladimir.livejournal.com/2013/10/15/](https://pro-vladimir.livejournal.com/2013/10/15/)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html)

Previous attempts to look at the "cannons" from a slightly different angle led us to the following conclusions:

- They were not fired with gunpowder.

- Their working position was vertical.

But if this is part of a system, where is the rest? In ["And there was a Country before Russia,"](http://dmitrijan.livejournal.com/71535.html), it feels as if we are nudged to look at bells, too, from a different angle. We should look at the "elephants", so to speak.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/1276507571_10744_12.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/1276507571_10744_12.jpg)
-->

[![](http://topwar.ru/uploads/posts/2010-06/1276507571_10744_1234963984_full.jpg#clickable)](http://topwar.ru/uploads/posts/2010-06/1276507571_10744_1234963984_full.jpg)

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/1276507656_10746_12.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/1276507656_10746_12.jpg)
-->

[![](http://topwar.ru/uploads/posts/2010-06/1276507656_10746_1234963985_full.jpg#clickable)](http://topwar.ru/uploads/posts/2010-06/1276507656_10746_1234963985_full.jpg)


We re-orient the barrel of the Tsar cannon vertically, on to the flat ''heel' of its base.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/h-4.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/h-4.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-4.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-4.jpg)

If you then look around, on the territory of the Kremlin and comparable in size to the Tsar cannon, is the Tsar bell.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/SemaukrasheniiCarko.png#clickable)](По мотивам «И была Страна до России». Часть 4._files/SemaukrasheniiCarko.png)
-->

[![](http://www.redov.ru/istorija/kremlyovskie_kolokola/SemaukrasheniiCarkolokola..png#clickable)](http://www.redov.ru/istorija/kremlyovskie_kolokola/SemaukrasheniiCarkolokola..png)

We push away the initial "Oh, well, nonsense" and look closely. Looks like a bell, looks just like a bell. Maybe a big one. 200 tons of bronze. The diameter is 6.6 meters. The height, together with the lugs, is 6.24 meters.

The suspension of the bell attracts attention. I mean, the lugs at the top. Somehow they are very interestingly shaped. More precisely, in the classic form they are not too functional for hanging a bell. More like squiggles of some kind, instead of rounded lugs.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/Mingun_Bell_1873.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/Mingun_Bell_1873.jpg)
-->

[![](https://upload.wikimedia.org/wikipedia/commons/c/cb/Mingun_Bell_1873.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/c/cb/Mingun_Bell_1873.jpg)

Here is a bell comparable in size to the Tsar Bell. Look at the lugs. Does anything bother you? Where is the same structure on the Tsar bell? 

It is not there.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/007.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/007.jpg)
-->

[![](http://zvon.ru/images/3_5_10/007.jpg#clickable)](http://zvon.ru/images/3_5_10/007.jpg)

The tiny hole is not comparable to the size of a bell.

If you look at modern bells, there is quite a full-fledged 'eye'.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/ustroystvo_kolokola.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/ustroystvo_kolokola.jpg)
-->

[![](http://www.vidania.ru/picture/raznoe/ustroystvo_kolokola2.jpg#clickable)](http://www.vidania.ru/picture/raznoe/ustroystvo_kolokola2.jpg)

[![](./images/Inspired by And There Was a Country Before Russia - Part 4_files/roughly translated h-2 tsar bell and cannon.png#clickable)](./images/Inspired by And There Was a Country Before Russia - Part 4_files/roughly translated h-2 tsar bell and cannon.png)
*Auto translated version*

That is, there is a very specific "Queen cell" (Translator's note: technically called the 'argent' in English). Why doesn't the two-hundred-ton bell have one?

View of the Tsar bell from above:

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/Image043psh_nn.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/Image043psh_nn.jpg)
-->

[![](http://www.decorbells.ru/pictures/bells_n/bells_large_gallery_nn/Image043psh_nn.jpg#clickable)](http://www.decorbells.ru/pictures/bells_n/bells_large_gallery_nn/Image043psh_nn.jpg)

*Translator's note: While looking for the correct English technical word for the crooked lugs found on some bells, we found a Lichfield Cathedral, UK, document - [NOTES ON BELLS, BELL HANGING, BELL RINGING AND BELL RESTORATION](https://www.lichfield.anglican.org/content/pages/documents/018c55716665fd6e25c2eeca042d6f6500b7be5a.pdf) - which says these lugs are called 'canons'. This remarkable choice of word provides etymological support for author Vladimir's contention about the connection between bells and cannons.*

*The same document notes that:*

> Foundry workers of earlier centuries were probably illeducated, barely literate even, and it is not unusual to find evidence of this in the form of e.g. the letters "N" and "S" being reversed or upside down.

*Another possible explanation is that bell casters were literate and familiar with the Cyrillic alphabet.*

The 'power ball and cross' are simply fitted on the top of the structure. And you can see there is quite a specific flat plane. Let's mentally remove the 'power ball', though it will be useful to us a little later. We turn the bell upside down and put it on the "barrel" of the Tsar cannon.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/h-3.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/h-3.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-3.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-3.jpg)

Maybe I made a little mistake with the scale.

In the ornately curved lugs, we have shock absorbers that allow the bell to oscillate at a certain frequency.

So what do we have in the Moscow Kremlin? And what functionality did all this carry?

Studying the resulting image, one thinks of a cup on a footing.

If you just forget about the scale, then... it looks like a cup... the Holy Grail?

Looking around the world wide web...

<!-- Local
[![](По мотивам «И была Страна до России». Часть 4._files/h-186.jpg#clickable)](По мотивам «И была Страна до России». Часть 4._files/h-186.jpg)
-->

[![](http://content.foto.my.mail.ru/mail/mamzerev/2/h-186.jpg#clickable)](http://content.foto.my.mail.ru/mail/mamzerev/2/h-186.jpg)

Radiates and resonates.....

The radiation pattern of the bell comes from the side surfaces, and if it propagates as is now accepted, then the sound goes to the sky, because the focus of the curvature of the surfaces is upwards. And if you turn it over, it diverges down, covering the district with a sort of umbrella. It would be logical to raise it higher so that its resonations spread further.

Vladimir Mamzerim 15.10.2013.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-5.html)

© All rights reserved. The original author has asserted their ownership and rights.

### Selected comments:

Q. Ay, laddie! What's the bell talk for? Or is it a late addition, unnecessary, and originally it was not included?

A. [And there was a Country before Russia_h2](http://dmitrijan.livejournal.com/71850.html)

But this requires a no less powerful instrument for dispersion, and is also non-magnetic. Ie from the same bronze, for example. And there are a lot of such devices – these are bells! Or stake-on-stake. We talk about something the same way when we break something about something. In this case, the energy is dispersed over areas. And these diffusers are adjusted by moving metal tabs inside the working area, such as weights on wheels, magnets on kinescopes, or the tongue of bells. Who didn't like to drive a magnet through a kinescope? Who has not seen how the settings of the beams change at the same time?

After all, if the energy is directed to the bell that hangs higher, then the entire space in the area will be covered by a kind of umbrella of radiation that will break on the bell and spread, and the flow only needs to be corrected by moving the tongue inside the bell. Then the radiator, in the form of the above-described barrel, should be placed vertically, pointing at the bell? And hang the bell itself as high as possible, and then we get a zone that is protected by energy and radiation from this bell! One thing is important-the energy must be constant, then it makes sense. Or only temporary in case of an attack - then by hitting the bell, you can bring the system out of static balance and increase the distributed energy of the screen around, making it unstable undulating. And when there is not enough energy, the emitters can be put on direct fire and hit the enemy with pinpoint shots. To do this, it is logical to have such emitters in strategic directions. And what is strange, analogues of the Tsar Cannon in the same Moscow are mentioned there.

It would seem nonsense? But then for what whim in a time of danger, including epidemics, the bells were rung? In case of an attack, it is still clear that you can notify the district in this way, but the banal "rail" is also suitable for this, but in case of infection? Church bell chimes also do not require bells, you can do with "rails" of different lengths and tones, respectively. But no, we use the bells that are left to us from the old! Where do the references that the bells sometimes rang themselves come from? Why put the bell towers so high, because their sound can spread from a low place. Why was there a ban on building buildings above the bell towers? that is, direct visibility of the bell towers was more than important, as well as the integrity of the surrounding buildings during their operation. Even today, functional towers are put higher than buildings and not for the sake of the beauty of the towers themselves.

As for the tongue that lies near the Tsar bell, it is not from him. absurdly and artistically executed. Here is the ball of power, here some suggest))) it is just from this system. But about it a little later.


Q. It turns out that either the security system or the perfect weapon...or maybe both at the same time...I wonder what served as the "battery" for the system...Oh and clever ancestors were)

A. See [http://dmitrijan.livejournal.com/71850.html](http://dmitrijan.livejournal.com/71850.html). It's about batteries. straight from the first lines.

Thank you for the flight of thought! I'll drag you to my place!


All this is great!

Thinking...
st_pit
2013-10-18 08: 17 am (UTC)
There is such a tool - a flat base, steel, set forged... I think - if you wanted to copy what you did not understand did something similar but under the gunpowder, then it is logical...

<!-- Local
![](По мотивам «И была Страна до России». Часть 4._files/19835_300.jpg "bombard pic")
-->

![](https://ic.pics.livejournal.com/st_pit/4903014/19835/19835_300.jpg "Vertical cannon copy")

Further, if you follow the same logic - "copied" combat "bells" and made these tureens?

<!-- Local
![](По мотивам «И была Страна до России». Часть 4._files/20288_300.jpg "Mortars 1")
-->

![](https://ic.pics.livejournal.com/st_pit/4903014/20288/20288_300.jpg "mortars 1")

<!-- Local
![](По мотивам «И была Страна до России». Часть 4._files/20057_300.jpg "Mortars 2")
-->

![](https://ic.pics.livejournal.com/st_pit/4903014/20057/20057_300.jpg "mortars 2")

Recent photos in St. Petersburg. The first in Paris (Invalides).

By the way, the first mortar shot stone balls, either from the fact that they were easier to make, whether from the fact there were many of them, and only then was the "bomb" and finally, the cross-section profile they have as much as the "Tsar Cannon"...

It turns out that copied and not understood that?

man_oleg
2013-10-22 04:10 am (UTC)
"Tell me, so in any store you can wall up here?" (Georges Miloslavsky, a friend of Anton Semyonych Shpak)

And also, Vimana, you can collect?

in General, keep it up! Thanks.

So you see, everything will be deciphered


