title: Videos: Forgotten Reality's video playlists
date: 2021-02-04 08:27
modified: 2021-09-04 13:10:45
category: 
tags: videos; links
slug: 
authors: asteroid707
summary: Forgotten Reality - Remembering the past, going back to the beginning. Youtube video playlists. As a test of a third-party video translation service, it didn't work as well as hoped but the videos may still be interesting, especially if you speak the native language.
status: published
from: https://www.youtube.com/c/asteroid707/playlists

#### Translated from:

[https://www.youtube.com/c/asteroid707/playlists](https://www.youtube.com/c/asteroid707/playlists)

**Russian:**

Встречи с другими исследователями.

**English:**

Meet other researchers

[View full playlist (1 video)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTEgbSe74unTBKyplmR_RvPF)

**Russian:**

Датировка событий прошлого

**English:**

Dating past events

[View full playlist (2 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTGSoOY2dEHuX_o6uzMZJPeE)

**Russian:**

2015: Искажение истории как метод управления сознанием

**English:**

2015: Distortion of history as a method of mind control

[View full playlist (10 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTHzUgTdhJEAabX9FtOqTOV-)

**Russian:**

Беседа А.Артемьева с Д.Шолоховым

**English:**

Conversation between Artemyev and Sholokhov.

[View full playlist (9 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTEBEfpae58zABaNO-8ix5KS)

**Russian:**

Алексей Артемьев

**English:**

Alexey Artemyev

[View full playlist (10 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTFJDeoY-dO9lUOmB2dbGHap)

**Russian:**

Альтернативная история

**English:**

Alternative History

[View full playlist (3 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTEPpIiroxGq7_ytzIlfgMny)

**Russian:**

Русская песня

**English:**

Russian song

[View full playlist (1 video)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTHGm_1Vi_Ma6_PLyT3dL3FX)

**Russian:**

Незамеченная реальность

**English:**

Unnoticed reality

[View full playlist (12 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTFatwoilczOSpDC_AXZv811)

**Russian:**

Забытая Реальность

**English:**

Forgotten reality

[View full playlist (1 video)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTFcwaUanIt4Tlk7vjupeh5s)

**Russian:**

Алексей Кунгуров о насущном

**English:**

Alexey Kungurov on the essentials

[View full playlist (4 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTHs6u1JsqSTLJpRjJAgSVah)

**Russian:**

Загадки истории 2015 с Алексеем Кунгуровым

**English:**

History Mysteries 2015 with Alexey Kungurov

[View full playlist (2 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTHQg14SNLM02gmXnAxe047u)

**Russian:**

ВладимирЪ ГоворовЪ

**English:**

Vladimir GovorovЪ

[View full playlist (5 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTE42RC3KiArMgFywKByx1rw)

**Russian:**

Беседа Кунгурова с Миланой и соратниками 13.08.2015 ЕКБ

**English:**

Kungurov's conversation with Milana and associates 13.08.2015 EKB

[View full playlist (4 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTFH64uahews2nTcu264IWJD)

**Russian:**

По загадочным местам Урала

**English:**

To the mysterious places of the Urals

[View full playlist (2 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTExAdqFaP5z5m3NZoryz-hx)

**Russian:**

Искажение понимания Физики как способ кправления

**English:**

Distortion of Physics as a Way of Control

[View full playlist (6 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTGDyDLbpWpHdAEU6gW_yPxf)

**Russian:**

Фильмы Евгения Бажанова

**English:**

Films by Evgeny Bazhanov

[View full playlist (15 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTEmx3E4e7h-RpWC1qMhUg-O)

**Russian:**

Алексей Кунгуров. Ответы на вопросы.

**English:**

Alexey Kungurov. Answers to questions.

[View full playlist (23 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTGJmGlMKpFdhTC-nCupbX-X)

**Russian:**

Искажение истории как метод управления сознанием. А. Кунгуров

**English:**

Distortion of history as a method of mind control. Alexei Kungurov

[View full playlist (13 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTGD2OJld80oFHQcpHQKKW10)

**Russian:**

Видео Чулкин Виктор Иванович

**English:**

Video: Chulkin Victor Ivanovich.

[View full playlist (11 videos)](https://www.youtube.com/playlist?list=PLhWs4RhkUuTGenBeJTT3tHkAl6SsOwMcb)

