title: RoHT newsfeed changes
date: 2021-02-15
modified: 2022-08-31 19:19:54
category: 
tags: Tartary; Tartaria; quarries; quarried Earth
slug: 
authors: RoHT
summary: Changes to RoHT newsfeeds
local: N/A
status: draft
originally: N/A

Obsoleted 'Tartary' feeds ([Atom](feeds/tartary.tag.atom.xml)), ([RSS](tag/tartary/feed)) are now 'Tartaria' feeds:

- [Tartaria (Atom)](feeds/tartaria.tag.atom.xml)

- [Tartaria (RSS)](tag/tartaria/feed)

Obsoleted 'quarries' feeds ([Atom](feeds/quarries.tag.atom.xml)), ([RSS](tag/quarries/feed) are now 'quarried Earth' feeds:

- [quarried Earth (Atom)](feeds/quarried-earth.tag.atom.xml)

- [quarried Earth (RSS)](tag/quarried-earth/feed)
