title: Videos: Forged history
date: 2021-02-03
modified: 2021-09-04 13:10:21
category: 
tags: forged history; videos
slug: 
authors: Various
summary: This page collects together videos about oddities in human technological development. As a test of a third-party video           translation service, it didn't work as well as hoped but the videos may still be interesting, especially if you speak the native language.
status: draft

<!--
#### Formatting from:

[http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/](http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/)
-->

[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/qYq3i42p5cs/0.jpg)](https://youtu.be/qYq3i42p5cs)

[How to create a false history of planet Earth](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/kak-sozdayut-falshivuyu-istoriyu-planety-zemlya/)


### Formatting example from the technology page:

[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/o0-Y1nD9rvA/0.jpg)](https://youtu.be/o0-Y1nD9rvA)

[Why they hide technology](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/014-otvet-po-teme-pochemu-skryvayut-texnologii/)

    - [Subtitles](https://downsub.com/?url=https://youtu.be/o0-Y1nD9rvA)
    - [English subtitle .srt file (RoHT translation)](../../files/014. Ответ по теме. Почему скрывают технологии.-o0-Y1nD9rvA.srt)


