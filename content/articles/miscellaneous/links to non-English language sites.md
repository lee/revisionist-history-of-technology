title: Non-English History, Chronology and Technology Sites
date: 2021-07-03 15:06
modified: 2023-06-05 17:10:06
category: 
tags: links
slug: 
authors: Various
summary: Links to non-English alt-history research sites translated via Google-translate. This is a somewhat categorised list of links to history and technology-related non-English sites.
status: published

<!-- RSS feed notes:
- vk.com RSS feeds created using [https://www.mysitemapgenerator.com/rss/create.html](https://www.mysitemapgenerator.com/rss/create.html)
- yandex.ru RSS feeds created with the same tool
- blogspot RSS feed instruction from https://oscarmini.com/discover-rss-feed-address-of-a-blogger-blogs/
-->

### Russian History Researchers

- [Poluyan (Russian)](https://poluyan.livejournal.com) ([English](https://poluyan-livejournal-com.translate.goog/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB))

- Post kbogam followers: [https://vk.com/@club183280204](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vk.com/@club183280204)

- Original kbogam: [https://vk.com/@kbogam](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vk.com/@kbogam) Alexander Alexeev but died around 2018. Community now at above

- Earlier kbogam: [https://zen.yandex.ru/outthere](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/outthere)

- Skankpunk: [https://zen.yandex.ru/id/5e4e66c8bb4a6d368b8dca85](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/id/5e4e66c8bb4a6d368b8dca85)

- Sibved24: [https://zen.yandex.ru/sibved24](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/sibved24) is same author as Sibved below but currently has more articles at Zen.Yandex.

- Sibved: [https://sibved.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com) [RSS](https://sibved.livejournal.com/data/rss)

- is3: [https://is3.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://is3.livejournal.com) [RSS](https://is3.livejournal.com/data/rss)

- Nezvanov: [https://nezvanov.livejournal.com](https://nezvanov-livejournal-com.translate.goog/11971.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

    - [Nezvanov index translated](https://nezvanov-livejournal-com.translate.goog/11971.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [https://terrasancta.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://terrasancta.livejournal.com/) [RSS](https://terrasancta.livejournal.com/data/rss)

- Axsmyth: [https://axsmyth.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://axsmyth.livejournal.com/)
[RSS:](https://axsmyth.livejournal.com/data/rss)

- [https://tart-aria.info/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://tart-aria.info/) Various authors collated and some translated to English. [RSS](https://www.tart-aria.info/feed/)

- [Yuri Shap 2015](https://yuri-shap2015.livejournal.com/) (Russian), ([English translation](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://yuri-shap2015.livejournal.com/)

- [https://wakeuphuman.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://wakeuphuman.livejournal.com/) Historic nuclear war and quarrying [RSS](https://wakeuphuman.livejournal.com/data/rss)

- [https://digitall-angell.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/) - melted rocks [RSS](https://digitall-angell.livejournal.com/data/rss)

  - Also at [metaisskra.com](https://metaisskra-com.translate.goog/tag/kniga-pamyati-zvezdnogo-plemeni/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [http://igor-grek.ucoz.ru/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://igor-grek.ucoz.ru/)

- [Igor the Greek Arhi Logos index](https://igor--grek-ucoz-ru.translate.goog/publ/?_x_tr_sch=http&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [https://atlantida-pravda-i-vimisel.blogspot.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://atlantida-pravda-i-vimisel.blogspot.com) blog. (Includes a Google Translate button.) [RSS](http://atlantida-pravda-i-vimisel.blogspot.com/feeds/posts/default?alt=rss)

- [https://levhudoi.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://levhudoi.livejournal.com) blog
[RSS](https://levhudoi.livejournal.com/data/rss)

- [http://levhudoi.blogspot.com/2015/08/blog-post_6.html - check content](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://levhudoi.blogspot.com/2015/08/blog-post_6.html)

- [https://mylnikovdm.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com)
[RSS](https://mylnikovdm.livejournal.com/data/rss)

- [https://neogeschichte.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://neogeschichte.livejournal.com) [RSS](https://neogeschichte.livejournal.com/data/rss)

- [Alex Frolov - http://alexfrolov.narod.ru/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://alexfrolov.narod.ru/) [RSS](https://neogeschichte.livejournal.com/data/rss)

- [https://pro-vladimir.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://pro-vladimir.livejournal.com/) - electricity, cannon, technology and architecture [RSS](https://pro-vladimir.livejournal.com/data/rss)

- [https://dmitrijan.livejournal.com/](dmitrijan.livejournal.com) Source of theory for some pro-vladimir content [RSS](https://dmitrijan.livejournal.com/data/rss)

- [https://i_mar_a.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://i_mar_a.livejournal.com/)
[RSS](https://i_mar_a.livejournal.com/data/rss)

- [https://kadykchanskiy.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://kadykchanskiy.livejournal.com/) [RSS](https://kadykchanskiy.livejournal.com/data/rss)

- chispa1707 variants:

    - [https://chispa1707.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://chispa1707.livejournal.com/) [RSS](https://chispa1707.livejournal.com/data/rss)

    - [https://chispa1707.blogspot.com/2019/07/tea-tobacco-and-drugs.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://chispa1707.blogspot.com/2019/07/tea-tobacco-and-drugs.html) (English)

    - [https://scan1707.blogspot.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://scan1707.blogspot.com) Ancient Geography

- [http://http://papadsolnuh.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://papadsolnuh.livejournal.com/) "Global Flush" "An amateur's view of geology" [RSS](http://http://papadsolnuh.livejournal.com/data/rss)

    - Eg [Meteoric origin of the Sahara (article that set Mylnikov checking impact direction)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://papadsolnuh.livejournal.com/1762.html)

- [https://alexandrafl.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alexandrafl.livejournal.com) [RSS](https://alexandrafl.livejournal.com/data/rss)

- [https://sayanarus.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sayanarus.livejournal.com/) Giants [RSS](https://sayanarus.livejournal.com/data/rss)

- [http://sil2000.livejournal.com - gone](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://sil2000.livejournal.com)

- [https://mishawalk.blogspot.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mishawalk.blogspot.com)

- [Samanta Black - older posts](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.liveinternet.ru/users/samanta_black/rubric/1415953/)

- [Alexey Sharapov](https://vk-com.translate.goog/away.php?to=https://sharapov-alexey.livejournal.com/11390.html&cc_key&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Okhotshiy](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://okhotshiy.livejournal.com/)

- [Alexander Makhov](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://slavruss.narod.ru/osnown/glawn.htm)

- [http://cont.ws/post/185121](https://cont-ws.translate.goog/@shef2016/185121?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem) 

- [xspline https://xspliner.livejournal.com/417282.html](https://xspline-livejournal-com.translate.goog/417282.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax)

- [https://old.lah.ru/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://old.lah.ru/)

- [https://lah.ru/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://lah.ru/)

- [Vademecum.in](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vademecum.in/)

- [http://ttolk.ru/](http://ttolk.ru/) ([English translation](https://ttolk-ru.translate.goog/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_sch=http))

- [Historyscope](https://uctopuockon-pyc.livejournal.com/) (*Russian*), ([*English*](https://uctopuockon--pyc-livejournal-com.translate.goog/?skip=40&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [Double-Break](https://double-break.livejournal.com/49267.html) (*Russian*), [(*English*)](https://double--break-livejournal-com.translate.goog/49267.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

- [hajoh](https://hajoh.livejournal.com/890.html) (*Russian*), [*(English)*](https://hajoh-livejournal-com.translate.goog/890.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)


#### Technology, history and economics

- [Yyprst](https://yyprst.livejournal.com/3602.html) (*Russian*) (*[English](https://yyprst-livejournal-com.translate.goog/3602.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=nui)*) [RSS](https://yyprst.livejournal.com/data/rss)

- [Faraday.ru](http://www.faraday.ru/rusprojects.html), [(English)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.faraday.ru/rusprojects.html)


#### Selenadia, frothy blood, fermented humans

- [Selenadia Livejournal](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://selenadia.livejournal.com/)
[RSS](https://selenadia.livejournal.com/data/rss)

##### Selenadia archives [Archive.org doesn't Google-translate]:
- ["Selenadia's earlier pages described as "All collected into one"](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://maxpark.com/community/8/content/1290678?)

##### Selenadia's two surviving earlier pages as "Citizen_from_Barnaul":
- [People will prevent the apocalypse](https://web.archive.org/web/20120918015114/http://www.liveinternet.ru:80/users/4084478/post157087666)
- [About the created "primal Edem"](https://web.archive.org/web/20120906040059/http://www.liveinternet.ru:80/users/4084478/post188780920)


#### Gorojanin frothy blood, fermented humans

[50095 About Nonhumans](http://gorojanin-iz-b.livejournal.com/50095.html) (*original Russian*), [About nonhumans](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/50095.html) (*English translation*)
Indexes many articles


#### Frequently updated

- [cont.ws](https://cont.ws/) [(English translation)](https://cont-ws.translate.goog/?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=wapp)

- [AlternatHistory - Catastrophe in the Sahara](https://alternathistory-com.translate.goog/masshtabnye-razlomy-i-kratery-pogubivshie-drevnyuyu-tsivilizatsiyu-v-sahare/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
    - [AlternatHistory - New Chronology](https://alternathistory-com.translate.goog/novaya-hronologiya/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)


#### Russian Forums

- [https://ru.geschichte-chronologie.de/index.php](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ru.geschichte-chronologie.de/index.php)

- [SciTechLibrary: Alternative Physics](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.sciteclibrary.ru/cgi-bin/yabb2/YaBB.pl?board=physik-alt)


#### Unchecked

- [cat-779](https://cat-779.livejournal.com/373098.html) *(Russian)*, [*(English)*](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cat-779.livejournal.com/373098.html)

- [Forgotten Reality](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/)
- [Alexei Kungarov videos at xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/category/kungurov/)

- [https://radmirkilmatov.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://radmirkilmatov.livejournal.com/) Logs chronological slips [RSS](https://radmirkilmatov.livejournal.com/data/rss)
- [https://peshkints.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://peshkints.livejournal.com/) [RSS](https://peshkints.livejournal.com/data/rss)
- [http://eaquilla.livejournal.com/494559.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://eaquilla.livejournal.com/494559.html) - check content [RSS](http://eaquilla.livejournal.com/data/rss)
- [https://gorojanin_iz_b.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://gorojanin_iz_b.livejournal.com) Anton Sizykh [RSS](https://gorojanin_iz_b.livejournal.com/data/rss)
- [https://rodom_iz_tiflis.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://rodom_iz_tiflis.livejournal.com) [RSS](https://rodom_/data/rss)
- [https://sandra-rimskaya.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sandra-rimskaya.livejournal.com) [RSS](https://sandra-rimskaya.livejournal.com/data/rss)
- [https://scaliger.ru](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://scaliger.ru)
- [https://st-pol81.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://st-pol81.livejournal.com) [RSS](https://st-pol81.livejournal.com/data/rss)
- [https://tatiana-matnina.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://tatiana-matnina.livejournal.com) [RSS](https://tatiana-matnina.livejournal.com/data/rss)
- [https://cucujudi.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://cucujudi.livejournal.com/) [RSS](https://cucujudi.livejournal.com/data/rss)
- [http://cat-779.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://cat-779.livejournal.com/) [RSS](http://cat-779.livejournal.com/data/rss)
- [https://skunk-69.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://skunk-69.livejournal.com/) [RSS](https://skunk-69.livejournal.com/data/rss)
- [https://well-p.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://well-p.livejournal.com/) [RSS](https://well-p.livejournal.com/data/rss)
- [https://alex-anpilogov.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://alex-anpilogov.livejournal.com/) [RSS](https://alex-anpilogov.livejournal.com/data/rss)
- Puli Snegopada [https://m.vk.com/id260184411/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://m.vk.com/id260184411/)
- Alexander Koltypin's [Russian Dopotopa](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.dopotopa.com/home.html)
    - Alexander Koltypin's [Earth Before the Flood](http://earthbeforeflood.com) (*English version of the site*)
    - Eg [On the formation of the Caucasoid, Negroid, Mongoloid and other races (view two years later)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://www.dopotopa.com/ob_obrazovanii_evropeoidnoy_negroidnoy_i_mongoloidnoy_ras.html)
    - [Description of the catastrophe that led to the change of the old and new worlds and the emergence of modern humanity, in the legends of different peoples: the creation of a new world and new people](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.dopotopa.com/opisanie_katastrofy_privedshey_k_smene_starogo_i_novogo_mirov_i_pojavleniju_ludey_sozdanie_novogo_mira_i_ljudey.html)
    - [Origins of Gods and People](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.dopotopa.com/soderzhanie_razdela_proishozhdenie_bogov_i_ludey.html)
- Alexander Koltypin's OK.ru channel [Earth Before the Flood](https://ok-ru.translate.goog/group/53427815317695?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB) (*English translation*)

- [https://yuri-shap2015.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://yuri-shap2015.livejournal.com/) [RSS](https://yuri-shap2015.livejournal.com/data/rss)
- [Izofatov](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://izofatov.livejournal.com/211254.html)



### Fomenko

- [https://chronologia.org](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://chronologia.org/)

##### Possibly focused on quarries:

- [https://vel124.livejournal.com](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vel124.livejournal.com) [RSS](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vel124.livejournal.com/data/rss)
- [https://metaisskra.com/blog/oplavlennye-megality-i-gorodishcha/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://metaisskra.com/blog/oplavlennye-megality-i-gorodishcha/)
- [https://digitall-angell.livejournal.com/770813.html](https://translate.google.com/website?                                          sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/770813.html)
- [https://digitall-angell.dreamwidth.org/706305.html (Seems to be older version of his livejournal site)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.dreamwidth.org/706305.html)
- [http://alumni.mgimo.ru/page/adaptive/id31258/blog/4308720/](https://translate.google.com/website?                                          sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://alumni.mgimo.ru/page/adaptive/id31258/blog/4308720/)


#### Untranslated History and Technology

- [Yuri Stepanovich Rybnikov - Vserod](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://vserod.com/)

- [http://atsuk.dart.ru/index.shtml](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://atsuk.dart.ru/index.shtml)

- [http://prometheus.al.ru/phisik/physa.htm](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://prometheus.al.ru/phisik/physa.html)

- [https://www.vn-experimenty.eu/](https://www.vn-experimenty.eu/) *(Slovak)* [*(English)*](https://translate.google.com/website?sl=sk&tl=en&ajax=1&se=1&elem=1&u=https://www.vn-experimenty.eu/)

- [http://newfiz.info](http://newfiz.info) (Russian) [*(English)*](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://newfiz.info/)

#### Science peer/paper oriented:
- [https://arxiv.livejournal.com (Igor Grek)](https://arxiv.livejournal.com) [RSS](https://arxiv.livejournal.com/data/rss)
- [https://assucareira.livejournal.com](https://assucareira.livejournal.com) [RSS](https://assucareira.livejournal.com/data/rss)
- [https://axsmyth.livejournal.com](https://axsmyth.livejournal.com)

#### Books

[https://ruscorpora.ru/new/search-main.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ruscorpora.ru/new/search-main.html)

[https://ruslit.traumlibrary.net/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://ruslit.traumlibrary.net/)

[http://many-books.org/auth/1/book/18874/-_bez_avtora/jizneopisanie_sofokla](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://many-books.org/auth/1/book/18874/-_bez_avtora/jizneopisanie_sofokla)

[https://bookscafe.net/book/fomenko_anatoliy-imperiya_i-81816.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://bookscafe.net/book/fomenko_anatoliy-imperiya_i-81816.html)

- [Andrey Sklyarov books - Google translated](https://coollib-com.translate.goog/a/42229-andrey-yurevich-sklyarov?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en)

- [Predmet.ru](https://predemet.ru/), [*(english)*](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://predmet.ru/)

---

Note on vk.com formats:

- https://vk.com/@kbogam gives seeming best index of articles
- https://vk.com/kbogam does not but does work for some RSS readers


#### Polish Researchers

- [Brusek Kodluch](https://translate.google.com/website?sl=pl&tl=en&ajax=1&se=1&elem=1&u=https://kodluch.wordpress.com/)  technology and history [RSS](https://translate.google.com/website?sl=pl&tl=en&ajax=1&se=1&elem=1&u=https://kodluch.wordpress.com/feed)

### Dutch Researchers
- [https://jefdemolder.blogspot.com](https://jefdemolder.blogspot.com/) [RSS](http://jefdemolder.blogspot.com/feeds/posts/default?alt=rss)

- [Hist.tk website (Russian)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://hist.tk/ory/Main_Page)

<!--
    - [Their page of authors they know or review](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://hist.tk/ory/%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82%D1%8B)
            
    - [Their page categorising genuine and provocateur researchers](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://hist.tk/ory/%D0%9A%D0%BE%D0%B3%D0%BE_%D0%B8%D0%B7_%D0%BD%D0%BE%D0%B2%D0%BE-%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%BA%D0%BE%D0%B2_%D0%BC%D0%BE%D0%B6%D0%BD%D0%BE_%D0%BF%D0%BE%D1%87%D0%B8%D1%82%D0%B0%D1%82%D1%8C) 
-->
### German Researchers

- [https://www.thomasliebl.de/](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://www.thomasliebl.de/)
- [http://radikalkritik.de (Hermann Detering passed away on 18-10-2018)](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://radikalkritik.de)
- [http://chronologiekritik.net](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://chronologiekritik.net)
- [http://de.geschichte-chronologie.de/index.php](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://de.geschichte-chronologie.de/index.php)
- [http://de.geschichte-chronologie.de/index.php/forum](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://de.geschichte-chronologie.de/index.php/forum)
<!-- - [https://dillum.ch (Christoph Pfister)](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://dillum.ch) -->
- [https://megalith-pyramiden.de (Walter Haug)](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://megalith-pyramiden.de)
- [https://m-meisegeier.homepage.t-online.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://m-meisegeier.homepage.t-online.de)
- [http://fantomzeit.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://fantomzeit.de)
- [http://symbolforschung.de/pages/volltexte.php](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://symbolforschung.de/pages/volltexte.php)
- [http://zeitensprünge.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://zeitensprünge.de)
- [http://ingolfo.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://ingolfo.de)
- [http://www.paf.li Christof Marx](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://www.paf.li)
- [http://www.ilya.it/chrono/enpages/hemeroen.html](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://www.ilya.it/chrono/enpages/hemeroen.html)
- [historyhacking.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://historyhacking.de/) Was:
    - [https://marioarndt.com](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://marioarndt.com/)
- [https://www.historyhacking.de/geschichtsanalytik/](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://www.historyhacking.de/geschichtsanalytik/)
- [https://sinossevis.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://sinossevis.de/)
- [thomasliebl.de](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=http://thomasliebl.de/)


#### German Forums
[https://de.geschichte-chronologie.de/index.php/forum](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://de.geschichte-chronologie.de/index.php/forum)

[StolenHistory (German)](https://translate.google.com/website?sl=de&tl=en&ajax=1&se=1&elem=1&u=https://de.stolenhistory.net)

#### Miscellaneous useful pages

- [Chronological Revolution, Part 1 (in english) Eugen Gabowitsch](https://de-geschichte--chronologie-de.translate.goog/index.php/geschichte-und/chronologie/83-chronological-revolution-part-1?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)
- [Chronological Revolution, Part 2 (in english) Eugen Gabowitsch](https://de-geschichte--chronologie-de.translate.goog/index.php/geschichte-und/chronologie/83-chronological-revolution-part-1?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem://de-geschichte--chronologie-de.translate.goog/index.php/geschichte-und/chronologie/84-chronological-revolution-part-2?_x_tr_sl=de&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

#### [Collected Technology, Myth, Strange Events and UFO resources from around the world](http://files.afu.se/Downloads/)

- [Magazines and bulletins from around the world](http://files.afu.se/Downloads/?dir=.%2FMagazines)
- English language:
    - [AFU collection of Canadian bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines/Canada/)
    - [AFU collection of Irish bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines/Ireland/)
    - [AFU collection of New Zealand bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines%2FNew%20Zealand)
    - [AFU collection of Australian bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines/Australia/)
    - [AFU collection of UK bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines/United%20Kingdom/)
    - [AFU collection of US bulletins and magazines](http://files.afu.se/Downloads/?dir=Magazines/United%20States/)

##### Probable Technological History - Check:

- [https://digitall-angell.livejournal.com/598726.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/598726.html)
- [https://digitall-angell.livejournal.com/394019.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/394019.html)
- [https://digitall-angell.livejournal.com/245067.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/245067.html)
- [https://digitall-angell.livejournal.com/701949.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://digitall-angell.livejournal.com/701949.html)
- [https://levhudoi.blogspot.com/2016/02/davidenko.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://levhudoi.blogspot.com/2016/02/davidenko.html)

##### LevHudoi videos

[https://vk.com/video/@levhudoigroup (translated)](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://vk.com/video/@levhudoigroup)

##### RIAP articles

[http://ufology-news.com/tag/riap](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://ufology-news.com/tag/riap)


##### General news - occasionally some history

- [Aftershock](https://aftershock.news) ([English translation](https://aftershock-news.translate.goog/?q=front&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)
