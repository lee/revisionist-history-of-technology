title: Inspired by 'And there was a Country before Russia'. Part 1
date: 2013-10-05
modified: Tue 06 Apr 2021 07:50:30 BST
category: pro_vladimir
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: Mons Meg was torn apart when fired. Had the Tsar cannon been fired, it would have experienced the same fate. There is also a Turkish example, which shows how the Tsar cannon's barrel would have burst.
status: published
local: Inspired by And There Was a Country Before Russia - Part 1_files
originally: По мотивам «И была Страна до России». Часть 1.html

### Translated from:
[https://pro-vladimir.livejournal.com/994.html](https://pro-vladimir.livejournal.com/994.html)

Original article based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html)

**Russian:**

Сложно что либо добавить к изложенному, разве что попробовать сделать некоторую подборку визуального ряда. Да расширить, если получится, ряд выводов.

**English:**

It is difficult to add anything to [the above article](http://dmitrijan.livejournal.com/71535.html), except to try to make some selection of the visual series. Yes, expand, if possible, a number of conclusions. 

**Russian:**

> «После того, как Царь-пушку отлили и отделали на Пушечном дворе, её перетащили к Спасскому мосту и уложили на землю рядом с пушкой «Павлин». Чтобы передвинуть орудие, к восьми скобам на его стволе привязывали верёвки, в эти верёвки впрягали одновременно 200 лошадей, и те катили пушку, лежащую на огромных брёвнах-катках. Первоначально орудия «Царь» и «Павлин» лежали на земле у моста, ведущего к Спасской башне, а Кашпирова пушка -- у Земского приказа, располагавшегося там, где сейчас Исторический музей. В 1626 г. их подняли с земли и установили на бревенчатых срубах, плотно набитых землёй. Эти помосты назывались роскаты...»

*(Александр Широкорад «Чудо-оружие Российской империи»).*

**English:**

> After the Tsar cannon was cast and finished in the Cannon yard, it was dragged to the Spassky bridge and laid on the ground next to the Peacock cannon. To move the gun, ropes were tied to the eight brackets on its barrel, 200 horses were harnessed to these ropes at the same time, and they rolled the cannon on huge log-rollers. Initially, the Tsar and Peacock guns lay on the ground near the bridge leading to the Spasskaya tower, and the Kashpirov cannon - at the Zemsky Prikaz, located in what is now the Historical Museum. In 1626 they were lifted from the ground and set on log bases, densely packed with earth. These walkways were called rockety..."

*"Miracle weapon of the Russian Empire", Alexander Shirokorad*

**Russian:**

Пробег по сети, дабы подтвердить или опровергнуть часть изложенного в «И была Страна до России» натолкнул на следующие находки. А именно:

**English:**

Search the Internet in order to confirm or refute part of the above. In ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html) came up with the following findings. Namely:

<!-- Local
![](Inspired by And There Was a Country Before Russia - Part 1_files/ANqzdRzAK0lArMWVrwhlDeHbJoEn6udgHMU8hlCYy_cCYMiGC86PPmdbiHT.jpeg#resized)
-->

![Obfuscated link to image of a large red-painted "cannon" viewed from breech, down barrel](https://imgprx.livejournal.net/277dbcd48fffc9e760e576cf1a9914bf440cce32/JRKih0bSoaoYgTnFYr6GIMDXjgFD8cyfQhfq8q0YvXjRcmy6yk6M6tdhDM2wk8QG94WSromuXKCF74eygBcFijj7z2HEIpfP7hGQFzOkGJUI45Dwe_gN8yOatp2vCQAs)

**Russian:**

Dulle Griet или Бешенная Гретта: [http://www.belgiumview.com/belgiumview/toonmaxi.php4?pictoshow=0004277aa](http://www.belgiumview.com/belgiumview/toonmaxi.php4?pictoshow=0004277aa)

**English:**

Dulle Griet or 'Mad Gretta': [http://www.belgiumview.com/belgiumview/toonmaxi.php4?pictoshow=0004277aa](http://www.belgiumview.com/belgiumview/toonmaxi.php4?pictoshow=0004277aa)

<!-- Local
![](Inspired by And There Was a Country Before Russia - Part 1_files/ANqzdRzAK0lArMWVrwhlDV6Snr4_WB93U_JF_BqkvhdUrvLRkcwsdXrKKyf.jpeg#resized)
-->

![obfuscated link to image showing view of, presumably, same red-painted "cannon" in what appears to be a Dutch street. Assumed device is 'Mad Gretta']()

![](./images/Inspired by And There Was a Country Before Russia - Part 1_files/dulle_greta_breech_to_barrel.jpg#resized)

**Russian:**

Еще одно орудие:

**English:**

Another weapon:

**Russian:**

> «Монс Мег» находилась в Шотландии не всегда. В 1682 году из нее выстрелили, чествуя визита герцога Йоркского (впоследствии ставшего королем Яковом II Английским), в результате чего древнее орудие лопнуло. Так что и сейчас справа в задней части пушки, под лопнувшими кольцами, можно увидеть продольные железные полосы. После того как у «Монс Мег» разорвало ствол, она лежала, снятая с лафета и всеми забытая, до 1753 или 1754 года, когда ее увезли в лондонский Тауэр, поскольку все вышедшие из строя орудия было велено тогда сдать на хранение в Артиллерийско-техническое управление.

**English:**

> Mons Meg was not always in Scotland. In 1682 it was fired to honor the visit of the Duke of York (who later became king James II of England), resulting in the ancient tool bursting. So now, on the right side of the back of the gun, under broken rings, you can see the longitudinal iron bands. After Mons Meg's barrel was torn off, it was taken from the carriage and left, forgotten by everyone. That was until 1753 or 1754, when it was taken to London Tower of London, as all failed guns were ordered to be, then deposited with the Artillery and Technical Department.

[http://jimcaldwell.files.wordpress.com/2012/12/mons-meg-edinburgh-castle-scottland.jpg](http://jimcaldwell.files.wordpress.com/2012/12/mons-meg-edinburgh-castle-scottland.jpg)

<!-- Local
![](Inspired by And There Was a Country Before Russia - Part 1_files/mons-meg-edinburgh-castle-scottland.jpg#resized)
-->

![image of Mons Meg 'cannon' at Edinburgh Castle](http://jimcaldwell.files.wordpress.com/2012/12/mons-meg-edinburgh-castle-scottland.jpg#resized)

**Russian:**

Интересный факт. С тем, что пушку разорвало от стрельбы.

**English:**

Interesting fact. With the fact that the gun was torn apart by the shooting.

**Russian:**

Тут же вспоминаются споры про то, что Царь пушка , также, по мнению экспертов, не способна стрелять как классическая пушка. И в случае стрельбы из Царь-пушки её постигла бы участь «Монс Мег», а попусту порвало.

**English:**

Immediately remember the controversy about the fact that the Tsar cannon, also, according to however, it is not capable of shooting like a classic cannon. And in the case of firing the Tsar cannon would have been the fate of the Mons Meg, but it was torn up for nothing.

**Russian:**

Есть еще и турецкий образец, по которому видно, как бы порвало Царь пушку.

**English:**

There is also a Turkish example, which shows how the Tsar cannon's barrel would have burst.

**Russian:**

Урбан построил «Базилику» для османского султана Мехмеда II, когда в апреле 1453 года турки осадили Константинополь, бывший тогда столицей Византийской империи. Эта бомбарда имела много недостатков, но обладала большой пробивной силой, которая позволила турецким артиллеристам разрушить прочные стены Константинополя. 29 мая 1453 года удачный выстрел из «Базилики» проделал огромную брешь в стене города, что помогло туркам одержать победу.

**English:**

Urban built a "Basilica" for the Ottoman Sultan Mehmed II when in April 1453, the Turks laid siege to Constantinople, which was then the capital of the Byzantine Empire. This bombardment had many disadvantages, but had a great penetration force, which allowed the Turkish gunners to destroy the strong walls of Constantinople. On May 29, 1453, one lucky shot from the Basilica made a huge hole in the wall the city, which helped the Turks to win.

**Russian:**

Уже на второй день эксплуатации на «Базилике» появились трещины, а через шесть недель она и вовсе разрушилась от собственной отдачи.

**English:**

On the second day of operation, the Basilika appeared to crack, and six weeks later it completely collapsed from its own impact.

**Russian:**

Точных данных, как и изображений, о «Базилике» не сохранилось, поэтому ниже приведены её приблизительные характеристики по описаниям различных источников:

**English:**

Accurate data, as well as images, about the "Basilica" has not been preserved, therefore, its approximate characteristics are given below according to the descriptions of various sources:

<!-- Local
![](Inspired by And There Was a Country Before Russia - Part 1_files/800px-Great_Turkish_Bombard_at_Fort_Nelson.jpg#resized)
-->

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Great_Turkish_Bombard_at_Fort_Nelson.JPG/800px-Great_Turkish_Bombard_at_Fort_Nelson.JPG#resized)

<!-- Local
![](Inspired by And There Was a Country Before Russia - Part 1_files/39024-a086a-68359260.jpg#resized)
-->

![](http://data23.gallery.ru/albums/gallery/39024-a086a-68359260-src-ua4de7.jpg#resized)


**Russian:**

Разорванный образец пушки находится в панораме экспозиции в Стамбуле. Панорама как раз посвящена осаде Константинополя 1453 года.

**English:**

The torn up sample of the gun is in the panorama of the exhibition in Istanbul. The panorama is dedicated to the siege of Constantinople in 1453.

**Russian:**

Что имеем, что орудия такой компоновки, при стрельбе порохом разваливаются. Что в принципе зафиксировано и в летописях, и даже в орудиях дошедших до наших времен.

**English:**

What we have is that the guns of this layout, when firing gunpowder, fall apart. That principle is recorded in the annals, and even in the artifacts that have come down to our times.

**Russian:**

То есть, стволы банально ломались, от использования не по прямому, их изначальному, назначению.

**English:**

That is, the barrels simply broke, due to being used in a different way than their direct, original, purpose.

**Russian:**

Теперь посмотрим, такие достаточно трудные в производстве изделия, в изобилии производятся в разных частях света, по разным технологиям, льют из бронзы, куют из железа и всё это, чтоб из них нельзя было стрелять, единственно нам известным способом? То есть, порох, ядра и прочее, это не для этих стволов. Результаты тех, кто считал иначе, приведены в фотографиях чуть выше.

**English:**

Now let's see, these products are quite difficult to produce, they are produced in abundance in different parts of the world, using different technologies, they are cast from bronze, they are forged from iron, and all this so that they cannot be fired in the only way we know how? That is, gunpowder, cannonballs, and so on, this is not for these barrels. The results of those who thought otherwise are shown in the photos just above.

**Russian:**

Пробовали. Разломали только стволы.

**English:**

When tried, the barrels burst.

**Russian:**

Но для чего тогда делать орудие, которое не может стрелять? Вкладывать в него силы и средства, и немалые средства, чтобы оно потом развлекало тех, кто ими будет любоваться? Чисто для эстетики? Сомнения берут по поводу, что для баловства.

**English:**

But why make a gun that can't shoot? To invest in it forces and means - and considerable means - so it then entertained those who will admire them? Purely for aesthetics? Doubts take over that for pampering.

**Russian:**

А вот для чего действительно, стоит задуматься.

**English:**

But that is really worth thinking about.

Владимир Мамзерев 05.10.2013 г.

Vladimir Mamzerev 05.10.2013.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-2.html)

© All rights reserved. The original author retains ownership and rights.
