title: When did Holland Sink?
date: 2020-05-12
modified: 2022-08-31 19:28:37
category:
tags: 
slug:
authors: Neogeschichte
from: https://neogeschichte.livejournal.com/15869.html
originally: When Did Holland Go Under?.html
local: when_did_holland_sink/
summary: Part of Vredenburg Castle's history is missing: the slice between 1577 and 1837.
status: published


### Translated from:

[https://neogeschichte.livejournal.com/15869.html](https://neogeschichte.livejournal.com/15869.html)

Translator's note: Original article is now deleted.

<!-- Local
![](images/when_did_holland_sink/Titel_Jan2020_2mb.jpg)
-->

![](https://cont.ws/uploads/pic/2020/5/Titel_Jan2020_2mb.jpg)

This article attempts to understand when Holland may have been partially below sea level. The article is also available as a popular science film:

<iframe width="100%" height="358" frameborder="0" allowfullscreen
    src="https://www.youtube.com/embed/OLE-zj2FNNw?autoplay=0&t=0s">
</iframe>
When did Holland Sink? Source: [Когда опустилась Голландия?](https://youtu.be/OLE-zj2FNNw)

What do we all know well about Holland? We know that this country is also called the Netherlands, which means low-lying lands in German or Dutch. Yes, we all know that part of this territory is below sea level - in some places three or even seven meters lower. Why did it happen that people began to live below sea level? There can be only two reasons: the sea level has risen or the land has sunk. I think that everyone understands that initially no one could settle on land below sea level for the reason that this would not be easy, since the land would be the bottom of the sea. Now Holland's status quo is supported by numerous dams built along the coast and at the mouths of rivers, and if these dams had not been, then part of the Netherlands would have long since gone under water and become the bottom of the North Sea.

<!-- Local
![](images/when_did_holland_sink/1%20%287%29.JPG)
-->
![](https://cont.ws/uploads/pic/2020/5/1%20%287%29.JPG)

<!-- Local
![](images/when_did_holland_sink/2%20%286%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/2%20%286%29.JPG)

<!-- Local
![](images/when_did_holland_sink/3%20%283%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/3%20%283%29.JPG)

<!-- Local
![](images/when_did_holland_sink/4%20%289%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/4%20%289%29.JPG)

The result of the 1944 flood in Walcheren.

The Netherlands is interesting to us because it is actually a natural example of seabed, which did not become seabed only because of human resistance to the forces of nature. For example, in 1944, the Allies bombed a dyke near the city of Walcheren, after which the waters of the North Sea rushed towards the city. This flood can serve as a partial example for us of what could have happened during that supposed catastrophe or catastrophes in the 16-18 centuries around the world.

The result of the 1944 flood is very much reminiscent of the post-catastrophic reality of that more ancient Europe, which the ruinist painters (in English: '[Capriccio painters](https://en.wikipedia.org/wiki/Capriccio_(art))') portrayed in their paintings. Here are destroyed houses, and the layer of soil brought by water, and how this soil is removed from the streets of the city. At the same time, the height difference between land and sea level was, judging by the flooded first floors, only about two meters, but a relatively thick layer of mud covered the streets of the city and some houses were even destroyed. Some of the trees and pillars have managed to be overgrown with mussels, which makes the picture look like an underwater film of the sunken cities of the Mediterranean.

Now, imagine what would have happened if the elevation difference had originally been much greater. Well, then the paintings of the ruinists simply become documentary evidence that the official science refers to as 'the fantasies of artists' who, with some maniacal persistence, depicted ruins. Okay, let's leave it on the conscience of those who think so. But for us the main question is: when could Holland have sunk below sea level? Here we will consider only the time period for a possible catastrophe, about which official science is silent. It tells us that the Dutch are reclaiming land from the sea, after it disappeared under the sea with the loss of Dogger thousands of years ago.

<!-- Local
![](images/when_did_holland_sink/17.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/17.JPG)

Well, let's try to find traces of a catastrophe in the Netherlands that is closer to us than those thousands of years, about which the generally accepted version tells us. I have repeatedly written that 'generally accepted' does not mean 'correct', since democracy does not apply to science. Only the one is right on whose side are the facts, and not the opinions of the greater mass of scientists.  

<!-- Local
![](images/when_did_holland_sink/7%20%282%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/7%20%282%29.JPG)

<!-- Local
![](images/when_did_holland_sink/8%20%288%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/8%20%288%29.JPG)

<!-- Local
![](images/when_did_holland_sink/9.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/9.JPG)

So, I found traces of this disaster in the example of the [Vredenburg castle](ihttps://en.wikipedia.org/wiki/Vredenburg_Castle), which was built in the Dutch city of Utrecht in 1532. ([Google Maps](https://www.google.com/maps/search/?api=1&amp;query=52.0925,5.113889)), ([Google Streetview](https://www.gps-coordinates.net/street-view/@52.0925,5.113889,h182,p5,z1)), ([OpenStreetMap](https://www.openstreetmap.org/?mlat=52.0925&mlon=5.113889&zoom=13#map=13/52.0925/5.113889)), ([NLS](https://maps.nls.uk/geo/find/#zoom=12&lat=52.0925&lon=5.113889&layers=B000000FFFFFFFFFFTFFFFFFFT&b=2&z=0&point=0,0)), ([Flickr images](https://www.flickr.com/nearby/52.0925,5.113889?show=thumb&fromfilter=1&by=everyone&taken=alltime&sort=distance)) The castle itself is very massive and is a bastion structure. I would say something like the prototype of star fortresses or, as they are also called, star bastions.

This castle was built by Charles V, the Holy Roman Emperor not, as you might think, to protect the city, but in order to control the city's relatively independent population. By the way, this is the official version! I have already written more than once about this - that most castles and fortresses were built to control the population living nearby, and not for military purposes. In war, the storming of such structures is too complicated. So a fortress or castle is simply bypassed or starved out.

Maybe, it is precisely from here - starvation - that the term 'serf', that is, 'a slave-peasant', became attributed to certain fortresses. I will not further develop the topic of serfs here, but in the future there will be a separate article about serfs and about fortresses, especially about their star-shaped version. I have already collected a lot of material on this topic.  

<!-- Local
![](images/when_did_holland_sink/10.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/10.JPG)

<!-- Local
![](images/when_did_holland_sink/12%20%281%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/12%20%281%29.JPG)

<!-- Local
![](images/when_did_holland_sink/11.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/11.JPG)

Let's go back to the Vredenburg castle in the Dutch Utrecht. We will not return to Vredenburg in the 16th century, but instead the 20th century. Then, during the 1976 construction of a concert hall in Utrecht, the remains of this castle were excavated. According to the official version, only the foundations of the castle remained, but as we can see, the foundations look very strange, since in some places something like windows are visible. It is interesting that in our time, heavy equipment - an excavator - was used to excavate these ruins. The soil had covered the foundations to a depth of about 3 meters or a little more.

The castle even had something like a modern sewage system with a drain or water collection system. And the excavated remains of ceramic products clearly tell us that this is not the foundation, but most likely the walls of the castle. It was these ceramics that were washed away by a mud stream at some point during the disaster, until they were brought up against the castle itself, which served as a barrier to the flow of mud. There are also the remains of people, though it is strange to place a cemetery on the territory of the castle. Though it looks more like a person buried alive by mud than a burial.

Furthermore, during the expansion of the road from Utrecht to another Dutch city of Arnhem, an old Frankish cemetery was excavated. A lot of pottery, glassware and ornaments were found. According to the official version: sure, this is a cemetery and the only unclear issue is what coins, dishes and decorations were made there in such a large amount, since at the time of glass-making here it seems that people with household items were no longer buried in Western Europe. Okay, let there be a cemetery, but most likely these remains are a cluster of people caught in the mud flood that destroyed Vredenburg castle.

Returning to the castle, it is worth mentioning that according to legend - and it is clear that those who wrote the history of this castle still retain their conscience because it is also written thus in the official descriptions of the castle - that this castle was destroyed by the inhabitants of Utrecht after the Spanish garrison of the castle left, and the city of Utrecht, along with the rest of Holland, gained independence from the Spanish crown in 1577. Yes, imagine, the inhabitants of Utrecht decided to destroy this hated castle so that the conquerors could no longer use it to control the population of Utrecht. The logic is almost ironclad: destroy the fortress which could be used to protect the city from the conquerors' return.

I think that this version does not stand up to criticism, not only because of the illogicality of destroying the most protected place in the city, but the destruction itself is still a rather laborious process, especially in the case of such a massive castle as Vredenburg. The residents had nothing else to do but destroy this fortress? They are not maniacs and vandals. I think this is why they write 'according to the legend' since neither the legend nor the official legend have anything to do with reality. After all, how can you manually destroy this massive masonry and brickwork, as shown in the photo below? It is, of course, possible to destroy it, water and stone will wear it away, but the result would not be worth the effort.

<!-- Local
![](images/when_did_holland_sink/13%20%282%29.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/13%20%282%29.JPG)

I personally believe that such a huge castle could most likely have been destroyed by a water-mud stream of enormous power and not by the hands of the inhabitants of the city of Utrecht. The castle itself was not completely destroyed and some of the towers survived even until the end of the 19th century. Some of the above-ground parts of the walls were finally destroyed only in 1919. These facts make even more ridiculous the version that claims the destruction of this castle was at the hands of the inhabitants of the city. As they write: they destroyed and destroyed, but for some reason they did not finish their work. Well, of course, the breakthrough of the dam could not do so much destruction in the city, which is relatively remote from the sea, since even in the city of Walcheren in 1944, a breakthrough of a nearby dam could not destroy massive buildings, and only some residential buildings were damaged.

So when did this destruction of the castle actually take place? I think not in 1577,  

<!-- Local
![](images/when_did_holland_sink/14.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/14.JPG)

This is how part of the castle looked like in 1837. To be honest, I do not quite understand what is depicted in this lithograph, but it is written that this is one of the vaults under the castle. This lithograph shows that the lower part is clearly covered with soil and, as I understand it, most of what is depicted is below ground level. For us, the main thing is that the castle was already destroyed in 1837, but in this image from 1656, the castle is depicted as it was in 1540.  

<!-- Local
![](images/when_did_holland_sink/15.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/15.JPG)

But it’s just not clear why in 1656 such a hated castle was depicted in the form as it was more than a hundred years earlier, in 1577. Well, the question naturally arises, how could a castle be depicted in 1656, if it was destroyed in 1577? It looks more like the castle was drawn from life, that is, in 1656 the castle was still standing. So, it must be that the castle was destroyed between 1656 and 1837. In general, it turns out that the catastrophe occurred between these years, which, in principle, fits into the conclusions about the destruction that I spoke about in my previous YouTube videos. Even if we accept the official date of destruction in 1577, then it still fits into the period when destruction took place in other parts of the world. This is how, bit by bit, we come to the conclusion that the disaster occurred not very long ago.

By the way, for a long time a cattle market stood on the site of the destroyed castle. The plot is almost from the paintings of ruinist artists, who very often depicted cows, sheep or goats lounging around on the ruins. And what was left for people to do after the disaster? They had to live somehow.

<!-- Local
![](images/when_did_holland_sink/16.JPG)
-->

![](https://cont.ws/uploads/pic/2020/5/16.JPG)

The presence of soil over the remains of the castle, as well as an incomprehensible cemetery, which was also relatively deep under the soil, tells us that Holland became the Netherlands, that is, 'the lowlands', between 1656 and 1837. How this happened is still difficult to answer, since regular readers know that I believe that there were two disasters - one at the beginning or in the middle of the 17th century, and the second at the end of the 18th century. Around this time, most likely, Holland sank or the North Sea rose. Perhaps at the same time, Doggerland also disappeared under the waters of the North Sea, which was reflected in the increased migration of the same Dutch, Germans, British and Norwegians to all parts of the world, which is most likely due to the fact that they lived in flooded territories.

That's all for now, write in the comments your opinion about the Netherlands, it is possible that you have something to supplement my conclusions, and possibly refute them.
