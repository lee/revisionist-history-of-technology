title: Pieces of the Mosaic - Evidence Earth Was Quarried and Terraformed
date: 2021-08-18
modified: 2022-10-20 10:00:42
category: 
tags: quarried Earth; terraformed Earth; catastrophe; links
slug: 
authors: Dmitry Mylnikov
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: 

Mylnikovdm's 'Pieces of the Mosaic series' continues from his '[Another History of the Earth](./mylnikovdms-another-history-of-the-earth-articles-google-translated.html)' series.

[Pieces of the mosaic. Main events, part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/392145.html)

[Pieces of the mosaic. Main events, part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/392309.html)

[Pieces of the mosaic. Main events, part 3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/395880.html)

[Pieces of the mosaic. Main events, part 4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/398804.html)

[Pieces of the mosaic. Main events, part 5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/414747.html)

[Ancient Dams and Canals of North America](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/13784.html)

[Ancient caves and grottoes created by mining combines](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/14005.html)

