title: About
date: 2020-11-17 13:04
modified: Sat 12 Dec 2020 21:11:49 GMT
category: 
tags: 
slug: 
authors: RoHT Support
summary: Explains RoHT site's purposes; and the principles RoHT follows.
status: published

### Purpose

RoHT exists to help history researchers and translators by:

- Providing a translated repository of history research.

- Providing a place for history researchers and translators to review translations.

- Providing and supporting archive workflows that ensures continued translation, presentation and preservation.

### Principles

- All content is copyright the original author.

- Images referenced in research are served from the original site content. RoHT archives only translated text. 

### Acknowledgements

- RoHT acknowledges with gratitude the financial and IT workflow consultants whose backing, skills and support enable its work.

