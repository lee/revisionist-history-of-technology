title: Bronzes and DVDs - what could they have in common?
date: 2014-03-14
modified: Thu 14 Jan 2021 07:22:00 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/24561.html
originally: Bronze and DVDs - what they may have in common.html
local: Bronze and DVDs - what could they have in common_files/
summary: The mirror, which you will learn about in this article, cannot talk, but it can show that which is inaccessible to the eye. Agree with me: this is  not insignificant for an object...
status: published

### Translated from:

[https://pro-vladimir.livejournal.com/24561.html](https://pro-vladimir.livejournal.com/24561.html)

**Russian:**

Приведу полностью одну статью и журнала "НАУКА И ЖИЗНЬ"

**English:**

Here is a full article from the journal "Nauka i zhizn"

**Russian:**


> ВОЛШЕБНОЕ ЗЕРКАЛО ИЗ ДАЛЕКОГО ПРОШЛОГО А. КАЛИНИН.
>
> Ей в приданое дано
>
> Было зеркальце одно;
>
> Свойство зеркальце имело:
>
> Говорить оно умело.

*Сказка о мертвой царевне и семи богатырях (1833)* А. С. Пушкин.

**English:**


> MAGIC MIRROR FROM THE DISTANT PAST A. KALININ.
>
> She was given as a dowry
>
> There was one mirror;
>
> The mirror had a property:
>
> It could talk.

*The Tale of the Dead Tsarevna and the Seven Heroes (1833)* А. С. Pushkin.

**Russian:**

Возможно, обыкновенное зеркало, знакомое и привычное нам с детства, это первый волшебный предмет, созданный человеком. Свойство показывать окружающий мир и, прежде всего, то, что мы не можем видеть - свое собственное лицо, разве это не чудо? Аборигены Африки, Австралии и Океании за эти маленькие осколки стекла отдавали колонизаторам все, что имели.

**English:**

Perhaps the ordinary mirror, familiar and accustomed to us since our childhood, is the first magic object created by man. Its ability to show the world around and, first of all, what we can't see - our own face, isn't it a miracle? Aborigines of Africa, Australia and Oceania gave to colonizers everything they had for these small shards of glass.

**Russian:**

Но существуют зеркала, обладающие свойствами, в какие трудно поверить, пока не увидишь их собственными глазами. И появились они задолго до того, как Александр Сергеевич Пушкин придумал зеркальце, способное и видеть то, что очень далеко, и рассказывать об этом.

**English:**

But there are mirrors that have properties that are hard to believe in until you see them with your own eyes. And they appeared long before Alexander Sergeyevich Pushkin invented a mirror capable of both seeing what is very far away and talking about it.

**Russian:**

Зеркало, о котором вы узнаете в этой статье, не умеет разговаривать, но может показать то, что недоступно глазу. Согласитесь, это не мало для предмета, который умели делать за десятки веков до написания "Сказки о мертвой царевне и семи богатырях".

**English:**

The mirror, which you will learn about in this article, cannot talk, but it can show what is inaccessible to the eye. Agree with me: this is not insignificant for an object, which was being made dozens of centuries before *The Tale of the Dead Tzarevna and the Seven Heroes* was written.

**Russian:**

Несколько лет назад я случайно услышал, что на территории Китая в древних храмах хранятся зеркала, которые могут показывать, где живет Будда. Тогда же я написал об этом письмо в Китай моему знакомому любителю головоломок, профессору университета в городе Нанкине, но он ответил, что ничего не слышал о подобных вещах. Прошли годы, и неожиданно мой старый китайский друг, которого зовут Сан Янзы и которому сейчас 70 лет, сообщил, что хочет прислать мне в подарок китайское бронзовое зеркало, о котором я спрашивал когда-то. Если это зеркало направить на солнце, а отраженный "зайчик" - на белую стену или лист бумаги, то на них появится изображение, которого нет на полированной лицевой стороне зеркала.

**English:**

Some years ago I accidentally heard that there are mirrors kept in ancient temples in China, which can show where the Buddha lives. At that time I wrote a letter to China about it to my acquaintance, a puzzle-lover, a university professor in the city of Nanjing, but he replied that he had not heard anything about such things. Years passed and unexpectedly my old Chinese friend, whose name is San Yanzi and he is 70 years old now, informed me that he wants to send me as a present the Chinese bronze mirror I asked about some time ago. If this mirror is pointed at the sun and the reflected "ray" is pointed at a white wall or a piece of paper, an image will appear on them that is not on the polished front side of the mirror.

**Russian:**

Вскоре пришла посылка с круглой бронзовой пластинкой диаметром 7 см, отполированной с лицевой стороны так, что ею можно было пользоваться, как зеркалом (см. фото). Лицевая сторона зеркала была немного выпукла, а тыльную сторону украшал барельеф с иероглифами, покрытый патиной - окислами зеленого цвета, которые возникают на старых бронзовых предметах.

**English:**

Soon a parcel arrived with a round bronze plate, 7 cm in diameter, polished on the front side so that it could be used as a mirror (see photo). The front side of the mirror was slightly convex, while the back side was decorated with a bas-relief with hieroglyphs, covered with a patina, a green oxide color that occurs on old bronze objects.

**Russian:**

Вы можете понять мое волнение, когда я направил зеркало на солнце и подставил под "солнечный зайчик" лист бумаги. На бумаге я увидел изображение... Не Будды, а только иероглифы, но увидел! Изображение было!

**English:**

You can understand my excitement when I pointed the mirror to the sun and put a piece of paper under the "sun ray". On the paper I saw an image... Not of the Buddha, just hieroglyphics, but I saw it! There was an image!

**Russian:**

Что же говорит наука о волшебных зеркалах из Китая? Выяснилось, что на эту тему написаны десятки статей и книг. Первое сообщение было опубликовано в британском "Философском журнале" в 1832 году, а последнюю статью о необычных зеркалах можно прочитать в Интернете сегодня. И почти каждый автор считал, что он нашел разгадку тайны. Затем появлялся научный труд с новой версией, и поэтому для большинства ученых магические зеркала остаются загадкой до сих пор.

**English:**

What does science say about magic mirrors from China? It turned out that dozens of articles and books were written on this topic. The first report was published in the British Philosophical Magazine in 1832, and the last article about unusual mirrors can be read on the Internet today. And almost every author thought he had found the solution to the mystery. Then appeared a scientific work with a new version, and therefore for most scientists magic mirrors remain a mystery until now.

**Russian:**

На родине этих зеркал, в Китае, они овеяны славой древних легенд. Одна из них гласит: однажды жена императора в солнечный день сидела в саду и занималась привычным делом - любовалась собой в бронзовом зеркале. Потом она опустила его на колени. Луч солнца отразился от зеркала на белую стену дворца, и в ярком круге на стене появилось изображение дракона. Рисунок дракона в точности повторял рельеф обратной стороны зеркала! Так было впервые открыто волшебное свойство китайских зеркал.

**English:**

In their homeland, China, these mirrors are covered with the glory of ancient legends. One of them says: once the wife of the emperor was sitting in the garden on a sunny day and was doing her usual thing - admiring herself in the bronze mirror. Then she put it down on her knees. A ray of sunlight was reflected from the mirror on the white wall of the palace, and in a bright circle on the wall there was the image of a dragon. The drawing of the dragon exactly repeated the relief of the back of the mirror! Thus for the first time was discovered the magic property of Chinese mirrors.

**Russian:**

С тех пор волшебные зеркала называют в Китае "прозрачные бронзовые зеркала", а происхождение китайской пословицы "На солнце правда всегда проступает наружу" объясняют ими же.

**English:**

Since then magic mirrors are called "transparent bronze mirrors" in China, and the origin of the Chinese proverb "The truth always comes out on the sun" is explained by them.

**Russian:**

Бронза (сплав меди, свинца и олова) была изобретена в Китае за 2000 лет до нашей эры, но самое старое из найденных волшебных зеркал датируется 500 годом нашей эры. Его обнаружили при раскопках гробницы знатного вельможи на юге Китая. Следующее зеркало лежало в усыпальнице императора из династии Танг, умершего примерно в 950 году нашей эры. С ним в одной могиле было 26 его жен в возрасте от 13 до 26 лет, которые не имели права жить после смерти мужа-императора. И на всех жен всего лишь одно магическое зеркало. Вот как мало их тогда было.

**English:**

Bronze (an alloy of copper, lead and tin) was invented in China 2,000 years before Christ, but the oldest discovered magic mirror dates back to 500 BC. It was discovered during the excavation of the tomb of a noble nobleman in southern China. The following mirror lay in the tomb of an emperor from the Tang dynasty, who died about 950 A.D. With him in the same tomb were 26 of his wives, aged from 13 to 26 years, who had no right to live after the death of her husband-emperor. And there was only one magic mirror for all the wives. That's how few there were back then.

**Russian:**

Но через 500 лет, в эпоху правления династии Мин (1368-1644), волшебные зеркала уже перестали для правителей Китая быть большой редкостью, и зеркала именно этой эпохи сейчас можно увидеть в крупнейших музеях мира.

**English:**

But in 500 years, during the Ming Dynasty (1368-1644) the magic mirrors are no longer a rarity for the Chinese rulers and the mirrors of this era can now be seen in the largest museums of the world.

**Russian:**

Возможно, тайна появления изображения на зеркале была не известна и самим китайским мастерам. Дело в том, что в среднем только одно из сотни сделанных зеркал проявляло волшебные способности. Первые попытки объяснить их причину предпринял еще в XI веке китайский ученый Шен Куа. Он полагал, что при литье более тонкая часть зеркала остывает быстрее, чем более толстая, что приводит к небольшим, незаметным глазу искривлениям поверхности. Древние китайские поэты давали свои, поэтические объяснения "прозрачности" металлических зеркал. Этих объяснений поэту Кин Ма, например, хватило на целую поэму.

**English:**

Probably, the mystery of appearance of the image on the mirror was not known to the Chinese masters. The matter is that on average only one mirror out of a hundred made manifested magic abilities. The first attempts to explain their cause were made as far back as the XI century by the Chinese scholar Sheng Kua. He believed that while casting the thinner part of the mirror cools down faster than the thicker one, which leads to small surface distortions imperceptible to the eye. Ancient Chinese poets gave their own, poetic explanations for the "transparency" of metal mirrors. The poet Kin Ma, for example, had enough of these explanations for a whole poem.

**Russian:**

Англичанин Джон Свинтон - первый известный нам европеец, увидевший волшебное зеркало. Он купил его в 1831-1832 годах в Индии в Калькутте, куда оно попало из Китая, и тут же отправил зеркало в Англию Дэвиду Брюстеру. Сэр Дэвид Брюстер (1781-1868) был шотландским физиком, известным своими открытиями в области поляризации света. Кстати, он изобрел любимую нами с детства игрушку-калейдоскоп и был автором еще нескольких оптических игрушек. Сэр Дэвид изучил полученное зеркало и опубликовал отчет в "Философском журнале". Отчет начинался с сообщения, что это зеркало "...удивило дилетантов и сбило с толку философов Калькутты". А затем сэр Дэвид раскрывал секрет. По его мнению, изображение, порождаемое зеркалом, не связано с рисунком на обратной стороне, а наносится слабым раствором кислоты на лицевую поверхность, после чего она шлифуется. В заключение он рекомендовал организовать производство и продажу таких зеркал в Англии, что будет очень прибыльным делом. Но вместо зеркал по его рецепту в Европе появились другие научные доклады с другими рецептами.

**English:**

The Englishman John Swinton is the first known European who has seen a magic mirror. He bought it in 1831-1832 in Calcutta, India, where it came from China, and immediately sent the mirror to England to David Brewster. Sir David Brewster (1781-1868) was a Scottish physicist known for his discoveries in the polarization of light. Incidentally, he invented the kaleidoscope toy we have loved since childhood and was the author of several other optical toys. Sir David studied the obtained mirror and published a report in the Philosophical Journal. The report began with the report that this mirror "...surprised the laymen and confounded the philosophers of Calcutta". And then Sir David revealed the secret. In his opinion the image generated by the mirror is not connected with the pattern on the back side, but is applied by a weak acid solution to the front surface, after which it is grinded. In conclusion, he recommended organizing the manufacture and sale of such mirrors in England, which would be a very profitable business. But instead of mirrors according to his recipe other scientific papers with other recipes appeared in Europe.

**Russian:**

В 1844 году известный французский астроном Араго, один из создателей фотографического процесса, рассказал о волшебных зеркалах на заседании Академии наук Франции. В Париже кроме Араго такое зеркало уже имел знаменитый французский математик маркиз де Лагранж.

**English:**

In 1844 the famous French astronomer Arago, one of the creators of the photographic process, told about magic mirrors at a meeting of the French Academy of Sciences. In Paris, besides Arago, the famous French mathematician Marquis de Lagrange already had such a mirror.

**Russian:**

Сенсационную статью, как сказали бы сейчас (и возможно, настолько же правдивую, как нынешние сенсации), опубликовал в популярном немецком журнале "Садовая беседка" в 1877 году известный в то время писатель Карус Стерн. Он нашел у римского писателя Аулюса Геллиуса, который жил во II-III веках нашей эры, фразу про "зеркала, некоторые из которых отражают их обратную сторону, а некоторые нет". Штерн также раскопал записи итальянского историка Муратори о том, что волшебное зеркало было найдено под подушкой некоего Бишопа из Вероны, который впоследствии был осужден на смерть. И наконец, в той же статье сообщалось, что в древнекитайской книге, относящейся к IX веку нашей эры, есть упоминание о волшебном зеркале.

**English:**

A sensational article, as they would say now (and probably as true as the current sensations), was published in the popular German magazine "Garden Arbor" in 1877 by the then famous writer Carus Stern. He found in the Roman writer Aulus Gellius, who lived in the second to third centuries AD, a phrase about "mirrors, some of which reflect their reverse side and some of which do not." Stern also unearthed the records of Italian historian Muratori that a magic mirror was found under the pillow of a certain Bishop of Verona, who was later condemned to death. Finally, the same article reported that an ancient Chinese book dating back to the ninth century A.D. mentions a magic mirror.

**Russian:**

А вот поблизости от Китая, в Японии, события развивались по-другому. В японских источниках от древнейших времен до второй половины XIX века не обнаружено никаких упоминаний о волшебных зеркалах. Но уже в середине того века в Европу были привезены зеркала, сделанные в Японии. Видимо, японские мастера сумели получить способ изготовления из Китая или научились делать их сами. В 1877 году в Лондоне была организова на целая выставка волшебных зеркал из Японии.

**English:**

But near to China, in Japan, the events developed differently. In the Japanese sources from the most ancient times up to the second half of the XIX century no records about the magic mirrors were found. But in the middle of that century mirrors made in Japan were brought to Europe. Apparently, Japanese masters managed to get the method of manufacture from China or learned to make them themselves. In 1877 an entire exhibition of magic mirrors from Japan was staged in London.

**Russian:**

В начале ХХ века большинство ученых и Запада и Востока считали, что волшебное зеркало делалось следующим способом. После отливки мастер сначала обрабатывал стальным инструментом тыльную часть зеркала, делая рельефный рисунок более качественным. Затем он помещал зеркало на стол тыльной стороной вниз и начинал шлифовать лицевую сторону, сильно надавливая на нее. При этом более тонкие места зеркала, расположенные над впадинами рельефа, немного прогибались и меньше подвергались воздействию абразива. После полировки они выпрямлялись и слегка выступали над средним уровнем зеркала. В результате на лицевой поверхности появлялись фигуры из выпуклых микрозеркал, соответствующие рельефу изображения на обратной стороне изделия. Эти микрозеркала должны были, по мнению ученых, формировать изображение внутри "солнечного зайчика". Объяснение звучало авторитетно, но никто не мог показать хотя бы одно зеркало, сделанное в Европе или Америке этим или каким-либо другим способом.

**English:**

At the beginning of XX century the majority of scientists of both West and East believed that a magic mirror was made in the following way. After casting the master at first processed the back part of a mirror with a steel tool, making a relief pattern more qualitative. Then he put the mirror on the table back side down and started polishing the front side, strongly pressed. Thus thinner parts of the mirror, located over relief hollows, sagged a little and were less influenced by abrasive. After polishing they were straightened and slightly protruded above the average level of the mirror. As a result the figures from convex micro-mirrors appeared on the facial surface corresponding to the relief image on the back side of the article. These micromirrors must have formed, according to scientists, an image inside the "solar bunny". The explanation sounded authoritative, but nobody could show at least one mirror made in Europe or America by this or any other method.

**Russian:**

А в Китае уже нашли волшебное зеркало диаметром 52 см, весом более 12 кг и толщиной 1,3 см. При такой толщине слоя бронзы объяснение европейских ученых выглядело неубедительно.

**English:**

And in China have already found a magic mirror 52 cm in diameter, weighing more than 12 kg and 1.3 cm thick. With such a thick layer of bronze, the explanation of European scientists looked unconvincing.

**Russian:**

Но не это гигантское зеркало вызвало замешательство специалистов, а то, что обнаружены зеркала, у которых рисунок в "солнечном зайчике" не соответствовал рельефу на обратной стороне зеркала! Например, в одном буддийском храме хранилось зеркало, на тыльной стороне которого изображена луна, сияющая над морем, а в отраженном солнечном луче на стене храма возникала фигура Будды в цветке лотоса.

**English:**

But it was not this giant mirror that caused confusion of specialists, but the fact that mirrors were found with the pattern in the "sun ray" that did not correspond to the relief on the back side of the mirror! For example, in one Buddhist temple there was a mirror with the image of the moon shining over the sea on its back side and the figure of Buddha in a lotus flower appeared in the reflected sunbeam on the wall of the temple.


**Russian:**

Волшебное зеркало как бы посмеялось над всем западным ученым миром. Новые необычные находки могли вызвать новую волну интереса к зеркалу, но этого не произошло, так как разразилась сначала Первая, а затем Вторая мировые войны. Кроме опубликованной в 1932 году статьи английского кристаллографа сэра Уильяма Брэгга в ХХ веке вплоть до 1958 года не было никаких сообщений о волшебных зеркалах. Но самое страшное, что и в Китае и в Японии зеркала перестали производить, так как умерли или были убиты те немногие мастера, которые умели их делать.

**English:**

The magic mirror sort of made a laughing stock of the entire Western scientific world. New unusual findings could cause a new wave of interest to the mirror, but this did not happen, because first the First and then the Second World War broke out. Except for published in 1932 article of English crystallographer Sir William Bragg, there were no reports about magic mirrors in the twentieth century up to 1958. But the worst thing is that both China and Japan stopped producing mirrors, as the few craftsmen who could make them died or were killed.

**Russian:**

В 1961 году премьер-министр коммунистического Китая Джоу Эньлай посетил Шанхайский музей, заинтересовался волшебными зеркалами и дал указание восстановить их производство. Эту работу поручили нескольким университетам и техническим институтам. В печати на протяжении двух лет появлялись публикации об их работе, в которых излагались главным образом отрицательные результаты экспериментов. Китайские ученые из разных учреждений вели исследования независимо, каждый пытался найти свой метод и критиковал коллег. Через два года публикации прекратились и появились новые китайские зеркала, которые ни в чем не уступали древним. Изображение, отражаемое ими, могло соответствовать или не соответствовать рельефу на тыльной стороне зеркала. Где и как делали новые зеркала и вся история их воссоздания были окутаны строжайшей тайной. Из переписки с моим китайским коллегой мне стало известно, что сейчас их делают в городе Янгжоу (Yangzhou).

**English:**

In 1961, the Prime Minister of Communist China Zhou Enlai visited the Shanghai Museum, became interested in magic mirrors and gave instructions to restore their production. This work was entrusted to several universities and technical institutes. For two years, publications about their work appeared in the press, which reported mainly negative results of the experiments. Chinese scientists from different institutions researched independently, each trying to find their own method and criticizing their colleagues. In two years the publications stopped and new Chinese mirrors appeared that were in no way inferior to the ancient ones. The image reflected by them might or might not correspond to the relief on the back side of the mirror. Where and how new mirrors were made and the whole history of their reconstruction was shrouded in the strictest secret. From the correspondence with my Chinese colleague I found out that now they are made in the city of Yangzhou.

**Russian:**

Итак, за последние полтора столетия десятки ученых занимались разгадкой волшебного зеркала. Многие из них были уверены, что им удалось раскрыть тайну. Но только в Китае научились делать зеркала, равные древним. Мировой науке остается недоступным метод, найденный в современном Китае, и поэтому сегодня можно только перечислить способы изготовления, предлагавшиеся на протяжении полутора столетий, тем более что каждый из них претендует на достоверность.

**English:**

So, during the last century and a half dozens of scientists were engaged in solving of a magic mirror. Many of them were sure that they managed to solve the mystery. But only in China have learned to make mirrors equal to the ancient ones. The method found in modern China remains inaccessible for the world science and that's why today we can only enumerate the manufacturing methods offered for a century and a half, moreover each of them pretends to be reliable.

**Russian:**

Итак, возможные способы производства волшебных зеркал.

1. При литье более тонкие части зеркала остывают быстрее, чем толстые, что приводит к деформациям поверхности. Поскольку этот процесс зависит от очень многих факторов, только одно-два из сотни зеркал как бы сами собой становятся "волшебными".

2. На лицевой стороне зеркала гравируется рисунок, который затем заполняется бронзой другого сорта и полируется.

3. На лицевой стороне зеркала вырезается рисунок, затем поверхность покрывается амальгамой ртути и полируется.

4. Рисунок на лицевой стороне зеркала протравливается кислотой или другими химикатами, а затем полируется.

5. Рисунок прорезается на тыльной стороне зеркала, что вызывает появление неровностей при полировке лицевой поверхности.

6. Рисунок штампуется на лицевой стороне зеркала, а затем поверхность полируется.

**English:**

So, the possible ways of producing magic mirrors:

1. During casting, the thinner parts of the mirror cool down faster than the thicker parts, which leads to surface deformations. Since this process depends on so many factors, only one or two out of a hundred mirrors seem to become "magic" by themselves.

2. A pattern is engraved on the front side of the mirror, which is then filled with bronze of a different grade and polished.

3. a pattern is cut out on the front side of the mirror, then the surface is coated with mercury amalgam and polished.

4. The pattern on the face of the mirror is etched with acid or other chemicals and then polished.

5. The pattern is cut on the back side of the mirror, which causes irregularities when polishing the front surface.

6. The pattern is stamped on the face of the mirror and then the surface is polished.

**Russian:**

Сейчас многие склоняются к тому, что волшебные зеркала можно делать разными, чуть ли не всеми перечисленными способами. Только никто почему-то не может это доказать, изготовив зеркало, демонстрирующее что-нибудь новое, например Эйфелеву башню.

**English:**

Now many people are inclined to the fact that magic mirrors can be made in different, almost all listed ways. Only, for some reason, nobody can prove it by making a mirror demonstrating something new. For example, the Eiffel Tower.

**Russian:**

Продолжающиеся научные исследования рождают новые сомнения. В 1999 году двое ученых: доктора наук М. Г. Томилин из Государственного оптического института им. С. И. Вавилова и Дж. Сайенс из Калифорнийского университета разрезали волшебное зеркало, чтобы проверить, существуют ли неоднородности металла в местах, которые проецируют изображение. Был использован новейший метод выявления структурной неоднород ности материала при помощи тонких слоев нематических (не знаю, что это такое) жидких кристаллов путем наблюдения их в поляризационном микроскопе. Результаты: структурных неоднородностей поверхности сечения зеркала выявить не удалось, и, как и полагается в науке, появилась еще одна публикация о волшебных зеркалах. Она начинается так: "В истории оптики едва ли можно отыскать столь захватываю щую тайну, которая может сравняться с загадкой волшебных зеркал Востока, хотя над объяснением их удивительных свойств человечество бьется почти четыре тысячелетия". Написано это накануне XXI века.

**English:**

Ongoing scientific research gives rise to new doubts. In 1999, two scientists: M.G. Tomilin, Ph.D., of the Vavilov State Optical Institute, and J. Sayens, of the University of California, cut a magic mirror to check whether metal inhomogeneities existed in places that projected the image. Was used the latest method of revealing the structural heterogeneity of the material using thin layers of nematic (I do not know what it is) liquid crystals by observing them in a polarization microscope. Results: structural inhomogeneities of mirror cross-section surface could not be revealed and, as it is believed in science, there appeared one more publication about magic mirrors. It begins as follows: "One can hardly find such an exciting mystery in the history of optics as the mystery of magic mirrors of the East, though mankind has been trying to explain their amazing properties for almost four thousand years". It was written on the eve of the XXI century.

**Russian:**

А что же в России? На территории нашей родины есть место, о котором мало кто знает, но которое хранит множество загадок, в том числе и связанных с зеркалами. Место это называется Минусинская котловина. Расположена она в Сибири, в 300 км к югу от Красноярска, вверх по течению Енисея. Удивительно, но в этих суровых местах археологи нашли следы культур, созданных нашими предками, начиная с XIV века до нашей эры.

**English:**

And what about Russia? On the territory of our homeland there is a place, about which few people know, but which keeps many mysteries, including those related to mirrors. This place is called the Minusinsk Basin. It is located in Siberia, 300 km south of Krasnoyarsk, upstream of the Yenisei. Surprisingly, in these harsh places archaeologists have found traces of cultures created by our ancestors starting from the 14th century B.C.

**Russian:**

Производство бронзы, как считают историки, было там в III тысячелетии до нашей эры, то есть раньше, чем в Китае. Существует гипотеза, что гунны, погубившие Древний Рим, родом из этих мест. Для нас же самое интересное, что в Минусинской котловине найдено более 360 древних бронзовых зеркал, относящихся к разным эпохам. То ли здесь на протяжении тысячелетий был какой-то культ зеркал, то ли женщины жившего там народа были необычайно кокетливы? Неизвестно...

**English:**

The production of bronze is believed by historians to have been there in the 3rd millennium BC, i.e. earlier than in China. There is a hypothesis that the Huns, who destroyed ancient Rome, came from these places. The most interesting thing for us is that more than 360 ancient bronze mirrors belonging to different epochs were found in the Minusinsk Basin. Could it be that there had been a mirror cult for thousands of years, or the women of the people who lived there were extremely coquettish? We don't know...

**Russian:**

При изучении зеркал Минусинской котловины историки, естественно, не обращали внимания на их лицевую сторону, покрытую слоем окислов, а занимались рисунками и надписями на обратной стороне. И в музеях эти зеркала лежат лицом вниз. Никому и в голову не приходит, что под слоем патины может прятаться некая тайна.

**English:**

When studying mirrors of the Minusinsk Basin historians, of course, did not pay attention to their front side, covered with a layer of oxides, and were engaged in drawings and inscriptions on the reverse side. And in museums these mirrors lie face down. No one thinks that under the patina layer there may be some secret.

**Russian:**

Сотрудники музеев стараются сохранять вещи в том виде, в каком они к ним поступили, и предложение отполировать лицевую сторону зеркала, которому тысяча лет, звучит для них кощунственно. Но если среди 190 старинных зеркал из бронзы, хранящихся в Минусинском краеведческом музее, есть такие, лицевая сторона которых немного выпукла, то велика вероятность, что это "волшебные зеркала". И полировка может открыть их тайну.

**English:**

Employees of museums try to preserve things as they came to them, and a suggestion to polish the front side of a mirror that is a thousand years old sounds blasphemous to them. But if among 190 ancient bronze mirrors stored in Minusinsk Museum of Local Lore there are some with a slightly convex front side it's highly probable that they are "magic mirrors". And polishing may reveal their mystery.

**Russian:**

Возможно, что среди читателей найдутся те, кто захотел бы сам проникнуть в секрет волшебных зеркал. Один из путей исследований - это попытаться изготовить волшебное зеркало из... монеты или медали. Ведь, по предсказаниям ученых, отштампованный, а затем сошлифованный рисунок можно увидеть в отраженном свете. И если полировать монету до момента исчезновения рисунка, то, может быть, он снова станет виден в "солнечном зайчике"? Можно шлифовать лицевую сторону монеты до тех пор, пока не начнет проявлять себя рисунок с обратной стороны, и получить "волшебную прозрачную монету", у которой герб России или СССР будет "виден" через слой металла. В обоих случаях поверхность монеты должна быть слегка выпуклой, так же как немного выпуклы все волшебные зеркала. При полировке верхнего рисунка не следует прилагать больших усилий. А если вы будете пытаться делать "прозрачную монету", то при шлифовке нужно сильнее давить на обрабатываемую поверхность, особенно в конце работы. Проверять монету следует солнечным лучом, а экран располагать на разных расстояниях от нее. Вот все советы, которые можно дать. Остальное зависит от вас. Желаем успеха и ждем сообщений о них.

**English:**

Probably, among readers there will be those who would like to penetrate into the secret of magic mirrors themselves. One of the ways of research is to try to make a magic mirror from... a coin or a medal. In fact, according to predictions of scientists, the stamped and then polished pattern may be seen in the reflected light. And if to polish a coin up to the moment of disappearance of figure, then, maybe, he again becomes visible in the "solar ray"? It is possible to polish the face of a coin until the pattern on the back side starts to show and get a "magic transparent coin", in which the coat of arms of Russia or the USSR will be "seen" through a layer of metal. In both cases, the surface of the coin should be slightly convex, just as all magic mirrors are slightly convex. When polishing the top pattern, you should not exert too much effort. And if you will try to make a "transparent coin", you should press harder on the treated surface when polishing, especially at the end of the work. You should check the coin with a sunbeam, and the screen should be placed at different distances from it. Here are all the tips that can be given. The rest is up to you. We wish you success and look forward to reports about them.

**Russian:**

Подробнее см.: [http://www.nkj.ru/archive/articles/5831/](http://www.nkj.ru/archive/articles/5831/) (Наука и жизнь, ВОЛШЕБНОЕ ЗЕРКАЛО ИЗ ДАЛЕКОГО ПРОШЛОГО)

**English:**

For more information, see: [http://www.nkj.ru/archive/articles/5831/](http://www.nkj.ru/archive/articles/5831/) (Science and Life, A Magic Mirror from the Far Past)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/PTI1j0UJMwVIIki2w-fV25neWotRiD7S3PHs-_9CCW8I5zSEZEeG7LIoUFt.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/fxCjR.jpg)
-->

[![](http://s5.uploads.ru/fxCjR.jpg#clickable)](http://s5.uploads.ru/fxCjR.jpg)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/PTI1j0UJMwVIIki2w-fV2-IWJJ1i_gKtp0JJKpjDiZQt4I2G7Mjtq-Q8X1d.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/KBPYl.jpg)
-->

[![](http://s4.uploads.ru/KBPYl.jpg#clickable)](http://s4.uploads.ru/KBPYl.jpg)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/PTI1j0UJMwVIIki2w-fV2-IWJJ1i_gKtp0JJKpjDiZRR20kloyJ4y00kKWx.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/bVaT7.jpg)
-->

[![](http://s4.uploads.ru/bVaT7.jpg#clickable)](http://s4.uploads.ru/bVaT7.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/PTI1j0UJMwVIIki2w-fV2-IWJJ1i_gKtp0JJKpjDiZQHxdgvYmMWqXlyXE0.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/NskdG.jpg)

-->

[![](http://s4.uploads.ru/NskdG.jpg#clickable)](http://s4.uploads.ru/NskdG.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/PTI1j0UJMwVIIki2w-fV25neWotRiD7S3PHs-_9CCW_-tLQqRrufZoaX6SL.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/BuL6W.jpg)

-->

[![](http://s5.uploads.ru/BuL6W.jpg#clickable)](http://s5.uploads.ru/BuL6W.jpg)


**Russian:**

тут еще одна из статей про бронзовые зеркала:

**English:**

Here's another one of those articles about bronze mirrors:

[http://www.liveinternet.ru/users/stas_stranger/post288398672/](http://www.liveinternet.ru/users/stas_stranger/post288398672/)


[Image no longer available](http://bbs.wenbo.cc/attachments/day_081207/20081207_6357f8def1e4e4947252r2T1DRRWJ3zK.jpg)


[Image no longer available](http://bbs.wenbo.cc/attachments/day_081219/20081219_cbae113f438dffc316ea85lKktUZVn8u.jpg)


[Image no longer available](http://bbs.wenbo.cc/attachments/day_101016/20101016_0fa35acaf95264f77b78fMLEl9sB24Tk.jpg)


[Image no longer available](http://jy.sida168.net/UploadFile/2009-10/200910415451853329.jpg)


[Image no longer available](http://jy.sida168.net/UploadFile/2009-10/200910415464980338.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8eb3NN99APo8jHCIJx_ZegtkCutuJ0zzep_002.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/20130922171550232.jpg)

-->

[![](http://blog.sina.com.tw/myimages/37/154405/images/20130922171550232.jpg#clickable)](http://blog.sina.com.tw/myimages/37/154405/images/20130922171550232.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8eb3NN99APo8jHCIJx_ZegtkCutuJ0zzep_005.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/20131019151837810.jpg)

-->

[![](http://blog.sina.com.tw/myimages/37/154405/images/20131019151837810.jpg#clickable)](http://blog.sina.com.tw/myimages/37/154405/images/20131019151837810.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8eb3NN99APo8jHCIJx_ZegtkCutuJ0zzep_004.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/20131017124537692.jpg)

-->

[![](http://blog.sina.com.tw/myimages/37/154405/images/20131017124537692.jpg#clickable)](http://blog.sina.com.tw/myimages/37/154405/images/20131017124537692.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8eb3NN99APo8jHCIJx_ZegtkCutuJ0zzeprVfH.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/20130911093645436.jpg)

-->

[![](http://blog.sina.com.tw/myimages/37/154405/images/20130911093645436.jpg#clickable)](http://blog.sina.com.tw/myimages/37/154405/images/20130911093645436.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8eb3NN99APo8jHCIJx_ZegtkCutuJ0zzep_003.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/20130914201045219.jpg)

-->

[![](http://blog.sina.com.tw/myimages/37/154405/images/20130914201045219.jpg#clickable)](http://blog.sina.com.tw/myimages/37/154405/images/20130914201045219.jpg)


**Russian:**

тут еще подборка: [http://nepoznannoe.rolevaya.ru/viewtopic.php?id=1316](http://nepoznannoe.rolevaya.ru/viewtopic.php?id=1316)

**English:**

Here's another set: [http://nepoznannoe.rolevaya.ru/viewtopic.php?id=1316](http://nepoznannoe.rolevaya.ru/viewtopic.php?id=1316)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsMwY87NTuP4XvNWEJVu57llD56wxpAcUrg6I84Utr.gif#clickable)](Bronze and DVDs - what could they have in common_files/20105711505034.jpg)
-->

[![](http://www.ewancn.com/_UploadImages/product/20105711505034.jpg#clickable)](http://www.ewancn.com/_UploadImages/product/20105711505034.jpg)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM1kt8p1zZNrIZ1-fi3ik3DPXasH_RPh6-k8JzeN.html)](Bronze and DVDs - what could they have in common_files/j214.jpg)
-->

[![](http://www.npm.gov.tw/exh93/kaidong9310/images/j214.jpg#clickable)](http://www.npm.gov.tw/exh93/kaidong9310/images/j214.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_002.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00447.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00447.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00447.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_007.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00442.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00442.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00442.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_004.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00443.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00443.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00443.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_006.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00444.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00444.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00444.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_005.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00445.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00445.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00445.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7zt_003.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00446.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00446.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00446.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM2DwNw06bUT4FtyYrKS8NSDDnyYdBK1A7ztDceZ.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/pho00448.jpg)

-->

[![](http://www.chiculture.net/20509/picture/pho00448.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00448.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM0UCCpzc3_T0mOtA1twCXI-QAEVHxKzzHkJPV0D.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/IMG_2410.JPG)

-->

[![](http://www.suiempire.com/bwg/IMG_2410.JPG#clickable)](http://www.suiempire.com/bwg/IMG_2410.JPG)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM0UCCpzc3_T0mOtA1twCXI_NwYV74dubfHDpHgV.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/DSC01906.JPG)

-->

[![](http://www.suiempire.com/pictures/DSC01906.JPG#clickable)](http://www.suiempire.com/pictures/DSC01906.JPG)


**Russian:**

тут очень много фотографий музейной экспозиции. в дальнейшем еще не раз к этому источнику вернемся.

[http://www.suiempire.com/viewthread.php?action=printable&tid=56](http://www.suiempire.com/viewthread.php?action=printable&tid=56)

**English:**

There are a lot of pictures of the museum exhibit. We will come back to this source more than once in the future.

[http://www.suiempire.com/viewthread.php?action=printable&tid=56](http://www.suiempire.com/viewthread.php?action=printable&tid=56)

<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8uWunZz2igWtJMoJf1kQBySLC4nA7K8dnsirYt5.bin)](Bronze and DVDs - what could they have in common_files/619c0d66-s.jpg)

-->

[![](http://livedoor.blogimg.jp/hakushikidoo/imgs/6/1/619c0d66-s.jpg#clickable)](http://livedoor.blogimg.jp/hakushikidoo/imgs/6/1/619c0d66-s.jpg)


**Russian:**

на этом ресурсе я так понял можно при желании и приобрести себе немного истории:

**English:**

On this resource, I understand that if you want, you can buy yourself some history:

[http://p479976266.key.shuoqian.net/](http://p479976266.key.shuoqian.net/)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM_Qru0ECqRSu7eQ4NqBBrvgel_SPJJB_lWJP6fA.html)](Bronze and DVDs - what could they have in common_files/b1032696eb00937d9292fc661e79acc6.jpg)
-->

[![](http://img15.artxun.com/sdc/oldimg/b103/b1032696eb00937d9292fc661e79acc6.jpg#clickable)](http://img15.artxun.com/sdc/oldimg/b103/b1032696eb00937d9292fc661e79acc6.jpg)

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM_Qru0ECqRSu7eQ4NqBBrvjqXUX6eanF8x7jNjk.html)](Bronze and DVDs - what could they have in common_files/d370ba76bc570ca3ffa82968f96f7c51.jpg)
-->

[![](http://img15.artxun.com/sdd/oldimg/d370/d370ba76bc570ca3ffa82968f96f7c51.jpg#clickable)](http://img15.artxun.com/sdd/oldimg/d370/d370ba76bc570ca3ffa82968f96f7c51.jpg)

**Russian:**


> ВОЛШЕБНОЕ ЗЕРКАЛО ИЗ ДАЛЕКОГО ПРОШЛОГО А. КАЛИНИН.

> Ей в приданое дано

> Было зеркальце одно;

> Свойство зеркальце имело:

> Говорить оно умело.

*Сказка о мертвой царевне и семи богатырях (1833)* А. С. Пушкин.

**English:**

> MAGIC MIRROR FROM THE DISTANT PAST A. KALININ.

> She was given as a dowry

> There was one mirror;

> The mirror had a property:

> It could talk.

*The Tale of the Dead Tsarevna and the Seven Heroes (1833)* А. С. Pushkin.

<!-- Local
[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsMwBlPlS2OhNvBY7VMWKNSlCiAhPEm2IvPfNvtsp.html)](Bronze and DVDs - what could they have in common_files/08c8a3c7ca5e28104892205b7522c850.jpg)
-->

[![](http://img25.artxun.com/sdb/oldimg/08c8/08c8a3c7ca5e28104892205b7522c850.jpg#clickable)](http://img25.artxun.com/sdb/oldimg/08c8/08c8a3c7ca5e28104892205b7522c850.jpg)

**Russian:**

Вот глядя на это разнообразие, просто диву даешься. Вот признайтесь, вы знали, что этого добра столько много? Я вот не знал. А их не просто много этих «зеркал», а просто очень много.  Что их все объединяет? У всех имеется по центру этакая «пипка», походит на ушко от некоторых типов пуговиц. То есть, элемент этот сделан не просто так, а для конкретной функциональной цели. А именно, для установки и фиксации «зеркала» в одно и тоже место. Не уж-то в устройство для просмотра и проигрывания?  Что роднит все наши дисковые носители информации? Отверстие по цетру.  Что роднит все эти «зеркала»? проушина по центру.  Далее. Все нынешние выпускаемые DVD и не только, имеют на своей тыльной поверхности рисунок, с названием и образами содержимого.  Что мы видим на тыльной стороне всех зеркал? Достаточно сложную картинку. Воспринимаем её как украшательство чистой воды. Вот на зеркалах наших современных как-то не очень с оформлением с тыла. А тут, если это зеркало, то к чему такие выкрутасы? Не уж то чтобы подписать содержимое на носителе информации?  А картинка, которую становится видно при солнечном свете с этого бронзового DVD, не напоминает банально заставку на диске?  И что же там такого в солнечном свете? Надо поизучать. Но ясно одно, что в случае проигрывания бронзовых DVD должен был быть свой источник такого же «солнечного» света.  То есть, при солнечном свете вырисовываемая в «зайчике» изображение получается за счет интерференции. Часть отраженного света усиливается , часть гасится, отсюда видим изображение.

**English:**

Looking at this variety, it's amazing. You have to admit, did you know there was so much of this stuff? I didn't. There are not only so many mirrors, but so many.  What do they all have in common? They all have this "pin" in the centre, like an ear of some kind of button. That is, this element is made for a specific functional purpose. Namely, for installation and fixation of the "mirror" in one and the same place. Isn't it in the device for viewing and playing?  What do all our disc drives have in common? The hole in the center.  What do all these "mirrors" have in common?  Next. All the currently produced DVD's and etc. have picture on their back side with title and pictures of their contents.  What do we see on the back of all mirrors? A rather complicated picture. We take it as decoration. Our modern mirrors do not have such a good decoration on the back side. And here, if it's a mirror, why are you doing such tricks? Is it for signing the content on a data carrier?  And the picture that becomes visible in sunlight from the bronze DVD, doesn't it remind a banal splash screen on the disc?  And what is it about sunlight? I'll have to look into it. But one thing is clear, in the case of playing the bronze DVDs, there had to be its own source of the same "sunlight".  That is, in sunlight the "bunny" image appears due to interference. Part of the reflected light is amplified and part of it is dampened, hence we can see the image.


<!-- Local
[![](Bronze and DVDs - what could they have in common_files/424px-Fentes_young.jpg#clickable)](Bronze and DVDs - what could they have in common_files/424px-Fentes_young.jpg)
-->

[![](http://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Fentes_young.jpg/424px-Fentes_young.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Fentes_young.jpg/424px-Fentes_young.jpg)

**Russian:**

Интерфере́нция све́та --- перераспределение интенсивности света в результате наложения (суперпозиции) нескольких когерентных световых волн. Это явление сопровождается чередующимися в пространстве максимумами и минимумами интенсивности. Её распределение называется интерференционной картиной.

[http://ru.wikipedia.org/wiki/%D0%98%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F_%D1%81%D0%B2%D0%B5%D1%82%D0%B0](http://ru.wikipedia.org/wiki/%D0%98%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F_%D1%81%D0%B2%D0%B5%D1%82%D0%B0)

**English:**

Interference of light --- redistribution of light intensity as a result of superposition (superposition) of several coherent light waves. This phenomenon is accompanied by alternating in space maxima and minima of intensity. Its distribution is called the interference pattern.

[http://ru.wikipedia.org/wiki/%D0%98%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F_%D1%81%D0%B2%D0%B5%D1%82%D0%B0](http://ru.wikipedia.org/wiki/%D0%98%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F_%D1%81%D0%B2%D0%B5%D1%82%D0%B0)

**Russian:**

Информационная структура

Информация на диске записывается в виде спиральной дорожки из питов (англ. pit --- углубление), выдавленных в поликарбонатной основе. Каждый пит имеет примерно 100 нм в глубину и 500 нм в ширину. Длина пита варьируется от 850 нм до 3,5 мкм. Промежутки между питами называются лендом (англ. land --- пространство, основа). Шаг дорожек в спирали составляет 1,6 мкм. Питы рассеивают или поглощают падающий на них свет, а подложка --- отражает. Поэтому записанный компакт-диск --- пример отражательной дифракционной решётки с периодом 1,6 мкм. Для сравнения, у DVD период --- 0,74 мкм.

**English:**

Information structure

Information on the disk is written as a helical track of pits imbedded in a polycarbonate backing. Each pit is about 100 nm deep and 500 nm wide. The length of a pith varies from 850 nm to 3.5 µm. The space between the pits is called a lend (land --- space, backbone). The pitch of the helix is 1.6 µm. The pits scatter or absorb incoming light and the substrate reflects light. Thus, a recorded CD is an example of a reflective diffraction grating with a period of 1.6 µm. By comparison, a DVD has a period of-- 0.74 µm.


**Russian:**

Считывание информации

Данные с диска читаются при помощи лазерного луча с длиной волны 780 нм, излучаемого полупроводниковым лазером. Принцип считывания информации лазером для всех типов носителей заключается в регистрации изменения интенсивности отражённого света. Лазерный луч фокусируется на информационном слое в пятно диаметром ~1,2 мкм. Если свет сфокусировался между питами (на ленде), то приёмный фотодиод регистрирует максимальный сигнал. В случае, если свет попадает на пит, фотодиод регистрирует меньшую интенсивность света.

**English:**

Reading information

Data from the disk is read by means of a 780 nm laser beam emitted by a semiconductor laser. The principle of information readout by laser for all types of media is to register the change in intensity of reflected light. The laser beam focuses on the information layer into a spot with a diameter of ~1.2 µm. If the light is focused between the pits (on the ribbon), the receiving photodiode registers the maximum signal. In case the light hits the pit, the photodiode registers lower light intensity.

<!-- Local

[![](Bronze and DVDs - what could they have in common_files/600px-CD_autolev_crop.jpg#clickable)](Bronze and DVDs - what could they have in common_files/600px-CD_autolev_crop.jpg)

-->

[![](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CD_autolev_crop.jpg/600px-CD_autolev_crop.jpg#clickable)](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CD_autolev_crop.jpg/600px-CD_autolev_crop.jpg)


[http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B0%D0%BA%D1%82-%D0%B4%D0%B8%D1%81%D0%BA](http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B0%D0%BA%D1%82-%D0%B4%D0%B8%D1%81%D0%BA)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM8wWfrPAfg1sw8vdSg6YdJdUGtnY3VfumzVp1o9.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/313580071.jpg)

-->

[![](http://2.bp.blogspot.com/_RjTur172eOw/TJ6n13Xu3OI/AAAAAAAAASA/3RASxCfsgac/s400/313580071.jpg#clickable)](http://2.bp.blogspot.com/_RjTur172eOw/TJ6n13Xu3OI/AAAAAAAAASA/3RASxCfsgac/s400/313580071.jpg)

<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM7Z8xKGkH4SJXlw-hNc44YDvSrho5PZ3AqKXA1N.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/101701_1.1.jpg)

-->

[![](http://www.cctv.com/program/tgkw/20040330/images/101701_1.1.jpg#clickable)](http://www.cctv.com/program/tgkw/20040330/images/101701_1.1.jpg)


<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM-X3MnZWD6ttZWcNSF_Y5kKggQwIQD74E7x0Ffs.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/img_news.aspx?id=20140129000563&no=1&kyodo=1)

-->

[![](http://www.shikoku-np.co.jp/img_news.aspx?id=20140129000563&no=1&kyodo=1)](http://www.shikoku-np.co.jp/img_news.aspx?id=20140129000563&no=1&kyodo=1)

<!-- Local

[![](Bronze and DVDs - what could they have in common_files/NMVwqtpOLmnAdS1qsFZsM_VemEe-i-k2nISlfN-0gx89pRkMxYdvtYjKIOH.jpeg#clickable)](Bronze and DVDs - what could they have in common_files/IMG_3136m-e2d3a.jpg)

-->

[![](http://blog.canpan.info/inagawamanyo/img/IMG_3136m-e2d3a.jpg#clickable)](http://blog.canpan.info/inagawamanyo/img/IMG_3136m-e2d3a.jpg)


**Russian:**

А если на этот бронзовый DVD диск лазером каким посветить?

Еще тут подсказывают, что к этому домашниму кинотеатру с бронзовыми дисками прилагался парогенератор для получения объемных изображений. В музейной экспозиции оные имеются.

В русской версии за парогенераторы кадило выступали.

**English:**

What if you shine a laser on this bronze DVD?

They also tell us that this home cinema with bronze disks had a steam generator for three-dimensional images. There are some of them in the museum's exhibition.

The Russian version had a censer for the steamers.


© All rights reserved. The original author retains ownership and rights.

