title: Inspired by "And there was a Country before Russia". Part 6
date: 2013-10-22
modified: Fri 02 Apr 2021 20:06:09 BST
category: 
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: Where was it all mounted? Well, it wasn't in the air.
status: published
originally: По мотивам «И была Страна до России». Часть 6.html

### Translated from:
[https://pro-vladimir.livejournal.com/2301.htmll](https://pro-vladimir.livejournal.com/2301.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html)

Unravelling the idea of a system that used a Tsar cannon with a Tsar bell, the whilring stream of thoughts came to a point: where was it all mounted? 

Well, it wasn't in the air.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/89_7.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/89_7.jpg)
-->

[![](http://mosday.ru/photos/89_7.jpg#clickable)](http://mosday.ru/photos/89_7.jpg)

We look at the bell tower of Ivan the Great.

The size of the bell and the design of the bell tower are comparable. That is, there is enough space on one of the tiers to install a bell into.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/itg-lj-big.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/itg-lj-big.jpg)
-->

[![](https://www.ljplus.ru/img4/5/_/5_14_1/itg-lj-big.jpg#clickable)](https://www.ljplus.ru/img4/5/_/5_14_1/itg-lj-big.jpg)

It turns out that orthodox architects place these types of structure in the category: "izhi under the bells", the main feature of which is the placement of bells above the temple. That is, in other words, the bell tower acted as a temple at the same time.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/kitay_sorok3_02.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/kitay_sorok3_02.jpg)
-->

[![](http://testan.narod.ru/article/kitay_sorok3_02.jpg#clickable)](http://testan.narod.ru/article/kitay_sorok3_02.jpg)

Look.

The drum (neck) of the dome. Most of them are called "light drum". For the light? Is it to illuminate the inside or the outside?

The Grail emits something in the picture. If we assume that solid diverging circles are a schematic representation of a sound or sound wave, then discontinuous uneven circles emit light? Why not?

To maximize the coverage of the system of which the bell bowl is a part, this very cup - or this very bell - must be raised as high as possible. The maximum height where the bell can be placed is the upper tier of the belfry.


![image no longer available]()

*Like a picture as a picture. Something from the annals. The Moscow Kremlin is in the background.*

We look at the bell tower of Ivan the Great. And with what intent did the artist select the light drum of Ivan the Great? 

It's a light spot.


Maybe the "artist" saw the system in action and it was quite ordinary for him to see the light from the light drum?

On the existing schematic images of the bell tower of Ivan the Great, the light drum is depicted as a blank wall.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/34203-443c0-2997790.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/34203-443c0-2997790.jpg)
-->

[![](http://data11.gallery.ru/albums/gallery/34203-443c0-29977904-m750x740.jpg#clickable)](http://data11.gallery.ru/albums/gallery/34203-443c0-29977904-m750x740.jpg)

*Translator's note: image renders but appears to be unusable.*

In the process of searching, I came across this article:
[http://astlena.livejournal.com/478167.html](http://astlena.livejournal.com/478167.html)

and thence:

Table 1 below compares the main moments of the creation of two bells on the basis of the memoirs of Aleppo and history of Tsar-bells.

Table 1.

> Bell AM 1653-4
>
> Tsar A. M. first called the masters from Austria and instructed them to make a bell. They asked him for a five-year term to make it, for works on its production and adaptations required for this purpose, very large and uncountable.
>
>Bell AI 1735
>
> Count Minich was instructed to " find a skilled person in Paris in order to make a plan of the bell kupno with all dimensions." Minich turned to "to the Royal goldsmith and member of the Academy of Sciences Germain, who to this day, the mechanic is considered the most skilled."
>
> "This artist was surprised when I told him about the weight of the bell, and at first I thought I was joking, but, convinced of the truth of the proposal, I made a plan where I increased the difficulty of the work and the cost of it to such an extent that the Empress abandoned his plans." [5]
>
> AM
>
> "They say that a Russian master appeared, a man of small stature, invisible he was a weakling, whom no one had ever thought of, and he asked the king to give him only one year's time." According to these characteristics, it is possible assume that the person was old already.
>
> AI
>
> The casting of the Central Committee was entrusted to Ivan Fedorovich Motorin (1660-1735) who was 74 years old at the time of casting the bell.
>
> AM
>
> According to Paul of Aleppo, this bell, cast by Russian master, soon split due to severe ringing and was lowered.
>
> AI
>
> The first attempt to cast the bell in 1734 was unsuccessful, the furnaces leaked.
>
> AM
> In the summer of the same year, 1654, the bell merchant Danila Danilov also died of the plague.
>
> AI
> At the beginning of 1735. Ivan Motorin died,
>
> AM
> A year later, a new bell was successfully cast.
>
> AI
> A year later, a new bell was successfully cast.
>
> AM
> When he died, and this rare thing remained spoiled, there was another one master of the survivors of the plague, a young man, short, puny, thin, under twenty years of age, still beardless.
>
> AI
> The son of Fyodor Motorin cast a new bell.
>
> AM
> Five furnaces were built for metal melting. Each oven delivered 2,500 pounds, and a total of 12,500 pounds.
>
> AI
> 
> The metal for the bell was smelted in four melting flaming furnaces. furnaces installed around the foundry pit. Each furnace held up to 50 tons metal. i.e. 12,500 pounds![6]
>
> For the Tsar Bell, rather complex calculations of its weight are given: "from the first casting, 14,814 poods 21 lb (242 t 662 kg) of metal remained, to this 498 poods 6 lb (8 t 160 kg) of tin was added. Total at the second smelting was 15,312 poods of 27 pounds (250 t 822 kg) of metal. The remainder turned out to be 2985 poods of 8 pounds (48 t 898 kg) of metal, therefore, minus the carbon monoxide, the cast bell weighs 12,327 pounds. pounds, or 201 tons 924 kg. Losses amounted to 1.3%" [7]. With a copper burn of 1.3%, the weight of Alexey Mikhailovich's bell turns out to be almost equal weight of the Central Committee-12337.5 poods! It is unlikely that such a coincidence can be random.

The engraving on the bell does not show the inscription, but it should be:

![image no longer available]()

There are two inscriptions on the Tsar bell. First: 

> "Blessed and technodolly memory of the great sovereign Tsar and Grand Prince Aleksei Mikhailovich, of all Great and Little and White Russia Autocrat commands in pervoozernaya virgin Mary Church fair and glorious in Her assumption, the merged was a great bell of eight thousand poods of copper containing in itself, in summer from world creation 7162, with Christmas on the flesh of God the Word 1654; copper this blagovesta began in the summer of the universe 7176, Christ's Nativity 1668 and rung until the summer of 7208 of the universe, of the Nativity of the Lord 1704, in which the month of June the 19th day from the great in the Kremlin of the former fire damaged; to 7239 summer from the beginning of Christ, into the world of Christmas, arriving mute".

The second inscription: 

> "Blagochestivaya and Samoderzhaviye Velikoj of Empress Anna Ioanovna, the Autocrat of all Russia commandment for the glory of God in Trinity in honor of the blessed mother, in perosamine of the Church of the glorious assumption of Her cast bell copper perednego eight thousand pounds, from the creation of the world in 7242, from Christmas in the flesh of God the Word 1734, prosperous of her Majesty's reign in the fourth summer."

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/17.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/17.jpg)
-->

[![](http://www.biancoloto.com/kreml/17.jpg#clickable)](http://www.biancoloto.com/kreml/17.jpg)

Somewhat confused by the quality of execution of the inscription. More precisely, it looks more like a nail-scratched "Vasya was Here".

And then:

> AM
>
> According to Paul of Aleppo, this bell, cast by Russian master, soon split due to severe ringing and was lowered.

Wow! This is how it was necessary to beat a multi-ton suspended structure so that it split?

Or maybe this is the bell a la Grail? Well, as an assumption. And all other inscriptions on it were made much later. Well, it's the same in Egypt: the pharaohs did not disdain to sign with their name buildings they did not build. It may be the same case here. We do that sometimes too.

So how did the Tsar bell supposedly get its damage?

There are eleven cracks, almost evenly spaced around the perimeter.

In a standard bell suspension, this is not very easy to do. Even if you try very hard. There is an opinion [http://yablor.ru/blogs/kto-razbil-car-kolokol/1150350](http://yablor.ru/blogs/kto-razbil-car-kolokol/1150350) from here:

![image no longer available]()

Eleven cracks along the perimeter of the bell in its lower part are 4 meters long. Two came together – and broke off a considerable piece (those who did not seeit, go look). It's simple: the bell shrank from cooling, and the blockhead under it did not collapse. So the bell was destroyed.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/9408_600.png#clickable)](По мотивам «И была Страна до России». Часть 6._files/9408_600.png)
-->

[![](https://ic.pics.livejournal.com/new_guid/32393246/9408/9408_600.png#clickable)](http://ic.pics.livejournal.com/new_guid/32393246/9408/9408_original.png)

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/9560_600.png#clickable)](По мотивам «И была Страна до России». Часть 6._files/9560_600.png)
-->

[![](https://ic.pics.livejournal.com/new_guid/32393246/9560/9560_600.png#clickable)](https://ic.pics.livejournal.com/new_guid/32393246/9560/9560_600.png)

Now we return to the version that we are considering.

The Tsar-cannon, acting as the base - as the foot - hammers on the bell that is upside down above it. The resonance - the blows - the simple force of gravity that acts on the skirt of the bell. Here's a dummy for casting. No, not a dummy, it turns out that the "speaker" was torn by its installers. They put pressure on everything and they broke it.

![image no longer available]()

With regard to the design of the executive mechanism. We won't know for sure what she looked like. But there is such an image in the Web:

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/gal_Stolb.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/gal_Stolb.jpg)
-->

[![](http://www.nhat-nam.ru/biblio/rusgods/gal_Stolb.jpg#clickable)](http://www.nhat-nam.ru/biblio/rusgods/gal_Stolb.jpg)

The desire of everyone to see the phallic symbol everywhere can be forgiven.

Dachshunds' leg = trunk. The image on the trunk suggests that there are some elements inside that we have not yet considered. Vividly expressed resonators of the bell support. Next, a wavy strip of light, a spark gap, the form of a power ball, and the bell tower dome itself. Which, in the picture, looks more like a glans penis.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 6._files/h-5.jpg#clickable)](По мотивам «И была Страна до России». Часть 6._files/h-5.jpg)
-->

[![](http://content.foto.mail.ru/mail/mamzerev/2/h-5.jpg#clickable)](http://content.foto.mail.ru/mail/mamzerev/2/h-5.jpg)

And that, according to modern interior designers, is how the torch of antiquity should look.

The Tsar cannon is also referred to as a shotgun. For some reason, "drum roll" came to mind. A fraction of the kibble? The Tsar cannon was beating a fraction, which was already being broken up at the wrong system frequency by broadcasting the Tsar bell?

Vladimir Mamzerim 22.10.2013 g.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-7.html)

© All rights reserved. The original author has asserted their ownership and rights.

16 comments:

Generic post from 1999 m. Zh.
dmitrijan
2013-10-22 08: 01 am (UTC)
Generic column from 1999 m. Zh. (SMZH) Ie if now 7522 SMZH and 2013 RH, then this... more than 5523 years ago! Or 3510 year NDA. What's in Egypt? The rise of monotheism? The heyday of Antithesis.

At the same time, 831 from the fall of the dragon, this is ... 4341 years ago, the last Dragon lost its power (fell) in the middle Kingdom. And the Chinese are counting their years based on this event. They have like 4341 years in the yard from the departure of the Dragon in the sky?

Re: Generic post from 1999 m. Zh.
vyatich_43
2014-05-15 01: 18 pm (UTC)
From the fall of Arkona. You need to be more careful.

Re: Generic post from 1999 m. Zh.
dmitrijan
2014-05-15 01: 40 pm (UTC)
And where did arcona fall?

Re: Generic post from 1999 m. SX.
vyatich_43
2014-05-15 08:34 pm (UTC)
There is a view of the destruction.


Re: Generic post from 1999 m. Zh.
dmitrijan
2014-05-16 04: 38 am (UTC)
Let it be so. And of course you can easily tell the details?


"drum roll"
dmitrijan
2013-10-22 08: 11 am (UTC)
"drum roll"
DRB-duckweed, small tremor. good - dark radiance of happiness, warmth. To bring good - to warm the soul with warmth. Robit-to see highlights, flashes, like fog, such a haze in the air, like steam.

The usual drum roll causes such a vibration in the air, if you put your finger on it.

They also say-shy-small shaking inside.


eaquilla
2013-10-22 02:30 pm (UTC)
I still don't know if the Tsar bell has a language or not. And the eyelet under it?

If there is, then there are no questions. If not - then there is only the purely technical details. :)


dmitrijan
2013-10-23 09: 31 am (UTC)
Officially there, but no one has ever seen them together, only next to each other.


dmitrijan
2013-10-25 12:17 pm (UTC)
Ivan and Mary: [http://dmitrijan.livejournal.com/91508.html](http://dmitrijan.livejournal.com/91508.html)
