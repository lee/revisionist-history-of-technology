title: Another history of the Earth - Part 1e2
date: 2017-03-30
modified: 2022-12-14 16:04:34
category:
tags: catastrophe
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/224787.html
originally: Another history of the Earth - Part 1e2.html
local: Another history of the Earth - Part 1e2_files
summary: I want to mention another inaccuracy in the diagrams that how the edges of the oceanic plates plunge into the mantle to a depth of 600 km before we move on to considering other facts that are the consequences of the catastrophe.
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/224787.html](https://mylnikovdm.livejournal.com/224787.html)

[First article in series (Russian)](http://mylnikovdm.livejournal.com/219459.html)

[First article in series (English)](./another-history-of-the-earth-part-1a.html) 

I want to mention another inaccuracy in the diagrams that how the edges of the oceanic plates plunge into the mantle to a depth of 600 km before we move on to considering other facts that are the consequences of the catastrophe.

Few people think about the fact that lithospheric plates actually float on the surface of molten magma for exactly the same reason that ice floats on the surface of water. The fact is that during cooling and solidification, the substances that make up the earth's crust crystallize. And in crystals, the distance between atoms in most cases is slightly greater than when the same substance is in a molten state and atoms and ions can move freely. This difference is very insignificant, for the same water there is only about 8.4%, but this is enough for the density of the solidified substance to be lower than the density of the melt, due to which the frozen fragments float to the surface.

With lithospheric plates, everything is somewhat more complicated than with water, since both the plates themselves and the molten magma on which they float consist of many different substances with different densities. But the general ratio of the density of lithospheric plates and magma should be met, that is, the total density of lithospheric plates should be slightly less than the density of magma. Otherwise, under the influence of gravitational forces, the lithospheric plates should begin to gradually sink down, and molten magma should begin to flow out very intensively from all the cracks and faults, of which there are a large number.

But if we have a solid matter that makes up an oceanic plate, has a lower density than the molten magma into which it is immersed, then a buoyant force (the force of Archimedes) should begin to act on it. Therefore, all the zones of the so-called "subduction" should look completely different from how they are now drawn to us.

Now on all the diagrams the region of "subduction" and subsidence of the end of the oceanic plate is depicted as in the upper diagram.
<!-- Local
![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/168787_900.png#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/168787/168787_900.png#resized)

But if our instruments by indirect methods really record the presence of some anomalies, then if these are precisely the edges of oceanic plates, we should observe the picture as in the lower diagram. That is, due to the buoyancy force that acts on the end of the plate, which is submerged down, the opposite end of this plate should also rise. Here are just such structures, especially in the region of the coast of South America, we do not observe. And this means that the interpretation of the data obtained from the devices proposed by the official science is erroneous. The instruments actually record some anomalies, but they are not the edges of the oceanic plates.

Separately, I would like to emphasize once again that I do not set myself the goal of "putting things in order" in the existing theories of the Earth's internal structure and the formation of its appearance. Also, I have no goal to develop some new, more correct theory. I am perfectly aware that for this I do not have enough knowledge, facts and time. As it was rightly noted in one of the comments: "the bootmaker should sew boots". But, at the same time, in order to understand that the craft offered to you in fact is not any kind of boots, you do not need to be a shoemaker yourself. And if the observed facts do not correspond to the existing theory, then this always means that we must recognize the existing theory as either erroneous or incomplete, and not discard facts inconvenient for the theory or try to distort them so as to fit into the existing erroneous theory.

Now let's return to the described disaster and look at the facts that fit well into the model of the disaster and the processes that should occur after it, but at the same time contradict the existing officially recognized theories.

Let me remind you that after the breakdown of the Earth's body by a large space object, presumably having a diameter of about 500 km, a shock wave and a flow along the channel pierced by the object were formed in the molten layers of magma, directed against the daily rotation of the planet, which ultimately should have led to the fact that the outer the solid shell of the Earth slowed down and rotated relative to its stable position. As a result, a very strong inertial wave should have appeared in the oceans, since the waters of the world's oceans should have continued to rotate at the same speed.

This inertial wave should go almost parallel to the equator in the direction from West to East, and not in some particular place, but across the entire width of the ocean. This wave, several kilometers high, meets the western edges of the continents of North and South America on its way. And then it begins to act like a bulldozer knife, washing away and raking up the surface layer of sedimentary rocks and crushing with its mass, increased by the mass of washed away sedimentary rocks, the continental plate, turning it into an "accordion" and forming or strengthening the mountain systems of the Northern and Southern Cordilleras. I want to once again draw the attention of readers to the fact that after the water begins to wash away sedimentary rocks, it is no longer just water with a specific density of about 1 ton per cubic meter, but a mudflow, when washed away sedimentary rocks are dissolved in the water, therefore, firstly,

Let's take another look at the relief maps of the Americas already cited.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/169154_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169154/169154_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/169154/169154_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169154/169154_original.jpg)

In North America, we see a very wide brown stripe that corresponds to an altitude of 2 to 4 km and only small patches of gray that correspond to an altitude of over 4 km. As I wrote earlier, on the Pacific coast we observe a rather sharp elevation change, but there are no deep-water trenches in front of the faults. At the same time, North America has one more feature, it is located at an angle of 30 to 45 degrees to the direction to the North. Consequently, when the wave reached the coast, it partly began to rise and enter the mainland, and partly, due to the angle, deviate downward to the south.

Now let's look at South America. There the picture is somewhat different.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/169308_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169308/169308_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/169308/169308_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169308/169308_original.jpg)

First, the strip of mountains here is much narrower than in North America. Secondly, most of the area is silver colored, that is, the height of this area is over 4 km. In this case, the coast forms an arc in the middle and, in general, the coastline is almost vertical, which means that the impact from the approaching wave will also be stronger. Moreover, it will be strongest in the bending of the arc. And it is there that we see the most powerful and highest mountain formation.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/169589_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169589/169589_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/169589/169589_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169589/169589_original.jpg)

That is, exactly where the pressure of the approaching wave was supposed to be the strongest, we just see the strongest deformation of the relief.

If you look at the ledge between Ecuador and Peru, which juts out into the Pacific Ocean like the bow of a ship, then the pressure there should be noticeably less, since it will cut and deflect the oncoming wave to the sides. Therefore, there we see noticeably less deformations of the relief, and in the region of the tip there is even a kind of "dip", where the height of the formed ridge is noticeably less, and the ridge itself is narrow.

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/169773_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169773/169773_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/169773/169773_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/169773/169773_original.jpg)

But the most interesting picture is at the lower end of South America and between South America and Antarctica!

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/170013_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170013/170013_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/170013/170013_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170013/170013_original.jpg)

First, between the continents one can clearly see the "tongue" of flushing, which remained after the passage of the inertial wave. And secondly, the very edges of the continents adjoining the washout between them were noticeably deformed by the wave and bent in the direction of the wave's motion. At the same time, it is clearly seen that the "lower" part of South America is all, as it were, torn to shreds, and a characteristic light "train" is observed on the right.

I suppose that we observe this picture because a certain relief and mountain formations in South America should have existed before the cataclysm, but were located in the central part of the continent. When the inertial wave began to approach the mainland, then reaching the elevation, the speed of movement of the water should have decreased, and the wave height should have increased. At the same time, the wave had to reach its maximum height exactly in the center of the arc. Interestingly, it is in this place that there is a characteristic deep-sea trench, which is not found along the coast of North America.

But in the lower part of the mainland before the catastrophe the relief was lower, so there the wave almost did not lose speed and simply flowed over the land, carrying further the sedimentary rocks washed away from the mainland, which formed a light "trail" to the right of the mainland. At the same time, in the continent itself, powerful streams of water left traces in the form of a multitude of gullies, which, as it were, tear the southern end into small pieces. But above, we do not see such a picture, since there was no rapid through flow of water across the land. The wave ran into a mountain ridge and slowed down, crushing the land, so there we do not observe a large number of gullies, as below. After that, most of the water, most likely, passed through the ridge and flowed into the Atlantic Ocean, while the bulk of the washed-out sedimentary rocks settled on the mainland, so we do not see a light "plume" there.

Also interesting is the form of the "tongue" that was formed in the washout between the continents. Most likely, before the catastrophe, South America and Antarctica were connected by an isthmus, which was completely washed out by an inertial wave during the catastrophe. At the same time, the wave dragged the washed away soil for almost 2,600 km, where it precipitated, forming a characteristic semicircle when the power and speed of the wave dried up.

But, what is most interesting, we observe a similar "ravine" not only between South America and Antarctica, but also between North and South America!

<!-- Local
[![](./Another%20history%20of%20the%20Earth.%20Part%201e2.%20-%20Dmitry%20Mylnikov%20-%20LiveJournal_files/170297_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170297/170297_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/170297/170297_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/170297/170297_original.jpg)

At the same time, I assume that this washout was also through, as well as below, but then, due to active volcanic activity, it closed again. At the end of the washout, we see exactly the same arcuate "tongue", which indicates the place where the power and speed of the wave dropped, due to which the washed-out soil precipitated.

The most interesting thing that makes it possible to link these two formations is the fact that the length of this "language" is also about 2600 km. And this, well, can in no way be a coincidence! It seems that this is exactly the distance that the inertial wave was able to travel until the moment when the outer solid shell of the Earth regained its angular velocity of rotation after the impact and the inertial force stopped creating the movement of water relative to land.

[*Continuation (Russian)*](http://mylnikovdm.livejournal.com/225365.html)

[Next Part (English)](./another-history-of-the-earth-part-1-and.html)

© All rights reserved. The original author retains ownership and rights.

