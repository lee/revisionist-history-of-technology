title: The Grand Canyon in the USA - an ancient uranium quarry
date: 2016-03-07
modified: Wed 07 Jul 2021 09:59:27 BST
category:
tags: quarried Earth
slug:
authors: WakeUpHuman
summary: The reforestation of the Crimea, as well as the entire planet, apparently began with the founding of the Botanical Gardens. When reading about Botanical Gardens, pay attention to the date of foundation, last name and country of the founder. For example, the date of foundation of the Nikitsky Botanical Garden in Yalta is 1811.
status: published
local: The Grand Canyon in the USA - an ancient uranium quarry_files/
from: https://wakeuphuman.livejournal.com/1877.html
originally: The Grand Canyon in the USA - an ancient uranium quarry.md

#### Translated from: 

[https://wakeuphuman.livejournal.com/1877.html](https://wakeuphuman.livejournal.com/1877.html)

The Grand Canyon in the USA - an ancient uranium quarry

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/144986_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/144986/144986_original.jpg#resized)

Good afternoon, dear readers.

I suggest you continue to rock the dilapidated building with a sign above the entrance - "The Official History of Mankind." Many readers in the comments to my last article - "An industrialized civilization has existed on Earth for tens of thousands of years" located here:
[http://wakeuphuman.livejournal.com/1342.html](http://wakeuphuman.livejournal.com/1342.html) posed frequently asked questions:  

1. What does the author smoke?  
2. Does he sleep?  

Answer:

1. In my free time I often smoke books and articles on a variety of topics.  
2. Maybe. I'm sleeping right now :)

From the title of the article, it is clear that it will be about uranium mining in the United States, but not only. The material will be much wider. I will try to give you all the search queries that I have used, so that you can not only check the information yourself, but also personally participate in the discovery of new interesting facts. Very soon you will realize that the man in the photo above with the "Stop the uranium mine" poster, protesting against the discovery of new quarries for uranium in the Grand Canyon, unknowingly, is protesting like a bee against honey. In fact, it protects the ancient uranium mine from further development! Oxymoron:)  
  
One of the rules that I use to search for traces of industrial extraction of resources in ancient times sounds like this: if a certain resource was previously mined in one place and did not fully develop the entire volume, then other people, no matter how many years later, will return to this place and continue prey. I will illustrate this thesis with an example from the Crimea. The video shows two limestone quarries. One is modern, and opposite it, across the road, is an ancient one. Judging by water and wind erosion, the ancient one is several thousand years old. Be sure to look for clarity. The video is short, only 30 seconds.

[![Video compares appearance of modern and ancient limestone quarry](https://img.youtube.com/vi/U-4YObuh5oU/0.jpg)](https://www.youtube.com/watch?v=U-4YObuh5oU)

*Video compares appearance of modern and ancient limestone quarry*

Following this rule, you can easily download from the Internet maps of active modern fields in any country or region for any element of interest in the periodic table, as well as for any combination of elements, and then simply visually compare. It's easy, informative, exciting. Like a game quest. To search for such maps, we use search words:  

- Map of mineral resources of Russia  
- Map of mineral resources of the region such as  
- Map of minerals of Russia  
- Map of minerals of the region such as  
- Map of copper ore  
- Map of uranium ore reserves
- Map of bauxite reserves

and so on. Then click show pictures. Similarly, repeat the search in different languages for other countries.Now I will show you using the example of an ancient mine - the Grand Canyon in the United States, the length of which is 446 km, the width (at the plateau level) ranges from 6 to 29 km, at the bottom level - less than a kilometer. Depth - up to 1800 m.

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/145935_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/145935/145935_original.jpg#resized)

I found a map of the United States, which marked areas with high uranium content for the search query **uranium mining reserves USA** :

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/117869_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/117869/117869_800.jpg#resized)

And the second map - with the location of uranium mines in the United States:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/118265_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/118265/118265_800.jpg#resized)

Then I compared the top maps with the location of the Grand Canyon:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/118305_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/118305/118305_800.jpg#resized)

*Map: Grand Canyon*

The Canyon sits in the zone of maximum uranium concentration. Then I narrowed down the search criteria and began to read the material found by searching for **Grand Canyon Uranium mining** . And I found interesting material. I demonstrate some of them: Articles found by searching for: **Uranium mining near the Grand Canyon should be permanently banned**  
[https://intercontinentalcry.org/uranium-mining-near-the-grand-canyon-should-be-permanently-banned/](https://intercontinentalcry.org/uranium-mining-near-the-grand-canyon-should-be-permanently-banned/)

And map with applications for uranium mining around the Grand Canyon from the article:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/118724_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/118724/118724_800.jpg#resized)

And the second map:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/118934_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/118934/118934_800.jpg#resized)

*Grand Canyon*

The map clearly shows that the undeveloped areas around the Grand Canyon are of great interest to uranium mining companies. Do you understand what I'm driving at? :) That is, earlier, we did not have time to fully develop all the uranium-containing rock in this area. Only the volume was developed, which later became the Grand Canyon. In the area of the "Canyon" there are a lot of decent "phoning" places, as warned by the signs:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/119129_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/119129/119129_800.jpg#resized)

*Grand Canyon Uranium sign*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/119997_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/119997/119997_800.jpg#resized)

*Grand Canyon Uranium sign*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/120228_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/120228/120228_800.jpg#resized)

*Grand Canyon Uranium sign*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/119494_800.png "grand_canyon_uran1"#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/119494/119494_800.png#resized)

*Grand Canyon Uranium sign*

Plus a 2011 video about radioactive rain in the Grand Canyon. The radioactive background has increased 2.7 times. The author of the video blames the Fukushima accident, which happened six months earlier. But Japan is far away; therefore, I believe that radioactive local dust fell out along with the rain, which was lifted into the atmosphere by the wind:  

[![Video shows radioactive rain in the Grand Canyon](https://img.youtube.com/vi/kvyXtMVwSZI/0.jpg)](https://www.youtube.com/watch?v=kvyXtMVwSZI)

*Video shows radioactivity of rain over the Grand Canyon*

To find out more about radiation in the vicinity of the Grand Canyon, google the search term - **Grand Canyon Radiation**.

How is it? Is history starting to play with new colors for you? Whoever had a huge amount of uranium on hand long ago, which could be used both for energy and for the production of nuclear weapons. Are you still interested in the official stories of how past generations exchanged sable skins for hemp and chased them across the seas on oared wooden galleys and sailing gulls? Maybe they changed and drove, but studying this simple life is like studying now the history of the Maori people in Australia, while such transnational corporations as BHP Billiton, Rio Tinto, Glencore Xstrata conduct their mining and metallurgical activities next to them. And Alcoa.

Using the above example, you can now independently explore the terrain in your area. And thus, by cooperating, in contact with miners, workers in the mining and processing industry who know those processes from the inside, you can put together this puzzle completely. Remember all:)

Now you need to imagine that you have a planet in front of you, on which you need to deploy a full-fledged mining and processing industry. You have a limited amount of equipment. The first thing you will start with is increasing its quantity. What is needed for this first of all? Energy. Any manipulation of matter requires energy. And then steel. No machine or plant can be built without a wide range of different grades of steel. And to produce steel, you need iron ore, alloying additives - chromium, nickel, molybdenum, manganese, etc., coal and flux limestone.

Coal is generally needed for the reduction of any metal oxides. Oxygen atoms in the blast furnace are taken away from the metal oxide as a result of a reducing chemical reaction and are attached to the carbon contained in the coal. Limestone and dolomite are used as fluxes in metallurgical processing of ores in order to form fusible slags for easier removal of impurities.

> "_Their widespread use in ferrous metallurgy is due to the fact that a significant amount of basic oxides is required for the fluxing of waste ore and coke ash. In addition, most of the production processes are aimed at removing harmful impurities that can be removed from the melt in whole or in part when working on basic slags. For the formation of the latter, significant additions of the main flux are required. The most important requirement for them is a low content of silica, alumina and harmful impurities (sulfur and phosphorus)_ . "

That is, without limestone - nowhere.

Here is a blast furnace loading scheme. Limestone - limestone, coal - coal, iron ore - iron ore:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/120975_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/120975/120975_800.jpg#resized)

As far as coal is concerned, everything should be clear from my [last article](http://wakeuphuman.livejournal.com/1342.html): all burning cone volcanoes are, most likely, coal waste heaps. Here, by analogy with the coal waste heaps of Donbass, you need to understand. They contain a decent amount of remains of coal dust and crumbs, and therefore such waste heaps and volcanoes-waste heaps burn very actively. The color of the constituent rock in the Donbass waste heaps and volcanoes is the same. You can try to compare the location of volcanoes with a map of coal basins in different countries.

By the way, the statement that the volcanoes are burning waste heaps received a critical comment that waste heaps cannot have a layered internal structure like the layering seen in the photo:

Nyamlagira waste heap  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/148041_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/148041/148041_original.jpg#resized)

Volcano: Popocatepetl waste heap volcano:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/152999_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/152999/152999_800.jpg#resized)

And they should have a uniform structure inside, like an anthill cake. However, I put forward a counterargument: conical waste heaps are poured using transport belts, as in the photo:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/121748_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/121748/121748_800.jpg#resized)

*Ore pile*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/122016_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/122016/122016_800.jpg#resized)

*Ore pile*

A similar process can be observed in an hourglass. With this method of filling, layers of different colors of the rock will inevitably form and the layers will be parallel to the surface of the slopes of the waste heap. The photo below shows the result of emulating this process. It is called stratification:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/122288_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/122288/122288_800.jpg#resized)

*Stratification by pouring*

That is, volcanoes are waste heaps. Here is another very clear proof of this statement:  45 years ago in the Donetsk region a waste heap exploded. Contemporaries included it in the list of man-made disasters in Ukraine. The below article about it was titles: **"Looking around, I involuntarily recalled the painting" The Last Day of Pompeii ""** . Quote:  

> On June 10, 1966, at 23.00, a piece with a total volume of 33 thousand cubic meters split off from the old waste heap of the Dimitrov mine of the Krasnoarmeyskugol trust in the city of Dimitrov (Donetsk region). Hot boulders of many tons and a free-flowing mass of incandescent rock slid onto the residential village, burying a dozen houses under them together with people. After the displacement of rock masses from the cavity formed in the lateral part of the one-hundred-meter waste heap, like from a volcano vent, an ejection of hot ash, dust and steam occurred, the temperature of which reached 3000 (!) Degrees. The tragedy was first written about only 30 years later ...

I recommend reading it in full at:  

[http://fakty.ua/134377-glyadya-vokrug-ya-nevolno-vspominal-kartinu-poslednij-den-pompei](http://fakty.ua/134377-glyadya-vokrug-ya-nevolno-vspominal-kartinu-poslednij-den-pompei)

And one more article on the eruption of a waste heap in Dimitrov in 1966. The quote is extremely interesting, especially for those who value the opinion of the authority, and not just reasonable, testable arguments:  

> "About the death of the village of Nakhalovka, and the danger that waste heaps pose today, an eyewitness, a person who took part in the investigation of the explosion, told the "Observer" AIN of Ukraine, Doctor of Technical Sciences, Professor, Head of the Department of the National Mining University, Director of the Research Institute of Mining Mechanics named after M.M. Fedorov, Boris the Coming:

> **"Volcanic eruption**... In the truest sense of the word. After all, our waste heaps are layers of rock, coal, issued from the mine, and many other elements, including rare earth metals and in the coal itself. So: the temperature in the center of such a dump of rock, especially a cone-shaped one, **exceeds 3-4 thousand degrees** . **That is, in fact, the city of Donetsk and mining towns are surrounded by slowly progressing volcanoes**... There is a good, beautiful song about Donetsk - a city with blue waste heaps, a city of silver poplars. But blue waste heaps are not a poetic metaphor. At night you can see the glow above the waste heaps. The blue glow creates the high temperature inside this waste heap, as well as the radiation of rare earth metals. And any impact of storm flows on the waste heap can lead to catastrophic consequences."

[Http://terrikon.donbass.name/ter\_s/318-vzryv-terrikona-v-dimitrove-v-1966-godu.html](http://terrikon.donbass.name/ter_s/318-vzryv-terrikona-v-dimitrove-v-1966-godu.html)

By the way, on the shores of the seas and rivers , you can often see a conical mountain half collapsed from the side of the water, consisting of layered sandstone. Perhaps this is an ancient compressed waste heap. Examples in the photo:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/122477_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/122477/122477_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/122703_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/122703/122703_800.jpg#resized)

Now let's move on to iron ore mining. I want to show you some interesting analogies. A photo of Danxia geological park in China already appeared in the [last article](http://wakeuphuman.livejournal.com/1342.html).

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/123039_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/123039/123039_800.jpg#resized)

I will add the town of Purmamarca, Andes, Argentina:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/126720_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/126720/126720_800.jpg#resized)

*Salinas Grandes*

Follow the link, take a look at the 360-degree photo panorama of Purmamarca dumps in Argentina:  
[Link to the](https://www.google.com/maps/@-23.7434283,-65.5071183,3a,75y,203.68h,77.04t/data=!3m8!1e1!3m6!1s-VPTfW-YZKgM%2FVX8Ysljq_aI%2FAAAAAAAAdak%2FqIWjIR_tt7A!2e4!3e11!6s%2F%2Flh3.googleusercontent.com%2F-VPTfW-YZKgM%2FVX8Ysljq_aI%2FAAAAAAAAdak%2FqIWjIR_tt7A%2Fw203-h101-n-k-no%2F!7i7168!8i3584)

Hornocal Mountains photo panorama, Argentina:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/123600_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/123600/123600_800.jpg#resized)

Altai:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/153317_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/153317/153317_800.jpg#resized)

Mountains: Altai Mountains:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/153409_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/153409/153409_800.jpg#resized)

Skazka Gorge, Kyrgyzstan... Erosion-eroded dumps:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/154919_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/154919/154919_800.jpg#resized)

Skazka Gorge, Kyrgyzstan:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/155306_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/155306/155306_800.jpg#resized)

Red Mountains, Kazakhstan:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/154461_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/154461/154461_800.jpg#resized)

Red Mountains, Kazakhstan:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/154807_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/154807/154807_800.jpg#resized)

Mountains of Vinicunca, Peru:

![11-Mountains-of-Vinicunca-P](https://ic.pics.livejournal.com/wakeuphuman/74019716/123757/123757_800.jpg#resized)

Compare them to the following modern dumps:  

Colorful depleted iron ore dumps:
![12-colorful-depleted-iron-o](https://ic.pics.livejournal.com/wakeuphuman/74019716/125376/125376_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/124404_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/124404/124404_800.jpg#resized)

Iron ore: 
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/124471_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/124471/124471_800.jpg#resized)

Iron ore:

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/124863/124863_800.jpg#resized)

It is clearly visible that the depleted rock, after being refined, forms colored layers in the dumps. Enriched iron ore often also has bright colors. But it is not only iron ore dumps that are colored. As examples of other metals that leave coloured tailings, I will cite the heaps of a copper mine and bauxite.

Kennecott Utah Copper (Heaps of a copper mine):

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/153610_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/153610/153610_800.jpg#resized)

A dump of the Krasnooktyabrsky bauxite deposit. Kazakhstan:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/154294_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/154294/154294_800.jpg#resized)

Kamenushinsky quarry:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/155599_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/155599/155599_800.jpg#resized)

By analogy, one can continue to compare endlessly. Do you see the difference between the Poldnevsky quarry in Russia:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/155881_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/155881/155881_800.jpg#resized)

AND the below "natural" Serhed mountains in Iran? I do not see it.
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/156056_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/156056/156056_800.jpg#resized)

The work of the Abzetzerov (German Absetzer) - bucket chain excavator for repacking soft and loose rocks in dumps:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/156462_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/156462/156462_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/156839_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/156839/156839_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/157072_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/157072/157072_800.jpg#resized)

Let's go back to the town of Purmamarca in Argentina. Let's look at a fragment of the Andes mountain range in the area of the town from a satellite. Coordinates: -23.654545, -65.653234. Let's raise the camera, take a screenshot of the terrain, ~ 150 km wide. Click the picture to enlarge:

<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/126509_800.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/126509/126509_original.jpg)
-->

[![](http://ic.pics.livejournal.com/wakeuphuman/74019716/126509/126509_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/126509/126509_original.jpg)

In the screenshot in red, I circled a small fragment of the Andes, 100 kilometers in diameter. These are colored dumps from mining and metallurgical activities, and naturally not only iron was mined here, but the entire periodic table. You can zoom in on the camera, take a look. And it is better to examine all the Andes at once. A sure sign of dumps and waste heaps is the erosion of their slopes. It appears under the influence of precipitation. The slopes are covered with water. If you see mountains, the slopes of which are covered with such waters, then these mountains are formed from bulk materials. Hard rock fragments can even stick out from their tops, but do not be confused by this, since exothermic reactions often go inside the dumps and waste heaps and bulk material can melt. It can even just be caked. A striking example is the sandstone. Solid rock formed from sand.

Photo of dumps with water weeds:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/125523_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/125523/125523_800.jpg#resized)

You should take a closer look at the hills and mountains with such erosion. The shape of the mountains does not really matter, it can be any, especially considering the repeated recycling of dumps.

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/126343_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/126343/126343_800.jpg#resized)

*Eroded waste piles*

Layers of different colors in the dumps will be formed in the way shown below:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/127022_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/127022/127022_800.jpg#resized)

*Waste Rock storage*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/127467_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/127467/127467_800.jpg#resized)

*Waste Rock Pile Structure*

Note the bottom of the salt lake in the satellite screenshot above. I circled it in green. It is called Salinas Grandes and is 45 km long. The distance from it to the ocean is 450 km:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/127740_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/127740/127740_800.jpg#resized)

Here are photos of the lake and its surroundings.
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/127755_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/127755/127755_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/128128_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/128128/128128_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/128258_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/128258/128258_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/128693_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/128693/128693_800.jpg#resized)

About this salt marsh (and thousands of others like it on the planet) you need to know two things:

1\. It is being re-mined. Salt, potash, borax and soda are mined.  
2\. And the second thing that is directly related to these types of lakes is the following:

Methods for chemical processing of ores can be divided into two main groups: acidic and alkaline. As a result of the dissolution of mineral raw materials, the elements of interest and their compounds pass into solution, from which they are then extracted by filters with thickeners and vacuum filters. The brine remaining from the process is discharged into sludge storage tanks. The sludge pond is the main type of surface storage, which is built according to a single or multi-stage principle with the creation of a dam, banks, and also a sludge storage. Natural processes occur in the sludge ponds - the accumulation of atmospheric precipitation, the development of microorganisms, the course of oxidative and other processes, i.e. self-healing is in progress, however, due to the presence of a large amount of salts with a general lack of oxygen, the self-healing process takes tens and hundreds of years. Search Google images for **sludge**, **pond**, **tailings pond** or **sludge pond** . I will show you photos of operating sludge collectors. They accumulate tens of meters of thickness of liquid waste.

Tailings pond at Stawell mine:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/128802_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/128802/128802_800.jpg#resized)

Tailings dam at Tanjianshan. To create a sludge pond, a dam is built. Over time, on ancient dry sludge ponds, the dam can degrade, lose its shape. This will make it possible to pass the object off as a salt marsh.  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/129232_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/129232/129232_800.jpg#resized)

Tailings (sludge) dam at the Sierrita Copper Mine:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/129290_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/129290/129290_800.jpg#resized)
Sludge collector: Belaruskali. Future mountains on the horizon and a dry salt lake:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/131312_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/131312/131312_800.jpg#resized)

Alberta Tar Sands Tailings Pond:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/137024_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/137024/137024_original.jpg#resized)

Tailings Pond: Ernest Henry Mine:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/130096_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/130096/130096_800.jpg#resized)

"White Sea:"

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/148655_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/148655/148655_original.jpg#resized)

Sludge pond: Berezniki, Perm Territory: Construction of a dam for a sludge pond Highland Valley Copper EYNAKR  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/130314_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/130314/130314_800.jpg#resized)

Here is a diagram of a sludge pond dam. Sludge-tailings are marked in gray:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/132740_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/132740/132740_800.jpg#resized)

Options for sludge collectors:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/134512_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/134512/134512_original.jpg#resized)

Sometimes the dams of the sludge ponds breaks through. And then the sludge floods the settlements located below:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/137271_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/137271/137271_original.jpg#resized)

The consequences of a breakthrough in Hungary. This is sludge from bauxite processing. Aluminum mining:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/137665_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/137665/137665_original.jpg#resized)

Consequences of a breakthrough in Brazil:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/137885_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/137885/137885_original.jpg#resized)

Most of the reservoirs, by the way, with earthen dams, are former quarries used as flooded sludge storage. I do spearfishing and have dived in many of them in Crimea. In the Partizansky reservoir, the Simferopol reservoir, the Schaslyve reservoir. Everywhere the same picture was observed - underwater ledges, horizontal shelves of the bottom of a large area, for example, at 5-7 meters in depth, which at a considerable distance from the coast abruptly break off with a steep drop into the depths. The bottom composition is white lime pulp, fine lime crumb. and it is often impossible to dive to the bottom, because the transparency at a depth of 7-12 meters drops sharply to zero due to the white lime emulsion, which, as a level, is in the horizontal plane.

Here is a photo of the Schastlivsky reservoir in the Crimea. The hills in the background are loose.

Blades:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/130766_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/130766/130766_800.jpg#resized)

In support of this statement about reservoirs, I bring an interesting piece of news. After Crimea returned to Russia, we switched to Russian standards. And the Gasfort lake near Sevastopol, in which I also dived, changed from 'reservoir' status to 'sludge storage' status. At the same time, Gasfort Lake remains a reserve source of water supply for Sevastopol.  

[http://www.c-inform.info/news/id/29824](http://www.c-inform.info/news/id/29824)  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/130845_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/130845/130845_800.jpg#resized)

And even a small lake in Pirogovka near Bakhchisarai 16 m deep, where I fired pikes, turned out to be a water-filled sludge pit. At the bottom there is a greasy white-gray silt. On one side, the water mirror is supported by an earthen dam. And on the horizon, there are either sawn limestone terraces, or heaps of lime chips. Virgin Crimea, the pearl of Russia :) Clickable:  
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/134835_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/134835/134835_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/134835/134835_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/134835/134835_original.jpg)

The turnover of modern metallurgy has dropped, of course. Previously, the scope was titanic. Photo - Dead Sea, Israel. A huge ancient sludge pit. Moreover, at first it was a quarry. And after the breed was chosen, they began to use it as a sludge collector. This is a logical and cost-effective practice:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/131481_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/131481/131481_800.jpg#resized)

The current water level in the Dead Sea has dropped. I believe the dam is much higher than the level. Circled in red:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/131703_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/131703/131703_800.jpg#resized)
**"metvoe-more2"**

Great Salt Lake. USA. 117 km length:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/131996_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/131996/131996_800.jpg#resized)

Great Salt Lake. The length of the propping dam is 17 km:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/132313_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/132313/132313_800.jpg#resized)

*Great Salt Lake*

Great Salt Lake. Dam:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/149142_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/149142/149142_original.jpg#resized)

Tuz Gölü. Turkey. 905 meters above sea level. 75 km in length:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/145209_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/145209/145209_original.jpg#resized)

Tuz Gölü. Turkey. 905 meters above sea level. 75 km in length 
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/145601_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/145601/145601_original.jpg#resized)

*Tuz Golu*

Nau Co Lake, Tibet. Height above sea level is 4378 meters. Large-scale colored dumps are located next to it. Clickable Natron is a salty and alkaline lake located in Tanzania. The lake is 57 km long and 22 km wide. Alkalinity can reach a pH of 9 to 10.5. The height of the lake above sea level is 800 m. Lake Baskunchak and Mount Bogdo in Russia, Astrakhan region. Dump against the background of a sludge pond: Bonneville Salt Flats, Utah (Bonneville Salt Flats, Utah) The Bonneville Desert, about 240 sq km, is famous for the extraction of table salt (90% of the total production in the USA), as well as other mineral salts - potassium, magnesium, lithium, soda On the surface of dried sludge bunkers, people set speed records:  
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/145867_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/145867/145867_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/145867/145867_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/145867/145867_original.jpg)
  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/148851_original.jpg)  #resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/148851/148851_original.jpg)  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/149813_800.jpg)  #resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/149813/149813_800.jpg)  
  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/133046_800.jpg)  #resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/133046/133046_800.jpg)  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/133266_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/133266/133266_800.jpg#resized)

In general, you understand the principle. If you are interested, go to Google maps, look for white salt spots on the continents, zoom in, look for the remains of dams... nearby there will be dumps with erosion on the slopes. 

Look at the maps of mineral resources that are being mined now in these areas, what explored minerals are there, and the picture will begin to emerge. But it should also be noted that there is a reasonable version of the overflow of salt water by tidal water from the oceans inland, so salt lakes close to the coastline may form for this reason. Therefore, to be sure, you can start analyzing salt lakes and deserts located high in the mountains. In Tibet, for example, there are 250 salt lakes.Now we turn to the extraction of limestone, without which it is impossible to remove slag when smelting metal from ore. Above, I showed that a lot of metals were mined. This means that a lot of limestone is needed. In the first article, I showed the scale of limestone mining in the Crimea. But then I thought it was mainly used for construction. It turns out not. It was and is used as a flux for the production of soda, quicklime. And as a means of neutralizing the pH of sludge ponds. This reduces the level of environmental threat. In general, limestone is very widely used in metallurgy, food, pulp and paper, coke-chemical, glass and paint and varnish industries.

Let's move on to photo materials on limestone: These are the chalk waste heaps of Slavyansk:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/133671_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/133671/133671_800.jpg#resized)

It can be seen that the local population slowly denudes them - chalk being useful both for whitewashing and for the soil as an additive.  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/134039_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/134039/134039_800.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/135112_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/135112/135112_original.jpg#resized)

At this link:
[http://tw1npeaks.blogspot.com/2014/03/blog-post\_19.html](http://tw1npeaks.blogspot.com/2014/03/blog-post_19.html)  
we discover that: 

> Not far from the confluence of the Kazenny Torets river into the Seversky Donets river, there is a large deposit of writing chalk - Raygorodskoye, which is being developed by an open pit ... In the quarries, a stratum of white writing chalk, 100 m thick, with concretions and slabs of black flint and nodules of marcasite was uncovered.

> The quarry was developed for the needs of the Slavic Soda Plant (chalk is used in the production of soda by the Solvay method). Now the quarry is used by LLC "Slavyanskiy Industrial Union Soda", they **recycle old dumps**... They sell chalk chips (natural calcium carbonate) for the needs of various industries.

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/135765_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/135765/135765_original.jpg#resized)

Now type into Google: 'Shikhans of Bashkiria' and click show pictures. You will see the following:

> Wikipedia: Shihan - a single hill (hill) that stands out well in the relief; remnant upland with regular slopes and a summit. In the Trans-Volga and Western Urals, shikhans are **remnants of reefs of ancient seas**, made of limestone. In the Urals, rocky peaks of mountains are called shikhans. Shikhans are often located in river valleys and rise 150-200 m. Scandals constantly occur around these unique “shikhans” protected by law. I give a screenshot of the article:  
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/135325_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/135325/135325_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/135325/135325_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/135325/135325_original.jpg)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/136034_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/136034/136034_original.jpg#resized)

> A natural monument or raw materials for JSC "Soda"?
> News - August 12, 2011
> The government of Bashkortostan wants to deprive the natural monument - Mount Yuraktau- of the nature protection status in order to start limestone mining.  They want to remove the status of a natural monument from one of the four hills in the Sterlitamak district of Bashkiria in order to use Mount Yuraktau as a raw material base for JSC "Soda" 67. This will lead to the actual elimination of this unique natural object.  Yuraktau The President of the Republic is against such a decision and for the preservation of a unique natural object Rustem Khamitov scientists of the Ufa Scientific Center of the Russian Academy of Sciences, the public of the republic and even the Department for Subsoil Use of Bashkortostan.  However, when it comes to the interests of the owners of the largest enterprise in Bashkortostan, this is not enough.

> "The situation with Mount Yuraktau is not the first case of the destruction of unique natural objects in Bashkortostan. Already in the early 2000s, the former authorities of the republic made an illegal decision to build the Yumaguzinsky reservoir, which flooded 2000 ha of forests in the National Park "Bashkiria", - says Mikhail Kreindlin, head of Greenpeace Russia programs on specially protected natural territories. - We turned to To the President of Bashkortostan with a request to protect the natural monument of Yuraktau. If the decision to cancel the status is still made, we will appeal to the Prosecutor General's Office to cancel this decision."

After reading the article from the screenshot, I suggest that you finally form your opinion regarding the authority of the RAS scientists. If you listen to their authority, you will never solve the quest. Now type in google - chalk mountains and click show pictures. You will see the following:  

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/136354_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/136354/136354_original.jpg#resized)

These are all ancient heaps of lime chips. Partially crumbling, with a bare compressed internal volume and in places torn apart for economic needs by the enterprising population living in the neighborhood.  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/136456_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/136456/136456_original.jpg#resized)

Have you ever seen such architectural and construction solutions on the limestone mountains? So otozh.  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/136758_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/136758/136758_original.jpg#resized)

In many places, limestone was mined right on the coastline. It is very convenient, as it allows to load raw materials directly onto a dry cargo ship with a conveyor belt. Here is a photo of the official old limestone quarry of the Hedbury Quarry in the UK:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/146941_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/146941/146941_original.jpg#resized)

But this huge ancient limestone quarry in the UK is already legendary as the chalk cliffs Beachy Head Cliffs:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/147110_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/147110/147110_original.jpg#resized)

Beachy Head Cliffs, UK:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/147204_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/147204/147204_original.jpg#resized)

Beachy Head Cliffs, UK:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/147630_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/147630/147630_original.jpg#resized)

Beachy Head Cliffs, UK:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/147933_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/147933/147933_original.jpg#resized)

Large mining corporations sometimes culturally close quarries along the following lines:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/138238_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/138238/138238_original.jpg#resized)

Then they sow such a green area with new people. Who don't remember anything. The procedure for reforestation of used quarries is called Reforestation. Trees are planted of the same species, in parallel rows, and trees in the same row are located at the same distance from each other:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/138321_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/138321/138321_original.jpg#resized)

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/138671_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/138671/138671_original.jpg#resized)

Here is the Mark Creek mine before landscaping - refrestization:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/144173_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/144173/144173_original.jpg#resized)

Here is the Mark Creek mine before after landscaping - refrestization:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/144481_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/144481/144481_original.jpg#resized)

A satellite screenshot of the refrestization of ancient dumps near Simferopol. Clickable: Satellite screenshot of ancient waste dumps refrestization near Sevastopol. Clickable: Satellite screenshot of ancient dumps underneath Sudak. The forest did not take root. Clickable:  
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/138883_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/138883/138883_original.jpg)  )
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/138883/138883_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/138883/138883_original.jpg)  )
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/139189_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139189/139189_original.jpg)  )
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/139189/139189_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139189/139189_original.jpg)  )
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/139332_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139332/139332_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/139332/139332_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139332/139332_original.jpg)

Satellite screenshot of reforestisation of what appears to be an old sandy tailing dump near Kherson. Clickable: There are also sandy tailings. E.g. tar sands tailing dump in Alberta, Canada  
<!-- Local
[![](The Grand Canyon in the USA - an ancient uranium quarry_files/139624_original.jpg#resized)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139624/139624_original.jpg)  )
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/139624/139624_original.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/139624/139624_original.jpg)  )
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/139973_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/139973/139973_original.jpg#resized)

The reforestation of the Crimea, as well as the entire planet, apparently began with the founding of the Botanical Gardens. When reading about Botanical Gardens, pay attention to the date of foundation, last name and country of the founder. For example, the date of foundation of the Nikitsky Botanical Garden in Yalta is 1811. Founder - Armand Emmanuel Sophie Septemanie de Vignerot du Plessis, 5eme duc de Richelieu. For us, who do not remember our roots further than 4 generations ago - Emmanuel Osipovich de Richelieu. French aristocrat. The first director is Christian von Steven. Russian botanist of Swedish origin, doctor of medicine, gardener and entomologist, founder and first director of the Nikitsky garden in Crimea, actual state councilor.

[An article about the Nikitsky Botanical Garden.](https://ru.wikipedia.org/wiki/%D0%9D%D0%B8%D0%BA%D0%B8%D1%82%D1%81%D0%BA%D0%B8%D0%B9_%D0%B1%D0%BE%D1%82%D0%B0%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D1%81%D0%B0%D0%B4)Often in cities, the Kremlin stands on flat-topped hills. According to the official history, such a hill was poured by men with wooden shovels in their free time. But this is bad work. If you brainwash and compare by analogy looking at modern reclaimed dumps, then everything falls into place. In the photo, the Kremlin of Nizhny Novgorod, built on an ancient reclaimed dump and below - a modern reclaimed dump of a uranium mine:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/151949_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/151949/151949_800.jpg#resized)

Recultivated dump of a uranium mine:  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/152076_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/152076/152076_800.jpg#resized)

The purpose of reclamation:

1. leveling the top of a waste heap or dump and setting up a flat terrace;  
2. Application of loam and fertile soil on the upper terrace of the waste heap;  
3. "cutting" of horizontal terraces along the slopes of the waste dump.  

These works will prepare a waste heap for planting plants that will prevent its destruction and have a beneficial effect on the atmosphere. Let me write a hypothesis about such an important aerospace metal as aluminum. It is extracted from bauxite - the main mineral raw material for the aluminum industry. It looks like clay. Search **keyword** - **Bauxite mining** . Production scheme:

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/140109_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/140109/140109_original.jpg#resized)

As can be seen from the scheme, fertile soil goes to the dump. Then a layer of bauxite is removed from a large area. Photo of current mining: Bauxita Paragominas, Brazil:
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/140307_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/140307/140307_original.jpg#resized)

*Bauxite mining*

<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/140778_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/140778/140778_original.jpg#resized)

Brazil, bauxite mining  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/148384_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/148384/148384_original.jpg#resized)
Bauxite mines Alcoa  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/142007_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/142007/142007_original.jpg#resized)
Kuantan bauxite road red. Red Bauxite Road to Kuantan, Malaysia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/141357_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/141357/141357_original.jpg#resized)
Rio Tinto's Bauxite Mine at Andoom, Australia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/146582_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/146582/146582_original.jpg#resized)
Considering the scale of metallurgy of the past, which I showed above, the question arises - where did the soil go in a number of countries in Latin America, Africa, Australia and other countries? If vegetation is not disturbed for thousands of years, not even forests, but meadow and savanna, then a layer of humus will form. But we see such landscapes in these countries: Africa  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/142168_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/142168/142168_original.jpg#resized)
Africa  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/142543_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/142543/142543_original.jpg#resized)
Australia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/142802_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/142802/142802_original.jpg#resized)
Brazil  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/142889_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/142889/142889_original.jpg#resized)
Australia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/143311_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/143311/143311_original.jpg#resized)
Australia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/143401_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/143401/143401_original.jpg#resized)
Namibia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/143744_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/143744/143744_original.jpg#resized)
Namibia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/144118_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/144118/144118_original.jpg#resized)
Namibia  
<!-- Local
![](The Grand Canyon in the USA - an ancient uranium quarry_files/144838_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/144838/144838_original.jpg#resized)

There is something to think about. In what year, according to legend, the king was presented with aluminum spoons, which were valued more than gold? :)

This rounds off. I hope you were interested and in your free time as a crossword puzzle using google maps you will solve many more interesting riddles.

PS: People often ask, where did the mining equipment go, despite the past hundreds of years? Should at least something stay? One of the recycling methods is in the video below :)

[![Video shows one recycling method for old mining equipment](https://img.youtube.com/vi/BVbzgVSMMKQ/0.jpg)](https://www.youtube.com/watch?v=BVbzgVSMMKQ)

*Video shows one recycling method for old mining equipment*

© All rights reserved. The original author retains ownership and rights.

