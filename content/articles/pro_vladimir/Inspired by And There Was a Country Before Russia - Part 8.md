title: Inspired by "And there was a Country before Russia". Part 8
date: 2013-11-10
modified: Fri 02 Apr 2021 20:07:30 BST
category:
tags: cannons, technology
slug:
authors: Vladimir Mamzerev
summary: After all, no matter how you twist it, the leather shield is as functional as a shield and    can be made simpler. But here are the bronze pancakes. Expensive, difficult and not practical. Where is the logic?
status: published
local: Inspired by And There Was a Country Before Russia - Part 8_files
originally: По мотивам «И была Страна до России». Часть 8.html

### Translated from:
[https://pro-vladimir.livejournal.com/2330.html](https://pro-vladimir.livejournal.com/2330.html)

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Inspired by ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html) and [Ivan and Mary](http://dmitrijan.livejournal.com/91508.html)

**Russian:**

В поисках артефактов к выше изложенным теориям в сети нарвался на один показательный пример.

отсюда: [http://liberis.livejournal.com/178950.html](http://liberis.livejournal.com/178950.html)

**English:**

Searching the Internet for artifacts for the above theories, I ran into one illustrative example.

From here: [http://liberis.livejournal.com/178950.html](http://liberis.livejournal.com/178950.html)

**Russian:**

Правда, он такой не один на всю ирландию. Если верить Средневековым текстам, настоящий центр поклонения этому богу был в графстве Каван в долине Маг Слехт, которая так и переводится - долина поклонения. Там стоял Идол Крома Дува, покрытый золотом, в окружении двенадцати других идолов, покрытых серебром, и Крому Дуву приносили человеческие жертвоприношения. В каком размере и каким образом остается спорным. Одни источники говорят - первенцев приносили. Другие - треть всего "приплода" - детей, зверей, плодов. Наиболее распространенная история, связанная с жертвоприношениями в долине Маг Слехт - это история о том, как король всея ирландии Тигернмас пришел поклониться Крому Круаху в Самайн. Сам Тигернмас очень интересная личность. По преданию он правил в период с 1621 по 1544 д н.э. (по другим версиям с 1209 по1159 д н э), в его правление в ирландии впервые научились плавить золото, он был первым из королей, кто додумался одаривать своих приверженцев рогами для питья, а также красить одеяния в пурпур, зелень и синь. И вот, после того как этот Тигернмас правил 77 (или 50) лет вздумалось ему поклониться идолу Крома Круаха в Маг Слэтх. Крома называют верховным язычеким идолом древних Ирландцев, его просили о прибавлении стад, молока и зерна. Так вот, пришел Тигернмас в Маг Слехт и давай поклоняться Крому. Но видимо в процессе и сам король и его сторонники впали в совершенно безудержный транс. Они бились о землю, простирались перед идолом, их лбы ударялись и дробились о камни, они стенали, рыдали и раздирали на себе одежды, их тела были изранены от ударов, и все это коллективное помешательство продолжалось до тех пор, пока три четверти ирландцев во главе с королем не умерло и не полегло прямо на месте, таким образом принеся себя в жертву Жестокому Крому. И правил в этой долине и по всей ирландии этот жестокий демон, пока не пришел святой патрик, не поверг его, и не основал на месте его капища монастырь.

**English:**

It is true that he is not alone in all of Ireland. According to medieval texts, the real centre of worship for this god was in Kavan County, in the Mag Slecht Valley, which translates as the Valley of Worship. There stood the Idol of Thunderbird, covered in gold, surrounded by twelve other idols covered in silver, and Thunderbird was offered human sacrifices. The size and manner in which this remains controversial. Some sources say that the firstborn was brought. Others are a third of all "offspring" - children, animals and fruits. The most common story about the sacrifices in the Mag Slecht Valley is that of how King Tigernmas of All Ireland came to worship Croma Cruach in Samhain. Tigernmas himself is a very interesting person. According to legend, he ruled between 1621 and 1544 BC. (other versions from 1209 to 1159 D.C.), he was the first king in Ireland to learn how to melt gold and was the first to give his followers horns for drinking and also to paint purple, green and blue robes. And so, after this Tigernmez ruled 77 (or 50) years, he decided to worship the Croix idol at Mag Slath. Cromus is called the supreme pagan idol of the ancient Irish, and he was asked to add herds, milk and grain. So Tigernmez came to the Mage Slecht and let us worship Crom. But apparently in the process, both the King and his supporters fell into a completely unbridled trance. They fought against the ground, stretched out in front of the idol, their foreheads hit and crushed against rocks, they wailed, sobbed and torn their clothes, their bodies were torn from blows and all this collective insanity continued until three quarters of the Irish led by the king died and lay down right there, thus sacrificing themselves to Cruel Crom. And ruled in this valley and all over Ireland this cruel demon until the Holy Patriarch came, defeated him and founded a monastery on the site of his temple.

**Russian:**

Вот такая прелестная средневековая антиязыческая монастырская агитка.

**English:**

This is a lovely medieval antique monastery agita.

**Russian:**

про безудержный транс понравилось. так что мы видим по сути? что на территории Ирландии центров поклонения, то есть систем типа "Идол" было. Что прожив и поправив достаточно долго, правитель Тигернмас едет до покрытым золотом идола, что стоит в окружении 12-ти покрытых серебром и....приводит в действие систему. специально или по недоразумению какому, мы теперь и не узнаем.

**English:**

We liked the unrestrained trance. So what do we see as a matter of fact? That there were worship centres in Ireland, i.e. Idol-type systems. That after living and correcting long enough, Ruler Tigernmas drives to a gold-coated idol, which is surrounded by 12 silver-coated idols and activates the system. specifically or by misunderstanding which, we will not know now.

**Russian:**

Так вот, пришел Тигернмас в Маг Слехт и давай поклоняться Крому. Но видимо в процессе и сам король и его сторонники впали в совершенно безудержный транс. Они бились о землю, простирались перед идолом, их лбы ударялись и дробились о камни, они стенали, рыдали и раздирали на себе одежды, их тела были изранены от ударов, и все это коллективное помешательство продолжалось до тех пор, пока три четверти ирландцев во главе с королем не умерло и не полегло прямо на месте, таким образом принеся себя в жертву Жестокому Крому.

**English:**

So, Tigernmas came to Mag Slecht and let's worship Kroma. But apparently in the process, both the King and his supporters fell into a completely unbridled trance. They fought against the ground, stretched out in front of the idol, their foreheads hit and crushed against rocks, they wailed, sobbed and torn their clothes, their bodies were torn from blows and all this collective insanity continued until three quarters of the Irish led by the king died and lay down right there, thus sacrificing themselves to Cruel Crom.


**Russian:**

и чего это они? не уж то накрыло полем МиРа? ну тем самым, что на комиксе колокольни присутствует.

**English:**

And what are they doing? Didn't they cover the MiRa field? Well, the fact that there is a bell tower on the comic book.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/23853_orig.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/23853_orig.jpg)
-->

[![](http://ic.pics.livejournal.com/dmitrijan/42576892/23853/23853_orig.jpg#clickable)](http://ic.pics.livejournal.com/dmitrijan/42576892/23853/23853_orig.jpg)

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmID3VjkqKhrbrqvi2CrmCh1OoGN5qCfWpM18ONX.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmID3VjkqKhrbrqvi2CrmCh1OoGN5qCfWpM18ONX.jpeg)
-->

[![](http://content-28.foto.mail.ru/mail/mamzerev/2/s-7.jpg#clickable)](http://content-28.foto.mail.ru/mail/mamzerev/2/s-7.jpg)

**Russian:**

Впрочем, мы так или иначе достаточно часто встречаем упоминания поля МаРа. тоже самое МаРево, что стоит над городами прошлого. Да и выходит, что "Слобода", это именно поселение для пришлых. Там где поле МаРа слабое. от того и "Слабода". Чтоб могли попривыкнуть. Вон Ирландцев скосило как с непривычки.

**English:**

One way or another, however, we find references to the MaRa field quite often. The same 'MaRevo' (haze) that stands above the cities of the past. And it turns out that 'Sloboda' is just a settlement for outsiders. Where the MaRa field is weak, so is Sloboda. So they can get used to it. The Irish have been cut off as if they were not used to it.

**Russian:**

Собствено, на ту страничку меня занесло в поисках информации про бронзовые щиты. Вообще вся атрибутика тех систем в изобилии строилась на бронзе. Потому как относительно доступный токопроводящий материал, не магнитный. Можно использовать и так и эдак.

**English:**

In fact, I was on that page looking for information about bronze shields. In general, all the attributes of those systems were abundantly built on bronze. Because the relatively accessible conductive material is not magnetic. You can use both of these materials.

**Russian:**

Вот он собственно щит, что был там в Ирландии при раскопках найден.

**English:**

This is the actual shield that was found there in Ireland during excavations.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmMnea51OsNExYIm36Jf7y6wai7i0WKBXWWJ1d6x.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmMnea51OsNExYIm36Jf7y6wai7i0WKBXWWJ1d6x.jpeg)
-->

[![](http://irelandwithkids.com/wp-content/uploads/2012/08/Phot-of-Day-3-Page-017-767x1024.jpg#clickable)](http://irelandwithkids.com/wp-content/uploads/2012/08/Phot-of-Day-3-Page-017-767x1024.jpg)


*Занятный щит. Что-то там в нем в геометрии его заложено.*

*Interesting shield. Something in it is embedded in its geometry.*

**Russian:**

Поиски информации про щиты дали интересные результаты. В том плане, что, попадались результаты современных испытаний, из которых следовало, что бронзовые щиты при ударах мечами и прочим холодным оружием достаточно быстро раскалывались. А первинство по прочности досталось кожаным щитам. То есть, кожа, достаточно долго выдерживала натиск рубящих и колящих ударов. На лицо выползает, что что-то с бронзовыми щитами не так. ведь как ни крути, а кожаный щит и функциональней как щит, да и сделать его проще. а тут бронзовые блины. дорого, тяжело и не практично. где логика?

**English:**

Searching for information about shields has yielded interesting results. In the sense that there were results of modern tests, which showed that bronze shields split quite quickly when struck by swords and other cold weapons. And leather shields took precedence in terms of strength. That is, the leather withstood the onslaught of chopping and stabbing blows for quite a long time. Something crawls out on your face that something is wrong with the bronze shields. After all, no matter how you twist it, the leather shield is as functional as a shield and can be made simpler. But here are the bronze pancakes. Expensive, difficult and not practical. Where is the logic?

**Russian:**

Геометрический стиль. Щит из Черветери (Италия). Бронза. 7 в. до н. э. Ватиканские музеи.

**English:**

The geometric style. Shield from Cerveteri (Italy). Bronze. 7th century BC. Vatican museums.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmCBSSOvy9VYriAer90z5t41c5-HV2o--y8jw1Ta.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmCBSSOvy9VYriAer90z5t41c5-HV2o--y8jw1Ta.jpeg)
-->

[![](http://www.help-rus-student.ru/pictures/17/858_3.jpg#clickable)](http://www.help-rus-student.ru/pictures/17/858_3.jpg)

о как. аж седьмой век до нашей эры.

**English:**
Possibly as old as the seventh century BC

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmKjt7NDKxxmLrDAyvV-FI7rP7pk2CePcFZHURn3.html)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmKjt7NDKxxmLrDAyvV-FI7rP7pk2CePcFZHURn3.html)
-->

[![](http://www.predistoria.org/img/img2006/articles_2006/9_sept/oruzh_files/image009.jpg#clickable)](http://www.predistoria.org/img/img2006/articles_2006/9_sept/oruzh_files/image009.jpg)

![image unavailable]()

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/0_b74ab_b2534d62_orig.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/0_b74ab_b2534d62_orig.jpeg)
-->

[![](http://img-fotki.yandex.ru/get/9265/32225563.b8/0_b74ab_b2534d62_orig)](http://img-fotki.yandex.ru/get/9265/32225563.b8/0_b74ab_b2534d62_orig)

**Russian:**

ох уж эти тазики. Вот спрашивается, зачем так изголятся с формой? мало того, что оно не выдерживает проверку на прочность, так еще и вензеля на этих щитах такие разные затейливые.

есть еще и вот такие варианты.

**English:**

Oh, those basins. That's why they ask, why do they have to be so out of shape? Not only does it not pass the strength test, but the monograms on these shields are so different and intricate.

There are also these options:

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM5T2QfoXNhSOZuqfx37VC_k0sMJFzrSHBModmk.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM5T2QfoXNhSOZuqfx37VC_k0sMJFzrSHBModmk.jpeg)
-->

[![](http://historylib.org/historybooks/pod-red--Andzhely-CHerinotti_Kelty-pervye-evropeytsy/1352704741_2218.jpg#clickable)](http://historylib.org/historybooks/pod-red--Andzhely-CHerinotti_Kelty-pervye-evropeytsy/1352704741_2218.jpg)

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM2ejk1iTGVGDANuOPuH7b8F9aOsjvHq13v9Pry.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM2ejk1iTGVGDANuOPuH7b8F9aOsjvHq13v9Pry.jpeg)
-->

[![](http://club-kaup.narod.ru/rec/kulakov010/kulakov010_img6.jpg#clickable)](http://club-kaup.narod.ru/rec/kulakov010/kulakov010_img6.jpg)

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmFMZcEUCumQiMo2CPdnWeWjtixFKk8wYcE6ORFL.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmFMZcEUCumQiMo2CPdnWeWjtixFKk8wYcE6ORFL.jpeg)
-->

[![](http://res-publica.ucoz.org/_pu/0/26430.jpg#clickable)](http://res-publica.ucoz.org/_pu/0/26430.jpg)

**Russian:**

остались и от великанов некоторые, что вмазаны в стену ибо больше не придумали куда пристроить

**English:**

There were also some giant versions that were smeared into the wall because they no longer thought about where to attach them:

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/0_76324_c71d306e_XL.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/0_76324_c71d306e_XL.jpg)
-->

[![](http://img-fotki.yandex.ru/get/58191/140132613.20/0_76324_c71d306e_XL.jpg#clickable)](http://img-fotki.yandex.ru/get/58191/140132613.20/0_76324_c71d306e_XL.jpg)

**Russian:**

впрочем, единство в материале не означает едиства в функциональности.

и ряд бронзовых щитов возможно и использовалось по прямому назначению как нами принято считать по классическим возрениям историков. А что если, они защищали от чего-то чего мы не знаем? ведь извечное противостояние брони и средств поражений имело место быть и тогда.

**English:**

However, similiarity of material does not mean similarity of functionality.

And a number of bronze shields have possibly, and have actually been, used for their intended purpose as we generally consider it to be in the beliefs of classical historians. What if they were protecting against something we don't know? Because the eternal confrontation between means of destruction and armour took place even then.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmPuIRaEVCtdrRFd6h_2qqgD9jjtdl9GkHgI6CHh.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmPuIRaEVCtdrRFd6h_2qqgD9jjtdl9GkHgI6CHh.jpeg)
-->

[![](http://www.ksv.ru/%D0%98%D1%81%D1%81%D0%BB%D0%B5%D0%B4%D1%83%D0%B5%D0%BC/%D0%A2%D0%B5%D1%81%D1%82%D1%8B/%D0%9E%D1%80%D1%83%D0%B6%D0%B8%D0%B5/%D0%92%D0%B0%D0%B4%D0%B6%D1%80%D1%8B/PIC/Z050.jpg#clickable)](http://www.ksv.ru/%D0%98%D1%81%D1%81%D0%BB%D0%B5%D0%B4%D1%83%D0%B5%D0%BC/%D0%A2%D0%B5%D1%81%D1%82%D1%8B/%D0%9E%D1%80%D1%83%D0%B6%D0%B8%D0%B5/%D0%92%D0%B0%D0%B4%D0%B6%D1%80%D1%8B/PIC/Z050.jpg)

*Варджа. Оружие богов.*

*Varja. Weapons of the gods.*

![image no longer available]()

**Russian:**

Среди обилия более поздних подражающих изделий нет такого изобилия деталей. Ну эстеты были мастера прошлого. Колокольчик вот однако тоже в комплекте. И ножка, то есть, ручка колокольчика имеет какие-то витиеватые элементы, ну прям как для настройки. ну почти что те разборные стволы, что засвечены в Константинополе.

**English:**

There is no such abundance of detail among the many later, imitation products. Well, aesthetes were masters in the past. The bell, however, is also included in this. The leg that forms the handle of the bell has some ornate elements. As if for tuning. Almost like those dismantlable barrels that are illuminated in Constantinople.

![image no longer available]()

**Russian:**

Влажность всё испортила. Не стало возможным накапливать большие статические заряды. Сила, что могла спалить, растаяла.

**English:**

The humidity ruined everything. It was not possible to accumulate large static charges. The power that could have ignited melted away.

![image no longer available]()

**Russian:**

упоминание охранных систем МаРа или МиРа мы встречаем в сказках. как некто Алладин ходил в некое подземное царство за лампой с джином. которую нужно было потереть, чтоб устройство заряд набрало для запуска. а система входного контроля была настроена на исполнение заповедей. впрочем, этот вопрос еще ждет своей очереди. на том же принципе видать и закрыто хранилище под сфинксом. не могут нынешние люди через то поле проходить. порвет вон как тех в Ирландии.

**English:**

We find mention of the security systems of Mara or Mira in fairy tales. Aladdin went to a certain underground kingdom for a lamp that contained a djinn. It had to be rubbed so that the device gained a charge to start. And the entrance control system was set up to fulfill the commandments spoken to it - "Open Sesame". However, this question is still waiting for its time to come. On the same principle, you can see that the vault under the Sphinx is closed. Today's people can't pass through that field. He'll tear them up like he tore up the ones in Ireland.

![image no longer available]()

**Russian:**

"Ва-рд-жа" и "Дер-жа-ва" как-то имеют одинаковые слоги не находите? случайно так совпало?

**English:**

"Wa-rd-Zha" and "Der-Zha-Va" somehow have the same syllables, don't you think? Is it a coincidence?

**Russian:**

Ваджра - оружие древних богов

**English:**

Vajra - weapon of the ancient gods

![image no longer available]()

**Russian:**

"ВАДЖРА" (санскр.; тиб. дордже, яп. конгосё, кит. дзиньганси, монг. очир) --- символ буддизма, подобно тому, как крест --- символ христ-ва, полумесяц --- ислама, ворота "тории" --- синтоизма. Тексты определяют его сл. образом: алмаз, громовой топор, пучок скрещенных молний, выступающие как символ божеств, истины учения Будды. В инд. мифологии Ваджра была известна задолго до буддизма как громовой топор бога Индры и ряда других богов. Ее эпитеты: медная, золотая, железная, каменная, с 4 или 100 углами, 1000 зубцами. Ваджра может иметь вид диска, а может быть крестообразной. Ей приписывали способность вызывать дождь и быть символом плодородия. В буддизме облик Ваджра таков: перехваченный посередине пучок молний с загнутыми концами (известны одинарный, двойной, тройной, крестообр. варианты).

**English:**

"VAJRA" (Sanskrit: TIB. Dorje, YAP. kongose, kit. jingansi, Mong. Ochir) Buddhist symbol, similar to the cross, symbol of Christ; the Crescent, symbol of Islam; and the gate of "Torii", symbol of Shinto. The texts define it as follows:

> A diamond, a thunder axe, a bundle of crossed lightning bolts, which act as a symbol of deities, the truth of the Buddha's teaching. In India, vajra mythology was known long before Buddhism as the thunder axe of the God Indra and a number of other gods. Its epithets: copper, gold, iron, stone, with 4 or 100 corners, 1000 teeth. The vajra may look like a disk, or it may be cross-shaped. It was credited with the ability to cause rain and be a symbol of fertility. In Buddhism, the shape of the vajra is as follows: intercepted in the middle of a beam of lightning with curved ends (known single, double, triple, cruciform. options).

**Russian:**

В переводе с санскрита это слово означает одновременно «молния» и «алмаз». Ваджра представляет собой палицу либо скипетр и символизирует прочность и неуничтожимость. Она может интерпретироваться в качестве одного из образов мировой оси, фиксируя воздействие верхнего мира на нижний.

Первоначально ваджра являлась атрибутом Индры, бога грозы. Он известен как губитель демонов, вел богов против асуров. Убийца змея Вритры, Индра сокрушил его огненной палицей, ваджрой, победив, таким образом, хаос, освободив воды, сотворив жизнь и солнце.

**English:**
In Sanskrit, this word means both "lightning" and "diamond". Vajra is a club or sceptre and symbolizes strength and indestructibility. It can be interpreted as one of the images of the world axis, fixing the effect of the upper world on the lower one.

Originally, the vajra was an attribute of Indra, the God of thunder. He is known as the destroyer of demons, led the gods against the Asuras. The Slayer of the serpent Vritra, Indra crushed the serpent with a fiery club -  Vajra - thus defeating chaos, freeing the waters, creating life and the sun.


остальное тут: [http://www.liveinternet.ru/users/samanta_black/post162449872](http://www.liveinternet.ru/users/samanta_black/post162449872)

The rest is here: [http://www.liveinternet.ru/users/samanta_black/post162449872](http://www.liveinternet.ru/users/samanta_black/post162449872)

**Russian:**

таки Варджа, это устройство, что позволяло портативно использовать статическое электричество? этакий переносной плазмомет с переносным же колокольчиком постановки заслона поля МиРа или МаРа.

**English:**

Taki Varja... is this a device that allowed portable use of static electricity? A sort of portable plasma meter with a portable bell setting the barrier of the World or 'Mar' field.

**Russian:**

Были и большие стационарные системы. Не всё смогли стащить в Ватикан. Для примера.

**English:**
There were also large stationary systems. Not everyone was able to steal to the Vatican. For an example.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmGp3k4tmaAwhrXUzFU7G8J__gEcemmkuxYh8Bih.jpeg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmGp3k4tmaAwhrXUzFU7G8J__gEcemmkuxYh8Bih.jpeg)
-->

[![](http://s002.radikal.ru/i197/1104/9a/80e3d87f53bf.jpg#clickable)](http://s002.radikal.ru/i197/1104/9a/80e3d87f53bf.jpg)

**Russian:**

Несомненно эта конструкция ну чисто для красоты. или вот.

**English:**

Undoubtedly, this design is purely for beauty. or here.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/0_e5d57_c2fc9ba7_L.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/0_e5d57_c2fc9ba7_L.jpg)
-->

[![](http://img-fotki.yandex.ru/get/5014/51667439.41b/0_e5d57_c2fc9ba7_L.jpg#clickable)](http://img-fotki.yandex.ru/get/5014/51667439.41b/0_e5d57_c2fc9ba7_L.jpg)

**Russian:**

вполне себе такой искровой фонарик.

**English:**

Quite a spark like this flashlight:

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/0_e5d7d_524a6d27_L.jpg#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/0_e5d7d_524a6d27_L.jpg)
-->

[![](http://img-fotki.yandex.ru/get/5009/51667439.41b/0_e5d7d_524a6d27_L.jpg#clickable)](http://img-fotki.yandex.ru/get/5009/51667439.41b/0_e5d7d_524a6d27_L.jpg)

световые-слуховые окна.

light-Dormer Windows.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/480px-Templeofrosycross.png#clickable)](Inspired by And There Was a Country Before Russia - Part 8_files/480px-Templeofrosycross.png)
-->

[![](http://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Templeofrosycross.png/480px-Templeofrosycross.png#clickable)](http://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Templeofrosycross.png/480px-Templeofrosycross.png)

*еще одна занятная картинка.*

*Another interesting picture.*

**Russian:**

Сотоварищи со стороны условно её назвали как "передвижной сканер территории". Вон и Ковчег на горе стоит. наверняка допотопная система прощупывает, передвижной колокольней, чего можно "откопать" и где лучше застолбится.

На картинке и излучение прорисовано в виде крылошек кадуцея. И SMS слуховых окон. И достаточно еще много чего, что тоже думаю еще дождется своей очереди для разбора. Особливо занимает кусочек картинки импульса, прям как осциллографа, оно в аккурат на "антенне" прорисовано. Многофункциональный комплекс, однако.

**English:**

Colleagues from outside conditionally called it as "mobile scanner of the territory". There's the Ark on the mountain. Surely the antediluvian system, a mobile bell tower, is probing what can be "dug up" and where it is better to stake out.

In the picture, the radiation is drawn in the form of caduceus wings. And SMS dormer windows. And quite a lot of other things that I think also still wait for their turn for analysis. Especially we note a pulse occupies a piece of the image, looking just like an oscilloscope. It is drawn exactly on the "antenna". A multifunctional complex, however.

<!-- Local
[![](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM5JhGqeBXHp5ppSQ7GnoGyjW6a0wVEc-ZU6NPD.html)](Inspired by And There Was a Country Before Russia - Part 8_files/HoY2vbwxKON58bZbDUePmM5JhGqeBXHp5ppSQ7GnoGyjW6a0wVEc-ZU6NPD.html)
-->

[![](http://astrovic.ru/lib/bagdasarov/swastika_05_image021.jpg "Image not found")](http://astrovic.ru/lib/bagdasarov/swastika_05_image021.jpg "Image not found")

**Russian:**

есть и вот такое импровизированное изображение системы Грааль и совместно со щитом.

как известно, до Александрийского маяка, историки утверждают, что названия у маяков не было. и именно с тех времен повелось все маяки называть ФаРос.

**English:**

There is also such an improvised image of the Grail system and together with the shield.

As you know, before the lighthouse of Alexandria, historians say that lighthouses were not named. In those times all lighthouses were called "Pharos".

**Russian:**

Фа-Ро-С. Это что? Фа-Родового-Столба?

в языке достаточно много что дошло до нас. Хоть вон туже Фа взять. а что такое Фа? нота? обозначение рабочий частоты системы? вон и на картинке послепотопного сканирования на маковке Fa прорисована.

а есть еще:

Фа-Ра

Фа-Ра-Он самый главный прожектор древности, что больше остальных мог статику накапливать? главная батарейка?

**English:**

Fa-Ro-S. What is it? Fa-Generic-Post?

The language has quite a lot that has come down to us. At least take the same 'FA'. What is the 'FA'? A note? A designation of the operating frequency of the system? On the top of the FA in the image of the post-flood scan is drawn:

![image not available]()

And there is also:

Fa-Ra

Fa-Ra-He is the most important searchlight of antiquity. What could accumulate static more than others? The main battery?

**Russian:**

это всё из той же оперы про средневековое бросание перчаток, что высвобождало накопленное? ибо статика, что монахи своим облачением копили имела определенную цену, потому как могла питать системы предтеч. та самая Мана, что Мана-хи копили в Мана-стырях. жаль не работает нынче. а они и вериги носили, храмовые слепые посетители думают, что это из-за высокой любьви к богу. ага, да тупо набор контаков для тела, что позволяло пользовать еще оставшиеся рабочие системы.

**English:**

This is all from the same opera about the medieval throwing of gloves, which released the accumulated? for the static that the monks saved up with their vestments had a certain price, because it could feed the forerunner systems. The same Mana that the Mana-Chi accumulated in the Mana-Styr.

Sorry it doesn't work now. 

And they also wore chains. Temple-blind visitors think that this is because of their high love of God. Yes. Yeah, a simple set of contacts for the body, which allowed you to use the remaining working systems.

**Russian:**

а Ми-Ра, не тоже нота?

Фа-ланга. ой, таки чего выходит, это строй пеший электрического шокера? от того и щиты у них бронзовые, что обкладки конденсатора? накапливали заряд и одновременно защищали от плазмы? ды вы сами подумайте, каким образом встав парадным строем с копьями длиной превышающими возможность человека, что либо ими сделать, да тупо длинные и тяжелые копья, кого-то этим передвижным забором напугать. а вот если забор бьётся током?

**English:**

And Mi-Ra, is it not also a note?

Fa-lang. Oh, what does it mean? Is this a mobile electricuntion formation? Why are their shields made of bronze, what are the capacitor plates? Did they accumulate a charge and simultaneously protect it from plasma? Do you think for yourself, how to stand in a parade formation with spears longer than a person can do anything with them. And with these stupidly long and heavy spears, scare someone with this mobile fence? But what if, instead, the fence was electrified?

**Russian:**

Что нам еще известно с тех стародавних времен. Что некогда существовала Могучая Римская империя, которой приписывают много чего построенного. Что они и не строили, а тупо откопали. Что римская империя раскололась надвое. На Византийскую со столицей в Константинополе и опять таки Римскую со столицей в Риме.
Игра названий мест и территорий нам свидетельствует, что как-то очень плотно Русь общалась с Константинополем и звался он Царь-Град. С чего такие почисти? Там некогда было также территориальное образование, которое звалось Османская империя. Османская, никому ось-маны не напоминает?

**English:**

What else do we know from those ancient times? That once there was a mighty Roman Empire, which is credited with building many things. What they didn't build, they simply dug up. That the Roman Empire was split in two. One the Byzantine empire with its capital in Constantinople, and a second with its capital in Rome.

The play of names of places and territories shows us that at one time Russia communicated very closely with Constantinople and called it Tsar-Grad. Why is this now cleansed? There was once also a territorial entity called the Ottoman Empire. Ottoman, does the axis remind anyone of mana?

**Russian:**

Римская империя благополучно свернулась до размеров Ватикана, после окончания своей раскопочной деятельности и заполонила на сколько могла все Католиками. Ка-то-лик.

А что это за «Ка» и «Лик»?

**English:**

The Roman Empire safely collapsed to the size of the Vatican, following the end of its excavation activities, and filled as much as it could with Catholics. "Ka-to-lik".

And what are these "Ka" and "Lik"?

**Russian:**

Туго было у предков с материалами. Провода по большей части из золота, растащили на кольца и серьги. Повставляли в носы. Изоляция была никакая, точнее разве что только шелк, что использовался вместо плат нынешнего текстолита. Вот находит наш недалекий предок шкатулку, а там она полна сокровищ! Да фига, камушки полупроводников, что сплетены золотой оправой схемы. Всё срочно и стремительно распихать по носам и ушам. А мы то думали, это мы придумали навесной монтаж? Ни, это давно «украдено» до нас.

**English:**

Our ancestors had a hard time with materials. The wires are mostly gold, and they've been stolen for rings and earrings. They put them in their noses. There was no insulation, or rather only silk, which was used instead of the current textolite boards. Here and there our near ancestor finds a box and it is full of treasures! Yes semi-conductor gem stones that are woven with the gold frame of the circuit. Everything is urgent and they quickly shove it in their noses and ears. And we thought that we came up with mounted electronics installation? No, it was "stolen" from long before our time.

**Russian:**

А еще в замках часто нам историки показывают исключительно пыточные подвалы. Непременным атрибутом которого является этакий саркофаг со штырями внутри. Да пытуемый даже испугаться не успеет пристрой его во внутрь. А может всё несколько иначе? Может то банальная этакая рабочая колодка для сборки и монтажа систем? Навесных систем. Железного дровосека из сказки помним? А если совместить доспехи рыцарей и эту «пытательную» форму? Мастерская по сборке роботов?

**English:**

And in castles, historians often show us rooms designed solely as torture cellars. An indispensable attribute of which is sometimes a kind of sarcophagus with pins inside. Yes, the subject will not even have time to be afraid. Or maybe it's a little different? Maybe it's a simple sort of working pad for the assembly and installation of systems? Mounted systems. Remember the tin woodman from the fairy tale? And if you combine the armor of knights and this "torturous" form? A robot assembly workshop?

**Russian:**

Мы конечно бредим. Мы конечно первая и самая умная цивилизация, а до нас ну ничегошеньки тут не было. это же все знают?

**English:**

Of course, thinking like this, we are delusional. Ours is, of course, the first and most intelligent civilization, and before us, well, there was nothing here. Everyone knows that, right?

**Russian:**

Вопросы, вопросы и снова вопросы. Собственно ответы они как водится приходят, если правильно ставить вопросы.

**English:**

Questions, questions, and more questions. Actually, the answers usually come, if you ask the correct questions.

Vladimir Mamzerev. 10.11.2013 G.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-9.html)

© All rights reserved. The original author has asserted their ownership and rights.

