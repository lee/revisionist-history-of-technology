title: Mylnikovdm's 'Another History of the Earth' articles - Google-translated
date: 2017-11-04
modified: 2022-12-14 16:03:07
category: 
tags: catastrophe; links
slug: 
authors: Dmitry Mylnikov
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: 


[Another History of the Earth Part 1a](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/219459.html)

[Another History of the Earth Part 1b](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/219687.html)

[Another History of the Earth Part 1c](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/220008.html)

[Another History of the Earth Part 1d](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/220823.html)

[Another History of the Earth Part 1e](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/222351.html)

[Another History of the Earth Part 1e2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/224787.html)

[Another History of the Earth Part 1g](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/225365.html)

[Another History of the Earth Part 1h](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/225623.html)

[Another History of the Earth Part 1i](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/227214.html)

[Another History of the Earth Part 2a](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/228615.html)

[Another History of the Earth Part 2b](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/231374.html)

[Another History of the Earth Part 2B](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=http://mylnikovdm.livejournal.com/239848.html)

[Another History of the Earth Part 2c](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/239978.html)

[Another History of the Earth Part 2d](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/247135.html)

[Another History of the Earth Part 2e](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/248914.html)

[Another History of the Earth Part 3a](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/250224.html)

[Another History of the Earth Part 3b](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/250449.html)

[Another History of the Earth Part 3B](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/250649.html)

[Another History of the Earth Part 3c](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/253287.html)

Mylnikovdm also looked at this evidence from a different perspective in his '[Pieces of the Mosaic](./mylnikovdms-quarried-and-terraformed-earth-articles-google-translated.html)' series.

He re-started his 'Another History of the Earth' investigation again in September 2022:

[Another history of the Earth. New facts and observations, part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/437542.html) 

[Another history of the Earth. New facts and observations, part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/437898.html)

[Another history of the Earth. New facts and observations, part 3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/438164.html)

[Another history of the Earth. New facts and observations, part 4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mylnikovdm.livejournal.com/438426.html)

<!--
[Another History of the Earth Part ](
https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=

[Another History of the Earth Part ](
https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=

[Another History of the Earth Part ](
https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=
-->

