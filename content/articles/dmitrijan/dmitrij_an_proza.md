title: Links to Dmitrij_an's articles on the Proza site
date: 2017-09-18
modified: Thu 14 Jan 2021 07:23:06 GMT
category: 
tags: links
slug: 
authors: Dmitry Anatolyevich
summary: Links to Dmitrijan's work on the Proza website
status: published
originally: Дмитрий Ан _ Проза.ру

#### Translated from: 

[https://proza.ru/avtor/dmitrijan](https://proza.ru/avtor/dmitrijan)

- The book "The Impossible is Possible" [http://www.proza.ru/2011/12/07/1438](http://www.proza.ru/2011/12/07/1438)
- Typewritten answers for those who have strengthened their writing [http://www.proza.ru/2013/10/23/1409](http://www.proza.ru/2013/10/23/1409)

[Write a personal message](https://proza.ru/login/messages.html?dmitrijan) [Add to favorites list](https://proza.ru/recommend.html?dmitrijan)

- Works: 259
- [Reviews received:](https://proza.ru/rec_author.html?dmitrijan) 169
- [Reviews written:](https://proza.ru/rec_writer.html?dmitrijan) 35
- [Readers:](https://proza.ru/readers.html?dmitrijan) 48548

#### Works

- [PoM 056 16092017 Gravity generator myth or...](https://proza.ru/2017/09/18/1565) - philosophy 18.09.2017 18:11
- [PoM 055 02082017 Metropolitans or...](https://proza.ru/2017/08/02/948) - philosophy 02.08.2017 12:43
- [PoM 054 31072017 Actual whether the 100th ape effect](https://proza.ru/2017/07/31/1505) - philosophy 31.07.2017 18:13
- [PoM 053 10052017 Learning - light, not learning - a little](https://proza.ru/2017/05/10/1368) - philosophy, 10.05.2017 16:45
- [PoM 052 16032017 What do people do all their life? Eat and...](https://proza.ru/2017/03/16/1391) - philosophy, 16.03.2017 15:25
- [PoM 051 30012017 Ech, Pyramids of the pyramid](https://proza.ru/2017/02/10/1572) - philosophy, 10.02.2017 16:32
- [Dream 20161129 Strange room](https://proza.ru/2016/12/30/1259)- mystic, 30.12.2016 15:38
- [Dream 20160826 Strange room](https://proza.ru/2016/09/15/1669) - fantasy, 15.09.2016 18:08
- [NV 25052016 Impossible h6 hl12-13](https://proza.ru/2016/06/10/1055) - fantasy, 10.06.2016 14:18
- [NV 25052016 Impossible h6 hl11](https://proza.ru/2016/06/03/1012) - Fantasy, 03.06.2016 13:26
- [NV 25052016 Impossible h6 hl9-10](https://proza.ru/2016/05/27/1119) - fantasy, 27.05.2016 14:17
- [PoM 024 23092013 Immortality Elixir](https://proza.ru/2016/04/18/1434) - philosophy, 18.04.2016 15:30
- [Dream 20160307 Sutonic World](https://proza.ru/2016/03/21/1718) - Philosophy, 21.03.2016 18:02
- [Txt 329 301202015 Planet Planet](https://proza.ru/2015/12/30/1597) - Philosophy, 30.12.2015 17:42
- [Txt 290 15072011 What about us?](https://proza.ru/2015/12/18/1547) - philosophy, 18.12.2015 17:27
- [Txt 150 10032009 Pro Time](https://proza.ru/2015/09/18/1302) - Philosophy, 18.09.2015 17:37
- [Txt 328 Egoregors, such egoregors ch1](https://proza.ru/2015/07/07/982) - philosophy, 07.07.2015 14:27
- [PoM 049 15062015 Measures, such measures](https://proza.ru/2015/06/15/1087) - philosophy, 15.06.2015 15:14
- [Dream 20150530 Frozen Lake](https://proza.ru/2015/05/05/1007) - Philosophy, 05.05.2015 14:17
- [PoM 048 24042015 Lies, such lies chapter 1](https://proza.ru/2015/04/24/1495) - philosophy, 24.04.2015 16:45
- [PoM 047 06042015 These mysterious runes](https://proza.ru/2015/04/07/1288) - philosophy, 07.04.2015 15:08
- [PoM 046 22022015 The Missing Gods](https://proza.ru/2015/02/21/2439) - Philosophy, 21.02.2015 23:35
- [Txt 327 20022015 Life as a Zubra](https://proza.ru/2015/02/20/1406) - Philosophy, 20.02.2015 15:25
- [PoM 045 17022015 Star and nowadays it's translated as Zubra](https://proza.ru/2015/02/17/1317) - Philosophy, 17.02.2015 15:32
- [Txt 326 22012015 Look and Peek](https://proza.ru/2015/01/23/1531) - Philosophy, 23.01.2015 17:26
- [PoM 044 16012015 Our Brains Title 2](https://proza.ru/2015/01/17/1443) - Philosophy, 17.01.2015 16:20
- [PoM 044 16012015 Our Brains Title 1](https://proza.ru/2015/01/16/1342) - Fantasy, 16.01.2015 15:24
- [PoM 043 14112014 Various devices, green and red](https://proza.ru/2014/11/14/1097) - fantasy, 14.11.2014 14:50
- [Dream 20141102 Strange zone](https://proza.ru/2014/11/07/1283) - fantasy, 07.11.2014 16:19
- [Txt 152 16032009 About the World of Man As is](https://proza.ru/2014/10/31/1356) - philosophy, 31.10.2014 16:12
- [PoM 042 27102014 Dill, such dill](https://proza.ru/2014/10/27/1328) - philosophy, 27.10.2014 16:33
- [OnLine-010 04082009 Hear Yourself As Is](https://proza.ru/2014/10/22/1459) - Philosophy, 22.10.2014 18:56
- [Dream 20140515 Izhe Cherubim h2 h2](https://proza.ru/2014/09/26/1063) - Fantasy, 26.09.2014 15:23
- [Dream 20140515 Izhe Cherubims h2 h1](https://proza.ru/2014/09/25/965) - Fantasy, 25.09.2014 14:28
- [Dream 20140515 Izhe Cherubims Ch1](https://proza.ru/2014/09/23/1477) - Fantasy, 23.09.2014 18:51
- [Dream 20110415 After UFO landing](https://proza.ru/2014/09/17/1007) - fantasy, 17.09.2014 15:02
- [Txt 121 19122008 Pro As is](https://proza.ru/2014/09/15/1355) - philosophy, 15.09.2014 18:22
- [ViVa 011 13092014 European Officials 5th Column Europe](https://proza.ru/2014/09/14/15) - History and Politics, 14.09.2014 00:04
- [PoM 041 22082014 As is](https://proza.ru/2014/09/08/1505) - Philosophy from Life, 08.09.2014 19:10
- [PoM 040 27082014 Scepter and Power is not just a...](https://proza.ru/2014/08/27/971) - philosophy, 27.08.2014 13:10
- [Dream 20140808 So soft this hard world](https://proza.ru/2014/08/08/2132) - fantasy, 08.08.2014 23:57
- [ViVa 010 08082014 We have long silently retreated](https://proza.ru/2014/08/08/1213) - history and politics, 08.08.2014 15:43
- [ViVa 009 30072014 Shots with](https://proza.ru/2014/07/30/918) - history and politics, 30.07.2014 12:51
- [Dream 20140718 Sleep, Castle and cunning old man](https://proza.ru/2014/07/18/1418) - philosophy, 18.07.2014 17:44
- [Txt 102 29122007 The more we know, the less pony](https://proza.ru/2014/07/17/1445) - philosophy, 17.07.2014 18:46
- [Txt 070 15122006 About Weather As is](https://proza.ru/2014/06/23/1370) - Philosophy, 23.06.2014 16:43
- [Txt 325 20062014 Our Dream Worlds](https://proza.ru/2014/06/20/939) - Philosophy, 20.06.2014 13:40
- [And so wanted from Maidan to the will...](https://proza.ru/2014/06/16/1660) - history and politics, 16.06.2014 18:22
- [PiPs-20140606 Would you cut a fly, son](https://proza.ru/2014/06/09/1446) - history and politics, 09.06.2014 17:32
- [PoM 039 22052014 Brighter than light](https://proza.ru/2014/05/23/1271) - philosophy, 23.05.2014 15:57

- [PoM 038 29042014 And was Slovo](https://proza.ru/2014/04/29/1454) - philosophy, 29.04.2014 18:11
- [ViVa 006 18042014 What is the power? Is it about money?](https://proza.ru/2014/04/18/1356) - history and politics, 18.04.2014 16:24
- [ViVa 005 150420014 Ech, Rus-Matushka](https://proza.ru/2014/04/15/1229) - history and politics, 15.04.2014 15:27
- [PoM 037 28032014 And cracked the world in half](https://proza.ru/2014/03/28/1491) - history and politics, 28.03.2014 17:14
- [PoM 036 26032014 Ech, Golden-Gold](https://proza.ru/2014/03/26/1845) - History and Politics, 26.03.2014 19:30
- [PoM 035 13032014 When Panes Fight Chapter 1](https://proza.ru/2014/03/13/1726) - History and Politics, 13.03.2014 19:02
- [Dream 20140301 Sleep, From the Beginning of Time](https://proza.ru/2014/03/03/1730) - Fantasy, 03.03.2014 18:12
- [PoM 034 28022014 Our Sweet Fear](https://proza.ru/2014/02/28/1410) - Philosophy, 28.02.2014 15:54
- [Txt 323 25022014 We've made it chapter 1](https://proza.ru/2014/02/25/1781) - philosophy, 25.02.2014 19:09
- [PoM 033 20022014 Whether the children h1](https://proza.ru/2014/02/20/1732) - philosophy, 20.02.2014 19:14
- [Txt 206 18122009 About Essences of Ch15 As](https://proza.ru/2014/02/14/1600) - Philosophy, 14.02.2014 16:33
- [RasSkazu 107 16012008 Pro Fragile Life As is](https://proza.ru/2014/02/11/1757) - Philosophy, 11.02.2014 19:09
- [PiP 192 16092009 Pro Essences Chapters 7 As is](https://proza.ru/2014/02/07/1503) - Philosophy, 07.02.2014 16:16
- [RasSkazu 111 25072008 Pro vegetarianism As is](https://proza.ru/2014/01/12/1918) - philosophy, 12.01.2014 18:29
- [Dream 20131231 Sleep, Ray in Something](https://proza.ru/2013/12/31/681) - Fantasy, 31.12.2013 10:51
- [PoM 032 27122013 Eh Triple H1](https://proza.ru/2013/12/28/1431) - Philosophy, 28.12.2013 16:44
- [PoM 031 19122013 And Everything Enemies Surround](https://proza.ru/2013/12/19/1546) - Philosophy, 19.12.2013 18:34
- [PoM 030 06122013 Life We Yo](https://proza.ru/2013/12/06/1298) - Philosophy, 06.12.2013 15:54
- [Dream 20131122 Sleep in a dream](https://proza.ru/2013/12/02/1391) - a mystic, 02.12.2013 17:21
- [PoM 028 19112013 Science is not engaged in the provision of pom](https://proza.ru/2013/11/20/872) - philosophy, 20.11.2013 12:30
- [PoM 027 18112013 And strong Russia The spirit of its people](https://proza.ru/2013/11/18/1305) - philosophy, 18.11.2013 16:51
- [PiP 165 10042009 about Magic Science As is](https://proza.ru/2013/11/11/1522) - Philosophy, 11.11.2013 18:39
- [PiP 167 16042009 About Thought Creating As is](https://proza.ru/2013/11/08/1067) - Philosophy, 08.11.2013 14:37
- [Ivanami da Mariai](https://proza.ru/2013/10/25/1272) - Philosophy, 25.10.2013 16:07
- [PiP 164 08042009 About the Reality of Teleportation As is](https://proza.ru/2013/10/16/1236) - Philosophy, 16.10.2013 16:14
- [RasSkazu 123 31122008 About Hidden Fears As is](https://proza.ru/2013/09/27/1338) - Philosophy, 27.09.2013 16:01
- [PiP 322 04092013 And was the Country before Russia](https://proza.ru/2013/09/04/1615) - Philosophy, 04.09.2013 20:22
- [PiP 319 16052013 Why people do not fly like birds](https://proza.ru/2013/05/16/1805) - philosophy, 16.05.2013 22:56
- [PiP 316 07122012 If hour X comes](https://proza.ru/2012/12/07/1451) - philosophy, 07.12.2012 18:45
- [PiP 315 22102012 Enlightenment will accidentally come](https://proza.ru/2012/10/24/186) - philosophy, 24.10.2012 01:45
- [Professional Opposition](https://proza.ru/2011/12/09/915) - Miniatures, 09.12.2011 14:05
- [Typified answers for those who have strengthened their writing](https://proza.ru/2013/10/23/1409) - literary criticism, 23.10.2013 16:55

- [Impossible possible](https://proza.ru/avtor/dmitrijan&book=1#1) (12)
- [PiP Collection](https://proza.ru/avtor/dmitrijan&book=6#6) (48)
- [OnLine Collection](https://proza.ru/avtor/dmitrijan&book=2#2) (22)
- [Collection of RasSkaz](https://proza.ru/avtor/dmitrijan&book=3#3) (37)
- [PiPs Collection](https://proza.ru/avtor/dmitrijan&book=4#4) (8)
- [Based on](https://proza.ru/avtor/dmitrijan&book=7#7) (26)
- [Dream Dreams](https://proza.ru/avtor/dmitrijan&book=8#8) (14)
- [ViVa Collection](https://proza.ru/avtor/dmitrijan&book=9#9) (6)
- [Poems](https://proza.ru/avtor/dmitrijan&book=5#5) (4)

#### Selected authors:
[Vladimir Mamzerev](https://proza.ru/avtor/mamzerev), [Efima Kulikova](https://proza.ru/avtor/efima), [Dmitry 9](https://proza.ru/avtor/dmitry999), [Olgam](https://proza.ru/avtor/oligamy), [Iya Iya](https://proza.ru/avtor/mamzerevsn)

Links to other resources:

- [Publication of the book "Impossible is possible" on RussoLit](https://proza.ru/go/russolit.ru/profile/DmitrijAN/created/books/)
- [All kinds of things in the Poems](https://proza.ru/go/www.stihi.ru/avtor/dmitrijan)
- [Page on livejournal](https://proza.ru/go/dmitrijan.livejournal.com)
- [Articles on the website of the magazine "Samizdat](https://proza.ru/go/samlib.ru/editors/d/dmitrij_an)
- [Collection of articles by one archive until 2009](https://proza.ru/go/yadi.sk/d/KZ7h_y7-BTFBH)
- [Collection of articles by one archive for 2010](https://proza.ru/go/yadi.sk/d/H7RLHAReBTGJ3)
- [Collection of articles by one archive for 2011](https://proza.ru/go/yadi.sk/d/bbySV0HtBTHXP)
- [Forum on Pi9 website](https://proza.ru/go/pi9.3dn.ru/forum)



© All rights reserved. The original author retains ownership and rights.


