title: Videos: Technology
date: 2021-02-03
modified: 2021-09-04 13:09:55
category: 
tags: technology; videos
slug: 
authors: Various
summary: This page collects together videos about oddities in human technological development. As a test of a third-party video translation servicer, it didn't work as well as hoped but the videos may still be interesting, especially if you speak the native language.
status: published

[![](https://img.pandoraopen.ru/https://img.youtube.com/vi/o0-Y1nD9rvA/0.jpg)](https://youtu.be/o0-Y1nD9rvA)

[Why they hide technology](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/014-otvet-po-teme-pochemu-skryvayut-texnologii/)

- [All Downsub.com Subtitles](https://downsub.com/?url=https://youtu.be/o0-Y1nD9rvA)
- [English subtitle .srt file (RoHT translation)](../../files/014. Ответ по теме. Почему скрывают технологии.-o0-Y1nD9rvA.srt)


[![](https://img.pandoraopen.ru/http://img.youtube.com/vi/xGK2ngpVQg4/0.jpg)](https://youtu.be/xGK2ngpVQg4)

[Ancient geopolymer technologies. Nikolay Subbotin](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/drevnie-geopolimernye-texnologii-nikolaj-subbotin/)

- [Subtitles .srt files](https://downsub.com/?url=https://youtu.be/xGK2ngpVQg4)

<!--
[![](https://img.youtube.com/vi/mCMBzHYZGkg/0.jpg)](https://youtu.be/mCMBzHYZGkg)

[Labyrinths as plasma resonators around the world. Ancient technology](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/labirinty-kak-plazmennye-rezonatory-po-vsemu-miru-drevnie-texnologii/)

- [Downsub subtitle files](https://downsub.com/?url=https://youtu.be/mCMBzHYZGkg)

- [English subtitle file (RoHT translation)](../../files/Лабиринты как плазменные резонаторы по всему миру. Древние технологии.-mCMBzHYZGkg.srt)

Waiting on translation time at happyscribe
-->

Technology-related [videos from 'Forgotten Reality' (not translated)](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/?s=%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8&x=8&y=0)

Examples (not translated):

- Secrets of Ancient Siberia. Mysterious discoveries in the taiga [https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/tajny-drevnej-sibiri-zagadochnye-naxodki-v-tajge/](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/tajny-drevnej-sibiri-zagadochnye-naxodki-v-tajge/)

- Atmospheric electricity [https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/atmosfernoe-elektrichestvo-est-kniga-1915-go-goda/](https://xn--80aaacvi7aqjpqei0jvae5b.xn--p1ai/atmosfernoe-elektrichestvo-est-kniga-1915-go-goda/)

<!--
#### Formatting from:

[http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/](http://sviridovserg.com/2017/05/22/embed-youtube-to-markdown/)
-->
