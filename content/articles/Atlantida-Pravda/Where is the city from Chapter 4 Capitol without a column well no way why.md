title: Where is the city from? Chapter 4. Capitol without a column... well, no way, why?
date: 2013-11-03 00:02:00
modified: 2022-04-24 10:08:06
category:
tags: St Petersburg
slug: 
authors: ZigZag
summary: I looked through the photographs of the Capitol, looked at the columns for a long time... I will not impose my point of view, I will simply post the photos, and you can draw your own conclusions.
status: published
from: http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html
originally: Where is the city from? Chapter 4. Capitol without a column... well, no way, why? .html
local: Where is the city from Chapter 4 Capitol without a column well no way why_files

### Translated from:

[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/3.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available     from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-   links.html). The translation below is kept purely for archival reasons.*

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

I looked through the photographs of the Capitol, looked at the columns for a long time... I will not impose my point of view, I will simply post the photos, and you can draw your own conclusions.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/74916014_large_World_Italy_The_ruins_of_old_Rome_022117_.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/74916014_large_World_Italy_The_ruins_of_old_Rome_022117_.jpg)
-->

[![](http://1.bp.blogspot.com/-Xu1gH5-zu0A/UihJKNCm3UI/AAAAAAAAEao/CF22m5HEV7I/s1600/74916014_large_World_Italy_The_ruins_of_old_Rome_022117_.jpg#clickable)](http://1.bp.blogspot.com/-Xu1gH5-zu0A/UihJKNCm3UI/AAAAAAAAEao/CF22m5HEV7I/s1600/74916014_large_World_Italy_The_ruins_of_old_Rome_022117_.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_004.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_004.jpg)
-->

[![](http://4.bp.blogspot.com/-wqM-AaEsIyM/UihJxOdKVhI/AAAAAAAAEa4/l1rIyPn87jM/s1600/%D0%A0%D0%B8%D0%BC.jpg#clickable)](http://4.bp.blogspot.com/-wqM-AaEsIyM/UihJxOdKVhI/AAAAAAAAEa4/l1rIyPn87jM/s1600/%D0%A0%D0%B8%D0%BC.jpg)

Rome. Old city.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/Roman-Forum.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/Roman-Forum.jpg)
-->

[![](http://1.bp.blogspot.com/-bK2HL-G91so/UihJXtSe35I/AAAAAAAAEaw/SlzeuTNlJxo/s1600/Roman-Forum.jpg#clickable)](http://1.bp.blogspot.com/-bK2HL-G91so/UihJXtSe35I/AAAAAAAAEaw/SlzeuTNlJxo/s1600/Roman-Forum.jpg)

Roman forum.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/kolonna-marka-avreliya-rim.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/kolonna-marka-avreliya-rim.jpg)
-->

[![](http://1.bp.blogspot.com/-q1PX__7OQDE/UihK76e_CDI/AAAAAAAAEbY/TtV6cx7r3yo/s1600/kolonna-marka-avreliya-rim+%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D0%B0+%D0%90%D0%B2%D1%80%D0%B5%D0%BB%D0%B8%D1%8F.jpg#clickable)](http://1.bp.blogspot.com/-q1PX__7OQDE/UihK76e_CDI/AAAAAAAAEbY/TtV6cx7r3yo/s1600/kolonna-marka-avreliya-rim+%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D0%B0+%D0%90%D0%B2%D1%80%D0%B5%D0%BB%D0%B8%D1%8F.jpg)

[Rome. Column of Marcus Aurelius.](http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0_%D0%9C%D0%B0%D1%80%D0%BA%D0%B0_%D0%90%D0%B2%D1%80%D0%B5%D0%BB%D0%B8%D1%8F)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/kolonna_Trojana.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/kolonna_Trojana.jpg)
-->

[![](http://1.bp.blogspot.com/-cDJ0LioT1q0/UihLa9yVZKI/AAAAAAAAEbo/nf14C6LkQt4/s1600/kolonna_Trojana.jpg#clickable)](http://1.bp.blogspot.com/-cDJ0LioT1q0/UihLa9yVZKI/AAAAAAAAEbo/nf14C6LkQt4/s1600/kolonna_Trojana.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_015.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_015.jpg)
-->

[![](http://3.bp.blogspot.com/-TIGRUR9eqqY/UihLaGOKDKI/AAAAAAAAEbg/fT9n1dUtIr0/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%A2%D1%80%D0%B0%D1%8F%D0%BD%D0%B0.jpg#clickable)](http://3.bp.blogspot.com/-TIGRUR9eqqY/UihLaGOKDKI/AAAAAAAAEbg/fT9n1dUtIr0/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%A2%D1%80%D0%B0%D1%8F%D0%BD%D0%B0.jpg)

[Rome. Trajan's Column.](http://ru.wikipedia.org/wiki/%CA%EE%EB%EE%ED%ED%E0_%D2%F0%E0%FF%ED%E0)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/panteon-v-parizhe_21737.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/panteon-v-parizhe_21737.jpg)
-->

[![](http://2.bp.blogspot.com/-XNSlP4xXuSM/Ui3YV1cAkaI/AAAAAAAAEhQ/JF0LWpA3R38/s1600/panteon-v-parizhe_21737.jpg#clickable)](http://2.bp.blogspot.com/-XNSlP4xXuSM/Ui3YV1cAkaI/AAAAAAAAEhQ/JF0LWpA3R38/s1600/panteon-v-parizhe_21737.jpg)

[Pantheon. Paris. France.](https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D0%BD%D1%82%D0%B5%D0%BE%D0%BD_(%D0%9F%D0%B0%D1%80%D0%B8%D0%B6))

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/it_roma_panteon.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/it_roma_panteon.jpg)
-->

[![](https://4.bp.blogspot.com/-5YaTHW9XvTE/V2ESAF6xtlI/AAAAAAAAH3U/0HnWoKuArnALHqdv12Tz1YGpu3cpfRwZQCLcB/s1600/it_roma_panteon.jpg#clickable)](https://4.bp.blogspot.com/-5YaTHW9XvTE/V2ESAF6xtlI/AAAAAAAAH3U/0HnWoKuArnALHqdv12Tz1YGpu3cpfRwZQCLcB/s1600/it_roma_panteon.jpg)

[Pantheon. Rome. Italy.](https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D0%BD%D1%82%D0%B5%D0%BE%D0%BD_(%D0%A0%D0%B8%D0%BC))

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_006.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_006.jpg)
-->

[![](http://1.bp.blogspot.com/-WGGzEKoQSy0/UihKKXEgHEI/AAAAAAAAEbA/gOR52YFIN58/s1600/%D0%94%D0%B5%D0%BB%D0%B8+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0.jpg#clickable)](http://1.bp.blogspot.com/-WGGzEKoQSy0/UihKKXEgHEI/AAAAAAAAEbA/gOR52YFIN58/s1600/%D0%94%D0%B5%D0%BB%D0%B8+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_007.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_007.jpg)
-->

[![](http://4.bp.blogspot.com/-ZRI8Qg-YnTA/UihKPbE-16I/AAAAAAAAEbI/XkMy3TNAItE/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%B2+%D0%94%D0%B5%D0%BB%D0%B8.jpg#clickable)](http://4.bp.blogspot.com/-ZRI8Qg-YnTA/UihKPbE-16I/AAAAAAAAEbI/XkMy3TNAItE/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%B2+%D0%94%D0%B5%D0%BB%D0%B8.jpg)

[Daly. India. Kutubov's column.](http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/blog-post_20.html)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/CapitoliumTunis.JPG)]Where is the city from Chapter 4 Capitol without a column well no way why_files/CapitoliumTunis.JPG)
-->

[![](http://3.bp.blogspot.com/-_2xwGKe2b6I/UihKgoqpm8I/AAAAAAAAEbQ/Y4ctH32e8UM/s1600/Capitolium+Tunis.JPG#clickable)](http://3.bp.blogspot.com/-_2xwGKe2b6I/UihKgoqpm8I/AAAAAAAAEbQ/Y4ctH32e8UM/s1600/Capitolium+Tunis.JPG)

Tunisian Capitol.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/ba24.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/ba24.jpg)
-->

[![](http://3.bp.blogspot.com/-hEm9ZY_RB1w/Ui3XKKs0JAI/AAAAAAAAEgs/t2qvDKDDUjU/s1600/ba24.jpg#clickable)](http://3.bp.blogspot.com/-hEm9ZY_RB1w/Ui3XKKs0JAI/AAAAAAAAEgs/t2qvDKDDUjU/s1600/ba24.jpg)

[Temple of Jupiter. Baalbek. Lebanon.](http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/44.html)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/92759love.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/92759love.jpg)
-->

[![](http://1.bp.blogspot.com/-EkFrZZzcRds/Ui3Xey8snwI/AAAAAAAAEg0/BibYx7x673M/s1600/92759love.jpg#clickable)](http://1.bp.blogspot.com/-EkFrZZzcRds/Ui3Xey8snwI/AAAAAAAAEg0/BibYx7x673M/s1600/92759love.jpg)

[Parthenon. Athens. Greece.](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_405.html) One project, right?

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/0_a154c_2b28916d_XXXL.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/0_a154c_2b28916d_XXXL.jpg)
-->

[![](http://4.bp.blogspot.com/-GLu2qle1pe8/UjKiVElruQI/AAAAAAAAEi0/QiLmO3qjnSU/s1600/0_a154c_2b28916d_XXXL.jpg#clickable)](http://4.bp.blogspot.com/-GLu2qle1pe8/UjKiVElruQI/AAAAAAAAEi0/QiLmO3qjnSU/s1600/0_a154c_2b28916d_XXXL.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/birja_3.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/birja_3.jpg)
-->

[![](http://4.bp.blogspot.com/-ATdarPsRmSY/UjKjHtbgDrI/AAAAAAAAEjA/LW7hO3xT6g4/s1600/birja_3.jpg#clickable)](http://4.bp.blogspot.com/-ATdarPsRmSY/UjKjHtbgDrI/AAAAAAAAEjA/LW7hO3xT6g4/s1600/birja_3.jpg)

Exchange building. Saint Petersburg. Russia.
 
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/033.jpg)](http://2.bp.blogspot.com/-NhgCMLUW2Bs/UihNATxArVI/AAAAAAAAEb0/csyw4TxwNdI/s1600/033.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/450px-nelsons-column.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/450px-nelsons-column.jpg)
-->

[![](http://1.bp.blogspot.com/-lofnTiCiskw/UihNAzCqa4I/AAAAAAAAEb4/Ld6MMbP_-tQ/s1600/450px-nelsons-column.jpg#clickable)](http://1.bp.blogspot.com/-lofnTiCiskw/UihNAzCqa4I/AAAAAAAAEb4/Ld6MMbP_-tQ/s1600/450px-nelsons-column.jpg)

Column of Vice Admiral Nelson in Trafalgar Square.

[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/800px-Trafalgar_Square_360_Panorama_Cropped_Sky_London_-_Jun.jpg)](http://4.bp.blogspot.com/-63iZxaUxKbI/UihNQyA8a6I/AAAAAAAAEcE/KgcjuDLnCZI/s1600/800px-Trafalgar_Square_360_Panorama_Cropped_Sky,_London_-_Jun_2009.jpg)

[Trafalgar Square. London.](http://ru.wikipedia.org/wiki/%D2%F0%E0%F4%E0%EB%FC%E3%E0%F0%F1%EA%E0%FF_%EF%EB%EE%F9%E0%E4%FC)

**Russian:**

Эти фотографии можно считать глубокой историей. А что если отреставрировать (восстановить) эти конструкции, или использовав имеющийся уже строительный материал поставить их в другом месте? Давайте пофантазируем вместе...

**English:**

These photographs can be considered a deep history. And what if you restore (restore) these structures, or use existing building material to put them elsewhere? Let's fantasize together...

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/capitol-building.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/capitol-building.jpg)
-->

[![](http://4.bp.blogspot.com/-gn4ktx4Zkeg/UihOrzcXlkI/AAAAAAAAEcQ/_BPoK1F3i04/s1600/capitol-building.jpg#clickable)](http://4.bp.blogspot.com/-gn4ktx4Zkeg/UihOrzcXlkI/AAAAAAAAEcQ/_BPoK1F3i04/s1600/capitol-building.jpg)

**Russian:**

Капитолий + колонна.

**English:**

Capitol + column.

**Russian:**

Очень занятна картинка с направлением улиц вокруг капитолия

**English:**

Very busy picture with the direction of the streets around the capitol.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_009.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_009.jpg)
-->

[![](http://1.bp.blogspot.com/-1-pmVHAJ4_k/UihX1XB49kI/AAAAAAAAEco/L_tMgMq2v5k/s1600/%D0%90%D0%9A.jpg#clickable)](http://1.bp.blogspot.com/-1-pmVHAJ4_k/UihX1XB49kI/AAAAAAAAEco/L_tMgMq2v5k/s1600/%D0%90%D0%9A.jpg)

**Russian:**

сравним с картой Санкт Петербурга.

**English:**

we'll compare it to a map of St. Petersburg.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_010.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_010.jpg)
-->

[![](http://4.bp.blogspot.com/-SJ64MnbhKsM/Uihdl-vwl3I/AAAAAAAAEc4/8Q0vlAUa5Eo/s1600/%D0%90%D0%B4%D0%BC.jpg#clickable)](http://4.bp.blogspot.com/-SJ64MnbhKsM/Uihdl-vwl3I/AAAAAAAAEc4/8Q0vlAUa5Eo/s1600/%D0%90%D0%B4%D0%BC.jpg)

**Russian:**

Треугольники, треугольники... Всё те же треугольники с памятниками "смотрящим по центру".

**English:**

Triangles, triangles... Still the same triangles with monuments "looking at центру".

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/main.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/main.jpg)
-->

[![](http://1.bp.blogspot.com/-tGN3M9sNJyQ/UiheIuAwsoI/AAAAAAAAEdA/1FwWL-BdROU/s1600/main.jpg#clickable)](http://1.bp.blogspot.com/-tGN3M9sNJyQ/UiheIuAwsoI/AAAAAAAAEdA/1FwWL-BdROU/s1600/main.jpg)

**Russian:**

Но не будем уходить от темы, идём дальше.

**English:**

But let's not get off the subject, let's move on.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_013.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_013.jpg)
-->

[![](http://1.bp.blogspot.com/-ZH6X0ezbxKc/UihhPpztb3I/AAAAAAAAEdk/w7skeiKz51w/s1600/%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%9E%D1%81%D1%82%D0%B8%D0%BD+%D0%A2%D0%B5%D1%85%D0%B0%D1%81.jpg#clickable)](http://1.bp.blogspot.com/-ZH6X0ezbxKc/UihhPpztb3I/AAAAAAAAEdk/w7skeiKz51w/s1600/%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%9E%D1%81%D1%82%D0%B8%D0%BD+%D0%A2%D0%B5%D1%85%D0%B0%D1%81.jpg)

**Russian:**

Капитолий штата Техас, город Остин. США.

**English:**

Capitol of Texas, Austin. USA.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/kapitoliy_shtata_tehas.jpeg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/kapitoliy_shtata_tehas.jpeg)
-->

[![](http://1.bp.blogspot.com/-wYRis3mVeCo/UihgRtHJtRI/AAAAAAAAEdU/3Gdza_p3R78/s1600/kapitoliy_shtata_tehas.png#clickable)](http://1.bp.blogspot.com/-wYRis3mVeCo/UihgRtHJtRI/AAAAAAAAEdU/3Gdza_p3R78/s1600/kapitoliy_shtata_tehas.png)

**Russian:**

... и статуя к нему.

**English:**

... ...and a statue to him.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_003.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_003.jpg)
-->

[![](http://2.bp.blogspot.com/-8ib6t51tNgg/Uihg9b3danI/AAAAAAAAEdc/_XoAtIAEFcQ/s1600/%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%B2+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B5+%D0%9C%D1%8D%D0%B4%D0%B8%D1%81%D0%BE%D0%BD.jpg#clickable)](http://2.bp.blogspot.com/-8ib6t51tNgg/Uihg9b3danI/AAAAAAAAEdc/_XoAtIAEFcQ/s1600/%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%B2+%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B5+%D0%9C%D1%8D%D0%B4%D0%B8%D1%81%D0%BE%D0%BD.jpg)

**Russian:**

Капитолий в городе Мэдисон. США.

**English:**

Capitol in Madison. USA.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/DenverCapital.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/DenverCapital.jpg)
-->

[![](http://3.bp.blogspot.com/-zFDyelWYvKo/UihixKWmxnI/AAAAAAAAEd8/2byLpsSgpL4/s1600/DenverCapital+%D0%9A%D0%BE%D0%BB%D0%BE%D1%80%D0%B0%D0%B4%D1%81%D0%BA%D0%B8%D0%B9+%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9.jpg#clickable)](http://3.bp.blogspot.com/-zFDyelWYvKo/UihixKWmxnI/AAAAAAAAEd8/2byLpsSgpL4/s1600/DenverCapital+%D0%9A%D0%BE%D0%BB%D0%BE%D1%80%D0%B0%D0%B4%D1%81%D0%BA%D0%B8%D0%B9+%D0%9A%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9.jpg)

**Russian:**

Колорадский Капитолий. Денвер. США.

**English:**

Colorado Capitol. Denver. USA.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/vfiles8530.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/vfiles8530.jpg)
-->

[![](http://1.bp.blogspot.com/-gGXlL_JdfUY/Uic5ywiruJI/AAAAAAAAEaI/kTet2aZcX6Q/s640/vfiles8530.jpg#clickable)](http://1.bp.blogspot.com/-gGXlL_JdfUY/Uic5ywiruJI/AAAAAAAAEaI/kTet2aZcX6Q/s640/vfiles8530.jpg)

**Russian:**

Капитолий штата Айова в Де-Мойне. США.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/68084955_0142.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/68084955_0142.jpg)
-->

[![](http://1.bp.blogspot.com/-hDAQFM_ROIQ/UihyBSws75I/AAAAAAAAEfU/VfDF8zjdAQ4/s1600/68084955_0142.jpg#clickable)](http://1.bp.blogspot.com/-hDAQFM_ROIQ/UihyBSws75I/AAAAAAAAEfU/VfDF8zjdAQ4/s1600/68084955_0142.jpg)

**English:**

Iowa State Capitol in Des Moines. США.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/68084955_0142.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/68084955_0142.jpg)
-->

[![](http://1.bp.blogspot.com/-hDAQFM_ROIQ/UihyBSws75I/AAAAAAAAEfU/VfDF8zjdAQ4/s1600/68084955_0142.jpg)
](http://1.bp.blogspot.com/-hDAQFM_ROIQ/UihyBSws75I/AAAAAAAAEfU/VfDF8zjdAQ4/s1600/68084955_0142.jpg)

**Russian:**

Капитолий в штате... Да нет, это не капитолий, это Исаакий, но быстро подсовывая картинки можно и Исаакий за капитолий принять, отличий то и не так много.

**English:**

The capitol in the state... No, it's not a capitol, it's Isaac's, but you can take Isaac's picture as a capitol quickly, there's not much difference.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/WW1S1543-1sAP.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/WW1S1543-1sAP.jpg)
-->

[![](http://1.bp.blogspot.com/-lfyA6KkD9mU/UicjpxVrsjI/AAAAAAAAEZs/qV_LeUqaNsc/s640/WW1S1543-1sAP.jpg#clickable)](http://1.bp.blogspot.com/-lfyA6KkD9mU/UicjpxVrsjI/AAAAAAAAEZs/qV_LeUqaNsc/s640/WW1S1543-1sAP.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/7557799784_5996c183b8_o.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/7557799784_5996c183b8_o.jpg)
-->

[![](http://2.bp.blogspot.com/-7W91kyNCAAk/Ui1brq3QL1I/AAAAAAAAEf0/W3mcRN-b3oI/s1600/7557799784_5996c183b8_o.jpg#clickable)](http://2.bp.blogspot.com/-7W91kyNCAAk/Ui1brq3QL1I/AAAAAAAAEf0/W3mcRN-b3oI/s1600/7557799784_5996c183b8_o.jpg)

**Russian:**

Капитолий штата Массачусетс в Бостоне. США.

**English:**

Massachusetts State Capitol in Boston. USA.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_014.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_014.jpg)
-->

[![](http://3.bp.blogspot.com/-ksMCeDWh1Lo/Uihhrk0LH_I/AAAAAAAAEds/Ns-2QCT8rFQ/s1600/%D0%9A%D0%B0%D1%80%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%9A%D1%83%D0%B1%D0%B0+%D0%93%D0%B0%D0%B2%D0%B0%D0%BD%D0%BD%D0%B0.jpg#clickable)](http://3.bp.blogspot.com/-ksMCeDWh1Lo/Uihhrk0LH_I/AAAAAAAAEds/Ns-2QCT8rFQ/s1600/%D0%9A%D0%B0%D1%80%D0%B8%D1%82%D0%BE%D0%BB%D0%B8%D0%B9+%D0%9A%D1%83%D0%B1%D0%B0+%D0%93%D0%B0%D0%B2%D0%B0%D0%BD%D0%BD%D0%B0.jpg)

**Russian:**

Кубинский капитолий. Гаванна.

**English:**

Cuban Capitol. Havana.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_012.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_012.jpg)
-->

[![](http://2.bp.blogspot.com/-MwnmNdGRGec/UihiLBhnclI/AAAAAAAAEd0/FhfAXBJeKdE/s1600/%D0%91%D1%83%D1%8D%D0%BD%D0%BE%D1%81+%D0%90%D0%B9%D1%80%D0%B5%D1%81.jpg#clickable)](http://2.bp.blogspot.com/-MwnmNdGRGec/UihiLBhnclI/AAAAAAAAEd0/FhfAXBJeKdE/s1600/%D0%91%D1%83%D1%8D%D0%BD%D0%BE%D1%81+%D0%90%D0%B9%D1%80%D0%B5%D1%81.jpg)

**Russian:**

Капитолий в городе Буэнос Айрес. Аргентина.

**English:**

Capitol in the city of Buenos Aires. Argentina.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/1320054451_berlien.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/1320054451_berlien.jpg)
-->

[![](http://4.bp.blogspot.com/-3pzTw-VYvIs/Ui3WfFuXDAI/AAAAAAAAEgk/7vvDs72Vhbw/s1600/1320054451_berlien..jpg#clickable)](http://4.bp.blogspot.com/-3pzTw-VYvIs/Ui3WfFuXDAI/AAAAAAAAEgk/7vvDs72Vhbw/s1600/1320054451_berlien..jpg)

**Russian:**

Рейхстаг. Берлин. Германия.

**English:**

The Reichstag. Berlin. Germany.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_1287409937.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_1287409937.jpg)
-->

[![](http://1.bp.blogspot.com/-HBdKOabuuks/UjLIxRKAOZI/AAAAAAAAEkM/80hOYFQ2P-M/s1600/1_1287409937.jpg#clickable)](http://1.bp.blogspot.com/-HBdKOabuuks/UjLIxRKAOZI/AAAAAAAAEkM/80hOYFQ2P-M/s1600/1_1287409937.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_0429200a9d25ce0484fa497b5319a5ef.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_0429200a9d25ce0484fa497b5319a5ef.jpg)
-->

[![](http://3.bp.blogspot.com/-EjEwgzoHxyo/UjKhSbOnSNI/AAAAAAAAEig/wwNeM_I5oKs/s1600/1_0429200a9d25ce0484fa497b5319a5ef.jpg#clickable)](http://3.bp.blogspot.com/-EjEwgzoHxyo/UjKhSbOnSNI/AAAAAAAAEig/wwNeM_I5oKs/s1600/1_0429200a9d25ce0484fa497b5319a5ef.jpg)

**Russian:**

Кафедральный собор в Хельсинки. Финляндия.

**English:**

The cathedral in Helsinki. Finland.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_8cbc06a56e73bbe47053d3230b1e6653.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/1_8cbc06a56e73bbe47053d3230b1e6653.jpg)
-->

[![](http://3.bp.blogspot.com/-8SzfsjeGJ5U/Ul9-HB0IjDI/AAAAAAAAE7I/fNTiTgEKqbg/s1600/1_8cbc06a56e73bbe47053d3230b1e6653.jpg#clickable)](http://3.bp.blogspot.com/-8SzfsjeGJ5U/Ul9-HB0IjDI/AAAAAAAAE7I/fNTiTgEKqbg/s1600/1_8cbc06a56e73bbe47053d3230b1e6653.jpg)

**Russian:**

Собор Вознесения Пресвятой Девы Марии и Святого Адальберта или более простое название строения - это Эстергомская базилика. Венгрия.

**English:**

The Cathedral of the Ascension of the Blessed Virgin Mary and St. Adalbert, or more simply the name of the structure, is the Esztergom Basilica. Hungary.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/48361192.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/48361192.jpg)
-->

[![](http://2.bp.blogspot.com/-aZoH__6ughI/Urfnk6IFviI/AAAAAAAAGGA/CFJcFUEwSZQ/s1600/48361192.jpg#clickable)](http://2.bp.blogspot.com/-aZoH__6ughI/Urfnk6IFviI/AAAAAAAAGGA/CFJcFUEwSZQ/s1600/48361192.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/___.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/___.jpg)
-->

[![](http://4.bp.blogspot.com/-8pF5G87vSf4/UrfnkXDRALI/AAAAAAAAGF8/Cce6FW-OUBk/s1600/%D0%A1%D0%BE%D0%B1%D0%BE%D1%80_%D0%A1%D0%B2%D1%8F%D1%82%D0%BE%D0%B3%D0%BE_%D0%9F%D0%B0%D0%B2%D0%BB%D0%B0_(%D0%9B%D0%BE%D0%BD%D0%B4%D0%BE%D0%BD).jpg#clickable)](http://4.bp.blogspot.com/-8pF5G87vSf4/UrfnkXDRALI/AAAAAAAAGF8/Cce6FW-OUBk/s1600/%D0%A1%D0%BE%D0%B1%D0%BE%D1%80_%D0%A1%D0%B2%D1%8F%D1%82%D0%BE%D0%B3%D0%BE_%D0%9F%D0%B0%D0%B2%D0%BB%D0%B0_(%D0%9B%D0%BE%D0%BD%D0%B4%D0%BE%D0%BD).jpg)

**Russian:**

Собор Святого Павла. Лондон. Великобритания.

**English:**

St. Paul's Cathedral. London. Great Britain.

**Russian:**

[Как они все похожи.](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html)

**English:**

[How alike they all look.](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/stupa.gif)]Where is the city from Chapter 4 Capitol without a column well no way why_files/stupa.gif)
-->

[![](http://3.bp.blogspot.com/-027m06r4Vb8/Uicl9KZSbfI/AAAAAAAAEZ4/idEepZjA6ps/s1600/stupa.gif#clickable)](http://3.bp.blogspot.com/-027m06r4Vb8/Uicl9KZSbfI/AAAAAAAAEZ4/idEepZjA6ps/s1600/stupa.gif)

**Russian:**

Теперь обратим своё внимание на колонны...

**English:**

Now let's turn our attention to the columns...

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_005.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_005.jpg)
-->

[![](http://1.bp.blogspot.com/-iestIfSYETg/UihkwcORPDI/AAAAAAAAEeQ/ycah1H8dd5o/s1600/%D0%94%D0%B2%D0%BE%D1%80%D1%86%D0%BE%D0%B2%D0%B0%D1%8F+%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D1%8C+%D0%92%D0%B0%D1%80%D1%88%D0%B0%D0%B2%D0%B0+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%A1%D0%B8%D0%B3%D0%B8%D0%B7%D0%BC%D1%83%D0%BD%D0%B4%D0%B0.jpg#clickable)](http://1.bp.blogspot.com/-iestIfSYETg/UihkwcORPDI/AAAAAAAAEeQ/ycah1H8dd5o/s1600/%D0%94%D0%B2%D0%BE%D1%80%D1%86%D0%BE%D0%B2%D0%B0%D1%8F+%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D1%8C+%D0%92%D0%B0%D1%80%D1%88%D0%B0%D0%B2%D0%B0+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%A1%D0%B8%D0%B3%D0%B8%D0%B7%D0%BC%D1%83%D0%BD%D0%B4%D0%B0.jpg)

**Russian:**

Дворцовая площадь. Колонна Сигизмунда. Варшава. Польша.

**English:**

Palace Square. Sigismund Column. Warsaw. Poland.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a.jpg)
-->

[![](http://3.bp.blogspot.com/-4LYmQvitjiU/Uihk9EWC75I/AAAAAAAAEeY/2xwVfncQodc/s1600/%D0%98%D0%B6%D0%B5%D0%B2%D1%81%D0%BA+%D0%9C%D0%B8%D1%85%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%BE%D0%BC%D1%83+%D0%9A%D0%BD%D1%8F%D0%B7%D1%8E+%D0%9C%D0%B8%D1%85%D0%B0%D0%B8%D0%BB%D1%83+%D0%9F%D0%B0%D0%B2%D0%BB%D0%BE%D0%B2%D0%B8%D1%87%D1%83.jpg#clickable)](http://3.bp.blogspot.com/-4LYmQvitjiU/Uihk9EWC75I/AAAAAAAAEeY/2xwVfncQodc/s1600/%D0%98%D0%B6%D0%B5%D0%B2%D1%81%D0%BA+%D0%9C%D0%B8%D1%85%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2%D1%81%D0%BA%D0%B0%D1%8F+%D0%BA%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%BE%D0%BC%D1%83+%D0%9A%D0%BD%D1%8F%D0%B7%D1%8E+%D0%9C%D0%B8%D1%85%D0%B0%D0%B8%D0%BB%D1%83+%D0%9F%D0%B0%D0%B2%D0%BB%D0%BE%D0%B2%D0%B8%D1%87%D1%83.jpg)

**Russian:**

Михайловская колонна, памятник Великому Князю Михаилу Павловичу. Ижевск. Россия.

**English:**

Mikhailovsky Column, monument to Grand Duke Mikhail Pavlovich. Izhevsk. Izhevsk. Russia.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_008.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_008.jpg)
-->

[![](http://2.bp.blogspot.com/-t9k1-Q4_I2c/UihleQTumyI/AAAAAAAAEeo/6RiT3HBUI0w/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%9A%D0%BE%D0%BD%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B0+%D0%B2+%D0%91%D1%80%D1%8E%D1%81%D1%81%D0%B5%D0%BB%D0%B5.jpg#clickable)](http://2.bp.blogspot.com/-t9k1-Q4_I2c/UihleQTumyI/AAAAAAAAEeo/6RiT3HBUI0w/s1600/%D0%9A%D0%BE%D0%BB%D0%BE%D0%BD%D0%BD%D0%B0+%D0%9A%D0%BE%D0%BD%D0%B3%D1%80%D0%B5%D1%81%D1%81%D0%B0+%D0%B2+%D0%91%D1%80%D1%8E%D1%81%D1%81%D0%B5%D0%BB%D0%B5.jpg)

**Russian:**

Колонна Конгресса в Брюсселе. Бельгия.

**English:**

Column of Congress in Brussels. Belgium.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_011.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_011.jpg)
-->

[![](http://4.bp.blogspot.com/-7uPHj_vmRs0/Uihl86opoiI/AAAAAAAAEew/Thvguakk9d0/s1600/%D0%A0%D0%B8%D0%B3%D0%B0+%D0%97%D0%B0%D0%BC%D0%BA%D0%BE%D0%B2%D0%B0%D1%8F+%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D1%8C+%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%BC+%D0%B2%D0%BE%D0%B9%D1%81%D0%BA%D0%B0%D0%BC.jpg#clickable)](http://4.bp.blogspot.com/-7uPHj_vmRs0/Uihl86opoiI/AAAAAAAAEew/Thvguakk9d0/s1600/%D0%A0%D0%B8%D0%B3%D0%B0+%D0%97%D0%B0%D0%BC%D0%BA%D0%BE%D0%B2%D0%B0%D1%8F+%D0%BF%D0%BB%D0%BE%D1%89%D0%B0%D0%B4%D1%8C+%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%BC+%D0%B2%D0%BE%D0%B9%D1%81%D0%BA%D0%B0%D0%BC.jpg)

**Russian:**

Замковая площадь, памятник русским войскам. Рига. Латвия.

**English:**

Castle Square, a monument to Russian troops. Riga. Latvia.

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/MonumenttoCristobalColonLocatedintheportofBarcelona.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/MonumenttoCristobalColonLocatedintheportofBarcelona.jpg)
-->

[![](http://1.bp.blogspot.com/-mEGJTNh9La0/UihkHtTIYCI/AAAAAAAAEeI/qrgArTy7A4c/s1600/Monument+to+Cristobal+Colon+Located+in+the+port+of+Barcelona.jpg#clickable)](http://1.bp.blogspot.com/-mEGJTNh9La0/UihkHtTIYCI/AAAAAAAAEeI/qrgArTy7A4c/s1600/Monument+to+Cristobal+Colon+Located+in+the+port+of+Barcelona.jpg)

<!-- Local
[![]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_002.jpg)]Where is the city from Chapter 4 Capitol without a column well no way why_files/a_002.jpg)
-->

[![](http://4.bp.blogspot.com/-96A84QVYkWs/UihmR3gOrmI/AAAAAAAAEfA/89x3fXbwWVc/s1600/%D0%9F%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D0%9A%D0%BE%D0%BB%D1%83%D0%BC%D0%B1%D1%83,+%D0%98%D1%81%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F.jpg#clickable)](http://4.bp.blogspot.com/-96A84QVYkWs/UihmR3gOrmI/AAAAAAAAEfA/89x3fXbwWVc/s1600/%D0%9F%D0%B0%D0%BC%D1%8F%D1%82%D0%BD%D0%B8%D0%BA+%D0%9A%D0%BE%D0%BB%D1%83%D0%BC%D0%B1%D1%83,+%D0%98%D1%81%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F.jpg)

Monument to Cristobal Colon Located in the port of Barcelona, Spain.

**Russian:**

Можно продолжать ещё, но тогда эта затея станет бесконечной...

**English:**

We can go on, but then it'll be endless...

**Russian:**

В интернете таких картинок уже даже не сотни, их тысячи, миллионы. Сейчас намного проще понять некоторые фрагменты истории. Невозможно сложить целостную картинку мировой истории из пазлов, имея в наличии их всего несколько штук. Но они, пазлы,  всё равно, рано или поздно, появляются то тут то там, важно не пройти мимо, обратить на них внимание, и попытаться найти им место, не наугад, а именно то место, где они должны быть на самом деле. Все эти постройки мне больше напоминают [культ карго](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html), я уже писал об этом ранее в статье Один проект, [один архитектор, или...?](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html).

**English:**

There aren't even hundreds of such pictures on the Internet, there are thousands, millions of them. Now it's much easier to understand some fragments of history. It is impossible to put together a complete picture of world history of puzzles, having only a few of them. But they, puzzles, anyway, sooner or later, appear here and there, it is important not to pass by, to pay attention to them, and try to find them a place, not at random, namely the place where they should actually be. All these buildings remind me more of [a cargo cult](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html), I've already written about it earlier in One Project, [one architect, or...?](http://atlantida-pravda-i-vimisel.blogspot.com/2012/07/blog-post_1.html).

**Russian:**

Мы пытаемся воссоздать то, что когда то было технологичным и выполняло определённые функции, в большей своей мере нам пока не понятные. В этом плане уже написан цикл статей, будите иметь время - почитайте. Даю ссылки:

**English:**

We are trying to recreate something that was once technological and performed certain functions that are mostly not yet clear to us. In this respect, a series of articles has already been written, and if you have time, read them. I give you the links:

**Russian:**

[Кто построил Колизей?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_18.html)

**English:**

[Who built the Colosseum?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_18.html)

**Russian:**

[Мегалитические комплексы окруженные акведуками и находящиеся возле рек и морей, зачем?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_5804.html)

**English:**

[Megalithic complexes surrounded by aqueducts and located near rivers and seas, why?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_5804.html)

**Russian:**

[Акведуки... Зачем?](http://atlantida-pravda-i-vimisel.blogspot.com/2012/04/blog-post.html)

**English:**

[The Aqueducts... What for?](http://atlantida-pravda-i-vimisel.blogspot.com/2012/04/blog-post.html)

**Russian:**

Пантеон - бетонный мегалит?!](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_2576.html)

**English:**

[Pantheon - concrete megalith?!](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_2576.html)

**Russian:**

[Высокотехнологичные предки, или...?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_21.html)

**English:**

[High-tech ancestors, or...?](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_21.html)

**Russian:**

...и много другого.

Копируя статью, не забывайте указывать авторство.

**English:**

...and much more.

When copying this article, remember to mention authorship.

**Russian:**

Адрес полной версии тут: ["Откуда город? (автор ZigZag)"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

**English:**

The full version address is here: ["Where's the city from? (by ZigZag)"](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

Author: ZigZag

© All rights reserved. The original author retains ownership and rights.

