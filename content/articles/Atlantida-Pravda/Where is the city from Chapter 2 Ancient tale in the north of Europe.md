title: Where is the city from? Chapter 2. Ancient tale in the north of Europe
date: 2013-11-02
modified: 2022-04-24 10:07:36
category: 
tags: St Petersburg
slug: 
authors: ZigZag
summary: Historians of the official school write that in a couple of decades on the Neva, without roads, a city appears in the swamps (note, the northernmost in the world at that time) with a modern layout, with avenues that no one had built before, a sewage system, channels with granite lining, massive stone structures, and columns, columns and columns...
status: published
originally: Where is the city from? Chapter 2. Ancient tale in the north of Europe.html
from: http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/2.html
local: Where is the city from Chapter 2 Ancient tale in the north of Europe_files

### Translated from:
[http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/2.html](http://atlantida-pravda-i-vimisel.blogspot.com/2013/11/2.html)

*A better translation of this article - and translations of all Atlantida-Pravda's articles about St Petersburg's missing history - is available     from: [Where is the city from? Introduction and links](https://roht.mindhackers.org/where-is-the-city-from-introduction-and-   links.html). The translation below is kept purely for archival reasons.*

This blog was created with the aim of understanding and accepting, or refuting the story that we know from textbooks.

Historians of the official school write that in a couple of decades on the River Neva, without roads, a city appears in the swamps (note, the northernmost in the world at that time) with a modern layout, with avenues that no one had built before, a sewage system, channels with granite lining, massive stone structures, and columns, columns and columns (what kind of laboriousness, why, for what?) ... like in Lebanon Baalbek, Greece Parthenon, Egypt Giza, etc. 

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/4665.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/4665.jpg)
-->

[![](http://2.bp.blogspot.com/-vZ9pPWAJ9oo/UBDaXxkgVhI/AAAAAAAAB90/3uAa8MjmWsI/s1600/4665.jpg#clickable)](http://2.bp.blogspot.com/-vZ9pPWAJ9oo/UBDaXxkgVhI/AAAAAAAAB90/3uAa8MjmWsI/s1600/4665.jpg)

St. Petersburg
<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6182329650_813a7b4d21_o.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6182329650_813a7b4d21_o.jpg)
-->

[![](http://2.bp.blogspot.com/-3Wr4HgxXDks/UBDarg4O1II/AAAAAAAAB98/ABQ97idA3w0/s1600/6182329650_813a7b4d21_o.jpg#clickable)](http://2.bp.blogspot.com/-3Wr4HgxXDks/UBDarg4O1II/AAAAAAAAB98/ABQ97idA3w0/s1600/6182329650_813a7b4d21_o.jpg)

St. Petersburg

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba19.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba19.jpg)
-->

[![](http://3.bp.blogspot.com/-9rh_o5T8JK8/UBDas8mmJRI/AAAAAAAAB-A/_ZF06ENJ4hM/s1600/ba19.jpg#clickable)](http://3.bp.blogspot.com/-9rh_o5T8JK8/UBDas8mmJRI/AAAAAAAAB-A/_ZF06ENJ4hM/s1600/ba19.jpg)

[But this is Baalbek](http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/44.html). Find 10 differences.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/14390876674101.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/14390876674101.jpg)
-->

[![](http://1.bp.blogspot.com/-8zIDxfya3VI/VdLwnPggtjI/AAAAAAAAHq0/6Z13hKBPUjg/s1600/14390876674101.jpg#clickable)](http://1.bp.blogspot.com/-8zIDxfya3VI/VdLwnPggtjI/AAAAAAAAHq0/6Z13hKBPUjg/s1600/14390876674101.jpg)

Compare diameter, height, material, processing quality...

Peter was built according to all the canons of an ancient city, not a Christian city: columns, statues, palaces. The first church there appeared in the Peter and Paul Fortress, Peter and Paul Cathedral (1712-1733). No more churches were built on the territory of the city during the life of Peter the Great. But St Petersburg impressed with a huge number of ancient sculptures and buildings.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_1f133_545e5ac4_L.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_1f133_545e5ac4_L.jpg)
-->

[![](http://3.bp.blogspot.com/-bBFfVyV-KRI/UBYlf3H5hiI/AAAAAAAACBo/RNh7OHzEqPw/s1600/0_1f133_545e5ac4_L.jpg#clickable)](http://3.bp.blogspot.com/-bBFfVyV-KRI/UBYlf3H5hiI/AAAAAAAACBo/RNh7OHzEqPw/s1600/0_1f133_545e5ac4_L.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/30.JPG#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/30.JPG)
-->

[![](http://2.bp.blogspot.com/-wzzWxPNRRFM/UBYlhcGyYVI/AAAAAAAACBs/hxxgRsuoaYs/s1600/30.JPG#clickable)](http://2.bp.blogspot.com/-wzzWxPNRRFM/UBYlhcGyYVI/AAAAAAAACBs/hxxgRsuoaYs/s1600/30.JPG)

And here's Athens, 19th century.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1819-2.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1819-2.jpg)
-->

[![](http://1.bp.blogspot.com/-BHzgK18fQRU/URidZIhSSaI/AAAAAAAACMM/VR25PH6Kxjo/s1600/1819-2.jpg#clickable)](http://1.bp.blogspot.com/-BHzgK18fQRU/URidZIhSSaI/AAAAAAAACMM/VR25PH6Kxjo/s1600/1819-2.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/800px-Walhalla_innen.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/800px-Walhalla_innen.jpg)
-->

[![](http://3.bp.blogspot.com/-Yh8EZerdZYo/URidihUiNDI/AAAAAAAACMU/wSHbqakh_Ig/s1600/800px-Walhalla_innen.jpg#clickable)](http://3.bp.blogspot.com/-Yh8EZerdZYo/URidihUiNDI/AAAAAAAACMU/wSHbqakh_Ig/s1600/800px-Walhalla_innen.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Nike1850_80.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Nike1850_80.jpg)
-->

[![](http://4.bp.blogspot.com/-coZsnhzUuFQ/URidzGoDFSI/AAAAAAAACMc/oevXWmcRhT0/s1600/Nike1850_80.jpg#clickable)](http://4.bp.blogspot.com/-coZsnhzUuFQ/URidzGoDFSI/AAAAAAAACMc/oevXWmcRhT0/s1600/Nike1850_80.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/800px-Erechtheum1.JPG#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/800px-Erechtheum1.JPG)
-->

[![](http://3.bp.blogspot.com/-c_jE659BWvU/URieEg6kriI/AAAAAAAACMk/7i4m77skpA4/s1600/800px-Erechtheum1.JPG#clickable)](http://3.bp.blogspot.com/-c_jE659BWvU/URieEg6kriI/AAAAAAAACMk/7i4m77skpA4/s1600/800px-Erechtheum1.JPG)

Does this remind you of anything?

One thought gnaws at me... modern technology still can't match ancient stone-working technologies. Some historians have been very hesitant about our civilisation's involvement in the construction of the pyramids, Baalbek, Pantheon and Parthenon, I'm not talking about the Great Wall of China (it was probably just a high-speed highway between west and east). The states that appeared there simply appropriated the merits of ancient builders, finishing the construction in their own way, or recognized the monuments of ancestors. Maybe Pyotr (Peter) was also informed of the discovery of the city or its ruins. It was not for nothing that he destroyed so violently all the old records and sources related to the emergence of the city. How many builders of St. Petersburg have remained in the foundation of its structures. How many underground passages have not yet been found, how many are still covered with mystery? 

In order to rewrite history for yourself, you must first destroy all the sources and records of the previous history (under Peter almost all church chronicles were forcibly taken away in order to copy them and replicate them, which have never appeared anywhere else). And most importantly: the annals were confused. Which was successfully done with it. Peter ordered that instead of counting January 1, 7208. "from the creation of the world", to instead count January 1, 1700. "from the Nativity of the Lord God and the salvation of our Jesus Christ". The civil New Year's Eve was also moved to 1 January. The year 1699 was the shortest for Russia: from September to December, i.e. 4 months. However, without wishing to conflict with the adherents of the old days and the church, the Tsar made a reservation in the decree: "And whoever wants to write both those summers, from the creation of the world and from the Nativity of Christ, will be free to do so", which led to even greater confusion with the dates.

Cities arise somehow by themselves: many people will like a famous place at once, they gather together voluntarily and willingly, build houses, take up handicrafts and activities and start living and living. What the official story says: "What happened to St. Petersburg was different: it was created, inhabited and built by a powerful hand of Peter the Great; almost one of his personal will, against the will of thousands of people, brought together these disobedient thousands and forced them to build their own houses, walk the streets and live a new, unconventional life. The very construction of St. Petersburg and life in it turned to a kind of state duty under Peter (even after him). The answer to this question is simple and clear: in order to become great, you must take the former greatness and merits of your predecessors and destroy the memory of them.

Here's a very interesting post [about cities](http://andrash878.blogspot.com/).

Let's look again at the map of the "city of Petra" 

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pitermap_m.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pitermap_m.jpg)
-->

[![](http://2.bp.blogspot.com/-F-dflq-Ij5k/URiWKAcJutI/AAAAAAAACLo/Qt5MfXL7F-o/s1600/pitermap_m.jpg#clickable)](http://2.bp.blogspot.com/-F-dflq-Ij5k/URiWKAcJutI/AAAAAAAACLo/Qt5MfXL7F-o/s1600/pitermap_m.jpg)

And here's a map of the ancient city of Athens. Don't you find the layout familiar?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/meyers_konversationslexikon_1890_athens_2953_2362_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/meyers_konversationslexikon_1890_athens_2953_2362_600.jpg)
-->

[![](http://1.bp.blogspot.com/-EkDwSq8yCjc/URiWmzn1vOI/AAAAAAAACLw/s96PCXx0vR0/s1600/meyers_konversationslexikon_1890_athens_2953_2362_600.jpg#clickable)](http://1.bp.blogspot.com/-EkDwSq8yCjc/URiWmzn1vOI/AAAAAAAACLw/s96PCXx0vR0/s1600/meyers_konversationslexikon_1890_athens_2953_2362_600.jpg)

And history says that Athens was still 1,000 years BC, or are they lying? There is a strange feeling of understatement. Either Peter's youth is sucked from the thumb, or the age of ancient Greece is too high. Here's another coincidence for you, the same Greece ... 

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_005.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_005.jpg)
-->

[![](http://2.bp.blogspot.com/-B5JrF1oh4nQ/UVMQuqgChAI/AAAAAAAACVE/sQCmGCQeCKQ/s1600/%D0%93%D1%80%D0%B5%D1%86%D0%B8%D1%8F.jpg#clickable)](http://2.bp.blogspot.com/-B5JrF1oh4nQ/UVMQuqgChAI/AAAAAAAACVE/sQCmGCQeCKQ/s1600/%D0%93%D1%80%D0%B5%D1%86%D0%B8%D1%8F.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_007.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_007.jpg)
-->

[![](http://1.bp.blogspot.com/--xJ9NZiDW5s/UoSIDInwRCI/AAAAAAAAFU4/pcE-_dbTv0w/s1600/%D0%9A%D0%B0.jpg#clickable)](http://1.bp.blogspot.com/--xJ9NZiDW5s/UoSIDInwRCI/AAAAAAAAFU4/pcE-_dbTv0w/s1600/%D0%9A%D0%B0.jpg)

And this is Peterhof

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/11111.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/11111.jpg)
-->

[![](http://3.bp.blogspot.com/-ZUb3p49i1YQ/UaxLgpAuiCI/AAAAAAAADRM/pZ92kCCadj0/s1600/11111.jpg#clickable)](http://3.bp.blogspot.com/-ZUb3p49i1YQ/UaxLgpAuiCI/AAAAAAAADRM/pZ92kCCadj0/s1600/11111.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_6be63_f19bb35c_L.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_6be63_f19bb35c_L.jpg)
-->

[![](http://3.bp.blogspot.com/-AkjAnFDpPUE/UoSHNUCybqI/AAAAAAAAFUs/K9Knpyh1K40/s1600/0_6be63_f19bb35c_L.jpg#clickable)](http://3.bp.blogspot.com/-AkjAnFDpPUE/UoSHNUCybqI/AAAAAAAAFUs/K9Knpyh1K40/s1600/0_6be63_f19bb35c_L.jpg)

Can you find ten differences?

It's not easy with fortifications either. An incredible number of ideally shaped granite blocks have been used for the construction of the Kronstadt Forts and the cladding of the embankments. About ten forts built from such blocks can be counted in the Gulf of Finland. They are faced with cut granite in weights up to 2 tons. And the blocks are stacked without mortar. For such stacking, it is necessary to produce them with tolerances of no more than 0.5 mm (the same is used for ceramic tiles). The same tolerances apply to the perpendicularity of faces. The blocks in the photographs resemble a standard (conveyor) product. Making them by hand, although possible, is both expensive and time-consuming (somewhere in the world, a small, well-equipped workshop could produce one block per month). It is necessary to grind the planes precisely and for a long time, in order to obtain smooth, even edges with no chipping. With repeated accuracy, and en mass, such production is hardly feasible. I do not remember that the textbooks of the Peter the Great era describe a gapless and solvent-free masonry. I am not talking about structures tens of meters high with such masonry. We are talking about excessive accuracy and thoroughness in the manufacture of defensive structures. It does not favour hand-made masonry. At the time, it wasn't worth it, and there wasn't even any money. But such precision is obtained automatically in machine, conveyor and mass production, and is not surprising. So everything stood in the flow, and the complex processing of granite blocks was given to our ancestors very easily, look:

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/30806_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/30806_original.jpg)
-->

[![](http://2.bp.blogspot.com/-FHdVJW4umiM/UbA5rvll5oI/AAAAAAAADSU/GkUbJ5IVdVE/s1600/30806_original.jpg#clickable)](http://2.bp.blogspot.com/-FHdVJW4umiM/UbA5rvll5oI/AAAAAAAADSU/GkUbJ5IVdVE/s1600/30806_original.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/31060_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/31060_original.jpg)
-->

[![](http://3.bp.blogspot.com/-YSVUDNoeNJQ/UbA5qYTWnJI/AAAAAAAADSM/x31HWOcnTNU/s1600/31060_original.jpg#clickable)](http://3.bp.blogspot.com/-YSVUDNoeNJQ/UbA5qYTWnJI/AAAAAAAADSM/x31HWOcnTNU/s1600/31060_original.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/HGLAg78XAYg.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/HGLAg78XAYg.jpg)
-->

[![](http://1.bp.blogspot.com/-HUebw6t4src/UkvdxWA6SJI/AAAAAAAAEoo/i9Z2bWh_hq8/s1600/HGLAg78XAYg.jpg#clickable)](http://1.bp.blogspot.com/-HUebw6t4src/UkvdxWA6SJI/AAAAAAAAEoo/i9Z2bWh_hq8/s1600/HGLAg78XAYg.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ou9sxZ4sLKY.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ou9sxZ4sLKY.jpg)
-->

[![](http://1.bp.blogspot.com/-ecCo99ifO3c/UkvdwraZNnI/AAAAAAAAEog/A7EUeohTKkI/s1600/ou9sxZ4sLKY.jpg#clickable)](http://1.bp.blogspot.com/-ecCo99ifO3c/UkvdwraZNnI/AAAAAAAAEog/A7EUeohTKkI/s1600/ou9sxZ4sLKY.jpg)

In the 18th episode of the film "Gorunov", a wall with an entertaining masonry comes into the frame, if someone knows where it is, write.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72.jpg)
-->

[![](http://4.bp.blogspot.com/-LLSZbCGAQDs/UrqEu09_u9I/AAAAAAAAGJ8/WRZvtONg_20/s1600/72.jpg#clickable)](http://4.bp.blogspot.com/-LLSZbCGAQDs/UrqEu09_u9I/AAAAAAAAGJ8/WRZvtONg_20/s1600/72.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/73.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/73.jpg)
-->

[![](http://4.bp.blogspot.com/-kCYIeV-jdUA/UrqEuQ10PcI/AAAAAAAAGJ0/C5jr48ystDg/s1600/73.jpg#clickable)](http://4.bp.blogspot.com/-kCYIeV-jdUA/UrqEuQ10PcI/AAAAAAAAGJ0/C5jr48ystDg/s1600/73.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_006.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_006.jpg)
-->

[![](http://2.bp.blogspot.com/-I1rqN667st8/UrqEuuLVFmI/AAAAAAAAGJ4/GsHl9F-HNJ8/s1600/%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.jpg#clickable)](http://2.bp.blogspot.com/-I1rqN667st8/UrqEuuLVFmI/AAAAAAAAGJ4/GsHl9F-HNJ8/s1600/%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.jpg)

Here are more wall fragments to the left and right of the gate.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/UEc569zNwKc.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/UEc569zNwKc.jpg)
-->

[![](http://1.bp.blogspot.com/-Af1p90Y_oSw/UlTsFkMlAHI/AAAAAAAAExM/eMrCYgEkOPE/s1600/UEc569zNwKc.jpg#clickable)](http://1.bp.blogspot.com/-Af1p90Y_oSw/UlTsFkMlAHI/AAAAAAAAExM/eMrCYgEkOPE/s1600/UEc569zNwKc.jpg)


[Live link with location reference.](https://maps.yandex.ru/2/saint-petersburg/?z=18&ll=30.279153%2C59.915652&l=map%2Csat%2Cskl&origin=jsapi_2_1_29&text=%D0%B3%D0%BE%D1%81%D0%BF%D0%B8%D1%82%D0%B0%D0%BB%D1%8C%20%D0%BC%D0%BE%D1%80%D1%81%D0%BA%D0%BE%D0%B9&sll=30.311736%2C59.918518&sspn=0.079222%2C0.029649&ol=psearch&oid=3289850&panorama%5Bpoint%5D=29.761600%2C60.001452&panorama%5Bdirection%5D=132.094476%2C0.208272&panorama%5Bspan%5D=130.000000%2C63.985009)

For comparison, let's look at the masonry of the Temple of Apollo in Delphi (Greece)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/foto-109.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/foto-109.jpg)
-->

[![](http://1.bp.blogspot.com/-PdfhTL5ACNA/Uk0BYN3oOuI/AAAAAAAAEpU/OlwGr7nyYug/s1600/foto-109.jpg#clickable)](http://1.bp.blogspot.com/-PdfhTL5ACNA/Uk0BYN3oOuI/AAAAAAAAEpU/OlwGr7nyYug/s1600/foto-109.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17993.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17993.jpg)
-->

[![](http://1.bp.blogspot.com/-U9xgXHDcRbw/Uk0B_iS1GZI/AAAAAAAAEpc/-0ddFwm9VGs/s1600/17993.jpg#clickable)](http://1.bp.blogspot.com/-U9xgXHDcRbw/Uk0B_iS1GZI/AAAAAAAAEpc/-0ddFwm9VGs/s1600/17993.jpg)

These stones, style and method of masonry do not resemble anything? The material is different, you say, I completely agree with you, but the way the stones are processed, the masonry technology itself - they are identical. Only the blind will not see this. 

I can't get rid of the thought: either during the construction of the docks, ready-made material was used, brought from another place (there the finished structure was dismantled and then re-assembled here), or to the craftsmen of that time, working with granite seemed like child's play. As they wanted it, they cut it. About the weight of the blocks I am silent; more on that later.

And these are the photos I unearthed looking at the Best Bridge.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17987_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17987_1000.jpg)
-->

[![](http://2.bp.blogspot.com/-I1VHZ0Dowtw/Vd1lO5RWqCI/AAAAAAAAHrg/sM8-z26yXss/s1600/17987_1000.jpg#clickable)](http://2.bp.blogspot.com/-I1VHZ0Dowtw/Vd1lO5RWqCI/AAAAAAAAHrg/sM8-z26yXss/s1600/17987_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19149_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19149_1000.jpg)
-->

[![](http://1.bp.blogspot.com/-Qetwt4t0Fc0/Vd1lQZ_Jp7I/AAAAAAAAHr4/kQA2TPjOPPc/s1600/19149_1000.jpg#clickable)](http://1.bp.blogspot.com/-Qetwt4t0Fc0/Vd1lQZ_Jp7I/AAAAAAAAHr4/kQA2TPjOPPc/s1600/19149_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/22137_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/22137_1000.jpg)
-->

[![](http://1.bp.blogspot.com/-mfvlV9KRTV4/Vd1lSwqwRqI/AAAAAAAAHsk/yZvukuelw8M/s1600/22137_1000.jpg#clickable)](http://1.bp.blogspot.com/-mfvlV9KRTV4/Vd1lSwqwRqI/AAAAAAAAHsk/yZvukuelw8M/s1600/22137_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18223_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18223_1000.jpg)
-->

[![](http://1.bp.blogspot.com/-lBF1XchqyMM/Vd1lPs-Ua-I/AAAAAAAAHro/64XdTbAzP_Q/s1600/18444_1000.jpg#clickable)](http://1.bp.blogspot.com/-lBF1XchqyMM/Vd1lPs-Ua-I/AAAAAAAAHro/64XdTbAzP_Q/s1600/18444_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18444_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18444_1000.jpg)
-->

[![](http://3.bp.blogspot.com/-LPRSZWHxMEo/Vd1lO4oUeHI/AAAAAAAAHrc/8DiyGGSEkbw/s1600/18223_1000.jpg#clickable)](http://3.bp.blogspot.com/-LPRSZWHxMEo/Vd1lO4oUeHI/AAAAAAAAHrc/8DiyGGSEkbw/s1600/18223_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18777_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/18777_1000.jpg)
-->

[![](http://4.bp.blogspot.com/-LQsyFG9PA2s/Vd1lP1nfT9I/AAAAAAAAHrw/aLVpu1MMmNU/s1600/18777_1000.jpg#clickable)](http://4.bp.blogspot.com/-LQsyFG9PA2s/Vd1lP1nfT9I/AAAAAAAAHrw/aLVpu1MMmNU/s1600/18777_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17838_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/17838_1000.jpg)
-->

[![](http://3.bp.blogspot.com/-vC5ocHAOiXw/Vd1lO6QljLI/AAAAAAAAHsQ/DYF9ux72kuc/s1600/17838_1000.jpg#clickable)](http://3.bp.blogspot.com/-vC5ocHAOiXw/Vd1lO6QljLI/AAAAAAAAHsQ/DYF9ux72kuc/s1600/17838_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19431_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19431_1000.jpg)
-->

[![](http://3.bp.blogspot.com/-zJj_RmTiGN0/Vd1lQjYzmqI/AAAAAAAAHsA/9jurvdVUZXM/s1600/19431_1000.jpg#clickable)](http://3.bp.blogspot.com/-zJj_RmTiGN0/Vd1lQjYzmqI/AAAAAAAAHsA/9jurvdVUZXM/s1600/19431_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19831_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/19831_1000.jpg)
-->

[![](http://2.bp.blogspot.com/-B8mAOihdtAM/Vd1lRfHRMhI/AAAAAAAAHsM/f6GQ8SUbofI/s1600/19831_1000.jpg#clickable)](http://2.bp.blogspot.com/-B8mAOihdtAM/Vd1lRfHRMhI/AAAAAAAAHsM/f6GQ8SUbofI/s1600/19831_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/20829_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/20829_1000.jpg)
-->

[![](http://2.bp.blogspot.com/-FL0tjInYrD8/Vd1lRo_L8_I/AAAAAAAAHss/3JmnWUXvJ18/s1600/20829_1000.jpg#clickable)](http://2.bp.blogspot.com/-FL0tjInYrD8/Vd1lRo_L8_I/AAAAAAAAHss/3JmnWUXvJ18/s1600/20829_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/21027_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/21027_1000.jpg)
-->

[![](http://2.bp.blogspot.com/-epo1kDF8AOI/Vd1lSPIcg0I/AAAAAAAAHsU/lgHzkALiCO0/s1600/21027_1000.jpg#clickable)](http://2.bp.blogspot.com/-epo1kDF8AOI/Vd1lSPIcg0I/AAAAAAAAHsU/lgHzkALiCO0/s1600/21027_1000.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/21304_1000.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/21304_1000.jpg)
-->

[![](http://3.bp.blogspot.com/--STwEwzeTFY/Vd1lSbzHoSI/AAAAAAAAHsc/XCPDUMr55FM/s1600/21304_1000.jpg#clickable)](http://3.bp.blogspot.com/--STwEwzeTFY/Vd1lSbzHoSI/AAAAAAAAHsc/XCPDUMr55FM/s1600/21304_1000.jpg)

Perhaps these are the remains of old forts and bastions, or perhaps antiquity is not as old as historians are trying to prove to us.

Photos from here: [http://vaduhan-08.livejournal.com/65543.html](http://vaduhan-08.livejournal.com/65543.html)

The wall of the Kronstadt dock pool. More than [polygonal masonry](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_17.html)?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd31.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd31.jpg)
-->

[![](http://2.bp.blogspot.com/-yLbd0tLPn-w/Ubbt3tkBTzI/AAAAAAAADck/IRTgMZri9sU/s1600/pd31.jpg#clickable)](http://2.bp.blogspot.com/-yLbd0tLPn-w/Ubbt3tkBTzI/AAAAAAAADck/IRTgMZri9sU/s1600/pd31.jpg)

Stairs in the western dock. The material is pink granite. You can't carve this on a machine. If labour is manual, why is it so labour-intensive? To take and cut steps from rectangular blocks is much easier. But for the masters of the time, such work did not seem difficult, so they cut it the way they wanted. 

Polygonal brickwork in the Vyborg Bay

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/8531647.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/8531647.jpg)
-->

[![](http://4.bp.blogspot.com/-8oGUSHC7Fxw/VBgCiKjNLGI/AAAAAAAAHhQ/K19jImPSV9E/s1600/8531647.jpg#clickable)](http://4.bp.blogspot.com/-8oGUSHC7Fxw/VBgCiKjNLGI/AAAAAAAAHhQ/K19jImPSV9E/s1600/8531647.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/lihaniemi_060325_7-04.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/lihaniemi_060325_7-04.jpg)
-->

[![](http://4.bp.blogspot.com/-zWHqt06b3_w/VBgCjfQjmaI/AAAAAAAAHhY/yNl1LsP9fW4/s1600/lihaniemi_060325_7-04.jpg#clickable)](http://4.bp.blogspot.com/-zWHqt06b3_w/VBgCjfQjmaI/AAAAAAAAHhY/yNl1LsP9fW4/s1600/lihaniemi_060325_7-04.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-06.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-06.jpg)
-->

[![](http://2.bp.blogspot.com/-LwvAg7XrtQw/VBgCjiIbP0I/AAAAAAAAHhc/n1NCd8Sn8qo/s1600/suurmer_050528-06.jpg#clickable)](http://2.bp.blogspot.com/-LwvAg7XrtQw/VBgCjiIbP0I/AAAAAAAAHhc/n1NCd8Sn8qo/s1600/suurmer_050528-06.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-11.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-11.jpg)
-->

[![](http://3.bp.blogspot.com/-qeFwkltY7jw/VBgCj5aNY9I/AAAAAAAAHhg/2Tkpy53OrPk/s1600/suurmer_050528-11.jpg#clickable)](http://3.bp.blogspot.com/-qeFwkltY7jw/VBgCj5aNY9I/AAAAAAAAHhg/2Tkpy53OrPk/s1600/suurmer_050528-11.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-12.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-12.jpg)
-->

[![](http://3.bp.blogspot.com/-kDuPhBLt9Zg/VBgClbFGe5I/AAAAAAAAHhw/qabc01-ruj4/s1600/suurmer_050528-12.jpg#clickable)](http://3.bp.blogspot.com/-kDuPhBLt9Zg/VBgClbFGe5I/AAAAAAAAHhw/qabc01-ruj4/s1600/suurmer_050528-12.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-22.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/suurmer_050528-22.jpg)
-->

[![](http://3.bp.blogspot.com/-o5V_MIfPayc/VBgCl951yWI/AAAAAAAAHh0/15tTPbb61Gg/s1600/suurmer_050528-22.jpg#clickable)](http://3.bp.blogspot.com/-o5V_MIfPayc/VBgCl951yWI/AAAAAAAAHh0/15tTPbb61Gg/s1600/suurmer_050528-22.jpg)

[Source photo:](http://prong777.livejournal.com/489529.html)

Wonderful tour of the forts [here](http://violet3333.livejournal.com/)

[Granite frame. 4th Northern Fort ("Beast").](http://violet3333.livejournal.com/5209.html)

[Granite frame. "Fort Emperor Alexander I" (Plague)](http://violet3333.livejournal.com/5489.html)

[Granite frame. Fort "Grand Duke Constantine". Kronstadt.](http://violet3333.livejournal.com/4844.html)

Granite megaliths of the Northern Fort

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/60714_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/60714_600.jpg)
-->

[![](http://2.bp.blogspot.com/-FMCP2bfsgP4/U1eCunKM1_I/AAAAAAAAGvA/eDGnx_31kfY/s1600/60714_600.jpg#clickable)](http://2.bp.blogspot.com/-FMCP2bfsgP4/U1eCunKM1_I/AAAAAAAAGvA/eDGnx_31kfY/s1600/60714_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61103_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61103_600.jpg)
-->

[![](http://1.bp.blogspot.com/-8M-QIwvBdfQ/U1eC0vHWf1I/AAAAAAAAGwg/xRMoFNqu_kc/s1600/61103_600.jpg#clickable)](http://1.bp.blogspot.com/-8M-QIwvBdfQ/U1eC0vHWf1I/AAAAAAAAGwg/xRMoFNqu_kc/s1600/61103_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61413_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61413_600.jpg)
-->

[![](http://4.bp.blogspot.com/-OudzDy5ES1k/U1eCuiDSMFI/AAAAAAAAGvE/V_bGSiKaxu4/s1600/61413_600.jpg#clickable)](http://4.bp.blogspot.com/-OudzDy5ES1k/U1eCuiDSMFI/AAAAAAAAGvE/V_bGSiKaxu4/s1600/61413_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62241_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62241_600.jpg)
-->

[![](http://2.bp.blogspot.com/-8M3l6NsXIUk/U1eCvQeA7FI/AAAAAAAAGvU/69BYc4EvbaU/s1600/62241_600.jpg#clickable)](http://2.bp.blogspot.com/-8M3l6NsXIUk/U1eCvQeA7FI/AAAAAAAAGvU/69BYc4EvbaU/s1600/62241_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62477_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62477_600.jpg)
-->

[![](http://2.bp.blogspot.com/-fmHyMR0Bdgg/U1eCv6JnTSI/AAAAAAAAGvQ/LKnOTyTtdi8/s1600/62477_600.jpg#clickable)](http://2.bp.blogspot.com/-fmHyMR0Bdgg/U1eCv6JnTSI/AAAAAAAAGvQ/LKnOTyTtdi8/s1600/62477_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62803_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/62803_600.jpg)
-->

[![](http://2.bp.blogspot.com/-YiILAnFD_-w/U1eCxAZKvsI/AAAAAAAAGvc/57X8U2Q7EFA/s1600/62803_600.jpg#clickable)](http://2.bp.blogspot.com/-YiILAnFD_-w/U1eCxAZKvsI/AAAAAAAAGvc/57X8U2Q7EFA/s1600/62803_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/63469_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/63469_600.jpg)
-->

[![](http://1.bp.blogspot.com/-KI07EMv9GRI/U1eCxmIm4DI/AAAAAAAAGvk/lFTAO2VAzto/s1600/63469_600.jpg#clickable)](http://1.bp.blogspot.com/-KI07EMv9GRI/U1eCxmIm4DI/AAAAAAAAGvk/lFTAO2VAzto/s1600/63469_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/65738_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/65738_600.jpg)
-->

[![](http://4.bp.blogspot.com/-EZlOV6RcpCc/U1eCyD0JA6I/AAAAAAAAGv0/nxbut7Fd0Ag/s1600/65738_600.jpg#clickable)](http://4.bp.blogspot.com/-EZlOV6RcpCc/U1eCyD0JA6I/AAAAAAAAGv0/nxbut7Fd0Ag/s1600/65738_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/66957_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/66957_600.jpg)
-->

[![](http://1.bp.blogspot.com/-rRSGQ-FyQfY/U1eCyWJ86fI/AAAAAAAAGvw/mT5YPzag3AQ/s1600/66957_600.jpg#clickable)](http://1.bp.blogspot.com/-rRSGQ-FyQfY/U1eCyWJ86fI/AAAAAAAAGvw/mT5YPzag3AQ/s1600/66957_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/67661_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/67661_600.jpg)
-->

[![](http://3.bp.blogspot.com/-ZgLlCpeY0eI/U1eCzP3WMdI/AAAAAAAAGv8/JRD0joXJrZ4/s1600/67661_600.jpg#clickable)](http://3.bp.blogspot.com/-ZgLlCpeY0eI/U1eCzP3WMdI/AAAAAAAAGv8/JRD0joXJrZ4/s1600/67661_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68126_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68126_600.jpg)
-->

[![](http://4.bp.blogspot.com/-fwn6MKuyUdU/U1eCzSoGiDI/AAAAAAAAGwI/LNzp9-fYUwg/s1600/68126_600.jpg#clickable)](http://4.bp.blogspot.com/-fwn6MKuyUdU/U1eCzSoGiDI/AAAAAAAAGwI/LNzp9-fYUwg/s1600/68126_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68432_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68432_600.jpg)
-->

[![](http://2.bp.blogspot.com/-2edjBKJ9QvI/U1eC0ClfXfI/AAAAAAAAGwM/1zg7JymDIHo/s1600/68432_600.jpg#clickable)](http://2.bp.blogspot.com/-2edjBKJ9QvI/U1eC0ClfXfI/AAAAAAAAGwM/1zg7JymDIHo/s1600/68432_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68687_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/68687_600.jpg)
-->

[![](http://3.bp.blogspot.com/-nxP0MOoMYSA/U1eC0jFypTI/AAAAAAAAGwY/1G2P7utegzk/s1600/68687_600.jpg#clickable)](http://3.bp.blogspot.com/-nxP0MOoMYSA/U1eC0jFypTI/AAAAAAAAGwY/1G2P7utegzk/s1600/68687_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69007_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69007_600.jpg)
-->

[![](http://3.bp.blogspot.com/-b4w2ZCOE8_I/U1eC1HVqgdI/AAAAAAAAGwc/KY9vW3s1fKk/s1600/69007_600.jpg#clickable)](http://3.bp.blogspot.com/-b4w2ZCOE8_I/U1eC1HVqgdI/AAAAAAAAGwc/KY9vW3s1fKk/s1600/69007_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69160_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69160_600.jpg)
-->

[![](http://4.bp.blogspot.com/-yIg9-oeGFP0/U1eC4bqRgMI/AAAAAAAAGxc/FStnc6Jtx0s/s1600/69160_600.jpg#clickable)](http://4.bp.blogspot.com/-yIg9-oeGFP0/U1eC4bqRgMI/AAAAAAAAGxc/FStnc6Jtx0s/s1600/69160_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69533_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/69533_600.jpg)
-->

[![](http://4.bp.blogspot.com/-yMiprHYmkNc/U1eC2KWtu-I/AAAAAAAAGw0/ghN7n-NS29Q/s1600/69533_600.jpg#clickable)](http://4.bp.blogspot.com/-yMiprHYmkNc/U1eC2KWtu-I/AAAAAAAAGw0/ghN7n-NS29Q/s1600/69533_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/70604_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/70604_600.jpg)
-->

[![](http://4.bp.blogspot.com/-zAZGQ16J0tI/U1eC2fOTEsI/AAAAAAAAGww/J2atBUEtI00/s1600/70604_600.jpg#clickable)](http://4.bp.blogspot.com/-zAZGQ16J0tI/U1eC2fOTEsI/AAAAAAAAGww/J2atBUEtI00/s1600/70604_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/70768_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/70768_600.jpg)
-->

[![](http://4.bp.blogspot.com/-CKGtTZ-3exE/U1eC3PaLumI/AAAAAAAAGxA/PFOD2j8b7Eg/s1600/70768_600.jpg#clickable)](http://4.bp.blogspot.com/-CKGtTZ-3exE/U1eC3PaLumI/AAAAAAAAGxA/PFOD2j8b7Eg/s1600/70768_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/71393_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/71393_600.jpg)
-->

[![](http://1.bp.blogspot.com/-7pw88KBOC88/U1eC3TKRlHI/AAAAAAAAGxE/Cu3Lgb_NJAM/s1600/71393_600.jpg#clickable)](http://1.bp.blogspot.com/-7pw88KBOC88/U1eC3TKRlHI/AAAAAAAAGxE/Cu3Lgb_NJAM/s1600/71393_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/71428_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/71428_600.jpg)
-->

[![](http://2.bp.blogspot.com/--G18nQ7a9Pg/U1eC4E2v9fI/AAAAAAAAGxM/G2lrbQ8Zuks/s1600/71428_600.jpg#clickable)](http://2.bp.blogspot.com/--G18nQ7a9Pg/U1eC4E2v9fI/AAAAAAAAGxM/G2lrbQ8Zuks/s1600/71428_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72466_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72466_600.jpg)
-->

[![](http://3.bp.blogspot.com/-0lrozwAycck/U1eC4dNdxhI/AAAAAAAAGxU/er7acXP6f4k/s1600/72466_600.jpg#clickable)](http://3.bp.blogspot.com/-0lrozwAycck/U1eC4dNdxhI/AAAAAAAAGxU/er7acXP6f4k/s1600/72466_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72805_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72805_600.jpg)
-->

[![](http://4.bp.blogspot.com/-mMm3E8NYWz0/U1eC6Azo9GI/AAAAAAAAGx0/yxwB9enLaPE/s1600/72805_600.jpg#clickable)](http://4.bp.blogspot.com/-mMm3E8NYWz0/U1eC6Azo9GI/AAAAAAAAGx0/yxwB9enLaPE/s1600/72805_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72982_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/72982_600.jpg)
-->

[![](http://4.bp.blogspot.com/-VC-aBbk9sB8/U1eC5QfILhI/AAAAAAAAGxk/NhZko9VbUF0/s1600/72982_600.jpg#clickable)](http://4.bp.blogspot.com/-VC-aBbk9sB8/U1eC5QfILhI/AAAAAAAAGxk/NhZko9VbUF0/s1600/72982_600.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/73363_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/73363_600.jpg)
-->

[![](http://1.bp.blogspot.com/-EbFlNMt9Llg/U1eC54_fJII/AAAAAAAAGxs/wAizOcrpVfE/s1600/73363_600.jpg#clickable)](http://1.bp.blogspot.com/-EbFlNMt9Llg/U1eC54_fJII/AAAAAAAAGxs/wAizOcrpVfE/s1600/73363_600.jpg)

For comparison, a modern (20th century) concrete building, the same northern fort

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/66464_600.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/66464_600.jpg)
-->

[![](http://3.bp.blogspot.com/-5vVJdVvIfdQ/U1eDeJK-_aI/AAAAAAAAGyE/dDu0PySc5Qg/s1600/66464_600.jpg#clickable)](http://3.bp.blogspot.com/-5vVJdVvIfdQ/U1eDeJK-_aI/AAAAAAAAGyE/dDu0PySc5Qg/s1600/66464_600.jpg)

Fort Alexander I and Constantine

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/11072011_2.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/11072011_2.jpg)
-->

[![](http://4.bp.blogspot.com/-eSA5YVlGlVQ/UbBAsev1pnI/AAAAAAAADSw/GJB9nYD6_mA/s1600/11072011_2.jpg#clickable)](http://4.bp.blogspot.com/-eSA5YVlGlVQ/UbBAsev1pnI/AAAAAAAADSw/GJB9nYD6_mA/s1600/11072011_2.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/7407.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/7407.jpg)
-->

[![](http://3.bp.blogspot.com/-kL_EUT4Cwy8/UbBArPH0g2I/AAAAAAAADSk/SeWRr91SsuU/s1600/7407.jpg#clickable)](http://3.bp.blogspot.com/-kL_EUT4Cwy8/UbBArPH0g2I/AAAAAAAADSk/SeWRr91SsuU/s1600/7407.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10086_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10086_original.jpg)
-->

[![](http://3.bp.blogspot.com/-P5ByXTJ_x3I/UbBA4iRBz6I/AAAAAAAADS8/E1y4rrjTCwA/s1600/10086_original.jpg#clickable)](http://3.bp.blogspot.com/-P5ByXTJ_x3I/UbBA4iRBz6I/AAAAAAAADS8/E1y4rrjTCwA/s1600/10086_original.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/9d45270b019a.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/9d45270b019a.jpg)
-->

[![](http://1.bp.blogspot.com/-yS7MCaro4Z0/UdKTPTiE8BI/AAAAAAAADgQ/4Ua3wlwHDVU/s640/9d45270b019a.jpg#clickable)](http://1.bp.blogspot.com/-yS7MCaro4Z0/UdKTPTiE8BI/AAAAAAAADgQ/4Ua3wlwHDVU/s640/9d45270b019a.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/point1241_Kronstadt_channel.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/point1241_Kronstadt_channel.jpg)
-->

[![](http://3.bp.blogspot.com/-vkE7VEmVHBg/UbBAsAhxA7I/AAAAAAAADSs/BW3Rctuysq4/s1600/point1241_Kronstadt_channel.jpg#clickable)](http://3.bp.blogspot.com/-vkE7VEmVHBg/UbBAsAhxA7I/AAAAAAAADSs/BW3Rctuysq4/s1600/point1241_Kronstadt_channel.jpg)

This is Kronstadt (forts), but the masonry technology itself surprises. It looks like [Machu Picchu.](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_4946.html)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/082.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/082.jpg)
-->

[![](http://4.bp.blogspot.com/-S0FY-qsDAjU/UbBBv_8JMHI/AAAAAAAADTQ/B1wAP2EHiAk/s1600/082.jpg#clickable)](http://4.bp.blogspot.com/-S0FY-qsDAjU/UbBBv_8JMHI/AAAAAAAADTQ/B1wAP2EHiAk/s1600/082.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61109001_hram_solnca_inkov.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/61109001_hram_solnca_inkov.jpg)
-->

[![](http://3.bp.blogspot.com/-8g3UVM8Z8sY/UbBK95dcIBI/AAAAAAAADUM/kkW-8HALcCM/s1600/61109001_hram_solnca_inkov.jpg#clickable)](http://3.bp.blogspot.com/-8g3UVM8Z8sY/UbBK95dcIBI/AAAAAAAADUM/kkW-8HALcCM/s1600/61109001_hram_solnca_inkov.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/78646613_Uvlekatelnoe_fotoputeshestvie_v_stranu_drevnih_inko.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/78646613_Uvlekatelnoe_fotoputeshestvie_v_stranu_drevnih_inko.jpg)
-->

[![](http://4.bp.blogspot.com/-d0YWAdESSs4/UbBK-EIPCsI/AAAAAAAADUU/trOaZNlBjuc/s1600/78646613_Uvlekatelnoe_fotoputeshestvie_v_stranu_drevnih_inkov_92.jpg#clickable)](http://4.bp.blogspot.com/-d0YWAdESSs4/UbBK-EIPCsI/AAAAAAAADUU/trOaZNlBjuc/s1600/78646613_Uvlekatelnoe_fotoputeshestvie_v_stranu_drevnih_inkov_92.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1-3.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1-3.jpg)
-->

[![](http://2.bp.blogspot.com/-a3RFJgnaqac/UbBBwOqwjWI/AAAAAAAADTI/3K-GoYwSasg/s1600/1-3.jpg#clickable)](http://2.bp.blogspot.com/-a3RFJgnaqac/UbBBwOqwjWI/AAAAAAAADTI/3K-GoYwSasg/s1600/1-3.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Machupicchu_intihuatana.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Machupicchu_intihuatana.jpg)
-->

[![](http://3.bp.blogspot.com/-aTwy3yrIs6s/UbBHJnnhyII/AAAAAAAADT0/NSU0Mm456Do/s1600/Machupicchu_intihuatana.jpg#clickable)](http://3.bp.blogspot.com/-aTwy3yrIs6s/UbBHJnnhyII/AAAAAAAADT0/NSU0Mm456Do/s1600/Machupicchu_intihuatana.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/3.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/3.jpg)
-->

[![](http://4.bp.blogspot.com/-TD9KkEN-kXg/UbBBw5QqKQI/AAAAAAAADTY/1WUopnCAwSs/s1600/3.jpg#clickable)](http://4.bp.blogspot.com/-TD9KkEN-kXg/UbBBw5QqKQI/AAAAAAAADTY/1WUopnCAwSs/s1600/3.jpg)

[Cuzco Region, Peru. (Machu Picchu. South America, Peru.)](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_4946.html)

Probably Peter the Great got the Incas and the Aztecs some time about a thousand years ago and made them build the Kronstadt. Or he learned the technology from them, built factories and schools, trained people, bought machines from the same Incas... and started production. When construction was finished, Enoch the Foreman said:

> "Bam, bam, bam, bam, bam, bam, bam, bam, bam, bam, bam, bam, bam!"

And everything evaporated: the machines, the factories, and most importantly the knowledge, the memory of the construction. And the structures have stood, in Russia - if we believe the historians - for two hundred years, in America, if we believe the same historians, a couple of thousand years at least. I wonder whose story is "more historic"?

> Hey, Vasya, did you learn history?

> What for? I'll come up with something more convoluted, I'll tell it more confidently, the teacher will give me an "A".

> What about the textbook?

> What about the textbook? The "old" was all worn out, and in the "new", nothing was said about what was in the "old".

A. Sklyarov wanders around the world in search of polygonal masonry and... ancient technologies. But everything is here on a silver platter:

> Take it!

> I don't want it!

The famous Petrovsky Dock. 

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/vg21.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/vg21.jpg)
-->

[![](http://3.bp.blogspot.com/-ejCo14UX0pg/UbBolvhOnxI/AAAAAAAADUk/EBND0rH6CqA/s1600/vg21.jpg#clickable)](http://3.bp.blogspot.com/-ejCo14UX0pg/UbBolvhOnxI/AAAAAAAADUk/EBND0rH6CqA/s1600/vg21.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/dok_bass_19.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/dok_bass_19.jpg)
-->

[![](http://3.bp.blogspot.com/-sIjWSDrrqPg/UbBopg0a7DI/AAAAAAAADUs/Do6dnA506Ss/s1600/dok_bass_19.jpg#clickable)](http://3.bp.blogspot.com/-sIjWSDrrqPg/UbBopg0a7DI/AAAAAAAADUs/Do6dnA506Ss/s1600/dok_bass_19.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd51.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd51.jpg)
-->

[![](http://4.bp.blogspot.com/-StfLu23MIwo/UbBoznzF7jI/AAAAAAAADU8/MEvpQLcDTws/s1600/pd51.jpg#clickable)](http://4.bp.blogspot.com/-StfLu23MIwo/UbBoznzF7jI/AAAAAAAADU8/MEvpQLcDTws/s1600/pd51.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd28.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/pd28.jpg)
-->

[![](http://1.bp.blogspot.com/-HhuoSQcPrdI/UbBowTg_qaI/AAAAAAAADU0/zQgK6Ds2s84/s1600/pd28.jpg#clickable)](http://1.bp.blogspot.com/-HhuoSQcPrdI/UbBowTg_qaI/AAAAAAAADU0/zQgK6Ds2s84/s1600/pd28.jpg)

One thing surprises: granite forts and embankments are present only in one area of the Baltic, namely in Kronstadt and St. Petersburg. The forts of Konigsberg (Kaliningrad) are brick, however, like all the others. So where did the knowledge go?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0001.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0001.jpg)
-->

[![](http://4.bp.blogspot.com/-6DcMyS5THxg/UbCcjk8v8II/AAAAAAAADVc/BNvNlsNkmTo/s1600/0001.jpg#clickable)](http://4.bp.blogspot.com/-6DcMyS5THxg/UbCcjk8v8II/AAAAAAAADVc/BNvNlsNkmTo/s1600/0001.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10995.jpg){width="640" height="406"}](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10995.jpg){width="640" height="406"}
-->

[![](http://1.bp.blogspot.com/-4Q1LNbS6rww/UbCckIHSgTI/AAAAAAAADVk/dTnRz8bTXMY/s1600/10995.jpg#clickable)](http://1.bp.blogspot.com/-4Q1LNbS6rww/UbCckIHSgTI/AAAAAAAADVk/dTnRz8bTXMY/s1600/10995.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1272461240_x_3c3724c4.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1272461240_x_3c3724c4.jpg)
-->

[![](http://4.bp.blogspot.com/-dK3YMPmTCmE/UbCciu6zOFI/AAAAAAAADVU/GvCLAvxdq80/s1600/1272461240_x_3c3724c4.jpg#clickable)](http://4.bp.blogspot.com/-dK3YMPmTCmE/UbCciu6zOFI/AAAAAAAADVU/GvCLAvxdq80/s1600/1272461240_x_3c3724c4.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/8d84514778a0.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/8d84514778a0.jpg)
-->

[![](http://1.bp.blogspot.com/-KoxYkQ1S5cg/UbCeRqG_svI/AAAAAAAADWA/yXTQVvJ27nA/s1600/8d84514778a0.jpg#clickable)](http://1.bp.blogspot.com/-KoxYkQ1S5cg/UbCeRqG_svI/AAAAAAAADWA/yXTQVvJ27nA/s1600/8d84514778a0.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/846191.jpeg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/846191.jpeg)
-->

[![](http://4.bp.blogspot.com/-n5GxUUfUsaM/UbCeRTPBAyI/AAAAAAAADV8/whcDVUjYBTA/s1600/84619+(1).jpeg#clickable)](http://4.bp.blogspot.com/-n5GxUUfUsaM/UbCeRTPBAyI/AAAAAAAADV8/whcDVUjYBTA/s1600/84619+(1).jpeg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/kronshlot5.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/kronshlot5.jpg)
-->

[![](http://2.bp.blogspot.com/-guhGrHBc1Vo/UbCeR7Le1bI/AAAAAAAADWI/UNFLprCa1gI/s1600/kronshlot5.jpg#clickable)](http://2.bp.blogspot.com/-guhGrHBc1Vo/UbCeR7Le1bI/AAAAAAAADWI/UNFLprCa1gI/s1600/kronshlot5.jpg)

A few words about the antiquity of the buildings, look at the map, the dates ... and the crosses on the spires.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/9_pQ-u69OCs.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/9_pQ-u69OCs.jpg)
-->

[![](http://4.bp.blogspot.com/-VsIPr2WDUIw/UkveKV_d4wI/AAAAAAAAEow/m7DfObMEJZ0/s1600/9_pQ-u69OCs.jpg#clickable)](http://4.bp.blogspot.com/-VsIPr2WDUIw/UkveKV_d4wI/AAAAAAAAEow/m7DfObMEJZ0/s1600/9_pQ-u69OCs.jpg)

... and the Kronstadt forts in the early 19th century.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/47lRN4Ad35A.jpg){width="640" height="466"}](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/47lRN4Ad35A.jpg){width="640" height="466"}
-->

[![](http://3.bp.blogspot.com/-rSUs0F8v0o0/UkvfXft52FI/AAAAAAAAEo8/n0ScekQ0bpE/s1600/47lRN4Ad35A.jpg#clickable)](http://3.bp.blogspot.com/-rSUs0F8v0o0/UkvfXft52FI/AAAAAAAAEo8/n0ScekQ0bpE/s1600/47lRN4Ad35A.jpg)

Either the artist lied, or such a construction, even in water... Well, it could not have been done without technical documentation, trained engineers, and most importantly, with a complete loss of memory of its construction.

For those who do not believe in the illogicality of the appearance of such structures in Peter's and after Peter's times, I offer only two photos:

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/96689_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/96689_original.jpg)
-->

[![](http://4.bp.blogspot.com/-yChD-KWHknM/VAq8jM5fO2I/AAAAAAAAHfo/SoKbhUfMAGk/s1600/96689_original.jpg#clickable)](http://4.bp.blogspot.com/-yChD-KWHknM/VAq8jM5fO2I/AAAAAAAAHfo/SoKbhUfMAGk/s1600/96689_original.jpg)

Granite facing of the channel in St. Petersburg.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/96863_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/96863_original.jpg)
-->

[![](http://3.bp.blogspot.com/-NmX59UPr1rM/VAq8jF8nstI/AAAAAAAAHfs/iOI7496PP54/s1600/96863_original.jpg#clickable)](http://3.bp.blogspot.com/-NmX59UPr1rM/VAq8jF8nstI/AAAAAAAAHfs/iOI7496PP54/s1600/96863_original.jpg)

Enlarged photo

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ZamokEdo0-0043.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ZamokEdo0-0043.jpg)
-->

[![](http://4.bp.blogspot.com/-1586RR0LuRQ/VAq8hpgj_YI/AAAAAAAAHfg/jvAL5_nAlU0/s1600/Zamok%2BEdo%2B0-0043.jpg#clickable)](http://4.bp.blogspot.com/-1586RR0LuRQ/VAq8hpgj_YI/AAAAAAAAHfg/jvAL5_nAlU0/s1600/Zamok%2BEdo%2B0-0043.jpg)

Polygonal masonry. Japan, Edo castle.

Here and there, traces of the formwork are clearly visible. I really look forward to an explanation.

#### About the oddities of the building material and the buildings themselves.

[The St. Petersburg Brick Museum](http://mirkis.sitecity.ru/ltext_2101194552.phtml?p_ident=ltext_2101194552.p_2102160209) presents more than five hundred samples from the ancient Greek times, and on a number of bricks the stamps are in Latin: RAMSAY, STABBARD, BORNHOLM and REX. Although 18th century brick factories branded the owners of the plants Sterling, Piston, Lvov, Brlvov, Stuckey, Strelin, Rusanov, Baranov, Stroganov, Kharchenko, Belyaev, Baikov, Khil, Lenin, Mufel, TOSM, Bogdanovich, Eduard, Evmentyev, Isakov, Tyrlov, Pirogov, Shorokhov, Gromov and K, Sobolev, Mga and a number of others. Where did Peter get the bricks from the times of ancient Greece? How much did it cost to transport the bricks? Who brought them there, and why such expense?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ltext_2101194552.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ltext_2101194552.jpg)
-->

[![](http://3.bp.blogspot.com/-YxtyzAqaaHc/UVrYz0zTCeI/AAAAAAAACY0/hgVACf098s8/s1600/ltext_2101194552.p_2102160209.308.jpg#clickable)](http://3.bp.blogspot.com/-YxtyzAqaaHc/UVrYz0zTCeI/AAAAAAAACY0/hgVACf098s8/s1600/ltext_2101194552.p_2102160209.308.jpg)

In addition to what has been said, we already know about the strict north-south orientation of all large megalithic structures, pyramids, temple complexes. So here: St. Isaac's Cathedral, as well as the Winter Palace, are oriented to the north in the same way as the "theatre" in Pompeii, and Baalbek in Lebanon. Coincidence? It is possible. However, they are not oriented towards the current pole, but towards the "pre-flood" pole. And the buildings outside the city centres are not oriented in any way. You can easily check this on "Google maps".

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_010.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_010.jpg)
-->

[![](http://3.bp.blogspot.com/-hJJrK46acZ4/UVrK1AuzbKI/AAAAAAAACX0/Q1MoKf2CtG0/s1600/%D0%98%D0%A1.jpg#clickable)](http://3.bp.blogspot.com/-hJJrK46acZ4/UVrK1AuzbKI/AAAAAAAACX0/Q1MoKf2CtG0/s1600/%D0%98%D0%A1.jpg)

St. Isaac's Cathedral

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a.jpg)
-->

[![](http://2.bp.blogspot.com/-aJTyIbjJz9s/UVrK3NUq3-I/AAAAAAAACX8/T33Jmh-uX5U/s1600/%D0%A2%D0%9F.jpg#clickable)](http://2.bp.blogspot.com/-aJTyIbjJz9s/UVrK3NUq3-I/AAAAAAAACX8/T33Jmh-uX5U/s1600/%D0%A2%D0%9F.jpg)

Theatre in Pompeii

Here, in my opinion, is a very interesting analysis of ideas about the origin of St. Isaac's Cathedral, just click on [the link](http://realhistory.borda.ru/?1-2-0-00000001-000-10001-0-1361239026).

Let's look at three facades...

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_008.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_008.jpg)
-->

[![](http://1.bp.blogspot.com/-XiCOMcWcz_8/UYjS-Ri2xAI/AAAAAAAACyk/dzGtQYxQ8a4/s1600/%D0%97%D0%9E.jpg#clickable)](http://1.bp.blogspot.com/-XiCOMcWcz_8/UYjS-Ri2xAI/AAAAAAAACyk/dzGtQYxQ8a4/s1600/%D0%97%D0%9E.jpg)

Ruins of the Olympic Temple of Zeus

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_009.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_009.jpg)
-->

[![](http://2.bp.blogspot.com/-_6s2nYh0DH8/UYNzCSLDbzI/AAAAAAAACwI/E0JmEsIbLUI/s1600/%D0%9F%D0%B0%D0%BD%D1%82%D0%B5%D0%BE%D0%BD.jpg#clickable)](http://2.bp.blogspot.com/-_6s2nYh0DH8/UYNzCSLDbzI/AAAAAAAACwI/E0JmEsIbLUI/s1600/%D0%9F%D0%B0%D0%BD%D1%82%D0%B5%D0%BE%D0%BD.jpg)

[Parthenon](http://atlantida-pravda-i-vimisel.blogspot.com/2011/11/blog-post_2576.html)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_003.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_003.jpg)
-->

[![](http://1.bp.blogspot.com/-wl85xPbxNAY/UYNz1ZdqfUI/AAAAAAAACwQ/TJqQ0-_Cegw/s1600/%D0%98%D1%81%D0%B0%D0%B0%D0%BA%D0%B8%D0%B9.jpg#clickable)](http://1.bp.blogspot.com/-wl85xPbxNAY/UYNz1ZdqfUI/AAAAAAAACwQ/TJqQ0-_Cegw/s1600/%D0%98%D1%81%D0%B0%D0%B0%D0%BA%D0%B8%D0%B9.jpg)

St. Isaac's Cathedral

And for the seed idea, guess what it could be... where is it?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/US_Capitol_Columns_002.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/US_Capitol_Columns_002.jpg)
-->

[![](http://3.bp.blogspot.com/-_WBmmO3k9SY/UZNLFWHLQ3I/AAAAAAAAC8A/keNx8FeVhyo/s1600/US_Capitol_Columns.jpg#clickable)](http://3.bp.blogspot.com/-_WBmmO3k9SY/UZNLFWHLQ3I/AAAAAAAAC8A/keNx8FeVhyo/s1600/US_Capitol_Columns.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1.jpg)
-->

[![](http://4.bp.blogspot.com/-yzuUxlbElF4/UZNFn-sFzAI/AAAAAAAAC68/5WtC_cKjAbg/s1600/1!!!.jpg#clickable)](http://4.bp.blogspot.com/-yzuUxlbElF4/UZNFn-sFzAI/AAAAAAAAC68/5WtC_cKjAbg/s1600/1!!!.jpg)

Count the number of columns. I will not torment you for a long time, I will give you a hint right away ...

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/attach.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/attach.jpg)
-->

[![](http://3.bp.blogspot.com/-MLiOKz5XfCU/UZNF6Jsl0XI/AAAAAAAAC7M/9VeetJ--1t4/s1600/attach.jpg#clickable)](http://3.bp.blogspot.com/-MLiOKz5XfCU/UZNF6Jsl0XI/AAAAAAAAC7M/9VeetJ--1t4/s1600/attach.jpg)

This is a model of the building, by William Thornton 1792. And here is the building itself

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/cb555f991fdb.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/cb555f991fdb.jpg)
-->

[![](http://1.bp.blogspot.com/-UjazVI3BzwI/UZNG0ZFs9OI/AAAAAAAAC7Y/Ljwic_9C7FI/s1600/cb555f991fdb.jpg#clickable)](http://1.bp.blogspot.com/-UjazVI3BzwI/UZNG0ZFs9OI/AAAAAAAAC7Y/Ljwic_9C7FI/s1600/cb555f991fdb.jpg)

Recognized the Capitol?

[Here is a link to the article on capitols, just click. ](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_15.html)

Pay attention to these columns, do they tell you anything?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/123jpg.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/123jpg.jpg)
-->

[![](http://4.bp.blogspot.com/-z1YDpkV0yeM/UZNJbIZOVHI/AAAAAAAAC7o/8gRXaTa2CR8/s1600/123,jpg.jpg#clickable)](http://4.bp.blogspot.com/-z1YDpkV0yeM/UZNJbIZOVHI/AAAAAAAAC7o/8gRXaTa2CR8/s1600/123,jpg.jpg)

These columns were originally supposed to support the dome of the Capitol. But there something did not grow together either with the number of columns brought, or with the size, and the columns had to be added to the Arboretum.

Now we look here

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1017_Zeus-Temple.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1017_Zeus-Temple.jpg)
-->

[![](http://4.bp.blogspot.com/-5LU4KdkxHU0/UZNKGYycT-I/AAAAAAAAC7w/j-dvMgI18lo/s1600/1017_Zeus-Temple.jpg#clickable)](http://4.bp.blogspot.com/-5LU4KdkxHU0/UZNKGYycT-I/AAAAAAAAC7w/j-dvMgI18lo/s1600/1017_Zeus-Temple.jpg)

Ruins of the Olympian Temple of Zeus. Compare columns

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0012-016-Drevnegrecheskaja-arkhitektura.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0012-016-Drevnegrecheskaja-arkhitektura.jpg)
-->

[![](http://2.bp.blogspot.com/-4KePQeNNuRQ/UsZlQhplEeI/AAAAAAAAGO4/1Tibyl0YpKo/s1600/0012-016-Drevnegrecheskaja-arkhitektura.jpg#clickable)](http://2.bp.blogspot.com/-4KePQeNNuRQ/UsZlQhplEeI/AAAAAAAAGO4/1Tibyl0YpKo/s1600/0012-016-Drevnegrecheskaja-arkhitektura.jpg)

Parthenon

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1326457617_002.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1326457617_002.jpg)
-->

[![](http://3.bp.blogspot.com/-gaQnb9YT8ig/UsZlgBRUXrI/AAAAAAAAGPI/fsXYnRbzF2g/s1600/1326457617_002.jpg#clickable)](http://3.bp.blogspot.com/-gaQnb9YT8ig/UsZlgBRUXrI/AAAAAAAAGPI/fsXYnRbzF2g/s1600/1326457617_002.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0b02c96febf4f1ede644ceeb52d5ee82.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0b02c96febf4f1ede644ceeb52d5ee82.jpg)
-->

[![](http://3.bp.blogspot.com/-ax0atbeeGF0/UsZlekCIpNI/AAAAAAAAGPA/B7_eGQknYes/s1600/0b02c96febf4f1ede644ceeb52d5ee82.jpg#clickable)](http://3.bp.blogspot.com/-ax0atbeeGF0/UsZlekCIpNI/AAAAAAAAGPA/B7_eGQknYes/s1600/0b02c96febf4f1ede644ceeb52d5ee82.jpg)

Baalbek, Temple of Jupiter.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6edbd66b56.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6edbd66b56.jpg)
-->

[![](http://2.bp.blogspot.com/-RgQFAZpoHOQ/UsZlteqIYoI/AAAAAAAAGPQ/A1OzexWQZwY/s1600/6edbd66b56.jpg#clickable)](http://2.bp.blogspot.com/-RgQFAZpoHOQ/UsZlteqIYoI/AAAAAAAAGPQ/A1OzexWQZwY/s1600/6edbd66b56.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/241834_0b2e50b1f1022e8bXL.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/241834_0b2e50b1f1022e8bXL.jpg)
-->

[![](http://4.bp.blogspot.com/-Nfz4Kao407Y/UsZltrBjNII/AAAAAAAAGPU/yU6g_K9R-sU/s1600/241834_0b2e50b1f1022e8bXL.jpg#clickable)](http://4.bp.blogspot.com/-Nfz4Kao407Y/UsZltrBjNII/AAAAAAAAGPU/yU6g_K9R-sU/s1600/241834_0b2e50b1f1022e8bXL.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/bluetooth-1090952985-162.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/bluetooth-1090952985-162.jpg)
-->

[![](http://1.bp.blogspot.com/-BLBxx5wrakY/UsZl78gayCI/AAAAAAAAGPo/7DTmOOU2t68/s1600/bluetooth-1090952985-162.jpg#clickable)](http://1.bp.blogspot.com/-BLBxx5wrakY/UsZl78gayCI/AAAAAAAAGPo/7DTmOOU2t68/s1600/bluetooth-1090952985-162.jpg)

Kazan Cathedral. St. Petersburg.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/US_Capitol_Columns.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/US_Capitol_Columns.jpg)
-->

[![](http://3.bp.blogspot.com/-_WBmmO3k9SY/UZNLFWHLQ3I/AAAAAAAAC8A/keNx8FeVhyo/s1600/US_Capitol_Columns.jpg#clickable)](http://3.bp.blogspot.com/-_WBmmO3k9SY/UZNLFWHLQ3I/AAAAAAAAC8A/keNx8FeVhyo/s1600/US_Capitol_Columns.jpg)

Capitol. Looking for differences in the colonnade ... not finding what conclusion? In terms of coincidences, this picture finished me off. Boston Capitol.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/WXQ3hR7ra8U.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/WXQ3hR7ra8U.jpg)
-->

[![](http://2.bp.blogspot.com/-J7HfRTxV3ek/UicjQVTbMPI/AAAAAAAAEZk/982YfqC8i38/s1600/WXQ3hR7ra8U.jpg#clickable)](http://2.bp.blogspot.com/-J7HfRTxV3ek/UicjQVTbMPI/AAAAAAAAEZk/982YfqC8i38/s1600/WXQ3hR7ra8U.jpg)

It seemed to me, or a copy of the Pillar of Alexandria is present here. This is how it looks now.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/WW1S1543-1sAP.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/WW1S1543-1sAP.jpg)
-->

[![](http://1.bp.blogspot.com/-lfyA6KkD9mU/UicjpxVrsjI/AAAAAAAAEZs/qV_LeUqaNsc/s1600/WW1S1543-1sAP.jpg#clickable)](http://1.bp.blogspot.com/-lfyA6KkD9mU/UicjpxVrsjI/AAAAAAAAEZs/qV_LeUqaNsc/s1600/WW1S1543-1sAP.jpg)

[Капитолий штата Массачусетс в Бостоне](http://www.townsusa.ru/dostoprimechatelnosti-goroda/kapitoliy-shtata-massachusets-v-bostone)

And here is the Capitol of Iowa in Des Moines.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/vfiles8530.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/vfiles8530.jpg)
-->

[![](http://1.bp.blogspot.com/-gGXlL_JdfUY/Uic5ywiruJI/AAAAAAAAEaI/kTet2aZcX6Q/s1600/vfiles8530.jpg#clickable)](http://1.bp.blogspot.com/-gGXlL_JdfUY/Uic5ywiruJI/AAAAAAAAEaI/kTet2aZcX6Q/s1600/vfiles8530.jpg)

Why not Isaac? The reference to [the Capitol without a column... well, why not?](http://atlantida-pravda-i-vimisel.blogspot.com/2012/11/blog-post_15.html)

There is a strong impression that all these columns came from the same workshop, created by the hand of a single master, somewhere in the Middle East or Egypt. They were simply taken from a warehouse and shipped all over the world according to the volumes ordered.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/apom1.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/apom1.jpg)
-->

[![](http://1.bp.blogspot.com/-RJhLiWd8ZDk/UZR3IkPSaNI/AAAAAAAAC9w/Om_s-GziwNo/s1600/apom1.jpg#clickable)](http://1.bp.blogspot.com/-RJhLiWd8ZDk/UZR3IkPSaNI/AAAAAAAAC9w/Om_s-GziwNo/s1600/apom1.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/apom2.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/apom2.jpg)
-->

[![](http://2.bp.blogspot.com/-dZ4V0q_XRL8/UZR3I_BzUuI/AAAAAAAAC90/vrmsJRBp1IU/s1600/apom2.jpg#clickable)](http://2.bp.blogspot.com/-dZ4V0q_XRL8/UZR3I_BzUuI/AAAAAAAAC90/vrmsJRBp1IU/s1600/apom2.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/IMG_6963_resize.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/IMG_6963_resize.jpg)
-->

[![](http://4.bp.blogspot.com/-yi12_2n7RgQ/UpiDmKgoLhI/AAAAAAAAFrM/DHjqLgjgJV0/s1600/IMG_6963_resize.jpg#clickable)](http://4.bp.blogspot.com/-yi12_2n7RgQ/UpiDmKgoLhI/AAAAAAAAFrM/DHjqLgjgJV0/s1600/IMG_6963_resize.jpg)

[Вот фотографии Апамеи и окрестностей (Сирия)](http://nikidel.livejournal.com/30514.html)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_9db44_4c8439ea_orig.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_9db44_4c8439ea_orig.jpg)
-->

[![](http://2.bp.blogspot.com/-hMvjBokm2nc/UpiCAD9ALCI/AAAAAAAAFq4/iMOWbUCBJFc/s1600/0_9db44_4c8439ea_orig.jpg#clickable)](http://2.bp.blogspot.com/-hMvjBokm2nc/UpiCAD9ALCI/AAAAAAAAFq4/iMOWbUCBJFc/s1600/0_9db44_4c8439ea_orig.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_9db4d_11d568f3_orig.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_9db4d_11d568f3_orig.jpg)
-->

[![](http://1.bp.blogspot.com/-PYX0FX6IcBQ/UpiDH40RKXI/AAAAAAAAFrE/Lyw224urtRA/s1600/0_9db4d_11d568f3_orig.jpg#clickable)](http://1.bp.blogspot.com/-PYX0FX6IcBQ/UpiDH40RKXI/AAAAAAAAFrE/Lyw224urtRA/s1600/0_9db4d_11d568f3_orig.jpg)

[Palmyra, also known as Tadmor](http://cool-skarlet.livejournal.com/tag/%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%8F)

And this is Egypt. The columns are all the same.

[](http://3.bp.blogspot.com/-EkISt6vGbRI/UbRCZJjbVII/AAAAAAAADXM/ztPkUElUa9s/s1600/%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.jpg)[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_002.jpg#clickable)](http://3.bp.blogspot.com/-EkISt6vGbRI/UbRCZJjbVII/AAAAAAAADXM/ztPkUElUa9s/s1600/%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/2.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/2.jpg)
-->

[![](http://3.bp.blogspot.com/-3iGlrf7M9bE/UbRCZKvt78I/AAAAAAAADXI/qiMuNKFiWk4/s1600/%D0%A12.jpg#clickable)](http://3.bp.blogspot.com/-3iGlrf7M9bE/UbRCZKvt78I/AAAAAAAADXI/qiMuNKFiWk4/s1600/%D0%A12.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/3_002.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/3_002.jpg)
-->

[![](http://4.bp.blogspot.com/-4R33SauAdeQ/UbRCZUbTdtI/AAAAAAAADXQ/No7gNG_-j4M/s1600/%D0%A13.jpg#clickable)](http://4.bp.blogspot.com/-4R33SauAdeQ/UbRCZUbTdtI/AAAAAAAADXQ/No7gNG_-j4M/s1600/%D0%A13.jpg)

Many artists of the 18th century painted antiquity: intact antiquity and half-destroyed antiquity.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1800caPaestumTempioDiNettunoHubertRobert.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1800caPaestumTempioDiNettunoHubertRobert.jpg)
-->

[![](http://1.bp.blogspot.com/-lgJbuuyi1Bw/UpcWsB7TaqI/AAAAAAAAFpg/2FImRLeMRCY/s1600/1800ca+Paestum,+Tempio+Di+Nettuno+Hubert+Robert.jpg#clickable)](http://1.bp.blogspot.com/-lgJbuuyi1Bw/UpcWsB7TaqI/AAAAAAAAFpg/2FImRLeMRCY/s1600/1800ca+Paestum,+Tempio+Di+Nettuno+Hubert+Robert.jpg)

[Here are the paintings for your attention ](http://atlantida-pravda-i-vimisel.blogspot.com/2012/10/blog-post_3.html)

[Robert Hubert (1733---1808). French painter, master of architectural landscape. ](http://atlantida-pravda-i-vimisel.blogspot.com/2012/10/blog-post_3.html)

One more question: [where did antiquity come from in China?](http://vk.com/album50348784_105253219)? As far as I remember, neither Greeks nor Romans got there, let alone Egyptians.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/EH3JWl15CaI.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/EH3JWl15CaI.jpg)
-->

[![](http://2.bp.blogspot.com/-9eWuYTP3V90/UpcWcJrdSDI/AAAAAAAAFpY/_4zw3XtB9ho/s1600/EH3JWl15CaI.jpg#clickable)](http://2.bp.blogspot.com/-9eWuYTP3V90/UpcWcJrdSDI/AAAAAAAAFpY/_4zw3XtB9ho/s1600/EH3JWl15CaI.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/GhpwHZhZavc.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/GhpwHZhZavc.jpg)
-->

[![](http://2.bp.blogspot.com/-bCt-gwU0mtQ/UpcV5tdLiyI/AAAAAAAAFpI/Z01UhKaNekM/s1600/GhpwHZhZavc.jpg#clickable)](http://2.bp.blogspot.com/-bCt-gwU0mtQ/UpcV5tdLiyI/AAAAAAAAFpI/Z01UhKaNekM/s1600/GhpwHZhZavc.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/gFK3USrk2ac.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/gFK3USrk2ac.jpg)
-->

[![](http://4.bp.blogspot.com/-dWZpNjRFIS8/UpcVmYVO6nI/AAAAAAAAFpA/aCKvVSey88I/s1600/gFK3USrk2ac.jpg#clickable)](http://4.bp.blogspot.com/-dWZpNjRFIS8/UpcVmYVO6nI/AAAAAAAAFpA/aCKvVSey88I/s1600/gFK3USrk2ac.jpg)

[Link](http://vk.com/album50348784_105253219)

It is silly, in my opinion, to attribute the authorship of St. Isaac's Cathedral to Montferan. Here is an excerpt from Vigel's "Notes" on the reconstruction of St. Isaac's Cathedral: In his words, the Emperor asked Betancourt to instruct someone to draw up a project for the reconstruction of St. Isaac's Cathedral in such a way as to preserve all of the building, except with a small size increase to make the view of this great monumen even more magnificent and noble.

Apparently, because these words belonged to the sovereign, censorship did not dare touch them.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/240410_original.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/240410_original.jpg)
-->

[![](http://3.bp.blogspot.com/-EXGwANTLv9A/UgHhio4ZKpI/AAAAAAAAEAs/I0Keip3k6Co/s1600/240410_original.jpg#clickable)](http://3.bp.blogspot.com/-EXGwANTLv9A/UgHhio4ZKpI/AAAAAAAAEAs/I0Keip3k6Co/s1600/240410_original.jpg)

Here is the material from the official history.

Paul I, who ascended the throne, commissioned the architect V. Brenna to urgently complete the construction (reconstruction) of the cathedral. Fulfilling the tsar's wish, the architect had to distort Rinaldi's project - to reduce the size of the upper part of the building and the main dome but (allegedly) he refused to build four small domes.

The marble for cladding the upper part of the cathedral was transferred (removed from the cathedral) to the construction of the residence of Paul I: Mikhailovsky Castle. The cathedral turned out to be stocky, and in artistic terms even ridiculous - ugly brick walls stood on a luxurious marble and granite base (neither the architect nor the builders tried - nor failed - to repeat the former grandeur of the monument and granite masonry). This structure caused ridicule and bitter irony among contemporaries. For example, Akimov, a naval officer who came to Russia after a long stay in England, wrote an epigram:

> A monument to two kingdoms.

> Both so decent

> On the marble base

> A brick top has been raised

When trying to attach a leaf with this quatrain to the facade of Akimov Cathedral, he was arrested. He paid a great deal for his wit: his tongue was cut off and exiled to Siberia.

The St. Petersburgers retold a dangerous epigram in various versions:

> "This temple will show us

> "Who caresses, who beats

> It started with marble

> Finished with a brick

Later, under Emperor Alexander I, when the final Monferranovsky version of the cathedral began to dismantle the brickwork, folklore responded with a new epigram.

> This temple of three reigns is an image:

> Granite, brick and destruction

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/efd03e016684.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/efd03e016684.jpg)
-->

[![](http://1.bp.blogspot.com/-fztaSEoCfgU/UYjUb38W3rI/AAAAAAAACyw/CTX_KkjhJiM/s1600/efd03e016684.jpg#clickable)](http://1.bp.blogspot.com/-fztaSEoCfgU/UYjUb38W3rI/AAAAAAAACyw/CTX_KkjhJiM/s1600/efd03e016684.jpg)

Restoration of St. Isaac's Cathedral. [Album by Montferrand A. Eglise de Saint Isaac. Saint-Petersburg, 1820](http://leb.nlr.ru/fullpage/324255/glise-cath%C3%A9drale-de-aint-saac-escription-architecturale-pittoresque-et-historique-de-ce-monument%E2%80%A6-par-icard-de-ontferrand).

On page 70 of [Monsieur Montferrand's famous album](http://leb.nlr.ru/fullpage/324255/glise-cath%C3%A9drale-de-aint-saac-escription-architecturale-pittoresque-et-historique-de-ce-monument%E2%80%A6-par-icard-de-ontferrand) it can clearly be seen that remnants of ancient temple structures were used for the construction of St. Petersburg buildings. He does not even hide it.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_004.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/a_004.jpg)
-->

[![](http://4.bp.blogspot.com/-rOnM-guKcIc/UhMs0DIkYEI/AAAAAAAAEI8/FfHRqDXXnCQ/s1600/%D0%9C.jpg#clickable)](http://4.bp.blogspot.com/-rOnM-guKcIc/UhMs0DIkYEI/AAAAAAAAEI8/FfHRqDXXnCQ/s1600/%D0%9C.jpg)

The red arrow indicates marble fragments of structures, yellow granite fragments. This construction waste is like two drops of water similar to the fragments of the ["Temple of Jupiter" in Baalbek](http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/44.html).

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_231b6_e45f4b71_orig.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_231b6_e45f4b71_orig.jpg)
-->

[![](http://4.bp.blogspot.com/-THMdCFkQmno/UhM6o-QJkdI/AAAAAAAAEKs/F6IuHHUYXKE/s1600/0_231b6_e45f4b71_orig.jpg#clickable)](http://4.bp.blogspot.com/-THMdCFkQmno/UhM6o-QJkdI/AAAAAAAAEKs/F6IuHHUYXKE/s1600/0_231b6_e45f4b71_orig.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba23.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba23.jpg)
-->

[![](http://2.bp.blogspot.com/-AouItegn3EY/UhMuR2eYI9I/AAAAAAAAEJY/ogRgkWOfZOw/s1600/ba23.jpg#clickable)](http://2.bp.blogspot.com/-AouItegn3EY/UhMuR2eYI9I/AAAAAAAAEJY/ogRgkWOfZOw/s1600/ba23.jpg)

There is a very interesting article about Baalbek at this link: [The secret of Baalbek has been solved!](http://nanoworld.narod.ru/144.htm)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba51.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/ba51.jpg)
-->

[![](http://2.bp.blogspot.com/-WEEI9PRfRso/UhMuR70uIMI/AAAAAAAAEJc/vr7vpL_O_FY/s1600/ba51.jpg#clickable)](http://2.bp.blogspot.com/-WEEI9PRfRso/UhMuR70uIMI/AAAAAAAAEJc/vr7vpL_O_FY/s1600/ba51.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_22cfa_5b312a8e_L.gif#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_22cfa_5b312a8e_L.gif)
-->

[![](http://3.bp.blogspot.com/-PV3WZfF69Sg/UhM0Ra9lW7I/AAAAAAAAEKE/q1OgV2ZQsGg/s1600/0_22cfa_5b312a8e_L.jpg#clickable)](http://3.bp.blogspot.com/-PV3WZfF69Sg/UhM0Ra9lW7I/AAAAAAAAEKE/q1OgV2ZQsGg/s1600/0_22cfa_5b312a8e_L.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10_231a7_9a0f07a9_orig.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/10_231a7_9a0f07a9_orig.jpg)
-->

[![](http://1.bp.blogspot.com/-Pm1JdoQerlE/UhM2E8-RGsI/AAAAAAAAEKY/SIAhvKC5jHI/s1600/10_231a7_9a0f07a9_orig.jpg#clickable)](http://1.bp.blogspot.com/-Pm1JdoQerlE/UhM2E8-RGsI/AAAAAAAAEKY/SIAhvKC5jHI/s1600/10_231a7_9a0f07a9_orig.jpg)

The same pattern, the same designs. Above, we examined the correspondence of the columns of St. Isaac's Cathedral in St. Petersburg to the columns of the [Temple of Jupiter in Baalbek, Lebanon](http://atlantida-pravda-i-vimisel.blogspot.com/2011/10/44.html)  They are almost identical. And here is the pattern on the floor of St. Isaac's Cathedral.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1isaak_sobor_23.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/1isaak_sobor_23.jpg)
-->

[![](http://4.bp.blogspot.com/-1r0b7yrT__A/UhM2Szo42-I/AAAAAAAAEKg/Xr27ZVheAbE/s1600/1isaak_sobor_23.jpg#clickable)](http://4.bp.blogspot.com/-1r0b7yrT__A/UhM2Szo42-I/AAAAAAAAEKg/Xr27ZVheAbE/s1600/1isaak_sobor_23.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/40712f081048d06212d5bf09d59_prev.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/40712f081048d06212d5bf09d59_prev.jpg)
-->

[![](http://3.bp.blogspot.com/-nd6pDRjyWrg/UhR22THPcMI/AAAAAAAAENQ/Pzu6W5FrtMY/s1600/40712f081048d06212d5bf09d59_prev.jpg#clickable)](http://3.bp.blogspot.com/-nd6pDRjyWrg/UhR22THPcMI/AAAAAAAAENQ/Pzu6W5FrtMY/s1600/40712f081048d06212d5bf09d59_prev.jpg)

St Isaac's Cathedral sinned with Greek symbolism

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/DSCN4870.JPG#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/DSCN4870.JPG)
-->

[![](http://4.bp.blogspot.com/-LlAueEIHGwA/Uk63eAwZ5GI/AAAAAAAAEqo/3ZhF_94E3ew/s1600/DSCN4870.JPG#clickable)](http://4.bp.blogspot.com/-LlAueEIHGwA/Uk63eAwZ5GI/AAAAAAAAEqo/3ZhF_94E3ew/s1600/DSCN4870.JPG)

And this picture

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/orname-ru-gi27f3.png#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/orname-ru-gi27f3.png)
-->

[![](http://3.bp.blogspot.com/-NGYqqYGEdI8/Uk65y8WmhHI/AAAAAAAAErA/tk0dIjCnV9o/s1600/orname-ru-gi27f3.png#clickable)](http://3.bp.blogspot.com/-NGYqqYGEdI8/Uk65y8WmhHI/AAAAAAAAErA/tk0dIjCnV9o/s1600/orname-ru-gi27f3.png)

says it is the officially recognized Greek meander circle.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/or7_2f2.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/or7_2f2.jpg)
-->

[![](http://2.bp.blogspot.com/-S8JKjrtIBrU/Uk646LYnMCI/AAAAAAAAEq0/j0Btu9f_L1A/s1600/or7_2f2.jpg#clickable)](http://2.bp.blogspot.com/-S8JKjrtIBrU/Uk646LYnMCI/AAAAAAAAEq0/j0Btu9f_L1A/s1600/or7_2f2.jpg)

GREEK ornament-meander.

Did the Greeks build St. Isaac's Cathedral? This bas-relief speaks for itself

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_70915_ecf9be55_orig.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_70915_ecf9be55_orig.jpg)
-->

[![](http://1.bp.blogspot.com/-4QJ8rb5GeFs/Uk66soL3sFI/AAAAAAAAErM/N8bScl8lhNg/s1600/0_70915_ecf9be55_orig.jpg#clickable)](http://1.bp.blogspot.com/-4QJ8rb5GeFs/Uk66soL3sFI/AAAAAAAAErM/N8bScl8lhNg/s1600/0_70915_ecf9be55_orig.jpg)

[Ornament on the inner walls of the Temple of Hadrian.](http://engineeer-garin.livejournal.com/57030.html)

Pay attention to the carved modular portico of the Temple of Jupiter

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_232e3_71f6d526_orig.gif#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/0_232e3_71f6d526_orig.gif)
-->

[![](http://3.bp.blogspot.com/-IdrIaMiBa-U/UhM0RyovFyI/AAAAAAAAEKQ/lx8krxyLZmc/s1600/0_232e3_71f6d526_orig.jpg#clickable)](http://3.bp.blogspot.com/-IdrIaMiBa-U/UhM0RyovFyI/AAAAAAAAEKQ/lx8krxyLZmc/s1600/0_232e3_71f6d526_orig.jpg)

Here are the modules at the Kazan Cathedral. Shall we play "Find 10 Differences"?

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/e35af79ca49b.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/e35af79ca49b.jpg)
-->

[![](http://1.bp.blogspot.com/-lxE13bD2kMI/UoToN8ANKoI/AAAAAAAAFZY/ar6hkvd66NY/s1600/e35af79ca49b.jpg#clickable)](http://1.bp.blogspot.com/-lxE13bD2kMI/UoToN8ANKoI/AAAAAAAAFZY/ar6hkvd66NY/s1600/e35af79ca49b.jpg)

And here is St. Isaac's Cathedral.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6d529b330776.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/6d529b330776.jpg)
-->

[![](http://4.bp.blogspot.com/-ggx_BB00G5o/UhW_LSkWk5I/AAAAAAAAEO0/rI1-G3TW9Ho/s1600/6d529b330776.jpg#clickable)](http://4.bp.blogspot.com/-ggx_BB00G5o/UhW_LSkWk5I/AAAAAAAAEO0/rI1-G3TW9Ho/s1600/6d529b330776.jpg)

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/16d529b330776.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/16d529b330776.jpg)
-->

[![](http://2.bp.blogspot.com/-AdOf9n9NlDc/UhXCVhXXweI/AAAAAAAAEPM/xoxFH-BLhkM/s1600/16d529b330776.jpg#clickable)](http://2.bp.blogspot.com/-AdOf9n9NlDc/UhXCVhXXweI/AAAAAAAAEPM/xoxFH-BLhkM/s1600/16d529b330776.jpg)

The three in the centre are original, and those at the sides are fresh - which is all that Montferan was able to do when he reconstructed the cathedral. He did not have either the skill or the time to repeat the original.

Here is another novelty

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/e53e8b551956.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/e53e8b551956.jpg)
-->

[![](http://2.bp.blogspot.com/-pCC_ShIEFVM/UhXAmbxXq4I/AAAAAAAAEPA/YA8Aiew1mBI/s1600/e53e8b551956.jpg#clickable)](http://2.bp.blogspot.com/-pCC_ShIEFVM/UhXAmbxXq4I/AAAAAAAAEPA/YA8Aiew1mBI/s1600/e53e8b551956.jpg)

To all of the above about St. Isaac's Cathedral and its resemblance to Jupiter's Cathedral in Baalbek, Lebanon, I will add another highlight that will seem like 'spice' to official historians. Take a look at the picture:

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/39.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/39.jpg)
-->

[![](http://4.bp.blogspot.com/-9ZQ2pCRMsYY/Uhw_c0jI_JI/AAAAAAAAEQA/TQb95F1NEEU/s1600/39.jpg#clickable)](http://4.bp.blogspot.com/-9ZQ2pCRMsYY/Uhw_c0jI_JI/AAAAAAAAEQA/TQb95F1NEEU/s1600/39.jpg)

This is a drawing from the Montferan album (19th century). The French signature reads:

> Un ouvrier russe sculptant la tete colossale de Jupiter dans un bloc en granit

Translation:

> A Russian worker carves a giant head of Jupiter from a granite block.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/YHzTn.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/YHzTn.jpg)
-->

[![](http://1.bp.blogspot.com/-RHrjxvoRZqg/UqCA0Zc843I/AAAAAAAAFsc/oylcrCETDRI/s1600/YHzTn.jpg#clickable)](http://1.bp.blogspot.com/-RHrjxvoRZqg/UqCA0Zc843I/AAAAAAAAFsc/oylcrCETDRI/s1600/YHzTn.jpg)

> And maybe the city was,

> and had a glorious name

> Peter!

> ...or the hail of Jupiter.

This was then renamed Petersburg, and later, Saint Petersburg.

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/X8GIs.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/X8GIs.jpg)
-->

[![](http://2.bp.blogspot.com/-KhsciODqAwc/UqB_Zf5Qz5I/AAAAAAAAFsM/jhOxe3ld5Bc/s1600/X8GIs.jpg)

And here's what's left for you

<!-- Local
[![](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Statuya_Yupitera.jpg#clickable)](Where is the city from Chapter 2 Ancient tale in the north of Europe_files/Statuya_Yupitera.jpg)
-->

[![](http://4.bp.blogspot.com/-66VEhfFsUPQ/Uhxa0czyVDI/AAAAAAAAESk/rU_QrNCs0ag/s1600/Statuya_Yupitera.jpg#clickable)](http://4.bp.blogspot.com/-66VEhfFsUPQ/Uhxa0czyVDI/AAAAAAAAESk/rU_QrNCs0ag/s1600/Statuya_Yupitera.jpg)

A colossal statue of the ruler of the gods of Jupiter, created by a Roman sculptor, reduced to three meters, magnificent [Original Physias](http://demsvet.ru/istoriya-knigi/bogi-olimpiytsyi).

The Supreme God is represented in the majestic pose of the victorious: in his hands he holds the attributes of a god - a sceptre and a statue of the goddess of victory Nika. At his right foot, a sacred eagle, a copy from the statue of Olympic Zeus - work of an ancient Greek sculptor of the V century BC. Phidias. State Hermitage Museum. St. Petersburg State Hermitage Museum.

### Conclusion:

The collection was standing! And it had been standing for more than a hundred years. The restorers had not only materials, but also [an unimaginable number of statues and monuments](http://eldisblog.com/post195385077), which were spread all over Peter and beyond. In the picture, Montferan tried to add a huge statue of Jupiter to the official version, but either the version did not pass, or the head of Jupiter was too compromised by the new forgers, and it was split into building material for the foundations of new buildings, as well as many other unsuitable things. Only the three-meter version remained.

But some of the statues have still reached our time, albeit in a modified form with other inscriptions, but they got here!

When copying an article, do not forget to mention authorship.

The address for the full version is here: [Where is the city from? (автор ZigZag)](http://atlantida-pravda-i-vimisel.blogspot.com/2012/12/blog-post_3.html)

Author: ZigZag

© All rights reserved. The original author retains ownership and rights.
