title: Inspired by "And there was a Country before Russia". Part 3
date: 2013-10-11
modified: Fri 02 Apr 2021 20:03:04 BST
category: pro_vladimir
tags: cannons, technology
slug: 
authors: Vladimir Mamzerev
summary: What use are barrels that cannot, and were not intended to, fire gunpowder, whose working position is vertical, which in some cases were not intended for transportation, and which have no mounting points?
status: published
local: Inspired by And There Was a Country Before Russia - Part 3_files
originally: По мотивам «И была Страна до России». Часть 3.html

### Translated from:
[https://pro-vladimir.livejournal.com/1376.html](https://pro-vladimir.livejournal.com/1376.html)

<!--
[https://pro-vladimir.livejournal.com/2013/10/11/](https://pro-vladimir.livejournal.com/2013/10/11/)
-->

[First in series (original Russian](https://pro-vladimir.livejournal.com/994.html)

[First in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-1.html)

Based on ["And there was a Country before Russia"](http://dmitrijan.livejournal.com/71535.html)

*Historical description of clothing and weapons of the Russian troops - Second ed. Publisher: Type. V. S. Balashev and Co, Saint Petersburg, 1899-1902*

The 1841-1862 edition of this book was the property of the Ministry of War and was not widely distributed. Printed in a small print run, by the end of the XIX century it had become a great bibliographic rarity. Only the largest metropolitan or imperial collections possessed a complete set of notebooks, which were grouped into so-called "folio", or even single "colored" copies of the work.

Therefore the Technical Committee Of the main Quartermaster's office republished the original edition in 1899 - 1902. Issued "for the wider use" - ie for general sale - this cheaper version received the unofficial name of the *"Quartermaster's Publication"*.

Those who are interested can get acquainted with the full collection by browsing here: [http://www.runivers.ru/lib/book3096/](http://www.runivers.ru/lib/book3096/)

<!-- Local
[![](По мотивам «И была Страна до России». Часть 3_files/139.jpg#clickable)](По мотивам «И была Страна до России». Часть 3_files/139.jpg)
-->

[![](http://legion1812.narod.ru/3/ris/139.jpg#clickable)](http://legion1812.narod.ru/3/ris/139.jpg)

This image of guns comes from this collection. When initially collecting information about these devices, there was an idea that only these sketches remained. But no, as it turned out. Praise to the all-powerful Web that access to information has been preserved.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 3_files/2875c546abff7c28a768e194df3066ee.jpg#clickable)](По мотивам «И была Страна до России». Часть 3_files/2875c546abff7c28a768e194df3066ee.jpg)
-->

[![](https://diletant.media/upload/medialibrary/287/2875c546abff7c28a768e194df3066ee.jpg#clickable)](https://diletant.media/upload/medialibrary/287/2875c546abff7c28a768e194df3066ee.jpg)

Look. A flat end face at the end of the barrel. There is a minimum of decoration on the barrel itself, or rather, there is no decoration at all, only the inscription. Some people suggest that the barrel is, rather, a certain kind of core (ie a former for coil), and that a coil was wound around it.

<!-- Local
![](По мотивам «И была Страна до России». Часть 3_files/5789_original.jpg#resized)
-->

[![](http://ic.pics.livejournal.com/aeromamont/14323686/5789/5789_original.jpg#clickable)](http://ic.pics.livejournal.com/aeromamont/14323686/5789/5789_original.jpg)

And this second barrel was found. Not much is known about it, either. But at least there are visible notches.

So what use can there be for barrels that cannot and were not intended for firing gunpowder, whose working position is vertical, which in some cases were not intended for transportation, which do not have any hooks for fixing. Which are disassembled and have replaceable breech parts. What is this set of pipes that we see presented to us as "squeaks", "bombards", or as anything other than... what?

Or maybe it's part of a device, a system that existed back then? Part of it, not the whole system. A certain spare part with a very specific purpose.

Does this remind you of anything?

[http://teslacoil.ru/em/magnetron-i-svch-pushka/](http://teslacoil.ru/em/magnetron-i-svch-pushka/)

The main element of a conventional microwave oven is a magnetron, a vacuum device for generating microwave radiation. Its older relatives are in all sorts of radars and radar systems.

<!-- Local
[![](По мотивам «И была Страна до России». Часть 3_files/SAM_1986.jpg#clickable)](По мотивам «И была Страна до России». Часть 3_files/SAM_1986.jpg)
-->

[![](http://teslacoil.ru/wp-content/gallery/magnetron/SAM_1986.JPG#clickable)](http://teslacoil.ru/wp-content/gallery/magnetron/SAM_1986.JPG)

In the photo, radio amateurs have assembled a microwave gun based on a magnetron from a household microwave oven, effectively igniting a gas-discharge lamp (a circular fluorescent tube).

But just having fun with a magnetron is pretty boring. It is much more interesting to adapt an antenna to it to create a more or less directed radiation flow. A parabolic dish would be ideal. There's just the diameter required: five meters. A little worse, but also not bad antenna would be a "horn" type, but its manufacture is quite tedious and it is quite cumbersome, although, of course, less so than a parabola. I eventually settled on a 'can antenna' (Google 'cantenna')

Wait, so this is what comes out of our barrels with a flat base, an antenna for a radiator element? Their breech ends were changed to adjust to a certain frequency of radiation? And this is just part of a certain system that worked back then.

Vladimir Mamzerev. 11.10.2013 G.

[Next in series (English)](./inspired-by-and-there-was-a-country-before-russia-part-4.html)

© All rights reserved. The original author has asserted their ownership and rights.
