title: Another history of the Earth - Part 1e
date: 2017-03-26
modified: 2022-12-14 16:04:53
category:
tags: catastrophe; Tartaria
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/222351.html
originally: Another history of the Earth - Part 1e.html
local: Another history of the Earth - Part 1e_files/
summary: The edges of the plates actually went deep into the mantle and yet have not melted because this happened relatively recently, not tens of millions of years ago. It happened during the catastrophe I am describing when a large object broke through the Earth.
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/222351.html](https://mylnikovdm.livejournal.com/222351.html)

[First article in series (Russian)](http://mylnikovdm.livejournal.com/219459.html)

[First article in series (English)](./another-history-of-the-earth-part-1a.html) 

After the publication of the first parts, as I expected, representatives of official science noted in the comments, who almost immediately declared everything written as nonsense, and called the author an ignoramus and ignoramus. Now, if the author had studied geophysics, petrology, historical geology and plate tectonics, he would never have written such nonsense.

Unfortunately, I never managed to get any intelligible explanations from the author of these comments. Instead, she moved on to insult me and even other blog readers, so I had to send her "to the bathhouse". At the same time, I want to reiterate that I am always ready for a constructive dialogue and admit my mistakes if a critic puts forward convincing arguments, and not arguments in the form of "there is no time to explain to fools, go read smart books, then you will understand". Moreover, I have read a large number of smart books on various topics in my life, so I don't find smart books intimidating. The main thing about such recommendations is that the book should actually be smart and meaningful.

In addition, according to the experience of the last few years, when I began to collect information about the planet-wide disasters that occurred on Earth, I can say that most of the proposals from the "experts" who recommended me to go and read "smart books" ended up with the result that I either found in their own books additional facts that favor my version, or I found errors and inconsistencies in them, without which the slender model promoted by the author fell apart. For example, this was the case with soil formation, when theoretical constructions, adjusted to the observed historical facts, gave one picture, while real observations of soil formation in disturbed territories gave a completely different picture. The fact that the theoretical and historical rate of soil formation and actually observed now differ significantly,

Therefore, I decided to spend some time studying the views of official science on how the mountain systems of the Northern and Southern Cordilleras were formed, not doubting that I would find there either further clues in favor of my version, or some problematic places that would indicate the fact that the representatives of official science only pretend that they have already explained everything and figured out everything, while there are still plenty of questions and blank spots in their theories, which means that the hypothesis of a global cataclysm put forward by me and the consequences observed after it quite has the right to exist.

Today, the dominant theory of the formation of the Earth's appearance is the [theory of "Plate Tectonics"](https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D0%BA%D1%82%D0%BE%D0%BD%D0%B8%D0%BA%D0%B0_%D0%BF%D0%BB%D0%B8%D1%82), according to which the earth's crust consists of relatively integral blocks - lithospheric plates, which are in constant motion relative to each other. What we see on the Pacific coast of South America, according to this theory, is called the "active continental margin." At the same time, the formation of the Andes mountain system (or the Southern Cordillera) is explained by the same subduction, that is, the diving of the oceanic lithospheric plate under the continental plate.

General map of the lithospheric plates forming the outer crust.

<!-- Local
![](Another history of the Earth - Part 1e_files/167166_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167166/167166_900.jpg#resized)

And this diagram shows the main types of boundaries between lithospheric plates.

<!-- Local
![](Another history of the Earth - Part 1e_files/167184_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167184/167184_900.jpg#resized)

We see the so-called "active continental margin" (ACO) on the right side. In this diagram, it is designated as the "convergent boundary (subduction zone)". Hot molten magma from the asthenosphere rises upward through the faults, forming a new young part of the plates, which move away from the fault (black arrows in the diagram). And on the border with the continental plates, oceanic plates "dive" under them and go down into the depths of the mantle.

Some explanations for the terms that are used in this diagram, as well as we can meet in the following diagrams.

**The lithosphere** is the hard shell of the Earth. It consists of the earth's crust and the upper part of the mantle, up to the Asthenosphere, where the velocities of seismic waves decrease, indicating a change in the plasticity of the substance.

**Asthenosphere**- a layer in the upper mantle of the planet, more plastic than neighboring layers. It is believed that in the asthenosphere, the substance is in a molten, and therefore plastic state, which is revealed by the way seismic waves pass through these layers.

**The MOXO** boundary is the boundary at which the nature of the passage of seismic waves changes, the speed of which increases sharply. It was named so in honor of the Yugoslav seismologist Andrei Mohorovich, who first identified it based on the results of measurements in 1909.

If we look at the general section of the structure of the Earth, as it seems today by official science, then it will look like this.

<!-- Local
![](Another history of the Earth - Part 1e_files/167429_900.png#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167429/167429_900.png#resized)


The earth's crust is part of the lithosphere. Below is the upper mantle, which is partly the lithosphere, that is, solid, and partly the asthenosphere, which is in a molten plastic state.

Next comes the layer, which in this diagram is simply labeled "mantle". It is believed that in this layer the substance is in a solid state due to the very high pressure, while the available temperature is not enough to melt it under these conditions.
Beneath the solid mantle is a layer of "outer core" in which the substance is supposed to be in a molten plastic state again. And finally, in the very center is again the solid inner core.

It should be noted here that when you start reading materials on geophysics and plate tectonics, you constantly come across phrases like "possible" and "quite likely." This is explained by the fact that we actually still do not know exactly what and how it works inside the Earth. All these schemes and constructions are exclusively artificial models, which are created on the basis of remote measurements using seismic or acoustic waves, the passage of which is recorded through the inner layers of the Earth. Today, supercomputers are used to simulate the processes that, as the official science suggests, occur inside the Earth, but this does not mean that such modeling allows one to unambiguously "dot all the i's".

In fact, the only attempt to check the consistency of theory with practice was made in the USSR, when the [Kola superdeep well was drilled](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%B2%D0%B5%D1%80%D1%85%D0%B3%D0%BB%D1%83%D0%B1%D0%BE%D0%BA%D0%B0%D1%8F_%D1%81%D0%BA%D0%B2%D0%B0%D0%B6%D0%B8%D0%BD%D0%B0) in 1970.[](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%B2%D0%B5%D1%80%D1%85%D0%B3%D0%BB%D1%83%D0%B1%D0%BE%D0%BA%D0%B0%D1%8F_%D1%81%D0%BA%D0%B2%D0%B0%D0%B6%D0%B8%D0%BD%D0%B0)... By 1990, the well had reached 12,262 meters in depth, after which the drill string broke off and drilling was stopped. So, the data that was obtained during the drilling of this well contradicted theoretical assumptions. It was not possible to reach the basalt layer, sedimentary rocks and fossils of microorganisms were encountered much deeper than they should have been, and methane was found at depths where no organic matter should in principle be present, which confirms the theory of non-biogenic origin of hydrocarbons in the bowels of the Earth. Also, the actual temperature regime did not coincide with that predicted by theory. At a depth of 12 km, the temperature was about 220 degrees C, while in theory it should have been around 120 degrees C, that is, 100 degrees lower. [Article about the well](https://lenta.ru/articles/2016/04/04/geo/)

But back to the theory of plate movement and the formation of mountain ranges along the western coast of South America from the point of view of official science. Let's see what oddities and inconsistencies are present in the existing theory. Below is a diagram in which the active continental margin (ACO) is indicated by the number 4.

<!-- Local
![](Another history of the Earth - Part 1e_files/197718_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/197718/197718_900.jpg#resized)


I took this image, along with several subsequent images, from the lecture materials of M.V. Lomonosov, Doctor of Geological and Mineralogical Sciences, [Ariskin Alexei Alekseevich](http://istina.msu.ru/profile/AlexeyA/), a teacher at the Geological Faculty of Moscow State University. [The complete file can be found here](http://geo.web.ru/Lectures/Ariskin/lecture_27/lecture_27.pdf). [The general list of materials for all lectures is here](http://geo.web.ru/db/msg.html?mid=1172173).

Pay attention to the edges of the oceanic plates, which bend and go deep into the Earth to a depth of about 600 km. Here is another diagram from the same place.

<!-- Local
![](Another history of the Earth - Part 1e_files/167701_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167701/167701_900.jpg#resized)

Here, too, the edge of the plate bends down and goes to a depth of more than 220 km beyond the boundary of the scheme. Here is another similar picture, but from an [English-language source](http://www.unalmed.edu.co/~rrodriguez/PlateTectonics/plate_tectonics.htm) .

<!-- Local
![](Another history of the Earth - Part 1e_files/167955_800.gif#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/167955/167955_800.gif#resized)

And again we see that the edge of the oceanic plate bends down and descends to a depth of 650 km.

How do we know that these solid plates really do bend down at the edges? According to seismic data, which record anomalies in these zones. Moreover, they are recorded at adequately large depths. Here is what is [reported](https://ria.ru/science/20130403/930856040.html) about this [in a note](https://ria.ru/science/20130403/930856040.html) on the "RIA Novosti" portal.

> "The largest mountain range in the world, the Cordillera of the New World, may have formed as a result of the sinking of three separate tectonic plates beneath North and South America in the second half of the Mesozoic era," geologists say in an article published in the journal Nature.

> ...

> Karin Zigloch of Ludwig Maximilian University in Munich, West Germany, and Mitchell Michalinuk, of the British Columbia Geological Survey in Victoria, Canada, have figured out some of the details of this process by enlightening rocks in the upper mantle beneath the Cordilleras in North America as part of the USArray project.

> Zigloch and Michalinuk theorized that the mantle may contain traces of ancient tectonic plates that plunged under the N American tectonic plate during the formation of the Cordillera. According to the scientists' plan, the "remains" of these plates should have been preserved in the mantle in the form of inhomogeneities, clearly visible for seismographic instruments. To the surprise of geologists, they managed to find three large plates at once, the remains of which lay at a depth of 1-2 thousand kilometers.

> One of them - the so-called Farallon plate - has long been known to scientists. The other two were not previously distinguished, and the authors of the article named them Angayuchan and Meskalera. According to the calculations of geologists, Angayuchan and Mescalera were the first to submerge under the continental platform about 140 million years ago, laying the foundations of the Cordillera. They were followed by the Farallon plate, which split into several pieces 60 million years ago, some of which are still sinking."

And now, if you haven't seen it yourself, I will explain what is wrong with these diagrams. Pay attention to the temperatures shown in these diagrams. In the first diagram, the author somehow tried to get out of the situation, so his isotherms at 600 and 1000 degrees bend downward following the bent plate. But on the right we already have isotherms with temperatures up to 1400 degrees. Moreover, over a noticeably colder stove. I wonder how the temperature in this zone above the cold plate is heated to such a high temperature? After all, the hot core that can provide such heating is actually at the bottom. In the second diagram, from an English-language resource, the authors did not even begin to invent something, they just took and drew a horizon with a temperature of 1450 degrees C, which a plate with a lower melting point calmly breaks through and goes deeper. At the same time, the melting temperature of the rocks that make up the downward bending oceanic plate is in the range of 1000-1200 degrees. So why didn't the bent down end of the plate melt?

Why, in the first diagram, the author needed to pull up a zone with a temperature of 1400 degrees C and above, it is just well understood, since it is necessary to somehow explain where volcanic activity comes from with outflowing flows of molten magma, because the presence of active volcanoes along the entire South Ridge - The Cordillera - is an indisputable fact. But the downward curving end of the oceanic plate will not allow hot flows of magma to rise from the inner layers, as shown in the second diagram.

But even if we assume that the hotter zone was formed due to some kind of lateral hotter magma flow, then the question still remains as to why the end of the plate is still solid? It did not have time to heat up to the required melting temperature? Why didn't it have time? What is our speed of movement of lithospheric plates? We look at the map obtained from measurements from satellites.

<!-- Local
![](Another history of the Earth - Part 1e_files/168203_600.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/168203/168203_600.jpg#resized)

At the bottom left there is a legend, which indicates the speed of movement in cm per year! That is, the authors of these theories want to say that those 7-10 cm that have entered due to this movement do not have time to heat up and melt in a year?

And this is not to mention the strangeness, which was pointed out by A. Sklyarov in his work ["The Sensational History of the Earth"](http://lah.ru/text/sklyarov/siz-web/siz.htm) (see "Scattering continents"), which consists in the fact that the Pacific plate moves at a speed of more than 7 cm per year, plates in Atlantic Ocean with a speed of only 1.1-2.6 cm per year, which is due to the fact that the ascending hot magma flow in the Atlantic Ocean is much weaker than a powerful "plume" in the Pacific Ocean.

<!-- Local
![](Another history of the Earth - Part 1e_files/168546_600.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/168546/168546_600.jpg#resized)

But the same measurements from satellites show that South America and Africa are moving away from each other. At the same time, we do not record any ascending flows under the center of South America, which could somehow explain the actually observed movement of the continents.

Or maybe the real reason for all the actually observed facts is completely different?

The edges of the plates actually went deep into the mantle and yet have not melted because this happened relatively recently, not tens of millions of years ago. It happened during the catastrophe I am describing when a large object broke through the Earth. That is, these are not the consequences of a slow sinking of the edges of the plates by several centimeters a year, but the rapid catastrophic indentation of fragments of continental plates under the influence of shock and inertial waves, which simply drove these fragments inside, as it drives ice floes into the bottom on rivers during a stormy ice drift, placing them on the edge and even turning them over.

Yes, and a powerful hot flow of magma in the Pacific Ocean can also be the remnant of the flow that should have appeared inside the Earth after the breakdown and burning of the channel during the passage of the object through the inner layers.

[*Continuation*](http://mylnikovdm.livejournal.com/224787.html)

[Next Part (English)](./another-history-of-the-earth-part-1e2.html)

© All rights reserved. The original author retains ownership and rights.

