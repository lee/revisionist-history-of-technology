title: Media of the Past - Part 2
date: 2014-04-01
modified: Thu 14 Jan 2021 12:16:28 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/28918.html
originally: Media of the Past - Part 2.html
local: Media of the Past - Part 2_files/
summary: What's the point of putting so much bronze on yourself? Of course, even today we make all sorts of decorations out of old CDs. They're  round and shiny and free. And it is a pity to throw them out. And it is possible to decorate something creatively. 
status: published

### Translated from:

[https://pro-vladimir.livejournal.com/28918.html](https://pro-vladimir.livejournal.com/28918.html)

**Russian:**

Бронза и DVD, что между ними может быть общего?

[http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

**English:**

Bronze and DVD, what could the two have in common? [http://pro-vladimir.livejournal.com/24561.html](http://pro-vladimir.livejournal.com/24561.html)

**Russian:**

Носители информации прошлого.

**English:**

Media of the past. [http://pro-vladimir.livejournal.com/28598.html](http://pro-vladimir.livejournal.com/28598.html)

**Russian:**

Далее отсюда: [http://www.ebay.co.uk/itm/GENUINE-INNER-MONGOLIA-or-CHINESE-Shamans-TOLI-MELONG-BRONZE-MIRROR-/400291861682](http://www.ebay.co.uk/itm/GENUINE-INNER-MONGOLIA-or-CHINESE-Shamans-TOLI-MELONG-BRONZE-MIRROR-/400291861682)

**English:**

Further from here: [http://www.ebay.co.uk/itm/GENUINE-INNER-MONGOLIA-or-CHINESE-Shamans-TOLI-MELONG-BRONZE-MIRROR-/400291861682](http://www.ebay.co.uk/itm/GENUINE-INNER-MONGOLIA-or-CHINESE-Shamans-TOLI-MELONG-BRONZE-MIRROR-/400291861682)

<!-- Local
[![](Media of the Past - Part 2_files/P1010187.html)](Media of the Past - Part 2_files/P1010187.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010187.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010187.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010186.html)](Media of the Past - Part 2_files/P1010186.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010186.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010186.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010180.html)](Media of the Past - Part 2_files/P1010180.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010180.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010180.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010179.html)](Media of the Past - Part 2_files/P1010179.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010179.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010179.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010188.html)](Media of the Past - Part 2_files/P1010188.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010188.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010188.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010211.html)](Media of the Past - Part 2_files/P1010211.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010211.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010211.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010200.html)](Media of the Past - Part 2_files/P1010200.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010200.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010200.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010201.html)](Media of the Past - Part 2_files/P1010201.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010201.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010201.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010195.html)](Media of the Past - Part 2_files/P1010195.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010195.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010195.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010196.html)](Media of the Past - Part 2_files/P1010196.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010196.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010196.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010205.html)](Media of the Past - Part 2_files/P1010205.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010205.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010205.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010208.html)](Media of the Past - Part 2_files/P1010208.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010208.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010208.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010198.html)](Media of the Past - Part 2_files/P1010198.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010198.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010198.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010199.html)](Media of the Past - Part 2_files/P1010199.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010199.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010199.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010189.html)](Media of the Past - Part 2_files/P1010189.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010189.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010189.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010190.html)](Media of the Past - Part 2_files/P1010190.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010190.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010190.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010191.html)](Media of the Past - Part 2_files/P1010191.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010191.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010191.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010192.html)](Media of the Past - Part 2_files/P1010192.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010192.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010192.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010203.html)](Media of the Past - Part 2_files/P1010203.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010203.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010203.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/P1010204.html)](Media of the Past - Part 2_files/P1010204.jpg)
-->

[![](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010204.jpg#clickable)](http://i589.photobucket.com/albums/ss334/Buddha-love/APRIL%20New%20camera/P1010204.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/Shaman-Coat-1.png#clickable)](Media of the Past - Part 2_files/Shaman-Coat-1.jpg)
-->

[![Image no longer available](http://i21.photobucket.com/albums/b256/spiritual-sky/Jan%2008/Shaman-Coat-1.jpg#clickable)](http://i21.photobucket.com/albums/b256/spiritual-sky/Jan%2008/Shaman-Coat-1.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/88_20shaman20costume.html)](Media of the Past - Part 2_files/88_20shaman20costume.jpg)
-->

[![](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/88_20shaman20costume.jpg#clickable)](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/88_20shaman20costume.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/Chinesemirrorshaman.html)](Media of the Past - Part 2_files/Chinesemirrorshaman.jpg)
-->

[![](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/Chinesemirrorshaman.jpg#clickable)](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/Chinesemirrorshaman.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/shamanscostume1.html)](Media of the Past - Part 2_files/shamanscostume1.jpg)
-->

[![](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/shamanscostume1.jpg#clickable)](http://i21.photobucket.com/albums/b256/spiritual-sky/PICS%20FOR%20EBAY/shamanscostume1.jpg)

**Russian:**

Украшение одежды позабавило. Тяжеловат нарядик то. Да и смысл столько бронзы на себя навешивать? Да еще и просто зеркалов. Это не шаман-монах тогда получается, а шарик зеркальный с дискотеки клубной. Мы конечно и нынче из старых CD устраиваем всякие разные украшательства. Ну кругленькое такое, блестит и даром. И выкидывать жалко. И можно чего оформить креативненько.

**English:**

The embellishment on the outfit is amusing. It's a bit heavy, isn't it? What's the point of putting so much bronze on yourself? He must be dressed in mirrors. He was not a shaman-monk then, but a disco ball-mirror. Of course, even today we make all sorts of decorations out of old CDs. They're round and shiny and free. And it is a pity to throw them out. And it is possible to decorate something creatively.

**Russian:**

Можно конечно допустить, что так выглядела фильмотека наиболее ценных дисков. Чтобы всегда под рукой. Ну народ носит же порой с собой внешние накопители. В данном случае они были вот такие.

**English:**

Of course, one can assume that this was how the most valuable disc library looked. To keep it on hand at all times. Well, people sometimes carry around external drives. In this case, they were like this.

**Russian:**

далее отсюда: [http://jd.cang.com/727721.html](http://jd.cang.com/727721.html)

**English:**

From here: [http://jd.cang.com/727721.html](http://jd.cang.com/727721.html)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_010.jpeg#clickable)](Media of the Past - Part 2_files/2011120320361239336844.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320361239336844.jpg#clickable)](http://estimation.cang.com/201112/2011120320361239336844.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_012.jpeg#clickable)](Media of the Past - Part 2_files/2011120320362035072769.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320362035072769.jpg#clickable)](http://estimation.cang.com/201112/2011120320362035072769.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_006.jpeg#clickable)](Media of the Past - Part 2_files/2011120320362410255208.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320362410255208.jpg#clickable)](http://estimation.cang.com/201112/2011120320362410255208.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_009.jpeg#clickable)](Media of the Past - Part 2_files/2011120320362509015818.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320362509015818.jpg#clickable)](http://estimation.cang.com/201112/2011120320362509015818.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_005.jpeg#clickable)](Media of the Past - Part 2_files/2011120320362590719769.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320362590719769.jpg#clickable)](http://estimation.cang.com/201112/2011120320362590719769.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zbQlrJ.jpeg#clickable)](Media of the Past - Part 2_files/2011120320362787027387.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320362787027387.jpg#clickable)](http://estimation.cang.com/201112/2011120320362787027387.jpg)

**Russian:**

Не так изящно выглядит как некоторые бронзовые образцы. но тем не менее. У нас как бы нынче тоже материалы дисковых носителей разные.

**English:**

Not as graceful as some of the bronze specimens, but still. We sort of have different disc media materials nowadays too.

**Russian:**

отсюда: [http://jd.cang.com/676296.html](http://jd.cang.com/676296.html)

**English:**

From here: [http://jd.cang.com/676296.html](http://jd.cang.com/676296.html)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_002.jpeg#clickable)](Media of the Past - Part 2_files/2011090209515953419746.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209515953419746.jpg#clickable)](http://estimation.cang.com/201109/2011090209515953419746.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_004.jpeg#clickable)](Media of the Past - Part 2_files/2011090209522215642262.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209522215642262.jpg#clickable)](http://estimation.cang.com/201109/2011090209522215642262.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_003.jpeg#clickable)](Media of the Past - Part 2_files/2011090209510728224332.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209510728224332.jpg#clickable)](http://estimation.cang.com/201109/2011090209510728224332.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_008.jpeg#clickable)](Media of the Past - Part 2_files/2011090209522061274689.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209522061274689.jpg#clickable)](http://estimation.cang.com/201109/2011090209522061274689.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_011.jpeg#clickable)](Media of the Past - Part 2_files/2011090209522981223074.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209522981223074.jpg#clickable)](http://estimation.cang.com/201109/2011090209522981223074.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d3NJ74NNHebhI04MrF25N7SEPY7skUI0_zb_007.jpeg#clickable)](Media of the Past - Part 2_files/2011090209520056028220.jpg)
-->

[![](http://estimation.cang.com/201109/2011090209520056028220.jpg#clickable)](http://estimation.cang.com/201109/2011090209520056028220.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d6GHJIy9af5niyBGTsvB6Q28AIgqmXPPFZaOGZk.jpeg#clickable)](Media of the Past - Part 2_files/1385347957-565344186.jpg?v=1385347958)
-->

[![](http://pic.pimg.tw/dj3ctungjeng/1385347957-565344186.jpg?v=1385347958)](http://pic.pimg.tw/dj3ctungjeng/1385347957-565344186.jpg?v=1385347958)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d8Pj4ccDauc5He_b60La_lbi4OFZcFq1SQk67aQ.jpeg#clickable)](Media of the Past - Part 2_files/20110527153656808.jpg)
-->

[![](http://www.liuliart.cn/Article/UploadFiles/201105/20110527153656808.jpg#clickable)](http://www.liuliart.cn/Article/UploadFiles/201105/20110527153656808.jpg)

<!-- Local
[![](Media of the Past - Part 2_files/dr3kVp9MaRMDeJ6wTjN9d8Pj4ccDauc5He_b60La_lbi4OFZcFq1SQk_002.jpeg#clickable)](Media of the Past - Part 2_files/20110527153538924.jpg)
-->

[![](http://www.liuliart.cn/Article/UploadFiles/201105/20110527153538924.jpg#clickable)](http://www.liuliart.cn/Article/UploadFiles/201105/20110527153538924.jpg)

**Russian:**

А вообще их реально много, что достаточно доступны для покупки и продажи. [http://www.ebay.co.uk/bhp/chinese-bronze-mirror](http://www.ebay.co.uk/bhp/chinese-bronze-mirror)

**English:**

And there are actually a lot of these that are quite affordable to buy and sell. [http://www.ebay.co.uk/bhp/chinese-bronze-mirror](http://www.ebay.co.uk/bhp/chinese-bronze-mirror)

© All rights reserved. The original author retains ownership and rights.
