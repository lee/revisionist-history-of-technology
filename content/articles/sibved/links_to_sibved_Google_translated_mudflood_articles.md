title: Evidence for Mudflood - Sibved
date: 2021-07-23 00:02
modified: 2022-04-09 20:04:51
category: 
tags: mudflood; catastrophe; links
slug: 
authors: sibved
summary: Links to original articles via Google translate to force translation into English. Each page renders in the original language, then switches to English after a few seconds.
status: published
local: 
from: 
originally: all_sibved.md

[The Flood - How it Happened](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/179607.html)

[A Real Catastrophe of the Past Thrown into the Future and Declared Fiction](https://translate.google.com/website?                                   sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://5elena4.livejournal.com/21604.html) Possibly based on English-language Camille Flamerion 'fiction' at: [https://www.gutenberg.org/files/57489/57489-0.txt](https://www.gutenberg.org/files/57489/57489-0.txt)

[Cosmic Dust Fallout](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/206173.html)

[Mudflooded Buildings - Excavations - Part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/239119.html)

[Mudflooded Buildings - Excavations - Part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/202126.html)

[Mudflooded Buildings - Excavations - Part 2 again](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/251287.html)

[Mudflooded Buildings - Part 3](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/210104.html)

[Mudflooded Buildings - Part 4](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/212959.html)

[Mudflooded Buildings - Part 5](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/217594.html)

[Mudflooded Buildings - Part 6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/217760.html)

[Mudflooded Buildings - Part 7](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/222543.html)

[Mudflooded Buildings - Part 8](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/226195.html)

[Mudflooded Buildings - Part 9](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/229323.html)

[Mudflooded Buildings - Part 10](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/231965.html)

[Mudflooded Buildings - Part 11](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/236909.html)

[Mudflooded Buildings - Part 12](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/241015.html)

[Mudflooded Buildings - Part 13](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/252235.html)

[Mudflooded Buildings - Part 14](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/256111.html)

[Mudflooded Buildings - Part 15](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/259382.html)

[Mudflooded Buildings - Part 16](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/261174.html)

[Mudflooded Buildings - Part 17](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/263950.html)

[Mudflooded Buildings - Part 18](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/266841.html)

[Mudflooded Buildings - Part 19](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/267634.html)

[Mudflooded Buildings - Part 20](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/268192.html)

[Mudflooded Buildings - Part 21](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/268390.html)

[Mudflooded Buildings - Objections and Explanations](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/265419.html)

[Mudflooded Chelyabinsk](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/217962.html)

[Mudflooded Moscow](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/215780.html)

[Mudflooded Novosibirsk](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/214528.html)

[Non-Cultural Layers of Krasnoyarsk](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/261535.html)

[About Mudflooded Cities - Irkutsk - Part 1](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/206719.html)

[About Mudflooded Cities - Irkutsk - Part 2](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/206966.html)

[Ancient Roads Covered with Clay](https://sibved-livejournal-com.translate.goog/325269.html?utm_source=3userpost&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Heart of Darkness - The London Dungeon](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://nat-geo.ru/science/serdtse-   tmy-podzemelya-londona/) (Mainstream Russian-language publication laced with clues for London mudflood researchers)

[Construction of the Paris Metro in Pre-Existing Old Tunnels](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.  livejournal.com/209886.html)

[Flood traces in Spain. A destroyed palace found at a depth of 6 meters](https://sibved-livejournal-com.translate.goog/323680.html?utm_source=3userpost&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[Ancient Buildings of India. Excavations at 6m depth](https://sibved-livejournal-com.translate.goog/323238.html?utm_source=3userpost&_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[People and Giants](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/249316.html)

[Doors for Giants](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/218273.html)

[The Demise of the Giants](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://sibved.livejournal.com/250413.html)

