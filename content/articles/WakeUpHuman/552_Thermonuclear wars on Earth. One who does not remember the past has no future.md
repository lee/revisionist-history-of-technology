title: Thermonuclear wars on Earth. One who does not remember the past has no future
date: 2015-04-07 08:17
modified: Thu 14 Jan 2021 07:20:02 GMT
category: 
tags: thermonuclear war; catastrophe
slug: 
authors: WakeUpHuman
summary: The respected Alexey Kungurov says that from about the 14th - 15th centuries, there was thermonuclear war on our planet. It was only occasionally interrupted for short periods of time. He demonstrated several nuclear craters. He mentioned the absence of old natural forests on the territory of almost the entire planet.
status: published
originally: 552_Thermonuclear wars on Earth. One who does not remember the past has no future.html
local: 552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/

### Translated from:

[http://wakeuphuman.livejournal.com/552.html](http://wakeuphuman.livejournal.com/552.html)

> Why can't 99% of the people on the planet say their name exactly, nor the last name of their great-great-grandparents? It's a curious fact.

By adulthood, every person living on this wonderful blue ball accumulates a baggage of unanswered questions about the surrounding world, phenomena, events, and historical monuments. Due to the fact that most people simply do not have time to find the right answers due to a busy work schedule, family, and so on, it is almost impossible to find the answer to the question themselves. And a person is satisfied with the official interpretation, even if it is crude and contradictory. So I just accumulated baggage of different interesting facts, focus our vision on a daily basis, such as Alexander column, Babolovskaya bath and St. Isaac's Cathedral in St. Petersburg, the Pyramids in Egypt, Pompeii column in Alexandria, megaliths of Peru, Baalbek, etc, there is no end to the number.

All these objects of the past are united by one remarkable fact - they cannot be created in our modern time. A time of oil and gas and nuclear energy. It is not possible for any money to replicate them due to the absence of the necessary technologies and equipment. Pictures of Montferrand, in which he depicted peasants dressed in rags and bast shoes, who simply move a 600-ton conical column along the ground, sometimes uphill, load it on a longboat, float it along the Gulf of Finland, whose depth is less than 1 meter, unload it with their hands and set it on a pedestal several meters high in 1 hour and 45 minutes with the help of gates, cause only a smile. Cyborgs, no less:

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3619_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3619_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/3619/3619_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/3619/3619_original.jpg)

For example, estimate how the Soviet Union installed a lunar rocket N1 with a dry mass of only 208 tons on its launchpad. It was three times lighter than the pillar of Alexandria:

[Youtube video removed]

Pay attention to the 2 powerful electric locomotives that carry the rocket and the hydraulics, which brought the rocket to a vertical position.

One can't help but conclude that the builders of the 17th and earlier centuries had a higher technical level. But the question arises: just where, if it existed, did the entire production base of the ancient builders go? Where is the infrastructure? And for a long time this question drove any person, including me,  into a corner, interrupting the logical chain of thought.

Until one day, I watched a video by the respected Alexey Kungurov, in which he says that on our planet since about the 14th - 15th centuries, there was a thermonuclear war, only occasionally interrupted for short periods of time. In the video, he demonstrated several nuclear craters detected via the Google maps service. He mentioned the absence of old natural forests on the territory of almost the entire planet. All the forests are young, most of them are planted artificially, in neat rows. And here's where the logic comes in. There were technologies, factories, and more advanced energy, but it disappeared as a result of the global war. And the remnants of the former infrastructure were pulled apart by descendants thrown into the feudal regime.

I decided to double-check these - for me - unthinkable statements, and what I found made me rethink everything about our history. We live in an artificial information matrix, a deception that is three times embedded in itself. And we need to figure this out.

Now I will show you a couple of the most odious facts about the use of super-powered weapons in Africa. We are interested in two objects: the Eye of the Sahara and Lake Victoria:

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/8330_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/8330/8330_original.jpg#resized)

I will make a small remark explaining the differences between the consequences of a large asteroid falling to the earth's surface and the consequences of a thermonuclear explosion.

1. The fall of an asteroid will almost always occur at different angles to the surface of the Earth. And at different speeds. It is quite possible that an asteroid will overtake the Earth, catching up with it, having only a small advantage in speed. Given this, the impact crater will rarely have a round shape. Mostly ellipsoid, elongated. Around such a crater, there may be cracks in the earth's crust on one side and piles of soil or rock on the other. An asteroid has a huge kinetic energy, which it transmits to the earth's crust as it goes deeper.

2. At the asteroid impact site, the temperature will rise only locally and by a few thousand or tens of thousands of degrees. There will be no melting of sand and rocks within a radius of several kilometers around, not even close. The temperatures are not the same. Look for videos on YouTube about testing armor-piercing tank tungsten shells. They are fired at the armor at a speed of 1.6 km per second. At the moment of impact, everything looks more than modest. No flashes.

3. Nuclear/thermonuclear missile/tactical special ammunition also approaches the surface at different angles. But, first, it has a low mass, and secondly, even when blasting with some sinking into the ground, and even more so when ground or air blasting completely loses mass, as it evaporates. The temperature at the epicenter is hundreds of millions of degrees. A real mini-Sun. The shock wave forms a uniformly expanding sphere that almost always forms a circular footprint. Sometimes slightly oval. There is such a thing as the resistance of the soil. But most importantly, around the stone, brick, sand will be very badly burned. Different types of stone acquire different colors. From brown, red-brown to black shiny color. Google the term tektites.

Now following the saying: "don't believe your ears, believe your eyes", we will conduct simple research:

### Eye Of The Sahara.

The diameter is 30 kilometers. Corresponds to ammunition with a capacity of about 200-250 megatons. If this is the site of a thermonuclear explosion, then the rocky area around it should be melted. Checking:

Using the Google Chrome browser, go to [https://maps.google.com](https://maps.google.com), entering coordinates into the search:
21.129472, -11.394238

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1249_900.png#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1249_original.png)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/1249/1249_900.png#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/1249/1249_original.png)

At the bottom of the map, Chrome will show previews of photos taken in the area of this crater. Let's look at some of them, often made at a distance of tens of kilometers from the epicenter.

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1441_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1441_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/1441/1441_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/1441/1441_original.jpg)

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1556_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1556_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/1556/1556_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/1556/1556_original.jpg)

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1919_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/1919_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/1919/1919_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/1919/1919_original.jpg)

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/2186_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/2186_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/2186/2186_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/2186/2186_original.jpg)

It is clearly visible that huge territories have been burned. In the first photos,  the bulldozer removed the top layer of burned stones while making the road, and under them a layer of light stone was revealed. In other photos, you can see that many of the stones on the upper side are fused, and on the lower side have a light shade, which clearly indicates a powerful radiation in all the spectra that came from the same direction. Obviously, there is no need to comment.

Looking further, the city destroyed by this explosion was called Hoden. I learned this from old maps of Africa, of which there are many on the Internet. The old maps were actually quite accurate. Links to maps are provided at the end of the article so that you can double-check them yourself.

### Lake Victoria

Moving on to lake Victoria:

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3066_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3066_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/3066/3066_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/3066/3066_original.jpg)

The lake's surroundings are unusual. Let's assume that this is the crash site of a large asteroid. Why not? :) The arrow indicates the direction of its movement before it hit the surface. The yellow line encircles the horseshoe-shaped lakes that were formed as a result of the earth's crust breaking. The red-circle shows an area of blistering to the surface. The green rectangle outlines lake Nyasa. Remember it.

Then [go to Wikipedia: Lake Victoria](https://ru.wikipedia.org/wiki/%D0%92%D0%B8%D0%BA%D1%82%D0%BE%D1%80%D0%B8%D1%8F_(%D0%BE%D0%B7%D0%B5%D1%80%D0%BE))

Pay attention to the name 'Victoria'. In English it means Victory. OK. The lake is huge - its greatest length is 320 km, and its greatest width is 274 km.

> "After the construction of the Owen Falls dam in 1954, the lake was turned into a reservoir"

This means that the water level has become higher, thereby deforming the original shape and flooding the edges. If you wanted to hide the fact that an asteroid fell, would you do the same? Further: 

> "The British Explorer John Henning Speke discovered the lake in 1858 and named it after Queen Victoria." 

Date 1858. 200 years before that, both Americas were already fully discovered and successfully colonized, but in the fertile Africa, which is located near the Anglo-Saxons, there was no lake known with the size of 300 by 300 km? Really? Let's check that by using the data of the Anglo-Saxons themselves...

In 1768, the encyclopedia Britannica was published. The largest encyclopedia at that time, with a detailed map of the world. Let's look at the English map of Africa from 1768, that is, created 90 years before the "discovery" of lake Victoria:

Source: [Britannica.com](http://blogs.britannica.com/2013/05/britannica1768-africa/) 

Note: the map on the link above was blocked. But it is available at this link on the Britannica website: [https://www.britannica.com/media/full/1833131/182057](https://www.britannica.com/media/full/1833131/182057)

The image is clickable:

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3148_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3148_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/3148/3148_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/3148/3148_original.jpg)

And what do we see? We see that Lake Nyasa, which we remembered earlier, is present. And in place of Victoria, not a white unexplored area, but the Nile basin with a couple of cities. One of which is called Sunguard. It turns out that 1858 is not the year of the discovery of this lake. This is the year this crater was formed. Give or take a few years.

Re-check the version using maps of different countries (at the same time, keep an eye on the place where the Eye of the Sahara now is):

Cartographer Guillaume Delisle. Carte d'Afrique. Paris: 1722

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3492_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3492_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/3492/3492_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/3492/3492_original.jpg)

English map 1795

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3884_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/3884_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/3884/3884_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/3884/3884_original.jpg)

Abraham Ortelius. Year 1584 

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4226_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4226_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/4226/4226_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/4226/4226_original.jpg)

If you click on the map of Ortelius and open it in high resolution, you can see that this area used to be the Nile basin. There were about 30 cities in this region that later disappeared. I think the earthquake in this region was much more than 10 points. The reader will reasonably ask: and what are these red symbols of cities on the Ortelius map? Are these villages made of reeds? I will explain using the principle of analogy. Find the cities of Alexandria and Cairo on the Ortelius map. They are closer to the mouth of the Nile than where they are now. Then go here [http://www.antique-prints.de](http://www.antique-prints.de) and look at English metallographs of the late 19th century with images of Alexandria and Cairo after the disaster. Antique style typical of the entire planet:

Alexandria:

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4667_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4667_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/4667/4667_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/4667/4667_original.jpg)

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/5800_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/5800/5800_900.jpg#resized)

Plan Of Alexandria

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4873_900.png#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/4873_original.png)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/4873/4873_900.png#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/4873/4873_original.png)

The lighthouse of Alexandria

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/5125_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/5125_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/5125/5125_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/5125/5125_original.jpg)

Pompeian column of granite

<!-- Local
[![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/5526_900.jpg#clickable)](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/5526_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/5526/5526_900.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/5526/5526_original.jpg)

Cairo. Photos from the 19th century. This is the leftovers; the surviving infrastructure. If you read somewhere that these are "colonial" buildings built by good Anglo-Saxons in the era of firewood and coal (ancient buildings in all cities of the planet are usually attributed to the good Anglo-Saxons), remember how many colonial buildings they built in Libya, Iraq, Syria, and so on in the era of oil and gas.

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/6367_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/6367/6367_900.jpg#resized)

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/6564_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/6564/6564_900.jpg#resized)

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/6840_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/6840/6840_900.jpg#resized)

<!-- Local
![](552_Thermonuclear wars on Earth. One who does not remember the past has no future_files/7020_900.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/7020/7020_900.jpg#resized)

It is too early to assert a conclusion, because this article shows only a small fragment of the megazaruba that unfolded on the planet for about a century between the 13th - 15th centuries. While simplistic, we can say that as a result of these wars, the energy of the past was completely lost. It allowed us to process stone products of absolutely exorbitant weight by the standards of today; to build granite cities according to plans that are still striking to today's architects.

It allowed us to sculpt marble statues, the quality of which is still not achievable by modern CNC machines. But it became clear how these statues were made. After this disaster, there were regular wars, the map of the world was redrawn like a calico dress in a women's dormitory. Most of the population died. And in the middle of the 19th century, we started using oil and gas, which allowed us to raise the standard of living a little and increase the population from 1 billion to 7. do you know why we can now produce oil and gas? Because they are underground. They were not mined by those who built megaliths. They were simply not interested in oil and gas as a source of energy.

PS: The answer to the question - why no one remembers - is at the beginning of the article. 99% do not know great-great-grandmothers by chance. In the mid-19th century, the 1% who know everything arranged a generation gap. This is when the intelligent adult urban population were killed in war and concentration camps, and their children fell into the world of boarding schools. Children are like a clean software CD. In the absence of parents, you can roll them out like any new operating system. Without any ideas about the former world order and with a fictional history. In IT, it's called flashing a smaller BIOS".

Translator's note: The comments section to [the original page](https://wakeuphuman.livejournal.com/552.html) contains more supporting imagery and wakeuphuman's additional comment:

>  I have pointed out the 15-16th century as the earliest time for the large-scale application of WMD, which I can prove on indirect grounds. I'll write more in the future. And the rest of you are absolutely right. And even more than that, World War I, World War II - these were nuclear wars. All these stories of our grandmothers about ball lightning come from there. There was no term for what they saw, but they saw a round flash from afar and then heard thunder. So they called it ball lightning. And the sayings? The peasant will not cross himself before it begins to thunder. The rhymes: thunder thunders, the ground shakes, the pop on the chicken rushes, etc.

This suggests a possible answer to the occasionally raised question:

> How were Soviet losses so extraordinarily high at the onset of the Second World War given the known movements of Axis forces?

© All rights reserved. The original author retains ownership and rights.
