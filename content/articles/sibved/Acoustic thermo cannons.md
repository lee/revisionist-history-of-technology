title: Ancient Cannons - Thermoacoustic Sonic Weapons of the Past - or What Jericho Trumpets  Are
date: 2020-11-24
modified: Fri 23 Jul 2021 20:35:48 BST
category: 
tags: cannons, technology
authors: sibved
summary: I don't rule out that the Jericho pipes are the thermoacoustic oscillators operating in resonance. They may well  have been the first cannons
status: published 
local: Acoustic thermo cannons_files/
from: https://zen.yandex.ru/media/sibved24/drevnie-pushki--termoakusticheskoe-zvukovoe-orujie-proshlogo-ili-chto-takoe-ierihonskie-truby-5fbccfef0b4af801492f8e03
originally: Acoustic thermo cannons.md

#### Translated from: 

[https://zen.yandex.ru/media/sibved24/drevnie-pushki--termoakusticheskoe-zvukovoe-orujie-proshlogo-ili-chto-takoe-ierihonskie-truby-5fbccfef0b4af801492f8e03
](https://zen.yandex.ru/media/sibved24/drevnie-pushki--termoakusticheskoe-zvukovoe-orujie-proshlogo-ili-chto-takoe-ierihonskie-truby-5fbccfef0b4af801492f8e03
)

**Russian:**

Многим известно упоминание в Библии об иерихонских трубах. Трубах, которые разрушили стены древнего Иерихона. Это описано в 6-й главе книги Иисуса Навина в Ветхом Завете.

**English:**

Many people know the reference in the Bible to the trumpets of Jericho. The trumpets that tore down the walls of ancient Jericho. This is described in Joshua 6 in the Old Testament.

![](./images/Acoustic thermo cannons_files/1.jpeg#resized)

**Russian:**

Взятие Иерихона. Жан Фуке (1420–1480)

**English:**

The capture of Jericho. Jean Fouquet (1420-1480).

**Russian:**

Иерихон был городом-крепостью на пути в северную Палестину при 40-летнем походе еврейского народа, который вел сначала Моисей, а потом Иисус Навин. Пройти даже многотысячной толпе можно было и ничего не разрушая. Если только с целью перехвата контроля территории. Но об этом Библия умалчивает. И тема статьи не об этом.

**English:**

Jericho was a fortress city on the road to northern Palestine during the 40-year march of the Jewish people, led first by Moses and then by Joshua. It was possible for even a crowd of thousands to get through without destroying anything. If only for the purpose of seizing control of the territory. But the Bible is silent about that. And that's not the topic of the article.

**Russian:**

Еще интересно то, что людей на картинах и гравюрах европейские живописцы изображали вполне обычными средневековыми жителями, не в арабских накидках, повязках, как мы привыкли воспринимать библейские мотивы:

**English:**

It is also interesting that the people in the paintings and engravings of the European painters depicted quite normal medieval inhabitants, not wearing Arabian capes, bandages, as we are accustomed to perceive biblical motifs:

![](./images/Acoustic thermo cannons_files/2.jpeg#resized)

![](./images/Acoustic thermo cannons_files/3.jpeg#resized)

**Russian:**

Кроме труб, толпа носила еще и Ковчег завета. Ящик со скрижалями с десятью заповедями. Толпа изображена как средневековое войско в доспехах, амуниции. Или художники не знали как выглядели тогда представители еврейского народа, либо в одеждах народов не было разницы.

**English:**

In addition to the trumpets, the crowd also carried the Ark of the Covenant. The box of tablets with the Ten Commandments. The crowd is depicted as a medieval army in armor, ammunition. Either the artists didn't know what the Jewish people looked like then, or there was no difference in the clothing of the nations.

![](./images/Acoustic thermo cannons_files/4.jpeg#resized)

**Russian:**

Таскать в таком ящике скрижали – как минимум странно. В свое время были выдвинуты версии, что это что-то типа энергетической установки. А левиты и коэны – жрецы (специалисты) для обслуживания этой установки в специальной одежде с нагрудниками. Откуда установка – отдельный вопрос. Может наследие прежних цивилизаций. Или подарок Богов, как предполагал А.Скляров.

**English:**

Carrying tablets in a box like this is strange, to say the least. At one time there were theories that it was some kind of power plant. Levites and Kohanim were priests (specialists) to maintain this machine, wearing special clothes with breastplates. Where did the installation come from is a separate question. Maybe a legacy of previous civilizations. Or a gift from the Gods, as suggested by A. Sklyarov.

![](./images/Acoustic thermo cannons_files/5.jpeg#resized)

Translation:
Left: Jericho pipes
Right: Ark of the Covenant

**Russian:**

Так как был разрушен Иерихон? Шесть дней толпа ходила вокруг стен, а на седьмой был дан сигнал в трубы и стены рухнули. Предположим, что все так и было. Тогда в этом должно быть физическое явление. Это может быть объяснено явлением резонанса. Но как добиться такого резонанса в каменных стенах? Чем? Что это за трубы?

**English:**

So how was Jericho destroyed? For six days the crowd walked around the walls, and on the seventh day a signal was given into the trumpets and the walls collapsed. Let's assume that this is how it happened. Then there must be a physical phenomenon involved. It can be explained by the phenomenon of resonance. But how does one achieve such resonance in stone walls? With what? What are these pipes?

**Russian:**

В физике известен термоаккустический эффект. Механизм, работающий на этом принципе – двигатель Стирлинга:

**English:**

The thermoacoustic effect is known in physics. The mechanism that works on this principle is the Stirling engine:

![](./images/Acoustic thermo cannons_files/6.jpeg#resized)

**Russian:**

Трубку, закрытую с одной стороны, нагревают у края. Горячий воздух, расширяясь, доходит до зоны охлаждения, сжимается и возвращяется (втягивается обратно). Появляются автоколебания, которые могут приводить в действие даже поршень. А если нет, то будет слышен гул. Думаю, все слышали гул от мощной тяги в буржуйке или дровяной печи.

**English:**

A tube, closed on one side, is heated at the edge. The hot air expands, reaches the cooling zone, is compressed and recirculates (drawn back in). Auto oscillations appear, which can even actuate the piston. And if it doesn't, you will hear a hum. I think everyone has heard the hum of a powerful draft in a wood-burning stove or a wood-burning stove.

**Russian:**

Модель двигателя Стирлинга, да еще с мини-генератором, можно посмотреть здесь:

**English:**

A model of the Stirling engine, and even with a mini generator, can be seen here:

*Translator's note: image apparently missing*

**Russian:**

На эту тему так же можно почитать про трубку Рийке и пламя Хиггинса. Смотрим видео про теорию термоакустического генератора звука:

**English:**

You can also read about the Riecke tube and the Higgins flame on this topic. Watch a video about the theory of the thermoacoustic sound generator:

<!--
[![](./images/Acoustic thermo cannons_files/sound amplification principle of operation.png#clickable)](https://youtu.be/R_S3niPvHHw)
-->

[![Video demonstrates how thermo acoustic tubes produce sound](https://img.youtube.com/vi/R_S3niPvHHw/0.jpg)](https://www.youtube.com/watch?v=R_S3niPvHHw)

*Video demonstrates principle of how thermo acoustic tubes produce sound*

Translation of initial image: 
Top left: Expansion
Bottom left: Flame
Centre: A tube
Right: Sound

**Russian:**

В youtubeмного роликов с демонстрацией звукового эффекта при нагревании колб с помещенными внутрь сетками из металла. Представьте, что у Вас эта «колба» гораздо больших размеров и Вы знаете алгоритм получения звуковых колебаний конкретной частоты. Делали расчеты. Если наложить звук от нескольких таких труб, то получится резонанс – звук усилится настолько, что будет способен разрушить любую стену. А если знать резонансную частоту каменный кладки, при которой камни начнут разваливаться на мелкие части, то энергия «труб» оптимизируется и ее не нужно тратить слишком много.

**English:**

There's a video demonstrating the sound effect of heating flasks with metal grids placed inside. Imagine that you have this "flask" of much larger size and you know the algorithm for producing sound vibrations of a particular frequency. Have done the calculations. If you superimpose the sound from several such pipes, you will get resonance - the sound will intensify so much that it will be able to destroy any wall. And if you know the resonance frequency of the masonry at which the stones will start to fall apart into small pieces, then the energy of the "pipes" is optimized and there is no need to waste too much of it.

**Russian:**

Так вот к чему эта теория? Я не исключаю, что иерихонские трубы – это и есть термоакустические генераторы, работающие в резонансе. В их качестве вполне могли выступать первые пушки:

**English:**

Is that what this theory is about? I don't rule out that the Jericho pipes are the thermoacoustic oscillators operating in resonance. They may well have been the first cannons:

![](./images/Acoustic thermo cannons_files/7.jpeg#resized)

![](./images/Acoustic thermo cannons_files/8.jpeg#resized)

**Russian:**

Осадные пушки Османской империи. Я не думаю, что они стреляли ядрами. Их бы просто разорвало. Обратите внимание на заднюю, зауженную часть. Возможно, это зона нагрева. Чем нагревали? Может быть, костер. А если предположить, что Ковчег завета – это электрогенератор, то вот так:

**English:**

Ottoman siege cannons. I don't think they fired cannonballs. They would have just been blown apart. Note the rear, narrowed part. That's probably the heating zone. Heated with what? Maybe a bonfire. And assuming the Ark of the Covenant is an electrical generator, there you go:

![](./images/Acoustic thermo cannons_files/9.jpeg#resized)

**Russian:**

Для нагрева не нужна большая энергия. Нужна определенная частота тока.

**English:**

You don't need a lot of energy to heat it. It needs a certain frequency of current.

**Russian:**

Я не подкрепляю эту версию расчетами, т.к. на это нужно время, нужно более глубоко погружаться. Это лишь предположение с описанием общего физического принципа, как древние могли разрушать крепости, замки.

**English:**

I don't back up this version with calculations, as that takes time, you need to delve more deeply. This is just a guess with a description of the general physical principle, how the ancients could destroy fortresses, castles.

**Russian:**

Хотя, археологи предполагают, что стены Иерихона разрушило мощное землетрясение:

**English:**

Although, archaeologists suggest that the walls of Jericho were destroyed by a powerful earthquake:

![](./images/Acoustic thermo cannons_files/10.jpeg#resized)

Source: https://history.wikireading.ru/212403

Text says:

> In 1907 and 1909, Professor Ernst Zellin, together with a group of German archaeologists and architects, excavated the ruins of Jericho near Ain es-Sultan. It goes without saying that the main attention was directed to a thorough study of the mysterious walls. The architects measured these amazing city walls and began to look for those weak points of masonry that could collapse. The thickness of the outer wall is approximately 1.5 meters, the inner wall is even 3.5 meters. The distance between these cyclopean walls is about 3 to 4 meters. Could such walls really have fallen? And just from the trumpet sounds, spells with the help of the "ark of the Lord" and the magic number seven? Archaeologists who have taken up methodological research. The results of the study showed that the walls in Jericho actually fell. This was confirmed by huge cracks and the collapse of large parts of the external walls to the outside, and the internal ones to the inside. At the same time, it does not matter much whether the giant earthquake in the Jordan Valley destroyed the walls at The Dead Sea, according to modern architects.

**Russian:**

Картина привычная, как и все древнее:

**English:**

The picture is as familiar as anything ancient:

![](./images/Acoustic thermo cannons_files/11.jpeg#resized)

**Russian:**

Разрушенная крепость, занесенная грунтом. Может здесь позже случилось то, что разрушило и Древний Рим?

**English:**

A ruined fortress covered with soil. Could it be here later that what destroyed ancient Rome also happened?

© All rights reserved. The original author retains ownership and rights.
