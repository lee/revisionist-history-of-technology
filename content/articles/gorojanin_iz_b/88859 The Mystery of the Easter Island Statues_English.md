title: The Mystery of the Easter Island Statues
date: 2021-01-11
modified: 2022-09-17 12:29:36
category:
tags: Easter Island; forged history
slug:
authors: Anton Sizykh
from: https://gorojanin-iz-b.livejournal.com/88859.html
originally: 88859.html
local: 88859_files
summary: From time to time I have made comments in various magazines about the mysterious Easter Island statues and the mystery of their movements around the island, which I have not collected together before now. It's about time for a proper post. I tried to find the earliest photo of the Easter Island icons. Wikipedia has only one early photo, allegedly from 1880...
status: published

#### Translated from:

[https://gorojanin-iz-b.livejournal.com/88859.html](https://gorojanin-iz-b.livejournal.com/88859.html)

Translator's note: All of the images referenced in this site are visible in the original article at the above link.

**Russian:**

Главная тайна истуканов острова Пасхи

**English:**

The great mystery of the Easter Island icons

**Russian:**

*Время от времени делал комментарии в разных журналах о загадочных истуканах острова Пасхи и тайне их перемещений по острову, которые до поры собирал [здесь](http://bit.do/fMJyV). Давно пора сделать нормальный пост.*

**English:**

Made occasional comments in various magazines about the mysterious Easter Island statues and the mystery of their movements around the island, which I collected [here](http://bit.do/fMJyV) until now. It's about time for a proper post.

**Russian:**

Попытался найти самое раннее фото истуканов острова Пасхи.

**English:**

Tried to find the earliest photo of the Easter Island icons.

**Russian:**

В вики есть только одно раннее фото якобы 1880 года.

**English:**

There is only one early photo on the wiki allegedly from 1880.

![Image visible only on original site](https://imgprx.livejournal.net/9da01a61e92ae744240ef83e41c15896952fffbe/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPl5CNcX1FxfbN8g-lLfY8N)

**Russian:**

Которое, бесспорно, делалось в 1922 году, вместе с этим.

**English:**

Which was undoubtedly done in 1922, along with this.

![Image visible only on original site](https://imgprx.livejournal.net/469112af98ccdc2148aa908ce83bf691b77b9472/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP1LdCqq0tp8Sw441lxLct6)

**Russian:**

Первые фото гигантских статуй острова появляются только на рубеже 19-20 веков, хотя открыт он в 18 веке. Это значит только одно: гигантские моаи сделаны незадолго до этого рубежа. В порядке создания истории Easter Island, он же остров Пасхи.

**English:**

The first photos of giant statues of the island appear only at the turn of the 19th-20th centuries, although it was discovered in the 18th century. This means only one thing: the giant moai were made shortly before this milestone. In the order of making history Easter Island, aka Easter Island.

**Russian:**

Первое упоминание о статуях было в книге немецкого автора Беренса, плававшего под командой голландского капитана **Якоба Роггевена** и якобы видевшего истуканов, "Путешествие по южным странам и вокруг света в 1721-1722 гг", изданной в 1737 году, а еще через век и в нашедшемся дневнике самого Роггевена. Но даже маленькой аннотации хватит, чтобы понять: это даже не вброс, а просто фантазия. Простой факт опровергает "свидетельство" - на всех первофото статуи закопаны, а некоторые в таком виде и по сей день.

**English:**

The first mention of the statues was in the book by the German author Behrens who sailed under the command of the Dutch captain **Jacob Roggeven** and allegedly saw statues, "Traveling in the southern countries and around the world in 1721-1722" published in 1737, and another century later in the found diary of Roggeven himself. But even a small annotation is enough to realize: this is not even a throw-in, but a mere fantasy. The simple fact refutes the "testimony" - in all the first photos the statues are buried, and some of them are still buried today.

**Russian:**

*"Больше всего голландцев удивили огромные статуи, высотой до 30 футов и более. Их было очень много, и все они стояли на каменных платформах, островитяне воздавали этим идолам (по мнению голландцев, глиняным) всевозможные почести. Все статуи были задрапированы длинными полосами материи, и эти драпировки свешивались с плеч до самого пьедестала. На головах статуй были водружены корзины, наполненные белыми камнями..."*

**English:**

> "Most of all the Dutch were surprised by the huge statues, up to 30 feet or more in height. There were a great many of them, all standing on stone platforms, and the islanders paid all sorts of homage to these idols (clay idols, according to the Dutch). All the statues were draped with long strips of cloth, and these draperies hung from their shoulders to the very pedestal. On the heads of the statues were placed baskets filled with white stones... "*

**Russian:**

Никто [не смог подтвердить открытия, приписанные Роггевену](http://bit.do/fMJjf), и даже узнать что-либо о жизни этого мореплавателя после описания его путешествия Беренсом. Так что это "свидетельство" - просто одна из фантазий чрезвычайно популярного в 18 веке жанра путешествий и приключений.

**English:**

No one [has been able to confirm the discoveries attributed to Roggeven](http://bit.do/fMJjf), or even learn anything about the life of this navigator after his description of his voyage by Behrens. So this "testimony" is just one of the fantasies of the extremely popular travel and adventure genre in the 18th century.

**Russian:**

А в таком виде открытие Роггевена представлялось современникам.

**English:**

And this is how Roggeven's discovery was presented to his contemporaries.

**Russian:**

> "Идолы острова Пасхи". Воспроизведение оригинальной гравюры (16,3 х 26 см, Баялус и Декарт, сделанные во время экспедиции Якова Роггевена, 1722), сохранилось в коллекции Национального Исторического музея.

**English:**

> "Idols of Easter Island." Reproduction of the original engraving (16.3 x 26 cm, Bayalus and Descartes, made during the expedition of Jacob Roggeven, 1722), preserved in the collection of the National History Museum.

**Russian:**

Имеет ли отношение эта фантазия южных морей к реальным статуям о.Пасхи? Нет, разумеется. На острове нет ни одной сидящей на постаменте статуи.

**English:**

Does this South Seas fantasy have anything to do with the real statues of Easter Island? No, of course not. There is not a single sitting statue on a pedestal on the island.

![Image visible only on original site](https://imgprx.livejournal.net/3e24b856cf2fe9291634ee83c8106e43def8ea2c/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP6Gj0QnhTPnTuGAjMgjF1_)

**Russian:**

Впрочем, одна статуя отдаленно похожа на фантазию Роггевена, но ее почти не показывают публике. Очень неудачная, сделанная не в стилистике остальных статуй, еще и какая-то проволока от каркаса прямо перед глазами торчит.

**English:**

However, one statue is remotely similar to Roggevien's fantasy, but it is almost never shown to the public. It is very unfortunate, not made in the style of the other statues, and there is some wire from the frame sticking out right in front of the eyes.

[![Image visible only on original site](https://imgprx.livejournal.net/bf7323db846a0017d2b58f462258cc218f6fb7f5/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMfEjHb6k0hKQpEcSleeRyR)](http://bit.do/fMJvj)

![Image visible only on original site](https://imgprx.livejournal.net/bcf5b5fbdc25de759fdd2e270bf5304e9c622f73/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP99WmBX-Moh1u5hpLvica7)

**Russian:**

В 1770 году испанский капитан **Фелипе Гонсалес де Ахедо** Википедия на двух кораблях, посланных вице-королем Перу, переоткрыл остров, аннексировал его в пользу испанского короля Карла III, изучил его, описал внешность и занятия местных жителей, составил словарик из ста слов, торжественно подписал с жителями договор об аннексии.

**English:**

In 1770 the Spanish captain **Felipe Gonzalez de Ahedo** Wikipedia on two ships sent by the Viceroy of Peru, rediscovered the island, annexed it in favor of the Spanish King Charles III, studied it, described the appearance and occupation of the locals, compiled a dictionary of a hundred words, solemnly signed with the inhabitants the annexation treaty.

**Russian:**

Есть книга 1903 года издания о путешествии испанского капитана Фелипе Гонсалеса на остров *The Voyage of Captain Don Felipe González to Easter Island. 1770-1.*

**English:**

There is a 1903 edition book about Spanish Captain Felipe González's voyage to Easter Island *The Voyage of Captain Don Felipe González to Easter Island. 1770-1.*

**Russian:**

Дословный перевод [отсюда](http://bit.do/fMJvr) , со стр.181:

**English:**

Verbatim translation [from here](http://bit.do/fMJvr) , from p.181:

**Russian:**

*"Мы убедились, что то, что мы приняли за кусты пирамидальной формы, на самом деле является статуями или изображениями идолов, которым поклоняются туземцы; они сделаны из камня и имеют такую высоту и полноту, что кажутся огромными толстыми колоннами, и, как я впоследствии убедился, рассматривая их и принимая их размеры, все тело состоит из одного блока, а корона или головной убор-из другого; на верхней поверхности последнего имеется небольшая вогнутость, в которую они помещают кости своих умерших, из чего можно заключить, что они служат одновременно идолам и погребальным кострам. Но трудно понять, как они могли установить такие великолепные статуи и поддерживать их должным образом сбалансированными на таком количестве мелких камней, которые помещены в основание или постамент, поддерживающий их большой вес. Материал статуи-очень твердый камень, а потому весомый; попробовав его сам с мотыгой, он ударил по огню: доказательство его плотности. Корона сделана из другого камня, который в изобилии встречается на острове, но я не видел ничего похожего на эту фигуру: она сделана очень грубо. Единственная особенность в конфигурации лица-это грубая выемка для глаз: ноздри довольно имитированы, а рот простирается от уха до уха, о чем свидетельствует небольшая борозда или выемка в камне. Шея имеет некоторое сходство; руки и ноги отсутствуют, и она идет от шеи вниз в виде грубо скроенного туловища. Диаметр короны гораздо больше диаметра головы, на которой она покоится, и ее нижний край выступает значительно за пределы лба фигуры; положение, которое вызывает удивление, что она не падает..."*

**English:**

> "We were persuaded that what we took to be pyramid-shaped bushes were really statues or representations of idols worshipped by the natives; they are made of stone, and are so high and full that they appear to be huge thick columns, and, as I subsequently ascertained by examining them and taking their dimensions, the whole body consists of one block, and the crown or headdress of another; on the upper surface of the latter there is a small concavity into which they place the bones of their dead, from which it may be inferred that they serve both as idols and funeral pyres. But it is difficult to see how they could mount such magnificent statues and keep them properly balanced on so many small stones, which are placed in a base or pedestal that supports their great weight. The material of the statue-very hard stone, and therefore weighty; having tried it himself with a hoe, he struck the fire: a proof of its density. The crown is made of another stone, which is found in abundance on the island, but I have seen nothing like this figure: it is very crudely made. The only peculiarity in the configuration of the face is the rough notch for the eyes: the nostrils are rather imitated, and the mouth extends from ear to ear, as indicated by a small groove or notch in the stone. The neck bears some resemblance; the arms and legs are absent, and it runs from the neck downwards as a roughly cut torso. The diameter of the crown is much larger than that of the head on which it rests, and its lower edge protrudes well beyond the figure's forehead; a position which causes one to wonder that it does not fall... "*

**Russian:**

Это вообще о чем? Пирамидальные статуи, рот от уха до уха, с корзиной костей, и камень искрит от удара железной мотыгой... Автор описания, без сомнений, не видел, не касался, не бил мотыгой по истуканам.

**English:**

What's that all about? Pyramidal statues, mouth from ear to ear, with a basket of bones, and stone sparks from hitting with an iron hoe... The author of the description, no doubt, has not seen, touched, or struck the statues with a hoe.

**Russian:**

В 1772 году Фелипе составил карту острова.

**English:**

In 1772, Felipe made a map of the island.

[![Image visible only on original site](https://imgprx.livejournal.net/4dcd6cf938af26f8c2e15a662844a0a03918f923/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcM_BICY5VCJUgYWe48zU2sz)](http://bit.do/fMJjq)

**Russian:**

На карту нанес три совсем уж условных идола, унеся их в море и не следуя даже собственному описанию.

**English:**

Three quite conventional idols were mapped, taking them out to sea and not even following their own description.

![Image visible only on original site](https://imgprx.livejournal.net/ca09d331f31e85b18a32316f8dd4dfdc9acf2b64/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP6FuytyyekENL7VX_5I_iG)

**Russian:**

Этот рисунок сделал английский художник **Уильям Ходжес**, участник второй экспедиции **Джеймса Кука**, высадившейся на острове 12 марта 1774 года.

**English:**

This drawing was made by the English artist **William Hodges**, a member of the second expedition of **James Cook** that landed on the island on March 12, 1774.

**Russian:**

Нет ни такого места, ни подобных статуй, ясно, что изображения сделаны из головы. Очевидны две идеи, которые стремился передать художник: очень большая древность и наличие шапок. Исследователи, объясняя полную непохожесть места и статуй, осторожно говорят, что художник делал рисунок по памяти.

**English:**

There is no such place or similar statues, it is clear that the images are made from the head. Two ideas that the artist sought to convey are obvious: a very large antiquity and the presence of hats. Researchers, explaining the complete dissimilarity of the place and the statues, cautiously say that the artist made a drawing from memory.

[![Image visible only on original site](https://imgprx.livejournal.net/372e519ec9740e6746e3a125d2eb0a75ea95b11e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPX-maTmU7fccro2LKWbo3k)](http://bit.do/fMJRQ)

[![Image visible only on original site](https://imgprx.livejournal.net/6783dd0afdd9697ade45337cf2556dab704472c3/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOu23yQyOnXojv-IuKRNYev)](http://bit.do/fMJRK)

**Russian:**

Нетрудно заметить, что множество свидетелей и описателей исполинов последующих лет просто брали за основу этот рисунок художника команды Кука и видоизменяли на свой манер.

**English:**

It is not difficult to notice, that set of witnesses and writers of giants of the subsequent years simply took as a basis this drawing of the artist of crew of Cook and modified in the own way.

[![Image visible only on original site](https://imgprx.livejournal.net/d1ee6aa6883b2e76e8234c17c30419ee58f7f40a/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOCGUjrmHdoAQeZgBgq36tH)](http://bit.do/fMJvA)

![Image visible only on original site](https://imgprx.livejournal.net/493f92539611ff7e8aef57da90a7dfe461cb2fb8/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPam71gelgcKK_Uqn3KoMtI)

![Image visible only on original site](https://imgprx.livejournal.net/0a522bddb8f2b224917f625484e734954a1150ad/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNqmKid42PMfQw3l7Bn_gG3)

![Image visible only on original site](https://imgprx.livejournal.net/e6224d19456370398ed13de46a020dae86fa2999/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPaB_6SaYVSMQYIzgYkC1KU)

**Russian:**

Есть и более откровенное свидетельство, исполненное с меньшим художественным мастерством, от капитана второго судна экcпедиции Кука Джона Джильберта. Хранится в Национальном Архиве Соединенного Королевства.

**English:**

There is a more candid testimony, with less artistic skill, from John Gilbert, captain of the second Cook expedition vessel. It is preserved in the National Archives of the United Kingdom.

**Russian:**

Map and sketches of Easter Island from J Gilbert's log of the voyage of HMS Resolution through the Pacific from 1772-5. This image is from the collections of The National Archives*.

**English:**

Map and sketches of Easter Island from J Gilbert's log of the voyage of HMS Resolution through the Pacific from 1772-5. This image is from the collections of The National Archives*.

![Image visible only on original site](https://imgprx.livejournal.net/87ac1a20aeeee9948c0571c994fc7ba494d04328/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNFHGxx-BzXqxJOmDSy69kb)

**Russian:**

Жан-Франсуа Лаперуз. *Путешествие по всему миру на "Буссоли" и "Астролябии"*. 1786 год.

**English:**

Jean-François La Perouse. *Voyage around the world on the Bussolie and the Astrolabe*. 1786.

**Russian:**

*"Самые большие из этих грубо изготовленных бюстов, стоящих на постаментах, которые мы обмерили, насчитывали не более шестнадцати футов шести дюймов в высоту, семь футов шесть дюймов в ширину на уровне плеч, три фута в толщину в средней части, шесть футов в ширину и пять футов в толщину у основания. Эти бюсты, говорю я, вполне могли быть творением народа, ныне населяющего этот остров, и его численность, я полагаю, и без преувеличения, может составлять две тысячи человек".*

**English:**

> "The largest of these crudely made busts, standing on pedestals, which we measured, numbered not more than sixteen feet six inches high, seven feet six inches wide at shoulder level, three feet thick in the middle, six feet wide, and five feet thick at the base. These busts, I say, may well have been the work of the people now inhabiting this island, and their number, I believe, and without exaggeration, may be two thousand. "*

**Russian:**

Снова фантазия.

**English:**

Fantasy again.

**Russian:**

Во-первых, моаи снова на постаментах, отсутствующих в натуре.

**English:**

First, the moai are again on pedestals missing in kind.

**Russian:**

Во-вторых, 16 футов 6 дюймов высоты самой крупной статуи - это 5 метров.

**English:**

Secondly, 16 feet 6 inches tall the largest statue is 5 meters tall.

**Russian:**

Имеется карандашный рисунок с натуры, выполненный 9 апреля 1786 года художником **Дюше де Вансом**, членом команды Ла Перу.

**English:**

There is a pencil drawing from life, made on April 9, 1786, by **Duche de Vans**, a member of the La Perouse team.

[![Image visible only on original site](https://imgprx.livejournal.net/af2855a3c49121fa989341d497b6c6a43a5b41b0/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMzUSHz29dfyiGuNSIQKj1l)](http://bit.do/fMJvE)

**Russian:**

Это явно не про те статуи, что в натуре. Автор в глаза не видел статуи острова и не обмерял самые крупные. Которые, если кто не знает, от 10 до 11 с половиной метров.

**English:**

This is clearly not about those statues in real life. The author hasn't seen the island's statues and hasn't measured the biggest ones. Which, in case you don't know, are from 10 to 11 and a half meters.

**Russian:**

Что особенно интересно, на карту хорошо изученной западной части острова Лаперуз нанес даже отдельные деревья - редкость для острова. Гигантским статуям места не нашел. Открывайте полный размер и любуйтесь.

**English:**

What is especially interesting, on the map of the well-studied western part of the island La Perouse even put individual trees - a rarity for the island. Giant statues found no place. Open the full size and admire.

[![Image visible only on original site](https://imgprx.livejournal.net/2cf905e6e6474e11306bbb9bee123082836cb4ba/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOrPZ2YvBszhztKXI0u6F_e)](http://bit.do/fMJjx)

**Russian:**

В апреле 1804 года капитан **Ю.Ф. Лисянский** стоял несколько дней возле острова Пасхи на корабле "Нева", дожидаясь И.Ф. Крузенштерна на корабле "Надежда". Сам не сходил, только осматривал остров с моря и заметил несколько статуй, а на остров отправил лейтенанта Повалишина, со слов которого дал описание статуй в книге *"Путешествие вокруг света в 1803, 4, 5 и 1806 годах". СПб., 1812. Ч. 1.*

**English:**

In April 1804 Captain **J.F. Lisyansky** stood for several days near Easter Island on the ship "Neva", waiting for I.F. Kruzenshtern on the ship "Nadezhda". He did not go down himself, but looked at the island from the sea and noticed several statues, while he sent lieutenant Pavlishin to the island, from whose words he gave a description of the statues in his book *"Journey Around the World in 1803, 4, 5 and 1806. St. Petersburg, 1812. Ч. 1.*

**Russian:**

Единственная деталь в книге говорит о том, что речь близко не идет о знаменитых моаи. Высота статуй Лисянского-Повалишина три метра, плюс метровые шапки.

**English:**

The only detail in the book says that it is not close to the famous moai. The height of the Lisyansky-Povalishin statues is three meters, plus the meter hats.

**Russian:**

*"Ялик стоял столь близко от берега, что можно было явственно рассмотреть несколько жилищ и камень, из которого составлены ближние монументы или статуи. По указанию Повалишина, они высотою около 13 футов [4 м]. Четвёртую часть их составлял цилиндр, поставленный на головах статуй".*

**English:**

> "The skiff stood so close to the shore that one could clearly see several dwellings and the stone of which the nearby monuments or statues were made. According to Povalishin's instructions, they were about 13 feet [4 m] high. The fourth part of them was a cylinder placed on the heads of the statues. "*

**Russian:**

Русский мореплаватель **О.Е. Коцебу**, знакомый с трудами Кука и Лаперуза, посетил остров в марте 1816 года и не нашел ни одной статуи, несмотря на все старания, а увидел только кучку камней и решил, что это и есть бывшие статуи. О чем честно сообщил в книге *"Путешествие в Южный океан и в Берингов пролив для отъискания северо-восточного морского прохода"*. СПб., 1821. Ч. 1.

**English:**

The Russian navigator **O.E.Kotzebu**, familiar with the works of Cook and La Perouse, visited the island in March 1816 and did not find any statues, despite all his efforts, but saw only a pile of stones and decided that it is the former statues. He honestly reported about that in his book "Journey to the Southern Ocean and Bering Strait to search for the north-east sea passage" *. SPb., 1821. Ч. 1.

![Image visible only on original site](https://imgprx.livejournal.net/d440a87cf4a4a1a396b5a886d227b0c0392f39b3/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOx9gNCvxlFCEKbIHMOHVOz)

![Image visible only on original site](https://imgprx.livejournal.net/f4b3191a9f442deafca331615ac3f1a26364d7d4/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMJ_7ScFHk4MipG0Mk0UnIY)

![Image visible only on original site](https://imgprx.livejournal.net/d3aecfd98d381d3abbfb2e42e5f4a2682e6a8951/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMPsb7R8QYUo4G-rvGdb56r)

**Russian:**

В 1825 году подробно описал остров, рельеф, нравы жителей, с которыми не задалась торговля, капитан корабля "Блоссом", морской офицер, полярник, гидрограф, впоследствие глава морского департамента, контр-адмирал, президент Королевского географического общества **Фредерик Уильям Бичи**. Никаких статуй не видел, разрушились они. Убеждайтесь в книге [Narrative of a voyage to the Pacific and Beering's strait, to co-operate with the polar expeditions: performed in His Majesty's ship Blossom, under the command of Captain F.W. Beechey in the years 1825, 26, 27, 28](http://bit.do/fMJjH), описание острова с 58-й страницы.

**English:**

In 1825 the captain of the ship "Blossom", a naval officer, polar explorer, hydrographer, later the head of the maritime department, Rear Admiral, President of the Royal Geographical Society **Frederick William Beachy** described in detail the island, the relief, the manners of the residents, with whom the trade did not succeed. Didn't see any statues, destroyed them. See the book [Narrative of a voyage to the Pacific and Beering's strait, to co-operate with the polar expeditions: performed in His Majesty's ship Blossom, under the command of Captain F.W. Beechey in the years 1825, 26, 27, 28](http://bit.do/fMJjH), description of the island from page 58.

**Russian:**

В 1837 году французский путешественник, ученый-энциклопедист, океанограф, совершивший три кругосветных плавания, который никогда не был на острове Пасхи, **Жюль Себастьен Сезар Дюмон-Дюрвиль** в книге [Всеобщее путешествие вокруг света, содержащее извлечения из путешествий известнейших доныне мореплавателей. Часть VI](http://bit.do/fMJjU) дал волю фантазии, живописуя огромные древние памятники от 10 до 15 футов (3-4,5 м), крупнейшие - 20 футов (6 м). Любуйтесь.

**English:**

In 1837 a French voyager, encyclopaedist scientist, oceanographer, who had made three voyages around the world, who had never been to Easter Island, **Jules Sébastien César Dumont-Durville** in his book [General voyage around the world, containing extracts from the voyages of hitherto most famous navigators. Part VI](http://bit.do/fMJjU) gave free rein to the imagination, painting huge ancient monuments from 10 to 15 feet (3-4.5 m), the largest - 20 feet (6 m). Admire.

![Image visible only on original site](https://imgprx.livejournal.net/9750c51ab062d5f38493dc75efb7b8ccb57fbfc0/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNw0iXID4bsOTPNTE8lQUqH)

![Image visible only on original site](https://imgprx.livejournal.net/8bc4fcd732c32a69820efa3436884f2245fc0c4f/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcN1tJzQLW48tPfVLAsMohK2)

![Image visible only on original site](https://imgprx.livejournal.net/0b2d0ce2808c7f82082f63aa3676594f6eafdbe0/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO-G4UrsknDDmUQLGvZp-T-)

**Russian:**

Фантазия проиллюстрирована членом экспедиций Дюмона-Дюрвиля Сенсоном.

**English:**

The fantasy is illustrated by a member of the Dumont-Durville Senson Expeditions.

**Russian:**

Как видим, иллюстратор не напрягал фантазию, использовав все тот же рисунок Ходжеса, художника из команды Кука, и перенеся на него портретное описание мэтра.

**English:**

As you can see, the illustrator did not strain his imagination, using the same drawing by Hodges, Cook's team artist, and transferring to it the portrait description of the maestro.

![Image visible only on original site](https://imgprx.livejournal.net/31320786dfc7b22509e40aa6ca18d24e809718a7/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP-vmJjLsrhJkoYUP5_q5FF)

**Russian:**

В 1838 году французский исследователь **Абель Обер дю Пети-Туар** снова увидел статуи, даже не одну, но какие! Пирамидальные из Фелипе Гонсалеса и ни ранее, ни сейчас не виданные красные с белыми шапками. В отчете [Voyage autour du monde sur la frégate "la Vénus" pendant les années 1836-1839. Tome 2](http://bit.do/fMJRC) об острове, начиная с 221 страницы: *"Еще мы заметили на берегу камни в форме пирамид, темного цвета, расставленные равном расстояния друг от друга; были увенчаны белым камнем в форме капители. Рядом с этими своеобразными памятниками виднелось большое количество белых камней, которые иногда казались выстроенными в пределенном порядке. Весьма вероятно, что эти пирамиды, как и эти камни, указывают на места захоронений. На западном побережье, прежде чем прибыть в залив Кука, мы заметили два или три памятника другого жанра, которые представляют собой платформы, на которых поставлены четыре красные статуи с верхушками, покрытыми белыми камнями или капителями".*

**English:**

In 1838 the French explorer **Abel Aubert du Petit-Toire** again saw statues, not even one, but what! Pyramidal ones from Felipe Gonzalez and never before or now seen red ones with white hats. In the report [Voyage autour du monde sur la frégate "la Vénus" pendant les années 1836-1839. Tome 2](http://bit.do/fMJRC) on the island, beginning on page 221: *"We also noticed on the shore stones in the shape of pyramids, of dark colour, spaced at equal distances from each other; were surmounted by a white stone in the shape of a capitol. Next to these peculiar monuments a large number of white stones could be seen, which sometimes seemed to be lined up in a certain order. It is very likely that these pyramids, as well as these stones, indicate burial sites. On the west coast, before arriving at Cook Inlet, we noticed two or three monuments of another genre, which are platforms upon which are placed four red statues with tops covered with white stones or capitals."

**Russian:**

В 1868 году прибыл английский военный корабль "Топаз", и судовой врач **Дж. Л. Палмер** не нашел каменных статуй. *"Он оказался достаточно любознательным,* - [процитируем книгу Хейердала](http://bit.do/fMJj8), - *чтобы поинтересоваться археологией острова, а также местными обычаями и языческими изделиями, которые пасхальцы прятали от миссионеров.

**English:**

In 1868 the British warship Topaz arrived and the ship's doctor **J. L. Palmer** found no stone statues. *"He proved inquisitive enough,* [to quote Heyerdahl's book](http://bit.do/fMJj8), *to inquire into the archaeology of the island, as well as the local customs and pagan wares which the Paschal people were hiding from the missionaries.

**Russian:**

В различных публикациях Палмер (1870 а, с. 111; 1870 b, с. 180; 1875, с. 287) рассказывает, что ему показали целый ряд деревянных поделок, бережно сохраняемых островитянами...

**English:**

In various publications, Palmer (1870 a, p. 111; 1870 b, p. 180; 1875, p. 287) relates that he was shown a number of wooden crafts carefully preserved by the islanders...

**Russian:**

К каменным фигуркам пасхальцы явно относились иначе. Любознательный судовой врач записал, что на острове есть мелкие каменные скульптуры, но их, в отличие от деревянных поделок, ему не показали, и выменять ничего не удалось. Он узнал только, что у этих скульптур уши не удлиненные, как у каменных исполинов (Palmer, 1875, р. 287)"*.

**English:**

The Easterlings had a different attitude to stone figurines. The inquisitive ship doctor recorded that there were small stone sculptures on the island, but he was not shown them, unlike the wooden craftsmen, and could not exchange anything. He learned only that these sculptures do not have elongated ears, as stone giants do (Palmer, 1875, p. 287).

**Russian:**

В том же 1868 году епископ Таити **Тепано Жоссан** получил коллекцию табличек ронго-ронго и артефактов от миссионера Эжена Эйро, прибывшего на остров Пасхи в 1864 году и основавшего со священником Ипполитом Русселом со товарищи первое европейское поселение в 1866 году. В описании предметов, отправленных в управление Конгрегации, наряду с реальными есть и зарисовка статуи моаи. Но такая, что сразу видно, что авторы свидетельства с описанием в глаза не видели никаких моаи. Судьба этой статуи неизвестна, разумеется))

**English:**

Also in 1868, the Bishop of Tahiti **Tepano Jossan** received a collection of rongo-rongo tablets and artifacts from the missionary Eugène Ayrault, who had arrived on Easter Island in 1864 and established the first European settlement in 1866 with the priest Hippolyte Roussel and associates. In the description of the items sent to the office of the Congregation, along with the real ones, there is a sketch of the statue of moai. But such that it is immediately obvious that the authors of the certificate with the description have not seen any moai in the eyes. The fate of this statue is unknown, of course))

![Image visible only on original site](https://imgprx.livejournal.net/fbfc5a2824d25a2937efebcb4e08357fc1cd65d2/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNLK_g8VpSovInFmMk0j1XB)

**Russian:**

Это мы сейчас улыбаемся. А знаменитому путешественнику, этнографу **Н.Н. Миклухо-Маклаю**, мечтавшему увидеть статуи, было не до смеха. Сам он прибыл к острову Пасхи - Рапа-Нуи, но сойти и исследовать памятники не смог по причине лихорадки. В подготовленной к печати книге [Плавание на корвете "Витязь", ноябрь 1870 - сентябрь 1871 г. Острова Рапа-Нуи, Питкаирн и Mангарева](http://bit.do/fMJkb) , впервые изданной по рукописи в 1872 году он с горечью пишет о пренебрежении исследователей к маленьким статуям, описывавшимся до сих пор. Не подозревая о том, какие большие, величественные исполины появятся совсем скоро.

**English:**

It's us who are smiling now. But the famous traveler, ethnographer **N.N. Miklukho-Maklai**, dreaming to see the statues, had no time for laughter. He himself arrived at Rapa Nui, the Easter Island, but he could not go down and examine the monuments because of fever. In the book [Voyage of the Vityaz corvette, November 1870 - September 1871, Rapa Nui, Pitkairn and Mangareva Islands] (http://bit.do/fMJkb), first published from a manuscript in 1872, he wrote with bitterness about the neglect of the small statues that were described until now. Unaware of what large, majestic giants would appear very soon.

**Russian:**

*"Очень сожалел я, и досадно мне было, находясь в виду острова, не побывать на нем, не осмотреть тех важных документов прежней жизни островитян, которые делают о. Рапа-Нуи единственным в этом роде изо всех островов Тихого океана. Мне было тем более досадно, что путешественники (Очень многие мореплаватели посетили о. Рапа-Нуи. Начиная с Роггевена, открывшего остров ^24^, Кук, Ла-Перуз, Коцебу ^25^, Крузенштерн ^26^, Бичи, Дю-Пти-Туар ^27^ и другие рассказывают о своем пребывании на этом острову, но все описания и изображения более чем недостаточны, если захочешь получить понятие об этих памятниках, а не удовольствоваться сообщением, что на о. Рапа-Нуи находятся большие каменные идолы ^28^. Очень вероятно даже, что, помимо колоссальных каменных фигур, на острову найдутся не такие громадные, но не менее интересные древности ^29^), видевшие эти [61] замечательные памятники, только смотрели на них глазами удивления или равнодушия, и ни один из них не постарался подробно и внимательно изучить эти достопримечательные образцы полинезийского искусства, которые до сих пор остаются почти столь же неизвестными, как и в 1721 г. (Ошибка в рукописи. Следует: в 1722 г), когда Роггевен первый описал их.

**English:**

> I regretted very much, and it was a pity to me, being in sight of the island, not to visit it, not to examine those important documents of the former life of the islanders, which make Rapa Nui the only island of its kind in the Pacific Ocean. Rapa Nui is the only island of its kind in the Pacific. It was all the more unfortunate to me that the travellers (Very many mariners have visited the island of Rapa Nui. Rapa Nui. Beginning with Roggeven, who discovered the island ^24^, Cook, La Perouse, Kotzebue ^25^, Kruzenshtern ^26^, Beachy, Du Petit Thoir ^27^ and others tell of their stay on this island, but all descriptions and pictures are more than insufficient if one wishes to gain an idea of these monuments, rather than being satisfied with the report that there are large stone idols ^28^ on Rapa Nui. It is even very probable that, besides the colossal stone figures, there are on the island less enormous but no less interesting antiquities ^29^) who have seen these [61] remarkable monuments, have only looked at them with eyes of wonder or indifference, and none of them have endeavoured to study in detail and closely these remarkable specimens of Polynesian art, which are still almost as unknown as they were in 1721. (Error in the manuscript. should be: in 1722), when Roggeven first described them.

**Russian:**

Последние более полные и интересные сведения были сообщены г-ном Пальмером, врачом на английском судне "Топаз", который был на Рапа-Нуи в пятидесятых годах ^30^, и г-ном Гана, командиром чилийского корвета "О'Гигинс", посетившим остров в прошлом году (Краткое сообщение **Луиса Игнасио Ганы** об этой интересной экспедиции напечатано в годовом отчете Чилийского морского министерства народному конгрессу (см. прим. на с. 59) ^31^). Чилийская экспедиция подтвердила в главных [63] чертах уже сообщенные г-ном Пальмером известия, что не все каменные идолы уничтожены (Бичи привез известие, что все колоссальные статуи на Рапа-Нуи разрушены, но уже бывший после него Дю-Пти-Туар опроверг это сообщение. Подтверждение рассказа Пальмера г-ном Гана оттого имеет вес, потому что некоторые писатели, как например, Г. Герланд (см.: Waitz Theodor. Anthropologie der Naturvolker. T. V. 2 Abth., fortgesetzt von G. Gerland, Leipzig, 1870. S. 225), сомневались в верности подробностей, сообщенных г. Пальмером), что еще многие стоят, другие опрокинуты, но еще целы, что главное место их выделки находится у края описанного г-ном Пальмером вулкана Утуити и что в некоторых местах можно было еще видеть, как они в прежнее время стояли, именно на высоких платформах или алтарях ^32^. На корвете "О'Гигинс" были привезены разные предметы, которые были отданы в Этнологический музей в Сант-Яго, где я имел случай и удовольствие их видеть. Кроме большого идола (Этот идол, 1 1/2 м вышины, был нарочно выбран как самый малый между громадными, из которых некоторые достигали, по словам Роггевена, до 12 м вышины (см.: Gerland. S. 224). На "Топазе" был отвезен в Англию один из идолов Рапа-Нуи, другой на фрегате французском "La Flore" отправлен во Францию ^33^) из черной лавы ^34^, в упомянутом музее находятся 4 барельефа..."*

**English:**

A last more complete and interesting account was given by Mr. Palmer, a doctor on the English ship Topaz, who was on Rapa Nui in the fifties ^30^, and by Mr. Gana, commander of the Chilean corvette O'Giggins, who visited the island last year (a brief report **Luis Ignacio Gana** on this interesting expedition is printed in the annual report of the Chilean Ministry of the Sea to the People's Congress (see ^31. note on p. 59) ^31^). The Chilean expedition confirmed in the main [63] features the news already reported by Mr. Palmer, that not all the stone idols were destroyed (Beachy brought the news that all the colossal statues at Rapa Nui were destroyed, but the Du-Pti-Toire already after him refuted this report. The confirmation of Palmer's account by Mr. Ghana therefore carries weight, because some writers, such as G. Gerland (see: Waitz Theodor. Anthropologie der Naturvolker. T. V. 2 Abth., fortgesetzt von G. Gerland, Leipzig, 1870. S. 225), doubted the correctness of the details reported by Mr. Palmer) that many were still standing, others overturned, but still intact, that the principal place of their dressing was near the edge of the Utuiti volcano described by Mr. Palmer, and that in some places they could still be seen to have stood in former times, namely on high platforms or altars ^32^. Various objects were brought on the corvette O'Giggins, which were given to the Ethnological Museum at St. Jago, where I had occasion and pleasure to see them. Besides the large idol (This idol, 1 1/2 m high, was deliberately chosen as the smallest among the enormous ones, of which some reached, according to Roggeven, up to 12 m in height (see: Gerland. S. 224). On the "Topaz" one of the idols of Rapa Nui was taken to England, another on the French frigate "La Flore" was sent to France ^33^) of black lava ^34^, in the mentioned museum there are 4 bas-reliefs... "*

**Russian:**

Французский корвет "Flore" был на острове в январе 1872 года. Ни одной целой статуи опять не нашлось. Согласно рапорту адмирала **Франсуа Тедора де Лапелена**, *"за неимением полной статуи в хорошей сохранности, которую легко можно было бы перевезти, мы должны были удовлетвориться тем, что отсекли голову от сброшенной статуи, которая лежала лицом вниз и которая была слишком тяжелой, чтобы погрузить ее целиком"* (Lapelin Т. de. L'Ile de Paques (Rapa-Nui) // Revue Maritime et Coloniale. Paris, 1872. T. 35. P. 106).

**English:**

The French corvette "Flore" was on the island in January 1872. Not a single complete statue was found again. According to a report by Admiral **François Tedore de Lapelin**, *"for want of a complete statue in good preservation which could easily be transported, we had to content ourselves with cutting off the head from a discarded statue which was lying face down and which was too heavy to load it whole" * (Lapelin T. de. L'Ile de Paques (Rapa-Nui) // Revue Maritime et Coloniale. Paris, 1872. T. 35. P. 106).

**Russian:**

Это она, 188 см. Сейчас находится в Музее на набережной Бранли (Париж).

**English:**

That's her, 188 cm. Now in the Musée du Quai Branly (Paris).

![Image visible only on original site](https://imgprx.livejournal.net/740534be2282b1df0634103976313f9efd3cf59a/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPTXwRTDvQYxnYhw9yqTqg_)

**Russian:**

Поскольку Миклухо-Маклай побывал на острове Пасхи за полгода до "Flore", а очерк завершил в августе - сентябре 1871 г., он не мог знать о событиях, случившихся позднее. Остается предположить, что во время совместной стоянки "Витязя" и "Flore" в Вальпараисо (см. об этом: Кр. вест. 1871. No 78) Миклухо-Маклай узнал о планах, связанных с предстоявшим посещением о. Пасхи французским корветом и намерением переправить одну из скульптур Рапа-Нуи во Францию. Судя по рапорту Лапелена, можно допустить, что доставка статуи входила в его предварительные планы.

**English:**

Since Miklukho-Maklai visited Easter Island six months before the "Flore" and finished his essay in August - September 1871, he could not know about the events which took place later. It is left to assume that during the joint anchorage of "Vityaz" and "Flore" in Valparaiso (see about it: Kr. vest. 1871. No 78) Miklukho-Maklai learned about the plans connected with the forthcoming visit to the island. Easter Island by a French corvette and the intention to transport one of the Rapa Nui sculptures to France. Judging from Lapelin's report, one can assume that the statue delivery was part of his preliminary plans.

**Russian:**

Рапорт подал капитан. В это время юный гардемарин, помогавший в том числе пилить голову, Луи Мари-Жюльен Вио, набросал рисунок, на основе коего впоследствии напишет акварель и использует ее в своей книге 1899 года, будучи маститым писателем в жанре путешествий и приключений. К нему мы обратимся ниже, он заслуживает того, а пока полюбуемся на рисунок безвестного гардемарина. Под ним надпись: "Остров Пасхи 7 января 1872 года в примерно 5 часов утра островитяне наблюдают за моим приплытием".

**English:**

The report was filed by the captain. At this time, a young midshipman, who also helped saw the head, Louis Marie-Julien Viot, sketched a drawing, on the basis of which he would later write a watercolor and use it in his book in 1899, as a venerable writer in the genre of travel and adventure. To him we will turn below, he deserves it, but meanwhile, let us admire the picture of the unknown gentleman. Underneath it is the inscription, "Easter Island, January 7, 1872, at about 5 a.m., the islanders are watching my arrival."

**Russian:**

В котором месте похожи статуи на оригиналы, исполненные в натуре?

**English:**

Where do the statues resemble the originals, executed in kind?

**Russian:**

Чистая фантазия жанра путешествий и приключений. Хотя сходство с отпиленной головой у этих монстров есть, но эта голова - не прославленных моаи.

**English:**

Pure fantasy of the travel and adventure genre. Although there is a resemblance to the sawed-off head of these monsters, this head is not the glorified moai.

[![Image visible only on original site](https://imgprx.livejournal.net/b8363ba433e8b39d24abfdc1107915225c8a869b/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMxACC1_2ad2kpQKSLUv2dL)](http://bit.do/fMJwG)

**Russian:**

Первые в истории относительно близкое и реальное изображение. Рисунок художника **Александра де Бара** по эскизу французского этнографа **Альфонсо Пинара**, посетившего остров Пасхи в 1877 году. Это лагерь офицеров в кратере вулкана Рано Рараку в Пасхальное воскресенье 1877 года.

**English:**

The first ever relatively close and real representation. A drawing by the artist **Alexandre de Bara** based on a sketch by French ethnographer **Alphonso Pinard**, who visited Easter Island in 1877. This is an officers' camp in the crater of the Rano Raraku volcano on Easter Sunday, 1877.

**Russian:**

Понятно, что там и таких статуй не существует. Ученые объясняют фантазию тем, что художник сознательно изобразил небывалое, чтобы показать разные стадии готовности вырубаемых и вытесываемых статуй.

**English:**

It is clear that such statues do not exist there either. Scientists explain the fantasy by the fact that the artist deliberately depicted the unprecedented in order to show different stages of readiness of the statues being cut down and carved.

[![Image visible only on original site](https://imgprx.livejournal.net/74cd31562e82f18ebfdf79441cdf936e42be1bda/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOVHLjHkopruS3LKSkCOPY2)](http://bit.do/fMJw3)

**Russian:**

Но главное не это.

**English:**

But that's not what matters.

**Russian:**

Рисунок выдающийся.

**English:**

The drawing is outstanding.

**Russian:**

Во-первых, он выдает то, что ни ученый, ни художник понятия не имели о реальных статуях, головы которых не могут стоять на скальном основании, поскольку у них есть туловища, закопанные в грунт.

**English:**

First, it gives away that neither the scientist nor the artist had any idea about the actual statues, whose heads cannot stand on a rock base because they have torsos buried in the ground.

**Russian:**

Второе и главное: в реале ученый и художник могли видеть только таких истуканов, закопанных по шею.

**English:**

The second and most important thing: in reality, the scientist and the artist could only see such statues buried up to their necks.

**Russian:**

Этот рисунок Пинар опубликовал во французском географическом журнале путешествий [Le Tour du monde](http://bit.do/fMHMB) в 1878 году в очерке об острове с тремя другими своими зарисовками.

**English:**

Pinard published this drawing in the French geographical travel magazine [Le tour du monde](http://bit.do/fMHMB) in 1878 in an essay on the island with his other three sketches.

**Russian:**

Две из них фантастичны.

**English:**

Two of them are fantastic.

![Image visible only on original site](https://imgprx.livejournal.net/634b24caf0018cb0d8621dc47188fa5323f15c1d/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOk03sHBoB_UHsmJm1L8A-G)

![Image visible only on original site](https://imgprx.livejournal.net/8d73171bac1ae014f55e3846af9970939b4a2e01/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO3S3OMDVLSV4K_iGY-M5fA)

**Russian:**

Третья бесспорно реальна. Не густо было реальных моаи в бытность на острове исследователя в 1877 году.

**English:**

The third is undeniably real. There were not many real moai when the explorer was on the island in 1877.

![Image visible only on original site](https://imgprx.livejournal.net/70012bba18015f2888875831ceb10cda3accf6b7/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcM8Roi4y_X1THnclJigMRT8)

**Russian:**

Рисунок вполне надежно указывает на период начала работ над формовкой статуй и готовности самых первых - конец 1870-х годов.

**English:**

The drawing quite reliably points to the period of the beginning of work on the moulding of the statues and the readiness of the very first - the end of the 1870s.

**Russian:**

Но это еще много лет делалось тихо и оставалось почти не известным общественности. Еще и 1909 году энциклопедический словарь **Брокгауза-Ефрона** все еще ничего не знал о выдающихся идолах. В статье указаны другие достпримечательности и события: *"Пасхи, остров или Вайгу (Waihu), самый вост. из островов Полинезии, 118 кв. км., 150 жит.; открыт 1722 в праздник Пасхи, 1888 занят респ. Чили. Развалины древних храмов".*

**English:**

But this was still done quietly for many years and remained almost unknown to the public. Even as late as 1909, the encyclopaedic dictionary **Brockhaus-Efrona** still knew nothing about prominent idols. The article lists other landmarks and events: **"Easter Island or Waihu, the easternmost of the islands of Polynesia, 118 sq. km., 150 inhabitants; discovered 1722 on Easter Day, 1888 occupied by Rep. Chile. Ruins of ancient temples".

**Russian:**

В 1886 году на строве начала трехлетнюю работу большая экспедиция, прибывшая на корабле "Могикан". Было проведено тотальное исследование острова, описано все, что надлежит описать, начиная от флоры, фауны, геологии, составлены карты, систематизирваны объекты, в частности, 113 ритуальных платформ аху, собраны бесчисленные артефакты, таблички ронго-ронго, составлены словари, описаны быт, жилища, занятия аборигенов... Итоги свел в 1891 году в отчете [Te Pito te Henua; or, Easter Island](http://bit.do/fMHYb) помощник командующего ВМФ США, герой Гражданской войны, вышедший в отставку в звании контр-адмирала, начальник и казначей экспедиции **Уильям Джуда Томсон**.

**English:**

In 1886, a large expedition, which arrived on the ship "Mohican", began its three-year work on the island. A total study of the island was carried out, everything that should be described, starting from flora, fauna and geology, maps were made, objects were systematized, in particular 113 ritual platforms of Ahu, countless artifacts, Rongo-Rongo tablets were collected, dictionaries were compiled, the life, housing, occupation of the aborigines were described... The results were summarized in 1891 in a report [Te Pito te Henua; or, Easter Island](http://bit.do/fMHYb) by the Assistant Commander of the U.S. Navy, a Civil War hero, retired Rear Admiral, the expedition's Chief and Treasurer **William Judah Thomson**.

**Russian:**

Отчет удивителен тем, что самому выдающемуся достоянию острова - статуям моаи уделено мизерное внимание.

**English:**

The report is surprising in that the island's most outstanding treasure, the moai statues, received scant attention.

**Russian:**

Стоит показать все.

**English:**

It's worth showing everything.

![Image visible only on original site](https://imgprx.livejournal.net/e587c920e6d74e61cdb3d8f628f6d1a29708e1ed/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOuKLJzLzRC2bPb_RCExGFI)

![Image visible only on original site](https://imgprx.livejournal.net/5063ed7e434717c5f18b3b45ffa92f7db3caa754/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOFgqF1XmSrezB3bTQR2a9u)

![Image visible only on original site](https://imgprx.livejournal.net/039ee4d62d8cfdb9aa2d9a12c78ce811feaf5742/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOmfjc1ZnXprd6FmRMBAE5Q)

**Russian:**

Еще один закопан возле аху.

**English:**

There's another one buried near the ahu.

![Image visible only on original site](https://imgprx.livejournal.net/c3f4825cfb05fb74543833d293ce7676f70d5169/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP0geTkHMThiqaCxdBUXr5f)

**Russian:**

Одна статуя, не классического моуи, стоит на аху, одна сброшена.

**English:**

One statue, not the classic moi, stands on the ahu, one dropped.

![Image visible only on original site](https://imgprx.livejournal.net/57e5579dc2141e9189ac30b6d1ec671621ce77a9/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOgZJEe6wKweYcmiNOXvbjL)

![Image visible only on original site](https://imgprx.livejournal.net/ea2123e7a71e98d9362447812c341b709acb61e8/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMlvZkrZBUqnlv8Mb35xZHW)

**Russian:**

На острове, сообщу или напомню, 887 моаи (886 с учетом происшествия, о котором в конце поста; вряд ли рассыпавшийся моаи подлежит восстановлению). Экспедиция, ставившая целью создание по сути энциклопедии острова, описывает даже не процент. Как это понимать? Единственным образом: не было других.

**English:**

The island, I report or recall, has 887 moai (886 with the incident at the end of the post; it is unlikely that the crumbling moai is restorable). The expedition, which aimed to create essentially an encyclopedia of the island, describes not even a percentage. How is this to be understood? In the only way: there were no others.

**Russian:**

В "каменоломне" с двумя "заготовками статуй" экспедиция зафиксировала удивительное: "заготовка" одна.

**English:**

In the "quarry" with two "billets of statues", the expedition recorded a surprising thing: the "billet" is one.

![Image visible only on original site](https://imgprx.livejournal.net/dcaae93b10f49bfc6a5e4b481b244f486d5ae21a/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP8YmspMOb0lWkgrnsEp47P)

**Russian:**

Имея фотоаппаратуру, экспедиция Томсона почему-то не пользуется ею для фиксирования моаи, кроме единственного раза. Разумеется, это то место на склоне, где первые моаи появились в 1877 году у Пинара. Статуй, как видим, ощутимо прибавилось.

**English:**

Having a camera, Thomson's expedition for some reason does not use it to record moai, except for the only time. Of course, this is the place on the slope where the first moai appeared in 1877 near Pienaar. The statues, as we can see, have appreciably increased.

![Image visible only on original site](https://imgprx.livejournal.net/b8cc7ec05aff8eb22fc5c435337fb6d053581402/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNpd7S3PLR5xFHnKQO8QIwS)

**Russian:**

Место известное. Сравнив это единственное фото с реальным видом, мы увидим, что у Томсона одна, верхняя статуя лишняя, т.е. изображение - фотомонтаж.

**English:**

The place is famous. Comparing this single photo with the real view, we see that Thomson has one, upper statue extra, i.e. the image is a photomontage.

![Image visible only on original site](https://imgprx.livejournal.net/0302c91b9b30e3e01b961d31d5c604895d500945/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPYALiBF_lfneE4Var3gXHw)

**Russian:**

Угадайте с семи раз, в курсе ли был начальник экспедиции, крупный чин ВМФ, что ставит в отчете липу?

**English:**

Guess from seven guesses whether the head of the expedition, a high ranking officer of the Navy, was aware that he was putting a fake in the report?

**Russian:**

И чем еще занималась экспедиция три года, помимо декларируемой деятельности?

**English:**

And what else did the expedition do for three years, besides the declared activities?

**Russian:**

И не напоминает ли этот лишний моаи в отчете о частенько втречавшихся нам в цикле о создании истории фактах упрощения проектов?

**English:**

And doesn't this extra moai in the report remind us of the frequent facts of simplification of projects in the cycle of making history?

**Russian:**

Теперь, как обещано, возвращаемся к гардемарину Вио, помогавшему в 1872 году капитану Лепелену пилить голову наиболее сохранившейся упавшей статуе. Его военная карьера не задалась, зато он стал состоятельным процветающим писателем **Пьером Лоти**. Написал десятки романов в модном жанре путешествий и приключений в экзотических странах, в жизни был таким был экзотическим и эксцентричным, как его романы, заводил жен в странах, где бывал, стал автором одного из первых в Европе романов об однополой мужской любви, ажиотировал свет, снимался голышом, собирал всякую всячину вплоть до мумий египетских кошек, а также и реально ценные артефакты и документы...

**English:**

Now, as promised, back to the midshipman Vio, who in 1872 helped Captain Lepelin saw the head of the best-preserved fallen statue. His military career failed, but he became a wealthy prosperous writer **Pierre Loti**. He wrote dozens of novels in the fashionable genre of travel and adventure in exotic countries, was so exotic and eccentric in life as his novels, made wives in the countries he visited, became the author of one of Europe's first novels about same-sex male love, agitated light, was shot naked, collected all kinds of things up to the mummies of Egyptian cats, and the really valuable artifacts and documents...

**Russian:**

В 1899 году писатель вспомнил о посещении в молодости острова и выпустил об этом книжицу "Rapa Nui". Буйные фантазийные описания мнгочисленных древних руин и статуй, которые он сроду не видел, ценности не представляют ([прочтите тут](http://bit.do/fMJQa), если интересно), это просто дань читателям модной темы. Что реально интересно - иллюстрации, которые в многочисленных изданиях книги были все помечены 1872 годом. Фактически это все, что успело появиться на острове к началу 20 века. Понятно, что популярному писателю надлежало "узаконить" все это.

**English:**

In 1899, the writer recalled visiting the island in his youth and published Rapa Nui, a book about it. The exuberant fantasy descriptions of numerous ancient ruins and statues which he never saw are of no value ([read it here](http://bit.do/fMJQa) if you're interested), it's just a tribute to the fashionable theme. What's really interesting are the illustrations, which in numerous editions of the book were all marked 1872. In fact, these are all that had time to appear on the island by the early 20th century. It is clear that the popular writer had to "legitimize" it all.

**Russian:**

В [библиотеке Galliсa нашлось 5 этих рисунков](http://bit.do/fMJSf) из архива писателя. Лоти своей рукой надписал, что они от Альфонса Пината 1877-78 годов. В чем также сомнения - мы показывали, что видел сам Пинат. Он тоже был эксцентричным, но в другую сторону: занимал деньги, выбивал гранты на путешествия и прибретение артефактов, собрал потрясающие коллекции, украшающие фонды многих музеев, вкладывал в этнографию больше, чем имел, умер в бедности, продавая библиотеку и оставив вдову работать прачкой. Пинат вполне мог выкупить и эти свидетельства.

**English:**

Five of these drawings were found in [the Gallisa library](http://bit.do/fMJSf) from the writer's archive. Loti inscribed with his own hand that they are from Alphonse Pinat of 1877-78. Which is also doubtful - we showed what Pinat himself saw. He too was eccentric, but in a different way: he borrowed money, hammered out grants to travel and acquire artefacts, amassed stunning collections that adorned the collections of many museums, invested more in ethnography than he had, died in poverty, selling his library and leaving his widow to work as a laundress. Pinat may well have redeemed these testimonies as well.

![Image visible only on original site](https://imgprx.livejournal.net/6deafc24cd7dd1aa853358f78ff8846b22a08ab3/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNJRfgiPBMrXnA9VR9H7yI7)

![Image visible only on original site](https://imgprx.livejournal.net/0d051cfac6898a18a6865af3d4bad16fee120767/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPqeDuS2RpsHAouBpj13lcu)

![Image visible only on original site](https://imgprx.livejournal.net/7f106d3820109c0d249aef840f8fdddc9bfd2fe1/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMHoUMcah2d1-T2YUT_5SHN)

![Image visible only on original site](https://imgprx.livejournal.net/67f6c2b8c485d85e4f78a5d3a106687edc75b996/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMDpuUREnggVAQl37B34Ipu)

![Image visible only on original site](https://imgprx.livejournal.net/e9498238c0436acb3e6361da976de544bfb464c6/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNBwCxwIrytZn8J42BwHBee)

**Russian:**

И еще пара новаций от Лоти, с которыми остров вступал в 20 век.

**English:**

And a couple of other innovations from Loti that the island was entering the 20th century with.

![Image visible only on original site](https://imgprx.livejournal.net/1535e011c9ac4dc36f9d476fb81fb24ed4576725/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNqzKkEv_7IH1zce2EptZP1)

![Image visible only on original site](https://imgprx.livejournal.net/9c9a820b0bc310f7c809aad89ecdc6b0414d0de2/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOHCPORH8H6wUF6_cnQz23k)

**Russian:**

Как видим, не часто посещали мореплаватели остров с такими замечательными, большими, неповторимыми, единственными в мире статуями. А когда посещали, то до конца 1870-х годов, а многие и после, видели какие-то совсем другие статуи, либо вообще никаких не видели.

**English:**

As we see, sailors did not often visit the island with such remarkable, large, unique, unique in the world statues. And when they visited, up to the end of 1870s, and many after that, they saw some quite different statues, or did not see any at all.

**Russian:**

Это значит только одно: не было нынешних замечательных статуй в эти посещения, первое реалистичное изображения появилось только в конце 1870-х годов. А первой работой, познакомившей широкий круг читателей с реальными статуями, стала книга [The Mystery of Easter Island: The Story of an Expedition](http://bit.do/fMJnH), изданная Кэтрин Рутледж (1866-1935) в 1919 году.

**English:**

It means only one thing: there were no the present remarkable statues during these visits, the first realistic image appeared only in the end of 1870th. And the first work that introduced the real statues to a wide range of readers was the book [The Mystery of Easter Island: The Story of an Expedition](http://bit.do/fMJnH) published by Catherine Rutledge (1866-1935) in 1919.

**Russian:**

На острове писательница прибыла в 1915 году, вместе с мужем Уильямом Рутледжем (1859-1939), которого английская Вики представляет [как британского этнографа, антрополога и авантюриста](http://bit.do/fMtLS). Сама Катрин [закончила дни в психиатрической больнице](http://bit.do/fMtL5). Вот такое авторство у великой истории, легенд и загадочного культа Человека-Птицы народа Рапа-Нуи, в науке - создателя статуй.

**English:**

The writer arrived on the island in 1915, with her husband William Rutledge (1859-1939), whom English Wiki presents [as a British ethnographer, anthropologist and adventurer](http://bit.do/fMtLS). Catherine herself [ended her days in a psychiatric hospital](http://bit.do/fMtL5). Such is the authorship of the great history, legends and mysterious cult of the Bird Man of the Rapa Nui people, in science the creator of the statues.

![Image visible only on original site](https://imgprx.livejournal.net/6201ffd7035ebb84b9a9a256f9bbd523a37de050/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNVD54omx-T87HR6Elq31bt)

**Russian:**

Остров - модель создания истории народа в миниатюре. Своего рода полигон цеха создателей истории. Разумеется, не было никаких войн, королей, династий на этом острове, никем и ничем, кроме записей Кэтрин Рутледж, не подтвержденных. Все сочинено с нуля и явлено общественности в 19 году 20 века. В том числе статуи моаи во всей красе. Полный новодел, и даже еще опалубную форму не со всех истуканов сняли в первой фотосессии.

**English:**

The island is a model for the creation of a nation's history in miniature. A kind of testing ground of the history makers' workshop. Certainly, there were no wars, kings, dynasties on this island, nobody and nothing, except Katherine Rutledge's records, confirmed. Everything is made up from scratch and revealed to the public in the 19th year of the 20th century. Including moai statues in all their glory. It is a complete novodel, and even the formwork has not been removed from all the statues in the first photo session.

![Image visible only on original site](https://imgprx.livejournal.net/c0a1e8a8ddac92365861a44fea4c1fcb2cb8ce32/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOuUH8LqK8cvXIZmCpxyYyS)

**Russian:**

Разобравшись с истоками, посмотрим на труды создателей истории острова с помощью современной фотоаппаратуры. Надо взглянуть современной техникой на истуканов, чтобы понять, что к "каменоломне" они не имеют отношения.

**English:**

Having solved the origins, let's look at the works of the creators of the island's history with the help of modern photo equipment. It is necessary to look at the icons with modern technology to understand that they have nothing to do with the "quarry".

**Russian:**

Та же аппаратура поможет найти ответ и на главный вопрос, мучающий поколения ученых: как перемещали аборигены истуканов?

**English:**

The same equipment will also help find the answer to the main question, torturing generations of scientists: how did the aborigines move the icons?

**Russian:**

Ответ: никак)) Страдать такой глупостью - вырубать неподъемных истуканов и таскать их по горам и долам могут только ученые. Они сделаны на месте. Статуи формовались твердеющей смесью с вулканическим туфом в составе в ямах на склонах гор в легких формах, грунт отбрасывался. Всего навсего.

**English:**

Answer: no way)) Only scientists can suffer such foolishness - to cut down unreachable icicles and drag them over mountains and dales. They are made on site. The statues were molded by a solid mixture with volcanic tuff in the composition in the pits on the mountain slopes in light forms, the ground was discarded. All in all.

**Russian:**

На первых фото, использованных в книге Рутледж, моаи только что выкопаны - световой загар не успел покрыть подземные части статуй. Британский музей, недавно выставивший эти фото, пишет, что сделаны они между 1914 и 1915 годом. Скорее в 1915-м - годом приезда четы Рутледж на остров.

**English:**

In the first photos used in Rutledge's book, the moai have just been excavated - the light tan has not had time to cover the underground parts of the statues. The British Museum, which recently exhibited these photos, writes that they were taken between 1914 and 1915. More likely 1915, the year the Rutledge couple arrived on the island.

![Image visible only on original site](https://imgprx.livejournal.net/17c8d493a2e271204791d3d7418565c3c2c1163b/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNav2mk-w0-zyjMaOgfdiFz)

![Image visible only on original site](https://imgprx.livejournal.net/8df095165c2afb7a603752ea58d90f4e33b95b6e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMe2c_Xq8UxGPtY_FUDV7P8)

![Image visible only on original site](https://imgprx.livejournal.net/f162c15ab45e3516ab912d9fe746373e05fbcf29/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOZ-vjKM5DG99NxMHWLhKgl)

![Image visible only on original site](https://imgprx.livejournal.net/d805e6ba8734276116fb4149cf51f0388e5a3c02/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMsJa-MHod8UqR7TD5NqYAg)

![Image visible only on original site](https://imgprx.livejournal.net/a4424320d77c16845f8fcb0d512714ba0e83df40/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcObxvgzhCvSPheUjw1e1y2X)

**Russian:**

Обращаем внимание на то, что "полувырубленные" в "каменоломне" "заготовки" формовались на плоской поверхности. Они вообще не предназначены для установки, сразу делались как лежащие.

**English:**

Note that the "half-cut" in the "quarry" "blanks" were molded on a flat surface. They are not intended for installation at all, at once were made as lying.

![Image visible only on original site](https://imgprx.livejournal.net/2cb31ab00474d5f3c1edf435f5dd90f3af1aa77c/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOLgxetZFo6t3e4spgkF7IP)

![Image visible only on original site](https://imgprx.livejournal.net/f37b33237c54ad6bcdadeee994630ece71625345/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPXS_Rz63EYLStfxTz6_Dy6)

![Image visible only on original site](https://imgprx.livejournal.net/4b2ac7f013ed5ce2f0957c7ef2afcfd6ab6eb31b/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOHmpl_pZqw4qoivTRmTWqj)

**Russian:**

Удобнейшее время для формовки большей части гигантских статуй моаи вполне ясно просматривается в истории аннексии острова чилийской администрацией (а все государства и ведомства, мы помним, поддерживают программу создания истории, как, например, [в Мексике](http://bit.do/fKgh2) ...) в 1888 году. Капитан-лейтенант ВМС Чили и командир крейсера "Эсмеральда", ветеран русско-турецкой войны в составе британского флота и Тихоокеанской - чилийского Торо Уркадо Поликарпо, в предыдущей чилийской экпедиции 1870 года под началом Луиса Игнасио Ганы служивший кадетом, занял остров, проведя до этого переговоры и уладив финансовые вопросы с владельцами имущества, англичанами, французами, католической миссией. Новая власть в его лице учредила колонию из трех семей, определила туземцев в деревню-резервацию, обнеся оную пограничной оградой. Остальной остров, более 90% площади, был передан в аренду шотландско-чилийской компании Williamson-Balfour Company, специализировавшейся на полезных ископаемых, под овцеводство. Это немного не сходится с датировкой рисунков с первыми правдоподобными моаи. То есть начинали создатели точно не под крылышком компании. Что не мешает успешно и масштабно продолжить под ним.

**English:**

The most convenient time for molding most of the giant moai statues is quite clearly seen in the history of the annexation of the island by the Chilean administration (and all states and departments, we remember, support the history-making program, such as [in Mexico](http://bit.do/fKgh2) ...) in 1888. Lieutenant Captain of the Chilean Navy and commander of the cruiser Esmeralda, a veteran of the Russo-Turkish War in the British Navy and of the Pacific, the Chilean Toro Urcado Policarpo, in the previous Chilean expedition of 1870 under Luis Ignacio Gana, serving as Cadet, occupied the island, having previously negotiated and settled financial issues with the property owners, the British, the French, the Catholic mission. The new power, in his person, established a colony of three families, assigned the natives to a village-reservation, and surrounded it with a border fence. The rest of the island, over 90% of the area, was leased to the Scottish-Chilean Williamson-Balfour Company, specializing in minerals, for sheep breeding. This is a bit inconsistent with the dating of the drawings with the first plausible moai. So the creators definitely didn't start out under the wing of the company. Which doesn't prevent them from successfully and extensively continuing under it.

**Russian:**

Некоторые авторы пишут про лагерь и исправительную колонию, учрежденную чилийской властью. Резон есть, зека отметились на памятниках и Пальмиры, [и Египта](http://bit.do/fKgi4) , но на острове Пасхи это не подтверждается.

**English:**

Some authors write about the camp and penal colony established by the Chilean authorities. There is a reason, the convicts were noted on the monuments of Palmyra, [and Egypt] (http://bit.do/fKgi4), but on Easter Island it is not confirmed.

**Russian:**

Материалом для форм, нетрудно догадаться, стал старый добрый целлулоид, открытый в 1839 году, он же паркезин. Не пластик 20 века, а самый первый, нынешнему поколению не знакомый. Он и горит как порох, мгновенно и сильно (еще в 1960-х годах советские детишки делали ракеты из картонных трубочек с фотопленками, еще сохранявшими это свойство первоцеллулоида), и разлагается сам по себе без особых воздействий за годы.

**English:**

The material for the molds, it is not difficult to guess, was good old celluloid, discovered in 1839, aka parquetzine. Not the plastic of the 20th century, but the very first, not familiar to the current generation. It both burns like gunpowder, instantly and strongly (back in the 1960s, Soviet kids made rockets out of cardboard tubes with photographic film, still retaining this property of first celluloid), and decomposes by itself without any special effects over the years.

**Russian:**

Ниже остальной фотоархив, из которого Кэтрин Рутледж взяла фтографии для своей книги. Роскошные снимки спустя несколько сезонов после создания, когда трава едва успела вырасти после копки, да и то не везде.

**English:**

Below is the rest of the photographic archive from which Kathryn Rutledge took the phtographs for her book. Gorgeous shots a few seasons after creation, when the grass has barely had time to grow after digging, and not everywhere.

![Image visible only on original site](https://imgprx.livejournal.net/a765aef6e087e627df9f83caa77603da7fc53ff7/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOP9JGTyvioe_ydqFoiU25p)

![Image visible only on original site](https://imgprx.livejournal.net/94972b31c14e39a7e523a83485c1aca64b1da239/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO-QtGNObrvt9kRLWHuwfOn)

![Image visible only on original site](https://imgprx.livejournal.net/e245efeea96feae8923ee97d64d1abaf56f8f74e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOd76NGP9j1y9kUGExX2G23)

![Image visible only on original site](https://imgprx.livejournal.net/b792f870eaaa6eecd6046e616505f54b5b5cfb53/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMcQ2NrPzvFpOVXe5_oEnaS)

![Image visible only on original site](https://imgprx.livejournal.net/0f55b927a1e237af0c2c2642283e18492ffc889b/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMoJDJXuDrG5be170USUgTY)

![Image visible only on original site](https://imgprx.livejournal.net/fedece00e43b9b43ec1ffcafd952ecd4e9829085/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO5uRkPYMVbP5or4OYGN5mB)

![Image visible only on original site](https://imgprx.livejournal.net/1199494c1acb285a3410bda1114064607a1af347/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO2FROiGpw2fhAmQCcDT04A)

**Russian:**

Вот как можно поколениям исследователей полтора века не замечать такую красоту?

**English:**

How is it possible that generations of researchers for a century and a half have not noticed such beauty?

![Image visible only on original site](https://imgprx.livejournal.net/b088ee4c79c7910bf9702a6b42608ef383d5911e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcObAtVUwdd6uWc0dEo1Bc-k)

![Image visible only on original site](https://imgprx.livejournal.net/e4880436392c4096162b56911a59e6da90b2451a/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPBd7eVDJdm-5ib3JIDEf4I)

![Image visible only on original site](https://imgprx.livejournal.net/ef6610bd791994e091c224f9d95e7df17914a854/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOx178xRTrv4s_Tiq9SbC9k)

![Image visible only on original site](https://imgprx.livejournal.net/ace48806a3d4aaf0ff254a42c92a3346f042b537/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOgDWyRJNdkxJJBdZuvjZnS)

![Image visible only on original site](https://imgprx.livejournal.net/f40f728e1026d49cfc647b6770316fa7f61437cc/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPFMrISCLIviX_RYGh_qwf9)

![Image visible only on original site](https://imgprx.livejournal.net/cfd8144c72d859e3ac25e3236cf2b33e2c8e854f/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP4nxc2mtzRzGzwuROFu7Xu)

![Image visible only on original site](https://imgprx.livejournal.net/7b7a1b8aaa7eccee8d3849f5de7597721896825d/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOIFy1md_QLfCvX8wDrbWl-)

![Image visible only on original site](https://imgprx.livejournal.net/859e4029e4ed01e107a0feffa8569f18818b2ae1/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcN_zlWiG17MNYpQby8eBi81)

![Image visible only on original site](https://imgprx.livejournal.net/0f1cf3385914d58fadaf15cba7ad7ce2d5bd7ebe/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNEc6ql2EpKSc96mT-Rr79m)

![Image visible only on original site](https://imgprx.livejournal.net/c77f1a9960fae42c5712a2ece2de49735155355c/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcN5krCEJeKAwjXD-FDzJ0M1)

![Image visible only on original site](https://imgprx.livejournal.net/27d40eba61fb1cbce6a7704d55f244abdf8fbe2f/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO7AYIzAproKWWML_c5DwzQ)

![Image visible only on original site](https://imgprx.livejournal.net/764c25b1574a93b6701113a263ce73d508ee40e8/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMPs9deUDtjH4nW1DtJZmbs)

![Image visible only on original site](https://imgprx.livejournal.net/3ee1ba59e2af51b2b0666119928b9efb2ccd1351/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNVyqaJxpictUOTpwaXXteK)

![Image visible only on original site](https://imgprx.livejournal.net/1f5aaf808cfec23fe349c20b8d33460611d9351d/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPj7_eyMDvSCsYfDY17NDEF)

![Image visible only on original site](https://imgprx.livejournal.net/ead32a1a9574eff3b745f7bd5cc28e461a7e7f22/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcO-Bpl-9FY3QCfdtTB4k9mU)

![Image visible only on original site](https://imgprx.livejournal.net/26ff34f9b4d502c539d025a418a0b1db30ca7ee2/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNX311BoeNYcU3V1E0NANyu)

![Image visible only on original site](https://imgprx.livejournal.net/d1153cd201b706c8b2108cc122efd8167a87d08c/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP0rTK_5NBCF404q3jHbSUz)

![Image visible only on original site](https://imgprx.livejournal.net/2cdd4a4335b7aef31273b4c62a64cf846da482e3/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOPs7XUuASElrMIczpFlyV-)

![Image visible only on original site](https://imgprx.livejournal.net/218537e0fdad2b6e3540e18c1d330da977a93df3/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMa9pz9N40qffx6exGin8fK)

![Image visible only on original site](https://imgprx.livejournal.net/732131dc101e89d6250b9dc0550fdce8d6f1da61/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcM4cAEiZNWctLNDQe5qkFfW)

![Image visible only on original site](https://imgprx.livejournal.net/4651d4ee5f809caed2d0485e2a932aaa6cac48b6/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOvXnzRYSZ48sbbtWtkn1dc)

![Image visible only on original site](https://imgprx.livejournal.net/f8d36a54da3694c41a3084e617c91c94c8f407bc/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNiiR3PbpfmvU21BJ_h__84)

![Image visible only on original site](https://imgprx.livejournal.net/c11b6fdf3cff1f76e42b520ffacfc706b2b7e25e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOQ7WZqJB4GUNAZ6fwrgsua)

![Image visible only on original site](https://imgprx.livejournal.net/06e7d3a7125430486680355808f311dd12db700d/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPG71Oo0Xp32eJULlBK4p1L)

**Russian:**

Обратите внимание: при Томсоне в 1886-89 годах этот моаи был только в проекте; в реале его развернули в другую сторону.

**English:**

Note: under Thomson in 1886-89, this moai was only in draft form; in reality, it was turned the other way.

![Image visible only on original site](https://imgprx.livejournal.net/f61f4c7aae90aed6f60724a4fc686d628438c7ea/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP25ptljqJoJFIkFF74ITvi)

![Image visible only on original site](https://imgprx.livejournal.net/3ec0419b89c9ad8e4c2eba6487457dbcd003e621/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOylrBTE1Ux1WPeL7JEPtFX)

![Image visible only on original site](https://imgprx.livejournal.net/a6e2b85a204a80c7994114e4b9b65c6c11ac70c1/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMOBQ1TkJN8IBAxghYJ7hGg)

![Image visible only on original site](https://imgprx.livejournal.net/4c3ce1ea0260a81623fd779f1140a032bc6b5c26/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPw9zzC9U5LOMGDibpA23Zf)

![Image visible only on original site](https://imgprx.livejournal.net/3a4d4a3313370a7238eaadb1a9230dc756bdf373/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNAT-pKUf-4N2VLnuqqMFOu)

![Image visible only on original site](https://imgprx.livejournal.net/b9990ab85467087fc1486c19781055e85d8e0549/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPmTJ0r7od70BIWI5zID2vh)

![Image visible only on original site](https://imgprx.livejournal.net/aeb04b4a0d765cabf1f1e64faba2c2921564d030/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMQGtJo4MvSpXdUAQPiAkP3)

![Image visible only on original site](https://imgprx.livejournal.net/2e6d1e5f140dea34d8ddf0d1fdb6610db07044a9/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOoWtQpi1tHc3hHietlqWup)

![Image visible only on original site](https://imgprx.livejournal.net/fa45cb73ea827af794be79fa839400afda85d1b6/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOjdCdlzjs5z6SbNrOG_Z4r)

![Image visible only on original site](https://imgprx.livejournal.net/a4a94030f7091b37c999ab3d769548d3727f0bcc/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcN7WFV9J9WocMvJgzZ-B2Tz)

![Image visible only on original site](https://imgprx.livejournal.net/ce733a13253e8d640042a6a6f4fa28e7ad4aea5f/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPQh1ChXLbVFW5G4DcA-Mx4)

![Image visible only on original site](https://imgprx.livejournal.net/aff9460c8aa3de27595e371bea3a5de8c12e6471/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcN2_AWOPfCnK8drJFkRpyf5)

![Image visible only on original site](https://imgprx.livejournal.net/914a3a13a68379c5d55076d5186f82c6b9bef7fb/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPg7Kq5i16DauONeMe-jtq-)

**Russian:**

Наверняка делали моаи и сразу группами - парочка этих наверняка формовалась однвременно.

**English:**

Surely moai were made in groups at once - a couple of these must have been molded at the same time.

![Image visible only on original site](https://imgprx.livejournal.net/925b91ee92da45bc8088466cb5f8bd11a3593266/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNlEN4tkNmiPFZtABLBK9EW)

**Russian:**

Наверняка неудачная попытка создателей "поставить" моаи, убрав грунт. Упал и развалился. Впредь сочли за лучшее предоставить откапывание ученым.

**English:**

Probably a failed attempt by the creators to "put up" the moai by removing the soil. It fell and fell apart. Henceforth they thought it best to leave the digging up to the scientists.

![Image visible only on original site](https://imgprx.livejournal.net/d736b25c014876fb94cf29b174cba03b37ecba30/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPZD0EmvoyiOtS3jv0I8YQY)

**Russian:**

Современная аппаратура видит больше, чем вековой давности аппарат Рутлеж.

**English:**

The modern apparatus sees more than the century-old Rutledge apparatus.

**Russian:**

Глаз на месте может и не заметить на такой фактуре, но хороший объектив увидит посторонние предметы, неизбежные при такой мега-формовке.

**English:**

The eye on the spot may not notice on such texture, but a good lens will see the extraneous objects inevitable with such mega-forming.

**Russian:**

И забутованный кирпич.

**English:**

And a bricked-up brick.

![Image visible only on original site](https://imgprx.livejournal.net/f72c1105fc7d812895e972ee06606289f58163c4/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcONAvcFlzfRxnhc0BXq7Cg5)

[![Image visible only on original site](https://imgprx.livejournal.net/2f39ae80ab602790346db733976360698da2c445/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPtUuEyIbfu2i-_PFCrgAz8)](http://bit.do/fMJN5)

**Russian:**

Еще три фото этого идола с кирпичом. Открывайте полные фото и смотрите.

**English:**

Three more photos of this idol with a brick. Open the full photos and look.

[![Image visible only on original site](https://imgprx.livejournal.net/f3bf63517e08ed10cb93c745dd6a6757b6e33555/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPyeloX5bou5jqZdZG5B4nC)](http://bit.do/fMLeb)

[![Image visible only on original site](https://imgprx.livejournal.net/a12567112f25393249ba68d2e81b809be66bf1b5/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMyu-yG9rNKDdtRIUVoBnof)](http://bit.do/fMLe5)

[![Image visible only on original site](https://imgprx.livejournal.net/2052dc503aecbc38e32529a804ae3c0ef504f3d7/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMUZ8pTjpHa2mJQb1QzH0xj)](http://bit.do/fMLfD)

**Russian:**

И любимую проволоку всех древних цивилизаций. В совокупности с годом появления артефактов в науке и СМИ - какие могут сомнения в эпохе изготовления?

**English:**

And the favorite wire of all ancient civilizations. Taken together with the year of appearance of artifacts in science and media - what doubts can there be about the age of manufacture?

![Image visible only on original site](https://imgprx.livejournal.net/f43dad97c531675bf66e482ba8922ec3691b54d2/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPIdbUphgtl8WWRuKAauptP)

[![Image visible only on original site](https://imgprx.livejournal.net/1c1f447a24a1f31f53f20f42dfdfba0e0d1f7535/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOS42kr3-zDBhq24OID0qMY)](http://bit.do/fMJN9)

**Russian:**

Для сетевых знатоков геологии скажу: вулканический туф включает в себя фрагменты других пород, но не квадратные кирпичи и не проволоки. А естественные включения не могут торчать наружу ни при какой внешней обработке. Они обязательно будут сбиты. Так торчать они могут только при постепенном затвердении-усыхании массы, слегка теряющей объем.

**English:**

For the netizens of geology, let me tell you: volcanic tuff includes fragments of other rocks, but not square bricks or wires. And natural inclusions cannot stick out with any external processing. They are bound to be knocked down. So they can stick out only at gradual hardening-drying of mass, slightly losing volume.

![Image visible only on original site](https://imgprx.livejournal.net/4b1923630c65c5bdb33fac375452edd083b09ab5/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPjLhONFfO6gCxCdzLTbNYZ)

[![Image visible only on original site](https://imgprx.livejournal.net/4692f744374914725ceadd0b23642520478af4db/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOQSc7ARLGWliKwOQ30iRZC)](http://bit.do/fMLug)

![Image visible only on original site](https://imgprx.livejournal.net/cb3aee3fb61b0135ea898c522c04d1349eef5404/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOLv0d3wuqTxMQyk6gsSr38)

[![Image visible only on original site](https://imgprx.livejournal.net/601c47352636bb457dc2d9df2ea73aba8d460f95/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPfxUZhKv-BZcyF-xTJiH-a)](http://bit.do/fMLzb)

**Russian:**

Ну и чтобы никаких не было сомнений у ученых академических и альтернативных наук, для них сделали, [как в Египте под Асуаном](http://bit.do/fKgiF), каменоломню с парой "незаконченных истуканов". Полазить и как следует рассмотреть их нельзя, близко к ним не пускают, но хотя бы профессиональную фотоаппаратуру не запретили автру нижнего снимка, как в Египте. Этого достаточно, чтобы увидеть: в этой формовке такие же потеки и постронние предметы, как и в истуканах, которых здесь якобы рубили и таскали по острову - вот делать-то было нечерта рапануйцам несчастным...

**English:**

Well and to avoid any doubts of scientists of academic and alternative sciences, for them made, [as in Egypt near Aswan](http://bit.do/fKgiF), a quarry with a couple of "unfinished icicles". One cannot climb over them or look at them closely, but professional photo equipment is not prohibited to the author of the picture, like in Egypt. This is enough to see that this molding has the same stains and constructions as the icons that were allegedly cut down and dragged around the island - there was nothing for the wretched Rapa nui to do...

![Image visible only on original site](https://imgprx.livejournal.net/9f70eaf4542d4356570460ad6c8500cf4dc11202/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNkcrM5MHpCCh4QtFvBTjid)

![Image visible only on original site](https://imgprx.livejournal.net/e1b16faa7f947f3761017b82267c0a6043db2706/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNTxVFnw0mEPqsjG1nMHo1b)

![Image visible only on original site](https://imgprx.livejournal.net/9a1605132ef19b97f73f326053c3df307b41a65e/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcOz7JsswUYNEFwJGHRpUKGs)

[![Image visible only on original site](https://i.imgur.com/8Na04MH.jpg)](http://bit.do/fMJnr)

**Russian:**

Статуи, разумеется, не армированы. Просто бутовали хлам какой попадет.

**English:**

The statues, of course, are not reinforced. They just booted up whatever junk they could find.

**Russian:**

Недавно пикап въехал в одну статую моаи. Пикапу хоть бы что, а статуя - в мелкие комочки.

**English:**

Recently, a pickup truck drove into a moai statue. The pickup didn't care, but the statue was in little lumps.

[Тут галерейка](http://bit.do/fMJnk) . И опять посторонние включения. Но не армирование, с ним бы статуя уцелела, а какие-то палки, мусор - вон они справа на ближнем плане. Бутовали все подряд в этого моаи...

[![Image visible only on original site](https://imgprx.livejournal.net/02bed4129a8d0b7c86c44802da7cfa1c979f188f/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNFtZVCRscMZe-_OF9KlyNN)](http://bit.do/fMJ2y)

**Russian:**

Инцидент показал, что статуи из хрупкого вулканического туфа не только неподъемные, но и невероятно хрупкие.

**English:**

The incident showed that statues made of fragile volcanic tuff are not only unreachable, but also incredibly fragile.

**Russian:**

Это существенно усложняет задачу ученых академических и альтернативных наук.

**English:**

This makes the task of academic and alternative science scientists much more difficult.

**Russian:**

Им надо не только исхитриться доставить во все концы, например, двух крупных моаи весом по 80 тонн на расстояние 3,6 и 5 км, но и не развалить их. Какой площади должно быть место приложения тянущей силы, способной двигать 80 тонн? И по какой поверхности должны скользить 80-тонные идолы, чтобы не обломать и не стереть выступающие части? Так надо ставить задачу, а не кантовать стоймя изваяния в масштаб меньшего веса и большей прочности по твердому ровному покрытию.

**English:**

They must not only contrive to deliver at all ends, for example, two large moai weighing 80 tons at a distance of 3.6 and 5 km, but also not to break them. What should be the area of application of the pulling force capable of moving 80 tons? And on what surface should the 80-ton idols slide so as not to break and not to rub off the protruding parts? That's the way to set the goal, not to roll upright statues in the scale of less weight and greater strength on a hard, level surface.

**Russian:**

Еще есть недострой в 21 м длиной, весом, оценивающимся в 270 тонн.

**English:**

There is also an unfinished building 21 meters long and weighing an estimated 270 tons.

**Russian:**

Что касается нескольких маленьких и мало похожих на нынешние изваяний, реально увиденных путешественниками 18-19 веков, то они и сейчас стоят в Британском музее. В коллекции Рутлеж, подаренной музею, среди других артефактов, найденных, украденных, выменянных в течение полувека до нее, помимо других предметов есть истуканы гораздо мельче сделанных на острове. Скорее их нужно рассматривать как эскизы.

**English:**

As for the few small and little like the present sculptures actually seen by travelers in the 18th and 19th centuries, they still stand in the British Museum. In the Rutledge collection, donated to the museum, among other artefacts found, stolen, traded in the half century before her, among other objects, there are statues much smaller than those made on the island. Rather, they should be regarded as sketches.

**Russian:**

Вот единственная похожая, ее высота 2,3 м.

**English:**

Here's the only one that looks like it, it's 2.3 meters tall.

![Image visible only on original site](https://imgprx.livejournal.net/1413d5b67d9adf594c10551d38a5750f84e9eeb4/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcPcELy0C4nVHkPvA0YFQzNk)

![Image visible only on original site](https://imgprx.livejournal.net/5f5db6afc52241e8c70e9ab2ebfc6e4baa5149a7/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcNKDrx7m2WPdEwQvWwop4nR)

**Russian:**

Остальные больше похожи на наброски, не очень удачные, делавшиеся как модели для форм, пока мастер не нашел нужный образ.

**English:**

The rest are more like sketches, not very successful, made as models for forms until the master found the right image.

![Image visible only on original site](https://imgprx.livejournal.net/e1921f43704fb5e7c7f150fc022126980012ab06/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcP3rcMFoOuGzGGU368r-fbh)

![Image visible only on original site](https://imgprx.livejournal.net/c9cae69fa73470741bd62c0a9800874448ddeb76/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMQrdsEhZXYltdMnKTn-wOx)

**Russian:**

Одно-два из этих маленьких изваяний вполне могут быть и реальным. Но в истории они останутся только как неказистые экспонаты в дополнение к исполинским статуям на острове.

**English:**

One or two of these small statues may well be real. But they will remain in history only as unsightly exhibits in addition to the giant statues on the island.

**Russian:**

В истории несчастные рапануйцы вместе с учеными, журналистами, общественностью сети будут вечно рубить и таскать неподъемные статуи, сделанные формовкой в грунте на маленьком острове среди Тихого океана.

**English:**

In the story, the unfortunate Rapa Nui, along with scientists, journalists, and the public network will forever be chopping and dragging the unreachable statues made by molding in the ground on a small island in the middle of the Pacific Ocean.

**Russian:**

Помянем, раз уж мы про историю рапануйцев, сделанную в литературе, и "письменность". Ею тоже наделили легендарный народ создатели истории, как без нее! [Несколько табличек ронго-ронго](http://bit.do/fMDoo), сделанных в середине 19 века на плашечках теспезии - священного дерева островов Полинезии для ритуальных фигурок и племенных барабанов, из европейского весла, обшивки европейского корабля, а также из подокарпа, дерева, растущего в ЮАР. Плашечки длиной 0,2-0,7 м плюс палка-посох длиной 1,26 м.

**English:**

Since we are talking about the history of Rapa Nui made in literature, let's remember the "writing" as well. The creators of history also endowed the legendary people with it, as without it! [Several tablets Rongo Rongo] (http://bit.do/fMDoo) made in the middle of the 19th century on plaques of tespeze - a sacred tree of the Polynesian islands for ritual figures and tribal drums, from European oar, European ship planking, and also from podocarpus, a tree growing in South Africa. The slats are 0.2-0.7 m long, plus a 1.26 m long stick stick stick.

**Russian:**

Ни к одной известной системе письменности, установили ученые, ронго-ронго не относится. Рапануйцев, понимающих таблички, не нашли, иных надписей где-либо, кроме острова, не обнаружили, [расшифровать по сей день не сумели](http://bit.do/fMDmP). И не сумеют. Потому что это сделано из головы, без всякого смысла, просто как необходимый атрибут истории - "письменность".

**English:**

Scientists have established that the rongo rongo does not belong to any known writing system. Rapanui people who understand the tablets have not been found, no other inscriptions have been found anywhere but on the island, [they have not been able to decipher to this day](http://bit.do/fMDmP). And they won't be able to. Because it is done out of the head, without any sense, just as a necessary attribute of history - "writing".

![Image visible only on original site](https://imgprx.livejournal.net/f1586a4073946045b74277205b2e0567c0fd0616/KHl_lhYH3FiANTEW7yS3979HpI8y_Wms4FEXdXmtrcMm15hWFu_myhTpxsFNc02Y)

**Russian:**

Ума и здоровья.

**English:**

Wit and health.

**Russian:**

Другие материалы о создании истории](http://bit.do/fMJxh)

**English:**

Other history-making materials](http://bit.do/fMJxh)

**Russian:**

Вынужден ставить это

**English:**

Forced to put this

**Russian:**

*Сейчас многие блогеры собирают донаты. Воздерживался, но приходится обращаться. Кто считает, что эта работа заслуживает какой-то оплаты - буду признателен. Карта Сбербанка 4276020014219544. ЮMoney (Яндекс-кошелек) 410013946572302.*

**English:**

Now a lot of bloggers are collecting donations. Refrained, but have to apply. Who thinks that this work deserves some kind of payment - would be grateful. Sberbank card 4276020014219544. Yandex purse 410013946572302.*

© All rights reserved. The original author retains ownership and rights.


