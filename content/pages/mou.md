title: Memorandum of Current Understanding
date: 2021-03-25 10:56
modified: Sun 04 Apr 2021 18:55:58 BST
category: 
tags: processing of humans
slug: 
authors: RoHT
summary:
status: draft

Planned adds:

1. ======================================

Sonofabor credit above [http://stolenhistory.org/articles/why-giant-skulls-the-repeal-or-the-funeral-of-miss-americ-stamp-1765-66.556/post-1432](http://stolenhistory.org/articles/why-giant-skulls-the-repeal-or-the-funeral-of-miss-americ-stamp-1765-66.556/post-1432)

Horizon credit to [https://malagabay.wordpress.com/](https://malagabay.wordpress.com), which seems to have been suppressed around the time this original post was made. As far as I know, it did not speculate that there is a church role in the meat and human-products trade.

2. ======================================

Link to sound of children playing in Sydney's secret tunnels: [https://stolenhistory.net/threads/the-secret-tunnels-of-sydney-australia.1408/](https://stolenhistory.net/threads/the-secret-tunnels-of-sydney-australia.1408/). This reminds us of the Maltese story of the Hypogeum of Hal Salflieni [https://www.guidememalta.com/en/stranger-things-the-mystery-of-the-lost-children-of-hal-saflieni-hypogeum](https://www.guidememalta.com/en/stranger-things-the-mystery-of-the-lost-children-of-hal-saflieni-hypogeum) - about a teacher and party of schoolchildren being taken into Maltese tunnels and never being seen again (but their screams occasionally being heard).

3. =======================================


4. ==============================================

5. ================================================
More on 1791 'Learned Societies' doing reverse engineering rather than science:

Adds to missing technology part:

- [https://stolenhistory.net/threads/beerit-some-russian-manual-the-casting-of-marble.1651/](https://stolenhistory.net/threads/beerit-some-russian-manual-the-casting-of-marble.1651/)
- [https://stolenhistory.net/threads/ancient-cannons-aka-ball-mills-aka-cement-kilns.551/](https://stolenhistory.net/threads/ancient-cannons-aka-ball-mills-aka-cement-kilns.551/)
- [https://stolenhistory.net/threads/renaissance-cannons-mysterious-and-misunderstood.1417/](https://stolenhistory.net/threads/renaissance-cannons-mysterious-and-misunderstood.1417/)

Deer parks... they seem to be associated with early 'observatories'? However, there are many problems with the English-language narratives of the early telescopes.

- Australian Telescope
- ORIAS


We can't get very far into the reverse engineering aspect because it will derail this thread. And, as KD pointed out, it is hidden away. But we can see disconnects in parts of the 'science' story that indicate where technologies began to be puzzled over in secret - or actually hidden. Alternative sources of electricity and lighter-than-air flight (levitated with ordinary 'town gas', as far as I can tell) are now becoming better known so if you want to pick over possible clues to other reversed-engineered technologies, have a look at https://stolenhistory.net/threads/1893-ish-yerkes-telescope-largest-refracting-telescope-ever.1297/.

If you enjoyed that, then feast your eyes on this: https://vk.com/@kbogam-teleskop (Russian original) https://178.62.117.238/australian-telescope.html (rough English translation).

To my mind, the pattern of events resembles the early part of George Orwell's 'Animal Farm'. Although Animal Farm is fiction, we might be able to use it as a model around which to frame questions about how our children keep ending up in wierd entities' beds, their dinner plates and their leather-tanning vats?

Questions like:

1. Can we work out who/what Snowball and Napoleon were?

2. Can we work out what they are doing now?

> it would probably crush the entire structure of the contemporary human related science making generations of “scholars” look like a bunch of bafoons.

> there is a common denominator for all the things we struggle to explain, amongst which are the giants.

> And that common denominator explains everything and is the reason for hiding.




3. There is reasonable evidence of experiments with humans involving some of that technology.




4. There is reason to believe that technology is still used to maintain reservoirs of humans for food, exotic products, experimentation and leisure use.

So, reverse engineering of hidden technology is intimately linked to 'eating of humans' because it seems to mark a moment in time when things could have improved for all of us but were not allowed to.

I don't think the reverse engineering of quickly stashed technology is the only reason history is hidden, or that giants and likely entities are hiding. I think it's the history - and continuing consumption - of humans on a vast scale.




However, the overt denial of tunnels and vaults *is* funny because it is so over the top in the face of the evidence. Not just in the face of reports of tunnels, vaults and the evidence of walling off that are found in some cellars but also funny in the face of the consistency that shows up over and over again in tunnel reports and tunnel legends. Don't take my word for it, take this comment from a thread on the UK urban explorer forum: 28dayslater:
https://www.28dayslater.co.uk/threads/newark-tunnel-legend-to-be-investigated.79482/post-939483

> There's always a tunnel between the church, the manor and the pub, even if there was no reason, and suitable roads.

Yes, absolutely. Not just in Newark, Nottinghamshire, but all over England, as I indicated in the previous post. True story: while driving in the countryside a couple of days ago I noticed subsidence in the road ahead. On the brow of a low ridge. Not a pothole but subsidence, which is caused by collapsing voids. I slowed down to look at it. A few yards beyond the subsidence and on my left was a track to an old hall, now a care home. On my right, a few fields away, was a church. I mentally drew a line between the old hall and the church and what did I have? The location of the subsidence in the road. This was a few miles away from (but not in) Temple Bruer, Lincolnshire. Temple Bruer is a templar ruin well known among archaeologists for spiral steps that lead down to 'non-existent' tunnels. Oh, and for its... 'sacrificed' baby remains.

Don't believe me? Look it up! You might want to look up nearby 'Wellingore Old Hall' while you're at it and ask yourself: WTF is that on the left?

So, why the tunnels? I think the answer is that you are looking at the workaday food outlets of a trade that went underground. The daily trade of disappeared travellers served up as pork or perhaps up-sold as 'child' to other travellers and the local lords. And - for the occasional treat - cherubs. Local or bought in. 

I don't want to give away the locations of the denials of the existence of these tunnels in case they get taken down. They need to stay up partly so that dedicated searchers for 'cognitive dissonance' can stumble up on them and recognise them for what they are. And partly because they show the human meat trade is still being covered up. Even when it costs seemingly knowledgeable local historians their intellectual dignity.

In other words the only available cover they have for the tunnels is denial. And denial is a pretty thin rag to cover subsidence, cellars with walled-up arches, meat-hooks in the ceiling and hundreds of eye-witness reports.

I really do think we are genetically programmed to believe - to have 'faith' - in 'authority' in the face of all evidence. It is as if Temple Grandin's panic-reducing abattoir designs were simply written in to as us lines of code. I call it the 'faith-loop'. I've thought for a long while that to any other entity that lives near us, our most terrifying characteristic must be our blind belief in any authority that points an accusing finger at someone else.

Or our incomprehension about the real cost of 'free'. "Look at all these free goodies from the Church! We'll listen to your confession, get you on to the right path, school your kids, and do our best to get you an after-life. It's all free - though donations are appreciated."

All this free pastoral care and the faith-loop prevents people from seeing that - just as with the tech industry - they are the product.

At least those of you who read this are breaking free of your faith-loop.


====================================================================================================
It's at this point - when we look into the reason why most can't see it - that we see how much we have to limit our sense of hope.

The faith-inducing lines of code in our genetic programming carry the religious over the hardly-secret paedophilia in the Catholic church; it carries protestants over paedophilia, money-grabbing *and* a trade in the meat products made from human youth. It carries voters over political disgrace after political disgrace. In that way, the voting booth is not very different to the confessional booth. You go in, admit your preferences and whisper 'I believe'. 

It's not that people are stupid. It's that they haven't broken free of their programming. The 'faith-loop' in our code has the minds of secular humans trussed up just as tightly: their faith in the good intent of the supra-national, national and local public and private sectors is as unshakeable as that of the snivelling Catholic who throws their savings at the pope.

Such pedevores and anthropovores can't strictly be called cannibals. Customers, yes, cannibals, no. 

It is possible that some human product trade occupations are reflected in last names (surnames):

- Olier (renderer/trader in human grease). 
- Others?

9. =================================================
Why 1785? There's a lot of analysis that dates much Eurasian destruction to 200-250 years or so ago. I used 1785 after reading [https://wakeuphuman.livejournal.com/1116.html](https://wakeuphuman.livejournal.com/1116.html). That's Russian but English translation is available here: [https://178.62.117.238/destruction-of-ekaterinoslav-dnepropetrovsk-by-a-thermonuclear-explosion-in-1785.html](https://178.62.117.238/destruction-of-ekaterinoslav-dnepropetrovsk-by-a-thermonuclear-explosion-in-1785.html).

10. ================================================
Castles as keeps:

From [https://neogeschichte.livejournal.com/15869.html](https://neogeschichte.livejournal.com/15869.html)

> [Utrecht's Castle Vredenburg] was built by Charles 5, the Holy Roman Emperor, not so much to protect the city as you might think, but in order to control the relatively independent population of the city.

> most of the castles and fortresses were built to control the population living nearby, not for military purposes, since the storming of such structures is too complicated and when a fortress or castle can be simply bypassed or simply starved out, 

> it is worth mentioning that according to legend, and so it is written in the official descriptions of the castle, it is clear that those who wrote the history of this castle still retain their conscience. So, according to legend, this castle was destroyed by the inhabitants of Utrecht after the Spanish garrison of the castle left, and the city of Utrecht, along with the rest of Holland, gained independence from the Spanish crown in 1577. Yes, imagine, the inhabitants of Utrecht decided to destroy this hated castle so that the conquerors could no longer use it to control the population of Utrecht. The logic is almost ironclad to destroy the fortress, which could be used to protect the city from the conquerors.


=======================================================

