title:  Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785
date: 2015-05-02 08:17
modified: Thu 14 Jan 2021 07:20:01 GMT
category: 
tags: thermonuclear war; catastrophe
slug: 
authors: WakeUpHuman
summary: It is now possible to assign the date of one of the thermonuclear explosions on this planet with an accuracy of plus or minus 3 years.
status: published
originally: 1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a thermonuclear explosion in 1785.html
local: 1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/

### Translated from:

[https://wakeuphuman.livejournal.com/1116.html](https://wakeuphuman.livejournal.com/1116.html)

It is now possible to assign the date of one of the thermonuclear explosions on this planet with an accuracy of plus or minus 3 years.

It was thermonuclear, not nuclear, because a nuclear explosion has a power limit set by the critical mass of uranium or plutonium.

To fully understand the scale of the destroyed object, you should read my [previous article about star-forts](http://wakeuphuman.livejournal.com/921.html). I talk about star-forts at the end of the article: [http://wakeuphuman.livejournal.com/921.html](http://wakeuphuman.livejournal.com/921.html)

So, I decided to find the old plan of Ekaterinoslav showing its bastion wall fortifications. I was looking for a plan of Ekaterinoslav, because it only renamed 'Dnepropetrovsk' later. It was not possible to do this, because the first Ekaterinoslav of Kilchensk' - it was also called 'Ekaterinoslav of Samara' - was allegedly given to the peasants because the city had been built in an unfortunate place: on the confluence between the rives Kilchen and Samara, so it was constantly flooded with water.

The second Ekaterinoslav was built after that on the right bank of the Dnieper. I quote this story taken from here: [http://gorod.dp.ua/history/article_ru.php?article=14](http://gorod.dp.ua/history/article_ru.php?article=14)

> Unfortunately, Ekaterinoslav was let down by a place carefully refined by V. A. Chertkov: in the spring, floods occupied the entire plain, leaving rotten swamps for the summer. Hopes for navigation were not fulfilled - the Samara river was impassable for merchant ships. And the organizers abandoned the old plans. By decree of Catherine II of January 22, 1784, the new location of the provincial city of Ekaterinoslav was determined "for the best need on the right side of the Dnieper river at Kaidak...".

> But despite the decree, the life of Ekaterinoslav Kilchenkogo (now under a new name - Novomoskovsk) continued. The Governor of the Ekaterinoslav Viceroyalty - Major General I. M. Sinelnikov - reported about its charms in letters and reports to G. A. Potemkin.

> On May 13, 1786, he wrote: 

> "the water from our city is beginning to wane. The current water in many houses was under the roof."

> "A year later, on April 21, 1787, on the day of The Empress's name day,"

> wrote I. M. Sinelnikov, 

> "after a cannon fire, a prayer service and a solemn dinner, I go in a boat to the Prince's house to watch. Like water breaking through a dam burst in the bottom picture of the garden..."

>"...a whole half of the city is in the water, and more are coming.. imagine a width of more than 7 versts, shaken by strong winds yesterday."

> The ruler finishes:

> "The most Serene Prince {Potemkin} said with divine lips that we are fools: why do we settle in low places?"

> Preparation for the construction of the new Ekaterinoslav began only in the autumn of 1786, and in January 1787. Catherine II decided to personally survey the land of the South. On April 22 of the same year, she, surrounded by a brilliant retinue, went down the Dnieper. I. M. Sinelnikov in despair wrote to the office of the Viceroy (April 19, 1787):

This story is clearly nonsense. Previously, they built much better than now, St. Petersburg is an example of this. No one would build a city without conducting a full range of geodetic and geological surveys. This story smells.

Using the keyword is "Ekaterinoslav Kilchenskiy", it is easy to find plans of the  demolished Ekaterinoslav Kilchenskiy and a separate plan of the Bogoroditskaya fortress, which is part of this city.

Let's look at the Bogoroditskaya fortress:


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/41039_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/41039/41039_800.jpg#resized)

And another plan:


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/41277_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/41277/41277_800.jpg#resized)

Now we load [Google maps](https://maps.google.com/), enter the coordinates 48.499565, 35.161087 and we see the remains of the destroyed Bogoroditskaya fortress.


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/41499_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/41499_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/41499/41499_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/41499/41499_original.jpg)

Now let's look at the full plan of the first Ekaterinoslav Kilchenskiy:


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/41805_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/41805/41805_800.jpg#resized)

We see that it is huge. Looking closely, I would say that it is 4.7 kilometers from the upper border to the lower. This is about the same as Vasilievsky Island in St. Petersburg. Naturally, the city was 100% antique and beautiful - it was called 'the Third Capital'. At the bottom of the plan, I highlighted the Bogoroditskaya fortress with a red circle. As you can see, it is part of Ekaterinoslav Kilchenskiy.

Now we can superimpose this plan on a satellite image of this area. We need to superimpose the Bogoroditskaya fortress on to the satellite image.

Raise the satellite camera higher or go to coordinates 48.524250, 35.137981 and take a screenshot:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42006_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42006_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/42006/42006_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/42006/42006_original.jpg)

The Bogoroditskaya fortress is marked with a red pin, and above it is a crater from an low-altitude air-burst thermonuclear explosion. The area to the right is flooded by a reservoir that was created later.

We superimpose the plan of Ekaterinoslav Kilchenskiy:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42380_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42380_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/42380/42380_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/42380/42380_original.jpg)

It is a perfect match.

The diameter of the crater from the explosion of the American thermonuclear bomb castle Bravo on Bikini Atoll - with a capacity of 15 megatons - left a crater with a diameter of 1.8 kilometers. [Read about castle Bravo](https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D1%81%D1%82%D0%BB_%D0%91%D1%80%D0%B0%D0%B2%D0%BE)

In our case, the crater is 4.7 km in diameter. Obviously, the explosion was much more powerful. The city was wiped off the face of the Earth. It is clearly visible that this was no ground burst. The ground is depressed. This effect reveals a low-altitude explosion. Like this:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42650_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/42650_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/42650/42650_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/42650/42650_original.jpg)

On the general plan of the city, which I posted above, there is a 12-bastion citadel at the top right. Here is its more detailed plan:


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/47035_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/47035/47035_original.jpg#resized)

This citadel is shown in this drawing. Here you can see only part of it, and to the left of it, the artist painted the northern suburbs of Ekaterinoslav, which are located behind the Bastion wall:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/43411_800.gif#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/43411_original.gif)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/43411/43411_800.gif#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/43411/43411_original.gif)

Note the large stonework of the citadel in the drawing. Most likely, the same as the Petropalov fortress (in St Petersburg?). The block sizes are exactly the same:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/43696_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/43696_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/43696/43696_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/43696/43696_original.jpg)

It was not fun to live at the end of the 18th century. The world is still on the edge, by the way.

PS: Since we are talking about the Petropalov fortress, I will write about its twin - the Kodak Fortress, located near Dnepropetrovsk. Its coordinates are 48.384005, 35.138045.

Read its history on the Internet, and I will show you in pictures its evolution over time.

It looked like this for a long time. Up to 1650 approximately. Quite a Petropalov fortress.


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/43970_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/43970/43970_800.jpg#resized)


The information that it was invented by the poles in 1635 is most likely a fairy tale. Given that Peter 1 canceled the year 7208 from the creation of the world in the star temple (although this may also be a story, I can not check) and began the chronology from 1700, the date of construction of any city or fortress can easily be in the range of 7500 years. Peter is clearly older than 300 years, this is clearly visible from the wear of the granite on the embankment.


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/44129_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/44129/44129_800.jpg#resized)

Then something cyclopean happened and half the fortress disappeared along with the shore.


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/44372_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/44372/44372_800.jpg#resized)


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45086_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/45086/45086_original.jpg#resized)

Now the remains of the fortress look like this: two bastions marked in red.


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45520_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45520_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/45520/45520_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/45520/45520_original.jpg)

Superimposing the old plan of the Kodak fortress on the current satellite screenshot:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45727_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45727_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/45727/45727_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/45727/45727_original.jpg)

And here is the plan of the Ukrainian authorities to restore the fortress of Kodak. As you can see, they are trying to restore not the whole, but half of the fortress using, most likely, metal structures. We are not even talking about filling in the reservoir that was formed earlier. I am not talking about the restoration of the fortress to the present state of the Petropalov fortress. Here are the possibilities of a 40-million people state. And before, these star-sharped objects were built by thousands.


<!-- Local
![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/45889_800.jpg#resized)
-->

![](https://ic.pics.livejournal.com/wakeuphuman/74019716/45889/45889_800.jpg#resized)

Well, for a snack, so as not to make a separate post, a few screenshots of carpet nuclear bombings of the east coast of the United States (South and North Carolina). Densely bombed, apparently with the purpose of grinding the infrastructure into rubble. The trees, as you can see, are young.

Images are clickable:


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/46254_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/46254_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/46254/46254_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/46254/46254_original.jpg)


<!-- Local
[![](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/46478_800.jpg#clickable)](1116_Destruction of Ekaterinoslav (Dnepropetrovsk) by a Thermonuclear Explosion in 1785_files/46478_original.jpg)
-->

[![](https://ic.pics.livejournal.com/wakeuphuman/74019716/46478/46478_800.jpg#clickable)](http://ic.pics.livejournal.com/wakeuphuman/74019716/46478/46478_original.jpg)

© All rights reserved. The original author retains ownership and rights.
