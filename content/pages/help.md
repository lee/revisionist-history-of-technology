title: Help
date: 2020-11-14 13:54
modified: Sun 31 Jan 2021 20:09:04 GMT
category: 
tags: 
slug: 
authors: RoHT Support
summary: Help with the markdown format used for RoHT's translations.
status: published

### Update notifications

*All* additions to  RoHT's translated/archived content can be followed. Use the site's Atom and RSS newsfeeds.

Modern news readers and browsers should find them automatically. From [https://www.bbc.co.uk/news/10628494](https://www.bbc.co.uk/news/10628494):

> Most modern browsers, including Firefox, Opera, Internet Explorer, Safari and Chrome automatically check for feeds when you visit a website, and display the orange RSS icon when they find one. Many of them allow you to add RSS feeds as a browser favourite or bookmark folder, automatically updating them with the latest content.

If required, manually configure your preferred news reader by using the following link styles.

To track *all* new articles, use:

- RoHT's [Atom feed](../feeds/all.atom.xml), or 

- RoHT's [RSS feed](../../feed).

To manually configure your newsreader to track updates to *individual* categories, authors or tags, use the below examples as templates:

- **RSS:** 

    - Follow updates to the ['mylnikovdm' category](../category/mylnikovdm.html) by subscribing RSS news readers to ['/category/mylnikovdm/feed'](../category/mylnikovdm/feed)

    - Follow updates from author ['Dmitry Anatolyevich'](../author/dmitry-anatolyevich.html) by subscribing RSS news readers to ['/author/dmitry-anatolyevich/feed'](../author/dmitry-anatolyevich/feed)

    - Follow updates tagged ['Tartaria'](../tag/tartaria.html) by subscribing RSS news readers to ['/tag/tartaria/feed'](../tag/tartaria/feed)

- **Atom:** 

    - Follow updates to the ['mylnikovdm' category](../category/mylnikovdm.html) by subscribing Atom news readers to ['/feeds/mylnikovdm.category.atom.xml'](../feeds/mylnikovdm.category.atom.xml)

    - Follow updates from author ['Dmitry Anatolyevich'](../author/dmitry-anatolyevich.html) by subscribing Atom news readers to ['/feeds/dmitry-anatolyevich.author.atom.xml'](../feeds/dmitry-anatolyevich.author.atom.xml)

    - Follow updates tagged ['Tartary'](../tag/tartary.html) by subscribing Atom news readers to ['/feeds/tartary.tag.atom.xml'](../feeds/tartary.tag.atom.xml)

### Technologies used

RoHT documents are in markdown format.

It uses [pandoc](https://pandoc.org/) to convert documents to markdown.

Various [python](https://python.org/) and [Almquist shell](https://en.wikipedia.org/wiki/Almquist_shell) scripts automate document parsing, cleaning and handling.

[git](https://git-scm.com/) shares and tracks work between machine handlers and human reviewers.

[meld](https://meldmerge.org/) is used to manually identify and correct document differences.

Custom-written python scripts automate text translation by connecting to various publicly-available machine translation services.

[pelican](https://blog.getpelican.com/) converts RoHT markdown to human-reviewable web content.

The website uses a slightly modified version of the [pelican-alchemy](https://github.com/nairobilug/pelican-alchemy) theme.

All code and automated work is carried out on [Alpine Linux 3.12](https://alpinelinux.org/) servers.


### Technology guides 

[Sustainable Authorship in Plain Text using Pandoc and Markdown](https://programminghistorian.org/en/lessons/sustainable-authorship-in-plain-text-using-pandoc-and-markdown) - The Programming Historian

[Getting Started with Markdown](https://programminghistorian.org/en/lessons/getting-started-with-markdown) - The Programming Historian

[Git for Authors (pdf)](http://mathbook.pugetsound.edu/gfa/git-for-authors.pdf) - University of Puget Sound


<!-- 

### New research material

RoHT welcomes new content to archive and/or translate. Preferred format is markdown. Acceptable - though not preferred - alternative formats are: HTML, ODT, RTF, Doc/Docx, or txt.

[https://pad.publiclab.org/p/links_to_Russian_material](https://pad.publiclab.org/p/links_to_Russian_material) last accessed 2021-01-13
-->


