title: Inspired by "And There Was a Country Before Russia" - Part 11.2
date: 2013-12-27 00:00:02
modified: Thu 14 Jan 2021 07:22:02 GMT
category: 
tags: flu; esotericism
slug:
authors: Vladimir Mamzerev
summary: 1918 - War went to sleep. H1N1 walked around Europe, and was intended for Russia and actively rebuilt the DNA of the Russians.
status: published
local: По мотивам "И была Страна до России". Часть 11.2_files
originally: По мотивам "И была Страна до России". Часть 11.2 - pro_vladimir — LiveJournal.html


### Translated from:

[https://pro-vladimir.livejournal.com/9704.html](https://pro-vladimir.livejournal.com/9704.html)

**Russian:**

Примеры прохода Волн - 1918-1919 - простейшая ДНК, что назвали гриппом H1N1. По Сути сбалансированный Элемент Добра. Его употребление ДНК Тушек вызвало волнообразную перестройку систем. Многие не приспособленные погорели. Предвестником вхождения в волну был 1908, Тунгусский активатор. Собственно он предназначался для России, где и ДНК Тушек пошли в разгон, многие почувствовали подъем, включение инструментов и понеслось. Завезенные же волной эмиграции в Европу ДНК Тушек из России, стали постепенно своим Светом менять тот мир, Тушки Западного мира оказались не готовы и при освоении, разгоне ДНК, начали очень быстро ломаться. Симптомы схожи с включением внутренней СВЧ. Ну что русскому хорошо, то...

**English:**

Examples of the passage of waves - 1918-1919 - the simplest DNA, which was called the H1N1 flu. The Suti (Sufi?) balanced Element of Good. Its use of Carcass DNA caused a wave-like system restructuring. Many unsuitable ones have been burned. The harbinger of entry into the wave was 1908, the Tungus activator. Actually it was intended for Russia where Tushkov's DNA also went in dispersal, many felt rise, inclusion of tools and was incurred. Brought by a wave of emigration to Europe DNA of Carcasses from Russia, began gradually with the Light to change that world, Carcasses of the Western world were not ready and at development, dispersal of DNA, began to break very quickly. Symptoms are similar to inclusion of internal microwave. Well, what's good for the Russian is...

**Russian:**

Эти волны прокатывают каждые 30 меток, 30 лет, хотя несколько нестабильно, движение по волнам все же. Вниз быстрее, вверх медленнее, замирание на гребне. Точно так же по волнам летит Планета, причем 4 раза в году, на гребне, притормаживает свой бег! Это есть, но совершенно необъяснимо с научной точки зрения. Вода в этот миг становится абсолютно стабильной, а по-нынешнему - лечебной, Святой. Что в целом не так и далеко от Сути. Остановка в Движении вымывает из Сути воды лишнее, остается только чистое Добро.

**English:**

These waves roll every 30 labels, 30 years, although somewhat unstable, movement on the waves still. Down faster, up slower, freezing in the crest. In the same way, the planet flies on the waves, and four times a year, on the crest, slows down its running. It does, but it's scientifically completely inexplicable. The water at this moment becomes absolutely stable, and in the present - healing, Holy. Which, in general, isn't that far from Suthi. Stopping in the movement washes away excess water from Sutya, there is only pure Good.


**Russian:**

Вот так и Грипп шествовал: 1889 - ДНК Россиян освоило H2N8 - более питательный набор элементов для ДНК, много не съешь, прошло относительно спокойно, и понеслось. ДНК россиян были готовы и смогли это употребить. Открытия в науке и технике, не просто так - новые инструменты - следствие разгона. N8 - больше информации по Душевному миру. Валом повалили организовываться кружки эзотерики. Некоторые там считали, что быстрый рост самосознания Душ приведет к чистому Добру. Практическая телепатия соперничала с радио! 1900 - Освоив предыдущий штамп, смогли поесть и H3N8 - еще более насыщенный информацией. Еще больше инструментов открывалось, кто куда их стал использовать. Чтобы слишком не разгонялись мир вокруг притушил взлет войной 1905. Пока малой. Впрочем H3 - чуть больше материи и N8 - максимум эзотерики! Практика материализации, левитации, телепортации уже практически становилась на поток. Кружки эзотерики демонстрировали практические опыты и успехи! Магия возвращалась, наука была в шоке, но не сдавалась. Шли в параллель! 1908 - всех не притушили, портал и т.д. - целенаправленно пришвартовался концентрат H1N1. Свет, бабах - распаковали, раздали. Больше питания, больше активность по материи и эзотерике. Кое-как мир успел запустить пожаротушение на ДНК Тушек России в 1914 году. Активность пустили в другое русло. 1918 - Война спала. H1N1 гуляет по Европе, а предназначался для России и активно перестроил ДНК Россиян. Они были сильно разогнаны, за что мир остальной и поплатился. Испанка - началось с эмигранта из России в Испании. Многое сгладила, притушила перед этим война. А то бы по Миру прошлось еще тяжелее. 1933 - H1N1 - очередной сбалансированный разгон ДНК в России и не только, очередное спешное тушение разгона остальным миром, опять война. Там уже не считают, что Душа важнее Тушки. 1946 - Оставшиеся после войны были подкормлены H1N1. ДНК пошли в рост. 1957 - H2N2 более информативный, для питания и продолжения разгона ДНК. Наука и Мир получили толчок, и пошли быстрее. Инструменты Тушек перешли на другой уровень. Там пока за баланс в развитии, слишком показательны уроки 1914 года. 1968 - H3N2 добавили информации, после успешного освоения. Чуть перекос в сторону материи и понеслись волны сторонников за материальное благополучие. Свободы нравов и т.д. 1977 - H1N1 - чтобы не мешать разгону, аккуратненько прибавили питания. 1995 - Миры делятся на Душевный и Технический путь - каждому свое - кому H1N1, а кому H3N2. А что же дальше? Смотрим на циклы - 1918-1946-1977, и... 2008 - H1N1 - питание для разгона! В зависимости от подготовленности Тушек плюс, минус сдвигается. Инструменты требуют питания и инструкций - вот они! В 2009... Ну это совсем просто. Тут даже ленивый догадается, что будет и почему.

**English:**

Here's how influenza walked: 

- 1889 - DNA Russians mastered H2N8 - a more nutritious set of elements for DNA, you can't eat much, it was relatively calm, and it went on. The DNA of the Russians was ready and they could use it. Discoveries in science and technology, for a reason - new tools - are the result of acceleration. N8 - more information on the soul world. The ramparts failed to organise esoteric mugs. Some believed that the rapid growth of the Soul's self-consciousness would lead to pure Good. Practical telepathy competed with radio!

- 1900 - Having mastered the previous stamp, they were able to eat the H3N8 - even more full of information. Even more tools opened up, who would use them anywhere. So that the world wouldn't be too dispersed around it, the rise of the 1905 war would be dampened. So far, small. But H3 is a little more matter and N8 is the maximum esoteric! The practice of materialization, levitation, teleportation was already practically becoming a flow. Cups of esotericism demonstrated practical experience and success! Magic came back, science was in shock, but did not give up. They were running in parallel!

- 1908 - everyone wasn't dimmed, portal, etc. - H1N1 concentrate was purposefully moored. Flash, bang - unpacked, distributed. More nutrition, more activity in matter and esoteric. Somehow the world managed to start fire extinguishing on DNA of Carcass of Russia in 1914. The activity was put in a different direction.

- 1918 - War went to sleep. H1N1 walked around Europe, and was intended for Russia and actively rebuilt the DNA of the Russians. They were greatly dispersed, for which the rest of the world paid the price. Spanish - started with an emigrant from Russia in Spain. Smoothed down a lot of things, dampened the war before that. Otherwise, it would have been even harder to walk the world.

- 1933 - H1N1 - the next balanced dispersal of DNA in Russia and not only, the next hasty extinguishing of dispersal by the rest of the world, again war. They no longer believe that the Soul is more important than Carcass.

- 1946 - The survivors of the war were fed H1N1. DNA went up in size.

- 1957 - H2N2 is more informative, to feed and continue the dispersal of DNA. Science and Peace were boosted, and they went faster. Carcass tools have moved to another level. The 1914 lessons are too revealing for the balance of development. 

- 1968 - H3N2 added information after successful development. A little skewed towards matter and waves of supporters for material well-being. Freedom of morals, etc.

- 1977 - H1N1 - not to interfere with the dispersal, neatly added power.

- 1995 - Worlds are divided into Soul and Technical Ways - each of them has its own way - H1N1 and H3N2. And what is next? We look at cycles - 1918-1946-1977, and... 

- 2008 - H1N1 - power for acceleration. Depending on the readiness of Carcass plus, minus shifts. Tools require power and instructions - here they are!

- 2009... Well, it's quite simple. Even the lazy one will guess what will happen and why.

**Russian:**

По всему выше выходит, что для Человека нет препятствий! Только он сам, его сознание и мешает ему двигаться, желание по-быстрому проскочить. Узнать тайные тропы от Мастеров. Завлекает слово Вознесенный! А тем не менее у Тушки Человека нет срока службы, она бесконечна! Ограничено только личным Самосознанием! Регенерация идет всю жизнь. Клетки Тушки полностью меняются за 7 лет! Заметим - опять 7. Или вот число Пи. Откуда, что за смысл? Длина окружности относится к диаметру! Казалось бы совершенно непонятно зачем это. Хотя используется, конечно. А Волны по sin? А Ведь это Суть Вселенной! Длинна окружности - длинна линейного Мира, оно же - длинна витка, год такого мира, оборот вокруг оси. Диаметр - путь напрямик! Смысл? Да хотя бы то, что движение напрямик уменьшает наш Мир в 3.14 раза! Мы теряем 3.14 Сути Мира, пытаясь двигаться напрямик. Так же это года объемного Мира, если двигаться внутрь, т.е. к Черта-К! А так же насколько мир из центра плотнее мира с линейного края "А". Так и выходит, что для бесконечно долгой жизни нет препятствий, так же для регенерации органов, кроме барьера нашего Ума.

**English:**

Everything above shows that there are no obstacles for Man. Only he himself and his consciousness prevents him from moving, the desire for a quick jump. To learn the secret trails from the Masters. The word 'Ascended' entices! Yet the Human Carcass has no life span, it is infinite! Restricted only by personal self-awareness! Regeneration is a lifelong process. The Carcass's cells change completely in seven years! Note again, seven. Or here's the number P. Where from, what's the point? The length of the circle refers to the diameter! It would seem completely unclear what it's for. Although it's used, of course. And the Waves by sin? Because that's the essence of the universe! The length of the circle is the length of the linear World, it's the length of the coil, the year of the world, the rotation around the axis. Diameter is the straight way! What's the point? Well, at least the fact that a straight path reduces our World by 3.14 times! We lose 3.14 Senses of the World by trying to move in a straight line. It's also a year of volumetric world if we move inwards, i.e., the Hellcats! And also how much denser is the world from the center of the world than from Linear Edge A. So it turns out that there are no obstacles for infinitely long life, as well as for regeneration of organs, except the barrier of our Mind.


**Russian:**

А хочется по-быстрому, тут же могут помочь методиками и техниками. Методик как всегда 3 - медитация, смерть Тушки, самостоятельный процесс. Техники сообщают, как надо махать и что читать, чтобы было вернее и быстрее. Тут как везде - технология всегда обещает, что именно эта последовательность взмахов и есть лучшая, разнообразие взмахов ограничено фантазией создателей техник. Людям думать не хочется, хочется по быстрее, а лучше сразу. Вот им и методичка, как правильно сесть, как дышать, как и куда смотреть. У кого получилось, те начинают выдвигать свою последовательность на роль самой верной.

**English:**

And if you want to be quick, you can immediately help with methods and techniques. As always there are three methods: meditation, death of the carcass, and an independent process. Technicians tell you how to wave and what to read to be more correct and faster. It's like everywhere else - technology always promises that this sequence of waving is the best, the variety of waving is limited by the imagination of the creators of techniques. People don't want to think, they want to think faster, but yet better. This is their technique, how to sit down, how to breathe, how and where to look. Those who succeed, they begin to put forward their sequence on the role of the most faithful.


**Russian:**

Проблемы начинаются потом - добраться то не самоцель, цель дорога, полученный сбалансированный опыт. Добежать до цели можно и медитацией, или просто сломав Тушку, но смерть имеет некоторые понятные неудобства, опять Черта-К и обратно. Медитация позволяет добраться до цели временно, пока Ум молчит. Состояние не так уж и далеко от смерти Тушки, с некоторыми нюансами конечно. Техники - не что иное, как быстрый разгон аппарата по схеме, без устранения причин возникновения проблем и без укрепления аппарата целиком. Восточные методики как таковых техник не предлагают, только через познание и гармонию. Долго, согласен. Действовать по цивилизованному, результат быстрее. И получится, на подобии, как взлетать на самолете по методичке и без практики. Причем повторяя действия пилота по картинкам. Взлететь наверняка получится, с посадкой сложнее. Медитация - не что иное, как полет без самолета. Т.е. пилот (Душа) есть, а самолет (Тушка) стоит на Земле. Как некий знак - Внутренний Диалог (ВД) - это полет, но с переговорами внутри самолета между пилотом и системой управления самолетом. Если он пропадает - они становятся единым целым, т.е. управление идет на интуитивном уровне, Душа и ДНК входят в резонанс. После взлета включаются дополнительные приборы, набор высоты, ощущение полета, однако это изрядно тратит энергии Добра. Вот тут и вопрос - возможно ли взлететь по методичке с техникой пилотирования и не опасно ли в дальнейшем так лететь, если нет опыта интуитивного общения у пилота и систем самолета? В резонанс ДНК и Души попасть не проблема, Более опытный пилот может натренировать попадать в это состояние. Проблема удержать. Переключиться на такое управление не сложно, а вот пользоваться. По прямой лететь не вопрос, а как поворачивать, ветер, посадка? Не опасно ли отсутствие опыта? А ведь опыт приобретается только лично пилотом и отрабатывается только временем налетов.

**English:**

Problems start later - to get there is not an end in itself, the goal is the road, the balanced experience gained. It is possible to run to the goal by meditation, or simply by breaking the Carcass, but death has some understandable inconveniences, again Hellcat-K and back. Meditation allows you to get to the target temporarily while the mind is silent. The state is not so far from the death of the Carcass, with some nuances of course. Techniques - nothing more than a rapid dispersal of the device on the scheme, without eliminating the causes of problems and without strengthening the entire device. Eastern techniques as such do not offer, only through knowledge and harmony. For a long time, I agree. Act in a civilized way, the result is faster. And it will turn out, in a similar way, how to take off on an airplane on a technique and without practice. And repeating the pilot's actions on the pictures. Take off will surely succeed, with a harder landing. Meditation is nothing but a flight without an airplane. That is, the pilot (Soul) is, and the plane (Carcass) is on Earth. As a certain sign - Internal Dialogue (IA) is a flight, but with negotiations inside the plane between the pilot and the aircraft control system. If it disappears, they become one, i.e. the control is on an intuitive level, the soul and DNA go into resonance. After takeoff, additional instruments, altitude gain and the feeling of flying are activated, but this wastes the energy of Good. Here is the question - is it possible to take off according to the technique of piloting and is it not dangerous to fly like this in the future, if the pilot has no experience of intuitive communication and systems of the aircraft? It is not a problem to get into the resonance of DNA and Soul, a more experienced pilot can train to get into this state. It's a problem to hold on. It's not difficult to switch to this kind of control, but to use it. Flying in a straight line is not a question, but how to turn, wind, land? Isn't the lack of experience dangerous? And the experience is gained only by the pilot himself and is practiced only by the time of raids.

**Russian:**

Как везде - хотим по-быстрому, типа отчитаться и возликовать, получить новые возможности, что первые взлетели - техники неплохи для этого. Хотим лететь и дальше, да чтобы более практично, маневрировать, да приборами пользоваться всеми, да энергия не кончалась, да Тушка от перегрузок не сгорела, тут без сбалансированного накопления опыта никак. Как вариант - хотим быстро ехать - ставим реактивный мотор, проще - твердотопливный ускоритель на автомобиль - зажигание, поехали! Ускорение, разгон! Красота! Только если корпус не развалится, да и рулить нечем, шины сотрутся от торможения. Далее корпус полетит над поверхностью, пока что-то его не остановит. Обычно в конце пути соскребают. И что интересно, есть такие примеры из жизни авто.

**English:**

As everywhere - we want to go quickly, we like to report and exclaim, to get new opportunities such as the first take off - techniques are not bad for this. We want to fly further, yes to be more practical, to maneuver, yes, to use all the devices, yes, the fuel did not run out, yes Carcass did not burn from overloads. We want to do this here without in any way acquiring a balanced accumulation of experience. We want to go quickly so we put in the jet engine option. It is easier - solid fuel accelerator for the car. Ignition... let's go! Acceleration, acceleration! Beauty! Only, if the body does not fall apart, leaving us with nothing to drive, the tires will shake off from braking. Then the body will fly over the surface until something stops it. Usually at the end of the journey, they scrape it off. And what's interesting, there are such examples of car life.


**Russian:**

Если Душа разогналась, а Тушка еще топчется, то ее материя, более слабая в этом отношении начнет портится, сохнуть, выгорать изнутри. Выцветут волосы, выпадут, кожу стянет, это наблюдается часто у давно живущих, говорят старость. Не совсем, хотя конечно - Душа с возрастом стала горячее для Тушки, вот она и высыхает, как цветок, которому не хватает воды. Вышеописанное справедливо и для разгона, или быстрого разгона ДНК. Каждый должен сам решить, как он хочет пройти. Опыт даже такого прохождения важен. Но без фанатизма, самовозгорание не слишком приятная вещь.

**English:**

If the Soul has dispersed, and the Carcass is still trampling, then its weaker matter will begin to spoil, dry, burn out from within. Hair will fade, fall out; skin will shrink &emdash; this is often observed in the long living. Of course, they say it is old age. But not quite &emdash; the Soul has become too hot for the Carcass with age, so the carcass dries up like a flower that lacks water. The above is also true for acceleration, or rapid DNA acceleration. It's up to everyone to decide how they want to go. Even the experience of such a passage is important. But without fanaticism, spontaneous combustion is not a pleasant thing to do.


**Russian:**

Совсем недавно еще, потеря Тушки, таким способом была не так страшна, Смерть, возврат, новая Тушка. Нынче времени на настройку новой Тушки под Мир все меньше и меньше.

**English:**

More recently, the loss of the Carcass in this way was not so terrible, Death, the return, the new Carcass. Nowadays, there's less and less time to adjust the new Carcass to the world.

**Russian:**

Человек 6 расы есть сбалансированная Сущность ДНК и Души, а потому не может себе позволить такой роскоши, как разбрасываться Тушками собственно Душно выращенную на Добре своей Души.

**English:**

The man of the 6th race is a balanced Essence of DNA and Soul, and therefore cannot afford such luxury as to be scattered with the Carcasses of the Soul grown on the Goodness of its Soul.


**Russian:**

А что собственно конденсирует конденсатор? эфир?

**English:**

What actually condenses the capacitor? The air?


**Russian:**

И от туда же, из истории схемотехники. Ранние посты показали, что корни произростания нашего современного понимания электричества кроются чуть дальше, чем ныне считает любой средне статистический обыватель. Скорее даже среднестатистический обыватель знает, что ток живет в розетка. А откуда он там и как давно его туда завели, ему за не надобность не интересно.

**English:**

And from there, from the history of circuitry. Early posts showed that the roots of our modern understanding of electricity lie a little further than any statistical average citizen now believes. It is more likely that even the average citizen knows that the current lives in a socket. And where it comes from and how long it was brought there, he need not be interested in.


**Russian:**

А а-РХ-ивольты, как выяснилось, были задолго до того, что мы называем Вольтами. [http://pro-vladimir.livejournal.com/5781.html](http://pro-vladimir.livejournal.com/5781.html)

**English:**

A-RX-volts, as it turns out, existed long before what we call Volts. [http://pro-vladimir.livejournal.com/5781.html](http://pro-vladimir.livejournal.com/5781.html)


**Russian:**

Далее. Стальные армирующие связи храмов. Очень часто имеют окончание, с кольцевой проушиной и вбитой в неё клином.

**English:**

Next. Steel reinforcing links of the temples. Very often they have terminate beyond the wall, with a ring eye and a wedge hammered into it.

**Russian:**

Как вот тут:

**English:**

It's like this:

<!-- Local
[![](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZqf2Ox7Sw927YtBTNZODRMYdmsZf3S93Qd7NIpD.html)](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZqf2Ox7Sw927YtBTNZODRMYdmsZf3S93Qd7NIpD.html)
-->

![Image no longer available](http://venividi.ru/files/img1/2782/66.jpg#resized)


**Russian:**

Их нынче, заделывают, замазывают, срезают.

**English:**

They're being slaughtered, smeared, cut.

**Russian:**

А вам этот элемент ничего не напоминает?

**English:**

Does this element remind you of anything?


<!-- Local
[![](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZtxSNnk1ojwQWDO5juGkv-dS7gV2Ur68CIKkef0Q.gif#clickable)](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZtxSNnk1ojwQWDO5juGkv-dS7gV2Ur68CIKkef0Q.gif)
-->

[![](http://ivatv.narod.ru/zifrovaja_texnika/1-14.gif#clickable)](http://ivatv.narod.ru/zifrovaja_texnika/1-14.gif)


**Russian:**

Ба! Так то ж классическая клемма.

**English:**

Ba! That's a classic clamp.

**Russian:**

Ниже небольшая подборка ссылок, в которых рассказывается как продлить службу люминесцентных ламп. Схемы приводятся не просто так. А как элемент имеющий непосредственное отношение искровым фонарям, что светились в световых барабанах ХРамов.

**English:**

Below is a small selection of links that tell you how to extend the service of fluorescent lamps. The diagrams are given for a reason. And as an element directly related to the sparklights that shone in the Khramov light drums.


[http://homemade-product.ru/vtorye-zhizni-lyuminescentnoj-lampy-i-payalnika/](http://homemade-product.ru/vtorye-zhizni-lyuminescentnoj-lampy-i-payalnika/)


<!-- Local
[![](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZsIAcUEqNhDkWPRI1dQVPvvBYvwyRsx21EskO2C4.gif#clickable)](%D0%9F%D0%BE%20%D0%BC%D0%BE%D1%82%D0%B8%D0%B2%D0%B0%D0%BC%20%22%D0%98%20%D0%B1%D1%8B%D0%BB%D0%B0%20%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B0%20%D0%B4%D0%BE%20%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8%22.%20%D0%A7%D0%B0%D1%81%D1%82%D1%8C%2011.2%20-%20pro_vladimir%20%E2%80%94%20LiveJournal_files/2C417VTEdxYQKg2v1DcmZsIAcUEqNhDkWPRI1dQVPvvBYvwyRsx21EskO2C4.gif)
-->

[![](http://homemade-product.ru/wp-content/uploads/2010/09/46.gif#clickable)](http://homemade-product.ru/wp-content/uploads/2010/09/46.gif)


[http://www.silver-kansk.narod.ru/radiofil/Lamp.htm](http://www.silver-kansk.narod.ru/radiofil/Lamp.htm)

[http://goodpc.chat.ru/other/energylamp/index.htm](http://goodpc.chat.ru/other/energylamp/index.htm)

[http://www.e-reading.co.uk/chapter.php/83924/105/Korshever_-_Elektrika_v_dome.html](http://www.e-reading.co.uk/chapter.php/83924/105/Korshever_-_Elektrika_v_dome.html)


**Russian:**

Кому интересно может сам поглубже изучить схемы и принципы работы. В некоторых случаях эти схемы вполне себе работают от одной лишь фазы.

**English:**

Who would be interested to learn more about the schemes and principles of work himself? In some cases, these circuits may well work from a single phase.


**Russian:**

Далее. Принимая во внимание, что краска в большинстве своем это подбор определенных химический элементов, что и задают цвет. а химические элементы обладают своими определенными свойствами. И соотвественно после этого понимания, они начинают выполнять свою собственную отведенную им роль. Ибо расцветка, как куполов, так и Вольтовых столбов а-РХ-вольтов, несут в себе вполне определенный функционал. Причем золочение куполов пошло не так уж и давно.

**English:**

Next. Taking into account that paint is mostly a selection of certain chemical elements, which set the color. And chemical elements have their own specific properties. And accordingly, after this understanding, they begin to fulfil their own role assigned to them. For the color, both domes and volts a-RX volts, have a certain functionality. And the gilding of domes did not occur so long ago.

**Russian:**

А кроме химического наполнения как то: - красные элементы -(Сu медь) - голубые элементы (купорос Cu медь - кристалл) - серые элементы (Zn цинк)

**English:**

And besides the chemical filling there are:

- red elements (Cu, copper)

- blue elements (Cu, copper sulfate - crystal)

- grey elements (Zn, zinc).


**Russian:**

Существуют и светофильтровальные функции. Как то у куполов ХР-амов. Синие или зеленые. Именно в эти цвета выкрашивали купола чаще всего. Можно предположить, что световая вольна на которую "настраивался" купол и была своеобразной тактовой частотой работы системы.

**English:**

There are also light filtration functions. It's like the XR-am domes. Blue or green. These are the colours most often used to paint the domes. We can assume that the light wave on which the dome was "adjusted" and was a kind of clock frequency of the system.


**Russian:**

Кроме того. Если присмотреться к ХР-амам чуть повнимательней, то несмотря на всё обилие деталей они достаточно часто повторятся. Чуть по разному, присутствуют так или иначе все однотипные элементы. Это закрученные винты разных направлений. И ломаные линии. И шарики всевозможные. Винты могут быть как в виде колонны, так и в виде купола. Как на соборе Василия Блаженного.

**English:**

Besides... If you take a closer look at the XR-Ams, then despite all the details they are quite often repeated. Slightly differently, there are all the same type of elements. These are screwed screws of different directions. And broken lines. And balls of all kinds. Screws can be either column-shaped or dome-shaped. Like St. Basil's Cathedral.

**Russian:**

Этот собор вообще стоит как насмехательство над всеми нами. Ибо каждый купол всего лишь максимально увеличенный элемент схемы. Со своей задающей частотой параметров. Все истории про ослеплении зодчих давно списали в разряд баек. Ибо мастера строившие данный ХР-ам банально пострадали от ослепления. некоторое, так сказать, нарушение техники безопасности имело место быть.

**English:**

Even this cathedral is worth a mockery of us all. For each dome is but a maximally enlarged circuit element. With its frequency setting parameters. All the stories about the dazzling architects' have long been written off as covers. For building masters the given XR-am simply suffered from dazzle.

[http://www.mepar.ru/library/bible/vethy_zavet/pyatiknizhie_moiseya/ex/27/](http://www.mepar.ru/library/bible/vethy_zavet/pyatiknizhie_moiseya/ex/27/)

**Russian:**

Кстати, о технике безопасности. Угадайке какой самый первый документ можно смело во многих его повествованиях приравнять к ТБ? Сею книгу считают самой читаемой на планете. Но это вообще форменное издевательство. Все читают, а что читают-то? немного от туда.

**English:**

Speaking of safety. Guess what the very first document can be safely equated with TB in many of his stories? This book is considered the most readable on the planet. But it's even a form of mockery. Everybody reads it, but what do they read? A little bit from there.

[http://www.mepar.ru/library/bible/vethy_zavet/pyatiknizhie_moiseya/ex/27/](http://www.mepar.ru/library/bible/vethy_zavet/pyatiknizhie_moiseya/ex/27/)

**Russian:**

Исход. Глава 27

1. И сделай жертвенник из дерева ситтим длиною пяти локтей и шириною пяти локтей, так чтобы он был четыреугольный, и вышиною трех локтей. 2. И сделай роги на четырех углах его, так чтобы роги выходили из него; и обложи его медью. 3. Сделай к нему горшки для высыпания в них пепла, и лопатки, и чаши, и вилки, и угольницы; все принадлежности сделай из меди. 4. Сделай к нему решетку, род сетки, из меди, и сделай на сетке, на четырех углах ее, четыре кольца медных; 5. и положи ее по окраине жертвенника внизу, так чтобы сетка была до половины жертвенника. 6. И сделай шесты для жертвенника, шесты из дерева ситтим, и обложи их медью; 7. и вкладывай шесты его в кольца, так чтобы шесты были по обоим бокам жертвенника, когда нести его. 8. Сделай его пустой внутри, досчатый: как показано тебе на горе, так пусть сделают (его). 9. Сделай двор скинии: с полуденной стороны к югу завесы для двора должны быть из крученого виссона, длиною во сто локтей по одной стороне; 10. столбов для них двадцать, и подножий для них двадцать медных; крючки у столбов и связи на них из серебра. 11. Также и вдоль по северной стороне - завесы ста локтей длиною; столбов для них двадцать, и подножий для них двадцать медных; крючки у столбов и связи на них (и подножия их) из серебра. 12. В ширину же двора с западной стороны - завесы пятидесяти локтей; столбов для них десять, и подножий к ним десять. 13. И в ширину двора с передней стороны к востоку - (завесы] пятидесяти локтей; [столбов для них десять, и подножий для них десять). 14. К одной стороне - завесы в пятнадцать локтей (вышиною), столбов для них три, и подножий для них три; 15. и к другой стороне - завесы в пятнадцать (локтей вышиною), столбов для них три, и подножий для них три.

**English:**

Exodus. Chapter 27.

1. And thou shalt make an altar of shittim-wood five cubits long and five cubits wide, so that it shall be rectangular, and three cubits high. 

2. And thou shalt make the horns at the four corners of it, so that the horns shall come out of it; and thou shalt cover it with brass. 

3. And thou shalt make pots of ashes and blades and bowls and forks and angles for him; and thou shalt make all the accessories of brass. 

4. And thou shalt make unto it a grid, a sort of netting, of brass; and thou shalt make four rings of brass on a grid, at the four corners of it; 

5. And thou shalt put it on the edge of the altar below, so that the grid is half the altar. 

6. And thou shalt make the staves for the altar, the staves of shittim-wood, and overlay them with brass:

7. And put the staves into the rings thereof, so that the staves are on both sides of the altar when thou shalt bear it. 

8. And thou shalt make it empty within, boarded: as shown to thee in the mount, so shalt they make it. 

9. And thou shalt make the court of the Tabernacle: on the midday side southward the hangings for the court shall be of fine twined linen, one hundred cubits long on one side; 

10. the pillars for them shall be twenty, and the sockets for them shall be twenty brass; the hooks of the pillars, and the bonds upon them shall be of silver. 

11. Also along the north side shall there be hangings of one hundred cubits long; their pillars twenty, and their sockets twenty brass; the hooks of the pillars, and the bonds upon them (and their sockets) shall be of silver. 

12. And the hangings of fifty cubits shall be on the west side of the court, and the pillars shall be ten for them, and their sockets ten. 

13. And the breadth of the court on the eastward side of the court shall be the hangings of fifty cubits; their pillars ten, and their sockets ten. 

14. And to the one side are hangings of fifteen cubits (a height), and their pillars three, and their sockets three; 

15. And to the other side are hangings of fifteen cubits (a height), and their pillars three, and their sockets three.

**Russian:**

Погодитека. Медные основания, связи из серебра. Так тож для чего, чтоб собрать цепь некую, сугубо электрическую?

**English:**

Wait a minute. Copper bases, connections made of silver. So, what's that for, to assemble a purely electrical circuit?

**Russian:**

16. А для ворот двора завеса в двадцать локтей (вышиною) из голубой и пурпуровой и червленой шерсти и из крученого виссона узорчатой работы; столбов для нее четыре, и подножий к ним четыре. 17. Все столбы вокруг двора должны быть соединены связями из серебра; крючки у них из серебра, а подножия к ним из меди. 18. Длина двора сто локтей, а ширина по всему протяжению пятьдесят, высота пять локтей; завесы из крученого виссона, а подножия у столбов из меди. 19. Все принадлежности скинии для всякого употребления в ней, и все колья ее, и все колья двора - из меди. Опять всё сплошь токопроводящие элементы. И еще шесть. Которая так хорошо дружить со статическим электричеством. И не просто шерсть, а раскрашенная шерсть. Краска, это и химические элементы с разными токопроводящими способностями и светофильтр. 20. И вели сынам Израилевым, чтобы они приносили тебе елей чистый, выбитый из маслин, для освещения, чтобы горел светильник во всякое время; 21. в скинии собрания вне завесы, которая пред ковчегом откровения, будет зажигать его Аарон и сыновья его, от вечера до утра, пред лицем Господним. Это устав вечный для поколений их от сынов Израилевых. А мы помним, что пламя есть источник ионизации. Далее целая "глава" посвящена одеждам в которых необходимо быть во всем этом: Исход. Глава 28 42. И сделай им нижнее платье льняное, для прикрытия телесной наготы от чресл до голеней, 43. и да будут они на Аароне и на сынах его, когда будут они входить в скинию собрания, или приступать к жертвеннику для служения во святилище, чтобы им не навести (на себя] греха и не умереть. Это устав вечный, [да будет) для него и для потомков его по нем.*

**English:**


16. And for the gates of the court a veil of twenty cubits (embroidery) of blue and purple and scarlet wool, and of fine twined linen of pattern work; pillars for it four, and the sockets for them four. 

17. All the pillars around the court shall be joined together by ties of silver; their hooks shall be of silver, and their sockets of brass. 

18. The length of the court shall be one hundred cubits, and the breadth throughout shall be fifty cubits; the hangings shall be of fine twined linen, and the sockets of the pillars shall be of brass. 

19. All the accessories of the Tabernacle for every use in it, and all its stakes, and all the stakes of the court, are of brass. Again, all the conductive elements. And six more. (Which is so good friends with static electricity. And not just wool, but dyed wool. Dye, it's both chemical elements with different conductive powers and a light filter.)

20. And they led the children of Israel to bring unto thee pure oils that were knocked out of olives, for the light to burn at all times. 

21. In the tabernacle of the congregation outside the veil, which is before the ark of the testimony, Aaron and his sons shall light it, from the evening until the morning, before the face of the Lord. It is an everlasting statute for their generations from the children of Israel. And we remember that the flame is the source of ionization. A whole chapter is then devoted to the garments in which it is necessary to be in all this:

The Exodus. Chapter 28 

42. And thou shalt make them a linen cloth underneath, to cover their bodily nakedness from their loins unto their shins:

43. And they shall be upon Aaron, and upon his sons, when they enter into the tabernacle of the congregation, or to come unto the altar for the service of the sanctuary, that they may not be brought to sin, or die. This is an everlasting statute, let it be for him, and for his descendants upon it.


**Russian:**

Там много достаточно элементов одежды и из чего она выполнена. Какие токопроводящие, какие изолирующие. А тот, кто не будет соблюдать технику безопасности, с теми мне придется общаться уже лично так сказать, вне его случайно убитого током тела.

**English:**

There are plenty of pieces of clothing and what they are made of. Which are conductive, which are insulating. And the one who will not comply with safety regulations, I will have to communicate with those in person, outside his accidentally electrocuted body.


**Russian:**

Исход. Глава 29

4. Аарона же и сынов его приведи ко входу в скинию собрания и омой их водою.

**English:**

 The origin. Chapter 29.

4. But bring Aaron and his sons to the door of the tabernacle of the congregation, and wash them with water.

**Russian:**

Исход. Глава 30

17. И сказал Господь Моисею, говоря: 

18. сделай умывальник медный для омовения и подножие его медное, и поставь его между скиниею собрания и между жертвенником, и налей в него воды; 

19. и пусть Аарон и сыны его омывают из него руки свои и ноги свои; 

20. когда они должны входить в скинию собрания, пусть они омываются водою, чтобы им не умереть; или когда должны приступать к жертвеннику для служения, для жертвоприношения Господу, 

21. пусть они омывают руки свои и ноги свои водою, чтобы им не умереть; и будет им это уставом вечным, ему и потомкам его в роды их.

**English:**

Exodus. Chapter 30.

17. And the LORD said unto Moses, Saying, 

18. Make a laver of brass for ablution, and a footstool of brass, and put it between the tabernacle of the congregation, and between the altar, and pour water into it:

19. And let Aaron and his sons wash their hands and their feet out of it:

20. The Lord said unto Moses. When they are to enter into the Tabernacle of the congregation, let them wash their hands and their feet with water, lest they die; or when they are to enter into the altar for the service of the sacrifice to the Lord;

21. Let them wash their hands and their feet with water, lest they die; and this shall be an everlasting statute unto them, unto him and his descendants in their generations.


**Russian:**

Собственно, все эти омовения, есть ни что иное, как уменьшение токопроводящей способности организма. Как и облачения, банально костюм для работы в зоне с повышенным зарядом статического электричества.

**English:**

Actually, all these ablutions are nothing but a decrease in the conductivity of the body. Like vestments, a simple suit for work in an area with a high charge of static electricity.

**Russian:**

Представьте наших энергетиков лет так через 1000, когда нынешний ток в проводах закончится. Как они нарядно будут муляжами божественных токоизмерительных клещей размахивать.

**English:**

Imagine our power engineers 1,000 years from now when the current in the wires has gone. How pretty they're gonna look as they swing the divine clamp meter in their rites.

**Russian:**

И да простят меня те кто обиделся.))))

**English:**

And may those who are offended forgive me. )))).

Vladimir Mamzerev. 27.12.2013 г.

© All rights reserved. The original author retains ownership and rights.

