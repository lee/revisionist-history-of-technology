title: Another history of the Earth - Part 1d
date: 2017-03-18 00:00
modified: 2022-12-14 16:04:21
category:
tags: catastrophe; Tartaria
slug:
authors: Dmitry Mylnikov
from: https://mylnikovdm.livejournal.com/220823.html
originally: Another history of the Earth Part 1d - Dmitry Mylnikov - LiveJournal.html
local: Another history of the Earth - Part 1d_files/
summary: Now let's see what we find along the Pacific coast. Let me remind you that the general scenario of the catastrophe is that a many kilometers high wall of water radiates out from the impact site in all directions. Below is a relief map of the continents and the seabed in the Pacific Ocean region, on which I marked the point of impact and the direction of the wave.
status: hidden

#### Translated from:

[https://mylnikovdm.livejournal.com/220823.html](https://mylnikovdm.livejournal.com/220823.html)

[First article in series (Russian)](http://mylnikovdm.livejournal.com/219459.html)

[First article in series (English)](./another-history-of-the-earth-part-1a.html)

Now let's see what we find along the Pacific coast. Let me remind you that the general scenario of the catastrophe is that a many kilometers high wall of water radiates out from the impact site in all directions. Below is a relief map of the continents and the seabed in the Pacific Ocean region, on which I marked the point of impact and the direction of the wave.

<!-- Local
[![](Another history of the Earth - Part 1d_files/165090_900.png#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165090/165090_original.png)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/165090/165090_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165090/165090_original.png)

I am not suggesting that all visible structures on the Pacific Ocean's coast and seabed were formed during this catastrophe. It goes without saying that a certain relief structure - faults, mountain ranges, islands, etc - already existed. But these structures should have been modified during this catastrophe both by a powerful wave of water and the new flows of magma that should have formed inside the Earth from the object's passage under the crust. And these influences should be strong enough that they should be visible on maps and images.

This is what we now see off the coast of Asia. I specially took a screenshot from the Google Earth program to minimize the distortion that occurs on the maps due to the projection of the Earth's sphere onto the map's flat plane.

<!-- Local
[![](Another history of the Earth - Part 1d_files/165255_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165255/165255_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/165255/165255_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165255/165255_original.jpg)

Looking at this image, you get the impression that some giant bulldozer crawled along the bottom of the Pacific Ocean from the entry point's crustal breakdown at the entry point to the shores of Japan and the ridge of the Kuril Islands, as well as the Commander and Aleutian Islands, which connect Kamchatka with Alaska. The force of a powerful shock wave smoothed out irregularities on the bottom and pushed down the edges of faults that already lined the coasts. It pressed the edges of faults, tilting the underlying plates to form ridges on the opposite side of the plates that almost reached the ocean surface and sometimes did so to become islands. Some of the islands may have been formed by volcanic activity after the cataclysm - volcanic activity that intensified along the entire length of the Pacific volcanic ring because of the event. But in any case, we can see that the wave spent most of its energy forming trenches. Where the wave went further, it was noticeably weaker. We do not see noticeable traces [of plate deformation] further on the coast. An exception is a small area of the Kamchatka coast, where part of the wave went through the Kamchatka Strait to the Bering Sea, forming a characteristic structure with a sharp drop in heights along the coast, though on a noticeably smaller scale.

<!-- Local
[![](Another history of the Earth - Part 1d_files/165393_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165393/165393_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/165393/165393_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165393/165393_original.jpg)

But on the eastern side we see a slightly different picture. There, apparently, the initial height of the ridge on which the Mariana Islands are located was lower than in the region of the Kuriles and the Aleutian Islands, so the wave lost only part of its energy and continued on.

<!-- Local
[![](Another history of the Earth - Part 1d_files/165677_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165677/165677_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/165677/165677_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/165677/165677_original.jpg)

As a result, around Taiwan and onwards towards Japan, as well as down along the Philippine Islands, we see a similar structure where the sea-bottom topography shows a sharp difference in elevation.

But the most interesting thing awaits us on the other side of the Pacific Ocean, off the coast of North and South America. This is what North America looks like on a topographic map:

<!-- Local
[![](Another history of the Earth - Part 1d_files/166011_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166011/166011_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/166011/166011_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166011/166011_original.jpg)

The Cordillera mountain range stretches along the entire Pacific coast. But most importantly, we do not see a smooth slope down to the ocean coast. In fact we are told that: "[The main mountain-forming processes that resulted in the formation of the Cordillera began in North America in the Jurassic period (wiki.ru)](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%80%D0%B4%D0%B8%D0%BB%D1%8C%D0%B5%D1%80%D1%8B)", which supposedly [ended 145 million years ago (wiki.ru)](https://ru.wikipedia.org/wiki/%D0%AE%D1%80%D1%81%D0%BA%D0%B8%D0%B9_%D0%BF%D0%B5%D1%80%D0%B8%D0%BE%D0%B4)... Then where are all those sedimentary rocks that are supposed to be formed by the erosion of mountains over 145 million years? Indeed, under the influence of water and wind, the mountains must constantly become smaller. Their slopes are gradually smoothed out and the products of washout and weathering gradually begin to smooth out the relief and, most importantly, be carried out by rivers to the ocean, forming a flatter coast. But in this case, almost everywhere we observe a very narrow coastal strip, or even the complete absence of it. And the coastal shelf is very narrow. Again there is a feeling that some giant bulldozer has grabbed everything from the Pacific Ocean and piled up the rampart that forms the Cordillera.

Exactly the same picture is observed on the Pacific coast of South America.

<!-- Local
[![](Another history of the Earth - Part 1d_files/166303_900.jpg#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166303/166303_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/166303/166303_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166303/166303_original.jpg)

The Andes - the Southern Cordillera - stretch in a continuous strip along the Pacific coast of South America. Moreover, here the elevation difference is much greater, and the coastline is even narrower than in North America. In contrast to the North America's west coast - where there is only a fault in the earth's crust and no associated deep-sea trench - there is a deep-sea trench off the coast of South America.

Here we come to another important point. The force of a shock wave - the tsunami in this case - decays with distance from the impact site. Therefore, we will see the strongest effects of a shock wave in the immediate vicinity of the Tamu massif, and in the area around Japan, Kamchatka and the Philippines. But off the coast of both Americas, the traces should be much weaker - especially off the coast of South America because it is farthest from the impact site. However, we see a completely different picture. The effect of the pressure exerted by a huge wall of water is most vividly observed just off the coast of South America. This means some other process created an even more powerful impact than the ocean shock wave created by the object's fall. Indeed, both continental American coasts offer a picture that is different to what we observer on the coast of Asia and the nearby large islands.

What else might such an impact and breakdown of the Earth's shell have done in addition to the consequences already described? Such an impact could not significantly slow down the rotation of the Earth around its axis, since if we start comparing the mass of the Earth and this object, and assume they possess similar densities, then the Earth is about 14 thousand times heavier than the object that hit it. Consequently, even in spite of its enormous speed, this object could not have any noticeable inhibitory effect on the rotation of the Earth. Moreover, most of the kinetic energy during the impact transformed into thermal energy which was spent heating and converting into plasma the matter of both the object and the Earth's body.

But the Earth is not a solid solid monolith. The outer shell is only about 40 km thick, while the total radius of the Earth is about 6,000 km. Further, under the Earth's hard shell, we have molten magma. The continental plates and ocean floor plates float on the surface of magma like ice floes float on the surface of water. Is it possible the Earth's crust shifted on impact? If we compare the mass of the Earth's crust and the mass of the object, then their ratio is only approximately 1:275. That is, the Earth's crust could receive some impulse from the object at the moment of impact. And this should have manifested itself in the form of very powerful earthquakes, which should have occurred across the entire surface of the Earth [as the crust moved]. Even so, the impact itself would hardly have been able to seriously move the hard shell of the Earth.

And now we recall that during the passage through Earth's magma, firstly, a similar shock wave should have formed as formed in the Pacific Ocean, and most importantly, a new magma flow should have formed along the line of the passage, which did not exist before. Various currents, ascending and descending flows inside the magma existed before the collision, but the general state of these flows and the continental and oceanic plates floating on them was more or less stable and balanced. And after the impact, this stable set of magma flows inside the Earth was disrupted by the appearance of a completely new flow, as a result of which practically all continental and oceanic plates had to start moving. Now let's look at the following diagram to understand how and where they were supposed to start moving.

<!-- Local
[![](Another history of the Earth - Part 1d_files/166605_900.png#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166605/166605_original.png)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/166605/166605_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166605/166605_original.png)

The impact is directed almost exactly against the direction of the Earth's rotation with a slight offset of 5 degrees from south to north. In this case, the newly formed magma flow will be maximum immediately after the impact, and then it will begin to gradually fade until the magma flow inside the Earth returns to a stable equilibrium state. Therefore, immediately after the impact, the earth's crust will experience the maximum inhibitory effect, the continents and the surface layer of magma will seem to slow down their rotation, and the core and the main part of the magma will continue to rotate at its original speed. And then, as the new flow weakens and its impact, the continents will again begin to rotate at the same speed along with the rest of the Earth. That is, the outer shell will seem to slip slightly right after the impact. Anyone who has worked with friction gears, such as belt drives, should be well aware of a similar effect when the drive shaft continues to rotate at its original speed while the mechanism it is driving through the belt and pulley rotates more slowly - or even stops - due to a large load. When we reduce the load the mechanism's rotation speed is restored and equalizes with the rotational speed of the drive shaft.

Now let's look at a similar circuit, but done from the other side of the Earth.

<!-- Local
[![](Another history of the Earth - Part 1d_files/166729_900.png#resized)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166729/166729_original.png)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/166729/166729_900.png#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/166729/166729_original.png)

Recently, a lot of work has appeared that collects and analyses evidence that quite recently the North Pole may have been sited in another place - presumably in the area of modern Greenland. In this diagram, I specifically showed the positions of the presumed earlier pole location and its current position. This to make clear which direction the shift took. In principle, the displacement of the continental plates that occurred after the described impact could well lead to a similar displacement of the earth's crust relative to the axis of rotation of the Earth. But we will discuss this point in more detail below. For now, we need to fix the fact that after the impact, due to the formation of a new flow of magma inside the Earth along the breakdown channel, there were two effects: a slowdown and slipping of the earth's crust, and a very powerful inertial wave within the Earth's magma. It will be much more powerful than the shock wave from the collision with the object, since it is not the shock movement of a volume of water equal to the 500 km diameter of the object, but the entire volume of water in the world's oceans. And it was this inertial wave that formed the picture that we see on the Pacific coasts of South and North America.

[*Continuation (Russian)*](http://mylnikovdm.livejournal.com/222351.html)

[*Continuation (English)*](./another-history-of-the-earth-part-1e.html)

© All rights reserved. The original author retains ownership and rights.

