title: Media of the Past - Part 5
date: 2014-04-03
modified: Thu 14 Jan 2021 12:30:46 GMT
category:
tags: technology
slug:
authors: Vladimir Mamzerev
from: https://pro-vladimir.livejournal.com/29661.html
originally: Media of the Past - Part 5.html
local: Media of the Past - Part 5_files/
summary: And it's funny - it doesn't occur to them (the museum curators) to hang these pots with their feet on ropes to the crossbeams, to hit them with the  bells and call it bell ringing.
status: published

#### Translated from:

[https://pro-vladimir.livejournal.com/29661.html](https://pro-vladimir.livejournal.com/29661.html)

**Russian:**

Носители информации прошлого. Часть 4. [http://pro-vladimir.livejournal.com/29387.html](http://pro-vladimir.livejournal.com/29387.html)

**English:**

Storage media of the past. Part 4. [http://pro-vladimir.livejournal.com/29387.html](http://pro-vladimir.livejournal.com/29387.html)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3Hx3JLqA23DSdbXHB5ZJoBJtVf0TiqxLR1Yt5KEl.jpeg#clickable)](Media of the Past - Part 5_files/bronze-pic4_zoom-1000x1000-98532.jpg)
-->

[![](http://img.gazeta.ru/files3/193/5718193/bronze-pic4_zoom-1000x1000-98532.jpg#clickable)](http://img.gazeta.ru/files3/193/5718193/bronze-pic4_zoom-1000x1000-98532.jpg)

**Russian:**

Несколько статей посвященных устройству детекторных приёмников. Ведь большая часть скепсиса, что технологических изделий, использующих электричество, быть не могло, базируется на том, что не найдены ни провода ни источники тока. Да и как так, если они были, то почему пришлось в таком случае переоткрывать всё это заново?

**English:**

Several articles on the construction of detector receivers. After all, most of the skepticism that there could not have been electrical technology in the past is based on the fact that no wires or current sources were found. And even if there were, why do we have to rediscover wires at all?

**Russian:**

Диод для детекторного приемника своими руками

[http://nnm.me/blogs/steadyinb/diod-dlya-detektornogo-priemnika-svoimi-rukami/](http://nnm.me/blogs/steadyinb/diod-dlya-detektornogo-priemnika-svoimi-rukami/)

**English:**

Diode for the detector receiver with your own hands

[http://nnm.me/blogs/steadyinb/diod-dlya-detektornogo-priemnika-svoimi-rukami/](http://nnm.me/blogs/steadyinb/diod-dlya-detektornogo-priemnika-svoimi-rukami/)

![Image no longer available](http://img12.nnm.me/e/a/9/2/d/5af85d337cac0ddd734c70a5a35_prev.jpg)

![Image no longer available](http://img11.nnm.me/d/8/9/1/d/c5e15a2dfbfd851c635a356bd66.jpg)

**Russian:**

Для того, чтобы детекторный приемник заработал, необходимо детектирование. Т.е. необходим диод. Статья о том как его сделать из подручных материалов самостоятельно.

**English:**

For a detector receiver to work, detection is required. I.e. a diode is required. Article about how to make it from improvised materials yourself.

**Russian:**

Начнем с получения галенита в домашних условиях, нам потребуется два вещества: Свинец (Pb) и Сера (S)

**English:**

To start with making galena at home, we need two substances: Lead (Pb) and Sulfur (S).

![Image no longer available](http://img12.nnm.me/6/2/4/f/f/4d914c26abe1ebd9a77249aa558.jpg)

![Image no longer available](http://Галенит%20—%20минерал,%20сульфид%20свинца(II).%20Химическая%20формула%20PbS.%20Синоним:%20свинцовый%20блеск.)

![Image no longer available](http://img12.nnm.me/1/a/8/2/3/9b8aae70029028f21e1ace50fd7.jpg)

**Russian:**

Свинец

**English:**

Lead

![Image no longer available](http://img11.nnm.me/e/e/8/d/f/ded34b113771c179aa936038ff5.jpg)

**Russian:**

Сера

**English:**

Sulfur

**Russian:**

Свинец - желательно чистый, но если найти затруднительно сгодится и свинец из автомобильного аккумулятора (с худшими результатами, так как содержит химические примеси). После того как кусок свинца найден - при помощи крупного напильника получаем из него необходимый нам порошок. Сера нужна тоже в виде порошка, если порошок найти затруднительно - размалываем кусочек серы вручную, проще всего это сделать в небольшой фарфоровой посудине, просто растолочь. Для экспериментов достаточно небольшого количества, 2-3 см в пробирке каждого вещества - достаточно.

**English:**

Lead - preferably pure, but if it is difficult to find, then lead from a car battery will also work (with worse results, because it contains chemical impurities). Once a piece of lead is found - with the help of a coarse file we get from it the powder we need. Sulphur is also needed in powder form, if the powder is difficult to find - we grind a piece of sulphur by hand, it is easiest to do it in a small porcelain dish, just grind it. For experiments a small amount is enough, 2-3 cm of each substance in a test tube is enough.

![Image no longer available](http://img11.nnm.me/d/d/3/7/5/8a7eceb90a738c2376579f1c24d.jpg)

**Russian:**

После того как порошки приготовлены - их нужно смешать, в соотношении на 1 грамм серы - 5 грамм свинца. Лучше делать смеси небольшими порциями (так как возможно придется провести не одну плавку), применяя электронные весы с точностью 0,1 грамм. Если весов нет - делаем на глазок, примерно на 4 части свинца 1 часть серы, здесь можно немного поэкспериментировать. Полученную смесь помещаем в пробирку и слегка утрамбовываем, достаточно 2-3 см.

**English:**

Once the powders are prepared - they should be mixed, in a ratio of 1 gram of sulfur to 5 grams of lead. It is better to make mixtures in small portions because it may be necessary to make more than one melt. Use electronic scales with an accuracy of 0.1 grams. If you don't have scales - do it by eye: about 4 parts lead 1 part sulfur. Here you can experiment a bit. The resulting mixture is placed in a test tube and tamped a little. 2-3 cm is enough.

![Image no longer available](http://img11.nnm.me/c/1/4/2/6/045d7da2b7a3d4670b1e72dcd9a_prev.jpg)

**Russian:**

Теперь, используя сухой спирт, постепенно прогреваем пробирку

**English:**

Now, using alcohol spirit, gradually warm up the test tube

![Image no longer available](http://img11.nnm.me/8/f/4/5/f/40ea7c74fb8316e7c14e3636987_prev.jpg)

**Russian:**

после того как начал плавиться свинец (этот момент как раз видно на фото), переносим пробирку в более горячий участок, пока не начнет плавиться сера. Практически сразу после того как сера начала плавиться - наступает реакция, при которой смесь раскаляется до красна. Процесс начинается в самом низу, где наибольшая температура расплава. Как только реакция началась - пробирку можно вынести из пламени и после того как реакция прекратится - подождать пока расплав остынет. После того как расплав остыл - осторожно раскалываем пробирку (если она сама не раскололась в процессе реакции :)

**English:**

After the lead began to melt (this moment can be seen in the photo), we move the test tube to a hotter area, until the sulfur begins to melt. Almost immediately after the sulfur begins to melt, there is a reaction in which the mixture becomes red-hot. The process begins at the very bottom, where the melt temperature is highest. Once the reaction has begun the test tube can be removed from the flame and after the reaction has stopped wait for the melt to cool down. After the melt has cooled down carefully split the test tube - if it has not itself cracked during the reaction :)

**Russian:**

Полученная таким образом масса серо-стального цвета и есть галенит.

**English:**

The resulting steel-gray color substance is galena.

![Image no longer available](http://img11.nnm.me/b/8/e/9/3/59062910a73e077059596a3e186_prev.jpg)

**Russian:**

Раскалываем полученную массу. Если внутри пробирки серая масса, которая очень легко крошится - плавка не удалась, если она имеет темный цвет, то скорее всего в смеси было слишком много серы. В этом случае повторяем эксперимент, при необходимости уменьшая серы.

**English:**

We split the obtained mass. If the mass inside the tube is gray and crumbles easily, the melting failed. If it has a dark color, it is likely that there was too much sulfur in the mixture. In that case, repeat the experiment, reducing the sulfur if necessary.

![Image no longer available](http://img12.nnm.me/c/0/2/3/9/829fa93002c7b6d072badf58939_prev.jpg)

**Russian:**

После того, как галенит получен - надо его испытать. Хорошо, если у вас есть кристаллодержатель, если нет - придется что-нибудь изобрести. Я использовал кусочек жести, на который была наплавлена большая капля олова, и в эту каплю опускался кристалл, далее из медной проволоки делается держатель обычной швейной иглы и все это хозяйство подключается к детекторному приемнику, например так:

**English:**

After the galena (halenite) is obtained you need to test it. It's good if you have a crystal holder, but if you don't, you'll have to invent something (to create a cat's whisker). I used a piece of tin, on which a big drop of tin was deposited, and the crystal was lowered on to this drop. Then I made a holder - a clamp - for an ordinary sewing needle from a copper wire. This set up is then connected to the detector receiver. For example, like this:

![Image no longer available](http://img12.nnm.me/e/a/9/2/d/5af85d337cac0ddd734c70a5a35_prev.jpg)

![Image no longer available](http://img12.nnm.me/7/8/0/e/d/63c8a25aaea44edef0aab7128be_prev.jpg)

**Russian:**

Остается подключить антенну и заземление, и найти рабочую точку на кристалле :) Для сравнения, полученный кристалл работает не хуже (был момент когда мне показалось что даже лучше, возможно показалось) обычных для детекторного приемника диодов - Д2 или Д9.

**English:**

It remains to connect the antenna and ground, and find the operating point on the crystal :) For comparison, the crystal works no worse (there was a moment when it seemed to me even better, maybe it *was* better) than diodes for the detector receiver - D2 or D9.

**Russian:**

Напроследок немного юмора:

**English:**

Finally, a little humor:

![Image no longer available](http://img11.nnm.me/5/6/6/5/e/364efa027494c48e18e7e9f529a.jpg)

**Russian:**

Схема детекторного приемника с вариометром (диапазон от 300 до 1800 м)

**English:**

Detector Receiver Schematic with Variometer (300 to 1800 m range)

[http://radiolamp.net/news/330-sxema-detektornogo-priemnika-s-variometrom-diapazon-ot-300-do-1800-m.html](http://radiolamp.net/news/330-sxema-detektornogo-priemnika-s-variometrom-diapazon-ot-300-do-1800-m.html)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H5cmsX__d8FebhaKmDgvKChU0MHsGNhdymNtT384.png#clickable)](Media of the Past - Part 5_files/detefon-10.png)
-->

[![](http://radiolamp.net/uploads/Image/schemes/receivers/detefon-10.png#clickable)](http://radiolamp.net/uploads/Image/schemes/receivers/detefon-10.png)

**Russian:**

СОВРЕМЕННЫЙ ДЕТЕКТОРНЫЙ ПРИЕМНИК

**English:**

MODERN DETECTOR RECEIVER

[http://ham.koshkavdome.com/sovremennyj-detektornyj-priemnik/](http://ham.koshkavdome.com/sovremennyj-detektornyj-priemnik/)

**Russian:**

Детекторные приемники, которые изготавливали в прошлом, требовали для работы хорошую антенну и заземление. В качестве детектора использовался германиевый диод 1N34A. В настоящее время конструируют детекторные приемники, которые не нуждаются во внешней антенне.

**English:**

The detector receivers that were made in the past required a good antenna and grounding to work. A 1N34A germanium diode was used as a detector. Nowadays we construct detector receivers which do not need an external antenna.

**Russian:**

Они работают и с комнатными антеннами. Кто конструировал детекторные приемники, тот знают, что необходимо искать компромиссное решение между избирательностью и чувствительностью. Для повышения чувствительности детектор подключается ко всему колебательному контуру (см. рис.1).

**English:**

They also work with room antennas. Those who have designed detector receivers know that a compromise must be made between selectivity and sensitivity. To increase the sensitivity, the detector is connected to the whole oscillating circuit (see Fig. 1).

![Image no longer available](http://ham.koshkavdome.com/wp-content/uploads/2013/02/%D1%80%D0%B8%D1%811.bmp)

**Russian:**

Рисунок 1

**English:**

Figure 1

**Russian:**

Для увеличения избирательности необходимо подключаться к части контура. В этом случае добротность контура будет выше. Добиться одновременно хорошей чувствительности и избирательности можно, если использовать в качестве детектора полевой транзистор с двумя затворами, например ALD110900A. На рис.2 представлена схема разработанная Дэвидом Крайком (KC3ZQ).

**English:**

To increase the selectivity it is necessary to connect to part of the loop. In this case the quality factor ('Q factor') of the circuit will be higher. You can get good sensitivity and selectivity at the same time by using a double gate FET, like the ALD110900A. Figure 2 shows the circuit designed by David Cripe (KC3ZQ).

![Image no longer available](http://ham.koshkavdome.com/wp-content/uploads/2013/02/%D1%80%D0%B8%D1%812.jpg)

Translator's note: Probably refers to *D. W. Cripe, KC3ZQ, "Nostalgia For The Future," 73 Amateur Radio Today*, Dec 1995, pp 14-16, which is mentioned [and developed in this ARRL article](http://www.arrl.org/files/file/Technology/tis/info/pdf/culter.pdf).

**Russian:**

Рисунок 2

**English:**

Figure 2

**Russian:**

Он использовал ферритовую антенну и переменную емкость в качестве колебательного контура. Один из электродов полевого транзистора, который использовался для детектирования, подключался к 8-му витку намотки антенны, что составляло 11% от общего количества намотки. Конденсатор С2 предназначен для сглаживания пульсаций высокочастотных колебания, а для звуковых колебаний этот конденсатор представляет большое сопротивление. Полевой транзистор имеет большое сопротивление, а телефон -- маленькое. Для согласования этих элементов используется трансформатор Т1. Магнитная антенна берется от транзисторного приемника. Антенну можно изготовить и самому используя феррит длиной 20 см. и диаметром 12 мм. На ферритовый стержень наматываются две обмотки 70 витков и 8 витков. В качестве С1 используется высокодобротный переменный конденсатор емкостью 365 пф. от старого лампового приемника. Параметры приемника зависят и от наушников, которые используются для его прослушивания. Современные стереонаушники имеют сопротивление от 8 до 64 ом. Использование таких наушников понизит чувствительность приемника. Лучше использовать старые наушники с сопротивлением в 150 ом. Однако эти наушники ограничены по частоте воспроизведения пределом в 3 кГц. Более высокие частоты не слышны. Если использовать наушники в несколько ком, то отпадет необходимость в трансформаторе, параметры которого не известны. Более чувствительным и избирательным приемник получается, если использовать не ферритовую антенну, а рамочную. Был создан приемник с рамочной антенной диаметром 30 см. Для работы такого приемника достаточно мощности сигнала равной 3.3 микроватт. Радиостанция мощностью 5 киловатт будет слышна на расстоянии 3-х миль. На рис. 3 представлен приемник для коротких волн от 40 до 90 метров. Диаметр катушки колебательного контура равняется 5 см. Количество витков составило 19.5, плотность намотки составило 10 витков на 2.5 см. Толщина провода составляет порядка1 мм. Добротность контурной катушки равнялась 44 единицам.

**English:**

He used a ferrite antenna and a variable capacitor as an oscillating circuit. One of the electrodes of the field-effect transistor, which was used for detection, was connected to the 8th winding of the antenna, which was 11% of the total winding. Capacitor C2 is designed to smooth out the ripple of high frequency oscillations, and for audio oscillations this capacitor presents a large resistance. The field-effect transistor has a large resistance and the telephone has a small resistance. A T1 transformer is used to match these elements. The magnetic antenna is taken from a transistor receiver. You can also make the antenna yourself using a 20 cm long ferrite rod with a diameter of 12 mm. On the ferrite rod, two windings of 70 turns and 8 turns are wound. As C1, a 365 pF high quality variable capacitor from an old tube receiver is used. The parameters of the receiver also depend on the headphones used to listen to it. Modern stereo headphones have impedance from 8 to 64 ohms. Using such headphones will lower the sensitivity of the receiver. It is better to use older headphones with an impedance of 150 ohms. However, these headphones are limited in frequency playback to a limit of 3 kHz. Higher frequencies are not audible. If you use high impedance headphones, there is no need for a transformer, the parameters of which are not known. The receiver is more sensitive and selective if a loop antenna rather than a ferrite antenna is used. A receiver with a 30 cm loop antenna was designed. The signal power of 3.3 microwatts is sufficient for operation of this receiver. A radio station with 5 kilowatts of power will be heard at a distance of 3 miles. Figure. 3 is a receiver for short waves from 40 to 90 meters. The diameter of the oscillating circuit coil is 5 cm. The number of turns was 19.5, the winding density was 10 turns per 2.5 cm. The thickness of wire is about 1 mm. Quality factor of the circuit coil was equal to 44 units.

![Image no longer available](http://ham.koshkavdome.com/wp-content/uploads/2013/02/%D1%80%D0%B8%D1%813.jpg)

**Russian:**

Рисунок 3

**English:**

Figure 3

**Russian:**

Автор использовал внешнюю антенну длиной 12 метров, которая подключалась к контурной катушке через индуктивность L2. Эта индуктивность представляла собой ферритовое кольцо диаметром 50 мм и внутренним диаметром 43 мм. толщина не известна, на который были намотаны 9 витков провода толщиной порядка 0.5 мм.

**English:**

The author used an external antenna 12 meters long, which was connected to the loop coil through the inductance L2. This inductance was a ferrite ring with a diameter of 50 mm and an inner diameter of 43 mm. The thickness is not known, on which 9 turns of wire with a thickness of about 0.5 mm were wound.

**Russian:**

Кто-то спросит, и к чему весь это перечень схем? и что такого в этом детекторном приёмнике, что про него вспомнили? а что его отличает в своей сути всех остальных приборов работающих от привычного нам электричества? Не уж то отсутствие привычного нам источника тока?

**English:**

One may ask, why all this list of circuits, and what's so important about this detector receiver that it was mentioned? And what makes it different from all the other appliances that work with electricity that we're accustomed to? Is it the absence of the current source we are accustomed to?

**Russian:**

Мастер-класс, Заводной телевизор, Сделай сам, Сергей Апресов, отсюда: [http://zateevo.ru/?section=page&alias=popmeh_dec08](http://zateevo.ru/?section=page&alias=popmeh_dec08)

**English:**

Master class, DIY Clockwork TV, Sergey Apresov From here: [http://zateevo.ru/?section=page&alias=popmeh_dec08](http://zateevo.ru/?section=page&alias=popmeh_dec08)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3Hz-GvMl8_6AMx8zNssm44CRXCND8pwA-njUYwKA.jpeg#clickable)](Media of the Past - Part 5_files/12810.jpg)
-->

[![](http://zateevo.ru/userfiles/image/Biblio/Popmeh/1208/12810.jpg#clickable)](http://zateevo.ru/userfiles/image/Biblio/Popmeh/1208/12810.jpg)

**Russian:**

Вероятно, многие наши читатели собирали в детстве радиоприемник. Или передатчик, или магнитофон, или гитарный усилитель, или музыкальный звонок. По схеме или даже собственной конструкции, из конструктора или из подручных материалов. А вот телевизор собирали немногие.

**English:**

Many of our readers probably assembled a radio when they were children. Or a transmitter, or a tape recorder, or a guitar amplifier, or a musical bell. According to a circuit diagram or even of their own design, from a construction set, or from improvised materials. But not many people assembled TV sets.

**Russian:**

Дело вовсе не в сложности его устройства: усилители высокочастотного и низкочастотного сигнала, генераторы строчной и кадровой развертки -- это по сути все те же магнитофоны и звонки. Просто собирать телевизор неинтересно и обидно. Каким бы искусным мастером ты ни был, все равно центральное место в его конструкции занимает Его Величество Кинескоп -- загадочная обитель грозной электронной пушки, вечного странника сканирующего луча и магического люминофорного экрана, которые могут быть собраны воедино и заключены в вакуумную трубку только в промышленных условиях. Все электронные блоки телевизора, которые можно собрать своими руками, -- лишь его верная свита.

**English:**

It is not about the complexity of its construction: high-frequency and low-frequency signal amplifiers, line and frame generators are essentially the same as tape recorders and bells. Simply assembling a TV set is not interesting and is frustrating. No matter how sophisticated the craftsman, the central place in its construction is occupied by His Majesty the Kinescope (Cathode Ray Tube, CRT), the mysterious abode of the formidable electron gun, the eternal harbinger of the scanning beam and the magic phosphor screen, which can be put together and enclosed in a vacuum tube only in industrial conditions. All the electronic components of a television set which can be assembled by hand are but its faithful retinue.

**Russian:**

На самом деле первые телевизоры обходились без всяких вакуумных трубок. Телекамеры и телеприемники с механической разверткой, которые впервые продемонстрировал публике британец Джон Лоджи Бэйрд в 1926 году, применялись для организации эфирного телевещания в течение 11 лет -- с 1928 по 1939 год. В последние три года механические телевизоры на равных сосуществовали с первыми кинескопными. Чтобы разобраться, как работали первые телевизоры, а заодно осуществить свою заветную детскую мечту, мы решили построить передатчик и приемник движущегося изображения своими руками, буквально из того, что попалось под руку. Предлагаем и вам повторить наш опыт, учтя наши ошибки.

**English:**

In fact, the first televisions did without any vacuum tubes. The mechanical sweep TV cameras and receivers, first introduced to the public by Briton John Logie Baird in 1926, were used for broadcasting television for 11 years, from 1928 to 1939. In the last three years, mechanical televisions coexisted on an equal footing with the first kinescope (CRT) televisions. To understand how the first television sets worked and to fulfil a childhood dream, we have decided to build a moving image transmitter and receiver with our own hands, literally from whatever came to hand. We suggest that you repeat our experience, taking into account our mistakes.

**Russian:**

далее отсюда: [http://mobbit.info/item/2007/11/22/istoriya-televizorov-v-kartinkah-25-foto](http://mobbit.info/item/2007/11/22/istoriya-televizorov-v-kartinkah-25-foto)

**English:**

from here on out: [http://mobbit.info/item/2007/11/22/istoriya-televizorov-v-kartinkah-25-foto](http://mobbit.info/item/2007/11/22/istoriya-televizorov-v-kartinkah-25-foto)

**Russian:**

Французский Semivisor 1929 года выпуска.

**English:**

A 1929 French Semivisor.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H0pZcgIcO7gxIyLA6l7ZeiEpIDKXvW217gqFNWb.jpeg#clickable)](Media of the Past - Part 5_files/vintage_TVs_1.jpg)
-->

[![](http://mobbit.info/media/3/vintage_TVs_1.jpg#clickable)](http://mobbit.info/media/3/vintage_TVs_1.jpg)

**Russian:**

Механический телевизор Baird Televisor 1930 года английского производства.

**English:**

A 1930 Baird Televisor mechanical television made in England.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H0pZcgIcO7gxIyLA6l7ZeiEpIDKXvW217gq_004.jpeg#clickable)](Media of the Past - Part 5_files/vintage_TVs_2.jpg)
-->

[![](http://mobbit.info/media/3/vintage_TVs_2.jpg#clickable)](http://mobbit.info/media/3/vintage_TVs_2.jpg)

**Russian:**

Советские телевизоры В2 (на первом фото) и Пионер-TM3 1934 года выпуска.

**English:**

Soviet TV sets: V2 (on the first photo) and Pioneer-TM3 of 1934.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H0pZcgIcO7gxIyLA6l7ZeiEpIDKXvW217gq_002.jpeg#clickable)](Media of the Past - Part 5_files/vintage_TVs_3.jpg)
-->

[![](http://mobbit.info/media/3/vintage_TVs_3.jpg#clickable)](http://mobbit.info/media/3/vintage_TVs_3.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H0pZcgIcO7gxIyLA6l7ZeiEpIDKXvW217gq_003.jpeg#clickable)](Media of the Past - Part 5_files/vintage_TVs_4.jpg)
-->

[![](http://mobbit.info/media/3/vintage_TVs_4.jpg#clickable)](http://mobbit.info/media/3/vintage_TVs_4.jpg)

**Russian:**

Fraccaro 30 Line Set - итальянский механический телевизор 30-х годов.

**English:**

The Fraccaro 30 Line Set is an Italian 30's mechanical television.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H0pZcgIcO7gxIyLA6l7ZeiEpIDKXvW217gq_005.jpeg#clickable)](Media of the Past - Part 5_files/vintage_TVs_5.jpg)
-->

[![](http://mobbit.info/media/3/vintage_TVs_5.jpg#clickable)](http://mobbit.info/media/3/vintage_TVs_5.jpg)

[http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/technology/item110.php](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/technology/item110.php)

**Russian:**

Механическое ТВ: технический аттракцион

**English:**

Mechanical TV: A technical attraction

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H6dl7PmFJBRoLP1CwjVVLfr3FVkIVHhhCl_KiR0.html)](Media of the Past - Part 5_files/03284aca540901344be31ae7d8a20a9e.jpg)
-->

[![Image no longer available](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/iblock/032/03284aca540901344be31ae7d8a20a9e.jpg#clickable)](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/iblock/032/03284aca540901344be31ae7d8a20a9e.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H6dl7PmFJBRoLP1CwjVVLfr3FVkIVHhhCl__002.html)](Media of the Past - Part 5_files/mechanic-tv-infogr.jpg)
-->

[![Image no longer available](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/images/mechanic-tv-infogr.jpg#clickable)](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/images/mechanic-tv-infogr.jpg)

**Russian:**

Следует заметить, что еще в 1840 году изображения могли передаваться по телеграфу, а Нипков лишь значительно упростил процесс кодирования и декодирования изображения. В 1885 году Нипков, потратив все свои сбережения, получил патент на изобретение электрического телескопа для воспроизведения светящихся объектов, однако разработать это устройство немецкий изобретатель так и не смог. Через 15 лет патент был отозван, а сам Нипков получил должность конструктора в институте Берлина и больше не интересовался темой передачи изображений.

**English:**

It should be noted that images could be transmitted by telegraph as early as 1840 and Nipkov only greatly simplified the process of coding and decoding the image. In 1885, Nipkov, having spent all his savings, received a patent for the invention of an electric telescope to reproduce luminous objects. But the German inventor was not able to develop this device. After 15 years the patent was revoked and Nipkov himself was promoted to designer at an institute in Berlin and was no longer interested in the subject of image transmission.

**Russian:**

Первый прибор, работающий по принципу оптико-механической развертки, придумал шотландский инженер Джон Лоджи Берд. В 1926 году он продемонстрировал передачу движущегося изображения членам Королевского института Великобритании. Разумеется, это не была современная «телевизионная картинка», на ней присутствовали лишь силуэты, но начало было положено. Спустя год Берд увеличивает количество отверстий на диске Нипкова до 30-ти.

**English:**

The first device working on the principle of optical-mechanical scanning was invented by the Scottish engineer John Logie Baird. In 1926, he demonstrated the transmission of a moving image to the members of the Royal Institution of Great Britain. Of course, it was not a modern "TV picture", there were only silhouettes, but the beginning was made. A year later, Byrd increased the number of holes on Nipkow's disk to 30.


**Russian:**

Устройство на основе диска Нипкова работало по следующему принципу: объектив фокусирует изображение на кадровом окне, мимо которого пробегает край диска. Отверстия диска сканируют по мере своего движения весь кадр и прочеркивают его горизонтальными строчками. Затем процесс повторяется. За диском стоит линза, которая фокусирует прошедший через отверстия свет на фотоэлементе. Колебания яркости фотоэлемент преобразует в последовательность электрических импульсов, которые по радио передаются к приемникам. На приемной станции также устанавливался аналогичный диск Нипкова между источником света и зрителем. Кстати, сам Нипков увидел практическое применение своего изобретения лишь в 1928 году. «Наконец я могу быть спокойным. Я видел мерцающую поверхность, на которой что-то двигалось, хотя нельзя было различить, что именно», - поделился своими впечатлениями Пауль Нипков от просмотра механического телевизора.

**English:**

The device based on the Nipkov disk worked according to the following principle: the lens focuses the image on the frame window, past which the edge of the disk runs. The disc's apertures scan the entire frame as it moves and draw horizontal lines through it. The process is then repeated. Behind the disc is a lens that focuses the light that has passed through the holes onto the photoelectric cell. The fluctuations in brightness are converted by the photoelectric cell into a sequence of electrical impulses which are transmitted by radio to receivers. At the receiving station, a similar Nipkov disk was also installed between the light source and the viewer. Incidentally, Nipkov himself did not see the practical application of his invention until 1928. Paul Nipkov shared his impressions of watching a mechanical television:

> At last I could be calm. I saw a shimmering surface on which something was moving, although it was impossible to distinguish what exactly.

**Russian:**

В 1929-1931 годах в ведущих странах мира стартовало опытное телевизионное вещание с механической системой развертки. В России первые передачи увидели свет в 1931 году, а 1 октября того же года началось регулярное вещание. Правда, у людей не было телевизоров, поэтому осуществлялись коллективные просмотры. Затем, покупая диск Нипкова, стали делать самодельные механические телевизоры: простейшее устройство для сканирования изображения собиралось из двигателя, вращающего диск Нипкова, небольшого контейнера с одним фотоэлектрическим элементом и обычным объективом для проецирования изображения.

**English:**

In 1929-1931, experimental television broadcasting with a mechanical scanning system was launched in the leading countries of the world. In Russia, the first programs saw the light in 1931, and regular broadcasting began on October 1 of the same year. It is true that people did not have TV sets, so there were collective viewings. Then, buying a Nipkov disk, began to make homemade mechanical TVs. The simplest device for image scanning was assembled from a motor that rotates the Nipkov disk, a small container with one photoelectric element and a conventional lens for image projection.

**Russian:**

Вскоре механическое телевидение стало доступно всем. Но существовал один недостаток -- очень низкое качество изображения. Другого на столь маленьком экране и быть не могло. Например, чтобы увеличить экран до размера средней фотографии (9x12 см), диск в телекамере должен был быть более двух метров в диаметре. Это все было не очень удобно и выгодно. А в кругах скептиков термин «телевидение» превратился в «елевидение». В декабре 1933 года передачи механического ТВ прекратились, а более перспективным было признано электронное телевидение. Однако вскоре выяснилось, что промышленность еще не освоила новую электронную аппаратуру, поэтому в феврале 1934 года механическое ТВ вернулось в эфир.

**English:**

Soon mechanical television was available to everyone. But there was one drawback: very poor picture quality. It was impossible to do otherwise on such a small screen. For example, to enlarge the screen to the size of an average photograph (9x12 cm), the disk in the camera had to be more than two meters in diameter. All this was not very convenient and profitable. And in skeptic circles, the term "television" became "elevision". In December 1933, mechanical TV transmissions ceased, and electronic television was considered more promising. However, it soon became clear that the industry had not yet mastered the new electronic equipment, so in February 1934, mechanical TV returned to the airwaves.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H6dl7PmFJBRoLP1CwjVVLfr3FVkIVHhhCl__003.html)](Media of the Past - Part 5_files/mechanic-tv-dop.jpg)
-->

[![Image no longer available](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/images/mechanic-tv-dop.jpg#clickable)](http://xn--e1aaatbcqh4abgd.xn--p1aadc.xn--p1ai/upload/images/mechanic-tv-dop.jpg)

**Russian:**

Конечно, многим было понятно, что вещание с механической разверткой, по большей части, интересный технический аттракцион. Так или иначе, должна была настать эра электронного ТВ. С запуском нового телецентра на Шаболовке, основанного уже на электронных принципах, регулярные передачи механического телевидения из Москвы прекратились. В период с 1936 по 1940 годы в большинстве развитых стран начались опытные телевизионные передачи через электронные системы ТВ, которые в итоге отодвинули механическое телевидение в сторону.

**English:**

Of course, it was clear to many that mechanical sweep broadcasting was, for the most part, limited to being an interesting technical attraction. One way or another, the era of electronic TV was to come. With the launch of the new TV centre on Shabolovka, which was already based on electronic principles, regular broadcasts of mechanical television from Moscow ceased. Between 1936 and 1940 most of the developed countries began experimental television broadcasting through electronic TV systems, which eventually pushed mechanical television aside.

[http://ru.wikipedia.org/wiki/%D0%94%D0%B8%D1%81%D0%BA_%D0%9D%D0%B8%D0%BF%D0%BA%D0%BE%D0%B2%D0%B0](http://ru.wikipedia.org/wiki/%D0%94%D0%B8%D1%81%D0%BA_%D0%9D%D0%B8%D0%BF%D0%BA%D0%BE%D0%B2%D0%B0)

**Russian:**

> Диск Нипкова (англ. Nipkow disk) - механическое устройство для сканирования изображений, изобретённое Паулем Нипковым в 1884 году[1]. Этот диск являлся неотъемлемой частью многих систем механического телевидения вплоть до 1930-х годов.

**English:**

> The Nipkow disk was a mechanical image scanning device invented by Paul Nipkow in 1884[1]. This disk was an integral part of many mechanical television systems until the 1930s.

<!-- Local
[![](Media of the Past - Part 5_files/560px-Nipkov_disk.png#clickable)](Media of the Past - Part 5_files/560px-Nipkov_disk.svg.png)
-->

[![](http://upload.wikimedia.org/wikipedia/ru/thumb/7/73/Nipkov_disk.svg/560px-Nipkov_disk.svg.png#clickable)](http://upload.wikimedia.org/wikipedia/ru/thumb/7/73/Nipkov_disk.svg/560px-Nipkov_disk.svg.png)

**Russian:**

отсюда: [http://zlostick.livejournal.com/39922.html?thread=204786](http://zlostick.livejournal.com/39922.html?thread=204786)

**English:**

From here: [http://zlostick.livejournal.com/39922.html?thread=204786](http://zlostick.livejournal.com/39922.html?thread=204786)

**Russian:**

Механический телевизор, 300 строк изображения, размер изобржения 12х16мм затем увеличивался линзой до размера 27х35мм

**English:**

Mechanical TV, 300 lines, 12x16mm picture size then enlarged to 27x35mm with a lens

<!-- Local
[![](Media of the Past - Part 5_files/IMG_4210.html)](Media of the Past - Part 5_files/IMG_4210.jpg)
-->

[![](http://i1172.photobucket.com/albums/r565/zlostick/Museum_of_Communications/IMG_4210.jpg#clickable)](http://i1172.photobucket.com/albums/r565/zlostick/Museum_of_Communications/IMG_4210.jpg)

**Russian:**

Вот такую картинку он выдает

**English:**

This is the picture it provides:

<!-- Local
[![](Media of the Past - Part 5_files/IMG_4489.html)](Media of the Past - Part 5_files/IMG_4489.jpg)
-->

[![](http://i1172.photobucket.com/albums/r565/zlostick/Museum_of_Communications/IMG_4489.jpg#clickable)](http://i1172.photobucket.com/albums/r565/zlostick/Museum_of_Communications/IMG_4489.jpg)

[http://popleteev.com/projects/nbtv/ru](http://popleteev.com/projects/nbtv/ru)

**Russian:**

Видеоплеер из подручных материалов

**English:**

Video player from improvised materials

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H5vh4irt77uXTx_Pt6tls0aIehjxNIyGC1VSbDEt.png#clickable)](Media of the Past - Part 5_files/architecture-ru.png)
-->

[![](http://popleteev.com/projects/nbtv/img/architecture-ru.png#clickable)](http://popleteev.com/projects/nbtv/img/architecture-ru.png)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H5vh4irt77uXTx_Pt6tls0aIehjxNIyGC1VSbDE.jpeg#clickable)](Media of the Past - Part 5_files/camera-thumb.jpg)
-->

[![](http://popleteev.com/projects/nbtv/img/camera-thumb.jpg#clickable)](http://popleteev.com/projects/nbtv/img/camera-thumb.jpg)

[http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html](http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html)

**Russian:**

Ранним предшественником телевидения следует считать копирующий телеграф Александра Бена, на который он получил патент в 1843 году. Основу отправляющего и принимающего аппаратов составляли здесь сургучно-металлические пластины, устроенные особым образом. Для их изготовления Бен брал изолированную проволоку, резал ее на куски длиной 2,5 см и плотно набивал ими прямоугольную раму, так чтобы отрезки проволоки были параллельны друг Другу, а их торцы располагались в двух плоскостях. Затем он заливал рамку жидким сургучом, остужал и полировал ее с обеих сторон до получения гладких диэлектрических поверхностей с металлическими вкраплениями.

**English:**

Alexander Bain's copying telegraph should be considered an early forerunner of television, for which he received a patent in 1843. The basis of the sending and receiving apparatus were wax-metal plates arranged in a special way. In order to make them, Ben took an insulated wire cut into 2.5 cm long pieces and densely packed them into a rectangular frame so that the wire segments were parallel to each other and their ends were positioned in two planes. Then he poured liquid sealing wax on the frame, cooled it down and polished it on both sides until smooth dielectric surfaces with metallic inclusions were obtained.

**Russian:**

Аппарат Бена был пригоден для передачи изображений с металлических клише или с металлических типографских литер. Если металлическое клише или типографский шрифт прижимали к одной из сторон металлосургучной пластины передающего аппарата, то часть проволок оказывалась электрически замкнута между собой и получала контакт с участком цепи. подводимым к шрифту и к источнику тока. Этот контакт переходил и на концы тех же проволок с противоположной стороны пластины. Одновременно к аналогичной пластине приемного аппарата прикладывали лист влажной бумаги, предварительно пропитанной солями калия и натрия, которая была способна изменять свою окраску под действием электрического тока.

**English:**

Ben's apparatus was suitable for transmitting images from metal clichés or from metal typefaces. If the metal cliché or the typographic type was pressed to one side of the metal-surgical plate of the transmitting device, a part of wires appeared to be electrically closed between themselves and received the contact with a section of a circuit brought to a font and to a current source. This contact was also transferred to the ends of the same wires on the opposite side of the plate. At the same time, a sheet of wet paper preimpregnated with potassium and sodium salts, which was capable of changing its color under the influence of electric current, was attached to the similar plate of the receiving apparatus.

**Russian:**

далее тут: [http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html](http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html)

**English:**

More here: [http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html](http://www.oldradioclub.ru/radioelbook/hystrory/art_012.html)

**Russian:**

История фотоаппарата: [http://www.medn.ru/statyi/Istoriyafotoapparata.html](http://www.medn.ru/statyi/Istoriyafotoapparata.html)

**English:**

History of the camera: [http://www.medn.ru/statyi/Istoriyafotoapparata.html](http://www.medn.ru/statyi/Istoriyafotoapparata.html)

**Russian:**

елевидение возникло как результат более чем десятка различных открытий и изобретений. Но основоположником «кумира XX века» - электронного телевидения - считается Борис Львович Розинг, профессор Петербургского технологического института. Именно он еще в начале нашего века высказал идею об использовании электронно-лучевой трубки для передачи изображений на расстояние. В 1907 году он продемонстрировал «дальновидение», правда, на весьма скромное расстояние - в пределах одной комнаты. Неподвижное изображение передавалось с помощью зеркал, которые вращал мотор. Зеркала «осматривали» объект и в зависимости от его яркости регулировали сигналы, передаваемые приемнику. В приемнике изображение «собиралось» с помощью другой системы зеркал. Источник: [http://www.medn.ru/statyi/Istoriyafotoapparata.html](http://www.medn.ru/statyi/Istoriyafotoapparata.html)

**English:**

Television emerged as a result of more than a dozen different discoveries and inventions. But the founder of the "idol of the 20th century" - electronic television - is considered to be Boris L. Rozing, professor of the St. Petersburg Institute of Technology. At the beginning of the century he put forward the idea of using an electron-beam tube to transmit images at a distance. In 1907 he demonstrated far-vision, although at a very limited distance - within the limits of one room. A still image was transmitted using mirrors that were rotated by a motor. The mirrors "inspected" the object and, depending on its brightness, adjusted the signals transmitted to the receiver. In the receiver, the image was "collected" using another system of mirrors. Source: [http://www.medn.ru/statyi/Istoriyafotoapparata.html](http://www.medn.ru/statyi/Istoriyafotoapparata.html)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H6dpZdT2FpGyc7B6KUHzm2AhmGdrk5vUMJtB8Z6.jpeg#clickable)](Media of the Past - Part 5_files/3-20.jpg)
-->

[![](http://www.medn.ru/images/22/3-20.jpg#clickable)](http://www.medn.ru/images/22/3-20.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H6dpZdT2FpGyc7B6KUHzm2AhmGdrk5vUMJt_002.jpeg#clickable)](Media of the Past - Part 5_files/3-21.jpg)
-->

[![](http://www.medn.ru/images/22/3-21.jpg#clickable)](http://www.medn.ru/images/22/3-21.jpg)

[http://www.3dnews.ru/572181](http://www.3dnews.ru/572181)

**Russian:**

Телевидение: основные этапы развития

**English:**

Television: main stages of development

**Russian:**

Возможно, первую осуществленную на практике передачу на расстояние изображения по проводам осуществил итальянец Джованни Козелли (Giovanni Caselli), трудившийся в Российской империи. Используя принцип "факсимильной телеграммы", обоснованный шотландцем Александром Бейном (Alexander Bain) в 1842 году, Козелли представил двадцать лет спустя "химический телеграф". С помощью телеграфа нового типа можно было осуществлять передачу текста либо рисунка по проводам. Новинка была названа "пантотелеграф Козелли", ее опробовали на телеграфной линии Санкт-Петербург - Москва. Устройство действительно работало, однако все при этом отчетливо увидели, что овчинка не стоит выделки. Оказывается, изображение для передачи по "пантотелеграфу Козелли" сначала нужно было вытравить на медной пластинке, а в пункте приема подобную пластинку подвергнуть химической обработке, отнимающей много времени. Наличие железной дороги, связывающей две российские столицы, позволяло переправить любую картинку примерно в те же сроки, что и посредством "химического телеграфа", причем безо всякой химии.

**English:**

Perhaps the first practical remote transmission of an image by wire was carried out by Giovanni Caselli, an Italian working in the Russian Empire. Using the principle of "facsimile telegraph" that had been justified by the Scotsman Alexander Bain in 1842, Caselli introduced the "chemical telegraph" twenty years later. With the help of a new type of telegraph it was possible to carry out transmission of the text or drawing on wires. The novelty was called "pantotelegraph Kozelli" and was tested on the telegraph line between St. Petersburg and Moscow. The device really worked, but everyone could clearly see that it was not worth the cost. It turned out that in order to use the pantotelegraph Koselli it was necessary to etch a picture on a copper plate and then subject it to a time-consuming chemical treatment at the receiving station. The presence of a railway that connected two Russian capitals allowed transmitting any picture in about the same time as the "chemical telegraph" - and without any chemistry.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3HypxlXSxoz0dXADSUUV1_gFsiheOcqFkn9k_002.jpeg#clickable)](Media of the Past - Part 5_files/116600.jpg)
-->

[![Image no longer available](http://www.3dnews.ru/assets/external/illustrations/2009/03/12/116600.jpg#clickable)](http://www.3dnews.ru/assets/external/illustrations/2009/03/12/116600.jpg)

**Russian:**

30-строчный телевизор Б-2.

**English:**

30-line TV B-2.

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3HypxlXSxoz0dXADSUUV1_gFsiheOcqFkn9kDnGW.jpeg#clickable)](Media of the Past - Part 5_files/116603.jpg)
-->

[![](http://www.3dnews.ru/assets/external/illustrations/2009/03/12/116603.jpg#clickable)](http://www.3dnews.ru/assets/external/illustrations/2009/03/12/116603.jpg)

**Russian:**

Типичный механический ТВ (без корпуса).

**English:**

Typical mechanical TV (without housing).

**Russian:**

Dmitrij: Но ведь совершенно необязательно вращать диск, можно вращать поле, а для этого нужна система из координат, тот же иконоскоп именно так и делает - вращает не пластину, а считывающий "луч".

Т.е. нам требуется на плоскости диска система из минимум 3-х координат: [http://icark.narod.ru/electro/3faz.html](http://icark.narod.ru/electro/3faz.html)

**English:**

Dmitrij: But it is not necessary to rotate the disk, you can rotate the field, and for this you need a coordinates system. Exactly the same as the Iconoscope does - it does not rotate the plate, but the reading "beam".

I.e., we need a system of at least 3 coordinates on the disk plane: [http://icark.narod.ru/electro/3faz.html](http://icark.narod.ru/electro/3faz.html)

**Russian:**

Dmitrij: Ведь ничто не мешает 3-х фазку разложить из 2-х. [http://electric-sochi.ru/photo/skhema_podkljuchenija_3_kh_faznogo_dvigatelja_v_1_ju_set/1-0-68](http://electric-sochi.ru/photo/skhema_podkljuchenija_3_kh_faznogo_dvigatelja_v_1_ju_set/1-0-68)

**English:**

Dmitrij: After all, nothing prevents 3-phase from decomposing down to 2 phases. [http://electric-sochi.ru/photo/skhema_podkljuchenija_3_kh_faznogo_dvigatelja_v_1_ju_set/1-0-68](http://electric-sochi.ru/photo/skhema_podkljuchenija_3_kh_faznogo_dvigatelja_v_1_ju_set/1-0-68)

**Russian:**

Dmitrij: Синусы, косинусы... Чем не принцип сканера с материала? [http://electricalschool.info/main/naladka/556-kak-opredelit-nachala-i-koncy-faz.html](http://electricalschool.info/main/naladka/556-kak-opredelit-nachala-i-koncy-faz.html)

**English:**

Dmitrij: Sines, cosines... What is not the principle of the material scanner? [http://electricalschool.info/main/naladka/556-kak-opredelit-nachala-i-koncy-faz.html](http://electricalschool.info/main/naladka/556-kak-opredelit-nachala-i-koncy-faz.html)

**Russian:**

Dmitrij: Простое разложение фаз [http://pro-radio.ru/start/12667/](http://pro-radio.ru/start/12667/)

**English:**

Dmitrij: Simple phase decomposition [http://pro-radio.ru/start/12667/](http://pro-radio.ru/start/12667/)

**Russian:**

Dmitrij: Графики магнитных полей в пространстве. [http://www.elsafety.ru/prod02.htm](http://www.elsafety.ru/prod02.htm)

**English:**

Dmitrij: Graphs of magnetic fields in space. [http://www.elsafety.ru/prod02.htm](http://www.elsafety.ru/prod02.htm)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3HwV8oAlfmPiLgy-iWTI-qk1NqlQFfd9RBkaScOs.jpeg#clickable)](Media of the Past - Part 5_files/u(t)%20%D0%B8%20%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%80.jpg)
-->

[![](http://www.elsafety.ru/images/u(t)%20%D0%B8%20%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%80.jpg#clickable)](http://www.elsafety.ru/images/u(t)%20%D0%B8%20%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%80.jpg)

**Russian:**

Dmitrij: Выходит, что всё мы и так знаем. Но микроскоп держим не тем концом.

**English:**

Dmitrij: It turns out that we already know everything. But we hold the microscope by the wrong end.

**Russian:**

Dmitrij: В кинескопе вращается "луч" "механически", отклоняя его магнитными полями. При этом ничто не мешает отклонять его системой фаз, как делается это в современных двигателях, путём ШИМ.

**English:**

Dmitrij: A kinescope (CRT) rotates the "beam" "mechanically" by deflecting it by magnetic fields. At the same time, nothing prevents it from being deflected by the phase system, as is done by 'PWM' in modern motors.

**Russian:**

Dmitrij: В принципе LCD экран это тот же диск, на котором разными ухищрениями смешается рисунок магнитных полей.

**English:**

Dmitrij: In principle, the LCD screen is the same disk on which, by various tricks, the pattern of magnetic fields is mixed.

**Russian:**

Dmitrij: Как известно - металл "проводит" не всякий ток, а лишь очень низкочастотный, в идеале безчастотный.

**English:**

Dmitrij: As you know, metal does not "conduct" any current, except only very low-frequency, ideally frequency-free (Direct Currence - DC).

**Russian:**

Для остального «тока» металл представляет собой что-то типа поляризационной решётки, или порогов, которые надо перетекать, потому в ВЧ проводах «ток бежит» в изоляторе между центральной жилой и оплёткой и чем толще изолятор, тем выше добротность этого провода, ниже погонное сопротивление и дальше можно передать сигнал. Т.е. фактически «сигнал» идёт в обычном диэлектрике, но между 2-х металлов, т.е. как в конденсаторе, но поперёк.

**English:**

For the rest of the "current", metal is something like a polarization grid, or thresholds that need to flow, so in radio frequency cables (coaxial cables) current "runs" in the insulator between the central core and braid. The thicker the insulator, the higher quality factor of this wire, the lower its resistance and the further you can transmit the signal. In fact, the "signal" moves in ordinary dielectric, as it does between two metals, i.e. as it does in a capacitor - it moves across the dialectric.

**Russian:**

Этим пользуются, используя «лапшу» или витую пару, где ввиду взаимного замещения не требуется экран, а «сигнал» за пределами провода гасит сам себя, фактически магнитная дифракция.

**English:**

This is taken advantage of using "noodles" or twisted pair cable where, due to mutual substitution, no shielding is required and the "signal" outside the wire dampens itself, in effect: magnetic diffraction.

**Russian:**

Однако качество среды имеет значение. Если диэлектрик ВЧ кабеля неравномерен, то его «пустоты» будут вплетены в сигнал. Этим пользуются для ВЧ дефектоскопии, которая позволяет «просвечивать» металлы на глубину в сантиметры (!) Фактически металл для такого тока прозрачен, и не является никаким экраном, типа стекла.

**English:**

However, the quality of the medium matters. If the dielectric of the RF cable is uneven, its "voids" will be woven into the signal. This is used for RF defectoscopy, which allows "penetrating" metals to a depth of centimeters (!) In fact, metal is transparent to such currents; it presents no kind of shield, just like glass presents no shield to light.

**Russian:**

Dmitrij: Мало того, этот эффект называют вихревыми токами, что «пробегая» по поверхности металлов, могут заставлять их двигаться, и даже вращаться, тот самый диск счётчика электричества, и даже униполярные двигатели. При этом «рисунок» на поверхности и в структуре металла в виде тех же насечек или пустот, даже трещин, значительно влияет на картину движения, на выходе этого «тока», накапливаясь в нём, как информационная составляющая или как рябь воды на поверхности водоёма. Остаётся лишь «снять» картинку, например через зеркало, которое будет «вибрировать», создавая в отражённом от его поверхности картинки в отражённом свете. Причём не так важно -- зеркало будет из воды или металла, или даже камня. На таких частотах эти материалы весьма гибки, а вода довольно тверда.

**English:**

Dmitrij: Not only that, so-called eddy currents "running" over metal surfaces can make metal move and even rotate, as it does in electricity meter disks and even in unipolar motors. In doing so, any "drawing" on the surface and in the structure of the metal in the form of notches, voids or cracks, significantly affects the pattern of motion of this "current". The current accumulates around the flaws like an information component or a ripple of water on the surface of the pond. It only remains to "shoot" a picture, for example, through a mirror, which will "vibrate", creating a picture in the reflected light from its surface. And it doesn't matter if the mirror is made of water or metal, or even stone. At such frequencies, these materials are very flexible, and water is quite hard.

**Russian:**

Dmitrij: Фактически, налитая в чан вода, вроде идеально гладкая, может вибрировать и создавать волны, рябь на поверхности, от вибрации этого самого чана, надо лишь обеспечить возможность вибрировать, скажем, поставить на гибкую для него ножку или подвесить, чтобы вибрации земли не передавались. Меняя плотность и консистенцию материала, например, добавляя те или иные материалы в среду, можно менять резонансные настройки на каналы, как перестраивать вибраторы приёмника. Соответственно эквипотенциальная поверхность «зеркала», будет принимать нужный нам сигнал, на частоту которого оно будет настроено физически.

**English:**

Dmitrij: In fact, water poured into a vat, seemingly perfectly smooth, can vibrate and create waves, ripples on the surface, from vibration of this very vat, it is only necessary to provide an opportunity to vibrate, by say, putting the vat on a vibration-absorbing plinth or suspending it, so that ground vibrations are not transmitted. By changing the density and consistency of the material, for example, adding certain materials to the medium, you can change the resonant tuning of the channels, like tuning the oscillators of a receiver. Accordingly, the equipotential surface of the "mirror", will receive the desired signal to the frequency to which it is physically tuned.

**Russian:**

Dmitrij: Впрочем, есть и более простые способы, например меня настройку глаз, можно напрямую всматриваться, смещая фокус зрения, в кристаллы драгоценностей или хрустальных черепов, как в экран виртуального шлема. При должном навыке и подсветке, например свечкой нужной цветовой температуры, картинка будет собираться не хуже чем в камере обскура, но сразу на сетчатке глаза, остаётся лишь научиться глазами менять глубину фокуса, «всматриваясь» в кристалл драгоценности.

**English:**

Dmitrij: However, there are simpler ways. For example I can directly peer, shifting the focus of vision, into the crystals of jewels or crystal skulls, as I might peer at the screen of a virtual helmet. With proper skill and illumination - for example, with a candle of proper color temperature - the picture collected will be no worse than in a camera obscura. Once on the retina it remains only to learn to change the depth of focus with eyes, "peering" into a crystal of jewels.

**Russian:**

Dmitrij: Правда, если в кристалл кто-то что-то записал. Ведь кремниевый песок, тоже по сути кристалл, а современная электроника, включая LED экраны, по сути, те же кристаллы и песок, сплавленный определённым образом, при этом внешняя форма носителя мало влияет на содержимое, а полости в материале или плоскости поверхностей с структуре материала, могут существовать очень долго. И даже множественные повреждения, и даже расколы, например раскол накопителя, не уничтожает информацию, а лишь снижает её интенсивность. Т.е. дублирование информации с кристалла может выглядеть как разламывание его. А при «отливке» нового кристалла, достаточно подать с кристалла оригинала всеволновый поток, например, осветив лучом света солнца в полдень или лучше в день солнцестояния -- луч полнее. Но при должном уровне подойдёт и лунный свет, в ночь полной Луны, правда копия будет не полной, а частичной, т.е. перепишется лишь слой, который соответствует лучу данного месяца. В целом для практически полной переписи хватает 13 последовательных этапов. А для возбуждения кристалла на запись можно использовать не только разряды «молний», но и чистую кровь, правда, для её использования требуется золотая оправа для кристалла из живого золота. Потом золото теряет это свойство, становясь лишь оправой. Фактически одноразовая запись. Для стирания кристалла под новую запись, надо лишь его огранить, сменив полярности граней и можно повторить запись. При этом прежняя запись не исчезнет, а лишь ослабнет, оставшись возможной для считывания на более серьёзных аппаратах, способных менять поляризацию или достаточно мощных.

**English:**

Dmitrij: This is true if someone wrote something into a crystal. After all, silicon sand is also essentially a crystal, and modern electronics, including LED screens, are essentially the same crystals and sand fused in a certain way, and the external shape of the media has little effect on the contents. The cavities in the material or surface planes of the material structure can last for a very long time. And even damage or splitting - for example, the splitting of a drive - does not destroy the information, but merely reduces its intensity. I.e. duplicating information in a crystal may involve breaking it. But when "casting" a new crystal, it is enough to feed an all-wave flux from the original crystal, for example, by shining a beam of sunlight at noon or better at the solstice day when the beam is fuller. Even at a proper, suitable level with moonlight on the night of the full moon, though the copy will not be complete. It will be partial, ie the only layer overwritten will be the layer that corresponds to the ray of the month. In general, 13 consecutive stages are enough for practically complete copying. And to excite the crystal for recording, one can use not only discharges of "lightning", but also pure blood, however, for its use a gold frame for the crystal of living gold is required. Then gold loses this property, becoming only a frame. Virtually a disposable record. To erase the crystal for a new record, it is necessary to cut it, changing the polarities of its faces, and then it is possible to repeat the recording process. In this case the former record will not disappear entirely but only weaken, remaining possible to read on more serious devices. That is, devices that are capable of changing polarization or that are powerful enough.

**Russian:**

Dmitrij: К слову -- работой с кристаллами занимались Ю-Де, фактически они создавали одноразовые накопители с записью, что не стиралась. В кристалле были множественные данные, и законы, которые легко высвечивались в аппаратах прилюдно. Фактически де-ю-ре, означает соответствие незыблемым законам, записанным Ю-Де в кристаллах. Умение ю-де передаётся по наследству, обычно материнской линии, информация для записи ими лишь копируется с большого кристалла оригинала, потому внести изменения они не в силах, но могут копировать. Собственно их и прозвали ювелирами.

**English:**

Translator's note: Here, dmitrij is quoted using a Russian word: 'Ю-Де' which has no direct equivalent in English. We have used 'jeweller'. Eventually he points out that 'jeweler' is the modern word for 'Ю-Де'.

Dmitrij: By the way, work with crystals was done by jewellers. In fact, they created disposable storage devices bearing records that could not be erased. The crystal held multiple data, and laws that were easily displayed in the machines in public. In fact, 'de-ju-re', meant conformity to the immutable laws recorded by jewellers in the crystals. Jewellery skill is inherited, usually down the mother's line. Information for recording by them is only copied from a large crystal of the original. Therefore they are not able to make changes, but they can copy. In fact, they are called jewelers.

**Russian:**

далее ряд изображений отсюда: [http://museum-world.livejournal.com/108762.html](http://museum-world.livejournal.com/108762.html)

**English:**

Then there's a series of images from here: [http://museum-world.livejournal.com/108762.html](http://museum-world.livejournal.com/108762.html)

<!-- Local
[![](Media of the Past - Part 5_files/979775_original.jpg#clickable)](Media of the Past - Part 5_files/979775_original.jpg)
-->

[![](http://ic.pics.livejournal.com/galmara/14580035/979775/979775_original.jpg#clickable)](http://ic.pics.livejournal.com/galmara/14580035/979775/979775_original.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/979603_original.jpg#clickable)](Media of the Past - Part 5_files/979603_original.jpg)
-->

[![](http://ic.pics.livejournal.com/galmara/14580035/979603/979603_original.jpg#clickable)](http://ic.pics.livejournal.com/galmara/14580035/979603/979603_original.jpg)

**Russian:**

Dmitrij: А теперь дружно скажем, что это не катушка ВЧ трансформатора.

**English:**

Dmitrij: And now let's say that this is not the coil of RF transformer.

<!-- Local
[![](Media of the Past - Part 5_files/987533_original.jpg#clickable)](Media of the Past - Part 5_files/987533_original.jpg)
-->

[![](http://ic.pics.livejournal.com/galmara/14580035/987533/987533_original.jpg#clickable)](http://ic.pics.livejournal.com/galmara/14580035/987533/987533_original.jpg)

**Russian:**

Список элементов:

**English:**

Item list:

[http://ic.pics.livejournal.com/galmara/14580035/987533/987533_original.jpg](http://ic.pics.livejournal.com/galmara/14580035/987533/987533_original.jpg)

**Russian:**

Гу - мы и так знаем - гуууууу

Цзя - нельзя.

Ху - лучше не трогать.

Цзюэ - вывернет наизнанку (воняет).

Цзюнь - наплевал кто-то.

Лин - и тут Ли

**English:**


(Translating the words on the museum exhibit:)

Goo - we already know - goo-goo-goo.

Jia, you can't.

Hoo - best not to touch it.

Jue - turn inside out (stinks).

Jun - someone spat.

Lin - and here's Li.

**Russian:**

Сосуды для еды - "фу" - понятно и без перевода насколько вкусно.

**English:**

The eating vessels are "ew" - it's clear without translation how delicious it is.

<!-- Local
[![](Media of the Past - Part 5_files/978406_original.jpg#clickable)](Media of the Past - Part 5_files/978406_original.jpg)
-->

[![](http://ic.pics.livejournal.com/galmara/14580035/978406/978406_original.jpg#clickable)](http://ic.pics.livejournal.com/galmara/14580035/978406/978406_original.jpg)

**Russian:**

А ведь мы тоже водку охлаждали у кондиционеров, а бутеры грели на трансформаторах и процах.

И что забавно - им не приходит в голову подвешивать эти горшки за ножки на верёвках к перекладинам, бить по ним колотушками и называть это колокольным звоном.

МАМЗЕРЕВ ВЛАДИМИР: на "ноги" получается катушки наматывали, а сам чан и был детектором?

**English:**

We used to cool our vodka at air conditioners and heat our sandwiches at transformers and prototypes.

And it's funny - it doesn't occur to them (the museum curators) to hang these pots with their feet on ropes to the crossbeams, to hit them with the bells and call it bell ringing.

MAMZEREV VLADIMIR: the "legs" turns out the coils were winded, and the vat itself was the detector?

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H1Qb43FFoE5EM-992MY-tSOe0fkY5LWI2z0k0a0.jpeg#clickable)](Media of the Past - Part 5_files/image001.jpg)
-->

[![](http://www.bibliotekar.ru/china1/22.files/image001.jpg#clickable)](http://www.bibliotekar.ru/china1/22.files/image001.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3H-93hy5Wj5c_rQWsWss3Dk1t-xT8ZBdrXaLc-AT.jpeg#clickable)](Media of the Past - Part 5_files/pho00442.jpg)
-->

[![](http://www.chiculture.net/20509/picture/pho00442.jpg#clickable)](http://www.chiculture.net/20509/picture/pho00442.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/988100_original.jpg#clickable)](Media of the Past - Part 5_files/988100_original.jpg)
-->

[![](http://ic.pics.livejournal.com/galmara/14580035/988100/988100_original.jpg#clickable)](http://ic.pics.livejournal.com/galmara/14580035/988100/988100_original.jpg)

<!-- Local
[![](Media of the Past - Part 5_files/3mp-EHeVZUp7YmWGuFv3Hww73KKXkTFLueRCQv5J7ZIi9f_x7jBnzSBALEo.jpeg#clickable)](Media of the Past - Part 5_files/2011120320361239336844.jpg)
-->

[![](http://estimation.cang.com/201112/2011120320361239336844.jpg#clickable)](http://estimation.cang.com/201112/2011120320361239336844.jpg)

![Image no longer available](http://flana.ru/pict/costume_history/mirror/china-mirror-206-BC-24AD-2.jpg)

**Russian:**

буковки "т" попадаются повсеместно.

Dmitrij: "Ноги" прикасались, как иголки детектора. Естественно под ту или иную посадку свои ноги требуются.

На колёсах бывает 5 болтов, бывает 6, бывает 3

**English:**

The "T" is everywhere.

Dmitrij: "Feet" touched like detector needles. Naturally, one or another landing requires one's own legs.

There are 5 bolts on wheels, 6 bolts, 3 bolts

© All rights reserved. The original author retains ownership and rights.


