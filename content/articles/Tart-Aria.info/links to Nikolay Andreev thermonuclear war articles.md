title: Evidence of Thermonuclear War - Nikolay Andreev
date: 2021-07-23
modified: 2022-04-24 09:56:26
category: 
tags: thermonuclear war; links
slug: 
authors: Various
summary: Links to Russian language articles about earlier thermonuclear wars on Earth with Google-translate function built in. Each article should render in English after a few seconds.
status: published
originally: Links to thermonuclear war articles translations.md
from: 
local: Links to thermonuclear war articles translations.md

[On nuclear strikes on the Ryazan land](https://translate.google.com/translate?sl=ru&tl=en&u=https://www.tart-aria.info/sboj-v-matrice-ili-paralleli-istorii/)

<!-- Not sure what is going on here
[On nuclear strikes on the Ryazan land](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://www.tart-aria.info/o-jadernyh- udarah-po-rjazanskoj-zemle/)
-->

[Cesium-137 in Ryazam land](https://www-tart--aria-info.translate.goog/jadernaja-vojna-xix-veka/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[Nicolay Andreev comments on Kramolo piece: Nuclear Strikes of the Recent Past](Earth://translate.google.com/website?sl=ru&tl=en&ajax=1&elem=1&se=1&u=https://andreevn-bgf.blogspot.com/2017/11/blog-post_23.html)

[Chronological travels in Crimea. Part 1 - Panticapaeum](https://www-tart--aria-info.translate.goog/hronologicheskie-puteshestvija-v-krymu-1/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,elem)

[The Unknown Past of Miass](https://translate.google.com/website?sl=ru&tl=en&ajax=1&elem=1&se=1&u=https://andreevn-bgf.blogspot.com/2018/01/2.html)

[Tunguska, but was it a meteorite?](https://www-tart--aria-info.translate.goog/tungusskij-no-meteorit-li/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB)

For similar articles, see ['Thermonuclear War' tag](https://178.62.117.238/tag/thermonuclear-war.html)

Related by Alexey Artemiev:

[Nuclear Strikes of the Recent Past](https://translate.google.com/website?sl=ru&tl=en&ajax=1&elem=1&se=1&u=https://www.kramola.info/vesti/neobyknovennoe/jadernye-udary-nedavnego-proshlogo)

Different version of the above: [Nuclear strikes of the recent past](https://www-kramola-info.translate.goog/vesti/neobyknovennoe/jadernye-udary-nedavnego-proshlogo?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)

[A nuclear battle has already occurred on Earth](https://atlantida--pravda--i--vimisel-blogspot-com.translate.goog/2012/03/1.html?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax,se,elem)


© All rights reserved. The original authors retain all rights.


