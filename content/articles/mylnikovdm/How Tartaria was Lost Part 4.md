title: How Tartaria Died Part 4 - 19th Century Climate Change
date: 2014-10-11
modified: Thu 14 Jan 2021 07:22:38 GMT
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: After the publication of the third part about "relic" forests, there were a lot of critical comments, to which I consider it necessary to respond.
status: published
originally: Как погибла Тартария, ч4.zip
local: How Tartaria was Lost Part 4_files/

### Translated from: 

Как погибла Тартария, ч4.zip, [https://mylnikovdm.livejournal.com/2593.html](https://mylnikovdm.livejournal.com/2593.html) and [https://mylnikovdm.livejournal.com/3024.html](https://mylnikovdm.livejournal.com/3024.html)

**Russian:**

- [http://mylnikovdm.livejournal.com/603.html](http://mylnikovdm.livejournal.com/603.html) - Часть 1.

- [http://mylnikovdm.livejournal.com/950.html](http://mylnikovdm.livejournal.com/950.html) - Часть 2а

- [http://mylnikovdm.livejournal.com/1088.html](http://mylnikovdm.livejournal.com/1088.html) - Часть 2б

- [http://mylnikovdm.livejournal.com/1646.html](http://mylnikovdm.livejournal.com/1646.html) - Часть 3а

- [http://mylnikovdm.livejournal.com/1990.html](http://mylnikovdm.livejournal.com/1990.html) - Часть 3б


**English:**

Previous parts:

- [Part 1: http://mylnikovdm.livejournal.com/603.html](http://mylnikovdm.livejournal.com/603.html)

- [Part 2a: http://mylnikovdm.livejournal.com/950.html](http://mylnikovdm.livejournal.com/950.html)

- [Part 2b: http://mylnikovdm.livejournal.com/1088.html](http://mylnikovdm.livejournal.com/1088.html)

- [Part 3a: http://mylnikovdm.livejournal.com/1646.html](http://mylnikovdm.livejournal.com/1646.html)

- [Part 3b: http://mylnikovdm.livejournal.com/1990.html](http://mylnikovdm.livejournal.com/1990.html)

**Russian:**

После опубликования третьей части о "реликтовых" лесах, пришло оченьмного критических комментариев, на которые я считаю необходимым ответить.

**English:**

After the publication of the third part about "relic" forests, there came a lot of critical comments, to which I consider it necessary to respond.

**Russian:**

Многие упрекнули меня в том, что я говоря о возрасте лесов не упомянулпро лесные пожары, которые регулярно уничтожают миллионы гектаров лесов в Сибири. Да, действительно, лесные пожары на большой территорииявляются большой проблемой для сохранности лесов. Но в той теме, которую я рассматриваю, важно как раз то, что старые леса на этой территорииотсутствуют. Причина, по которой они отсутствуют, это уже другой вопрос. Другими словами, я вполне могу принять версию, что причиной того, чтолеса в Сибири "живут не более 120 лет" (как заявил один из комментаторов), являются именно пожары. Этот вариант, в отличие от"реликтовых" лесов, никак не противоречит тому, что в начале 19-го века на территории Зауралья и Западной Сибири произошла масштабнаяпланетарная катастрофа.

**English:**

Many reproached me for not mentioning the forest fires that regularly destroy millions of hectares of forest in Siberia. Yes, indeed, forest fires in a large area are a big problem for forest conservation. But the important thing about the topic I am looking at is that there are no old forests in this area. The reason they are absent is a different issue. In other words, I may well accept the version that the reason why forests in Siberia "live no longer than 120 years" (as one commentator put it) is because of fires. This version, unlike "relic" forests, does not contradict in any way the fact that in the beginning of the 19th century there was a large-scale planetary catastrophe in the Trans-Urals and Western Siberia.

**Russian:**

Тем не менее, следует заметить, что пожарами невозможно объяснить очень тонкий слой почвы на территории ленточных боров. При пожарах будетвыгорать только два верхних горизонта почвенного слоя с индексами А0 и А1 (расшифровка в части 3б). Остальные горизонты уже практически негорят и должны были сохраниться. Кроме того, мне прислали ссылку на одну из работ, где как раз и исследуются последствия лесных пожаров. Из неёследует, что по почвенному слою легко определяется то, что на данной территории был пожар, поскольку в почве будет наблюдаться слой золы. Приэтом по глубине расположения зольного слоя можно даже приблизительно определить когда произошёл пожар. Так что если провести исследования наместе, то можно будет точно сказать горели ли когда-нибудь ленточные боры или нет, а также примерное время, когда это происходило.

**English:**

However, it should be noted that fires cannot explain the very thin layer of soil in the territory of belt forests. In case of fires only two upper horizons of soil layer with indices A0 and A1 will burn out (from the table in part 3). Other horizons are already almost unburned and should have been preserved. Besides, I was sent a link to one of the investigations where the consequences of forest fires are researched. It showed that it is easy to determine there has been a fire on a given territory because a layer of ashes will be observed in the soil. Thus it is possible even approximately to define when there was a fire by assessing the depth of the ash layer. So, if the research is done, it will be possible to tell exactly whether ribbon burns have ever occurred or not and approximate time when they have occurred.

**Russian:**

Ещё одно дополнение я хочу сделать ко второй части, где я рассказывалпро крепость в селе Миасском. Так как это село находится в 40 км. от Челябинска, где я живу, то в один из выходных я совершил туда небольшоепутешествие, в ходе которого лично у меня не осталось никаких сомнений, что крепость когда-то находилась именно на месте острова, а протока,которая сейчас отделяет остров, является тем, что осталось от рва, окружавшего крепость и прилегающие к ней дома.

**English:**

Another addition I want to make is to the second part, where I talked about the fortress in the village Miassky. Since this village is located 40 km from Chelyabinsk, where I live, I made a short trip there one weekend, during which I pesonally had no doubt that a fortress once stood on the site of the island, and the ditch which now separates the island is what is left of the moat that surrounded the fortress and the houses adjacent to it.

**Russian:**

Во первых, на местности там, где по схеме крепости должен быть правыйверхний угол канала с выступающим "лучом", имеется холм высотой около 1.5 метров с прямоугольными очертаниями. От этого холма в сторону рекипросматривается вал, направление которого также совпадает с направлением канала на схеме. Этот вал примерно посередине рассекается протокой. Наостров попасть, к сожалению не удалось, поскольку мостика, который виден на снимке, в настоящий момент уже нет. Поэтому я не уверен на 100%, но сэтого берега создаётся ощущение, что на противоположном берегу в том месте, где должна была быть крепость, также имеется вал. По крайнеймере, тот берег заметно выше. Там, где должен был быть верхний левый угол крепости, который сейчас срезан протокой, на местности имеетсяровная площадка прямоугольной формы.

**English:**

First, on the ground, where according to the plan of the fortress there should be the upper right corner of the channel with a protruding "beam", there is a hill about 1.5 meters high with rectangular outlines. From this hill towards the river there is a rampart whose direction also coincides with the direction of the canal in the diagram. The ditch cuts this shaft 
approximately in half. Unfortunately, it was not possible to get to the island, because the bridge, which can be seen in the image, is no longer there. Therefore, I am not 100% sure, but on this shore, it feels like there is also a rampart on the opposite shore in the place where the fortress was supposed to be. At least that bank is noticeably higher. Where the upper left corner of the fortress was supposed to be, which is now cut by the ditch, there is a rectangular impression on the ground.


**Russian:**

Но самое главное, что мне удалось поговорить прямо на берегу рядом с протокой с местными жителями. Они подтвердили, что сегодняшний мостявляется новым, старый мост быть ниже, рядом с островом. При этом они не знают точно, где была крепость, но показали мне старый фундаменткакого-то сооружения, который находится у них на огороде. Так вот этот фундамент идёт точно параллельно направлению протоки, а значит положениюстарой крепости, но при этом под углом к существующей планировке посёлка.

**English:**

But the most important thing is that I was able to talk to the local right on the shore near the ditch. They confirmed that today's bridge is new, the old bridge was lower near the island. At the same time, they did not know exactly where the fortress was, but they showed me the old foundation of some building, which was in their garden. So this foundation goes exactly parallel to the direction of the ditch, which indicates the position of the old fortress, but at the same time at an angle to the existing layout of the village.


**Russian:**

Остаётся, правда, вопрос, почему крепость была построена так близко кводе, ведь её должно было бы затапливать при весеннем паводке. Или наличие рва с водой, который защищал крепость и посёлок, для них былонамного важнее, чем весеннее подтопление?

**English:**

The question remains, however, why was the fortress built so close to the gate? Surely it would be flooded in the spring flood. Or perhaps the presence of a moat with water, which protected the fortress and the village, was more important for them than the spring flood?


**Russian:**

А может быть есть и другой ответ на этот вопрос. Возможно, что в то время климат был другим, большого весеннего паводка не было вообще,поэтому он и не принимался в расчёт.

**English:**

Or maybe there's another answer to that question. It is possible that at that time the climate was different, there was no big spring flood at all, so flooding was not taken into account.


**Russian:**

Когда была опубликована первая часть, то некоторые из комментаторов указывали на то, что подобная масштабная катастрофа обязательно должнабыла сказаться на климате, но свидетельств того, что в начале 19-го века произошло изменение климата, у нас якобы нет.

**English:**

When the first part of this series was published, some commentators pointed out that such a major disaster was bound to have an impact on the climate, but we have no evidence that climate change occurred in the early 19th century.


**Russian:**

Действительно, при такой катастрофе, когда на большой территорииуничтожаются леса и повреждается верхний плодородный слой почвы, серьёзные изменения климата неизбежны.

**English:**

Indeed, in the event of such a catastrophe, when forests are destroyed and the top fertile soil layer is damaged over a large area, serious climate change is inevitable.


**Russian:**

Во-первых, леса, особенно хвойные, выполняют роль термостабилизаторов,не давая зимой сильно промерзать почве. Есть исследования, которые показывают, что в мороз температура возле ствола ели может быть на10С-15С выше, чем на открытом пространстве. Летом же, наоборот, температура в лесах ниже.

**English:**

Firstly, forests, especially coniferous ones, act as heat stabilizers, preventing the soil from freezing heavily in winter. There are studies that show that in frosty weather the temperature near the trunk of spruce can be 10C-15C higher than in open space. In summer, on the contrary, the temperature in forests is lower.


**Russian:**

Во-вторых, леса обеспечивают водяной баланс, не давая воде слишкомбыстро сбегать, а земле высыхать.

**English:**

Secondly, forests provide water balance by preventing water from escaping too quickly and the ground from drying out.


**Russian:**

В третьих, во время самой катастрофы при прохождении плотного метеоритного потока, будет наблюдаться как перегрев, так и повышенноезагрязнение, как от тех метеоритов, которые разрушились в воздухе не долетев до Земли, так и от той пыли и пепла, которые будутобразовываться при падении и повреждении поверхности метеоритами, размеры которых, судя по следам на снимках, от нескольких десятковметров до нескольких километров. Кроме того, мы не знаем реальный состав метеоритного потока, который столкнулся с Землёй. Очень вероятно, чтокроме крупных и очень крупных объектов, следы которых мы наблюдаем, этот поток также содержал средние и мелкие объекты, а также пыль. Средние имелкие объекты должны были разрушится при прохождении атмосферы. При этом сама атмосфера должна была разогреться и наполниться продуктамираспада этих метеоритов. Очень мелкие объекты и пыль должны были затормозиться в верхних слоях атмосферы, сформировав своеобразноепылевое облако, которое ветрами может переноситься на тысячи километров  от места катастрофы, после чего, при повышении влажности атмосферы,выпадать вниз грязевым дождём. А всё то время, пока эта пыль находилась в воздухе, она создавала экранирующий эффект, который должен был вызватьэффект аналогичный "ядерной зиме". Поскольку солнечный свет плохо доходит до поверхности, температура на поверхности Земли должна былазаметно понизиться, вызвав локальное похолодание, своеобразный малый ледниковый период.

**English:**

Third, during the catastrophe itself, during the passage of a dense meteorite swarm, there will be observed both overheating and increased pollution, both from those meteorites that exploded in the air and did not reach the Earth's surface, and from the dust and ash, which will be formed by the fall and damage to the surface by meteorites, whose size, judging by the traces shown on the images, ranged from a few tens of meters to several kilometers. In addition, we do not know the actual composition of the meteorite swarm that collided with the Earth. It is very likely that this flux also contained medium and small objects - including dust - in addition to the large and very large objects whose ground traces we easily see. Medium sized objects must have disintegrated they passed through the atmosphere. In doing so, the atmosphere itself should have been heated up and filled with the decay products of these meteorites. Very small objects and dust were slowed down in the upper layers of the atmosphere, forming a kind of dust cloud, which could be carried by winds to thousands of kilometers from the site of the disaster, and then, with increasing humidity of the atmosphere, precipitated as mud-rain. And all the time this dust was in the air, it created a shielding effect, which should have caused an effect similar to "nuclear winter". Since sunlight does not reach the surface well, the temperature on the Earth's surface should have dropped noticeably, causing local cooling - a kind of small glacial period.


**Russian:**

На самом деле фактов, которые указывают на то, что климат на территорииРоссии заметно изменился, достаточно много.

**English:**

In reality, there are quite a few facts that indicate that the climate in Russia has changed significantly.


**Russian:**

Думаю, что большинству читателей известен "Аркаим" - уникальный археологический памятник на юге Челябинской области. Официальная наукасчитает, что это древнее сооружение было построено от 3.5 до 5.5 тысяч лет назад. Об Аркаиме и вокруг Аркаима написано уже множество какнаучных, так и совершенно безумных книг и статей. Нам же интересно то, что найденным в земле останкам археологи смогли достаточно точновосстановить исходную конструкцию данного сооружения. Вот её мы и рассмотрим подробнее.

**English:**

I think that most readers know "Arkaim" - a unique archaeological monument in the south of the Chelyabinsk region. Official science believes that this ancient structure was built from 3.5 to 5.5 thousand years ago. Many scientific and absolutely mad books and articles have already been written about Arkaim and the area around Arkaim. It is interesting to us that archeologists could accurately enough restore the original construction of this structure from the remains that have been found in the ground. Here we will consider the finds in more detail.


<!-- Local
![](How Tartaria was Lost Part 4_files/100000000000080000000600703F7804.jpg#resized)
-->

<!-- Local
[![](How Tartaria was Lost Part 4_files/31733_900.jpg#clickable)](How Tartaria was Lost Part 4_files/31733_original.jpg)
-->

[![](http://ic.pics.livejournal.com/mylnikovdm/24488457/31733/31733_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/31733/31733_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/31894_900.jpg#clickable)](How Tartaria was Lost Part 4_files/31894_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/31894/31894_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/31894/31894_original.jpg)

**Russian:**

В музее, который находится рядом с памятником, можно увидеть показанныйна фотографиях детальный макет сооружения. Оно состоит из двух колец, которые образуются вытянутыми жилыми помещениями, с выходом из каждогово внутренний круг. Ширина одной секции около 6 метров, длина около 30 метров. Проходом между секциями нет, они располагаются вплотную друг кдругу. Всё сооружение в целом окружает стена, которая выше, чем крыши внутренних строений.

**English:**

In the museum, which is located next to the monument, you can see a detailed layout of the structure shown in photos. It consists of two rings that are formed by elongated living quarters, with the exit from each inner circle. The width of one section is about 6 meters and the length is about 30 meters. There is no passageway between the sections, they are located close to each other in a circle. The entire structure is surrounded by a wall that is higher than the roofs of the inner structures.


**Russian:**

В своё время, когда я первый раз увидел реконструкцию Аркаима, меняпоразил очень высокий технический и технологический уровень жителей Аркаима. Построить сооружение с крышей шириной 6 метров и длиной 30метров далеко не самая простая техническая задача. Но нас сейчас интересует вовсе не это.

**English:**

At one time, when I first saw the reconstruction of Arkaim, I noticed the very high technical and technological level of Arkaim's inhabitants. To build a structure with a roof 6 meters wide and 30 meters long is not the easiest technical task. But that is not what we are interested in here.


**Russian:**

При проектировании любых зданий и сооружений конструктор обязанучитывать такой параметр, как снеговая нагрузка на крышу. Снеговая нагрузка зависит от особенностей климата местности, где будет находитьсяданное здание или сооружение. На основании многолетних наблюдений для всех регионов определяется набор параметров для подобных расчётов.

**English:**

At the design stage of any building or construction, the designer is obliged to consider parameters such as snow loading on a roof. The snow load depends on the climate of the area where the building or structure will be located. On the basis of long-term observations, a set of parameters for such calculations can be - and is - determined for all regions.


**Russian:**

Из конструкции Аркаима совершенно однозначно вытекает, что в то время,когда он существовал, снега зимой в этой местности не было вообще! То есть, климат в этой местности был намного теплее. Представьте, что надАркаимом прошёл хороший снегопад, который не редкость зимой в Варненском районе Челябинской области. И что делать со снегом?

**English:**

From the construction of the Arkaim, it is clear that at the time when it existed, there was no snow in winter in this area at all! That is, the climate in this area was much warmer. Imagine that there was good snowfall over Arkaim, which is not uncommon in winter in Varna district of Chelyabinsk region. What to do with the snow?


**Russian:**

Если мы возьмём типичную сегодняшнюю деревню, то там на домах обычноделают достаточно крутые двухскатные крыши, чтобы снег сам скатывался с них вниз по мере накопления или при таянии весной. Между домами большиерасстояния, где может этот снег накапливаться. То есть, обычно современному жителю деревенского дома или коттеджа вообще не надоспециально что-либо делать, чтобы решить проблему снега. Разве что при очень сильных снегопадах помочь тем или иным способом снегу сойти вниз.

**English:**

If we take a typical village today, there are usually enough steep gable roofs on the houses to make the snow roll down from them as it accumulates or melts in spring. There are large distances between houses where snow can accumulate. That is, usually a resident of a modern village house or cottage does not need to do anything special to solve the snow problem. Except in case of very heavy snowfalls, where they may help in one way or another the snow to move down off the roof.


**Russian:**

Конструкция Аркаима такова, что в случае выпадения снега у васпоявляется масса проблем. Крыши плоские и большие. Значит снега они соберут много и он останется лежать на них. Промежутков между секциями,чтобы скинуть туда снег, у нас нет. Если мы будем скидывать снег во внутренний проход, то он у нас очень быстро заполнится снегом. Кидатьнаружу через стену, которая выше крыши? Но, во-первых, это весьма долго и трудоёмко, а во вторых, через некоторое время у вас вокруг стеныобразуется снежный вал, причём достаточно плотный, поскольку при чистке и сваливании снег заметно уплотняется. А это значит, что резкопонижается оборонительная способность вашей стены, поскольку по снежному валу будет более легко подняться на стену. Тратить ещё массу времени исил на то, чтобы откинуть снег дальше от стены?

**English:**

The design of Arkaim is such that you would have a lot of problems with snow. The roofs are flat and large. So they'll collect a lot of snow and it'll stay on them. We don't have any spaces between sections to disgorge snow into. If we clear snow into the inner aisle, it will fill with snow very quickly. So, throw it outside through a wall that's higher than the house roof? Firstly, it is very long and laborious, and secondly, after a while a barrier of snow will form around the wall. And it will be quite dense because during cleaning and dumping, snow visibly thickens. And it means that the defensive ability of your wall is sharply reduced, because it will be easier to climb the wall via the piled snow. Would you like to spend yet more time clearing snow away from the wall to counter that risk?


**Russian:**

А теперь представим, что будет с Аркаимом, если начнётся снежная буря, которая в том районе также происходит зимой достаточно часто. Апоскольку там кругом степи, то при сильных снежных бурях дома может занести снегом по самые крыши. А Акраим, в случае сильной снежной бури,может занести снегом по самые внешние стены! И уж точно заметёт все внутренние проходы до уровня крыш жилых секций. Так что если у вас непредусмотрены люки в крышах, то выйти из этих секций после бури будет не так-то просто.

**English:**

Now let's imagine what will happen to Arkaim if there is a snowstorm, which also happens quite often in winter in that area. Because the steppe is all over the place, a house can be snowed up to roof level in heavy snow storms. And Akraim, in the event of a heavy snow storm, could bring snow down the outermost walls! And snow will definitely notice - and settle in - all the internal passageways up to the roof level of the residential sections. So if you don't have any roof hatches, it won't be easy to get out of these sections after a storm.


**Russian:**

У меня ест большие сомнения, чтобы жители Аркаима построили свой городне учитывая перечисленные выше проблемы, а потом бы мучились каждую зиму со снегом и заносами во время бури. Подобное сооружение могло бытьпостроено только там, где снега зимой либо нет совсем, либо бывает очень мало и очень редко, не образуя постоянного снежного покрова. А этозначит, что климат во времена Аркаима на юге Челябинской области был похож на климат Южной Европы или даже мягче.

**English:**

I have great doubts that the people of Arkaim would build their town given the above problems and then suffer every winter with snow and drifts during winter storms. Such a construction could only be built where there is either no snow at all in winter or very little and very rarely, ie without permanent snow cover. This means that the climate at the time of Arkaim in the south of the Chelyabinsk Region was similar to or milder than in today's southern Europe.


**Russian:**

Но, могут заметить скептики, Аркаим существовал достаточно давно. Занесколько тысяч лет с того момента, как Аркаим был уничтожен, климат мог измениться много раз. Из чего следует, что это изменение произошлоименно в конце 18-го начале 19-го века?

**English:**

But skeptics may note that Arkaim existed long enough that in the several thousand years since Arkaim was destroyed the climate may have changed many times. What suggests that this change occurred at the end of the 18th and beginning of the 19th century?


**Russian:**

Опять же, если подобное изменение климата произошло так близко к нам, то в документах, книгах и газетах того времени просто обязаны бытьсвидетельства резкого похолодания. И, действительно, оказывается свидетельства такого резкого похолодания в 1815-1816 годах имеются вбольшом количестве, 1816 год вообще известен как "год без лета".

**English:**

Again, if such climate change happened so close to us, then documents, books and newspapers of that time simply have to show evidence of a sudden cold spell. And, indeed, it turns out that there is evidence of such a sharp cold in 1815-1816, and 1816 is generally known as "the year without summer".

[https://ru.wikipedia.org/wiki/%D0%93%D0%BE%D0%B4\_%D0%B1%D0%B5%D0%B7\_%D0%BB%D0%B5%D1%82%D0%B0](https://ru.wikipedia.org/wiki/Год_без_лета)

**Russian:**

Вот что об этом периоде писали в Канаде: [http://russianmontreal.ca/index.php?newsid=281](http://russianmontreal.ca/index.php?newsid=281)

**English:**

This is what they wrote about this period in Canada: [http://russianmontreal.ca/index.php?newsid=281](http://russianmontreal.ca/index.php?newsid=281).


**Russian:**

> До сегодняшнего дня 1816 год остаётся самым холодным с начала документирования метеорологических наблюдений. В США его также прозвали"Eighteen hundred and frozen to death", что можно перевести как "Тысяча-восемьсот-замёрз-до-смерти".

**English:**

> To this day, 1816 remains the coldest year since the beginning of the documentation of meteorological observations. In the USA it was also called "Eighteen hundred and frozen to death", which can be translated as "A Thousand-Eight Hundred and Frozen to Death".


**Russian:**

> "Погода все еще остается исключительно холодной и неблагопрятной. Повсей видимости, сезон фруктов и цветов отложится на более поздний период. Старожилы не помнят такого холодного начала лета", - писаламонреальская "Газетт" 10 июня 1916 года.

**English:**


> "The weather is still extremely cold and untidy. Hang in there, the season of fruit and flowers will be postponed to a later period. Old-timers do not remember such a cold beginning of summer," 

wrote the Montreal, June 10, 1916.


**Russian:**

> 5 июня холодный фронт спустился с Гудзонова залива и "схватил" в свои ледяные объятия всю долину реки Святого Лаврентия. Вначале пошелмонотонный холодный дождь, сменившийся на пару дней в городе Квебеке снегопадом, а днем позже в Монреале дикой снежной метелью. Столбиктермометра опустился на минусовые отметки, вскоре толщина снега достигла 30 сантиметров: снежные сугробы намело до осей карет и повозок,застопорив все летние средства передвижения намертво. Пришлось в середине июня(!) выводить сани. Холод ощущался повсюду, пруды, озера ибольшую часть реки Святого Лаврентия вновь сковал лед.

**English:**

> On June 5, the cold front descended from the Hudson Bay and "grabbed" the whole valley of the St. Lawrence River into its icy embrace. It began with a monotonous cold rain, followed by a couple of days of snowfall in Quebec City and a wild snowstorm in Montreal the following day. The thermometer dropped to the minus mark, soon the snow thickness reached 30 centimeters: snow drifts outlined the axes of carriages and wagons, stopping all summer vehicles deliberately. We had to remove the sledge in the middle of June (!). Cold was felt everywhere, ponds, lakes and most of the St. Lawrence River again stiffened with ice.


**Russian:**

> На первых порах жители провинции не унывали. Привычные к суровым канадским зимам, они достали зимнюю одежду и надеялись, что это"недоразумение" вскоре закончится. Кто-то шутил и посмеивался, а детвора вновь каталась с горок. Но когда замерзающие птицы начали залетать вдома, а на селе их маленькие окоченевшие тельца усеяли черными точками поля и огороды, а постриженные по весне овцы, не выдержав холод, сталимассово погибать, стало совсем тревожно.

**English:**

> At first, the people of the province were not discouraged. Used to the harsh Canadian winters, they took out their winter clothes and hoped that this "misunderstanding" would soon be over. Everyone was joking and laughing, and the kids were back on the slides. But when the freezing started, the birds struggled to take a breath, and in the village their little stiffened calves were dotted as black dots in the fields and vegetable gardens, and the sheep cut in spring, unable to withstand the cold, became quite anxious to die.


**Russian:**

> 17 июля наконец появилось солнце. Газеты радостно сообщили, что есть надежда на урожай тех культур, что выдержали заморозки. Однакооптимистичные высказывания репортеров оказались преждевременными. В конце июля пришла вторая волна холодного сухого воздуха, вслед за нейтретья, вызвавшая такую засуху на полях, что стало ясно - погиб весь урожай.

**English:**

> On July 17th, the sun finally appeared. The newspapers happily reported that there is hope for the crops that have withstood the frosts. However, the optimistic statements of reporters were premature. At the end of July came a second wave of cold dry air, followed by the third wave, which caused such a drought in the fields that it became clear - the entire harvest was lost.


**Russian:**

Бороться с бедой жителям Канады пришлось не только в 1816 году. Жан-ТомаТашро, член канадского парламента, писал:

> "Увы, зима 1817-1818 года оказалась вновь чрезвычайно тяжелой. Число умерших в этот год былонеобычайно высоким".

**English:**

It wasn't just in 1816 that the people of Canada had to fight trouble. Jean-TomaTashro, a member of the Canadian Parliament, wrote:

> "Alas, the winter of 1817-1818 was again extremely hard. The death toll that year was unusually high".

**Russian:**

Аналогичные свидетельства можно встретить и в США, и в странах Европы, включая Россию.

**English:**

Similar evidence can be found in the US and European countries, including Russia.

<!-- Local
[![](How Tartaria was Lost Part 4_files/32016_900.jpg#clickable)](How Tartaria was Lost Part 4_files/32016_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/32016/32016_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/32016/32016_original.jpg)

**Russian:**

Но по официальной версии это похолодание якобы было вызвано мощнейшим извержением вулкана Тамбора на индонезийском островеСумбава. При этом интересно, что данный вулкан расположен в южном полушарии, в то время как катастрофические последствия почему-тонаблюдались именно в северном полушарии.

**English:**

But according to the official version, this cooling down was allegedly caused by a powerful eruption of Tamboro volcano on the Indonesian island of Sumbava. However, it is interesting that this volcano is located in the southern hemisphere, while the catastrophic consequences were somehow observed in the northern hemisphere.

<!-- Local
[![](How Tartaria was Lost Part 4_files/32280_900.jpg#clickable)](How Tartaria was Lost Part 4_files/32280_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/32280/32280_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/32280/32280_original.jpg)

**Russian:**

Извержение вулкана Кракатау, произошедшее 26 августа1883 года, уничтожило крошечный островок Раката, находившийся в узком проливе межу Явой и Суматрой. Звук был слышен на расстоянии 3500километров в Австралии и на острове Родригес, что в 4800 километрах. Считается, что это был самый громкий звук за всю письменную историючеловечества, его слышали на 1/13 части земного шара. Это извержение было несколько слабее извержения Тамбора, но вот катастрофическоговлияния на климат вообще практически не наблюдалось.

**English:**

The eruption of Krakatoa volcano on August 26, 1883, destroyed the tiny island of Rakatoa in the narrow strait between Java and Sumatra. The sound was heard at a distance of 3,500 kilometres away in Australia and on Rodriguez Island, 4,800 kilometres away. It is believed to have been the loudest sound in the written history of mankind, heard on 1/13th of the globe. The eruption was slightly weaker than Tambor. However, there was virtually no catastrophic impact on the climate at all.

**Russian:**

Когда стало выясняться, что одного извержения вулкана Тамбора недостаточно, чтобы вызвать столь катастрофические изменения климата,была придумана легенда прикрытия, о том, что в 1809 году якобы где-то в районе тропиков произошло ещё одно извержение, сравнимое с извержениемвулкана Тамбора, но которое ни кем было не зафиксировано. И именно благодаря этим двум извержениям наблюдался аномально холодный период с1810 по 1819 года. Как так получилось, что столь мощное извержение оказалось никем незамеченным, авторы работы никак не объясняют, да и поизвержению вулкана Тамбора ещё вопрос, было ли оно настолько сильным, как об этом пишут британцы, под чьим контролем находился в тот моментостров Сумбава. Поэтому есть основания предположить, что это всего лишь легенды прикрытия истинных причин, которые вызывали катастрофическоеизменение климата в Северном полушарии.

**English:**

When it became clear that an eruption of Tamboro alone was not enough to cause such catastrophic climate change, a cover legend was invented that in 1809, allegedly somewhere in the tropics, another eruption occurred, comparable to the eruption of Tamboro, but which was not recorded by anyone. And it was thanks to these two eruptions that the abnormally cold period from 1810 to 1819 was observed. How it was that such a powerful eruption happened to go unnoticed is not explained by these authors. Regarding the eruption of Tamboro, there is also a question about whether it was as strong as described by writers from Britain, under whose control Sumbava was at the time. Therefore, there is reason to believe that these are just legends covering the true causes of the catastrophic climate change in the Northern Hemisphere.


**Russian:**

Сомнения эти возникают ещё и потому, что в случае с извержениями вулканов, влияние на климат является временным. Наблюдается некотороепохолодание из-за пепла, который выброшен в верхние слои атмосферы и создаёт экранирующий эффект. Как только этот пепел осядет, климатвосстанавливается к исходному состоянию. Но в 1815 году мы имеем совершенно другую картину, поскольку если в США, Канаде и большинствестран Европы климат постепенно восстановился, то на большей части территории России произошёл так называемый "климатический сдвиг", когдасреднегодовая температура резко упала, а назад потом не вернулась. Никакое извержение вулкана, да ещё и в Южном полушарии, вызвать подобныйклиматический сдвиг не могло. А вот массовое уничтожение лесов и растительности на большой площади, особенно в середине континента,должно давать именно такой эффект. Леса выполняют роль температурных стабилизаторов, не давая земле слишком сильно промерзать зимой, а такжеслишком сильно нагреваться и пересыхать летом.

**English:**

These doubts arise also because in case of volcanic eruptions, the impact on the climate is temporary. There is some cooling because of the ash that is released into the upper atmosphere and creates a shielding effect. Once this ash has settled, the climate is restored to its original state. But in 1815 we have a completely different picture, because if in the U.S., Canada and most European countries the climate gradually recovered, in most of Russia there was a so-called "climate shift", when the average annual temperature fell sharply and then did not return. No volcanic eruption, or even in the Southern Hemisphere, could cause such a climatic shift. But the mass destruction of forests and vegetation on a large area, especially in the middle of the continent, should have this effect. The forests act as temperature stabilizers, preventing the earth from freezing too much in winter and from getting too hot and dry in summer.


**Russian:**

Есть свидетельства, которые говорят о том, что до 19 века климат на территории России, в том числе в Санкт-Петербурге, был заметно теплее. Впервом издании энциклопедии "Британика" от 1771 года сказано, что основным поставщиком ананасов в Европу является Российская империя.Правда, подтвердить эту информацию сложно, поскольку получить доступ к оригиналу данного издания практически невозможно.

**English:**

There is evidence that before the 19th century the climate in Russia, including St. Petersburg, was noticeably warmer. The first edition of the Encyclopedia Britannica of 1771 says that the main supplier of pineapples to Europe is the Russian Empire.


**Russian:**

Но, как и в случае с Аркаимом, о климате 18 века можно многое сказать потем зданиям и сооружениям, которые были построены в то время Санкт-Петербурге. Во время неоднократных поездок по пригородамСанкт-Петерубрга, кроме восхищения талантом и мастерством строителей прошлого, я обратил внимание на одну интересную особенность. Большинстводворцов и особняков, которые строились в 18 веке, были построены под другой, более тёплый климат!

**English:**

But, as in the case of Arkaim, the buildings and structures that were built in the 18th Century say a lot about the climate of that time. And that includes St Petersburg. During repeated trips to the suburbs of St. Petersburg, in addition to admiring the talent and skill of builders of the past, I noticed one interesting feature. Most architecture and mansions that were built in the 18th century were built for a different, warmer climate!


**Russian:**

Во-первых, они имеют очень большую площадь окон. Простенки между окнамиравны или даже меньше ширины самих окон, а сами окна очень высокие.

**English:**

First, they have a very large window area. Partitions between windows are equal or even less than the width of the windows themselves, and the windows themselves are very high.


**Russian:**

Во-вторых, во многих зданиях изначально не предполагалась система отопления, её встраивали позже уже в готовое здание.

**English:**

Secondly, many buildings did not originally have a heating system, it was built into the finished building later.


**Russian:**

Например, посмотрим на Екатерининский дворец в Царском селе.

**English:**

For example, let's look at Catherine's Palace in Tsarskoye Selo.

<!-- Local
[![](How Tartaria was Lost Part 4_files/32738_900.jpg#clickable)](How Tartaria was Lost Part 4_files/32738_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/32738/32738_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/32738/32738_original.jpg)


**Russian:**

Потрясающее огромное здание. Но, как нас уверяют, это "летний дворец". Его построили якобы только для того, чтобы приезжатьсюда исключительно летом.

**English:**

It's a stunning huge building. But we're told, it's a "summer palace." Supposedly, it was built only to come to in the summer.

<!-- Local
[![](How Tartaria was Lost Part 4_files/32834_900.jpg#clickable)](How Tartaria was Lost Part 4_files/32834_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/32834/32834_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/32834/32834_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/33178_900.jpg#clickable)](How Tartaria was Lost Part 4_files/33178_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/33178/33178_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/33178/33178_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/33485_900.jpg#clickable)](How Tartaria was Lost Part 4_files/33485_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/33485/33485_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/33485/33485_original.jpg)

**Russian:**

Если посмотреть на фасад дворца, то прекрасно виднаочень большая площадь окон, которая характерна для южных, жарких районов, а не для северных территорий.

**English:**

If you look at the facade of the palace, you can clearly see the large area of windows, which is typical for southern, hot areas, not the northern territories.

<!-- Local
[![](How Tartaria was Lost Part 4_files/33621_900.jpg#clickable)](How Tartaria was Lost Part 4_files/33621_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/33621/33621_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/33621/33621_original.jpg)

<!-- Suspect the zip doc contained two more images here -->
(.zip document no longer accessible)

**Russian:**

Позже, уже в начале 19 века, к дворцу сделали пристрой, где располагался знаменитый лицей, в котором обучался АлександрСергеевич Пушкин вместе с будущими декабристами. Пристрой отличается не только архитектурным стилем, но и тем, что он уже построен под новыеклиматические условия, площадь окон заметно меньше.

**English:**

Later, at the beginning of the 19th century, an annex was made to the palace, where the famous lyceum - where Alexander Pushkin studied with future Decembrists - was located. The outhouse differs not only in its architectural style, but also in the fact that it already it had been built for new climate conditions: the window area is noticeably smaller.

<!-- Local
[![](How Tartaria was Lost Part 4_files/33847_900.jpg#clickable)](How Tartaria was Lost Part 4_files/33847_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/33847/33847_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/33847/33847_original.jpg)

**Russian:**

Левое крыло, которое рядом с лицеем, было существенноперестроено примерно в то же время, когда строился лицей, а вот правое крыло осталось в том виде, как оно было построено изначально. И в нёмможно увидеть, что печи для отопления помещения не были запланированы изначально, а добавлялись позже в уже готовое здание.

**English:**

The left wing, which is next to the lyceum, was substantially rebuilt around the same time that the lyceum was built, but the right wing remained as it was originally built. And you can see in the picture of it that the stoves for heating the room were not planned in initially. They were added later to the already finished building.

**Russian:**

Вот так выглядит кавалерская (серебрянная) столовая.

**English:**

This is what the Cavalry dining room looks like.

<!-- Local
[![](How Tartaria was Lost Part 4_files/34240_900.jpg#clickable)](How Tartaria was Lost Part 4_files/34240_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/34240/34240_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/34240/34240_original.jpg)

**Russian:**

Печь просто поставили в угол. Отделка стен наличие в этом углу печи игнорирует, то есть, была сделана до того, как она тампоявилась. Если посмотреть на верхнюю часть, то видно, что она не плотно прилегает к стене, поскольку ей мешает фигурная золочённая рельефнаяотделка верха стены.

**English:**

The furnace was just put in a corner. The wall decoration in this corner of the furnace ignores it. That is, it was decorated before the furnace was fitting in. If you look at the top part, you can see that it does not fit tightly against the wall, as it is hampered by the curved gilded relief finishing at the top of the wall.

<!-- Local
[![](How Tartaria was Lost Part 4_files/34374_900.jpg#clickable)](How Tartaria was Lost Part 4_files/34374_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/34374/34374_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/34374/34374_original.jpg)

**Russian:**

Хорошо видно, что отделка стены продолжается сзади запечью.

**English:**

It's good to see that the wall trim continues in the back of the stove.

<!-- Local
[![](How Tartaria was Lost Part 4_files/34635_900.jpg#clickable)](How Tartaria was Lost Part 4_files/34635_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/34635/34635_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/34635/34635_original.jpg)

**Russian:**

Вот ещё один из залов дворца. Тут печь уже лучшевписана в существующее оформление угла, но если посмотреть на пол, то видно, что печь просто стоит сверху. Узор на полу наличие печиигнорирует, уходя под неё. Если бы печь изначально планировалась в данном помещении в этом месте, то любой мастер сделал бы узор пола сучётом этого факта.

**English:**

Here's another one of the halls of the palace. Here the stove is already better integrated into the existing design of the corner, but if you look at the floor, you can see that the stove just stands on top. The pattern on the floor ignores the presence of the stove, going under it. If the furnace was originally planned in this room in this place, then any master would make the floor pattern conform to this fact.


**Russian:**

А в большом зале дворца никаких печей или каминов нет вообще!

**English:**

And the big hall of the palace has no stoves or fireplaces at all!


**Russian:**

Официальная легенда, как я уже говорил, гласит, что это изначально этот дворец планировался как летний, зимой там не жили, поэтому он так ипостроен.

**English:**

The official legend, as I said before, says that this palace was originally planned as a summer palace, they did not live there in winter, so it was built for summer.

**Russian:**

Очень интересно! Вообще-то это ведь не просто сарай, который может спокойно перезимовать и без обогрева. А что будет с интерьерами,картинами и скульптурами, которые вырезаны из дерева, если зимой помещения не топить? Если зимой всё это вымораживать, а весной и осеньюдавать отсыревать, то сколько сезонов сможет простоять всё это великолепие, на создание которого потрачены огромные усилия и ресурсы?Екатерина была весьма умной женщиной и такие-то уж вещи должна была хорошо понимать.

**English:**

Very interesting! Actually, it's not as if the summer palace is a barn that can overwinter in peace without heating. What will happen to the interiors, paintings and sculptures, which are carved out of wood, if the premises are not heated in winter? If all this is frozen in winter, and in spring and autumn it becomes damp, how many seasons could all this splendor withstand? Weren't a lot of effort and resources spent on the creation of these resources? Catherine was a very smart woman and she should have understood such things well.

**Russian:**

Продолжим осмотр Екатерининского дворцы в Царском селе.

**English:**

We'll continue our tour of Catherine's Palace in Tsarskoye Selo.


**Russian:**

По этой ссылке все желающие могут совершить виртуальное путешествие вЦарское село и полюбоваться как внешним видом дворца, так и его интерьерами

**English:**

At this link, everyone can take a virtual trip to Tsarskoe Selo and admire both the palace's appearance and its interiors.


[http://tsars-palaces.livejournal.com/17051.html](http://tsars-palaces.livejournal.com/17051.html)


**Russian:**

Там мы можем увидеть, например, что в первой антикамере (прихожая поитальянски), печи стоят на ножках, что ещё раз подтверждает тот факт, что при строительстве дворца установка печей там не планировалась.

**English:**

There we can see, for example, that in the first anti-chamber (entrance hall in Poitalian), the stoves stand on their feet, which once again confirms the fact that at construction time, the installation of stoves was not part of the plan for the palace.

<!-- Local
![](How Tartaria was Lost Part 4_files/0_9d695_91a7a865_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6302/86441892.2f2/0_9d695_91a7a865_XL.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 4_files/0_9d697_2ad96e47_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6306/86441892.2f2/0_9d697_2ad96e47_XL.jpg#resized)

**Russian:**

Во время просмотра замечательных фотографий я вам рекомендую такжеобратить внимание на тот факт, что для обогрева очень многих помещений во дворце используются вовсе не печи, а камины! Мало того, что каминыочень пожароопасны, из-за чего во всех дворцах регулярно случались пожары, так они ещё и крайне неэффективны для отопления помещений зимой.А судя по тому, что мы видим, именно камины предусматривались как основная система отопления во всех дворцах постройки 18 века. Такую жекартину мы позже увидим и в большом дворце Петергофа, и даже в самом Зимнем Дворце в Санкт-Петербурге. И даже там, где мы сегодня видим печи,судя по тому, как они установлены, они заменили существовавшие когда-то в этих помещениях камины и используют их дымоходы. И установили их какраз потому, что они более эффективны.

**English:**

While viewing the wonderful pictures, I recommend you also to pay attention to the fact that many rooms in the palace are heated not by stoves but by fireplaces! Not only are fireplaces extremely dangerous, which is why palaces are regularly affected by fires, but they are also extremely ineffective at heating rooms in winter and, judging by what we see, fireplaces were envisaged as the main heating system in all palaces built in the 18th century. We will see the same picture later in the big Peterhof Palace, and even in the Winter Palace in St. Petersburg itself. And even where we see stoves today, judging by the way they are installed, they have replaced the fireplaces that once existed in these rooms and use their chimneys. And they've been installed only because they're more efficient.

**Russian:**

То, что к моменту строительства дворцов печи были уже давно известны человечеству как более эффективная и безопасная система отопления, чемкамин, не вызывает никаких сомнений. Поэтому для использование именно каминов в качестве основной системы отопления в царских дворцах должнабыла быть веская причина. Например, использоваться они будут очень редко по причине тёплого климата. То, что это было сделано из-за неграмотностиархитекторов, которые строили дворцы, в списке возможных причин будет стоять на последнем месте, поскольку для проектирования и строительствацарских дворцов приглашали лучших из лучших, да и по всем остальным техническим и архитектурным решениям всё сделано на самом высшем уровне.

**English:**

The fact that by the time the palaces were built, stoves had long been known to mankind as a more efficient and safer heating system than fireplaces is beyond doubt. Therefore, there must have been a good reason for using fireplaces as the main heating system in royal palaces. For example, they would be used very rarely because of the warm climate. The claim that this was done only because the architects who built the palaces were illiterate has to last on the list of possible reasons. This because the best of the best were invited for the design and construction of the royal palaces. And because all the other technical and architectural solutions were all done at the highest level.

**Russian:**

Посмотрим как выглядит Большой Дворец в Петергофе.

**English:**

Let's see how the Grand Palace in Peterhof looks.

<!-- Local
[![](How Tartaria was Lost Part 4_files/35068_900.jpg#clickable)](How Tartaria was Lost Part 4_files/35068_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/35068/35068_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/35068/35068_original.jpg)

<!-- Local
![](How Tartaria was Lost Part 4_files/1000000000000BCD000003181F1E262A.jpg#resized)
-->

<!-- Local
[![](How Tartaria was Lost Part 4_files/35203_900.jpg#clickable)](How Tartaria was Lost Part 4_files/35203_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/35203/35203_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/35203/35203_original.jpg)

**Russian:**

Также, как и в случае с Екатерининским дворцом, мывидим очень большие окна и большую площадь остекления фасадов. Если же мы заглянем во внутрь, то обнаружим, что с системой отопления та жекартина. Большая часть помещений отапливается с помощью каминов. Так выглядит портретный зал.

**English:**

As with the Catherine Palace, we see very large windows and a large area of glazing in the facade. If we look inside, we will find that the heating system is the same. Most of the rooms are heated with fireplaces. That's what the portrait hall looks like.


<!-- Local
[![](How Tartaria was Lost Part 4_files/35349_900.jpg#clickable)](How Tartaria was Lost Part 4_files/35349_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/35349/35349_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/35349/35349_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/35774_900.jpg#clickable)](How Tartaria was Lost Part 4_files/35774_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/35774/35774_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/35774/35774_original.jpg)

**Russian:**

В больших зала, танцевальном и тронном, система отопления непредусмотрена вообще, нет ни печей, ни каминов.

**English:**

In large halls, dancing and throne rooms, no heating system is provided at all. There are no stoves or fireplaces.

<!-- Local
[![](How Tartaria was Lost Part 4_files/35935_900.jpg#clickable)](How Tartaria was Lost Part 4_files/35935_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/35935/35935_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/35935/35935_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/36273_900.jpg#clickable)](How Tartaria was Lost Part 4_files/36273_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/36273/36273_900.jpg#clickable)](https://ic.pics.livejournal.com/mylnikovdm/24488457/36273/36273_original.jpg)

**Russian:**

К сожалению, в залах большого дворца запрещается фотографировать обычным посетителям, поэтому хорошие фотографии егоинтерьеров найти сложно, но даже на тех, что есть, видно отсутствие каминов и печей.

**English:**

Unfortunately, ordinary visitors are forbidden from taking photographs in the halls of the big palace. So good photos of its interiors are difficult to find, but even on those photos that are available, you can see the absence of fireplaces and stoves.

<!-- Local
[![](How Tartaria was Lost Part 4_files/36409_900.jpg#clickable)](How Tartaria was Lost Part 4_files/36409_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/36409/36409_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/36409/36409_original.jpg)

**Russian:**

Аналогичную картину мы видим и в Зимнем дворце, само название которого говорит о том, что он должен быть рассчитан на суровые русские зимы.

**English:**

We see a similar picture in the Winter Palace, the very name of which suggests that it should be designed for severe Russian winters.

**Russian:**

Вот тут можно найти огромную подборку материалов по царским дворцам [http://tsars-palaces.livejournal.com/](http://tsars-palaces.livejournal.com/), включая массу прекрасных фотографий, а также картин разных авторов с изображением интерьеров.Весьма рекомендую.

**English:**

Here you can find a huge selection of materials on royal palaces: [http://tsars-palaces.livejournal.com/](http://tsars-palaces.livejournal.com/), including a lot of beautiful photographs, as well as paintings by different authors depicting interiors. I highly recommend it.

**Russian:**

По Зимнему дворцу там можно посмотреть следующие материалы:

Прогулки по залам Эрмитажа:

- [часть 1 http://tsars-palaces.livejournal.com/16428.html](http://tsars-palaces.livejournal.com/16428.html)

- [часть 2 http://tsars-palaces.livejournal.com/16247.html](http://tsars-palaces.livejournal.com/16247.html)

- [часть 3 http://tsars-palaces.livejournal.com/16720.html](http://tsars-palaces.livejournal.com/16720.html)

Несколько подборок с уникальными акварелями Эдуарда Петровича Гау:

- [http://tsars-palaces.livejournal.com/9375.html](http://tsars-palaces.livejournal.com/9375.html)

- [http://tsars-palaces.livejournal.com/10026.html](http://tsars-palaces.livejournal.com/10026.html)

- [http://tsars-palaces.livejournal.com/10240.html](http://tsars-palaces.livejournal.com/10240.html)

- [http://tsars-palaces.livejournal.com/17497.html](http://tsars-palaces.livejournal.com/17497.html)

- [http://tsars-palaces.livejournal.com/17837.html](http://tsars-palaces.livejournal.com/17837.html)

- [http://tsars-palaces.livejournal.com/18023.html](http://tsars-palaces.livejournal.com/18023.html)

**English:**

The following materials can be viewed at the Winter Palace:

Walks in the halls of the Hermitage:

- [part 1: http://tsars-palaces.livejournal.com/16428.html](http://tsars-palaces.livejournal.com/16428.html)

- [part 2: http://tsars-palaces.livejournal.com/16247.html](http://tsars-palaces.livejournal.com/16247.html)

- [part 3: http://tsars-palaces.livejournal.com/16720.html](http://tsars-palaces.livejournal.com/16720.html)

Several blog posts with unique watercolors by Eduard Gau:

- [http://tsars-palaces.livejournal.com/9375.html](http://tsars-palaces.livejournal.com/9375.html)

- [http://tsars-palaces.livejournal.com/10026.html](http://tsars-palaces.livejournal.com/10026.html)

- [http://tsars-palaces.livejournal.com/10240.html](http://tsars-palaces.livejournal.com/10240.html)

- [http://tsars-palaces.livejournal.com/17497.html](http://tsars-palaces.livejournal.com/17497.html)

- [http://tsars-palaces.livejournal.com/17837.html](http://tsars-palaces.livejournal.com/17837.html)

- [http://tsars-palaces.livejournal.com/18023.html](http://tsars-palaces.livejournal.com/18023.html)

**Russian:**

Говоря про Зимний дворец следует отметить, что сильные пожары происходили в нём регулярно, например в 1837 году, поэтому мы не можемговорить о том, что внутри мы наблюдаем именно то, что было задумано архитектором при его строительстве. Насколько эти пожары былислучайными, вопрос отдельный, который выходит за рамки данной статьи. При этом перестройка внутренних помещений в Зимнем дворце происходилапостоянно, как в следствие пожаров, так и просто по желанию его обитателей. В тоже время следует отметить, что большая часть помещенийЗимнего дворца продолжает отапливаться каминами, не смотря на все перестройки и реконструкции. И я так понимаю, что одна из причин того,что в помещениях оставались именно камины, как раз и состоит в том, что изначально конструкция здания не предусматривала установку печей,которые требуют специальной подготовки здания как в плане фундаментов, так и в плане организации дымоходов и конструкции стен.

**English:**

Speaking about the Winter Palace, it should be noted that strong fires occurred in it regularly, for example in 1837, so we can not claim that the inside we see is exactly what was conceived by the architect during its construction. Whether or not these fires were accidental is a separate issue which is beyond the scope of this article. At the same time, the reconstruction of the interior of the Winter Palace was carried out constantly, both as a result of the fires and simply at the request of its inhabitants. And at the same time, it should be noted that most of the Winter Palace premises continue to be heated by fireplaces, despite all the reconstructions. And I understand that one of the reasons why there were fireplaces in the premises is precisely because the original design of the building did not provide for the installation of stoves, which require special preparation of the building in terms of both foundations and chimneys and wall construction.

**Russian:**

Если же смотреть на фасады Зимнего дворца, то мы видим всё те жепризнаки здания, которое строится для тёплого климата - большая площадь окон, узкие простенки между окнами.

**English:**

If we look at the facades of the Winter Palace, we see all the same signs of a building that was being built for a warm climate: a large area of windows and narrow partitions between the windows.

<!-- Local
![](How Tartaria was Lost Part 4_files/0_a2ff2_9054ffe9_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6314/86441892.339/0_a2ff2_9054ffe9_XL.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 4_files/0_a2fef_6cf140e_XL.jpg#resized)
-->

![](https://img-fotki.yandex.ru/get/6114/86441892.339/0_a2fef_6cf140e_XL.jpg#resized)

**Russian:**

Причём эта особенность наблюдается не только у царских дворцов. Вотфотографии фасадов двух зданий. Первое построено в 18-ом веке, а второе в 19-ом.

**English:**

And this feature is not only observed in royal palaces. Here are photos of the facades of two buildings. The first one was built in the 18th century and the second one in the 19th.

<!-- Local
[![](How Tartaria was Lost Part 4_files/36680_900.jpg#clickable)](How Tartaria was Lost Part 4_files/36680_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/36680/36680_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/36680/36680_original.jpg)

<!-- Local
[![](How Tartaria was Lost Part 4_files/37051_900.jpg#clickable)](How Tartaria was Lost Part 4_files/37051_original.jpg)
-->

[![](https://ic.pics.livejournal.com/mylnikovdm/24488457/37051/37051_900.jpg#clickable)](http://ic.pics.livejournal.com/mylnikovdm/24488457/37051/37051_original.jpg)

**Russian:**

Очень хорошо видна разница в площади остекления, атакже то, что у второго здания ширина простенков между окнами более чем в два раза шире окон, в то время как у первого здания она равна илименьше ширины окон.

**English:**

Very clearly visible is the difference in glazing area and also that, in the second building, the width of partitions between windows is more than twice as wide as the width of the windows, while in the first building it is equal or less than the width of windows.

**Russian:**

Начиная с 19 века здания в Санкт-Петербурге начинают строить с учётом новых климатических условий, а с 30-х годов 19 века уже появляютсяздания с центральным отоплением, у которых котельная находится в отдельной пристройке или вообще как самостоятельное сооружение во дворе,которое обслуживает несколько прилегающих домов. Например, во время последнего посещения Санк-Перебурга летом этого года я жил в доме поадресу ул. Чайковского, д. 2, которое было построено в 1842 году сразу с отдельной котельной и централизованной системой водяного отопления.отдельной котельной и централизованной системой водяного отопления.

**English:**

Starting from the 19th century, buildings in St. Petersburg began to be built taking into account the new climatic conditions, and from the 30s of the 19th century there had already appeared buildings with central heating, where the boiler house is located in a separate annex or in general as an independent structure in the yard, which serves several adjacent houses. For example, during my last visit to Sanka-Pereburga in the summer of this year, I lived in a house at 2 Tchaikovsky Street, which was built in 1842 with a separate boiler house and a centralized water heating system.

© All rights reserved. The original author retains ownership and rights.
