title: Onward to the Past - The Faking of Conventional History
date: 2019-12-18
modified: 2022-04-24 09:58:18
category: Tart-Aria.info
tags: St Petersburg
slug: 
authors: tech_dancer
summary: Something was taught to us in history classes, but recently it is beginning to rapidly collapse under the onslaught of facts extracted from the depths by numerous researchers. It turns out that many historical documents were simply forged for a specific purpose.
status: published
originally: Forward to the Past.html

### Translated from:

[https://www.tart-aria.info/vpered-v-proshloe/](https://www.tart-aria.info/vpered-v-proshloe/)

[tech_dancer](https://www.tart-aria.info/author/tech_dancer/) Разносторонне развитый инженер.

[tech_dancer](https://www.tart-aria.info/author/tech_dancer/) is an advanced engineer.

**Russian:**

Хотелось назвать данную статью "Назад, в будущее", но уж слишком заезженной показалась эта фраза. Её неоднократно употребляли многие писатели разных лет, и сейчас это звучит избито, не свежо и не актуально. Ну и в связи с этим более подошёл инверсный вариант. Не исключено, что его тоже употребляли и употребляют, но в нашем случае это будет звучать более корректно. Благо и сюжет для данной статьи более подходит именно под эту фразу.

**English:**

I wanted to call this article "Back to the Future", but this phrase seemed too hackneyed. It was repeatedly used by many writers of different years, and now it sounds hackneyed, not fresh and not relevant. Well, in this regard, the inverse option is more suitable. It is possible that it was also used and is still used, but in our case it will sound more correct. Fortunately, the plot for this article is more suitable for this phrase.

**Russian:**

Известный интернет-политолог неоднократно анализировал поведение Саудовской Аравии на современной политической арене, которое на его взгляд представляет большой интерес. Действительно, как известно, руководством этой страны принята непростая задача - снизить зависимость бюджета страны от продаж нефти и перейти от полуфеодального состояния к стандартам общественного развития 21 века. Первую часть задачи оставим без комментариев, а вот ко второй слегка приглядимся. Зачем это всё для страны, которая всю известную свою историю никогда не жила по другому, и это вполне устраивало всех её граждан? История знала похожие случаи и до этого, когда например Монголия на волне влияния мирового коммунизма декларировала переход сразу же из феодализма в социализм, то есть через одну ступень развития общества. Чем это закончилось, всем известно. Зачем же так рваться в непонятное и несвойственное будущее, когда прошлое всё равно приходит и берёт своё по полному праву? Может, сразу пойти в прошлое, дабы не тратить время и ресурсы? А может, и само прошлое было таким хорошим, что никакого будущего и не надо, вот только мы об этом не знаем?

**English:**

A well-known Internet political scientist has repeatedly analyzed the behavior of Saudi Arabia in the modern political arena, which in his opinion is of great interest. Indeed, as you know, the leadership of this country has adopted a difficult task-to reduce the dependence of the country's budget on oil sales and move from a semi-feudal state to the standards of social development of the 21st century. We will leave the first part of the problem without comments,but we will take a closer look at the second part. Why do all this for a country that has never lived differently in its entire known history, and this is quite acceptable to all its citizens? History has known similar cases before, when, for example, Mongolia, in the Wake of the influence of world communism, declared the transition immediately from feudalism to socialism, that is, through one stage of development of society. Everyone knows how it ended. Why so rush into an incomprehensible and uncharacteristic future, when the past still comes and takes its rightful place? Maybe go straight back in time, so as not to waste time and resources? Or maybe the past itself was so good that no future is necessary, but we don't know about it?

**Russian:**

Вопросов слишком много. Но давайте на этом закончим вступление и перейдём к главной теме. А именно к агитации похода в прошлое. Нам то это зачем? И тут, давайте, положа руку на сердце, сами себе скажем, что прошлого мы не знаем. Что-то нам преподавали на уроках истории, но в последнее время это начинает стремительно разрушаться под натиском фактов, доставаемых из глубин многочисленными исследователями. Выясняется, что многие исторические документы были банально подделаны с определённой целью. Остались только старые фотографии, которые подделывали не так массово. Имелись, конечно же, постановочные и ретушированные фото, но местами сохранились и оригинальные, которые по счастливому случаю (а может, по недосмотру цензоров) попали в общий доступ. Такие фотографии могут рассказать о многом. Ну и мы попробуем понять на основе этих фотографий, каким же оно было, то самое прошлое. Ну и в некоторых местах будут использоваться фотографии из прошлых статей, т.к. найти их в общем доступе очень сложно.

**English:**

There are too many questions. But let's finish the introduction and move on to the main topic. Namely, to agitate a trip to the past. Why would we do that? And here, let's put our hands on our hearts and tell ourselves that we don't know the past. Something was taught to us in history classes, but recently it is beginning to rapidly collapse under the onslaught of facts extracted from the depths by numerous researchers. It turns out that many historical documents were simply forged for a specific purpose. There were only old photos that were not so massively forged. There were, of course, staged and retouched photos, but in some places the original ones were preserved, which by a lucky chance (or perhaps by an oversight of the censors) were made publicly available. These photos can tell you a lot. Well, we will try to understand on the basis of these photos, what it was like, the very past. Well, in some places, photos from previous articles will be used, because it is very difficult to find them in the public domain.

**Russian:**

Согласно учению известного, но слегка подзабытого классика, основой жизни и развития общества выступает материальное производство, т.е. производство материальных благ. И с этой истиной не поспоришь. Она справедлива даже для феодального общества. По тому же классику, производство материальных благ обеспечивает процесс постоянного воспроизводства самой жизни людей, без чего вообще невозможно существование общества. Если перефразировать на понятный язык, то рост популяции человека возможен только при производстве материальных предметов, обеспечивающих нормальные условия жизни этой популяции. Без такого производства начинается хаос, и количество людей, даже при высокой рождаемости, начинает стремительно сокращаться из-за голода, болезней и борьбы за выживание. Примеров этому масса. Но речь сейчас о другом.

**English:**

According to the teachings of a well-known, but slightly forgotten classic, the basis of life and development of society is material production, i.e. the production of material goods. And you can't argue with this truth. It is true even for a feudal society. According to the same classic, the production of material goods provides a process of constant reproduction of the very life of people, without which it is impossible for society to exist at all. If we paraphrase it in a clear language, the growth of the human population is possible only in the production of material items that provide normal living conditions for this population. Without such production, chaos begins, and the number of people, even with a high birth rate, begins to decline rapidly due to hunger, disease, and the struggle for survival. There are many examples of this. But this is about something else.

**Russian:**

Каким образом осуществлялось данное производство в прошлом? Про натуральное хозяйство, мелкотоварное производство и т.п. мы уже слышали. А давайте обратимся к тем самым старым фотографиям.

<!-- Local
[![](Forward to the Past_files/1-India_boat_1857_2-750x605.jpg#clickable)](Forward to the Past_files/1-India_boat_1857_2.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/1-India_boat_1857_2-750x605.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/1-India_boat_1857_2.jpg)

**English:**

How was this production carried out in the past? We have already heard about subsistence farming, small-scale production, etc. And let's turn to those old photos.

**Russian:**

Это Индия образца 1857 года, если верить аннотации фото (что следует делать далеко не всегда). Для чего в стране, имеющей в то время (да и сейчас) множество малообеспеченных людей, такие корабли? Это даже не предмет первой необходимости. Можно возразить, что в любом подобном обществе есть богатые и сверхбогатые люди, которые могут позволить себе подобное. Но в данном случае всё объясняется разрухой на заднем плане и полузатопленными кораблями. И этот манерный корабль просто брошен. Не будем вдаваться, кем он брошен и почему, а отметим для себя, что его как минимум надо построить (т.е. произвести) и уметь им управлять. Но как управлять именно таким? Наше прошлое на поверку оказывается не таким простым. Впрочем, к кораблям мы ещё вернёмся.

**English:**

This is India of the 1857 model, according to the photo annotation (which should not always be done). Why in a country that had at that time (and still has) a lot of low-income people, such ships? It's not even an essential item. One might argue that in any such society, there are rich and super-rich people who can afford such things. But in this case, everything is explained by the devastation in the background and half-submerged ships. And this mannered ship is just abandoned. We will not go into who threw it and why, but note for ourselves that it at least needs to be built (i.e. produce) and be able to manage it. But how do you manage this one? Our past turns out to be not so simple. However, we will return to the ships later.

**Russian:**

Или другой пример. Во многих музеях мира и просто в старинных помещениях находится много люстр и подсвечников с отверстиями для помещения в них каких-то предметов. Нас постоянно убеждают, что в эти люстры вставлялись свечи, и это при том, что от пола до самих люстр имеются значительные расстояния, а на потолках нет ни малейшего следа копоти. И вдруг оказывается, что это не совсем так. Точнее, совсем не так.

**English:**

Or another example. In many museums around the world and just in old rooms there are many chandeliers and candlesticks with holes for placing some objects in them. We are constantly convinced that candles were inserted into these chandeliers, and this despite the fact that there are considerable distances from the floor to the chandeliers themselves, and there is not the slightest trace of soot on the ceilings. And suddenly it turns out that this is not quite true. Or rather, not at all.

<!-- Local
[![](Forward to the Past_files/2-750x566.jpg#clickable)](Forward to the Past_files/2.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/2-750x566.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/2.jpg)

**Russian:**

Это зал для трапез где-то в США 19 века. Как видим, свечи здесь как-то далеки. И в то же время, даже если свечи условно заменялись стаканами с какой-то жидкостью, их надо для образования света как-то поджигать и дозаправлять. Как это можно сделать при таком количестве? Но даже не это интересно, а то, как это произвести и что это вообще такое? И куда это исчезло, причём бесследно, и заменилось легендами о свечах из органических материалов?

**English:**

This is a dining hall somewhere in the 19th century USA. As you can see, the candles are somehow far away. And at the same time, even if the candles were conditionally replaced with glasses with some liquid, they must be somehow ignited and refueled to form light. How can this be done with this number? But even this is not interesting, but how to produce it and what is it in General? And where did this disappear, and without a trace, and was replaced by legends about candles made of organic materials?

**Russian:**

Наше прошлое смотрит на нас с пугающей откровенностью подобных фотографий. И виртуальная дорога в это прошлое была бы явно не простой. Например такой.

<!-- Local
[![](Forward to the Past_files/3-19th-century-photographer-henry-taunt-monovisions-04-750x5.jpg#clickable)](Forward to the Past_files/3-19th-century-photographer-henry-taunt-monovisions-04.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/3-19th-century-photographer-henry-taunt-monovisions-04-750x5.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/3-19th-century-photographer-henry-taunt-monovisions-04.jpg)

**English:**

Our past looks at us with the frightening frankness of such photos. And the virtual road to this past would clearly not be easy. Such as this.

**Russian:**

И шишек на такой дороге пришлось бы набить немало. Но... продолжим.

**English:**

And you'd have to fill a lot of bumps on such a road, But ... let's continue.

<!-- Local
[![](Forward to the Past_files/4-X12418-750x581.jpg#clickable)](Forward to the Past_files/4-X12418.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/4-X12418-750x581.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/4-X12418.jpg)

**Russian:**

Это таможенный терминал образца 1865 года где-то на побережье Тихого океана. Таможня, как впрочем любое другое государственное учреждение, по определению не может функционировать без каналов связи с единым центром. И если от таможенного терминала до этого центра сотни километров, курьерская связь по понятным причинам отпадает сразу. Без средств связи подобный пункт сбора пошлин стал бы лёгкой добычей морских грабителей или коммерческой организацией (полагаю, все поняли, о чём я). И глядя на это фото, попробуйте угадать, какая там была связь. И это до Попова и Маркони.

**English:**

This is an 1865 customs terminal somewhere on the Pacific coast. Customs, like any other state institution, by definition cannot function without communication channels with a single center. And if there are hundreds of kilometers from the customs terminal to this center, the courier connection for obvious reasons disappears immediately. Without communications, a toll booth like this would be easy prey for sea robbers or a commercial organization (I guess everyone knows what I mean). And looking at this photo, try to guess what the connection was. And this is before Popov and Marconi.

**Russian:**

Полагаю, не стоит ломать голову, достаточно взглянуть на вид типового почтового отделения из Бразилии от 19 века. Пусть это не фотография, но на определённые мысли наводит.

**English:**

I don't think you need to worry, just look at the type of post office from Brazil from the 19th century. It may not be a photo, but it does suggest certain thoughts.

<!-- Local
[![](Forward to the Past_files/5-750x433.jpg#clickable)](Forward to the Past_files/5.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/5-750x433.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/5.jpg)

**Russian:**

Прошлое поражает нас удивлять. Возможно ли было существование беспроводного телеграфа до его официального изобретения? Очевидно, да. Только в 19 веке произошла неведомая нам перезагрузка массового сознания, причём в мировом масштабе (пусть даже где-то и с опозданием на полвека-век). Как она могла произойти? Это другой вопрос, но факт имеет место быть - после неё весь мир начал думать по другому. В том числе и про своё прошлое.

**English:**

The past amazes us. Was it possible that the wireless Telegraph existed before its official invention? Obviously, Yes. Only in the 19th century there was an unknown reset of mass consciousness, and on a global scale (even if somewhere and with a delay of half a century or a century). How could it have happened? This is a different question, but the fact is that after her, the whole world began to think differently. Including about your past.

<!-- Local
[![](Forward to the Past_files/6-750x464.jpg#clickable)](Forward to the Past_files/6.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/6-750x464.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/6.jpg)

**Russian:**

Это Швейцария образца 1850-60 годов. Наверное, если заменить всех людей с фото на рыцарей, выглядело бы тоже вполне реалистично. Особенно возле афишной тумбы на заднем плане. Странная, кстати, тумба какая-то. Она явно отличается от своих современных видов, и даже от рудиментов старых тумб, ещё местами сохранившихся в Европе. Взгляните на её крышу. Официально, вроде бы, уличные уборщики хранили в таких тумбах свой инвентарь. А что там было на самом деле? Но здесь интересно несколько другое - то общество, изображенное на фото, имело такое качество жизни, которое позволяло им производить такие вот тумбы для рекламы культурных мероприятий. Это явно не предметы первой необходимости, которые нужны при феодальном строе, чтобы выжить. Но это только Европа. А как же остальной мир?

**English:**

This is Switzerland of the 1850-60 model. Probably, if you replace all the people from the photo with knights, it would also look quite realistic. Especially near the poster stand in the background. Strange, by the way, some kind of Cabinet. It clearly differs from its modern types, and even from the rudiments of old curbstones, still preserved in places in Europe. Take a look at it. Officially, it seems, street cleaners kept their inventory in such cabinets. What was really there? But what is interesting here is a little different-the society depicted in the photo had a quality of life that allowed them to produce such curbstones for advertising cultural events. These are clearly not the basic necessities that are needed in a feudal system to survive. But this is only Europe. What about the rest of the world?

<!-- Local
[![](Forward to the Past_files/7-1870_bus-750x533.jpg#clickable)](Forward to the Past_files/7-1870_bus.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/7-1870_bus-750x533.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/7-1870_bus.jpg)

**Russian:**

А это простой американский автобус образца 1870 года. Интересно, как он работал? Теперь уже наверное не узнаем. Особенно интересно выглядят колёса. Слово "шина" применительно к колёсам ещё окончательно не вышло из оборота, по крайней мере в русском языке. И в то же время в технике этим словом называют проводник, на который монтируют другие подходящие или отходящие проводники меньшего сечения для осуществления электрического контакта. Невольно вспоминаются дорожные мостовые из металлических деталей, которые ещё кое-где сохранились. Неужели это и есть тот самый случай? И кстати, автомобиль такого типа ещё надо произвести. Судя по его виду, в кустарных условиях это не возможно. Вот бы сесть на такой автобус и поехать в это самое прошлое, но... будем реалистами и посмотрим на корабли прошлого с того же континента.

**English:**

And this is a simple American bus model 1870. I wonder how it worked? We probably won't know now. The wheels look particularly interesting. The word "tire"in relation to wheels has not yet completely left the circulation, at least in Russian. And at the same time, in technology, this word is called a conductor on which other suitable or outgoing conductors of smaller cross-section are mounted to make electrical contact. I can't help but think of road pavements made of metal parts, which are still preserved in some places. Is this the case? And by the way, a car of this type still needs to be produced. Judging by its appearance, this is not possible in artisanal conditions. I would like to take a bus like this and go to this very past, but... let's be realistic and look at the ships of the past from the same continent.

<!-- Local
[![](Forward to the Past_files/8-1.jpg#clickable)](Forward to the Past_files/8-1.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/8-1.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/8-1.jpg)

**Russian:**

Когда впервые увидел корабль на заднем плане этого дагерротипа, думал, показалось. Уж слишком смело он выглядит, не находите? Пусть там есть две трубы, этот факт совсем не удивителен. Но что из них торчит (запомните эту деталь труб) и что находится чуть дальше труб? По официальным данным, дагерротипы стали резко вытесняться обычными фотографиями после 1850 года, и есть все основания полагать, что это фото 1 половины 19 века. И как оказалось, это совсем не ошибка.

**English:**

When I first saw the ship in the background of this daguerreotype, I thought I was imagining it. It looks too bold, don't you think? Even if there are two pipes, this fact is not at all surprising. But what is sticking out of them (remember this part of the pipes) and what is just beyond the pipes? According to official data, daguerreotypes began to be sharply replaced by ordinary photos after 1850, and there is every reason to believe that this is a photo of the 1st half of the 19th century. And as it turned out, this is not a mistake at all.
Fast forward to the past tech_dancer

<!-- Local
[![](Forward to the Past_files/9-High-pressure-steamboat-Mayflower-first-class-packet-betwe.jpg#clickable)](Forward to the Past_files/9-High-pressure-steamboat-Mayflower-first-class-packet-between-St.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/9-High-pressure-steamboat-Mayflower-first-class-packet-betwe.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/9-High-pressure-steamboat-Mayflower-first-class-packet-between-St.jpg)

**Russian:**

Поскольку это рисунок, прошу сейчас обратить внимание только на самую верхнюю часть корабля. Для чего там нужна та самая в некотором роде церквушка? Наверное, её политкорректно стали называть капитанским мостиком, и находился там cap-tain (крышка для чего-то там), который с течением времени после той самой перезагрузки превратился в корабельный чин. Видимо, был в этой конструкции какой-то секрет, который вращал гребные винты таких кораблей.

**English:**

Since this is a drawing, please pay attention only to the uppermost part of the ship. What is the purpose of that little Church of some sort? Probably, it was politically correct to call it the captain's bridge, and there was a cap-tain (a cover for something there), which over time after that very reboot turned into a ship's rank. Apparently, there was some secret in this design that turned the propellers of such ships.

**Russian:**

Может, всё же это есть ошибка? Совсем нет.

**English:**

Maybe this is a mistake after all? Not at all.

<!-- Local
[![](Forward to the Past_files/10-Browns-landing-Rice-Creek-750x377.jpg#clickable)](Forward to the Past_files/10-Browns-landing-Rice-Creek.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/10-Browns-landing-Rice-Creek-750x377.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/10-Browns-landing-Rice-Creek.jpg)

**Russian:**

Наше прошлое уплыло вместе с этими кораблями, причём уплыло так, что практически отовсюду были удалены их следы и подброшены фотографии совсем других кораблей. И кстати, чтобы изготовить такой корабль, завод нужен ещё серьёзнее, чем для рассмотренного выше автобуса. Но Америка есть Америка, её возможности тогда и сейчас были немалыми. А что происходило на других континентах?

**English:**

Our past sailed away with these ships, and sailed away so that almost everywhere their traces were removed and photos of completely different ships were planted. And by the way, to make such a ship, the plant is needed even more seriously than for the bus discussed above. But America is America, and its opportunities then and now were considerable. What happened on other continents?

<!-- Local
[![](Forward to the Past_files/11-Dwelling-Beulah-Park-1905-750x517.jpeg#clickable)](Forward to the Past_files/11-Dwelling-Beulah-Park-1905.jpeg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/11-Dwelling-Beulah-Park-1905-750x517.jpeg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/11-Dwelling-Beulah-Park-1905.jpeg)

**Russian:**

Подобная конструкция в очень упрощённом виде, к примеру, освещает крыльцо какого-то дома в Южной Африке. Просто освещает, не имея к тому же никаких подходящих проводов. А это между прочим начало 20 века. Тоже прошлое, причём совсем недавнее. Почему же нигде не осталось ничего подобного, даже просто в человеческой памяти? И неужели это было так просто?

**English:**

Such a design in a very simplified form, for example, illuminates the porch of a house in South Africa. Just illuminates, without having any suitable wires. And this, by the way, is the beginning of the 20th century. Also the past, and quite recently. Why is there nothing like this left anywhere, even just in human memory? And was it really that easy?

<!-- Local
[![](Forward to the Past_files/12-750x984.jpg#clickable)](Forward to the Past_files/12-750x984.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/12-750x984.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/12-750x984.jpg)

**Russian:**

В Египте времён правления Исмаил-Паши (конец 19 века) подобные конструкции занимались трансляцией музыки в окружающее пространство. Да-да, именно трансляцией, как сейчас это делают микрофон и усилительная аппаратура. Надо отметить, Исмаил-Паша получил европейское образование, и подобные идеи музыкальных ретрансляторов он привёз скорее всего оттуда. Сохранилось множество фотографий таких музыкальных киосков практически из всех крупных городов Европы того времени. Каким же уровнем развития обладало общество в том самом прошлом, если оно умело создавать подобное, чего не сделать и сейчас?

**English:**

In Egypt, during the reign of Ismail Pasha (late 19th century), such structures were used to transmit music to the surrounding space. Yes, it is broadcast, as now it is done by a microphone and amplifying equipment. It should be noted that Ismail Pasha received a European education, and he most likely brought similar ideas of music repeaters from there. There are many photos of such music kiosks from almost all major European cities of that time. What level of development did society have in the very past, if it is able to create such a thing, which is not done now?

<!-- Local
[![](Forward to the Past_files/13-Opening-of-the-Beaton-Memorial-Fountain-in-Port-Augusta.jpeg#clickable)](Forward to the Past_files/13-Opening-of-the-Beaton-Memorial-Fountain-in-Port-Augusta.jpeg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/13-Opening-of-the-Beaton-Memorial-Fountain-in-Port-Augusta.jpeg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/13-Opening-of-the-Beaton-Memorial-Fountain-in-Port-Augusta.jpeg)

**Russian:**

А в Австралии с помощью таких конструкций просто добывали воду. Без всяких громоздких гидравлических систем. Австралия, кстати, является континентом, большую часть которого занимают пустыни с непригодными для жизни условиями. Именно из-за отсутствия воды. Как же в прошлом так получалось, что проблему решали таким лёгким способом? И что за волшебными свойствами обладала та самая конструкция, дающая усиленную музыку, свет и тягу в корабельных двигателях? Какими сакральными знаниями обладали мастера, которые создавали подобное волшебство? Но уж если это сохранилось на фото, то волшебства тут нет совсем.

**English:**

And in Australia, using such structures, they simply extracted water. Without any bulky hydraulic systems. Australia, by the way, is a continent most of which is occupied by deserts with uninhabitable conditions. It is because of the lack of water. How did it happen in the past that the problem was solved in such an easy way? And what were the magical properties of the same design that gave amplified music, light, and thrust in the ship's engines? What sacred knowledge did the masters who created such magic possess? But if this is preserved in the photo, then there is no magic at all.

**Russian:**

Но и это ещё далеко не всё.

**English:**

But this is not all.

<!-- Local
[![](Forward to the Past_files/14-750x947.jpg#clickable)](Forward to the Past_files/14-750x947.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/14-750x947.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/14-750x947.jpg)

**Russian:**

Угадайте, в каком веке сделано это фото внутреннего интерьера какой-то из церквей Бразилии. По аннотации архива это 1885 год. Интересно, а молились ли вообще в таких местах? Там просто постоять и посмотреть на всё это великолепие уже приносило духовное исцеление, не говоря уже о том, что по многочисленным данным, в таких местах исцеление совершалось путём влияния на человека неких физико-химических процессов, которые сейчас забыты или хранятся под строгим секретом. Увы, все эти атрибуты прошлого тоже ушли, вместе с тем самым прошлым.

**English:**

Guess what century this photo was taken of the interior of one of the churches in Brazil. According to the archive annotation, it is 1885. I wonder if they ever prayed in such places? There just to stand and look at all this splendor already brought spiritual healing, not to mention the fact that according to numerous data, in such places healing was performed by the influence of certain physical and chemical processes that are now forgotten or kept under strict secrecy. Alas, all these attributes of the past are also gone, along with the very past.

<!-- Local
[![](Forward to the Past_files/15-750x548.jpeg#clickable)](Forward to the Past_files/15.jpeg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/15-750x548.jpeg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/15.jpeg)

**Russian:**

И снова Австралия, где в начале 20 века подготовили какой-то из городов Нового Южного Уэльса под торжественный приём герцога Йоркского. Почему-то после 1920 года подобные иллюминации пропадают, и не только в Австралии, но и по всему миру. И как ни парадоксально, это происходит именно в то время, когда начинается научно-технический рывок 20 века. Так может, тот рывок был совсем не в ту сторону?

**English:**

And again Australia, where at the beginning of the 20th century prepared some of the cities of New South Wales for the Grand reception of the Duke of York. For some reason, after 1920, such illuminations disappear, and not only in Australia, but also around the world. And paradoxically, this is happening at a time when the scientific and technical breakthrough of the 20th century begins. So maybe that jump was in the wrong direction at all?

**Russian:**

Кстати, об этом странном историческом персонаже герцоге Йоркском будут чуть позже приведены несколько интересных фактов. А кто-нибудь слышал вообще про такого? По идее, его многие должны знать. Попробуйте разгадать, о ком идёт речь, пока до него дойдёт очередь по тексту.

**English:**

By the way, a few interesting facts will be given later about this strange historical character, the Duke of York. Has anyone ever heard of such a person? In theory, many people should know it. Try to figure out who you are talking about until it reaches the turn of the text.

**Russian:**

Но раз мы начали в самом начале статьи ссылаться на классика, подарившего всему миру вместе со своей философией материальное производство с его базисом и надстройкой, то как же выглядели заводы, которые это всё производили?

**English:**

But since we started at the very beginning of the article to refer to the classic, who gave the whole world, along with his philosophy, material production with its basis and superstructure, what did the factories that produced it all look like?

**Russian:**

Собственно, выглядели они также, как нам и показывали в многочисленных художественных фильмах. И даже по всему миру осталось ещё очень много зданий тех заводов. И сохранилось много старых фотографий цехов тех заводов. Тех самых фотографий, где через весь цех идёт длинная трансмиссия с валами и шкивами, и каждый станок подключался к ней индивидуально своим приводным ремнём. Всё бы ничего, но везде под строжайшим секретом скрыто то оборудование, которое приводило в движение эту трансмиссию.

**English:**

In fact, they looked the same as we were shown in numerous feature films. And even around the world there are still a lot of buildings of those factories. And many old photos of the workshops of those factories have been preserved. The same photos where a long transmission with shafts and pulleys runs through the entire shop, and each machine was connected to it individually with its own drive belt. Everything would be fine, but everywhere under the strictest secret is hidden the equipment that drove this transmission.

<!-- Local
[![](Forward to the Past_files/16-750x587.jpg#clickable)](Forward to the Past_files/16.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/16-750x587.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/16.jpg)

**Russian:**

Как то странно, но мы снова видим аналог той же конструкции, как и на капитанском мостике того самого корабля. Как вы думаете, возможна ли в принципе на заводах, где стояло подобное оборудование, эксплуатация человека человеком, как воспел другой не менее известный классик? Ответ напрашивается сам собой. Труды обоих этих классиков написаны гениально и во многом очень даже правдиво, но они написаны про совсем другие заводы, которые пришли на смену этим самым заводам из прошлого. Заводы, которые пришли вновь, как раз обладали всеми атрибутами, а именно пролетариатом и его эксплуатацией. Наверное, это и есть та самая буржуазная революция, о которой вскользь упоминалось в учебниках истории. Ну а заводы прошлого также канули в лету, оставив может быть для своих преемников старые здания. Как же так незаметно могла пройти перезагрузка таких масштабов и размеров?

**English:**

Oddly enough, we again see an analog of the same design as on the bridge of the same ship. What do you think, is it possible in principle in factories where there was such equipment, the exploitation of man by man, as sung by another equally well-known classic? The answer is obvious. The works of both of these classics are written brilliantly and in many ways very truthfully, but they are written about completely different factories that replaced these very factories from the past. The factories that came back had all the attributes, namely, the proletariat and its exploitation. Perhaps this is the same bourgeois revolution that was mentioned in passing in the history textbooks. Well, the factories of the past have also sunk into oblivion, leaving perhaps for their successors the old buildings. How could a reboot of this scale and size go so unnoticed?

**Russian:**

Полагаю, вид следующего здания на современном фото знаком многим.

**English:**

I think the view of the next building in the modern photo is familiar to many.

<!-- Local
[![](Forward to the Past_files/17-2-750x504.jpg#clickable)](Forward to the Past_files/17-2.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/17-2-750x504.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/17-2.jpg)

**Russian:**

Подобные здания в различных вариациях сохранились по всему миру. В России их тоже сохранилось немало. Данные здания возводились в основном для технических нужд на железных дорогах. Даже не будучи профессиональным архитектором, при взгляде на подобные здания всегда находил некую незавершенность. Очень странным выглядел тот факт, что старинные мастера делали кровлю подобного здания (сооружения) плоской без особой на то нужды. Интерес к этой теме подогрела некая гравюра, запечатлевшая какую-то железнодорожную станцию в районе Страсбурга, в 1865 году. Точнее не сама гравюра, а одна малая деталь на ней.

**English:**

Similar buildings in various variations have been preserved all over the world. In Russia, too, a lot of them have been preserved. These buildings were built mainly for technical needs on the Railways. Even without being a professional architect, when looking at such buildings, I always found a certain incompleteness. Very strange was the fact that the old masters made the roof of such a building (structure) flat without any special need. Interest in this topic was fueled by an engraving depicting a railway station in the area of Strasbourg, in 1865. More precisely, not the engraving itself, but one small detail on it.

<!-- Local
[![](Forward to the Past_files/18-Chemins_de_fer_de_lEst_.jpg#clickable)](Forward to the Past_files/18-Chemins_de_fer_de_lEst_...Maugendre_A_btv1b10227524r_1.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/18-Chemins_de_fer_de_lEst_.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/18-Chemins_de_fer_de_lEst_...Maugendre_A_btv1b10227524r_1.jpg)

**Russian:**

На рисунке изображена труба, та самая, из которой никогда не шёл дым. В различных материалах исследований эти трубы многократно описаны. Остатков этих труб сохранилось ещё немало. Но здесь на такой трубе просматривается нечто, что не наблюдалось ни на фото, ни на оставшихся трубах. Что это за таинственная деталь? И вспомним тот самый дагерротип с кораблём, трубами и непонятными деталями на этих трубах.

**English:**

The drawing shows a chimney, the one that never gave off smoke. These pipes are repeatedly described in various research materials. There are still many remnants of these pipes. But here on such a pipe you can see something that was not observed either in the photo or on the remaining pipes. What is this mysterious detail? And remember the same daguerreotype with the ship, pipes and strange details on these pipes.

**Russian:**

Совершенно случайно нашлось старое фото железной дороги Швейцарии от 19 века, и загадка решилась сама собой.

**English:**

Quite by chance, an old photo of the Swiss railway from the 19th century was found, and the mystery was solved by itself.

<!-- Local
[![](Forward to the Past_files/19-df_hauptkatalog_05054061-750x537.jpg#clickable)](Forward to the Past_files/19-df_hauptkatalog_05054061.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/19-df_hauptkatalog_05054061-750x537.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/19-df_hauptkatalog_05054061.jpg)

**Russian:**

Увы, все имеющиеся остатки подобных сооружений во всём мире доработаны, а именно их верхняя металлическая часть демонтирована полностью. Если на кораблях эта конструкция стояла для вращения винтов, то здесь то зачем? Ответ снова очевиден. Это устройство, которое обеспечивало тягу всех локомотивов, ехавших по данному шлейфу дороги. Или, кто в курсе, то, что делают сейчас ТЧ и ШЧ, вместе взятые (железнодорожники поймут о чём речь), делало всего лишь одно это здание. И делало, надо отметить, без сбоев и человеческого фактора, как часы. Ничего себе было прошлое.

**English:**

Alas, all the existing remains of such structures around the world have been finalized, namely, their upper metal part has been completely dismantled. If on ships this design was used for rotating propellers, then why here? The answer is obvious again. This is a device that provided traction for all locomotives traveling along a given road loop. Or, who knows, what PM and SHCH are doing now, taken together (the railway workers will understand what we are talking about), only this one building did. And it did, it should be noted, without failures and the human factor, like a clock. What a past it was.

**Russian:**

Остатки подобных сооружений иногда сейчас обозначают как водоёмные здания при железной дороге. Не исключено, что так оно и есть, но подобные факты надо тщательно проверять. Вероятно, что подобные здания в старом виде, с железным куполом наверху, были многофункциональными и не только осуществляли тягу, но и хранили запас воды для паровозов. После той самой перезагрузки железная часть была снесена полностью, что в общем сделать вполне легко. И то же самое было сделано на тех самых бездымовых трубах, которые скорее всего были упрощённой формой подобных зданий. Но это тема для отдельного рассказа.

**English:**

The remains of such structures are sometimes now designated as reservoir buildings at the railway. It is possible that this is the case, but such facts should be carefully checked. It is likely that such buildings in the old form, with an iron dome at the top, were multifunctional and not only carried out traction, but also stored a supply of water for locomotives. After that very reboot, the iron part was completely demolished, which in General is quite easy to do. And the same was done on those smokeless chimneys, which were most likely a simplified form of such buildings. But this is a topic for a separate story.

**Russian:**

Внимательные читатели наверняка обратили внимание на некие странные детали, которые можно заметить при увеличении фрагмента фото с этой железной конструкцией на железнодорожном здании. Если нет, то прилагаю крупный план.

**English:**

Attentive readers probably noticed some strange details that can be noticed when you zoom in on a fragment of a photo with this iron structure on a railway building. If not, I attach a close-up.

<!-- Local
[![](Forward to the Past_files/20-750x576.jpg#clickable)](Forward to the Past_files/20.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/20-750x576.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/20.jpg)

**Russian:**

Какое-то дежавю. Где-то это уже встречалось. Кстати, пишу сейчас из той страны, где перед Рождеством подобные светящиеся символы висят почти на каждом доме. И не случайно упомянул выше про ТЧ и ШЧ, вместе взятые. ШЧ, помимо прочего, обеспечивает ещё и связь. Почему форму такой конструкции называли Люциферовой звездой?

**English:**

Some kind of deja vu. I've seen this before somewhere. By the way, I am writing now from the country where such glowing symbols hang on almost every house before Christmas. And it is not by chance that I mentioned above about PM and WH combined. SHCH, among other things, also provides communication. Why was the shape of such a structure called a Lucifer star?

**Russian:**


> Люцифе́р (Луцифер, лат. Lucifer «светоносный», от lux «свет» + fero «несу»), в римской мифологии — персонификация утренней звезды — планеты Венеры.

**English:**


> Lucifer (Lucifer, lat. Lucifer "light-bearing", from lux "light" + fero "carry"), in Roman mythology-personification of the morning star-the planet Venus.

Таак... И какой же свет несла эта звезда на палочке? В данном случае это был явно не свет оптического диапазона, хотя ничего исключать нельзя. Для чего такой лучевой свет на железной дороге? Вспомним таможенный пост с антеннами на крыше. Сомнений нет - эти звёзды были прообразами современных радиорелейных линий, только в том самом прошлом. Какими по размерам и форме эти звёзды стали в 20 веке, хотя бы на той же железной дороге, полагаю, многие представляют. Ничего не поделаешь, это перезагрузка и научно-техническая революция.

**English:**

Taak... And what light did this star on a stick carry? In this case, it was clearly not light in the optical range, although nothing can be ruled out. Why is there such a beam of light on the railway? Remember the customs post with antennas on the roof. There is no doubt-these stars were the prototypes of modern radio relay lines, only in the very past. What size and shape these stars became in the 20th century, at least on the same railway, I think many people imagine. There's nothing you can do, it's a reboot and a scientific and technological revolution.

**Russian:**

И в очередной раз наше прошлое нас удивляет и ставит перед нами очередной вопрос - как это вдруг такие звёзды становятся символами каких-то субкультур? Каким таким рейдерским захватом их взяли и заставили нести свет от одних народов к другим, и исключительно в наглядной агитации? Вопрос остаётся без ответа. Ну или пока без ответа. Но смотрим дальше.

**English:**

And once again, our past surprises us and raises another question - how can such stars suddenly become symbols of some subcultures? What kind of raider capture did they take and make them carry the light from one nation to another, and only in visual agitation? The question remains unanswered. Well, or no answer yet. But we look further.

<!-- Local
[![](Forward to the Past_files/21-750x597.jpg#clickable)](Forward to the Past_files/21.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/21-750x597.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/21.jpg)

**Russian:**

Это фото какой-то оранжереи из Бразилии (странно, сам понимаю) сезона 1860-1870 годов. Подобные оранжереи в прошлом были совсем не редкостью в те времена, но это тоже тема для отдельного рассказа. Они встречались практически во всех странах. И здесь, как и выше, интересна совсем не оранжерея, а задний план.

**English:**

This is a photo of some greenhouse from Brazil (strange, I understand it myself) of the 1860-1870 season. Such greenhouses in the past were not uncommon in those days, but this is also a topic for a separate story. They were found in almost all countries. And here, as above, it is not the greenhouse that is interesting, but the background.

<!-- Local
[![](Forward to the Past_files/22-750x564.jpg#clickable)](Forward to the Past_files/22.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/22-750x564.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/22.jpg)

**Russian:**

Здесь Люциферовы звёзды стоят уже как символы. Хотя по архитектуре здания можно подумать, что это совсем не Бразилия, а какой нибудь Ближний Восток. Кто захватил эту территорию и водрузил подобные символы, и главное, а что же там было в прошлом до этого захвата? Какая страна? Ещё одна загадка прошлого. Но и это не всё.

**English:**

Here Lucifer's stars are already symbols. Although the architecture of the building makes you think that this is not Brazil at all, but some kind of middle East. Who captured this territory and planted such symbols, and most importantly, what was there in the past before this capture? What country? Another mystery of the past. But that's not all.

<!-- Local
[![](Forward to the Past_files/23-750x543.jpg#clickable)](Forward to the Past_files/23.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/23-750x543.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/23.jpg)

**Russian:**

На фото машинного зала какой-то выставки из Австралии мы видим те самые наши конструкции, но на них стоит непонятный символ. Точнее, более чем понятный и даже до боли знакомый всем советским людям. Как он там очутился? Кому светила данная звезда, чтобы перейти затем с таких технических узлов на красные знамёна с последующим уничтожением тех самых технических узлов?

**English:**

In the photo of the machine room of an exhibition from Australia, we see our very designs, but they have an incomprehensible symbol on them. More precisely, more than understandable and even painfully familiar to all Soviet people. How did he get there? To whom did this star Shine in order to move from such technical nodes to red banners with the subsequent destruction of those very technical nodes?

<!-- Local
[![](Forward to the Past_files/24-Brokoff_Bellmann_04.jpg#clickable)](Forward to the Past_files/24-Brokoff_Bellmann_04.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/24-Brokoff_Bellmann_04.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/24-Brokoff_Bellmann_04.jpg)

**Russian:**

А это уже Прага, памятник на Карловом мосту, который стоял там ещё в 1870 году. Как видим, весь набор символов в наличии, причём в звёздах явно присутствуют электрические лампы, и не те, которые изобрели Эдисон с Яблочковым. Какому народу эти лампы несли свет? На вид памятник свежий, и даже сделан не очень аккуратно. Кто захватил и эту территорию, чтобы водрузить там те же символы, уничтожив устройства, прообразами которых они были? Кстати, полагаю, что многие не знают, откуда пошло название американской валюты - доллара. Это всего лишь название чешской горы Талер, на которой эти доллары впервые стали чеканить. Почему первая твёрдая валюта начала издаваться в Чехии? На восточных территориях эта валюта тоже ходила, но под другим названием.

**English:**

And this is Prague, a monument on the Charles bridge, which stood there in 1870. As you can see, the entire set of symbols is available, and the stars clearly contain electric lamps, and not the ones that Edison and Yablochkov invented. To what people did these lamps bring light? The monument looks fresh, and even made not very carefully. Who took over this territory to plant the same symbols there, destroying the devices of which they were prototypes? By the way, I believe that many people do not know where the name of the American currency - the dollar-came from. This is just the name of the Czech mountain Thaler, where these dollars were first minted. Why was the first hard currency published in the Czech Republic? In the Eastern territories, this currency was also used, but under a different name.

**Russian:**

Полагаю, все понимают, почему например Финляндию назвали Финляндией, Арабию (Аравию) - Аравией, Мавританию - Мавританией. А почему Европу назвали Европой? Истории про арабское или древнегреческое происхождение этого слова уже слышали, верится с трудом. Слышал версию, что Якутия и Иудея суть два одинаковых обозначения одной географической области. Коренные народы Якутии никогда так свою страну не называли, они называли и называют её Саха. Уж не оттуда ли в Европу Моисей вывел свои народы? Очень странные совпадения.

**English:**

I think everyone understands why, for example, Finland was called Finland, Arabia (Arabia) - Arabia, Mauritania - Mauritania. Why was Europe called Europe? Stories about the Arabic or ancient Greek origin of this word have already been heard, it is hard to believe. I have heard the version that Yakutia and Judea are two identical designations of the same geographical area. The indigenous peoples of Yakutia have never called their country that, they called it Sakha. Was it from there that Moses brought his people to Europe? Very strange coincidences.

**Russian:**

Полагаю, здесь надо сделать небольшое отступление, дабы читатель понял, о чём речь. 19 век практически целиком является малоизвестным историческим периодом. Практически все официальные исторические документы по тому периоду сфальсифицированы (мнение автора, полагаю, что согласятся многие). Судить о событиях того периода мы можем только по таким вот фото, случайно просочившимся в наши дни, и по другим косвенным выводам. Из того, что можно сейчас понять, то в 19 веке вся планета условно разделилась на две воюющие стороны. Назовём их условно Империя и Интернационал (полагаю, ошибёмся не сильно). В ходе проводимых военных действий Империя потерпела поражение от Интернационала. Не исключено, что победа Интернационала была по сути пирровой победой, т.к. в ходе военных действий были истрачены почти все ресурсы, и их уже не хватило на тотальную оккупацию территорий Империи. И очень большой загадкой является некий природный катаклизм, уничтоживший бóльшую часть планеты. Чем он был - причиной или следствием тех военных действий, установить сейчас сложно. История была переписана победившей стороной, все ненужные артефакты от прошлой жизни были уничтожены. И все технические достижения прошлого были внедрены в повседневный обиход в виде их символов. И так исчезло то самое прошлое, которое мы сейчас с грустью вспоминаем.

**English:**

I think we should make a small digression here, so that the reader will understand what we are talking about. The 19th century is almost entirely a little-known historical period. Almost all official historical documents for that period are falsified (the author's opinion, I believe that many will agree). We can only judge the events of that period by such photos that have accidentally leaked in our days, and by other indirect conclusions. From what we can now understand, in the 19th century, the entire planet was conditionally divided into two warring parties. Let's call them the Empire and the international (I think we won't make a big mistake). In the course of the ongoing military operations, the Empire was defeated by the international. It is possible that the victory of the international was in fact a Pyrrhic victory, since almost all the resources were spent during the military operations, and they were no longer enough for the total occupation of the territories of the Empire. And a very big mystery is a natural cataclysm that destroyed most of the planet. What it was-the cause or consequence of those military actions, it is difficult to establish now. History was rewritten by the winning side, all unnecessary artifacts from the previous life were destroyed. And all the technical achievements of the past were introduced into everyday life in the form of their symbols. And so the very past that we now remember with sadness disappeared.

**Russian:**

Приписывать эти символы определенной национальности, наверное, не совсем справедливо. Равно как и обвинять эту национальность во всех смертных грехах и мучениях прочих народов. С таким же успехом можно обвинять щуку, которую Всевышний создал, чтобы карась не дремал. В экосистеме всё продумано, и истребление одного вида ни к чему хорошему для оставшихся видов не приводит. И если применять эту аллегорию на 20 век и современные дни, то получается, что весь этот период был ознаменован тем, что либо щуки давили карасей, либо караси давили щук, либо щуки давили карасей силами самих же карасей. А Всевышнему, который создал всех этих видов, глубоко всё равно на эту мирскую суету. Впрочем, вернёмся.

**English:**

Attributing these symbols to a particular nationality is probably not entirely fair. As well as accusing this nationality of all the deadly sins and torments of other peoples. You might as well blame the pike, which God created to keep the carp awake. In the ecosystem, everything is thought out, and the destruction of one species does not lead to anything good for the remaining species. And if we apply this allegory to the 20th century and modern days, it turns out that this entire period was marked by the fact that either pike crushed carp, or carp crushed pike, or pike crushed carp by the forces of the carp themselves. And the Supreme, who created all these species, does not care about this worldly vanity. However, let's go back.

**Russian:**

Полагаю, вы уже догадались, какие территориальные образования стояли за определениями Империи и Интернационала. Начнём, пожалуй, с последнего.

**English:**

I think you have already guessed what territorial formations were behind the definitions of the Empire and the international. Let's start with the last one.

<!-- Local
[![](Forward to the Past_files/24-Brokoff_Bellmann_04.jpg#clickable)](Forward to the Past_files/25.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/25-750x641.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/25.jpg)

<!-- Local
[![](Forward to the Past_files/24-Brokoff_Bellmann_04.jpg#clickable)](Forward to the Past_files/26.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/26.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/26.jpg)

**Russian:**

Ничего случайно эта символика не напоминает? Некоторые государства до сих пор имеют нечто похожее. А в 19 веке Интернационал включал в себя очень много наций, собственно из-за этого он так и назывался. И под действием объединенных сил Интернационала Империя стала терять свои земли одну за другой. Постепенно она потеряла все свои владения в Африке, обоих Америках, Ближнем Востоке, Индии, Китае. В Европе её владения значительно уменьшились. Оставшаяся часть владений перешла под чужой протекторат, но об этом позже.

**English:**

Does this symbolism remind you of anything? Some States still have something similar. And in the 19th century, the international included a lot of Nations, in fact, because of this, it was called so. And under the combined forces of the international, the Empire began to lose its lands one by one. Gradually, it lost all its possessions in Africa, the Americas, the middle East, India, and China. In Europe, its holdings have significantly decreased. The remaining part of the domain passed under someone else's protectorate, but more on that later.

**Russian:**

Заранее приношу свои извинения англичанам за публикацию подобных картинок. Ни в коей мере не хочу оскорбить их национальное достоинство или поставить под сомнение официальную историю их флага. Просто изображённый на нём символ очень странно совпадает с некой технической архитектурной деталью, которая видимо в прошлом также имела свой функционал. Как впрочем и другие символы с флагов тех времён.

**English:**

I apologize in advance to the British for publishing such images. In no way do I want to offend their national dignity or question the official history of their flag. It's just that the symbol depicted on it very strangely coincides with a certain technical architectural detail, which apparently also had its own functionality in the past. As well as other symbols from the flags of those times.

<!-- Local
[![](Forward to the Past_files/icon46391_03-750x693.jpg#clickable)](Forward to the Past_files/icon46391_03.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/icon46391_03-750x693.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/icon46391_03.jpg)

**Russian:**

Что это была за деталь? Наверное, в её центре было нечто, что привлекало внимание. Неспроста же в это место на флаге поместили герб. Как уже говорил ранее, если на каком-то артефакте с признаками древности разместили любой символ, то искать надо именно здесь. Этот символ и даёт подсказку, что им заменили какой-то секрет прошлого. Интересно, а что же символизировал красный цвет флага? Легенду о цвете пролитой за свободу крови оставим как вечернюю сказку для партийных функционеров. Догадки на этот счёт есть, но это всего лишь догадки. Может, дочитав до конца, кто-то из читателей поймёт.

**English:**

What was that detail? There must have been something in the center of it that attracted attention. It is not for nothing that the coat of arms was placed in this place on the flag. As I said earlier, if any symbol was placed on an artifact with signs of antiquity, then you should look for it here. This symbol also gives a hint that they have replaced some secret of the past. I wonder what the red color of the flag symbolized? We will leave the legend of the color of blood spilled for freedom as an evening fairy tale for party functionaries. There are guesses about this, but they are just guesses. Maybe, after reading to the end, some of the readers will understand.

**Russian:**

Есть несколько интересных моментов по тем самым войнам Интернационала и Империи, которые следовало бы отметить.

**English:**

There are several interesting points about the wars of the international and the Empire that should be noted.
<!-- Local
[![](Forward to the Past_files/27-Soldiers-Templemore-1854-750x640.jpg#clickable)](Forward to the Past_files/27-Soldiers-Templemore-1854.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/27-Soldiers-Templemore-1854-750x640.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/27-Soldiers-Templemore-1854.jpg)

**Russian:**

На этом редком фото солдат армии Интернационала образца 1854 года есть очень много странностей. Солдаты одеты в светлую одежду, что в военное время не совсем практично, а главное, что следует как вывод, у них не было цели маскироваться под окружающее пространство. Очевидно, снайперов в то время не было. Чем это вызвано и что это вообще за род войск? Но это не всё. Носить на голове цилиндры, а не каски, например в боевых условиях ещё более непрактично. И этот цилиндр застёгивался очень странно, снизу под подбородок. А что находилось на этом цилиндре? При ближнем просмотре можно констатировать, что это не кисточки, а те же... лампочки. И стояла обратная цель - этих солдат должно было быть видно издалека. Зачем? Найти на фото что-то похожее удалось с очень большим трудом.

**English:**

This rare photo of soldiers of the army of the international sample of 1854 has a lot of oddities. The soldiers are dressed in light clothing, which is not very practical in wartime, and the main thing that follows as a conclusion, they had no purpose to disguise themselves as the surrounding space. Obviously, there were no snipers at that time. What is the reason for this and what kind of military service is it? But that's not all. Wearing top hats on your head rather than hard hats is even more impractical in combat, for example. And this top hat was fastened very strangely, under the chin. What was on that cylinder? When you look closely, you can see that these are not brushes, but the same... light bulbs. And there was a reverse goal - these soldiers should have been visible from afar. What for? It was very difficult to find something similar in the photo.

<!-- Local
[![](Forward to the Past_files/28-750x495.jpg#clickable)](Forward to the Past_files/28.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/28-750x495.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/28.jpg)

**Russian:**

Имеется ряд признаков, что данное фото является постановочным, но тем не менее, версия с лампочками, причём разноцветными, только подтверждается. Но загадок меньше не становится. И чтобы их решить, рассмотрим фото одной из кампаний Интернационала против Империи - второй опиумной войны в Китае 1860 года (так её благородно сейчас называют).

**English:**

There are a number of signs that this photo is staged, but nevertheless, the version with light bulbs, and multi-colored, is only confirmed. But there are no fewer mysteries. And to solve them, consider a photo of one of the international's campaigns against the Empire - the second opium war in China in 1860 (as it is now nobly called).

<!-- Local
[![](Forward to the Past_files/29-Interior-of-Pehtang-Fort-Showing-Probyns-Horses-750x304.jpg#clickable)](Forward to the Past_files/29-Interior-of-Pehtang-Fort-Showing-Probyns-Horses.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/29-Interior-of-Pehtang-Fort-Showing-Probyns-Horses-750x304.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/29-Interior-of-Pehtang-Fort-Showing-Probyns-Horses.jpg)

**Russian:**

Это внешний вид форта Пехтанг, занятого союзными войсками в Китае. Несмотря на серьёзность внешнего вида форта и его явную роль в этой войне, Википедия его даже не упоминает. Есть одна маленькая странность - шатры войск с определённым интервалом идут вдоль всей стены форта. Зачем? Но не будем вдаваться в тонкости военного дела тех времён, а посмотрим другую фотографию из того же места.

**English:**

This is the exterior of Pehtang Fort occupied by allied forces in China. Despite the seriousness of the Fort's appearance and its obvious role in this war, Wikipedia doesn't even mention it. There is one small oddity - the tents of the troops at a certain interval go along the entire wall of the Fort. What for? But we will not go into the subtleties of military Affairs of those times, and look at another photo from the same place.

<!-- Local
[![](Forward to the Past_files/30-Interior-of-Pehtang-Fort-Showing-the-Magazine-and-Wooden-.jpg#clickable)](Forward to the Past_files/30-Interior-of-Pehtang-Fort-Showing-the-Magazine-and-Wooden-Gun-1860.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/30-Interior-of-Pehtang-Fort-Showing-the-Magazine-and-Wooden-.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/30-Interior-of-Pehtang-Fort-Showing-the-Magazine-and-Wooden-Gun-1860.jpg)

**Russian:**

Совершенно не стесняясь архив называет это фото "Солдаты у деревянного орудия". На вид, как ни странно, орудие действительно выглядит деревянным и стянутым обручами. Но кроме этого, ещё есть шины-колёса и кучи непонятных тросов, один из которых очень сильно похож на электрокабель. И вот тут уже совершенно резонно возникает даже не один вопрос. Как стреляли данные орудия и зачем тут кабель?

**English:**

The archive does not hesitate to call this photo "Soldiers at a wooden gun". In appearance, oddly enough, the weapon really looks wooden and tied with hoops. But in addition, there are tires-wheels and piles of strange cables, one of which is very similar to an electric cable. And here it is quite reasonable to have more than one question. How did these guns fire and why is there a cable?

<!-- Local
[![](Forward to the Past_files/31-Head-Quarter-Staff-Pehtang-Fort-1860-750x595.jpg#clickable)](Forward to the Past_files/31-Head-Quarter-Staff-Pehtang-Fort-1860.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/31-Head-Quarter-Staff-Pehtang-Fort-1860-750x595.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/31-Head-Quarter-Staff-Pehtang-Fort-1860.jpg)

**Russian:**

Это фото того же места, но под другим углом. И мы снова видим действительно деревянный ствол и кабели. Это явно не ошибка. Вверху возле флага стоят два шатра. Загадок становится ещё больше. И ещё одна деталь - на стороне союзников воюют явно азиаты. Как это всё понимать? Взглянем ещё на одно фото.

**English:**

This is a photo of the same place, but from a different angle. And again we see a really wooden trunk and cables. This is clearly not a mistake. At the top near the flag there are two tents. There are even more riddles. And one more detail - clearly Asians are fighting on the side of the allies. What does this mean? Let's take a look at another photo.

<!-- Local
[![](Forward to the Past_files/32-The-Provost-Marshal-of-the-Division-of-General-Bosquet-75.jpg#clickable)](Forward to the Past_files/32-The-Provost-Marshal-of-the-Division-of-General-Bosquet.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/32-The-Provost-Marshal-of-the-Division-of-General-Bosquet-75.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/32-The-Provost-Marshal-of-the-Division-of-General-Bosquet.jpg)

**Russian:**

Обратим пристальное внимание на аксельбант, про который ранее было написано очень много и у разных исследователей. Почему он уходит разными линиями в погон? Первая ассоциация сразу же выдаёт то, что в погоне находится та самая шина, в которую через разъёмы входят наконечники аксельбантов. И если противоположные наконечники распутанного аксельбанта ввести в свободный погон следующего солдата, то получается живая цепь. Термин, кстати, распространенный. И если такая цепь выстроится возле ряда орудий, то всё встает на свои места. Лампочка на головном уборе будет давать чёткую индикацию того, что цепь исправна. Видимость её будет отличной даже ночью, что в общем и требовалось. В полевых условиях организация таких батарей является одним из важных дел. А сами орудия были деревянными, кожаными и прочими, и никаких пороховых зарядов не использовалось. Стреляли они с помощью обычных кабелей, которые заходили в шатры. Шатры есть не что иное, как те самые металлические конструкции, только в походно-полевом исполнении. Было у них и такое военное приложение.

**English:**

Let's pay close attention to the axelbant, which was previously written about a lot by various researchers. Why does it go in different lines in pursuit? The first Association immediately gives out that the same tire is in pursuit, into which the tips of the axelbants enter through the connectors. And if the opposite tips of the untangled aiguillette are inserted into the free shoulder strap of the next soldier, then a live chain is obtained. The term, by the way, is common. And if such a chain is lined up near a row of guns, then everything falls into place. The light on the headdress will give a clear indication that the chain is working properly. Visibility will be excellent even at night, which is generally required. In the field, the organization of such batteries is one of the most important things. The guns themselves were made of wood, leather, and other materials, and no powder charges were used. They fired using the usual cables that went into the tents. Tents are nothing more than the same metal structures, only in the field version. They also had such a military application.

**Russian:**

В русском языке есть хорошо известное слово "ряд", которое в общем подразумевает некий набор однотипных предметов на определённом расстоянии друг от друга. Представим ряд таких деревянных орудий, например в бойницах того же форта. Что стоит за этим рядом орудий? Очевидно, за-ряд. Ещё раз отмотаем назад и посмотрим на фото форта Пехтанг, где вдоль стен сразу за бойницами стоят шатры. Посмотрим также другие фото, где лежат ядра с отверстиями калибром много больше имеющихся орудий. Вот он и есть, тот самый заряд, который после введения пороха так и продолжал называться. И неспроста военный чин, который в англоязычной армии назывался cap-tain, в российской армии ещё долго назывался урядником. Паззлы удивительно начинают складываться.

**English:**

In Russian, there is a well-known word "row", which generally implies a certain set of similar objects at a certain distance from each other. Let's imagine a number of such wooden tools, for example, in the loopholes of the same Fort. What is behind this row of guns? Obviously, for-row. Let's rewind again and look at the photo of Pehtang Fort, where tents stand along the walls just behind the loopholes. Let's also look at other photos where there are cores with holes of a caliber much larger than the existing guns. Here it is, the same charge, which after the introduction of gunpowder and continued to be called. And it is not without reason that the military rank, which in the English-speaking army was called cap-tain, in the Russian army for a long time was called uryadnik. The puzzles are surprisingly beginning to add up.

**Russian:**

И оружие это, кстати, имело огромную разрушительную силу, не зря же артиллеристов называли богами сражений. В сочетании с примитивностью исполнения это оружие давало войскам колоссальные возможности и преимущества. Такие, что после победы Интернационала над Империей это оружие было решено полностью уничтожить, в целях собственной же безопасности. Мало ли что. Тем более что внутри сил Интернационала нередко вспыхивали сражения, которые с годами становились всё сильнее, пока не переросли в мировые войны.

**English:**

And this weapon, by the way, had a huge destructive force, no wonder the gunners were called the gods of battles. In combination with the primitiveness of the execution of this weapon gave the troops great opportunities and benefits. Such that after the victory of the international over the Empire, this weapon was decided to completely destroy, for their own safety. You never know. Moreover, within the forces of the international, battles often broke out, which over the years became stronger and stronger, until they turned into world wars.

**Russian:**

И кстати, ещё одно безотносительное фото времён второй опиумной войны в Китае.

**English:**

And by the way, another irrelevant photo from the second opium war in China.

<!-- Local
[![](Forward to the Past_files/33-750x885.jpg#clickable)](Forward to the Past_files/33.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/33-750x885.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/33.jpg)

**Russian:**

Не являюсь специалистом по восточным языкам, но по моему там что-то написано вязью. Это в Китае 1860 года. А здесь конкретно Пекин. В аннотации сухо написано, что это разрушенная татарская мечеть. Судя по внешнему виду, разрушена эта мечеть не так давно. Кто жил в Китае в прошлом?

**English:**

I am not an expert in Eastern languages, but I think there is something written in script. This is in China in 1860. And here specifically Beijing. The annotation says dryly that this is a destroyed Tatar mosque. Judging by its appearance, this mosque was destroyed not so long ago. Who lived in China in the past?

**Russian:**

Впрочем, не надо искать ответ на этот вопрос. Внутри Империи гармонично на одной территории проживали очень различные народы, причём даже местами разных рас. Представители одной из рас владели секретами технических устройств, которые обеспечивали все народы теми самыми материальными и духовными благами (по классику), причём совершенно бесплатными, и в огромном масштабе. Среди этих народов никогда не шли войны - им было не за что воевать. Правило страной обычное натуральное хозяйство, которое было совсем не таким плохим и отсталым, как нам преподавали в школе. Государственный аппарат не был громоздким и малоэффективным, и было много чего ещё, чему сейчас мы можем позавидовать. И даже после завоевания Империи остатки былого уклада очень долго существовали на её территории, пока не были уничтожены совсем. Вышеприведённые фото и есть те самые запечатленные остатки прошлого. Того прошлого, в которое и надо идти.

**English:**

However, there is no need to look for an answer to this question. Within the Empire, very different peoples lived harmoniously on the same territory, and even in places of different races. Representatives of one of the races possessed the secrets of technical devices that provided all peoples with the same material and spiritual benefits (according to the classics), and completely free, and on a huge scale. There were never any wars among these peoples - they had nothing to fight for. The rule was simple subsistence farming, which was not as bad or backward as we were taught at school. The state apparatus was not cumbersome and ineffective, and there were many other things that we can now envy. And even after the conquest of the Empire, the remnants of the former way of life existed for a very long time on its territory, until they were completely destroyed. The above photos are the most imprinted remnants of the past. The past that you need to go to.

<!-- Local
[![](Forward to the Past_files/34-750x947.jpg#clickable)](Forward to the Past_files/34.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/34-750x947.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/34.jpg)

**Russian:**

Слабой стороной Империи было то, что с ней не надо было воевать классическим способом. Достаточно было разрушить те самые технические устройства, производившие материальные блага, и человек быстро становился тем, кто он есть на самом деле. Начиналась банальная борьба за выживание, и народы, жившие до этого вместе не одну сотню (может быть) лет, начинали уничтожать друг друга по национальному признаку. И практически всегда проигрывал народ, представители которого владели теми самыми техническими секретами воспроизводства материальных благ. На некоторых территориях Империи их просто уничтожили, как например на современных территориях Турции, Сирии, Ирана, Средней Азии. Как видим, не случайно на фото второй опиумной войны за союзные войска воюют азиаты. И таких примеров по малопонятным и малоизвестным войнам 19 века на других континентах очень много. Как говорится, разделяй и властвуй.

**English:**

The weak point of the Empire was that it didn't have to be fought in the classic way. It was enough to destroy the very technical devices that produced material goods, and a person quickly became who he really is. A banal struggle for survival began, and peoples who had lived together for more than one hundred (maybe) years before began to destroy each other on a national basis. And almost always lost the people, whose representatives owned the very technical secrets of reproduction of material goods. In some territories of the Empire, they were simply destroyed, such as in the modern territories of Turkey, Syria, Iran, and Central Asia. As you can see, it is no accident that Asians are fighting for the allied forces in the photo of the second opium war. And there are many such examples of obscure and little-known wars of the 19th century on other continents. As they say, divide and conquer.

<!-- Local
[![](Forward to the Past_files/35-750x577.jpg#clickable)](Forward to the Past_files/35.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/35-750x577.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/35.jpg)

**Russian:**

А технические достижения Империи, как например эти беспроводные столбы с недавно повешенными проводами, стали быстро приспосабливать под получение дохода. Глупо было бы не использовать такой ресурс для обогащения. И делалось это постепенно, без резких движений, с учётом естественного убытия населения, ещё помнившего те золотые времена прошлого.

**English:**

And the Empire's technological advances, such as these wireless poles with newly hung wires, were quickly adapted to generate revenue. It would be foolish not to use such a resource for enrichment. And this was done gradually, without sudden movements, taking into account the natural decline of the population, who still remembered those Golden times of the past.

<!-- Local
[![](Forward to the Past_files/36-750x988.jpg#clickable)](Forward to the Past_files/36.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/36-750x988.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/36.jpg)

**Russian:**

На этом фото изображена иллюминация по случаю прибытия в Сидней (Новый Южный Уэльс, Австралия) некоего герцога Йоркского, который, судя по сохранившимся фото, приезжал туда регулярно с 1901 года. Вот только фотографий этого герцога в сети почему-то нет в период с 1901 года и вплоть до 1930-х годов, несмотря на то, что иллюминации по его приезду сняты подробнейшим образом. А может, этот герцог приехал только один раз и остался там навсегда?

**English:**

This photo shows the illumination on the occasion of the arrival in Sydney (New South Wales, Australia) of a certain Duke of York, who, according to the surviving photos, came there regularly since 1901. But for some reason there are no photos of this Duke on the web in the period from 1901 until the 1930s, despite the fact that the illuminations on his arrival were shot in detail. Or maybe this Duke only came once and stayed there forever?

<!-- Local
[![](Forward to the Past_files/37-Accession-of-Edward-VII-illumination-of-Parliamen.jpg#clickable)](Forward to the Past_files/37-Accession-of-Edward-VII-illumination-of-Parliamen....jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/37-Accession-of-Edward-VII-illumination-of-Parliamen.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/37-Accession-of-Edward-VII-illumination-of-Parliamen....jpg)

**Russian:**

А эти красочные иллюминации, напоминавшие чем-то аналогичные из Москвы, просто растиражировали.

**English:**

And these colorful illuminations, resembling something similar from Moscow, just replicated.

<!-- Local
[![](Forward to the Past_files/38-750x443.jpg#clickable)](Forward to the Past_files/38.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/38-750x443.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/38.jpg)

**Russian:**

На данном фото этот герцог Йоркский уже в образе короля Георга, который приехал в Австралию в 1916 году. На дворе Великая война (как её называли в тех странах), а он совсем её не побоялся для такой поездки. И имеются сведения, что он приезжал ещё в 1918 и 1921 году. Собственно, вот и этот герцог Йорскский собственной персоной, но уже в другом статусе.

**English:**

In this photo, this Duke of York is already in the image of king George, who came to Australia in 1916. It was the Great war (as it was called in those countries), and he was not afraid of it at all for such a trip. And there is evidence that he came back in 1918 and 1921. Actually, this is the Duke of York in person, but in a different status.

<!-- Local
[![](Forward to the Past_files/39-the-victory-smile-of-his-majesty-the-king-a6e3f5-750x925.jpg#clickable)](Forward to the Past_files/39-the-victory-smile-of-his-majesty-the-king-a6e3f5.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/39-the-victory-smile-of-his-majesty-the-king-a6e3f5-750x925.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/39-the-victory-smile-of-his-majesty-the-king-a6e3f5.jpg)

**Russian:**

Никого не узнаёте в этом слегка постаревшем господине? Если бы он захотел совершить виртуальный тур из Санкт-Петербурга (например) в Сидней, причём в Европе в это время шла война на суше, море и воздухе, то самым идеальным маршрутом для него была бы поездка на железной дороге через Екатеринбург куда-нибудь на Владивосток или Порт-Артур, ну а дальше вопрос техники. Полагаю, до русскоязычного сегмента читателей начинает доходить масштаб оболванивания народных масс.

**English:**

Do you recognize anyone in this slightly aged gentleman? If he wanted to make a virtual tour from St. Petersburg (for example) to Sydney, and in Europe at this time there was a war on land, sea and air, then the most ideal route for him would be a trip by rail through Yekaterinburg to somewhere in Vladivostok or Port Arthur, and then the question of technology. I believe that the Russian-speaking segment of readers is beginning to reach the scale of fooling the masses.

**Russian:**

Ну а что происходило в 1/6 части суши на территории вроде бы не завоёванной Империи?

**English:**

Well, what happened in 1/6 of the land on the territory of a seemingly UN-conquered Empire?

<!-- Local
[![](Forward to the Past_files/40-750x475.jpg#clickable)](Forward to the Past_files/40.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/40-750x475.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/40.jpg)

**Russian:**

А собственно, всё то же - Люциферовы звёзды в виде символов везде, на любых народных мероприятиях. Есть много старых фото, где они присутствуют на зданиях и даже на склонах гор в Киеве, Владимире и других городах. Как это могло произойти? Очевидно, официальная история нам не договаривает очень много.

**English:**

And actually, all the same-Lucifer stars in the form of symbols everywhere, at any folk events. There are many old photos where they are present on buildings and even on the slopes of mountains in Kiev, Vladimir and other cities. How could this happen? Obviously, the official history doesn't tell us very much.

**Russian:**

Вероятнее всего, Крымская война для нас раскрыта только на малую часть, основной акцент при этом сделан на войну 1812 года. С какой целью? Если эти события действительно хронологически разнесены и соответствуют своим датам (в чём есть совершенно доказанные сомнения многих исследователей), то можно смело заявить, что Империя была именно завоёвана полностью в первой половине 19 века, и именно тогда началось уничтожение всех её технологических достижений, а где это сделать не удалось, началась массовая подмена их бутафорией.

**English:**

Most likely, the Crimean war for us is revealed only in a small part, the main focus is on the war of 1812. For what purpose? If these events are really chronologically spaced and correspond to their dates (which is absolutely proven doubts of many researchers), then we can safely say that the Empire was completely conquered in the first half of the 19th century, and it was then that the destruction of all its technological achievements began, and where this could not be done, the mass substitution of their props began.

<!-- Local
[![](Forward to the Past_files/41.jpg#clickable)](Forward to the Past_files/41.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/41.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/41.jpg)

**Russian:**

Фотосвидетельств тому сохранилось крайне мало. Действительно, победителям было вовсе не нужно сохранять память для потомков тех, кто выжил в этой войне.

**English:**

There is very little photographic evidence of this. Indeed, the victors did not need to preserve the memory for the descendants of those who survived the war.

**Russian:**

Подсказки иногда может дать сам русский язык. К примеру, ещё до сих пор известен статус столбового дворянина, который имела знать той самой номинально новой Империи. Что это обозначает? В Википедии, как всегда, имеется очень куцее безотносительное определение этого титула. Причём здесь какие-то столбцы и были ли они вообще? Наверное, здесь опять надо вспомнить архитектуру.

**English:**

Sometimes the Russian language itself can give you hints. For example, the status of a pillar nobleman, which the nobility of the nominally new Empire had, is still known. What does this mean? Wikipedia, as always, has a very short, non-relative definition of this title. What does this have to do with any columns, and were there any columns at all? Probably, here again we need to remember the architecture.

**Russian:**

Двором в то время, да и во многих местах сейчас, называется пристрой к деревянному дому, выполненный общей крышей с остальным домом, либо отдельно. Сейчас в этих дворах хранится сельхозинвентарь и зимует скот. А в былые времена прошлого двор использовался для каких-то других целей, и его обладатель был выше статусом, чем все простые, имевшую обычную избу. А что там могло быть такого? И причём здесь столбы? Так может, столбы ставили на дворы для каких-то технических нужд, и дом превращался в смарт-дом (по европейскому определению), и обеспечивал себя сам водой, светом и теплом? Картина начинает проясняться.

**English:**

A courtyard at that time, and in many places now, is called an extension to a wooden house, made with a common roof with the rest of the house, or separately. Now these yards store agricultural equipment and winter livestock. And in the old days of the past, the yard was used for some other purposes, and its owner was of a higher status than all the simple ones who had an ordinary hut. And what could be wrong with that? And what do the pillars have to do with it? So maybe the poles were put in the yards for some technical needs, and the house turned into a smart home (according to the European definition), and provided itself with water, light and heat? The picture is beginning to clear up.

<!-- Local
[![](Forward to the Past_files/42-750x604.jpg#clickable)](Forward to the Past_files/42.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/42-750x604.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/42.jpg)

**Russian:**

И вот она перед вами, улица столбовых дворян, в данном случае в Благовещенске образца 1870 года. Простым смертным такие архитектурные изыски не дозволялись или были не по карману. Для них были другие цели и задачи. А для титулованных особ были даже специальные обращения, типа "ваше сиятельство" или "ваше благородие". Как ни странно оба они говорят о том, что их обладателям дозволялось иметь технические достижения уничтоженной цивилизации - свет и нечто, что приносило в дом душевный покой и оздоровление. Всё примитивно и старо как мир. У каждого правящего слоя была своя степень свободы и свой уровень дозволенности, ну и свои обязанности в этой не очень сложной иерархии.

**English:**

And here it is before you, the street of stolbovyh noblemen, in this case in Blagoveshchensk sample of 1870. Mere mortals were not allowed or could not afford such architectural delights. They had other goals and objectives. And for titled persons, there were even special addresses, such as" your Excellency "or"your honor". Oddly enough, both of them say that their owners were allowed to have the technical achievements of a destroyed civilization-light and something that brought peace of mind and health to the house. Everything is primitive and old as the world. Each ruling stratum had its own degree of freedom and its own level of permissiveness, as well as its own responsibilities in this not very complex hierarchy.

**Russian:**

Как вам звучит такая фраза - "колонизаторы, завоевав страну, посадили в ней правительство коллаборационистов, а коренное население согнали в резервации, при этом разрушали города и подавляли очаги восстаний до полной победы". Это вполне обыденная ситуация для политики 19 века, например, в какой-нибудь латиноамериканской стране. Ничего удивительного, всё так и было. А если перефразировать её несколько иначе? К примеру, "под эгидой Екатерины Великой возродившееся дворянство ввело крепостное право, которое ограничило внутреннюю миграцию в стране, попытки побега на украинные земли карались смертью, а многочисленные крестьянские восстания жестоко подавлялись. Города были перестроены на новый лад". Это в общем не по учебнику и в произвольной форме, без претензии на истину, но смысл фразы почти одинаков с предыдущей. Но только фраза уже для другой страны и выглядит изящнее.

**English:**

How does this phrase sound to you - "the colonialists, having conquered the country, put a government of collaborators in it, and the indigenous population was driven to reservations, while destroying cities and suppressing hotbeds of revolts to complete victory". This is quite an ordinary situation for the politics of the 19th century, for example, in some Latin American country. No wonder it was like that. And if you rephrase it a little differently? For example, " under the auspices of Catherine the great, the revived nobility introduced serfdom, which restricted internal migration in the country, attempts to escape to their native lands were punished with death, and numerous peasant uprisings were brutally suppressed. Cities were rebuilt in a new way." This is in General not according to the textbook and in any form, without a claim to the truth, but the meaning of the phrase is almost the same as the previous one. But only the phrase is already for another country and looks more elegant.

<!-- Local
[![](Forward to the Past_files/43-750x501.jpg#clickable)](Forward to the Past_files/43.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/43-750x501.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/43.jpg)

**Russian:**

А это и есть один из случаев подавления таких очагов сопротивления, известный нам ныне как оборона Севастополя.

**English:**

And this is one of the cases of suppression of such centers of resistance, known to us now as the defense of Sevastopol.

**Russian:**

И откуда-то вдруг появилось очень много писателей, восхвалявших особый путь развития этой территории, который по факту банально прикрывал особый вид колонизации, очень удачно проработанный под ментальность населяющих эту территорию народных масс.

**English:**

And from somewhere suddenly there were a lot of writers who praised the special way of development of this territory, which in fact simply disguised a special type of colonization, very well developed under the mentality of the people who inhabit this territory.

**Russian:**

Говорить на эту тему можно много, поэтому вернёмся. Как же так получилось, что на территории огромной Империи за очень небольшой промежуток времени произошла полная перезагрузка сознания всего населения? Здесь, наверное, из прошлого надо уже перенестись в настоящее, или в начало 20 века. И процитировать немного с сайта armycarus. Всегда считал, что там надо читать и делить на восемь (да простит меня автор за такую откровенность), но здесь не та ситуация. Речь пойдёт о тех самых событиях октября 1917 года в Санкт-Петербурге (орфография авторская).

**English:**

You can talk about this topic a lot, so let's go back. How did it happen that on the territory of a huge Empire in a very short period of time there was a complete reset of the consciousness of the entire population? Here, perhaps, from the past we should already move to the present, or to the beginning of the 20th century. And to quote a little from the armycarus website. I always thought that there should be read and divided by eight (may the author forgive me for such frankness), but this is not the situation. We will talk about the very events of October 1917 in St. Petersburg (the author's spelling).

**Russian:**


> Все улицы и панели были завалены трупами хорошо одетых мужчин. Рядом с некоторыми из них валялись их бумажники. На трупах людей были хорошие часы, перстни, их даже не грабили. Трупов было очень много, ими было завалено всё, весь Город. Весь Город был завален трупами и залит лужами крови. Трупы стали подванивать и по всему Городу стоял удушающий трупный смрад. Собаки, которых никто не кормил, ели эти трупы. Чтобы избавиться от трупов, большевики согнали гопников и приказали сбросать эти трупы в Неву и каналы. Трупов было столько, что они шли по реке ещё полтора года.

**English:**


> All the streets and streets were littered with the corpses of well-dressed men. Some of them had their wallets next to them. The corpses of people had good watches, rings, they were not even robbed. There were a lot of corpses, they were littered with everything, the whole City. The entire City was littered with corpses and pools of blood. The corpses began to stink, and the stench of corpses was suffocating all over the City. Dogs that were not fed by anyone ate these corpses. To get rid of the corpses, the Bolsheviks rounded up the gopniks and ordered them to be dumped into the Neva river and the canals. There were so many corpses that they walked along the river for another year and a half.

**Russian:**


> ..............................

> Потом, через много лет, пьяный гопник в кабаке рассказал её про то, как большевики их согнали в команды и заставили их прочесать весь Город. Обыскивали все квартиры, все чердаки и даже подвалы, искали выживших и спрятавшихся людей. Кого находили, убивали на месте. Все трупы большевики заставляли переворачивать. И если находили кого-то раненного, но ещё живого, большевики приказывали гопникам убить этих людей. Добивали раненных. После этого рейда гопники страшно боялись большевиков и разбегались при одном их появлении, как только их видели.

**English:**


> Then, many years later, a drunken Gopnik in a tavern told her about how the Bolsheviks drove them into teams and forced them to comb the entire City. They searched all apartments, all attics and even basements, looking for survivors and hidden people. Those who were found were killed on the spot. The Bolsheviks forced all the corpses to be turned over. And if they found someone wounded, but still alive, the Bolsheviks ordered the Gopnik to kill these people. They finished off the wounded. After this RAID, the gopniks were terribly afraid of the Bolsheviks and fled at their very appearance, as soon as they were seen.

**Russian:**


> ...............................

> Сразу же после Погрома на Петроград налетели крестьяне с телегами из окрестных деревень: грабить опустевшие дома. Красноармейцы прогоняли крестьян, считая весь Петроград своей добычей. Мёртвый Петроград кинулись грабить все подряд, и между группировками грабителей часто были перестрелки и настоящие бои. По условиям брестской капитуляции 04 ноября 1918 года в Петроград должны были войти немцы. А если в Петроград войдут немцы, то они вырежут всех. Так говорили в Ленинграде. И Ленин с Троцким драпанули в Москву. По этой причине Москва и стала Столицей СССР. Столицей России Москва никогда не была. В СССР смеялись по этому поводу: «Москва – столица Московской области!». Ещё рассказ одного их тех, кто был во время революции в Петрограде, потом сбежал, потом опять вернулся. Когда они приехали в Петроград в 1921 году, то Петроград стоял Мёртвым, Город без Людей. Людей было очень мало. Его деда вызвали знакомые, которые работали в ЧК и он попал на Гороховую. На Гороховой он увидел внутри двора кучи документов, которые жгли чекисты. Это были документы убитых петроградцев. Этими документами убитых петроградцев был завален весь Петроград. Поскольку их никто не отмечал в Книге Мёртвых, как умерших, то все они юридически считались живыми. И этим воспользовались уголовники, чтобы уйти от возмездия за бандитизм во время революции. Они брали себе чистые документы убитых петроградцев и найти этих бандитов потом было очень трудно. По документам это уже были другие люди.

**English:**


> Immediately after the Pogrom, Petrograd was attacked by peasants with carts from the surrounding villages: to Rob empty houses. The red army chased away the peasants, considering the whole of Petrograd their prey. Dead Petrograd rushed to Rob everything in a row, and between groups of robbers there were often skirmishes and real fights. Under the terms of the Brest capitulation on November 04, 1918, the Germans were to enter Petrograd. And if the Germans enter Petrograd, they will cut everyone out. So they said in Leningrad. And Lenin and Trotsky scurried off to Moscow. For this reason, Moscow became the capital of the USSR. Moscow has never been the capital of Russia. In the USSR, they laughed about this: "Moscow is the capital of the Moscow region!". Another story of one of those who was in Petrograd during the revolution, then ran away, then came back again. When they arrived in Petrograd in 1921, Petrograd was Dead, a City without People. There were very few people. His grandfather was called by friends who worked in the Cheka and he got to Gorokhovaya. On Gorokhovaya street, he saw piles of documents inside the courtyard that were being burned by the security officers. These were the documents of the murdered Petrograders. These documents dead petrograda was overwhelmed with the entire Petrograd. Since no one marked them as dead in the Book of the Dead, they were all legally considered alive. And this was used by criminals to escape retribution for banditry during the revolution. They took for themselves clean documents of the killed petrogradites and then it was very difficult to find these bandits. According to the documents, these were already other people.

**Russian:**


> ...................................

> Тогда многие бандиты нахватали документов убитых профессоров, князей, графьёв и прочих известных людей, в том числе и тех, кто занимался творчеством и был известен в Европе и России. Скандал начала белоэмиграция в Европе, когда в Петрограде выползли «подснежники». Большевики захотели показать, что научная и прочая элита перешла на их сторону. И как только в Петрограде и Москве нарисовались эти красные профессора, в Европе тут же разразился скандал такой силы, что Сталину пришлось применять меры по отношению к этим красным петроградским профессорам. На Западе доказали, что настоящие были убиты во время Погрома Петрограда, а их документами завладели эти Оборотни, которые сейчас выступают от имени убитых профессоров, художников и прочих. И вот тогда в Петрограде начались «чистки». Из Москвы и всей России, через НКВД приезжали люди, которые знали этих бандитов в лицо, либо знали убитых профессоров. Они ходили по Ленинграду, посещали мероприятия и отлавливали этих Оборотней. Этих красных петроградских профессоров и прочую элиту, замешанную в революции в Петрограде, сталинские соколы тогда отстреливали, как бешеных собак.

**English:**


> Then many bandits picked up documents of the killed professors, princes, counts and other famous people, including those who were engaged in creative work and were known in Europe and Russia. The white emigration in Europe started the scandal when "snowdrops" crawled out in Petrograd. The Bolsheviks wanted to show that the scientific and other elite had come over to their side. And as soon as these red professors appeared in Petrograd and Moscow, a scandal of such force broke out in Europe that Stalin had to take measures against these red Petrograd professors. In the West, they proved that the real ones were killed during the Pogrom of Petrograd, and their documents were taken over by these Werewolves, who now act on behalf of the murdered professors, artists, and others. And that's when the "purges" began in Petrograd. From Moscow and all over Russia, through the NKVD, people came who knew these bandits by sight, or knew the murdered professors. They walked around Leningrad, attended events and caught these Werewolves. These red Petrograd professors and other elites involved in the revolution in Petrograd were then shot by Stalin's falcons like mad dogs.

**Russian:**

М-да... Такой перезагрузке позавидовал бы сам Барак Обама. Полагаю, теперь всем ясно, чем вызвано стирание народной памяти о почти вчерашних событиях. И как вы думаете, такое на просторах завоёванной Империи происходило только в 1917 году в Санкт-Петербурге и затем в Москве?

**English:**

Hmm... yes... Such a reset would be the envy of Barack Obama himself. I think it is now clear to everyone what caused the Erasure of people's memory of almost yesterday's events. And how do you think this happened in the vast expanses of the conquered Empire only in 1917 in St. Petersburg and then in Moscow?

**Russian:**

Ещё в советское время, при изучении истории родного края в школе, довелось прочитать буклет для туристов по истории древнего города Мурома. Спустя годы, уже в наши дни, цитаты из того буклета слово в слово перекочевали в современные источники. И была там одна странная фраза, которая почему-то запомнилась. Вот она.

**English:**

Back in Soviet times, when studying the history of the native land at school, I had a chance to read a booklet for tourists on the history of the ancient city of Murom. Years later, in our days, quotes from that booklet word for word migrated to modern sources. And there was one strange phrase that I remember for some reason. Here it is.

**Russian:**


> В конце XVIII века после визита в город императрицы Екатерины II город, как и многие города того времени, полностью перестроили, радиально-кольцевая планировка была заменена прямоугольной сеткой кварталов.

**English:**


> At the end of the XVIII century, after the visit of Empress Catherine II to the city, the city, like many cities of that time, was completely rebuilt, the radial-ring layout was replaced by a rectangular grid of blocks.

**Russian:**

Как вы думаете, возможна такая реновация без выселения жителей? А как было проще всего переселить жителей? Ответ напрашивается сам, и тем более упоминаются другие города. Всё более чем просто - большевики были совсем не изобретателями подобных перезагрузок, ещё до них эта тактика была успешно опробована и им она была хорошо известна. И о масштабе такой реновации сейчас можно только догадываться. Кто уцелел, жил в деревнях, и понадобилось совсем немного лет, чтобы народная память была полностью стёрта.

**English:**

Do you think such renovation is possible without eviction of residents? What was the easiest way to relocate residents? The answer suggests itself, and even more so other cities are mentioned. Everything is more than simple - the Bolsheviks were not at all the inventors of such reboots, even before them this tactic was successfully tested and they were well aware of it. And the scale of such a renovation can only be guessed at now. Those who survived lived in villages, and it took only a few years for the people's memory to be completely erased.

<!-- Local
[![](Forward to the Past_files/Urusov-and-Group-Photo-of-Camp-Krasnoe-Selo-750x234.jpg#clickable)](Forward to the Past_files/Urusov-and-Group-Photo-of-Camp-Krasnoe-Selo.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Urusov-and-Group-Photo-of-Camp-Krasnoe-Selo-750x234.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Urusov-and-Group-Photo-of-Camp-Krasnoe-Selo.jpg)

**Russian:**

Где же вы, господа-демократы минувшего века, воспетые почти забытым ныне очень талантливым исполнителем Игорем Тальковым? Неужели уехали в Сидней, прочитав переписку Энгельса с Каутским?

**English:**

Where are you, gentlemen-Democrats of the last century, sung by the now almost forgotten very talented performer Igor Talkov? Did they go to Sydney after reading Engels ' correspondence with Kautsky?

**Russian:**

Дальнейшая история в общем более-менее известна, несмотря на то, что ряд подтасовок осуществлялся и здесь. Забытые технологии Империи в начале 20 века начали стихийно и неуправляемо возрождаться, что ставило существование Интернационала под угрозу. По стечению обстоятельств в Интернационале начался раздрай, или оппортунизм, как его там называли, что привело к огромным разрушительным войнам. Их итогом в начале 20 века стало переформатирование территорий бывшей Империи, ну а дальше последовали те события, о которых написано выше. Под видом насаждения свободы и братства остатки достижений прошлого были уничтожены окончательно. Эти события всем хорошо знакомы и повторять их нет смысла. Хочется ещё добавить эпизод про сталинскую архитектуру, Берию и стратегический запас красной ртути на случай войны, которую они ждали и не дождались, но об этом как-нибудь потом.

**English:**

Further history is generally more or less known, despite the fact that a number of frauds were carried out here. Forgotten technologies of the Empire at the beginning of the 20th century began to spontaneously and uncontrollably revive, which put the existence of the 'International' at risk. By coincidence, the international began to be torn apart, or opportunism, as it was called there, which led to huge destructive wars. Their result at the beginning of the 20th century was the reformatting of the territories of the former Empire, and then followed the events described above. Under the guise of instilling freedom and fraternity, the remnants of the achievements of the past were completely destroyed. These events are well known to everyone and there is no point in repeating them. I would also like to add an episode about Stalinist architecture, Beria and a strategic reserve of red mercury in case of war, which they were waiting for and did not wait for, but about this later.

**Russian:**

Ну конечно же, не в это прошлое надо идти, а это прошлое надо хорошо осмыслить и забыть. А мы снова возвратимся к тому прошлому, которое рассмотрено выше на чудом сохранившихся фотографиях.

**English:**

Of course, it is not the past that we should go to, but the past that we should think about and forget. And we will again return to the past, which is considered above in the miraculously preserved photos.

**Russian:**

Если подвести итог и снова применить аллегории, то процесс уничтожения технологий прошлого выглядел примерно так. Некто взял ящерицу и отрубил ей хвост, но при этом внушил, что её хвост отрасти сам не может, и прицепил ей платный протез, не дающий отрастать этому хвосту. С течением лет этот протез модернизировался и дошёл уже до нанотехнологичного исполнения, но он так и остался платным протезом. А что будет, если его снять?

**English:**

If we sum up and apply allegories again, the process of destroying the technologies of the past looked something like this. Someone took a lizard and cut off its tail, but at the same time suggested that its tail could not grow itself, and attached a paid prosthesis to it, which does not allow this tail to grow. Over the years, this prosthesis has been modernized and has already reached nanotechnological performance, but it has remained a paid prosthesis. And what will happen if you remove it?

**Russian:**

Ну и наверное, хватит писать этот грустный историко-фантастический рассказ. Дадим этому хвосту ящерицы немного подрасти.

**English:**

Well, probably enough to write this sad historical and fantastic story. Let's let this lizard's tail grow a little.

<!-- Local
[![](Forward to the Past_files/222-1.jpg#clickable)](Forward to the Past_files/222-1.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/222-1.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/222-1.jpg)

**Russian:**

Как вы поняли, это модель той самой конструкции, которая вращала винты, тянула паровозы и делала много других полезных вещей. Она сама, а равно как её секрет, находятся за пределами Российской Федерации и к автору этих строк уже (или ещё?) никакого отношения не имеет. Это не хорошо и не плохо, это неизбежно.

**English:**

As you understand, this is a model of the very structure that turned the screws, pulled the locomotives and did many other useful things. She herself, as well as her secret, are located outside the Russian Federation and to the author of these lines already (or else?) it has nothing to do with it. This is neither good nor bad, it is inevitable.

**Russian:**

Стоя под ней, невольно ощущал себя героем забытого старого польского фильма "Секс-миссия" (не подумайте чего), который в советском прокате шёл под названием "Новые Амазонки". Кто не смотрел или не помнит, по одной из сюжетных линий героиням этого фильма много лет внушали, что они могут жить только на космической базе, вне которой жизнь не возможна. И в один прекрасный момент они вышли за территорию базы и пошли по планетному пространству, и на удивление вид безжизненного пространства оказался декорацией, сломав которую, они вошли в неведомый ранее мир. Обычный мир, где есть воздух, цветут растения и есть много чего другого. В то время этим фильмом был дан посыл обществу, что с коммунизмом в Европе уже покончено и решение принято, остальное было только вопросом недалёкого времени. Кто мог, тот это понял.

**English:**

Standing under it, I could not help feeling like the hero of a forgotten old Polish film "Sex Mission" (don't think about it), which in the Soviet box office was called "New Amazons". Who did not watch or do not remember, according to one of the storylines of this film, for many years the heroines were taught that they can only live on a space base, outside of which life is not possible. And at one point, they went out of the base and went through the planetary space, and surprisingly the view of lifeless space turned out to be a decoration, breaking which, they entered a previously unknown world. An ordinary world where there is air, plants are blooming and there is a lot of other things. At that time, this film sent a message to society that communism in Europe was already over and the decision was made, the rest was only a matter of time. Those who could, understood it.

**Russian:**

Некоторыми итогами, в общем, уже могу поделиться для любителей точных и естественных наук:

**English:**

Some of the results, in general, I can already share for fans of exact and natural Sciences:

**Russian:**


> Данная конструкция и обычный силовой трансформатор суть одно и то же, только отличается конструкция от трансформатора применением "шумерской" геометрии. В трансформаторе энергию во вторичной обмотке создаёт рукотворный дисбаланс некой субстанции, а в конструкции этот дисбаланс создаёт движение той же самой субстанции, которой теоретически вроде бы не существует. Если бы трансформатор имел хоть малую часть возможности осуществлять этот управляемый самодисбаланс, он тоже наверняка стал бы жертвой "научно-технической революции" и мы бы тоже долго думали, глядя на его фото. Но есть два нюанса. Первый - в том принципе действия трансформатора, который нам преподают в школе, правды (не побоюсь этих цифр) 5-10%. Второй - "шумерская" геометрия взята в кавычки от того, что к тому самому народу отношение она вряд ли имеет. Эта геометрия широко использовалась вплоть до начала 20 века в архитектуре. Её мы используем до сих пор, например циферблат часов. Взгляните на него и на эту конструкцию сверху. Попробуйте понять, почему циферблат разделён на 4 сектора, а каждый из секторов на 3 равные части. Если догадаетесь, то гарантию даю, будете очень сильно удивлены, почему не додумались до этого раньше.

**English:**


> This design and a conventional power transformer are one and the same, but the design differs from the transformer by using "Sumerian" geometry. In the transformer, the energy in the secondary winding is created by a man-made imbalance of a certain substance, and in the design this imbalance creates the movement of the same substance, which theoretically does not seem to exist. If the transformer had at least a small part of the ability to carry out this controlled self-balance, it would also probably become a victim of the" scientific and technological revolution " and we would also think for a long time, looking at its photo. But there are two nuances. The first is that the principle of operation of the transformer, which we are taught at school, is true (I'm not afraid of these figures) 5-10%. The second - "Sumerian" geometry is taken in quotation marks from the fact that it is unlikely to be related to that very people. This geometry was widely used until the early 20th century in architecture. We still use it, for example, the clock face. Take a look at it and this structure from above. Try to understand why the dial is divided into 4 sectors, and each of the sectors is divided into 3 equal parts. If you guess, I guarantee you will be very surprised why you didn't think of this before.

**Russian:**


> Волны Шумана действительно существуют, и они строго упорядочены. Их частоту сейчас выдают в режиме реального времени многие информационные ресурсы. Но их реальное расположение в пространстве не знает никто. Не потому, что эта информация засекречена, а потому, что методика рисования по пространству амплитуд этих волн ("роза ветров", вспоминайте) действительно утрачена. Тот, кто сможет реанимировать любой старинный измерительный прибор из музея, который сможет показать "розу ветров" в настоящее время, добьётся в этой жизни очень многого.

**English:**


> Schumann waves do exist, and they are strictly ordered. Their frequency is now given out in real time by many information resources. But no one knows their actual location in space. Not because this information is classified, but because the technique of drawing the amplitudes of these waves in space ("wind rose", remember) is really lost. Anyone who can reanimate any old measuring device from the Museum, which can show the "wind rose" at the present time, will achieve a lot in this life.

**Russian:**


> Рецепты красной ртути, которые гуляют сейчас по интернету, есть фейк. То, что это советское изобретение, и что им пугали в 90-х годах весь мир, тоже фейк. Спешу всех успокоить, лично я в руках её не держал (честно) и могу только догадываться, что это такое.

**English:**


> Recipes for red mercury, which are now walking on the Internet, there is a fake. The fact that this is a Soviet invention, and that they scared the whole world in the 90s, is also fake. I hasten to reassure everyone, I personally did not hold it in my hands (honestly) and I can only guess what it is.

**Russian:**

Больше ничего сказать не могу, и не пытайте. Поверьте, на повестке стоят задачи уровня продвинутого студента политеха 3 курса.

**English:**

I can't say any more, and don't try. Believe me, on the agenda are the tasks of an advanced 3rd-year student of the Polytechnic University.

**Russian:**

Ну и в общем можно констатировать, что начало похода в прошлое осуществилось. Как долго продлится этот путь и состоится ли он вообще, известно одному Всевышнему. А пока эта дорога в прошлое совершенно свободна.

**English:**

Well, in general, we can say that the beginning of the campaign in the past was realized. How long this path will last and whether it will take place at all, is known only to the Almighty. In the meantime, this road to the past is completely free.

<!-- Local
[![](Forward to the Past_files/Bez-nazvanija-1-750x360.jpg#clickable)](Forward to the Past_files/Bez-nazvanija-1.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Bez-nazvanija-1-750x360.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Bez-nazvanija-1.jpg)

**Russian:**

Где-то через несколько лет (предполагаю), на этой дороге начнётся давка и хождение по головам из желающих попасть в это прошлое. И начнут игнорироваться все глупые законы, ограничивающие мир и счастье во всём мире. И кто будет убеждать, что это всё враньё, будет только создавать конкурентные преимущества тем самым бегущим в прошлое. И никто не захочет делать скачок в непонятное будущее, как некоторые страны.

**English:**

Somewhere in a few years (I assume), on this road will start a stampede and walking on the heads of those who want to get into this past. And all the stupid laws that restrict world peace and happiness will be ignored. And who will convince that this is all a lie, will only create competitive advantages for those who are running into the past. And no one wants to jump into an obscure future like some countries.

**Russian:**

Ну а мы запасаемся попкорном и ждём, кто первый побежит в прошлое сколько провисит эта статья (шутка). И прошу не забывать, что писатель - это человек гротеска, ему свойственно сгустить краски, прифантазировать, сместить акценты и т.п., совершенно не было цели кого-то оскорблять или агитировать кого-то делать необдуманные противоправные поступки.

**English:**

Well, we stock up on popcorn and wait for who will be the first to run into the past how much this article will hang (joke). And please do not forget that the writer is a man of the grotesque, he tends to thicken the colors, fantasize, shift the emphasis, etc., there was absolutely no goal to offend someone or agitate someone to do rash illegal actions.

<!-- Local
[![](Forward to the Past_files/Trockij-750x650.jpg#clickable)](Forward to the Past_files/Trockij.jpg)
-->

[![](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Trockij-750x650.jpg#clickable)](https://m2.tart-aria.info/wp-content/uploads/2019/12/tech_dancer/Trockij.jpg)

P.S.
**Russian:**


> И всё же я не уверен, что эта статья провисит долго, поэтому она синхронно может выйти на нескольких каналах, в т.ч. зарубежных. Прошу не удивляться. Спасибо тем, кто проявлял беспокойство по поводу отсутствия меня в информационном поле. В связи с переходом задач в практическую плоскость времени на статьи нет, писать буду по мере возможности.

**English:**

PS:

> Still, I am not sure that this article will hang for a long time, so it can be published simultaneously on several channels, including foreign ones. Please don't be surprised. Thank you to those who expressed concern about my absence from the information field. Due to the transition of tasks to the practical plane, there is no time for articles, I will write as much as possible.

**Russian:**


> Всех лиц, заинтересованных в продвижении данного проекта на практике (подчёркиваю) , прошу писать на tainproject@gmail.com. Русский, English, Deutsch. Особенно технических специалистов, что-то знающих о работе таких установок, или просто технических специалистов по физике и химии, считающих себя уникальными и не востребованными.

**English:**


> All persons interested in promoting this project in practice (I emphasize) , please write to tainproject@gmail.com. Russian, English, Deutsch. Especially technical specialists who know something about the operation of such installations, or just technical specialists in physics and chemistry who consider themselves unique and not in demand.

**Russian:**

При использовании материалов статьи активная ссылка на tart-aria.info с указанием автора обязательна.
	Text.ru - 100.00%

**English:**

When using the materials of the article, the active link to tart-aria.info with an indication of the author is required.
Text.ru - 100.00%

**Russian:**

Если вы нашли ошибку, пожалуйста, выделите фрагмент текста и нажмите Ctrl+Enter.

**English:**

If you find an error, please select a text fragment and press Ctrl+Enter.

© All rights reserved. The original author retains ownership and rights.
