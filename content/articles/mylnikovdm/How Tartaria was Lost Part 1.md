title: How Tartaria Died Part 1 - Without a Trace?
date: 2014-08-21
modified: Thu 14 Jan 2021 07:22:35 GMT
category: mylnikovdm
tags: Tartaria; thermonuclear war; catastrophe
slug: 
authors: Dmitry Mylnikov
summary: Tartaria... How did such a huge state, with a huge population, with many cities, suddenly disappear without a trace?
status: published
originally: Как погибла Тартария, ч1.zip
local: How Tartaria was Lost Part 1_files/

### Translated from:

[https://mylnikovdm.livejournal.com/603.html](https://mylnikovdm.livejournal.com/603.html)

<!-- Local
![](How Tartaria was Lost Part 1_files/352_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/352/352_original.jpg#resized)

**Russian:**

О том, что до начала 19 века на территории современной Сибири существовало
огромное государство "Тартария" сегодня написана масса статей и снято несколько
документальных фильмов, в том числе опубликованных на портале ["Крамола"](http://www.kramola.info/):

[http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty)

**English:**

The fact that before the beginning of the 19th century in the territory of modern Siberia there was a huge state of "Tartary" today a lot of articles have been written and several documentaries have been made, including those published on the portal ["Sedition"](http://www.kramola.info/):

**Russian:**

["Великая Тартария, только факты."](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty)

["Великая Тартария - только факты. "Римская" Империя"](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty-rimskaja-imperija) - 

["Великая Тартария - только факты. Грифон."](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty-grifon)

["Флаг и герб Тартарии. Части 1"](http://www.kramola.info/vesti/letopisi-proshlogo/flag-i-gerb-tartarii)

["Флаг и герб Тартарии. Части 2"](http://www.kramola.info/vesti/letopisi-proshlogo/flag-i-gerb-tartarii-chast-2)

**English:**

["Great Tartary, just the facts."](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty)

["Great Tartary-only facts. "Roman empire"](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty-rimskaja-imperija-)

["Great Tartary-only facts. Gryphon."](http://www.kramola.info/video/zamalchivaemaja-istorija/velikaja-tartarija-tolko-fakty-grifon-) 

["Tartarian Flag and Coat of Arms Tartaries. Part 1"](http://www.kramola.info/vesti/letopisi-proshlogo/flag-i-gerb-tartarii)

["Tartarian Flag and Coat of Arms Tartaries. Part  2"](http://www.kramola.info/vesti/letopisi-proshlogo/flag-i-gerb-tartarii-chast-2)

<!-- Local
![](How Tartaria was Lost Part 1_files/767_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/767/767_original.jpg#resized)

**Russian:**

Я не буду пересказывать все факты и доказательства существования Тартарии, это
займёт слишком много места. Желающие могут ознакомится с ними по приведённым выше
ссылкам. На мой взгляд, они достаточно убедительные и исчерпывающие. Вопрос в другом.
Каким образом такое огромное государство, с огромным населением, со множеством городов,
вдруг исчезло без следа? Почему мы не находим останков городов, объектов экономической
1инфраструктуры, которые обязаны быть в любом большом и развитом государстве. Если
жило большое количество людей, они должны были торговать, перемещаться между
городами. А это значит, что должны быть дороги и мосты, множество посёлков вдоль них,
которые обслуживают караваны и т. п.

**English:**

I will not recount all the facts and evidence of the existence of Tartary, it will take up too much space. Those interested can read them at the links above. In my opinion, they are quite convincing and comprehensive. Question in other. How did such a huge state, with a huge population, with many cities, suddenly disappear without a trace? Why don't we find the remains of cities, objects of economic activity 1 infrastructure that must be in any large and developed state. If a large number of people lived, they had to trade, move between cities. This means that there must be roads and bridges, many villages along them that serve caravans, and so on.

**Russian:**

Отсутствие большого количества материальных следов на территории Сибири
является одним из самых весомых аргументов в устах сторонников официальной версии
истории, согласно которой "Тартария" всего лишь миф, который старые картографы нанесли
на карту. Если бы в Сибири существовало огромное государство с многомиллионным
населением, то там должно быть множество городов, поселений, дорог их связывающих и
прочих следов жизнедеятельности. Но по факту мы этих следов в Сибири в должном
количестве, по их мнению, не наблюдаем.

**English:**

Absence of a large number of material traces on the territory of Siberia it is one of the most powerful arguments in the mouth of supporters of the official version of history, according to which "Tartary" is just a myth that the old cartographers put on the map. If there were a huge state with a population of millions in Siberia, there should be a lot of cities, settlements, roads connecting them and other traces of life. But in fact, we do not observe these traces in Siberia to the proper extent, in their opinion.

**Russian:**

В одной из статей, также опубликованной на портале "Крамола", автор пытается дать
объяснение куда могла исчезнуть Тартария:
http://www.kramola.info/vesti/letopisi-proshlogo/gibel-tartarii - "Гибель Тартарии"
Кратко, по мнению автора Тартария была уничтожена массированной ядерной
бомбардировкой, которая выжгла леса в Сибири и на Урале, а также, якобы, оставила
множество воронок от ядерных взрывов.

**English:**

In one of the articles, also published on the portal "Sedition", the author tries to explain where Tartary could have disappeared:

["The Death of Tartary"](http://www.kramola.info/vesti/letopisi-proshlogo/gibel-tartarii)

In short, according to the author, Tartary was destroyed by a massive nuclear
bombardment that burned forests in Siberia and the Urals, and allegedly left
lots of craters from nuclear explosions.

**Russian:**

Сразу скажу, что я не отрицаю того, что примерно 200 лет назад были произведены
ядерные взрывы. После прочтения данной статьи, а также знакомства с видеофильмами
"Искажение истории" с Алексеем Кунгуровым, не смотря на первоначальное скептическое
отношение к этой версии, мне самому с друзьями удалось найти несколько следов от ядерных
взрывов, в том числе очень хорошо читаемую воронку в 40 км. от Челябинска, где я живу, в
районе города Еманжелинск. Диаметр этой воронки 13 км.

**English:**

I will say at once that I do not deny that nuclear explosions occurred about 200 years ago. After reading this article, as well as getting acquainted with the *"Distortion of History"* videos by Alexey Kungurov, despite the initial skeptical attitude to this version, I myself and my friends managed to find several traces of nuclear explosions, including a very easy to see crater 40 km away from Chelyabinsk, where I live, near the city of Yemanzhelinsk. The diameter of this crater is 13 km.

<!-- Local
![](How Tartaria was Lost Part 1_files/983_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/983/983_original.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 1_files/1108_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1108/1108_original.jpg#resized)

**Russian:**

Но у этой версии есть серьёзная проблема. Во-первых, она не объясняет исчезновение
всех следов культурной и экономической деятельности жителей огромной империи. Во-
вторых, чтобы произвести подобную тотальную зачистку территории необходимо было
подорвать очень много ядерных зарядов. Фактически необходимо было покрыть всю
территорию Сибири равномерной сеткой взрывов, с шагом порядка 100-150 км, а может и
2меньше. Мало того, изучая старые карты я обнаружил, что на некоторых из них на
территории Сибири изображено очень много городов, особенно в области между реками
Иртыш и Обь. То есть, в то время тут была достаточно высокая плотность населения. А это
значит, что без подобной плотной бомбардировки неизбежно бы выжило достаточно много
людей, а также осталось много мелких и средних населённых пунктов. На деле же
выясняется, что большая часть населённых пунктов на территории той же Челябинской
области были основаны как раз в первой половине 19 века, причём в промежутке от 1825 до
1850 года. Мало того есть версия, что некоторые из городов и посёлков, которые якобы
основаны в 18 или даже в 17 веках и упоминаются в различных документах, были построены
заново на месте когда-то существовавших поселений или рядом с ними (подробнее про эту
странность я расскажу ниже).

**English:**

But this version has a serious problem. First, it does not explain the disappearance of all traces of cultural and economic activity of the inhabitants of the vast Empire.

Secondly, to make such a total sweep of the territory, it was necessary to detonate a lot of nuclear charges. In fact, it was necessary to cover the entire territory of Siberia with a uniform grid of explosions, with a step of about 100-150 km, and maybe even 2 less. Moreover, studying old maps, I found that some of them show a lot of cities on the territory of Siberia, especially in the area between rivers The Irtysh and The Ob. 

That is, at that time there was a fairly high population density. This means that without such a heavy bombardment, quite a lot of people would inevitably have survived,as well as many small and medium-sized settlements. In fact, it turns out that most of the settlements on the territory of the same Chelyabinsk region were founded just in the first half of the 19th century, and in the period from 1825 to 1850. Moreover, there is a version that some of the cities and towns that were allegedly founded in the 18th or even in the 17th centuries and are mentioned in various documents were built anew on the site of once-existing settlements - or near them (I'll tell you more about this oddity below).

**Russian:**

Проблема в том, что в случае подобной массированной равномерной бомбардировки
мы должны наблюдать на территории Сибири как раз более-менее равномерную сетку из
воронок, но, увы, мы её там не наблюдаем. Некоторое количество воронок и других следов наблюдается в районе Урала и Поволжья (восточный берег Волги). А дальше от Урала на восток подобных следов, характерных для ядерных взрывов, не наблюдается.
Но, если внимательно посмотреть на спутниковые снимки территории Сибири, то мы
можем обнаружить там совсем другие следы!

**English:**

The problem is that in the case of such a massive uniform bombardment, we should observe a more or less uniform grid of craters on the territory of Siberia, but, alas, we do not observe it there. A certain number of craters and other traces are observed in the area of the Urals and the Volga region (the Eastern Bank of the Volga). And further from the Urals to the East, such traces characteristic of nuclear explosions are not observed.But if you look closely at satellite images of the territory of Siberia, we can find quite different traces there!

Впервые на эти необычные объекты моё внимание обратил мой тесть Карпаев
Василий Алексеевич несколько лет назад. Причём они хорошо видны как на спутниковых
снимках, так и на топографических картах, и большинству известны как "Сибирские
ленточные боры".

**English:**

For the first time my father in law Karpaev drew my attention to these unusual objects Vasily Alekseevich a few years ago. Moreover, they are clearly visible both on satellite images and on topographic maps, and most are known as "Siberian ribbon forests".

<!-- Local
![](How Tartaria was Lost Part 1_files/1108_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1108/1108_original.jpg#resized)

**Russian:**

Это несколько узких полос сосновых боров, шириной в среднем 5 километров,
которые тянутся от реки Обь по диагонали с северо-востока на юго-запад практически до
реки Иртыш. Длина самой большой линии более 240 км. По профилю это широкие
углубления, глубиной от 20 до 200 метров. Согласно официальной легенде, эти траншеи
были прорыты ледником много тысяч лет назад, после чего они заросли "реликтовыми"
борами.

**English:**

These are several narrow strips of pine forests, an average width of 5 kilometers, which stretch from the Ob river diagonally from northeast to southwest almost to the Irtysh river. The length of the largest line is more than 240 km. According to the profile, these are wide depressions, from 20 to 200 meters deep. According to official legend, these trenches were dug by the glacier many thousands of years ago, after which they were overgrown with "relic" forests.

**Russian:**

Но данное объяснение про "следы ледника" можно принять только если не
задумываться о том, что же мы на самом деле такое видим на снимках и картах. Подобные
следы не могут быть оставлены ледником. Теория ледникового происхождения подобных
образований берёт свои корни из наблюдений за последствиями движения ледников в горной
местности, в частности в Альпах. В горах, за счёт большого перепада высот, лед,
действительно, начинает течь, прорывая на своём пути траншеи и ущелья. Но то, что
аналогичные по силе воздействия и размерам следы могут образовываться на относительно
равнинной местности, где мы наблюдаем "ленточные боры", это лишь предположение. Даже
если допустить, что был мощный ледовый слой, которые "отползал" на север, то лёд должен
был стекать по существующему рельефу местности. При этом ледник никогда не будет
"сползать" строго по прямой линии, так же, как реки никогда не текут строго по прямой
линии, а огибают естественные неровности рельефа. На снимках же хорошо видно, что
следы начинаются от левого (западного) крутого берега Оби, то есть, фактически разрезают
склон перпендикулярно сложившемуся рельефу местности. При этом несколько следов идут
почти по прямой линии, да ещё и параллельно друг другу!

**English:**

But this explanation about the "glacier tracks" can only be accepted if you do not think about what we actually see on the images and maps. Such tracks cannot be left by a glacier. The theory of the glacial origin of such formations takes its roots from observations of the effects of glacier movement in mountainous areas, in particular in the Alps. In the mountains, due to the large difference in altitude, the ice really begins to flow, breaking through trenches and gorges on its way. But the fact that similar in strength and size of the impact traces can be formed on relatively the flat terrain where we observe "ribbon forests" is just a guess. Even if we assume that there was a powerful ice layer that "crawled" to the North, the ice should have flowed over the existing terrain. At the same time, the glacier will never "slide" strictly in a straight line, just as rivers never flow strictly in a straight line, but bend around natural terrain irregularities. The images also clearly show that the tracks start from the left (Western) steep Bank of the Ob river, that is, they actually cut the slope perpendicular to the existing terrain. At the same time, several traces go almost in a straight line, and even parallel to each other!

**Russian:**

Искусственными сооружениями эти следы тоже быть не могут, поскольку совершенно
непонятно кто и с какой целью мог выкопать подобные траншеи?

**English:**

These traces can't be artificial structures either, because it is completely unclear who and for what purpose could have dug such trenches?

**Russian:**

Данные следы могли оставить только упавшие из космоса на поверхность Земли
крупные объекты. Это подтверждается и тем фактом, что азимут угла наклона следов
составляет от 67 до 53 градусов, при этом следы от падения мелких объектов в районе озера
Чаны, у которых отклонение от начальной траектории при прохождении атмосферы было
меньше вследствие меньшей поперечной площади сечения, лежат в диапазоне от 67 до 61
градуса. То есть, это практически совпадает с углом наклона оси вращения Земли к
плоскости эклиптики, то есть к плоскости вращения планет и астероидов вокруг Солнца,
который составляет 66.6 градуса. Поэтому совершенно логично, что объекты, те же
астероиды, которые двигаются в плоскости эклиптики, падая на поверхность Земли
оставляют следы именно под этим углом. А вот "отступление ледника" именно под этим
углом, да ещё и не взирая на имеющийся рельеф местности, совершенно не логично.

**English:**

These traces could only be left by large objects that fell from space to the earth's surface . This is confirmed by the fact that the azimuth of the angle of inclination of the tracks is from 67 to 53 degrees, while traces of falling small objects in the area of the Lake Vats whose deviation from the initial trajectory during the passage of the atmosphere was less due to a smaller cross-sectional area, lie in the range from 67 to 61 degree's. That is, it almost coincides with the angle of inclination of The earth's rotation axis to the plane of the Ecliptic, that is, to the plane of rotation of planets and asteroids around the Sun, which is 66.6 degrees. Therefore, it is quite logical that objects, the same asteroids that move in the plane of the Ecliptic, falling on The earth's surface leave traces at this angle. But the "retreat of the glacier" at this angle, and even in spite of the existing terrain, is not logical at all.

**Russian:**

Чтобы лишний раз убедится в том, что это именно тот угол, я специально нашёл
изображение глобуса Земли, повёрнутого нужным образом. "Ленточные боры" в этом
положении оказываются расположены как раз горизонтально.

**English:**

To once again make sure that this is the right angle, I specifically found an image of the globe of the Earth, rotated the right way. The "belt burs" in this position are located just horizontally.

<!-- Local
![](How Tartaria was Lost Part 1_files/1308_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1308/1308_original.jpg#resized)

**Russian:**

Что можно сказать, глядя на эти следы. Во-первых, одновременно упало несколько
крупных тел, диаметром, судя по ширине следов, около 5 километров. На снимках хорошо
читаются два нижних длинных следа, длиной более 240 км и 220 км (No1 и No2). Расстояние
между ними в начале порядка 30 км. Дальше на северо-запад примерно в 40 км находится
ещё один след длинной порядка 145 км (No3). Ещё дальше, на расстоянии около 100 км
имеется ещё одна хорошо читаемая полоса, самая широкая из всех, шириной 7-8 км и
длинной 110 км (No4). Между полосами No3 и No4 при приближении видно множество мелких
следов, которые не образуют таких чётких полос и оставлены, скорее всего, более мелкими
фрагментами.

**English:**

What can you tell by looking at these footprints? First, several large bodies fell at the same time, with a diameter, judging by the width of the tracks, about 5 kilometers. The images clearly show the two lower long tracks, more than 240 km long and 220 km long (No1 and No2). The distance between them at the beginning is about 30 km. Further North-West, about 40 km away, is another track about 145 km long (No 3). Even further away, at a distance of about 100 km , there is another well-read strip, the widest of all, 7-8 km wide and 110 km long (No 4). Between the bands No3 and No4 when approaching you can see a lot of small traces that do not form such clear bands and are most likely left in smaller fragments.

<!-- Local
![](How Tartaria was Lost Part 1_files/1607_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1607/1607_original.jpg#resized)

**Russian:**

Но и это ещё не всё. Если от следа No4 продвигаться дальше на северо-запад, то мы увидим множество смазанных полос, которые являются следами от падения гигантского количества более "мелких" обломков. Например, они очень хорошо видны в районе озера Чаны.

**English:**

But that's not all. If we move further North-West from track No 4, we will see a lot of smeared bands, which are traces of the fall of a huge number of "smaller" debris. For example, they are very clearly visible in the area of lake
Chany.

<!-- Local
![](How Tartaria was Lost Part 1_files/1991_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/1991/1991_original.jpg#resized)

**Russian:**

При этом данные "мелкие" обломки, судя по размерам следов, на самом деле тоже
были достаточно крупными. Ширина многих "полос" от 500 метров до 1 километра, длина
десять и более километров. Для сравнения напомню, что размер Челябинского метеорита,
который упал 15 февраля 2013 года, наделал столько шума и нанёс массу ущерба,
оценивается всего в 17 метров! Количество же упавших объектов, судя по следам на снимках,
составляет многие тысячи!

**English:**

At the same time, these "small" fragments, judging by the size of the tracks, were actually quite large. The width of many "lanes" is from 500 meters to 1 kilometer, and the length is ten or more kilometers. For comparison, let me remind you that the size of the Chelyabinsk meteoritei - which fell on February 15, 2013, and made so much noise and caused a lot of damage - is estimated at only 17 meters! The number of fallen objects, judging by the traces on the images, was in the many thousands!

**Russian:**

Замерив ширину полосы, на которой видны подобные следы, от оси падения следа
No4, мы получим величину около 330 км. Общая же ширина видимой зоны поражения от
следа No1 получается более 500 км.

**English:**

By measuring the width of the lane where such tracks are visible, from the axis of the fall of the track No 4, we get a value of about 330 km. The total width of the visible damage zone from the No 1 trail is more than 500 km.

**Russian:**

Если посмотреть на то, как выглядит данное место на карте рельефа, то, во-первых,
мы увидим, что это именно углубления в террасе левого западного берега Оби, а во-вторых,
что параллельно следу No1 ниже его на юго-восток, на расстоянии 42 км и 75 км от его оси
можно увидеть ещё две "борозды", параллельные ему (на данной карте более тёмный
зелёный цвет обозначает более низкие места, как это принято на физических картах). При
этом ближний след более длинный и изрезан оврагами и руслами небольших речек, а также
руслом реки Алей, вдоль которых распахано множество полей, поэтому он не так чётко виден
6на обычных снимках, как основные следы. На карте рельефа этот след идёт от города
Рубцовск, через который протекает река Алей. При этом если до населённого пункта
Поспелиха русло реки Алей имеет достаточно сложную форму, то далее, до впадения в реку
Обь, течёт внутри узкой достаточно прямой полосы шириной 1 км, которая идёт как раз
параллельно следу No1.

**English:**

If we look at what looks like the place of the terrain on the map, the first thing we will see is that this is the deepening of the western terrace of the left bank of Ob river. Secondly, it is parallel to the track No 1 below it. To the south-east, at a distance of 42 km and 75 km from its axis, you can see two "grooves" parallel to it (on this map, as is customary on physical maps, the darker green color refers to lower areas). At the same time, the nearby trail is longer and is cut by ravines and small riverbeds. In addition to the riverbed is the river Aley, along which many fields are plowed, so it is not so clearly visible, as the main traces. Not, at least, in normal images. On the terrain map, this trail goes from the city of Rubtsovsk, through which the river Aley flows. In this case, if to the locality Pospelikha riverbed Aley has a fairly complex shape, then further, to the confluence with the Ob river flows inside a narrow, fairly straight 1 km wide strip that runs just parallel to track No 1.

<!-- Local
![](How Tartaria was Lost Part 1_files/2112_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2112/2112_original.jpg#resized)

**Russian:**

Что касается самого крайнего следа, длина которого около 75 км, то он интересен тем,
что по нему тоже протекает река под названием Порозиха, но при этом течёт она в
противоположную от реки Обь сторону! Там, где данная борозда заканчивается, Порозиха
впадает в реку Чарыш, которая снова бежит в сторону реки Обь и благополучно впадает в неё
примерно через 100 км. Если эти следы оставил ледник, как нас уверяют, то каким образом
получилось так, что одна часть ледника, в районе русла реки Алей, ползла в одну сторону, а
другая часть, в 32 км от него, ползла в совершенно противоположную?

**English:**

As for the most extreme trail, which is about 75 km long, it is interesting because in it also runs a river called the Porozikha, but it flows in the opposite direction from the Ob river! Where this furrow ends, the Porozikha flows into the Charysh river, which again runs towards the Ob river and safely flows into it after about 100 km. If these tracks were left by a glacier, as we are assured, how did it happen that one part of the glacier, in the area of the Aley riverbed, crawled in one direction, and the other part, 32 km away, crawled in the opposite direction?

**Russian:**

То, что мы имеем большое количество различных по величине объектов, которые при
этом двигаются практически по параллельным траекториям, поскольку все следы в зоне
начала следов идут под одним углом, а также очень широкой зоне их падения, можно
утверждать следующее:

1. Все эти объекты упали на поверхность земли одновременно. То есть, это не следы
от множества катастроф, происходивших в разное время.

2. Это не осколки одного большого метеорита, который раскололся на множество
фрагментов при столкновении с атмосферой Земли. Иначе бы они шли по расходящимся
траекториям от места взрыва, то есть имели форму веера, лучи которого бы сходились к точке взрыва.

**English:**

From the fact that we have a large number of objects of different sizes, which at the same time move along almost parallel trajectories, since all the tracks in the area of the beginning of the tracks go at the same angle, as well as a very wide area of their fall, we can assume the following:

1. All these objects fell to the earth's surface at the same time. In other words, these are not traces
of many catastrophes that occurred at different times.

2. These are not fragments of one large meteorite that split into many fragments when it collided with the Earth's atmosphere. Otherwise, they would have followed divergent paths from the place beneath the explosion site. That is, their trajectories would have had the shape of a fan, the rays of which would converge at the point of the explosion.

**Russian:**

Другими словами, это было столкновение Земли с крупным метеоритным полем.

**English:**

In other words, it was a collision of the Earth with a large meteor field.

**Russian:**

То, что следы очень вытянуты, а их глубина сравнительно небольшая 4% - 0.4% от
ширины следа, говорит о том, что данные объекты падали практически точно по касательной
к поверхности Земли, а их большая длина говорит о высокой скорости вхождения в
атмосферу данных объектов, которую не смогли погасить ни атмосфера Земли, ни
длительный контакт с её поверхностью.

**English:**

The fact that the traces are very extended, and their depth is relatively small - 4% - 0.4% of the width of the trail - suggests that these objects fell almost exactly on a tangent to the surface of the Earth. Their great length indicates the high speed of entry into the atmosphere of these objects. Neither the atmosphere nor prolonged contact with the surface stopped these objects.

**Russian:**

Если бы данные объекты летели под более крутым углом, то они должны были
врезаться в поверхность и образовать на ней кратеры, какие имеются на поверхности Земли и
планетах Солнечной системы и их спутниках от множества других, в том числе крупных
метеоритов. Тоже самое должно было произойти, если бы они двигались с низкой скоростью,
меньше 8 км/с. При вхождении в атмосферу продольная скорость должна была упасть, а
скорость по направлению к центру Земли, за счёт силы притяжения, вырасти, за счёт чего
угол падения должен был стать более крутым.

**English:**

If these objects were flying at a steeper angle, they would crash into the surface and form craters on it, which are present on the surface of the Earth and the planets of the Solar system and their satellites from many others, including large meteorites. The same thing would have happened if they were moving at a low speed, less than 8 km/s. When entering the atmosphere, the longitudinal velocity should have fallen, and the speed towards the center of the Earth, due to the force of attraction, should have increased, due to which the angle of incidence should have become steeper.

**Russian:**

Если бы они падали под ещё более пологим углом, то они должны были либо
пролететь по верхним слоям атмосферы и из-за высокой скорости уйти дальше в космос,
либо вообще отскочить от атмосферы так же, как отскакивают камни от поверхности воды,
когда мы пускаем "блинчики"

**English:**

If they fell at an even more gentle angle, they would either fly through the upper layers of the atmosphere and go further into space due to high speed, or even bounce off the atmosphere in the same way that rocks bounce off the surface of water when we launch "pancakes", ie skim flat pebbles on water.

**Russian:**

На основании того, что мы видим, а точнее того, чего не видим, мы можем сказать из
чего состояли эти крупные объекты. В конце следов мы не видим ни крупных каменных
глыб, ни россыпи камней, которые могли образоваться при их разрушении, да и вообще не
видим почвы с поверхности, которую каменный метеорит должен был нагрести перед собой
прорыв траншею шириной 5 км и длиной 240 км. А учитывая размер объекта в несколько
километров, в конце каждой траншеи должна была образоваться гора высотой в несколько
километров, перед которой был бы полукругом земляной вал. Аналогичные земляные валы
должны были образоваться по краям траншеи (точно так же, как у бульдозера, который
прорывает траншею отвалом). Но вместо этого мы видим, что в конце следы начинают
расширятся и образуют рисунок, характерный для дельты реки, которая впадает в море. Это
может означать только одно. Данные объекты были ледяными айсбергами и состояли в
основном из воды. При этом в начале касания поверхности они были ещё твёрдыми, чем и
объясняется тот факт, что на достаточно большой длине следов они имеют примерно
одинаковую ширину. Но от трения о поверхность и атмосферу они в конце концов
нагреваются и расплавляются, превращаясь в гигантскую волну, которая растекается уже во
все стороны, смывая всё на своём пути. Этим же, скорее всего, объясняется и то, что следы
оказались не очень глубокими и достаточно длинными, при этом имеют профиль не с
крутыми склонами, а с достаточно пологими. Если бы метеорит был каменный, то он должен
был вырыть ров с более крутыми и резкими краями. Но в нашем случае нижняя часть
айсберга от интенсивного трения с землёй плавилась быстрее, чем верхняя, и образовывала
водяной слой, который играл роль смазки, улучшающей скольжение, а также размазывавший
края, образуя более гладкий поперечный профиль.

**English:**

Based on what we see, or rather, what we don't see, we can tell what these large objects were made of. At the end of the tracks, we don't see any large boulders, or placers of stones that could have been formed during their destruction, and we don't see any soil at all from the surface, which the stone meteorite should have heated up in front of it through a trench 5 km wide and 240 km long. And, given the size of the object of several kilometers, at the end of each trench there should have formed a mountain several kilometers high, in front of which there would be a semi-circular earthen rampart. They should have formed similar earthworks along the edges of the trench (just like a bulldozer when it scrapes a trench). But, instead, we see that at the ends, the tracks begin to expand and form a pattern characteristic of a river delta where a river flows into the sea. This can only mean one thing. These objects were ice-cold and the ice was made mostly of water. At the same time, when they first contacted the Earth's surface, they were still solid, which explains the fact that they have approximately the same width over such a long track length. However, friction of the surface and atmosphere eventually heated them up and melted them, turning each one into a giant wave that spread in
all directions, washing away everything in its path. This is also likely to explain the fact that the tracks
were not very deep for their length, while they also have a cross-sectional profile not with steep slopes, but with rather gentle ones. If the meteorite was made of stone, it should have dug a ditch with steeper and sharper edges. But in our case, the intense friction with the ground melted the lower part of the "iceberg" 
faster than the top, and formed a water layer, which played the role of a lubricant that improves sliding, and also smeared the edges, forming a smoother cross-section.

**Russian:**

В конце следов No1 и No2 хорошо видно, что они начинают очень быстро расширяться
и в конечном итоге соединяются в одну сплошную широкую полосу, что также хорошо
согласуется с теорией именно ледяных метеоритов, которые в конечном итоге растаяли,
8образовав две гигантских волны, сметающих всё на своём пути подобно цунами, и
соединившихся вместе на последнем участке. Интересно также, что от метеорита, который
оставил след юго восточнее следа No1, по которому протекает река Алей, также имеется
весьма характерная зона выноса. После удара и образования волны большая её часть
перевалила через линию водораздела между реками Обь и Иртыш и ушла в последний в
районе города Семей. Видимо, судя по следам на снимках, в Иртыш в конечном итоге ушла и
вода из ледяных метеоритов, которые оставили следы No1, No2 и No3.

**English:**

At the end of tracks No 1 and No 2, it is clearly visible that they begin to expand very quickly and eventually merge into one continuous wide band, which also agrees well with the theory of icy meteorites that eventually melted, forming two giant waves that sweep away everything in their path like a tsunami, then joined together in the last section. It is also interesting that from the meteorite that left a trace south-east of track No 1, along which the river Aley flows, there is also a very characteristic zone of removal. After the impact and the formation of the wave, most of it crossed the watershed line between the Ob and Irtysh rivers and went to the last one near the city of Semey. Apparently, judging by the traces on the images, the Irtysh eventually left
water from ice meteorites, which left traces of No 1, No 2 and No 3.

**Russian:**

Я затрудняюсь до конца представить масштабы этой катастрофы, но для меня
очевидно, что в данной полосе шириной более 500 км и длиной более 250 км было
уничтожено всё, что находилось на поверхности. Волна цунами снесла все постройки, все
растения, уничтожила все живые организмы. При этом во время падения и торможения об
атмосферу и землю поверхность метеоритов должна была разогреваться до высоких
температур, а значит вода, в которую превращался лёд, должна была интенсивно
превращаться в пар. Исходя из того, что мы видим на снимках, особенно в районе озера
Чаны, плотность объектов в упавшем метеоритном поле была достаточно высокой, а это
значит, что в области падения воздух должен был быть наполнен перегретым паром, а
возможно и какими-то газами, если метеориты состояли не только из воды. Смешиваясь с
грунтом на поверхности Земли, вся эта масса вместе с паром должна была подниматься в
верхние слои атмосферы. Другими словами, у меня есть большие сомнения, что хоть кто-то
мог выжить в непосредственной зоне катастрофы, если только у него не было специально
оборудованных убежищ, способных перенести ядерный удар. А подобных убежищ, как мы
все понимаем, в начале 19-го века, когда, по моему мнению, произошла данная катастрофа,
строить ещё никто не умел.

**English:**

I find it difficult to fully imagine the scale of this disaster, but it is obvious to me that in this strip more than 500 km wide and more than 250 km long, everything that was on the surface was destroyed. The tsunami wave demolished all buildings, all plants, destroyed all living organisms. At the same time, during the fall and deceleration caused by the atmosphere and contact with the earth, the surface of the meteorites had to warm up to high temperatures. This means that the water that the ice turned into, turned into intense steam. Based on what we see in the images, especially in the area of the lake, the density of objects in the fallen meteorite field was quite high, which means that in the area of the fall, the air must have been filled with superheated steam, and possibly some gases, if the meteorites consisted of more than just water. Mixing with the ground on the surface of the Earth, all this mass, together with steam, should have risen to the upper atmosphere. In other words, I have great doubts that anyone could have survived in the immediate disaster zone, unless they had specially equipped shelters that could withstand a nuclear attack. And such shelters, as we all understand, at the beginning of the 19th century, when, in my opinion, this disaster occurred, no one was yet able to build.

**Russian:**

Когда я начал более внимательно изучать космические снимки близлежащих
территорий, то очень быстро обнаружилось, что территория поражения не ограничивается
показанной выше территорией.

**English:**

When I began to look more closely at satellite images of nearby territories, it quickly became clear that the affected area is not limited to the territory shown above.

**Russian:**

Во-первых, похожие параллельные следы с характерным углом наклона, но меньшего
размера, были обнаружены на левом западном берегу реки Томь в районе города Томск, где
упало некоторое количество метеоритов из данного метеоритного поля.

**English:**

First, similar parallel tracks with a characteristic tilt angle, but smaller, were found on the left western bank of the Tom river near the city of Tomsk, where a certain number of meteorites from this meteorite field also fell.

<!-- Local
![](How Tartaria was Lost Part 1_files/2534_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2534/2534_original.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 1_files/2718_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2718/2718_original.jpg#resized)


**Russian:**

Если же двигаться на запад, в район Омска, Кургана и Челябинска, то там мы тоже
найдём следы метеоритной бомбардировки, но они уже выглядят несколько по другому.

**English:**

If we move to the west, to the area of Omsk, Kurgan and Chelyabinsk, then we will also find traces of meteor bombardment, but already they look somewhat different.

**Russian:**

Чуть выше Омска, на левом западном берегу реки Иртыш мы увидим характерные
смазанные следы, а также множество круглых озёр, которые являются кратерами от упавших
метеоритов. Угол наклона следов от 65 до 67 градусов. Следов и кратеров очень много,
размеры от 2 км до нескольких сот метров, но большая часть от 700 метров до 1200 метров.
То, что следы стали более короткие, а также имеются кратеры практически круглой формы
говорит о том, что здесь метеориты либо летели с меньшей скоростью, либо падали уже под
более отвесным углом, а возможно, что и то, и другое сразу.

**English:**

Just above Omsk, on the left western bank of the Irtysh river, we will see characteristic smudged tracks, as well as many round lakes that are craters from fallen meteorites. The angle of the tracks is from 65 to 67 degrees. There are a lot of tracks and craters, ranging in size from 2 km to several hundred meters, but most of them are from 700 meters to 1,200 meters. The fact that the tracks have become shorter, and there are craters of almost circular shape suggests that here the meteorites either flew at a lower speed, or fell at a more vertical angle, or perhaps both at once.

<!-- Local
![](How Tartaria was Lost Part 1_files/2718_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2718/2718_original.jpg#resized)

**Russian:**

*От Иртыша хорошо видимая на снимках полоса следов составляет порядка 110 км.*

**English:**

*From the Irtysh, the band of tracks that is clearly visible in the images is about 110 km.*

**Russian:**

Дальше на северо-запад, выше и восточнее города Ишим, наблюдается ещё одна
крупная область падения метеоритов. Причём характерные параллельные следы на снимках
читаются почти до самого Тобольска, ширина полосы от Ишима порядка 180 км. От Ишима
до Тобольска по прямой 240 км, то есть от Тобольска полоса падения прошла всего в 60 км.
Это важно, поскольку в первом издании энциклопедии "Британика", изданном в 1771 году,
упоминается, что столица Тартарии находилась в городе Тобольск.

**English:**

Further north-west, above and east of the city of Ishim, there is another major meteorite impact area. Moreover, the characteristic parallel tracks on the images can be read almost all the way to Tobolsk, the width of the strip from Ishim is about 180 km. From Ishim to Tobolsk in a straight line 240 km, that is, from Tobolsk, the fall lane was only 60 km away. This is important because the first edition of the encyclopedia Britannica, published in 1771, mentions that the capital of Tartary was located in the city of Tobolsk.

**Russian:**

На западе это поле следов ограничено рекой Тобол. В районе Тюмени мы подобных
следов уже не наблюдаем. Если же смотреть западнее Ишима, то мы увидим, что там следы также очень хорошо читаются на юге до Петропавловска, который находится на севере Казахстана. На запад же полоса продолжается практически до города Южноуральск в Челябинской области, но в районе Кургана мы уже почти не видим характерных вытянутых следов, но продолжаем наблюдать множество озёр и болот практически круглой формы диаметром от 200 метров до 2 км, при этом большая часть имеет диаметр в пределах 700 метров до 1 км. Общая длина поля порядка 600 км. На юге следы хорошо читаются по всему северу Казахстана, в том числе характерные смазанные следы под городом Рудный. Но там угол падения стал уже 70-73 градуса, что может быть вызвано тем, что в данном месте падение было позже и Земля успела повернуться вокруг своей оси, что изменило угол падения метеоритов. По этой же причине в конце следа мы наблюдаем в основном озёра-кратеры, а вытянутых следов практически нет.

**English:**

To the west, this field of footprints is bounded by the Tobol river. In the Tyumen region, we see no more tracks.

If we look to the west of Ishim, we will see that the tracks there are also very easy to read to the south to Petropavlovsk, which is located in the north of Kazakhstan. To the west, the strip continues almost to the city of Yuzhnouralsk in the Chelyabinsk region, but in the area of Kurgan we almost do not see the characteristic elongated tracks. However, we continue to observe many lakes and swamps of almost circular shape with a diameter of 200 meters to 2 km, while most of them have a diameter of 700 meters to 1 km. The total length of the field is about 600 km. In the south, the tracks are clearly visible throughout the north of Kazakhstan, including the characteristic smudged tracks under the city of Rudny. But there the angle of fall was already 70-73 degrees, which may be due to the fact that in this place the fall came later and the Earth had managed to turn around its axis, which changed the angle of the meteorites. For the same reason, at the end of the trail, we see mostly crater lakes, and there are almost no elongated tracks.

<!-- Local
![](How Tartaria was Lost Part 1_files/2960_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/2960/2960_original.jpg#resized)

<!-- Local
![](How Tartaria was Lost Part 1_files/3291_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/3291/3291_original.jpg#resized)

**Russian:**

*Следы северо-восточнее Ишима над с. Абатское*

**English:**

*Traces of the north-east of the Ishim river on S. Abatskaya*

<!-- Local
![](How Tartaria was Lost Part 1_files/3532_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/3532/3532_original.jpg#resized)

**Russian:**

*Следы под Тобольском*

**English:**

*Footprints near Tobolsk*

<!-- Local
![](How Tartaria was Lost Part 1_files/3713_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/3713/3713_original.jpg#resized)

**Russian:**

*Следы под городом Рудный, северо-запад Казахстана*

**English:**

*Tracks under the city of Rudny, north-west Kazakhstan*

**Russian:**

Для примера я хочу привести фрагмент снимка севернее Челябинска, где тоже
множество озёр, которые, согласно официальной версии, остались после отступления
ледника. Но, что интересно, тут мы вообще не наблюдаем круглых озёр диаметром от 500 до
1500 метров, а имеющиеся озёра далеко не круглой формы, так как заполняют естественные
углубления рельефа сложной формы.

**English:**

As an example, I want to show a fragment of the image north of Chelyabinsk, where there are also many lakes that, according to the official version, were left after the retreat of the glacier. But, interestingly, here we do not observe round lakes with a diameter of 500 to 1500 meters, and the existing lakes are far from circular in shape, because they fill natural recesses of the terrain of complex shape.

Translator's note: I think mylnikovdm is suggesting that this area shows how natural glacial lakes look. That he is using it as a counter example to meteor-orginated lakes.

<!-- Local
![](How Tartaria was Lost Part 1_files/4069_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/4069/4069_original.jpg#resized)


**Russian:**

*Форма и размеры озёр севернее Челябинска*

**English:**

*Shape and size of lakes North of Chelyabinsk*

**Russian:**

Таким образом на западе Сибири мы имеем гигантскую зону поражения, которая
пострадала от массированной метеоритной бомбардировки, общая площадь которой
превышает 1.5 миллиона километров! Если до катастрофы на этой территории существовало
какое-либо государство, то после него ни о каком величии и могуществе немногих чудом
выживших людей не могло быть и речи.

**English:**

Thus, in the west of Siberia, we have a huge zone of destruction, which suffered from a massive meteor bombardment, the total area of which exceeds 1.5 million kilometers! If there was any state on this territory before the disaster, then after it there was no question of the greatness and power of the few people who miraculously survived.

<!-- Local
![](How Tartaria was Lost Part 1_files/4251_original.jpg#resized)
-->

![](https://ic.pics.livejournal.com/mylnikovdm/24488457/4251/4251_original.jpg#resized)

**Russian:**

*Общая схема областей чётко читаемых следов*

**English:**

*General outline of areas of clearly legible traces*

**Russian:**

Ну хорошо, скажут скептики. То, что подобная гигантская катастрофа была, судя по
снимкам, можно согласиться, но из чего следует, что произошла она именно 200 лет назад?
Она могла произойти и несколько тысяч, а может даже миллионов лет назад, а потому к
исчезновению Тартарии, которой, возможно, и вовсе не было, не имеет никакого отношения.

**English:**

All right, say the skeptics. Judging by the pictures we can agree the fact that there was such a huge disaster. But what says it happened exactly 200 years ago? It could have happened several thousand or even millions of years ago, and therefore has nothing to do with the disappearance of Tartary, which may not have existed at all.

**Russian:**

*Продолжение следует...*

**21 августа 2014 года**
**Дмитрий Мыльников**

**English:**

*To be continued...*

**21 August 2014**
**Dmitry Mylnikov**

© All rights reserved. The original author retains ownership and rights.
