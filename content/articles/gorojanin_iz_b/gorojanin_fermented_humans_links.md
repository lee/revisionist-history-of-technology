title: gorojanin_iz_b's Fermented Humans articles - Google-translated
date: 2021-01-14
modified: 2022-10-20 10:28:59
category:
tags: forged history; links
slug:
authors: Anton Sizykh
from: https://gorojanin-iz-b.livejournal.com/50095.html
originally: 50095.html
local: 
summary: How non-humans shuttered the world... Links to main articles and supporting articles.
status: published

#### Translated from:

[50095 About Non-Humans - How non-humans shuttered the world](http://gorojanin-iz-b.livejournal.com/50095.html) (*original Russian*), [About nonhumans shuttered the world](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/50095.html) (*English translation*)
Indexes the article below:

- [22062 The Apocalypse](http://gorojanin-iz-b.livejournal.com/22062.html) (*original Russian*), [22062 The Apocalypse](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/22062.html) (*English translation*)

- [4771 The Apocalypse Part II](http://gorojanin-iz-b.livejournal.com/4771.html) (*original Russian*), [4771 The Apocalypse Part II](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/4771.html)

- [44289 Discussion and imagery of the Non-Humans](http://gorojanin-iz-b.livejournal.com/44289.html) (*original Russian*), [44289 Discussion and imagery of the Non-Humans](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/44289.html)

- [45936 About the Primary Edens](http://gorojanin-iz-b.livejournal.com/45936.html) (*original Russian*), [45936 About the Primary Edens](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/45936.html) (*English translation*)

- [46768 About the Primary Edens Part II](http://gorojanin-iz-b.livejournal.com/46768.html) (*original Russian*), [46768 About the Primary Edens Part II](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/46768.html) (*English translation*)
<!-- summarises human and infant remains showing eat-marks -->

- [48349 About Non-Humans Part I](http://gorojanin-iz-b.livejournal.com/48349.html) (*original Russian*), [About Non-Humans Part I](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/48349.html) (*English translation*)
- [48484 About Non-Humans Part II](http://gorojanin-iz-b.livejournal.com/48484.html) (*original Russian*), [About Non-Humans Part II](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/48484.html) (*English translation*)
- [48701  About Non-Humans Part III](http://gorojanin-iz-b.livejournal.com/48701.html) (*original Russian*), [ About Non-Humans Part III](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/48701.html) (*English translation*)
- [49031  About Non-Humans Part IV](http://gorojanin-iz-b.livejournal.com/49031.html) (*original Russian*), [ About Non-Humans Part IV](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/49031.html) (*English translation*)
- [49206  About Non-Humans Part V](http://gorojanin-iz-b.livejournal.com/49206.html) (*original Russian*), [ About Non-Humans Part V](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/49206.html) (*English translation*)
- [49493  About Non-Humans Part VI](http://gorojanin-iz-b.livejournal.com/49493.html) (*original Russian*), [ About Non-Humans Part VI](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/49493.html) (*English translation*)
- [8109 Technical addition to part VI](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/8109.html) (*English translation*)
- [49762 About Non-Humans Part VII](http://gorojanin-iz-b.livejournal.com/49762.html) (*original Russian*), [49762 About Non-Humans Part VII](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/49762.html) (*English translation*)
- [50179 About Non-Humans Part VIII](http://gorojanin-iz-b.livejournal.com/50179.html) (*original Russian*), [50179 About Non-Humans Part VIII](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/50179.html) (*English translation*)
- [52724 About Non-Humans Part IX](http://gorojanin-iz-b.livejournal.com/52724.html) (*original Russian*), [52724 About Non-Humans Part IX](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/52724.html) (*English translation*)
- [52795 About Non-Humans Part X](http://gorojanin-iz-b.livejournal.com/52795.html) (*original Russian*), [52795 About Non-Humans Part X](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/52795.html) (*English translation*)
- [53131 About Non-Humans Part XI](http://gorojanin-iz-b.livejournal.com/53131.html) (*original Russian*), [53131 About Non-Humans Part XI](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/53131.html) (*English translation*)
- [53291 About Non-Humans Part XII](http://gorojanin-iz-b.livejournal.com/53291.html) (*original Russian*), [53291 About Non-Humans Part XII](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/53291.html) (*English translation*)
- [53623 About Non-Humans Part XIII](http://gorojanin-iz-b.livejournal.com/53623.html) (*original Russian*), [53623 About Non-Humans Part XIII](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/53623.html) (*English translation*)

Related:

- [60831 Little sacred secret of religions](http://gorojanin-iz-b.livejournal.com/60831.html) (*original Russian*), [60831 Little sacred secret of religions](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/60831.html) (*English translation*)

- [61579 topic of why children feel bad in churches](http://gorojanin-iz-b.livejournal.com/61579.html) (*original Russian*), [61579 the topic of why children feel bad in churches](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/61579.html) (*English translation*)

- [61744 The return of cannibalism. Non-Human Program](http://gorojanin-iz-b.livejournal.com/61744.html) (*original Russian*), [61744 The return of cannibalism. Non-Human Program](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/61744.html) (*English translation*)

- [62171 Identifying gene-rubbish in holy, tap, drinking water](http://gorojanin-iz-b.livejournal.com/62171.html) (*original Russian*), [62171 Identifying gene-rubbish in holy, tap, drinking water](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/62171.html) (*English translation*)

- [62265 About trade in human flesh](http://gorojanin-iz-b.livejournal.com/62265.html) (*original Russian*), [62265 About trade in human flesh](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/62265.html).

- [65118 To whom do we give our blood? Syrian false war, and what it hides](http://gorojanin-iz-b.livejournal.com/65118.html) (*original Russian*), [65118 To whom do we give our blood? Syrian false war, and what it hides](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/65118.html) (*English translation*)

- [65566 Organ donation does not require organs from corpses](http://gorojanin-iz-b.livejournal.com/65566.html) (*original Russian*), [65566 Organ donation does not require organs from corpses](https://translate.google.com/website?sl=ru&tl=en&hl=en-GB&u=http://gorojanin-iz-b.livejournal.com/65566.html) (*English translation*)


© All rights reserved. The original author retains ownership and rights.

