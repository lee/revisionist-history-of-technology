title: Links to Non-english Electronics sites
date: 2021-07-03 15:06
modified: 2021-11-04 18:12:45
category: 
tags: links
slug: 
authors: Various
summary: Links to non-English alt-history research sites translated via Google-translate
status: draft


#### Electronics:


[Alexander Olikevich's journal (MNTC):](https://mntc-livejournal-com.translate.goog/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_pto=ajax)

[https://mntc.livejournal.com/11944.html](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://mntc.livejournal.com/11944.html)

[OpenFabLab.ru https://openfablab.livejournal.com/](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://openfablab.livejournal.com/)

[https://zen.yandex.ru/samelectric](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/samelectric)

[https://zen.yandex.ru/practical_electronics](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/practical_electronics)

[https://zen.yandex.ru/dima (auto-translates in Chromium](https://zen.yandex.ru/dima)

Visible article still at: [https://zen.yandex.ru/media/dima/samaia-prostaia-shema-vkliucheniia--vykliucheniia-odnoi-nefiksiruemoi-knopkoi-liuboi-nagruzki-6077e0bbb98df9344e5d0aa6](https://translate.google.com/website?sl=ru&tl=en&ajax=1&se=1&elem=1&u=https://zen.yandex.ru/media/dima/samaia-prostaia-shema-vkliucheniia--vykliucheniia-odnoi-nefiksiruemoi-knopkoi-liuboi-nagruzki-6077e0bbb98df9344e5d0aa6)

Anton Pisarev DIY-ACraft
[https://vk.com/moydomiksite_ru](https://vk.com/moydomiksite_ru)
[https://zen.yandex.ru/id/5ef920de322e1a13de505c91?lang=en](https://zen.yandex.ru/id/5ef920de322e1a13de505c91?lang=en)
[https://www.youtube.com/channel/UC9V64yNLAMcHQlOc2H5nFgQ?](https://www.youtube.com/channel/UC9V64yNLAMcHQlOc2H5nFgQ?)

