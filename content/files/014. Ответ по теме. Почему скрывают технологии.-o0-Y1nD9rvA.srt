﻿1
00:00:00,560 --> 00:00:04,200
...But we take an illustrative example.

2
00:00:04,400 --> 00:00:07,360
We have here a man
who came down from a tree.

3
00:00:07,560 --> 00:00:12,920
He came down and slowly developed
on this timeline and in the 18th century

4
00:00:13,120 --> 00:00:20,360
- starting a little bit in the 17th
century - went into industrial production.

5
00:00:20,560 --> 00:00:25,280
So it is for us humans and now
we have reached this production.

6
00:00:25,480 --> 00:00:29,640
Let's look at what we
have that does not fit.

7
00:00:29,840 --> 00:00:32,560
We have the Egyptian pyramids.

8
00:00:32,760 --> 00:00:38,040
They start at a point so far back
that they were build by gods or by aliens.

9
00:00:38,240 --> 00:00:39,840
But there they are

10
00:00:40,040 --> 00:00:43,040
. Here is the Bent Pyramid,

11
00:00:43,240 --> 00:00:44,360
here is

12
00:00:44,560 --> 00:00:48,200
the Temple of Dendera. There is the temple of BaalBek, which is
three thousand years old.

13
00:00:49,840 --> 00:00:52,040
There is

14
00:00:52,240 --> 00:00:53,280
St Isaacs Cathedral.

15
00:00:53,480 --> 00:00:55,680
Then what is easier.

16
00:00:55,880 --> 00:00:58,920
Development goes on like that.
According

17
00:00:59,120 --> 00:01:03,600
to the evidence that is the technological
structure that exists.

18
00:01:03,800 --> 00:01:10,560
This is not a question of the history
of events: it is a question

19
00:01:10,760 --> 00:01:16,920
of the technological level of what
happens in the 18th century.

20
00:01:20,040 --> 00:01:20,440
Here is the first car, Watt's steam engine, and so on and so forth. And

21
00:01:20,640 --> 00:01:27,120
from there it went up to the
1960s and then development stopped.

22
00:01:27,320 --> 00:01:34,400
Why, from 1961 - after the launch
of the first satellite into space

23
00:01:34,600 --> 00:01:38,240
- has not a single really new
invention emerged?

24
00:01:38,440 --> 00:01:45,440
That is, from the point of view of this
level of technology, it has frozen.

25
00:01:45,640 --> 00:01:49,160
All the progress is directed to

26
00:01:49,360 --> 00:01:53,440
improvement of dog toys and
[extensible] washing poles.

27
00:01:53,640 --> 00:01:56,120
Now, the next innovation is the creation

28
00:01:56,320 --> 00:02:01,560
of the next phone, although in essence
it's the same technology we already have.

29
00:02:01,760 --> 00:02:05,600
And the technology... Let's say

30
00:02:07,160 --> 00:02:07,170
we switched from 120nm to 45nm [CPU]

31
00:02:07,360 --> 00:02:12,240
technology. But nothing has changed
about the primacy of the technological

32
00:02:12,440 --> 00:02:18,480
ceiling when the frequency of, let's say,
a CPU core is no longer

33
00:02:18,480 --> 00:02:21,160
increasing, and the only
increase is in the number of cores.

34
00:02:21,360 --> 00:02:22,760
Despite the performance

35
00:02:22,960 --> 00:02:27,840
, questions about when to size
, what to do about layers change.

36
00:02:28,040 --> 00:02:29,800
But in the end we still

37
00:02:30,000 --> 00:02:36,600
have an operational technological ceiling
in this particular system, and what about

38
00:02:36,800 --> 00:02:42,120
all the other development? Of
industrial development in energy?

39
00:02:42,320 --> 00:02:46,280
The only thing is, let us suppose now

40
00:02:46,480 --> 00:02:47,600
we have a mechanised tree
felling machine (a manipulator).

41
00:02:47,600 --> 00:02:47,840
Just as we sawed trees 100 years ago, we still saw them today. It

42
00:02:48,040 --> 00:02:53,160
takes us forward but no further. In fact,
it is one and the same technology.

43
00:02:55,920 --> 00:02:58,520
It is called a manipulator but it is exactly
the same [sawing] technology.

44
00:02:58,720 --> 00:03:03,760
If we then moved on to, let us say,
fuel-less technology

45
00:03:05,600 --> 00:03:06,920
we could say we were developing.

46
00:03:15,000 --> 00:03:16,640
If we moved not to nano-technology, but to the moving of glass by willpower, then we could say that we are developing.

47
00:03:19,560 --> 00:03:21,120
Why did

48
00:03:22,760 --> 00:03:25,800
we exist for 11,000 years
on this technological level?

49
00:03:26,000 --> 00:03:30,840
We have existed for another
million years at this level.

50
00:03:31,040 --> 00:03:32,360
It is enough.
i.

51
00:03:32,440 --> 00:03:36,320
e.
they invented this situation for what? In

52
00:03:36,520 --> 00:03:42,200
order for us to sit here and think
that everything is okay with us,

53
00:03:42,400 --> 00:03:49,240
that it is normal when there is total
lawlessness? It was normal when savagery

54
00:03:49,440 --> 00:03:53,280
and barbarism were always
like this and so on.

55
00:03:53,480 --> 00:03:55,840
There are a lot of examples.
