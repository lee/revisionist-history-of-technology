title: Atlantida-Pravda's Links
date: 2016-03
modified: 2022-04-24 10:39:43
category: Atlantida-Pravda
tags: links
slug: 
authors: ZigZag
summary: Links to various Atlantida-Pravda pages. Destination pages are in Russian and present a translation option once the page is fully loaded.
status: published
originally: atlantida-pravda links page.md

### Translated from: 
[http://atlantida-pravda-i-vimisel.blogspot.com/2016/03/blog-post.html](http://atlantida-pravda-i-vimisel.blogspot.com/2016/03/blog-post.html)

From [Aqueducts - Why?](https://atlantida--pravda--i--vimisel-blogspot-com.translate.goog/search/label/%D0%9C%D0%B0%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0.%20%D0%92%D1%81%D1%91%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en-GB&_x_tr_sch=http):

> ...ancient aqueducts were built by alien expeditions to bring spring water rich in precious metals to evaporation complexes. Curiously, some of them are part of the evaporation complex, i.e. spring water was heated and began to evaporate already when moving along the aqueducts. Moreover, some aqueducts are flow-type electromagnetic heaters/boilers in which several harmonics are formed. These are triangular bias current generators, which are more powerful than sinusoidal current generators.

These links were taken from the foot of one page in Atlantida-Pravda's ['Mysteries of history. Controversial facts and speculations'](http://atlantida-pravda-i-vimisel.blogspot.com/2016/03/blog-post.html) series.

Translator's note: most of this list of links has been translated into English switches, though a section mid-way through has both Russian and English.

-   ["Wonders of the World"](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%22%D0%A7%D1%83%D0%B4%D0%B5%D1%81%D0%B0%20%D1%81%D0%B2%D0%B5%D1%82%D0%B0%22) (2)
-   ["Forbidden" finds](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%C2%AB%D0%97%D0%B0%D0%BF%D1%80%D0%B5%D1%89%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5%C2%BB%20%D0%BD%D0%B0%D1%85%D0%BE%D0%B4%D0%BA%D0%B8) (7)
-   [Is everything as it seems?](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%90%20%D0%B2%D1%81%D1%91%20%D0%BB%D0%B8%20%D1%82%D0%B0%D0%BA%20%D0%BA%D0%B0%D0%BA%20%D0%BA%D0%B0%D0%B6%D0%B5%D1%82%D1%81%D1%8F%3F) (6)
-   [All Around is Lies](http://atlantida-pravda-i-vimisel.blogspot.com/2012/10/blog-post_8530.html)
-   [ANOMALOUS ZONES](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%90%D0%9D%D0%9E%D0%9C%D0%90%D0%9B%D0%AC%D0%9D%D0%AB%D0%95%20%D0%97%D0%9E%D0%9D%D0%AB) (15)
-   [No comments - AKA Mysteries of History: Controversial Facts and Speculations](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%91%D0%B5%D0%B7%20%D0%BA%D0%BE%D0%BC%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%80%D0%B8%D0%B5%D0%B2) (5)
-   [Was there a nuclear winter?](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%91%D1%8B%D0%BB%D0%B0%20%D0%BB%D0%B8%20%D1%8F%D0%B4%D0%B5%D1%80%D0%BD%D0%B0%D1%8F%20%D0%B7%D0%B8%D0%BC%D0%B0%3F) (3)
-   [Great win or big bluff](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%B0%D1%8F%20%D0%BF%D0%BE%D0%B1%D0%B5%D0%B4%D0%B0%20%D0%B8%D0%BB%D0%B8%20%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%B9%20%D0%B1%D0%BB%D0%B5%D1%84) (7)
-   [Water and its secrets](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%92%D0%BE%D0%B4%D0%B0%20%D0%B8%20%D0%B5%D1%91%20%D1%82%D0%B0%D0%B9%D0%BD%D1%8B) (1)
-   [Questions AKA Who is the author? Whose is it all?](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%92%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B) (3)
-   [Geoglyphs](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%93%D0%B5%D0%BE%D0%B3%D0%BB%D0%B8%D1%84%D1%8B) (5)
-   [Giant Statues](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%93%D0%B8%D0%B3%D0%B0%D0%BD%D1%82%D1%81%D0%BA%D0%B8%D0%B5%20%D1%81%D1%82%D0%B0%D1%82%D1%83%D0%B8) (1)
-   [Giants](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%93%D0%B8%D0%B3%D0%B0%D0%BD%D1%82%D1%8B) (3)
-   [Hypotheses](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%93%D0%B8%D0%BF%D0%BE%D1%82%D0%B5%D0%B7%D1%8B) (6)
-   [Dolmens](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%94%D0%BE%D0%BB%D1%8C%D0%BC%D0%B5%D0%BD%D1%8B) (3)
-   [There is such a place](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%95%D1%81%D1%82%D1%8C%20%D1%82%D0%B0%D0%BA%D0%BE%D0%B5%20%D0%BC%D0%B5%D1%81%D1%82%D0%BE) (6)
-   [The life and death of the last three civilizations](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%96%D0%B8%D0%B7%D0%BD%D1%8C%20%D0%B8%20%D0%B3%D0%B8%D0%B1%D0%B5%D0%BB%D1%8C%20%D1%82%D1%80%D1%91%D1%85%20%D0%BF%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BD%D0%B8%D1%85%20%D1%86%D0%B8%D0%B2%D0%B8%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B9) (15)
-   [Beyond the Earth/Land](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%97%D0%B0%20%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B0%D0%BC%D0%B8%20%D0%B7%D0%B5%D0%BC%D0%BB%D0%B8) (8)
-   [RIDDLES OF PAST CIVILIZATIONS](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%97%D0%90%D0%93%D0%90%D0%94%D0%9A%D0%98%20%D0%A3%D0%A8%D0%95%D0%94%D0%A8%D0%98%D0%A5%20%D0%A6%D0%98%D0%92%D0%98%D0%9B%D0%98%D0%97%D0%90%D0%A6%D0%98%D0%99) (2)
-   [Sunken Lands](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%97%D0%B0%D1%82%D0%BE%D0%BD%D1%83%D0%B2%D1%88%D0%B8%D0%B5%20%D0%B7%D0%B5%D0%BC%D0%BB%D0%B8) (1)
-   [Mirrors](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%97%D0%B5%D1%80%D0%BA%D0%B0%D0%BB%D0%B0) (1)
-   [Distortion of History](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%98%D1%81%D0%BA%D0%B0%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%B8) (4)
-   [The Vanished Continent](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%98%D1%81%D1%87%D0%B5%D0%B7%D0%BD%D1%83%D0%B2%D1%88%D0%B8%D0%B9%20%D0%BA%D0%BE%D0%BD%D1%82%D0%B8%D0%BD%D0%B5%D0%BD%D1%82) (4)
-   [End of the World](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9A%D0%BE%D0%BD%D0%B5%D1%86%20%D1%81%D0%B2%D0%B5%D1%82%D0%B0) (4)
-   [Crop Circles](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9A%D1%80%D1%83%D0%B3%D0%B8%20%D0%BD%D0%B0%20%D0%BF%D0%BE%D0%BB%D1%8F%D1%85) (4)
-   [Левитация - Levitation](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9B%D0%B5%D0%B2%D0%B8%D1%82%D0%B0%D1%86%D0%B8%D1%8F) (3)
-   [Легенды - Legends](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9B%D0%B5%D0%B3%D0%B5%D0%BD%D0%B4%D1%8B) (1)
-   [Ледниковый период - Ice Age](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9B%D0%B5%D0%B4%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D1%8B%D0%B9%20%D0%BF%D0%B5%D1%80%D0%B8%D0%BE%D0%B4) (5)
-   [Люди-звери - Animal People](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9B%D1%8E%D0%B4%D0%B8-%D0%B7%D0%B2%D0%B5%D1%80%D0%B8) (4)
-   [Математика. Всё просто - Math: It's Simple](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9C%D0%B0%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0.%20%D0%92%D1%81%D1%91%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE) (1)
-   [Мегалиты - Megaliths](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9C%D0%B5%D0%B3%D0%B0%D0%BB%D0%B8%D1%82%D1%8B) (8)
-   [Мифы о космосе - Myths about the Universe](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9C%D0%B8%D1%84%D1%8B%20%D0%BE%20%D0%BA%D0%BE%D1%81%D0%BC%D0%BE%D1%81%D0%B5) (1)
-   [Мумии - Mummies](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9C%D1%83%D0%BC%D0%B8%D0%B8) (1)
-   [Наследие прошлых цивилизаций - Legacy of Past Civilisations](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9D%D0%B0%D1%81%D0%BB%D0%B5%D0%B4%D0%B8%D0%B5%20%D0%BF%D1%80%D0%BE%D1%88%D0%BB%D1%8B%D1%85%20%D1%86%D0%B8%D0%B2%D0%B8%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B9) (14)
-   [Необъясненимые наукой явления - Phenomena Science Cannot Explain](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9D%D0%B5%D0%BE%D0%B1%D1%8A%D1%8F%D1%81%D0%BD%D0%B5%D0%BD%D0%B8%D0%BC%D1%8B%D0%B5%20%D0%BD%D0%B0%D1%83%D0%BA%D0%BE%D0%B9%20%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F) (4)
-   [Николa Тесла - Nikola Tesla](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9D%D0%B8%D0%BA%D0%BE%D0%BBa%20%D0%A2%D0%B5%D1%81%D0%BB%D0%B0) (1)
-   [Нубиру - Nubira (Nubia?)](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9D%D1%83%D0%B1%D0%B8%D1%80%D1%83) (8)
-   [О концах света - About the Ends of the World](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9E%20%D0%BA%D0%BE%D0%BD%D1%86%D0%B0%D1%85%20%D1%81%D0%B2%D0%B5%D1%82%D0%B0) (3)
-   [Образование - Education](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9E%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5) (1)
-   [Откуда город? - Where is the City From?](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9E%D1%82%D0%BA%D1%83%D0%B4%D0%B0%20%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%3F) (28)
-   [Параллельные миры - Parallel Worlds](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%B0%D1%80%D0%B0%D0%BB%D0%BB%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5%20%D0%BC%D0%B8%D1%80%D1%8B) (7)
-   [Петроглифы - Petroglyphs](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%B3%D0%BB%D0%B8%D1%84%D1%8B) (2)
-   [Под водой - Under Water](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%BE%D0%B4%20%D0%B2%D0%BE%D0%B4%D0%BE%D0%B9) (2)
-   [Под землёй - Underground](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%BE%D0%B4%20%D0%B7%D0%B5%D0%BC%D0%BB%D1%91%D0%B9) (10)
-   [Потерянные знания - Lost Knowledge](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%BE%D1%82%D0%B5%D1%80%D1%8F%D0%BD%D0%BD%D1%8B%D0%B5%20%D0%B7%D0%BD%D0%B0%D0%BD%D0%B8%D1%8F) (1)
-   [Потоп - The Flood](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D0%BE%D1%82%D0%BE%D0%BF) (10)
-   [Предсказания - Predictions](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D1%80%D0%B5%D0%B4%D1%81%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%B8%D1%8F) (1)
-   [Пришельцы со звёзд - Aliens from the Stars](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D1%80%D0%B8%D1%88%D0%B5%D0%BB%D1%8C%D1%86%D1%8B%20%D1%81%D0%BE%20%D0%B7%D0%B2%D1%91%D0%B7%D0%B4) (1)
-   [Путешествия во времени - Time Travel](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%9F%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%B8%D1%8F%20%D0%B2%D0%BE%20%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%B8) (5)
-   [Разнообразие форм жизни - Diversity of Life Forms](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%B8%D0%B5%20%D1%84%D0%BE%D1%80%D0%BC%20%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8) (1)
-   [Революции - Revolutions](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A0%D0%B5%D0%B2%D0%BE%D0%BB%D1%8E%D1%86%D0%B8%D0%B8) (2)
-   [Самодвижущиеся камни - Self-Moving Stones](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A1%D0%B0%D0%BC%D0%BE%D0%B4%D0%B2%D0%B8%D0%B6%D1%83%D1%89%D0%B8%D0%B5%D1%81%D1%8F%20%D0%BA%D0%B0%D0%BC%D0%BD%D0%B8) (6)
-   [Семь чудес света Древнего Мира - Seven Wonders of the Ancient World](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A1%D0%B5%D0%BC%D1%8C%20%D1%87%D1%83%D0%B4%D0%B5%D1%81%20%D1%81%D0%B2%D0%B5%D1%82%D0%B0%20%D0%94%D1%80%D0%B5%D0%B2%D0%BD%D0%B5%D0%B3%D0%BE%20%D0%9C%D0%B8%D1%80%D0%B0) (10)
-   [Тайна Фаэтона - The Secret of the Phaeton](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D0%B0%20%D0%A4%D0%B0%D1%8D%D1%82%D0%BE%D0%BD%D0%B0) (2)
-   [Тайна чисел -  The Mystery of Numbers](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D0%B0%20%D1%87%D0%B8%D1%81%D0%B5%D0%BB) (1)
-   [Тайна шумеров - The Secret of the Sumerians](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D0%B0%20%D1%88%D1%83%D0%BC%D0%B5%D1%80%D0%BE%D0%B2) (2)
-   [Тайны Азии - Secrets of Asia](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%90%D0%B7%D0%B8%D0%B8) (4)
-   [Тайны Американских континентов - Secrets of the American Continents](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%90%D0%BC%D0%B5%D1%80%D0%B8%D0%BA%D0%B0%D0%BD%D1%81%D0%BA%D0%B8%D1%85%20%D0%BA%D0%BE%D0%BD%D1%82%D0%B8%D0%BD%D0%B5%D0%BD%D1%82%D0%BE%D0%B2) (17)
-   [Тайны Антарктиды - Secrets of Antartica](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%90%D0%BD%D1%82%D0%B0%D1%80%D0%BA%D1%82%D0%B8%D0%B4%D1%8B) (1)
-   [Тайны Атлантиды - Secrets of Atlantis](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%90%D1%82%D0%BB%D0%B0%D0%BD%D1%82%D0%B8%D0%B4%D1%8B) (10)
-   [Тайны Африки - Secrets of Africa](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%90%D1%84%D1%80%D0%B8%D0%BA%D0%B8) (8)
-   [Тайны Бермуд - Mysteries of Bermuda](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%91%D0%B5%D1%80%D0%BC%D1%83%D0%B4) (5)
-   [Тайны Ближнего Востока - Secrets of the Middle East](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%91%D0%BB%D0%B8%D0%B6%D0%BD%D0%B5%D0%B3%D0%BE%20%D0%92%D0%BE%D1%81%D1%82%D0%BE%D0%BA%D0%B0) (3)
-   [Mysteries of Time](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%B8) (1)
-   [Secrets of Europe](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%95%D0%B2%D1%80%D0%BE%D0%BF%D1%8B) (23)
-   [Secrets of Egypt](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%95%D0%B3%D0%B8%D0%BF%D1%82%D0%B0) (19)
-   [Secrets of Sound](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%B7%D0%B2%D1%83%D0%BA%D0%B0) (1)
-   [Secrets of India](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%98%D0%BD%D0%B4%D0%B8%D0%B8) (10)
-   [Secrets of Kamchatka](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%9A%D0%B0%D0%BC%D1%87%D0%B0%D1%82%D0%BA%D0%B8) (1)
-   [Secrets of China](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%9A%D0%B8%D1%82%D0%B0%D1%8F) (14)
-   [Moon Secrets](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%BB%D1%83%D0%BD%D1%8B) (26)
-   [Mysteries of Malta](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%9C%D0%B0%D0%BB%D1%8C%D1%82%D1%8B) (1)
-   [Mysteries of Mars](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%9C%D0%B0%D1%80%D1%81%D0%B0) (6)
-   [Easter Island Secrets](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B2%D0%B0%20%D0%9F%D0%B0%D1%81%D1%85%D0%B8) (2)
-   [Secrets of Tibet](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%A2%D0%B8%D0%B1%D0%B5%D1%82%D0%B0) (6)
-   [Secrets of the Urals](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B0%D0%B9%D0%BD%D1%8B%20%D0%A3%D1%80%D0%B0%D0%BB%D0%B0) (2)
-   [Teleportation](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B5%D0%BB%D0%B5%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D1%86%D0%B8%D1%8F) (1)
-   [Conspiracy Theories](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B5%D0%BE%D1%80%D0%B8%D0%B8%20%D0%B7%D0%B0%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%B0) (2)
-   [Paleocontact theory](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B5%D0%BE%D1%80%D0%B8%D1%8F%20%D0%BF%D0%B0%D0%BB%D0%B5%D0%BE%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B0) (7)
-   [Technology of the Gods](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A2%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8%20%D0%B1%D0%BE%D0%B3%D0%BE%D0%B2) (8)
-   [Facts & Speculation](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A4%D0%B0%D0%BA%D1%82%D1%8B%20%D0%B8%20%D0%B4%D0%BE%D0%BC%D1%8B%D1%81%D0%BB%D1%8B) (21)
-   [Photo report](http://atlantida-pravda-i-vimisel.blogspot.com/search/label/%D0%A4%D0%BE%D1%82%D0%BE%D1%80%D0%B5%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%B6) (6)

© All rights reserved. The original author retains ownership and rights.
